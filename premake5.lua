workspace "Shard3D"
   architecture "x86_64"
   preferredtoolarchitecture "x86_64"
   configurations { "Debug", "Release" }

local outputSubfolder = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"
local VULKAN_SDK = os.getenv("VULKAN_SDK")

local editorKitLib = { 
	 "export/"..outputSubfolder,
     "editor_kit/extern/imgui/lib",
	 "editor_kit/extern/imguizmo/lib"
   }
local editorKitLink = { 
	"Shard3D",
	"Shard3DEditorKit",
	"imguizmo"
   }

local globalInclude = {
	  VULKAN_SDK.."/Include",
	  "engine/extern/volk/include",
	  "engine/extern/glfw-3.3.7/include", 
	  "engine/extern/spdlog", 
	  "engine/extern/entt/1include", 
	  "engine/extern/stb/include", 
	  "engine/extern/miniaudio/1include", 
	  "engine/extern/mono/include", 
	  "engine/extern/assimp/include", 
	  "engine/extern/json/include",
	  "engine/extern/half",
	  "engine/extern/zstd/include",
	  "engine/extern/libktx/include",
	  "engine/extern/SHUDCore/include",
	  "engine/extern/SHUDVulkan/include",
	  "engine/extern/ode/include",
	  "engine/extern/magic_enum/include", 
	  "reactor_physics"
   }
local editorKitInclude = {
	  "editor_kit",
      "editor_kit/extern/imgui",
	  "editor_kit/extern/imguizmo",
      "editor_kit/extern/imnodes"
}
	  
project "ReactorPhysicsEngine"
   location "reactor_physics"
   debugdir "./"
   
   local bin = "export/bin/"..outputSubfolder
  
   kind "StaticLib"
   language "C++"
   cppdialect "C++17"
   targetdir (bin)

   flags { "MultiProcessorCompile" }
   disablewarnings { "26812;4244;4996;4005" }
   
   files {
	  "reactor_physics/ReactorPhysicsEngine/**.*",
	  "reactor_physics/ReactorPhysicsEngine/solvers/**.*",
	  "reactor_physics/ReactorPhysicsEngine/colliders/**.*",
	  "reactor_physics/ReactorPhysicsEngine/physics/**.*",
	  "reactor_physics/ReactorPhysicsEngine/constraints/**.*"
   }

   includedirs {
	  VULKAN_SDK.."/Include", -- only for GLM
	  "reactor_physics",
	  "reactor_physics/extern/ccd/include"
   }
   libdirs {
	  "reactor_physics/extern/ccd/lib",
   }
   links {	
	
   }

   filter "configurations:Debug"
	  defines { "_DEBUG" }
      runtime "Debug"
      symbols "On"
	  links {
	  "ccd_d"
	  }

   filter "configurations:Release"
	  defines { "NDEBUG" }
      runtime "Release"
      optimize "Speed"
	  links {
	  "ccd"
	  }
	  
project "Shard3D"
   location "engine/Shard3D"
   debugdir "./"
   local bin = "engine/Shard3D/bin/"..outputSubfolder

   kind "StaticLib"
   language "C++"
   cppdialect "C++17"
   targetdir (bin)

   flags { "MultiProcessorCompile" }
   disablewarnings { "26812;4244;4996;4005" }
   
   files {
      "engine/Shard3D/**.cpp",
      "engine/Shard3D/**.h",
      
	  "engine/Shard3D/core/asset/**.*",
	  "engine/Shard3D/core/audio/**.*",
      "engine/Shard3D/core/ecs/**.*",
	  "engine/Shard3D/core/ecs/components/**.*",
	  "engine/Shard3D/core/misc/**.*",
      "engine/Shard3D/core/rendering/**.*",
      "engine/Shard3D/core/vulkan_api/**.*",
      "engine/Shard3D/core/math/**.*",
      
	  "engine/Shard3D/events/**.h",
	  "engine/Shard3D/plugins/**.h",
	  
	  
      "engine/Shard3D/scripting/**.cpp",
      "engine/Shard3D/scripting/**.h",
	  
	  "engine/Shard3D/systems/systems.h",
	  "engine/Shard3D/systems/buffers/**.*",
	  "engine/Shard3D/systems/computational/**.*",
	  "engine/Shard3D/systems/handlers/**.*",
	  "engine/Shard3D/systems/rendering/**.*",
	  "engine/Shard3D/systems/post_fx/**.*",
	  "engine/Shard3D/systems/post_fx/fx/**.*",
	 
      "engine/Shard3D/utils/**.h",
      "engine/Shard3D/utils/**.cpp"
   }
   
   includedirs {
	  globalInclude,
	  "engine/extern/bitmap"
   }
   libdirs {
	  VULKAN_SDK.."/Lib",
	  "engine/extern/volk/lib",
	  "engine/extern/glfw-3.3.7/lib-vc2019", 
	  "engine/extern/json/lib", 
	  "engine/extern/mono/lib", 
	  "engine/extern/assimp/lib",
	  "engine/extern/zstd/lib",
	  "engine/extern/libktx/lib",
	  "engine/extern/SHUDCore/lib",
	  "engine/extern/SHUDVulkan/lib",
	  "engine/extern/ode/lib",
	  "export/"..outputSubfolder
   }
   links {
	  "glfw3",
	  "ktx",
	  "ktx_read",
	  "libzstd_dll",
	  "objUtil",
-- convenient for some functions
	  "ReactorPhysicsEngine"
   }

   filter { "system:windows" }
      links {
	   "Ws2_32",
	   "Winmm",
	   "Bcrypt",
	   "Version",
	   "Comdlg32", 
	   "shlwapi"
      }
	
   filter "configurations:Debug"
      defines { "_DEBUG", "_LIB" }
      runtime "Debug"
      symbols "On"
	  links {
	   "volk_d",
	   "shadercd",
	   "shaderc_combinedd",
	   "shaderc_sharedd",
	   "shaderc_utild",
	   "libmono-static-sgen_d",
	   "assimp-vc143-mtd",
	   "simdjson_d",
	   "oded",
	   "SHUDCore_d"
	  }

   filter "configurations:Release"
	  defines { "NDEBUG" }
      runtime "Release"
      optimize "Speed"
	  links {
	   "volk",
	   "shaderc",
	   "shaderc_combined",
	   "shaderc_shared",
	   "shaderc_util",
	   "libmono-static-sgen",
	   "assimp-vc143-mt",
	   "simdjson",
	   "ode",
	   "SHUDCore",
	  } 

project "Shard3DEditorKit"
   location "editor_kit"
   debugdir "./"
   
   local bin = "export/bin/"..outputSubfolder
  
   kind "StaticLib"
   language "C++"
   cppdialect "C++17"
   targetdir (bin)

   flags { "MultiProcessorCompile" }
   disablewarnings { "26812;4244;4996;4005" }
   
   files {
	  "editor_kit/Shard3DEditorKit/core.h",
	  "editor_kit/Shard3DEditorKit/framework.h",
	  "editor_kit/Shard3DEditorKit/kit.h",
	  "editor_kit/Shard3DEditorKit/dllmain.cpp",
	  "editor_kit/Shard3DEditorKit/gui/**.*",
	  "editor_kit/Shard3DEditorKit/rendering/**.*",
	  "editor_kit/Shard3DEditorKit/tools/**.*"
   }

   includedirs {
	  globalInclude,
      editorKitInclude,
	  "engine"
   }
   libdirs {
	 "engine/Shard3D/bin/"..outputSubfolder,
	 "editor_kit/extern/imgui/lib",
	 "editor_kit/extern/imguizmo/lib"
   }
   links {	
	 "Shard3D",
	 "imguizmo"
   }

   filter { "system:windows" }
      links {  
	  "kernel32", 
	  "user32", 
	  "gdi32", 
	  "winspool", 
	  "comdlg32", 
	  "advapi32", 
	  "shell32", 
	  "ole32", 
	  "oleaut32", 
	  "uuid", 
	  "odbc32", 
	  "odbccp32"
      }
	
   filter "configurations:Debug"
	  defines { "_DEBUG", "S3D_DLLEXPORT" }
      runtime "Debug"
      symbols "On"
	  links {
		"ImGui_d"
	  }

   filter "configurations:Release"
	  defines { "NDEBUG", "S3D_DLLEXPORT" }
      runtime "Release"
      optimize "Speed"
	  links {
	   "ImGui"
	  }
	  
project "Shard3DEditor"
   location "editor"
   debugdir "./"
   
   local bin = "export/bin/"..outputSubfolder
  
   kind "ConsoleApp"
   language "C++"
   cppdialect "C++17"
   targetdir (bin)

   flags { "MultiProcessorCompile" }
   disablewarnings { "26812;4244;4996;4005" }
   
   files {
	  "editor/application.h",
	  "editor/application.cpp",
	  "editor/main.cpp",
	  "editor/imgui/**.*",
	  "editor/panels/**.*",
	  "editor/panels/material_panel/**.*",
	  "editor/editor/**.*"
   }

   includedirs {
	  globalInclude,
	  "engine",
	  editorKitInclude
   }
   libdirs {
	  editorKitLib
   }
   links {
	  editorKitLink
   }

   icon "exeico_editor.ico"
   filter { "system:windows" }
	  files { "$(SolutionDir)/editor/Shard3DEditor.rc", "**.ico" }
	  vpaths { ["Resources/*"] = { "*.rc", "**.ico" } }
      links {  
      }
	
   filter "configurations:Debug"
	  defines { "_DEBUG" }
      runtime "Debug"
      symbols "On"
	  links {
	  }

   filter "configurations:Release"
	  defines { "NDEBUG" }
      runtime "Release"
      optimize "Speed"
	  links {
	  }

project "Shard3DRuntime"
   location "runtime"
   debugdir "./"
   
   local bin = "export/bin/"..outputSubfolder
  
   kind "ConsoleApp"
   language "C++"
   cppdialect "C++17"
   targetdir (bin)

   flags { "MultiProcessorCompile" }
   disablewarnings { "26812;4244;4996;4005" }
   
   files {
	  "runtime/application.h",
	  "runtime/application.cpp",
	  "runtime/main.cpp",
   }

   includedirs {
	  globalInclude,
	  "engine"
   }
   libdirs {
	 "engine/Shard3D/bin/"..outputSubfolder
   }
   links {
	  "Shard3D"
   }

   filter { "system:windows" }
      links {  
	  "kernel32", 
	  "user32", 
	  "gdi32", 
	  "winspool", 
	  "comdlg32", 
	  "advapi32", 
	  "shell32", 
	  "ole32", 
	  "oleaut32", 
	  "uuid", 
	  "odbc32", 
	  "odbccp32"
      }
	
   filter "configurations:Debug"
	  defines { "_DEBUG" }
      runtime "Debug"
      symbols "On"

   filter "configurations:Release"
	  defines { "NDEBUG" }
      runtime "Release"
      optimize "Speed"
	  
project "s3dcorelib"
   location "script"

   kind "SharedLib"
   language "C#"
   clr "Unsafe"
   dotnetframework "4.5"
   targetdir ("resources/scriptdata")
   objdir ("script/obj")
   
   files {
   	"script/**.cs",
   	"script/actorsystem/**.cs",
   	"script/animation/**.cs",
   	"script/math/**.cs",
   	"script/actorsystem/**.cs",
   }
   

   filter "configurations:Release"
   	optimize "Full"
   	symbols "Off"