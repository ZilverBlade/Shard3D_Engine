#pragma once
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>

namespace RPhys {
	class PhysicsActor;
	class CollisionResult {
	public:
		CollisionResult() = default;
		CollisionResult(const bool intersects, const glm::vec3 direction, const float length, const glm::vec3 pointWorldOnA, const glm::vec3 pointWorldOnB, const float timeOfImpact)
			: intersects(intersects), direction(direction), length(length), pointWorldOnA(pointWorldOnA), pointWorldOnB(pointWorldOnB), timeOfImpact(timeOfImpact) {}
		bool intersects = false;
		glm::vec3 direction{};
		float length{};
		glm::vec3 pointWorldOnA{};
		glm::vec3 pointWorldOnB{};
		float timeOfImpact{};
	};
	
	struct IntersectionInfo {
		IntersectionInfo() = default;
		PhysicsActor* actorA{};
		PhysicsActor* actorB{};
	};
	struct CollisionData {
		CollisionData() = default;
		IntersectionInfo info;
		CollisionResult result;
	};
}