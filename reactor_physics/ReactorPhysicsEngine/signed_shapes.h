#pragma once
#include <glm/glm.hpp>
#include <glm/gtx/norm.hpp>
#include <ReactorPhysicsEngine/matrix_math.h>
#include <ReactorPhysicsEngine/convex_shape.h>
namespace RPhys {

	struct PointMinkowski {
		glm::vec3 diffPoint{};
		glm::vec3 ptA{};
		glm::vec3 ptB{};

		PointMinkowski() = default;
		const PointMinkowski& operator=(const PointMinkowski& other) {
			diffPoint = other.diffPoint;
			ptA = other.ptA;
			ptB = other.ptB;
			return *this;
		}

		bool operator==(const PointMinkowski& other) const {
			return ptA == other.ptA && ptB == other.ptB && diffPoint == other.diffPoint;
		}
	};

	static inline bool compareSigns(float a, float b) {
		return (a > 0.0f && b > 0.0f) || (a < 0.0f && b < 0.0f);
	}

	static inline glm::vec2 signedVolume1D(glm::vec3 s1, glm::vec3 s2) {
		const glm::vec3 ab = s2 - s1;
		const glm::vec3 ap = -s1;
		const glm::vec3 p0 = s1 + ab * glm::dot(ab, ap) / glm::length2(ab);

		int index = 0;
		float muMax = 0.0f;
		for (int i = 0; i < 3; i++) {
			const float mu = s2[i] - s1[i];
			if (mu * mu > muMax * muMax) {
				muMax = mu;
				index = i;
			}
		}

		const float a = s1[index];
		const float b = s2[index];
		const float p = p0[index];

		const float C1 = p - a;
		const float C2 = b - p;

		if ((p > a && p < b) || (p > b && p < a)) {
			return glm::vec2{ C2, C1 } / muMax;
		}
		if ((a <= b && p <= a) || (a >= b && p >= a)) {
			return { 1.0f, 0.0f };
		}
		return { 0.0f, 1.0f };
	}

	static inline glm::vec3 signedVolume2D(glm::vec3 s1, glm::vec3 s2, glm::vec3 s3) {
		const glm::vec3 normal = glm::cross(s2 - s1, s3 - s1);
		const glm::vec3 p0 = normal * glm::dot(s1, normal) / glm::length2(normal);

		int index = 0;
		float areaMax = 0.0f;
		for (int i = 0; i < 3; i++) {
			const int j = (i + 1) % 3;
			const int k = (i + 2) % 3;

			const glm::vec2 a = { s1[j], s1[k] };
			const glm::vec2 b = { s2[j], s2[k] };
			const glm::vec2 c = { s3[j], s3[k] };
			const glm::vec2 ab = b - a;
			const glm::vec2 ac = c - a;

			const float area = ab.x * ac.y - ab.y * ac.x;
			if (area * area > areaMax * areaMax) {
				areaMax = area;
				index = i;
			}
		}
		const int x = (index + 1) % 3;
		const int y = (index + 2) % 3;

		const glm::vec2 s[3]{
			{ s1[x], s1[y] },
			{ s2[x], s2[y] },
			{ s3[x], s3[y] }
		};
		const glm::vec2 p = { p0[x], p0[y] };

		glm::vec3 areas{};
		for (int i = 0; i < 3; i++) {
			const int j = (i + 1) % 3;
			const int k = (i + 2) % 3;

			const glm::vec2 a = p;
			const glm::vec2 b = s[j];
			const glm::vec2 c = s[k];
			const glm::vec2 ab = b - a;
			const glm::vec2 ac = c - a;

			areas[i] = ab.x * ac.y - ab.y * ac.x;
		}
		
		if (compareSigns(areaMax, areas[0]) && compareSigns(areaMax, areas[1]) && compareSigns(areaMax, areas[2])) {
			return areas / areaMax;
		}

		float dist = 3.4e38f;

		glm::vec3 lambdas = { 1.0f, 0.0f, 0.0f };
		for (int i = 0; i < 3; i++) {
			const int j = (i + 1) % 3;
			const int k = (i + 2) % 3;

			const glm::vec3 edges[3]{
				s1, s2, s3
			};

			const glm::vec2 lambdaEdge = signedVolume1D(edges[j], edges[k]);
			const glm::vec3 point = edges[j] * lambdaEdge[0] + edges[k] * lambdaEdge[1];
			if (float lenSq = glm::length2(point); lenSq < dist) {
				dist = lenSq;
				lambdas[i] = 0.0f;
				lambdas[j] = lambdaEdge[0];
				lambdas[k] = lambdaEdge[1];
			}
		}
		return lambdas;
	}

	static inline glm::vec4 signedVolume3D(glm::vec3 s1, glm::vec3 s2, glm::vec3 s3, glm::vec3 s4) {
		const glm::mat4 m = {
			{ s1.x, s1.y, s1.z, 1.0f },
			{ s2.x, s2.y, s2.z, 1.0f },
			{ s3.x, s3.y, s3.z, 1.0f },
			{ s4.x, s4.y, s4.z, 1.0f },
		};

		const glm::vec4 C4 = {
			matrixCofactor(m, 0, 3),
			matrixCofactor(m, 1, 3),
			matrixCofactor(m, 2, 3),
			matrixCofactor(m, 3, 3)
		};

		const float detM = C4[0] + C4[1] + C4[2] + C4[3];

		if (compareSigns(detM, C4[0]) && compareSigns(detM, C4[1]) && compareSigns(detM, C4[2]) && compareSigns(detM, C4[3])) {
			return C4 / detM;
		}


		float dist = 3.4e38f;
		glm::vec4 lambdas = { 0.0f, 0.0f, 0.0f, 0.0f };
		for (int i = 0; i < 4; i++) {
			const int j = (i + 1) % 4;
			const int k = (i + 2) % 4;

			const glm::vec3 face[4]{
				s1, s2, s3, s4
			};

			const glm::vec3 lambdaFace = signedVolume2D(face[i], face[j], face[k]);
			const glm::vec3 point = face[i] * lambdaFace[0] + face[j] * lambdaFace[1] + face[k] * lambdaFace[2];
			if (float lenSq = glm::length2(point); lenSq < dist) {
				dist = lenSq;
				lambdas = {};
				lambdas[i] = lambdaFace[0];
				lambdas[j] = lambdaFace[1];
				lambdas[k] = lambdaFace[2];
			}
		}
		return lambdas;
	}


	static inline bool simplexSignedVolume1D(const PointMinkowski* points, glm::vec3& newDirection, glm::vec2& outLambdas) {
		constexpr float epsilon = 1e-8f;
		glm::vec2 lambdas = signedVolume1D(points[0].diffPoint, points[1].diffPoint);
		glm::vec3 v = {};
		for (int i = 0; i < 2; i++) {
			v += points[i].diffPoint * lambdas[i];
		}
		newDirection = -v;
		outLambdas = lambdas;

		return glm::length2(v) < epsilon;
	}

	static inline bool simplexSignedVolume2D(const PointMinkowski* points, glm::vec3& newDirection, glm::vec3& outLambdas) {
		constexpr float epsilon = 1e-8f;
		glm::vec3 lambdas = signedVolume2D(points[0].diffPoint, points[1].diffPoint, points[2].diffPoint);
		glm::vec3 v = {};
		for (int i = 0; i < 3; i++) {
			v += points[i].diffPoint * lambdas[i];
		}
		newDirection = -v;
		outLambdas = lambdas;

		return glm::length2(v) < epsilon;
	}

	static inline bool simplexSignedVolume3D(const PointMinkowski* points, glm::vec3& newDirection, glm::vec4& outLambdas) {
		constexpr float epsilon = 1e-8f;
		glm::vec4 lambdas = signedVolume3D(points[0].diffPoint, points[1].diffPoint, points[2].diffPoint, points[3].diffPoint);
		glm::vec3 v = {};
		for (int i = 0; i < 4; i++) {
			v += points[i].diffPoint * lambdas[i];
		}
		newDirection = -v;
		outLambdas = lambdas;

		return glm::length2(v) < epsilon;
	}

	static inline bool simplexContainsPoint(const PointMinkowski simplexPoints[4], const PointMinkowski& newPoint) {
		constexpr float limit = 1e-12f;

		for (int i = 0; i < 4; i++) {
			glm::vec3 delta = simplexPoints[i].diffPoint - newPoint.diffPoint;
			if (glm::length2(delta) < limit) {
				return true;
			}
		}
		return false;
	}

	static inline void sortValids(PointMinkowski* simplexPoints, glm::vec4& lambdas) {
		bool valids[4]{};
		for (int i = 0; i < 4; i++) {
			valids[i] = lambdas[i] != 0.0f;
		}

		glm::vec4 validLambdas{};
		int validCount = 0;
		PointMinkowski validPoints[4]{};
		for (int i = 0; i < 4; i++) {
			if (valids[i]) {
				validPoints[validCount] = simplexPoints[i];
				validLambdas[validCount] = lambdas[i];
				validCount++;
			}
		}

		for (int i = 0; i < 4; i++) {
			simplexPoints[i] = validPoints[i];
			lambdas[i] = validLambdas[i];
		}
	}

	static inline int numValidLambdas(glm::vec4 lambdas) {
		int num = 0;
		for (int i = 0; i < 4; i++) {
			if (lambdas[i] != 0.0f) {
				num++;
			}
		}
		return num;
	}

	static inline glm::vec3 barycentricCoords(glm::vec3 s1, glm::vec3 s2, glm::vec3 s3, glm::vec3 point) {
		s1 -= point;
		s2 -= point;
		s3 -= point;

		const glm::vec3 normal = glm::cross(s2 - s1, s3 - s1);
		const glm::vec3 p0 = normal * glm::dot(s1, normal) / glm::length2(normal);

		int index = 0;
		float areaMax = 0.0f;
		for (int i = 0; i < 3; i++) {
			const int j = (i + 1) % 3;
			const int k = (i + 2) % 3;

			const glm::vec2 a = { s1[j], s1[k] };
			const glm::vec2 b = { s2[j], s2[k] };
			const glm::vec2 c = { s3[j], s3[k] };

			const glm::vec2 ab = b - a;
			const glm::vec2 ac = c - a;

			const float area = ab.x * ac.y - ab.y * ac.x;
			if (area * area > areaMax * areaMax) {
				areaMax = area;
				index = i;
			}
		}

		const int x = (index + 1) % 3;
		const int y = (index + 2) % 3;

		const glm::vec2 s[3]{
			{ s1[x], s1[y] },
			{ s2[x], s2[y] },
			{ s3[x], s3[y] }
		};
		const glm::vec2 p = { p0[x], p0[y] };

		glm::vec3 areas{};
		for (int i = 0; i < 3; i++) {
			const int j = (i + 1) % 3;
			const int k = (i + 2) % 3;

			const glm::vec2 a = p;
			const glm::vec2 b = s[j];
			const glm::vec2 c = s[k];
			const glm::vec2 ab = b - a;
			const glm::vec2 ac = c - a;

			areas[i] = ab.x * ac.y - ab.y * ac.x;
		}

		glm::vec3 lambdas = areas / areaMax;
		if (lambdas * glm::vec3(0.0f) != glm::vec3(0.0f)) {
			lambdas = { 1.0f, 0.0f, 0.0f };
		}
		return lambdas;
	}

	static inline glm::vec3 normalDirection(Triangle triangle, const std::vector<PointMinkowski>& points) {
		const glm::vec3 a = points[triangle.a].diffPoint;
		const glm::vec3 b = points[triangle.b].diffPoint;
		const glm::vec3 c = points[triangle.c].diffPoint;
		 
		const glm::vec3 ab = b - a;
		const glm::vec3 ac = c - a;
		return glm::normalize(glm::cross(ab, ac));
	}

	static inline float signedDistanceToTriangle(Triangle triangle, glm::vec3 point, const std::vector<PointMinkowski>& points) {
		const glm::vec3 normal = normalDirection(triangle, points);
		const glm::vec3 a = points[triangle.a].diffPoint;
		const glm::vec3 a2pt = point - a;
		return glm::dot(normal, a2pt);
	}
	
	static inline int closestTriangle(const std::vector<Triangle> triangles, const std::vector<PointMinkowski>& points) {
		float minDist2 = 3.4e38f;
		int index = -1;
		for (int i = 0; i < triangles.size(); i++) {
			const Triangle triangle = triangles[i];
			const float dist = signedDistanceToTriangle(triangle, glm::vec3(0.0f), points);
			const float dist2 = dist * dist;
			if (dist2 < minDist2) {
				index = i;
				minDist2 = dist2;
			}
		}
		return index;
	}

	static inline bool containsPoint(glm::vec3 w, const std::vector<Triangle>& triangles, const std::vector<PointMinkowski>& points) {
		constexpr float epsilon = 1e-6f;
		glm::vec3 delta;
		
		for (int i = 0; i < triangles.size(); i++) {
			const Triangle triangle = triangles[i];
			delta = w - points[triangle.a].diffPoint;
			if (glm::length2(delta) < epsilon) {
				return true;
			}
			delta = w - points[triangle.b].diffPoint;
			if (glm::length2(delta) < epsilon) {
				return true;
			}
			delta = w - points[triangle.c].diffPoint;
			if (glm::length2(delta) < epsilon) {
				return true;
			}
		}
		return false;
	}
	
	static inline int eraseTrianglesFacingPoint(glm::vec3 point, std::vector<Triangle>& triangles, const std::vector<PointMinkowski>& points) {
		int numRemoved = 0;
		for (int i = 0; i < triangles.size(); i++) {
			const Triangle triangle = triangles[i];
			const float dist = signedDistanceToTriangle(triangle, point, points);
			if (dist > 0.0f) {
				triangles.erase(triangles.begin() + i);
				i--;
				numRemoved++;
			}
		}
		return numRemoved;
	}

	static inline void findDanglingEdges(std::vector<Edge>& danglingEdges, const std::vector<Triangle>& triangles) {
		danglingEdges.clear();

		for (int i = 0; i < triangles.size(); i++) {
			const Triangle triangle = triangles[i];

			Edge edges[3]{
				{triangle.a, triangle.b},
				{triangle.b, triangle.c},
				{triangle.c, triangle.a},
			};
			int counts[3]{ 0, 0,0 };

			for (int j = 0; j < triangles.size(); j++) {
				if (j == i) {
					continue;
				}

				const Triangle triangle2 = triangles[j];

				Edge edges2[3]{
					{triangle2.a, triangle2.b},
					{triangle2.b, triangle2.c},
					{triangle2.c, triangle2.a},
				};

				for (int k = 0; k < 3; k++) {
					if (edges[k] == edges2[0]) {
						counts[k]++;
					}
					if (edges[k] == edges2[1]) {
						counts[k]++;
					}
					if (edges[k] == edges2[2]) {
						counts[k]++;
					}
				}
			}

			for (int j = 0; j < 3; j++) {
				if (counts[j] == 0) {
					danglingEdges.push_back(edges[j]);
				}
			}
		}
	}
}