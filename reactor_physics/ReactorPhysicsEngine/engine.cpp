#include "engine.h"
#include <ReactorPhysicsEngine/colliders/abstract.h>
#include <ReactorPhysicsEngine/colliders/convex_hull.h>

namespace RPhys {
	PhysicsEngine::PhysicsEngine() {
	}
	size_t PhysicsEngine::addActor(const PhysicsActor& actor) {
		physicsActorList.push_back(actor);
		return physicsActorList.size() - 1;
	}
	void PhysicsEngine::destroyActor(size_t actorIndex) {
		physicsActorList.erase(physicsActorList.begin() + actorIndex);
	}
	void PhysicsEngine::destroyAllActors() {
		physicsActorList.clear();
	}


	static void resolveContact(CollisionData intersection) {
		PhysicsActor& actorA = *intersection.info.actorA;
		PhysicsActor& actorB = *intersection.info.actorB;

		const glm::vec3 N = intersection.result.direction;
		const float invMassSum = actorA.getInvMass() + actorB.getInvMass();

		const glm::mat3 invInertiaA = actorA.getInvInertiaTensorWorld();
		const glm::mat3 invInertiaB = actorB.getInvInertiaTensorWorld();
		const glm::vec3 ra = intersection.result.pointWorldOnA - actorA.getCenterOfMassWorld();
		const glm::vec3 rb = intersection.result.pointWorldOnB - actorB.getCenterOfMassWorld();

		const glm::vec3 angularA = glm::cross(invInertiaA * glm::cross(ra, N), ra);
		const glm::vec3 angularB = glm::cross(invInertiaB * glm::cross(rb, N), rb);
		const float angularFactor = glm::dot(angularA + angularB, N);

		const glm::vec3 netVelA = actorA.getLinearVelocity() + glm::cross(actorA.getAngularVelocity(), ra);
		const glm::vec3 netVelB = actorB.getLinearVelocity() + glm::cross(actorB.getAngularVelocity(), rb);
		const glm::vec3 velAB = netVelA - netVelB;
		const float velocityMagnitude = glm::dot(velAB, N);

		const glm::vec3 impulseRaw = N * velocityMagnitude / (invMassSum + angularFactor);

		actorA.addForceAtPoint(-impulseRaw * (1.0f + actorA.getMaterial().elasticity), intersection.result.pointWorldOnA);
		actorB.addForceAtPoint(impulseRaw * (1.0f + actorB.getMaterial().elasticity), intersection.result.pointWorldOnB);

		const glm::vec3 velocityNormal = N * velocityMagnitude;
		const glm::vec3 velocityTangent = velAB - velocityNormal;
		const glm::vec3 relativeVelTan = velocityTangent != glm::vec3(0.0f) ? glm::normalize(velocityTangent) : glm::vec3(0.0f);

		const glm::vec3 inertiaA = glm::cross(invInertiaA * glm::cross(ra, relativeVelTan), ra);
		const glm::vec3 inertiaB = glm::cross(invInertiaB * glm::cross(rb, relativeVelTan), rb);
		const float invInertia = glm::dot(inertiaA + inertiaB, relativeVelTan);

		const float reducedMass = 1.0f / (invMassSum + invInertia);
		const float dotFriction = actorA.getMaterial().friction * actorB.getMaterial().friction;
		const glm::vec3 frictionForce = velocityTangent * reducedMass * dotFriction;

		actorA.addForceAtPoint(-frictionForce, intersection.result.pointWorldOnA);
		actorB.addForceAtPoint(frictionForce, intersection.result.pointWorldOnB);

		const float tA = actorA.getInvMass() / invMassSum;
		const float tB = actorB.getInvMass() / invMassSum;

		const glm::vec3 ds = N * intersection.result.length;

		actorA.setPosition(actorA.getPosition() + ds * tA);
		actorB.setPosition(actorB.getPosition() - ds * tB);
	}

	void PhysicsEngine::registerCollisions(float dt) {
		for (int i = 0; i < physicsActorList.size(); i++) {
			for (int j = i + 1; j < physicsActorList.size(); j++) {
 				PhysicsActor& actorA = physicsActorList.at(i);
				PhysicsActor& actorB = physicsActorList.at(j);

				if (actorA.getMobility() == PhysicsMobility::Static && actorB.getMobility() == PhysicsMobility::Static) continue;

				IntersectionInfo iInfo{};
				iInfo.actorA = &actorA;
				iInfo.actorB = &actorB;

				CollisionData intersection = actorA.getCollider().collide(iInfo, actorB.getCollider(), dt);
				if (intersection.result.intersects) {
					RPHYS_DEBUG_LOG("A colission was detected between actors " << i << " and " << j << ", dir ("<< intersection.result.direction.x <<","<< intersection.result.direction.z <<","<<intersection.result.direction.y<<")distance " << intersection.result.length);
					resolveContact(intersection);
				}
			}
		}
	}
	void PhysicsEngine::simulate(float dt) {
		for (uint32_t i = 0; i < physicsActorList.size(); i++) {
			PhysicsActor& actor = physicsActorList.at(i);
			if (actor.getMobility() == PhysicsMobility::Static) continue;

			actor.setLinearVelocity(actor.getLinearVelocity() += universeData.gravity * dt);
		}

		registerCollisions(dt);
		for (uint32_t i = 0; i < physicsActorList.size(); i++) {
			PhysicsActor& actor = physicsActorList.at(i);
			if (actor.getMobility() == PhysicsMobility::Static) continue;
			actor.update(dt);
		}
	}
}