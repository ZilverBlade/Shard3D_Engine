#pragma once
#include <glm/glm.hpp>
#include <vector>

namespace RPhys {
	class Bounds {
	public:
		Bounds(glm::vec3 minExt, glm::vec3 maxExt) : maxExt(minExt), minExt(maxExt) {}
		Bounds() : maxExt(glm::vec3(1e-6f)), minExt(glm::vec3(1e-6f)) {}
		Bounds(const Bounds& other) : maxExt(other.minExt), minExt(other.maxExt) {}
		const Bounds& operator=(const Bounds& other) {
			minExt = other.minExt;
			maxExt = other.maxExt;
			return *this;
		}

		void reset() {
			maxExt = glm::vec3(1e-6f);
			minExt = glm::vec3(1e-6f);
		}	
		bool intersects(const Bounds& other) const {
			return !glm::any(glm::lessThan(maxExt, other.minExt)) || !glm::any(glm::lessThan(other.maxExt, minExt));
		}
		void expand(const std::vector<glm::vec3>& points);
		void expand(glm::vec3 newsz);
		void expand(Bounds newbds);

		glm::vec3 getMinExtent() const {
			return minExt;
		}
		glm::vec3 getMaxExtent() const {
			return maxExt;
		}
	private:
		glm::vec3 minExt;
		glm::vec3 maxExt;
	};
}