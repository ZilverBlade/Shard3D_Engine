#pragma once
#include <glm/glm.hpp>
#include <glm/gtx/norm.hpp>
#include <vector>
#include <array>
#include <ReactorPhysicsEngine/bounds.h>
namespace RPhys {

	struct Triangle {
		Triangle() = default;
		uint32_t a, b, c;
	};
	struct Edge {
		Edge() = default;
		uint32_t a, b;
		
		bool operator==(const Edge& other) const {
			return (a == other.a) && (b == other.b) || (a == other.b) && (b == other.a);
		}
	};

	struct ConvexHull {
		std::vector<glm::vec3> points;
		std::vector<Triangle> triangles;
	};

	static inline int findPointIndexFurthestInDirection(const std::vector<glm::vec3>& points, glm::vec3 direction) {
		int maxid = 0;
		float maxDist = glm::dot(points[0], direction);
		for (int i = 1; i < points.size(); i++) {
			float dist = glm::dot(points[i], direction);
			if (dist > maxDist) {
				maxDist = dist;
				maxid = i;
			}
		}
		return maxid;
	}
	static inline glm::vec3 findPointFurthestInDirection(const std::vector<glm::vec3>& points, glm::vec3 direction) {
		return points[findPointIndexFurthestInDirection(points, direction)];
	}
	static inline float pointDistanceFromLine(glm::vec3 a, glm::vec3 b, glm::vec3 point) {
		const glm::vec3 ab = glm::normalize(a - b);
		const glm::vec3 ray = point - a;
		const glm::vec3 proj = ab * glm::dot(ray, ab);
		const glm::vec3 perp = ray - proj;
		return glm::length(perp);
	}

	static inline glm::vec3 findPointFurthestFromLine(const std::vector<glm::vec3>& points, glm::vec3 ptA, glm::vec3 ptB) {
		int maxid = 0;
		float maxDist = pointDistanceFromLine(ptA, ptB, points[0]);
		for (int i = 1; i < points.size(); i++) {
			float dist = pointDistanceFromLine(ptA, ptB, points[i]);
			if (dist > maxDist) {
				maxDist = dist;
				maxid = i;
			}
		}
		return points[maxid];
	}

	static inline float pointDistanceFromTriangle(glm::vec3 a, glm::vec3 b, glm::vec3 c, glm::vec3 point) {
		const glm::vec3 ab = b - a; 
		const glm::vec3 ac = c - a; 
		const glm::vec3 normal = glm::normalize(glm::cross(ab, ac));
		const glm::vec3 ray = point - a;
		return glm::dot(ray, normal);
	}

	static inline glm::vec3 findPointFurthestFromTriangle(const std::vector<glm::vec3>& points, glm::vec3 ptA, glm::vec3 ptB, glm::vec3 ptC) {
		int maxid = 0;
		float maxDist = pointDistanceFromTriangle(ptA, ptB, ptC, points[0]);
		for (int i = 1; i < points.size(); i++) {
			float dist = pointDistanceFromTriangle(ptA, ptB, ptC, points[i]);
			if (dist * dist > maxDist * maxDist) {
				maxDist = dist;
				maxid = i;
			}
		}
		return points[maxid];
	}

	static inline ConvexHull buildTetrahedron(const std::vector<glm::vec3>& points) {
		ConvexHull tetrahedron;
		tetrahedron.points.resize(4);
		tetrahedron.triangles.resize(4);

		tetrahedron.points[0] = findPointFurthestInDirection(points, glm::vec3(1.0f, 0.0f, 0.0f));
		tetrahedron.points[1] = findPointFurthestInDirection(points, -tetrahedron.points[0]);
		tetrahedron.points[2] = findPointFurthestFromLine(points, tetrahedron.points[0], tetrahedron.points[1]);
		tetrahedron.points[3] = findPointFurthestFromTriangle(points, tetrahedron.points[0], tetrahedron.points[1], tetrahedron.points[2]);

		const float dist = pointDistanceFromTriangle(tetrahedron.points[0], tetrahedron.points[1], tetrahedron.points[2], tetrahedron.points[3]);
		if (dist > 0.0f) {
			std::swap(tetrahedron.points[0], tetrahedron.points[1]);
		}

		tetrahedron.triangles[0] = { 0, 1, 2 };
		tetrahedron.triangles[1] = { 0, 2, 3 };
		tetrahedron.triangles[2] = { 2, 1, 3 };
		tetrahedron.triangles[3] = { 1, 0, 3 };

		return tetrahedron;
	}

	static inline void eraseInternalPoints(const ConvexHull& hull, std::vector<glm::vec3>& pointToCheck) {
		for (int i = 0; i < pointToCheck.size(); i++) {
			const glm::vec3 point = pointToCheck[i];

			bool external = false;
			for (Triangle triangle : hull.triangles) {
				const glm::vec3 a = hull.points[triangle.a];
				const glm::vec3 b = hull.points[triangle.b];
				const glm::vec3 c = hull.points[triangle.c];
				const float dist = pointDistanceFromTriangle(a, b, c, point);
				if (dist > 0.0f) {
					external = true;
					break;
				}
			}

			if (!external) {
				pointToCheck.erase(pointToCheck.begin() + i);
				i--;
			}
		}
		for (int i = 0; i < pointToCheck.size(); i++) {
			const glm::vec3 point = pointToCheck[i];

			bool tooClose = false;
			for (glm::vec3 hpoint : hull.points) {
				glm::vec3 ray = hpoint - point;
				if (glm::length2(ray) < 0.0001f) {
					tooClose = true;
					break;
				}
			}

			if (tooClose) {
				pointToCheck.erase(pointToCheck.begin() + i);
				i--;
			}
		}
	}

	static inline bool isEdgeUnique(const std::vector<Triangle> triangles, const std::vector<uint32_t>& facingTriangles, uint32_t ignoreTriangle, Edge edge) {
		for (uint32_t triangleIndex : facingTriangles) {
			if (triangleIndex == ignoreTriangle) {
				continue;
			}

			const Triangle triangle = triangles[triangleIndex];

			const Edge edges[3]{
				{ triangle.a, triangle.b },
				{ triangle.b, triangle.c },
				{ triangle.c, triangle.a }
			};

			for (int i = 0; i < 3; i++) {
				if (edges[i] == edge) {
					return false;
				}
			}
		}

		return true;
	}

	static inline bool isPointExternal(const ConvexHull& hull, glm::vec3 point) {
		bool isExternal = false;
		for (Triangle triangle : hull.triangles) {
			const glm::vec3 a = hull.points[triangle.a];
			const glm::vec3 b = hull.points[triangle.b];
			const glm::vec3 c = hull.points[triangle.c];


			const float dist = pointDistanceFromTriangle(a, b, c, point);
			if (dist > 0.0f) {
				isExternal = true;
				break;
			}
		}

		return isExternal;
	}

	static inline void addPointToConvexHull(ConvexHull& hull, glm::vec3 point) {
		std::vector<uint32_t> facing;
		for (int i = hull.triangles.size(); i > 0; i--) {
			const uint32_t index = i - 1;
			const Triangle triangle = hull.triangles[index];

			const glm::vec3 a = hull.points[triangle.a];
			const glm::vec3 b = hull.points[triangle.b];
			const glm::vec3 c = hull.points[triangle.c];

			const float dist = pointDistanceFromTriangle(a, b, c, point);
			if (dist > 0.0f) {
				facing.emplace_back(index);
			}
		}

		std::vector<Edge> uniqueEdges;
		for (uint32_t triIndex : facing) {
			Triangle triangle = hull.triangles[triIndex];
			const Edge edges[3]{
				{ triangle.a, triangle.b },
				{ triangle.b, triangle.c },
				{ triangle.c, triangle.a }
			};

			for (int i = 0; i < 3; i++) {
				if (isEdgeUnique(hull.triangles, facing, triIndex, edges[i])) {
					uniqueEdges.emplace_back(edges[i]);
				}
			}
		}

		for (uint32_t eraseTri : facing) {
			hull.triangles.erase(hull.triangles.begin() + eraseTri);
		}

		hull.points.emplace_back(point);
		const uint32_t newIndex = hull.points.size() - 1;

		for (Edge edge : uniqueEdges) {
			hull.triangles.push_back({ edge.a, edge.b, newIndex });
		}
	}

	static inline void eraseUnusedVertices(ConvexHull& hull) {
		for (int i = 0; i < hull.points.size(); i++) {
			bool used = false;
			for (Triangle triangle : hull.triangles) {
				if (i == triangle.a || i == triangle.b || i == triangle.c) {
					used = true;
					break;
				}
			}
			if (used) {
				continue;
			}

			for (Triangle& triangle : hull.triangles) {
				if (triangle.a > i ) {
					triangle.a--;
				}
				if (triangle.b > i) {
					triangle.b--;
				}
				if (triangle.c > i) {
					triangle.c--;
				}
			}
			
			hull.points.erase(hull.points.begin() + i);
		}
	}

	static inline void expandConvexHull(ConvexHull& hull, const std::vector<glm::vec3>& points) {
		std::vector<glm::vec3> externalVerts;
		std::copy(points.begin(), points.end(), std::back_inserter(externalVerts));

		eraseInternalPoints(hull, externalVerts);

		while (externalVerts.size() != 0) {
			uint32_t index = findPointIndexFurthestInDirection(externalVerts, externalVerts[0]);
			
			glm::vec3 point = externalVerts[index];
			externalVerts.erase(externalVerts.begin() + index);
			addPointToConvexHull(hull, point);
			eraseInternalPoints(hull, externalVerts);
		}
		eraseUnusedVertices(hull);
	}

	static inline ConvexHull buildConvexHull(const std::vector<glm::vec3> vertices) {
		assert(vertices.size() >= 4 && "Convex hull must at least have 4 points!!");

		ConvexHull hull = buildTetrahedron(vertices);
		expandConvexHull(hull, vertices);

		return hull;
	}


	static inline glm::vec3 calculateCenterOfMass(const ConvexHull& hull, uint32_t samples) {
		Bounds bounds;
		bounds.expand(hull.points);

		glm::vec3 centerOfMass{};
		glm::vec3 delta = (bounds.getMaxExtent() - bounds.getMinExtent()) / static_cast<float>(samples);
		uint32_t effectiveSamples = 0;

		for (float x = bounds.getMinExtent().x; x < bounds.getMaxExtent().x; x += delta.x) {
			for (float y = bounds.getMinExtent().y; y < bounds.getMaxExtent().y; y += delta.y) {
				for (float z = bounds.getMinExtent().z; z < bounds.getMaxExtent().z; z += delta.z) {
					glm::vec3 point = { x, y, z };
					if (isPointExternal(hull, point)) {
						continue;
					}
					centerOfMass += point;
					effectiveSamples++;
				}
			}
		}

		return centerOfMass / static_cast<float>(effectiveSamples);
	}


	static inline glm::mat3 calculateInertiaTensor(const ConvexHull& hull, glm::vec3 centerOfMass, uint32_t samples) {
		Bounds bounds;
		bounds.expand(hull.points);

		glm::mat3 tensor{0.0f};

		glm::vec3 delta = (bounds.getMaxExtent() - bounds.getMinExtent()) / static_cast<float>(samples);
		uint32_t effectiveSamples = 0;

		for (float x = bounds.getMinExtent().x; x < bounds.getMaxExtent().x; x += delta.x) {
			for (float y = bounds.getMinExtent().y; y < bounds.getMaxExtent().y; y += delta.y) {
				for (float z = bounds.getMinExtent().z; z < bounds.getMaxExtent().z; z += delta.z) {
					glm::vec3 point = { x, y, z };
					if (isPointExternal(hull, point)) {
						continue;
					}
					point -= centerOfMass;
					tensor[0][0] += point.y * point.y + point.z * point.z;
					tensor[1][1] += point.x * point.x + point.z * point.z;
					tensor[2][2] += point.x * point.x + point.y * point.y;

					tensor[1][0] -= point.x * point.y;
					tensor[2][0] -= point.x * point.z;
					tensor[2][1] -= point.y * point.z;

					tensor[0][1] -= point.x * point.y;
					tensor[0][2] -= point.x * point.z;
					tensor[1][2] -= point.y * point.z;
					effectiveSamples++;
				}
			}
		}

		return tensor / static_cast<float>(effectiveSamples);
	}
}