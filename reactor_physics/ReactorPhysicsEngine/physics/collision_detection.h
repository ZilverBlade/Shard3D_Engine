#pragma once
#include <glm/glm.hpp>
#include <ReactorPhysicsEngine/intersection_data.h>

namespace RPhys {
	namespace Intersection {
		CollisionData sphereSphere(glm::vec3 sphereCenterA, float sphereRadiusA, glm::vec3 sphereCenterB, float sphereRadiusB);
		// for use if one of the actors has a convex hull collider
		CollisionData convexActor(const PhysicsActor& actorA, const PhysicsActor& actorB);
		CollisionData planeSphere(glm::vec4 cartesianPlane, glm::vec3 sphereCenter, float sphereRadius);
	}
	namespace Collision {
		CollisionResult sphereSphere(glm::vec3 sphereCenterA, float sphereRadiusA, glm::vec3 sphereCenterB, float sphereRadiusB, float dt);
		// for use if one of the actors has a convex hull collider
		CollisionResult convexActor(PhysicsActor& actorA, PhysicsActor& actorB, float dt);
		CollisionResult planeSphere(glm::vec4 cartesianPlane, glm::vec3 sphereCenter, float sphereRadius, float dt);
	}
}