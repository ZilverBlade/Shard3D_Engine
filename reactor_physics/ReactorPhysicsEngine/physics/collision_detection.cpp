#pragma once
#include "collision_detection.h"
#include <ReactorPhysicsEngine/physics_actor.h>
#include <ReactorPhysicsEngine/colliders/convex_hull.h>
#include <ReactorPhysicsEngine/colliders/sphere.h>
#include <ReactorPhysicsEngine/signed_shapes.h>

#define CCD_STATIC_DEFINE
#include <ccd/ccd.h>
#include <ccd/quat.h>

void supportCCD(const void* actor_, const ccd_vec3_t* dir_, ccd_vec3_t* vec) {
	RPhys::PhysicsActor* actor = (RPhys::PhysicsActor*)actor_;
	*vec = *(ccd_vec3_t*)&actor->getCollider().getSupport(glm::normalize(*(glm::vec3*)dir_), actor->getPosition(), actor->getOrientation(), 0.0f);
}
void centerCCD(const void* actor_, ccd_vec3_t* center) {
	RPhys::PhysicsActor* actor = (RPhys::PhysicsActor*)actor_;
	*center = *(ccd_vec3_t*)&actor->getPosition();
}
void firstDirCCD(const void* obj1, const void* obj2,
								 ccd_vec3_t* dir) {
	*dir = (ccd_vec3_t&)glm::vec3(1.0f, 1.0f, 0.0f);
}
namespace RPhys {


	static inline PointMinkowski getActorSupport(const PhysicsActor& actorA, const PhysicsActor& actorB, glm::vec3 direction, float bias) {
		PointMinkowski point{};

		point.ptA = actorA.getCollider().getSupport(glm::normalize(direction), actorA.getPosition(), actorA.getOrientation(), bias);

		point.ptB = actorB.getCollider().getSupport(-glm::normalize(direction), actorB.getPosition(), actorB.getOrientation(), bias);

		point.diffPoint = point.ptA - point.ptB;
		return point;
	}

	static inline float EPAExpand(const PhysicsActor& actorA, const PhysicsActor& actorB, float bias, const PointMinkowski simplexPoints[4], glm::vec3& ptOnA, glm::vec3& ptOnB) {
		std::vector<PointMinkowski> points;
		std::vector<Triangle> triangles;
		std::vector<Edge> danglingEdges;

		glm::vec3 center{};
		for (int i = 0; i < 4; i++) {
			points.push_back(simplexPoints[i]);
			center += simplexPoints[i].diffPoint;
		}
		center *= 0.25f;

		for (int i = 0; i < 4; i++) {
			const int j = (i + 1) % 4;
			const int k = (i + 2) % 4;
			const int h = (i + 3) % 4;
			Triangle triangle = { i, j, k };

			float dist = signedDistanceToTriangle(triangle, points[h].diffPoint, points);
			if (dist > 0.0f) {
				std::swap(triangle.a, triangle.b);
			}
			triangles.push_back(triangle);
		}

		while (true) {
			const int index = closestTriangle(triangles, points);
			Triangle triangle = triangles[index];
			const glm::vec3 normal = normalDirection(triangle, points);
			const PointMinkowski newPoint = getActorSupport(actorA, actorB, normal, bias);
			if (containsPoint(newPoint.diffPoint, triangles, points)) {
				break;
			}

			float dist = signedDistanceToTriangle(triangle, newPoint.diffPoint, points);
			if (dist <= 0.0f ) {
				break;
			}

			const int newIndex = points.size();
			points.push_back(newPoint);

			int numRemoved = eraseTrianglesFacingPoint(newPoint.diffPoint, triangles, points);
			if (numRemoved == 0) {
				break;
			}

			danglingEdges.clear();
			findDanglingEdges(danglingEdges, triangles);
			if (danglingEdges.size() == 0) {
				break;
			}

			for (Edge edge : danglingEdges) {
				Triangle tri{ newIndex, edge.b, edge.a };
				float dist = signedDistanceToTriangle(tri, center, points);
				if (dist > 0.0f) {
					std::swap(triangle.b, triangle.c);
				}

				triangles.push_back(tri);
			}
		}

		const int index = closestTriangle(triangles, points);
		const Triangle triangle = triangles[index];
		glm::vec3 ptA_w = points[triangle.a].diffPoint;
		glm::vec3 ptB_w = points[triangle.b].diffPoint;
		glm::vec3 ptC_w = points[triangle.c].diffPoint;
		glm::vec3 lambdas = barycentricCoords(ptA_w, ptB_w, ptC_w, {});

		glm::vec3 ptA_a = points[triangle.a].ptA;
		glm::vec3 ptB_a = points[triangle.b].ptA;
		glm::vec3 ptC_a = points[triangle.c].ptA;
		ptOnA = ptA_a * lambdas[0] + ptB_a * lambdas[1] + ptC_a * lambdas[2];

		glm::vec3 ptA_b = points[triangle.a].ptB;
		glm::vec3 ptB_b = points[triangle.b].ptB;
		glm::vec3 ptC_b = points[triangle.c].ptB;
		ptOnB = ptA_b * lambdas[0] + ptB_b * lambdas[1] + ptC_b * lambdas[2];

		return glm::length(ptOnB - ptOnA);
	}

	static inline bool GJKIntersection(const PhysicsActor& actorA, const PhysicsActor& actorB, float bias, glm::vec3& ptOnA, glm::vec3& ptOnB) {
		const glm::vec3 origin = glm::vec3(0.0f);

		int numPoints = 1;
		PointMinkowski simplexPoints[4];


		simplexPoints[0] = getActorSupport(actorA, actorB, glm::vec3{ 1.0f, 1.0f, 1.0f }, 0.0f);
		float closestDist = 3.4e38f;
		bool containsOrigin = false;

		glm::vec3 newDir = -simplexPoints[0].diffPoint;

		do {
			const PointMinkowski newPoint = getActorSupport(actorA, actorB, newDir, 0.0f);
			if (newPoint.diffPoint * glm::vec3(0.0f) != glm::vec3(0.0f)) {
				break; // something went wrong
			}
			if (simplexContainsPoint(simplexPoints, newPoint)) {
				break;
			}
			simplexPoints[numPoints] = newPoint;
			numPoints++;

			const float dotdot = glm::dot(newDir, newPoint.diffPoint - origin);
			if (dotdot < 0.0f) {
				break;
			}

			glm::vec4 lambdas{};
			switch (numPoints) {
			case (2):
				containsOrigin = simplexSignedVolume1D(simplexPoints, newDir, reinterpret_cast<glm::vec2&>(lambdas));
				break;
			case (3):
				containsOrigin = simplexSignedVolume2D(simplexPoints, newDir, reinterpret_cast<glm::vec3&>(lambdas));
				break;
			case (4):
				containsOrigin = simplexSignedVolume3D(simplexPoints, newDir, reinterpret_cast<glm::vec4&>(lambdas));
				break;
			}
			if (containsOrigin) break;


			const float dist = glm::length2(newDir);
			if (dist >= closestDist) {
				break;
			}
			closestDist = dist;

			sortValids(simplexPoints, lambdas);
			numPoints = numValidLambdas(lambdas);
			containsOrigin = numPoints == 4;
		} while (!containsOrigin);

		if (!containsOrigin) {
			return false;
		}

		if (numPoints == 1) {
			glm::vec3 searchDir = glm::normalize(-simplexPoints[0].diffPoint);
			PointMinkowski newPoint = getActorSupport(actorA, actorB, searchDir, 0.0f);
			simplexPoints[numPoints] = newPoint;
			numPoints++;
		}
		if (numPoints == 2) {
			glm::vec3 ab = simplexPoints[1].diffPoint - simplexPoints[0].diffPoint;
			glm::vec3 u, v;
			{
				glm::vec3 n = glm::normalize(ab);
				glm::vec3 w = n.z * n.z > 0.9f * 0.9f ? glm::vec3(1.0f, 0.0f, 0.0f) : glm::vec3(0.0f, 0.0f, 1.0f);
				u = glm::normalize(glm::cross(w, n));
				v = glm::normalize(glm::cross(n, u));
				u = glm::normalize(glm::cross(v, n));
			}
			PointMinkowski newPoint = getActorSupport(actorA, actorB, u, 0.0f);
			simplexPoints[numPoints] = newPoint;
			numPoints++;
		}
		if (numPoints == 3) {
			glm::vec3 ab = simplexPoints[1].diffPoint - simplexPoints[0].diffPoint;
			glm::vec3 ac = simplexPoints[2].diffPoint - simplexPoints[0].diffPoint;
			glm::vec3 normal = glm::cross(ab, ac);
			PointMinkowski newPoint = getActorSupport(actorA, actorB, normal, 0.0f);
			simplexPoints[numPoints] = newPoint;
			numPoints++;
		}

		glm::vec3 average = (simplexPoints[0].diffPoint + simplexPoints[1].diffPoint + simplexPoints[2].diffPoint + simplexPoints[3].diffPoint) * 0.25f;
		for (int i = 0; i < numPoints; i++) {
			PointMinkowski& point = simplexPoints[i];
			glm::vec3 direction = glm::normalize(point.diffPoint - average);
			point.ptA += direction * bias;
			point.ptB -= direction * bias;
			point.diffPoint = point.ptA - point.ptB;
		}

		EPAExpand(actorA, actorB, bias, simplexPoints, ptOnA, ptOnB);
		return true;
	}

	static inline void GJKClosestPoints(const PhysicsActor& actorA, const PhysicsActor& actorB, glm::vec3& ptOnA, glm::vec3& ptOnB) {
		const glm::vec3 origin = { 0.0f, 0.0f, 0.0f };
		float closestDist = 3.4e38f;
		const float bias = 0.0f;

		int numPoints = 1;
		PointMinkowski simplexPoints[4];
		simplexPoints[0] = getActorSupport(actorA, actorB, { 1.0f, 1.0f, 1.0f }, bias);

		glm::vec4 lambdas = { 1.0f, 0.0f, 0.0f, 0.0f };
		glm::vec3 newDir = -simplexPoints[0].diffPoint;
		do {
			const PointMinkowski newPoint = getActorSupport(actorA, actorB, newDir, bias);
			if (simplexContainsPoint(simplexPoints, newPoint)) {
				break;
			}
			simplexPoints[numPoints] = newPoint;
			numPoints++;

			switch (numPoints) {
			case (2):
				simplexSignedVolume1D(simplexPoints, newDir, reinterpret_cast<glm::vec2&>(lambdas));
				break;
			case (3):
				simplexSignedVolume2D(simplexPoints, newDir, reinterpret_cast<glm::vec3&>(lambdas));
				break;
			case (4):
				simplexSignedVolume3D(simplexPoints, newDir, reinterpret_cast<glm::vec4&>(lambdas));
				break;
			}
			sortValids(simplexPoints, lambdas);
			numPoints = numValidLambdas(lambdas);
			const float dist = glm::length2(newDir);
			if (dist >= closestDist) {
				break;
			}
			closestDist = dist;
		} while (numPoints < 4);

		ptOnA = {};
		ptOnB = {};
		for (int i = 0; i < 4; i++) {
			ptOnA += simplexPoints[i].ptA * lambdas[i];
			ptOnB += simplexPoints[i].ptB * lambdas[i];
		}
	}

	namespace Intersection {
		//CollisionResult sphereSphere(glm::vec3 sphereCenterA, float sphereRadiusA, glm::vec3 sphereCenterB, float sphereRadiusB) {
		//	const float radiusDist = sphereRadiusA + sphereRadiusB;
		//	const glm::vec3 dir = sphereCenterB - sphereCenterA;
		//	const glm::vec3 normal = glm::normalize(dir);
		//	const float centerDist = glm::length(dir);
		//	const float dist = centerDist - radiusDist;
		//
		//	const glm::vec3 ptwrldOnA = sphereCenterA + normal * sphereRadiusA;
		//	const glm::vec3 ptwrldOnB = sphereCenterB - normal * sphereRadiusB;
		//
		//	return CollisionResult(centerDist < radiusDist, normal, dist, ptwrldOnA, ptwrldOnB, 0.0f);
		//}
		//CollisionResult planeSphere(glm::vec4 cartesianPlane, glm::vec3 sphereCenter, float sphereRadius) {
		//	const float distFromCenter = glm::abs(glm::dot(glm::vec3(cartesianPlane), sphereCenter) + cartesianPlane.w);
		//	const float dist = distFromCenter - sphereRadius;
		//
		//	return dist < 0.f;
		//}
		//CollisionResult convexActor(const PhysicsActor& actorA, const PhysicsActor& actorB) {
		//	constexpr float bias = 1e-6f;
		//
		//	ccd_t ccd;
		//	CCD_INIT(&ccd);
		//
		//	ccd.first_dir = firstDirCCD;
		//	ccd.max_iterations = 100;
		//	ccd.epa_tolerance = 1e-4f;
		//	ccd.mpr_tolerance = 1e-4f;
		//	ccd.support1 = supportCCD;
		//	ccd.support2 = supportCCD;
		//	ccd.center1 = centerCCD;
		//	ccd.center2 = centerCCD;
		//
		//	int intersect = ccdMPRIntersect(&actorA, &actorB, &ccd);
		//	return intersect;
		//}
	}



	static bool conservativeAdvance(PhysicsActor& actorA, PhysicsActor& actorB, float dt, CollisionData& intersection) {
		*intersection.info.actorA = actorA;
		*intersection.info.actorB = actorB;

		float toi = 0.0f;

		int numIter = 0;

		IntersectionInfo iInfo{};
		iInfo.actorA = &actorA;
		iInfo.actorB = &actorB;
		while (dt > 0.0f) {
			constexpr float bias = 1e-6f;

			ccd_t ccd;
			CCD_INIT(&ccd);

			ccd.first_dir = firstDirCCD;
			ccd.max_iterations = 100;
			ccd.epa_tolerance = 1e-4f;
			ccd.mpr_tolerance = 1e-4f;
			ccd.support1 = supportCCD;
			ccd.support2 = supportCCD;
			ccd.center1 = centerCCD;
			ccd.center2 = centerCCD;

			ccd_real_t depth;
			ccd_vec3_t dir, pos;
			int intersect = ccdMPRPenetration(&actorA, &actorB, &ccd, &depth, &dir, &pos);

			if (intersect == 0) {
				CollisionResult result{};
				glm::vec3 ptOnA{}, ptOnB{};
				ptOnA = (glm::vec3&)pos;
				ptOnB = (glm::vec3&)pos;

				intersection.result = CollisionResult(true, -glm::normalize((glm::vec3&)dir), depth, ptOnA, ptOnB, toi);
				actorA.update(-toi);
				actorB.update(-toi);
				return true;
			} else {
				GJKClosestPoints(actorA, actorB, intersection.result.pointWorldOnA, intersection.result.pointWorldOnB);
			}

			++numIter;
			if (numIter > 10) {
				break;
			}

			glm::vec3 ab = intersection.result.pointWorldOnB - intersection.result.pointWorldOnA;
			ab = glm::normalize(ab);

			glm::vec3 relvel = actorA.getLinearVelocity() - actorB.getLinearVelocity();
			float orthospeed = glm::dot(relvel, ab);

			float angularspeedA = actorA.getCollider().fastestLinearVelocity(actorA.getAngularVelocity(), ab);
			float angularspeedB = actorB.getCollider().fastestLinearVelocity(actorB.getAngularVelocity(), -ab);

			orthospeed *= angularspeedA + angularspeedB;
			if (orthospeed <= 0.0f) {
				break;
			}

			float timeLeft = intersection.result.length / orthospeed;
			if (timeLeft > dt) break;
			dt -= timeLeft;
			toi += timeLeft;
			actorA.update(timeLeft);
			actorB.update(timeLeft);
		}
		actorA.update(-toi);
		actorB.update(-toi);
		return false;
	}

	namespace Collision {
		CollisionResult sphereSphere(glm::vec3 sphereCenterA, float sphereRadiusA, glm::vec3 sphereCenterB, float sphereRadiusB, float dt) {
			const float radiusDist = sphereRadiusA + sphereRadiusB;
			const glm::vec3 dir = sphereCenterB - sphereCenterA;
			const glm::vec3 normal = glm::normalize(dir);
			const float centerDist = glm::length(dir);
			const float dist = centerDist - radiusDist;

			const glm::vec3 ptwrldOnA = sphereCenterA + normal * sphereRadiusA;
			const glm::vec3 ptwrldOnB = sphereCenterB - normal * sphereRadiusB;

			return CollisionResult(centerDist < radiusDist, normal, dist, ptwrldOnA, ptwrldOnB, 0.0f);
		}
		CollisionResult planeSphere(glm::vec4 cartesianPlane, glm::vec3 sphereCenter, float sphereRadius, float dt) {
			const float distFromCenter = glm::abs(glm::dot(glm::vec3(cartesianPlane), sphereCenter) + cartesianPlane.w);
			const float dist = distFromCenter - sphereRadius;

			return CollisionResult(dist < 0.f, glm::vec3(cartesianPlane), dist, sphereCenter - glm::vec3(cartesianPlane) * sphereRadius, sphereCenter - glm::vec3(cartesianPlane) * sphereRadius, 0.0f);
		}
		CollisionResult convexActor(PhysicsActor& actorA, PhysicsActor& actorB, float dt) {
			constexpr float bias = 1e-6f;

			ccd_t ccd;
			CCD_INIT(&ccd);

			ccd.first_dir = firstDirCCD;
			ccd.max_iterations = 100;
			ccd.epa_tolerance = 1e-4f;
			ccd.mpr_tolerance = 1e-4f;
			ccd.support1 = supportCCD;
			ccd.support2 = supportCCD;
			ccd.center1 = centerCCD;
			ccd.center2 = centerCCD;

			ccd_real_t depth;
			ccd_vec3_t dir, pos;
			int intersect = ccdMPRPenetration(&actorA, &actorB, &ccd, &depth, &dir, &pos);

			if (intersect == 0) {
				CollisionResult result{};
				glm::vec3 ptOnA{}, ptOnB{};
				ptOnA = (glm::vec3&)pos;
				ptOnB = (glm::vec3&)pos;
				
				glm::vec3 ab = actorB.getPosition() - actorA.getPosition();
				
				return CollisionResult(true, -glm::normalize((glm::vec3&)dir), depth, ptOnA, ptOnB, 0.0f);
				//RPHYS_DEBUG_LOG("A colission was detected between actors " << (void*)&actorA << " and " << (void*)&actorA << ", distance " << depth);
			} else if (intersect == -1) {

				CollisionResult result{};
				glm::vec3 ptOnA{}, ptOnB{};
				ptOnA = (glm::vec3&)pos;
				ptOnB = (glm::vec3&)pos;

				glm::vec3 ab = actorB.getPosition() - actorA.getPosition();

				CollisionData data{};
				data.info.actorA = &actorA;
				data.info.actorB = &actorB;

				conservativeAdvance(actorA, actorB, dt, data);
				return data.result;
			}
			return CollisionResult();
		}
	}
}