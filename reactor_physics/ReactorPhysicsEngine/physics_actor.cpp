#define GLM_FORCE_RADIANS
#include "physics_actor.h"
namespace RPhys {
	void PhysicsActor::update(float dt) {
		position += linearVelocity * dt;

		const glm::vec3 posCoM = getCenterOfMassWorld();
		const glm::vec3 comToPos = position - posCoM;

		const glm::mat3 matOrient = glm::toMat3(orientation);
		const glm::mat3 inertiaTensor = matOrient * abstractCollider->getIntertiaTensor() * glm::transpose(matOrient);
		const glm::vec3 alpha = glm::inverse(inertiaTensor) * glm::cross(angularVelocity, inertiaTensor * angularVelocity);
		angularVelocity += alpha * dt;

		const glm::vec3 dHalfVector = angularVelocity * dt * 0.5f;
		if (glm::dot(dHalfVector, dHalfVector) > 0.0f) {
			const glm::quat dquat = glm::quat(1.0f, dHalfVector.x, dHalfVector.y, dHalfVector.z);
			orientation = glm::normalize(orientation * dquat);
			position = posCoM + glm::rotate(dquat, comToPos);
			assert(!glm::any(glm::isnan(orientation)));
			assert(!glm::any(glm::isinf(orientation)));
		}
	}
}