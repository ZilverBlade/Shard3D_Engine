#pragma once
#include <ReactorPhysicsEngine/colliders/abstract.h>

namespace RPhys {
	class PlaneCollider : public AbstractCollider {
	public:
		PlaneCollider(glm::vec4 cartesianEq);

		virtual const CollisionData collide(const IntersectionInfo& info, const AbstractCollider& abstractCollider, float dt) const override;
		virtual Bounds getBounds(glm::vec3 pos, glm::quat ori) const;
		virtual Bounds getBounds() const;
		virtual glm::mat3 getIntertiaTensor() const override;
		virtual glm::vec3 getSupport(glm::vec3 direction, glm::vec3 position, glm::quat orientation, float bias) const {
			return {};
		}
		glm::vec4 getPlane() const {
			return cartesianPlane;
		}
		glm::vec3 getNormal() const {
			return cartesianPlane;
		}
		float getDistance() const {
			return cartesianPlane.w;
		}
	private:
		glm::vec4 cartesianPlane;
	};
}