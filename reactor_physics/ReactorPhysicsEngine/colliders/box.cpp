#include "box.h"

namespace RPhys {
	BoxCollider::BoxCollider(glm::vec3 extent, std::optional<glm::vec3> centerOfMassOverride)
		: AbstractCollider(ColliderType::Box), extent(extent) {
		if (centerOfMassOverride.has_value()) {
			centerOfMass = centerOfMassOverride.value();
		} else {
			centerOfMass = glm::vec3(0.0f);
		}
	}

	const CollisionData BoxCollider::collide(const IntersectionInfo& info, const AbstractCollider& collider, float dt) const {
		return { info, Collision::convexActor(*info.actorA, *info.actorB, dt) };
	}

	Bounds BoxCollider::getBounds(glm::vec3 position, glm::quat orientation) const {
		glm::vec3 cmp[]{
			glm::rotate(orientation, { -extent.x, extent.y, extent.z }) + position,
			glm::rotate(orientation, { -extent.x, extent.y, -extent.z }) + position,
			glm::rotate(orientation, { -extent.x, -extent.y, -extent.z }) + position,
			glm::rotate(orientation, { -extent.x, -extent.y, extent.z }) + position,
			glm::rotate(orientation, { extent.x, extent.y, extent.z }) + position,
			glm::rotate(orientation, { extent.x, extent.y, -extent.z }) + position,
			glm::rotate(orientation, { extent.x, -extent.y, -extent.z }) + position,
			glm::rotate(orientation, { extent.x, -extent.y, extent.z }) + position
		};
		
		Bounds bounds;
		for (int i = 0; i < 8; i++) {
			bounds.expand(cmp[i]);
		}
		return bounds;
	}

	Bounds BoxCollider::getBounds() const {
		return Bounds(-extent, extent);
	}

	glm::vec3 BoxCollider::getSupport(glm::vec3 direction, glm::vec3 position, glm::quat orientation, float bias) const {
		const glm::vec3 cmp[]{
			glm::rotate(orientation, { -extent.x, extent.y, extent.z }) + position,
			glm::rotate(orientation, { -extent.x, extent.y, -extent.z }) + position,
			glm::rotate(orientation, { -extent.x, -extent.y, -extent.z }) + position,
			glm::rotate(orientation, { -extent.x, -extent.y, extent.z } ) + position,
			glm::rotate(orientation, { extent.x, extent.y, extent.z } ) + position,
			glm::rotate(orientation, { extent.x, extent.y, -extent.z }) + position,
			glm::rotate(orientation, { extent.x, -extent.y, -extent.z }) + position,
			glm::rotate(orientation, { extent.x, -extent.y, extent.z }) + position
		};

		float maxDist = glm::dot(direction, cmp[0]);
		glm::vec3 maxPt = cmp[0];
		for (int i = 1; i < 8; i++) {
			const float dist = glm::dot(direction, cmp[i]);
			if(dist > maxDist) {
				maxDist = dist;
				maxPt = cmp[i];
			}
		}

		return maxPt + direction * bias;
	}

	float BoxCollider::fastestLinearVelocity(glm::vec3 angularVelocity, glm::vec3 direction) const {
		const glm::vec3 cmp[]{
			{ -extent.x, extent.y, extent.z },
			{ -extent.x, extent.y, -extent.z },
			{ -extent.x, -extent.y, -extent.z },
			{ -extent.x, -extent.y, extent.z },
			{ extent.x, extent.y, extent.z },
			{ extent.x,  extent.y, -extent.z },
			{ extent.x,  -extent.y, -extent.z },
			{ extent.x, -extent.y, extent.z }
		};

		float maxVelocity = {};
		for (int i = 0; i < 8; i++) {
			const glm::vec3 r = cmp[i] - centerOfMass;
			const glm::vec3 linearV = glm::cross(angularVelocity, r);
			const float speed = glm::dot(direction, linearV);
			if (speed > maxVelocity) {
				maxVelocity = speed;
			}
		}
		return maxVelocity;
	}

	glm::mat3 BoxCollider::getIntertiaTensor() const {
		constexpr float oneTwelveth = 1.0f / 12.0f;
		const float dx = extent.x + extent.x;
		const float dy = extent.y + extent.y;
		const float dz = extent.z + extent.z;
		const float w2 = dx * dx;
		const float h2 = dy * dy;
		const float d2 = dz * dz;

		const glm::vec3 nCoM = -centerOfMass;
		const float R2 = glm::length2(centerOfMass);
		return {
			oneTwelveth * (h2 + d2) + R2 - nCoM.x * nCoM.x, nCoM.x * nCoM.y, nCoM.x * nCoM.z,
			nCoM.x * nCoM.y, oneTwelveth * (w2 + d2) + R2 - nCoM.y * nCoM.y, nCoM.y * nCoM.z,
			nCoM.x * nCoM.z, nCoM.y * nCoM.z, oneTwelveth * (w2 + h2) + R2 - nCoM.z * nCoM.z
		};
	}
}