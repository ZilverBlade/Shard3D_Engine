#pragma once
#include <ReactorPhysicsEngine/colliders/abstract.h>
#include <vector>
namespace RPhys {
	class ConvexHullCollider : public AbstractCollider {
	public:
		ConvexHullCollider(const std::vector<glm::vec3>& points, std::optional<glm::vec3> centerOfMassOverride = {});
		ConvexHullCollider(const std::vector<glm::vec3>& points, glm::vec3 centerOfMass, glm::mat3 inertiaTensor);

		virtual const CollisionData collide(const IntersectionInfo& info, const AbstractCollider& collider, float dt) const override;
		virtual Bounds getBounds(glm::vec3 position, glm::quat orientation) const override;
		virtual Bounds getBounds() const override;
		virtual glm::vec3 getSupport(glm::vec3 direction, glm::vec3 position, glm::quat orientation, float bias) const override;
		virtual float fastestLinearVelocity(glm::vec3 angularVelocity, glm::vec3 direction) const override;
		const std::vector<glm::vec3>& getPoints() const {
			return points;
		}

		virtual glm::mat3 getIntertiaTensor() const override;
	private:
		void build();

		std::vector<glm::vec3> points;
		glm::mat3 inertiaTensor;
		Bounds bounds;
	};
}