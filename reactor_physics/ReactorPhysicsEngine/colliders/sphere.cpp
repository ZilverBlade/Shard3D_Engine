#include "sphere.h"
#include <ReactorPhysicsEngine/colliders/plane.h>
#include <ReactorPhysicsEngine/physics_actor.h>
#include <ReactorPhysicsEngine/intersection_data.h>

namespace RPhys {
	SphereCollider::SphereCollider(float radius, std::optional<glm::vec3> centerOfMassOverride)
		: AbstractCollider(ColliderType::Sphere), radius(radius) {
		if (centerOfMassOverride.has_value()) {
			centerOfMass = centerOfMassOverride.value();
		} else {
			centerOfMass = glm::vec3(0.0f);
		}
	}

	const CollisionData SphereCollider::collide(const IntersectionInfo& info, const AbstractCollider& abstractCollider, float dt) const {
	//	if (abstractCollider.getType() == ColliderType::Sphere) {
	//		const SphereCollider& collider = reinterpret_cast<const SphereCollider&>(abstractCollider);
	//		return { info, Intersection::sphereSphere(info.actorA->getPosition(), radius, info.actorB->getPosition(), collider.getRadius()) };
	//	} else if (abstractCollider.getType() == ColliderType::Plane) {
	//		const PlaneCollider& collider = reinterpret_cast<const PlaneCollider&>(abstractCollider);
	//		return { { info.actorB, info.actorA }, Intersection::planeSphere(collider.getPlane(), info.actorA->getPosition(), radius) };
	//	} else {
	//		return { info, Intersection::convexActor(*info.actorA, *info.actorB) };
	//	}

		return { info, Collision::convexActor(*info.actorA, *info.actorB, dt) };
	}
	Bounds SphereCollider::getBounds(glm::vec3 position, glm::quat orientation) const {
		return Bounds(glm::vec3(-radius) + position, glm::vec3(radius) + position);
	}
	Bounds SphereCollider::getBounds() const {
		return Bounds(glm::vec3(-radius), glm::vec3(radius));
	}
	glm::vec3 SphereCollider::getSupport(glm::vec3 direction, glm::vec3 position, glm::quat orientation, float bias) const {
		return position + direction * (radius + bias);
	}
	glm::mat3 SphereCollider::getIntertiaTensor() const {
		const float v = 2.0f * radius * radius / 5.0f;
		glm::vec3 nCoM = -centerOfMass;
		float R2 = glm::length2(centerOfMass);
		return {
			v + R2 - nCoM.x * nCoM.x, nCoM.x * nCoM.y, nCoM.x * nCoM.z,
			nCoM.x * nCoM.y, v + R2 - nCoM.y * nCoM.y, nCoM.y * nCoM.z,
			nCoM.x * nCoM.z, nCoM.y * nCoM.z, v + R2 - nCoM.z * nCoM.z
		};
	}
}