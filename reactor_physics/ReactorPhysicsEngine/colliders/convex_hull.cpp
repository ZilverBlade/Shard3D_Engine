#include "convex_hull.h"
#include <ReactorPhysicsEngine/convex_shape.h>


#ifndef NDEBUG
constexpr int samples = 10;
#else
constexpr int samples = 100;
#endif

namespace RPhys {
	ConvexHullCollider::ConvexHullCollider(const std::vector<glm::vec3>& points, std::optional<glm::vec3> centerOfMassOverride)
		: AbstractCollider(ColliderType::ConvexHull), points(points) {
		build();
		if (centerOfMassOverride.has_value()) {
			centerOfMass = centerOfMassOverride.value();
			ConvexHull hull = buildConvexHull(points);
			inertiaTensor = calculateInertiaTensor(hull, centerOfMass, samples);
		}
	}

	ConvexHullCollider::ConvexHullCollider(const std::vector<glm::vec3>& points_, glm::vec3 centerOfMass_, glm::mat3 inertiaTensor)
		: AbstractCollider(ColliderType::ConvexHull), points(points_), inertiaTensor(inertiaTensor) {
		centerOfMass = centerOfMass_;
		bounds.reset();
		bounds.expand(points);
	}

	void ConvexHullCollider::build() {
		ConvexHull hull = buildConvexHull(points);
		points = hull.points;

		bounds.reset();
		bounds.expand(points);

		centerOfMass = calculateCenterOfMass(hull, samples);
		inertiaTensor = calculateInertiaTensor(hull, centerOfMass, samples);
	}

	const CollisionData ConvexHullCollider::collide(const IntersectionInfo& info, const AbstractCollider& collider, float dt) const {
		return { info, Collision::convexActor(*info.actorA, *info.actorB, dt) };
	}

	Bounds ConvexHullCollider::getBounds(glm::vec3 position, glm::quat orientation) const {
		Bounds bounds;
		for (glm::vec3 point : points) {
			bounds.expand(glm::rotate(orientation, point) + position);
		}
		return bounds;
	}

	Bounds ConvexHullCollider::getBounds() const {
		return Bounds(bounds.getMinExtent(), bounds.getMaxExtent());
	}

	glm::vec3 ConvexHullCollider::getSupport(glm::vec3 direction, glm::vec3 position, glm::quat orientation, float bias) const {
		float maxDist = glm::dot(direction, glm::rotate(orientation, points[0]) + position);
		glm::vec3 maxPt = points[0];
		for (int i = 1; i < points.size(); i++) {
			glm::vec3 point = points[i];
			const glm::vec3 pt = glm::rotate(orientation, point) + position;
			const float dist = glm::dot(direction, pt);
			if(dist > maxDist) {
				maxDist = dist;
				maxPt = pt;
			}
		}

		return maxPt + direction * bias;
	}

	float ConvexHullCollider::fastestLinearVelocity(glm::vec3 angularVelocity, glm::vec3 direction) const {
		float maxVelocity = {};
		for (glm::vec3 point : points) {
			const glm::vec3 r = point - centerOfMass;
			const glm::vec3 linearV = glm::cross(angularVelocity, r);
			const float speed = glm::dot(direction, linearV);
			if (speed > maxVelocity) {
				maxVelocity = speed;
			}
		}
		return maxVelocity;
	}

	glm::mat3 ConvexHullCollider::getIntertiaTensor() const {
		constexpr float oneTwelveth = 1.0f / 12.0f;
		const float dx = bounds.getMaxExtent().x - bounds.getMinExtent().x;
		const float dy = bounds.getMaxExtent().y - bounds.getMinExtent().y;
		const float dz = bounds.getMaxExtent().z - bounds.getMinExtent().z;
		const float w2 = dx * dx;
		const float h2 = dy * dy;
		const float d2 = dz * dz;

		const glm::vec3 nCoM = -centerOfMass;
		const float R2 = glm::length2(centerOfMass);
		return {
			oneTwelveth * (h2 + d2) + R2 - nCoM.x * nCoM.x, nCoM.x * nCoM.y, nCoM.x * nCoM.z,
			nCoM.x * nCoM.y, oneTwelveth * (w2 + d2) + R2 - nCoM.y * nCoM.y, nCoM.y * nCoM.z,
			nCoM.x * nCoM.z, nCoM.y * nCoM.z, oneTwelveth * (w2 + h2) + R2 - nCoM.z * nCoM.z
		};
	}


}