#include "plane.h"
#include <ReactorPhysicsEngine/colliders/sphere.h>
#include <ReactorPhysicsEngine/physics_actor.h>

namespace RPhys {
	PlaneCollider::PlaneCollider(glm::vec4 cartesianEq)
		: AbstractCollider(ColliderType::Plane), cartesianPlane(cartesianEq / glm::length(glm::vec3(cartesianEq))) {
	
	}
	const CollisionData PlaneCollider::collide(const IntersectionInfo& info, const AbstractCollider& abstractCollider, float dt) const {
		if (abstractCollider.getType() == ColliderType::Sphere) {
			const SphereCollider& collider = reinterpret_cast<const SphereCollider&>(abstractCollider);
			return { info, Collision::planeSphere(cartesianPlane, info.actorB->getPosition(), collider.getRadius(), dt) };
		} 
	}
	Bounds PlaneCollider::getBounds(glm::vec3 pos, glm::quat ori) const {
		return Bounds(glm::vec3(INFINITY) + pos, glm::vec3(INFINITY) + pos);
	}
	Bounds PlaneCollider::getBounds() const {
		return Bounds(glm::vec3(INFINITY), glm::vec3(INFINITY));
	}
	glm::mat3 PlaneCollider::getIntertiaTensor() const {
		return {
			{ 1.0f, 0.0f, 0.0f },
			{ 0.0f, 1.0f, 0.0f },
			{ 0.0f, 0.0f, 1.0f }
		};
	}
}