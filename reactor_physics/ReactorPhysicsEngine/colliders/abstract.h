#pragma once
#include <glm/glm.hpp>
#include <ReactorPhysicsEngine/coreutils.h>
#include <ReactorPhysicsEngine/physics/collision_detection.h>
#include <ReactorPhysicsEngine/bounds.h>
#include <optional>

namespace RPhys {
	enum class ColliderType : uint8_t {
		Sphere = 0,
		Box = 1,
		Plane = 2,
		Capsule = 3,
		Cylinder = 4,
		ConvexHull = 5
	};
	class AbstractCollider {
	public:
		AbstractCollider(ColliderType type) : type(type) {
			RPHYS_DEBUG_LOG("A collider was created");
		}

		virtual const CollisionData collide(const IntersectionInfo& info, const AbstractCollider& abstractCollider, float dt) const = PURE_VIRTUAL;
		virtual Bounds getBounds(glm::vec3 position, glm::quat orientation) const = PURE_VIRTUAL;
		virtual Bounds getBounds() const = PURE_VIRTUAL;
		virtual float fastestLinearVelocity(glm::vec3 angularVelocity, glm::vec3 direction) const {
			return 0.0f;
		}

		virtual glm::vec3 getSupport(glm::vec3 direction, glm::vec3 position, glm::quat orientation, float bias) const = PURE_VIRTUAL;
		virtual glm::mat3 getIntertiaTensor() const = PURE_VIRTUAL;

		glm::vec3 getCenterOfMass() const {
			return centerOfMass;
		}
		ColliderType getType() const {
			return type;
		}
	protected:
		const ColliderType type;
		glm::vec3 centerOfMass{};
	};
}