#pragma once
#include <ReactorPhysicsEngine/colliders/abstract.h>
namespace RPhys {
	class SphereCollider : public AbstractCollider {
	public:
		SphereCollider(float radius, std::optional<glm::vec3> centerOfMassOverride = {});

		virtual const CollisionData collide(const IntersectionInfo& info, const AbstractCollider& collider, float dt) const override;
		virtual Bounds getBounds(glm::vec3 position, glm::quat orientation) const override;
		virtual Bounds getBounds() const override;
		virtual glm::vec3 getSupport(glm::vec3 direction, glm::vec3 position, glm::quat orientation, float bias) const override;

		virtual glm::mat3 getIntertiaTensor() const override;
		float getRadius() const {
			return radius;
		}
	private:
		float radius;
	};
}