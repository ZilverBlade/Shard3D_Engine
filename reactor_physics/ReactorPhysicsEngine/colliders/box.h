#pragma once
#include <ReactorPhysicsEngine/colliders/abstract.h>
namespace RPhys {
	class BoxCollider : public AbstractCollider {
	public:
		BoxCollider(glm::vec3 extent, std::optional<glm::vec3> centerOfMassOverride = {});

		virtual const CollisionData collide(const IntersectionInfo& info, const AbstractCollider& collider, float dt) const override;
		virtual Bounds getBounds(glm::vec3 position, glm::quat orientation) const override;
		virtual Bounds getBounds() const override;
		virtual glm::vec3 getSupport(glm::vec3 direction, glm::vec3 position, glm::quat orientation, float bias) const override;
		virtual float fastestLinearVelocity(glm::vec3 angularVelocity, glm::vec3 direction) const override;

		virtual glm::mat3 getIntertiaTensor() const override;
		glm::vec3 getMaxExtent() const {
			return extent;
		}
		glm::vec3 getMinExtent() const {
			return -extent;
		}
	private:
		glm::vec3 extent;
	};
}