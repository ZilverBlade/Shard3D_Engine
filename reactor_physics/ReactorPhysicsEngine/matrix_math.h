#pragma once
#include <glm/glm.hpp>
#include <glm/gtx/norm.hpp>
namespace RPhys {
	template<size_t S>
	static inline glm::mat<S - 1, S - 1, float, glm::highp> matrixMinor(const glm::mat<S, S, float, glm::highp>& matrix, int i, int j) {
		glm::mat<S - 1, S - 1, float, glm::highp> minor{0.0f};

		int yy = 0;

		for (int y = 0; y < S; y++) {
			if (y == j) continue;

			int xx = 0;
			for (int x = 0; x < S; x++) {
				if (x == i) continue;

				minor[xx][yy] = matrix[x][y];
				xx++;
			}
			yy++;
		}

		return minor;
	}

	template<size_t S>
	static inline float matrixCofactor(const glm::mat<S, S, float, glm::highp>& matrix, int i, int j) {
		glm::mat<S - 1, S - 1, float, glm::highp> minor = matrixMinor(matrix, i, j);
		return static_cast<float>(pow(-1, i + j)) * glm::determinant(minor);
	}

}