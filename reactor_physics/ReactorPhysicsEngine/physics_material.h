#pragma once

namespace RPhys {
	struct PhysicsMaterial {
		float elasticity = 0.5f;
		float friction = 0.5f;
	};
}