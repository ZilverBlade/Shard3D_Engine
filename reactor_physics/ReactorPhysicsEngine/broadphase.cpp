#include "broadphase.h"
#include <algorithm>
namespace RPhys {
	const static glm::vec3 axis = { 0.5773502588f, 0.5773502588f, 0.5773502588f };
	std::vector<PseudoActor> Broadphase::sortActorBounds(const std::vector<PhysicsActor>& actors, float dt) {
		std::vector<PseudoActor> sortedActors{};
		sortedActors.resize(actors.size() * 2);

		for (int i = 0; i < actors.size(); i++) {
			const PhysicsActor& actor = actors.at(i);
			Bounds bounds = actor.getCollider().getBounds();
			bounds.expand(bounds.getMinExtent() + actor.getLinearVelocity() * dt);
			bounds.expand(bounds.getMaxExtent() + actor.getLinearVelocity() * dt);

			constexpr float epsilon = 0.01f;
			bounds.expand(bounds.getMinExtent() + glm::vec3(-1.0) * epsilon);
			bounds.expand(bounds.getMaxExtent() + glm::vec3(1.0) * epsilon);

			sortedActors[i * 2 + 0].id = i;
			sortedActors[i * 2 + 0].val = glm::dot(axis, bounds.getMinExtent());
			sortedActors[i * 2 + 0].isMin = true;

			sortedActors[i * 2 + 1].id = i;
			sortedActors[i * 2 + 1].val = glm::dot(axis, bounds.getMaxExtent());
			sortedActors[i * 2 + 1].isMin = false;
		}

		std::sort(sortedActors.begin(), sortedActors.end());
		return sortedActors;
	}
	std::vector<CollisionPair> Broadphase::buildPairs(const std::vector<PseudoActor>& actors) {
		std::vector<CollisionPair> collisionPairs{};

		for (int i = 0; i < actors.size(); i++) {
			const PseudoActor& a = actors[i];
			if (!a.isMin) continue;

			CollisionPair pair;
			pair.a = a.id;

			for (int j = i + 1; j < actors.size(); j++) {
				const PseudoActor& b = actors[j];
				if (b.id == a.id) break;

				if (!b.isMin) continue;

				pair.b = b.id;
				collisionPairs.push_back(pair);
			}
		}
		return collisionPairs;
	}
}