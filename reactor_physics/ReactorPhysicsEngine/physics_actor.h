#pragma once
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>

#include <ReactorPhysicsEngine/coreutils.h>
#include <ReactorPhysicsEngine/physics_material.h>
#include <ReactorPhysicsEngine/colliders/abstract.h>
#include <ReactorPhysicsEngine/colliders/sphere.h>
#include <ReactorPhysicsEngine/ref_counter.h>
namespace RPhys {
	enum class PhysicsMobility {
		Static = 0, // not affected by gravity or other objects, velocity is always 0
		Dynamic = 1 // affected by gravity and other objects, has a variable velocity
	};

	struct PhysicsActorCreateInfo {
		AbstractCollider* collider;
		glm::vec3 initialLinearVelocity;
		glm::vec3 initialAngularVelocity;
		glm::vec3 initialPosition;
		glm::quat initialOrientation;
		float mass;
		PhysicsMobility mobility;
		PhysicsMaterial material;
	};

	class PhysicsActor {
	public:
		PhysicsActor(const PhysicsActorCreateInfo& createInfo)
			: position(createInfo.initialPosition), orientation(createInfo.initialOrientation), linearVelocity(createInfo.initialLinearVelocity), angularVelocity(createInfo.initialAngularVelocity),
			invMass(createInfo.mobility == PhysicsMobility::Static ? 0.0f : 1.0f / createInfo.mass), abstractCollider(createInfo.collider), mobility(createInfo.mobility), material(createInfo.material) {
			RPHYS_DEBUG_LOG("A physics actor was created");
		}
		~PhysicsActor() {
			refCounter.decrement();
			if (!refCounter.getCount()) {
				delete abstractCollider;
				RPHYS_DEBUG_LOG("A collider was destroyed");
			}
			RPHYS_DEBUG_LOG("A physics actor was destroyed");
		}

		PhysicsActor(const PhysicsActor& other)
			: position(other.position), orientation(other.orientation), linearVelocity(other.linearVelocity), angularVelocity(other.angularVelocity),
			invMass(other.invMass), abstractCollider(other.abstractCollider), mobility(other.mobility), material(other.material) {
			refCounter.increment();
		}

		void update(float dt);

		glm::vec3 getPosition() const {
			return position;
		}
		void setPosition(glm::vec3 np) {
			position = np;
		}
		glm::quat getOrientation() const {
			return orientation;
		}
		void setOrientation(glm::quat no) {
			orientation = no;
		}
		glm::vec3 getLinearVelocity() const {
			return linearVelocity;
		}	
		void setLinearVelocity(glm::vec3 nv) {
			linearVelocity = nv;
		}
		glm::vec3 getAngularVelocity() const {
			return angularVelocity;
		}
		void setAngularVelocity(glm::vec3 nv) {
			angularVelocity = nv;
		}

		void addLinearForce(glm::vec3 force) {
			linearVelocity += force * invMass;
		}
		void addTorque(glm::vec3 force) {
			angularVelocity += getInvInertiaTensorWorld() * force;
		}
		void addForceAtPoint(glm::vec3 force, glm::vec3 point) {
			addLinearForce(force);
			addTorque(glm::cross(point - getCenterOfMassWorld(), force));
		}

		float getMass() const {
			return 1.0f / invMass;
		}
		float getInvMass() const {
			return invMass;
		}

		glm::mat3 getInvInertiaTensorLocal() const {
			return glm::inverse(abstractCollider->getIntertiaTensor()) * invMass;
		}
		glm::mat3 getInvInertiaTensorWorld() const {
			glm::mat3 localInvTensor = glm::inverse(abstractCollider->getIntertiaTensor());
			localInvTensor *= invMass;
			glm::mat3 matOrient = glm::toMat3(orientation);
			return matOrient * localInvTensor * glm::transpose(matOrient);
		}

		glm::vec3 getCenterOfMassLocal() const {
			return abstractCollider->getCenterOfMass();
		}
		glm::vec3 getCenterOfMassWorld() const {
			return position + glm::rotate(orientation, abstractCollider->getCenterOfMass());
		}

		glm::vec3 worldToLocal(glm::vec3 pt) const {
			return glm::rotate(glm::inverse(orientation), pt - getCenterOfMassWorld());
		}
		glm::vec3 localToWorld(glm::vec3 pt) const {
			return getCenterOfMassWorld() + glm::rotate(orientation, pt);
		}

		const AbstractCollider& getCollider() const {
			return *abstractCollider;
		}
		template<typename ColliderType>
		const ColliderType& getCollider() const {
			return reinterpret_cast<ColliderType&>(getCollider());
		}

		PhysicsMobility getMobility() const {
			return mobility;
		}
		PhysicsMaterial getMaterial() const {
			return material;
		}
	private:
		glm::vec3 position;
		glm::vec3 linearVelocity;
		glm::vec3 angularVelocity;
		glm::quat orientation;
		float invMass;

		RefCounter refCounter;

		AbstractCollider* abstractCollider;
		PhysicsMobility mobility;
		PhysicsMaterial material;
	};
}