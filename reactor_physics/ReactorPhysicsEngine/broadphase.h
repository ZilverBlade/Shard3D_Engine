#pragma once
#include <glm/glm.hpp>
#include <vector>
#include <ReactorPhysicsEngine/coreutils.h>
#include <ReactorPhysicsEngine/physics_actor.h>
namespace RPhys {
	struct PseudoActor {
		uint32_t id;
		float val;
		bool isMin;
		bool operator<(const PseudoActor& other) {
			if (val < other.val) return 1;
			return 0;
		}
	};
	struct CollisionPair {
		uint32_t a;
		uint32_t b;

		bool operator==(const CollisionPair& other) const {
			return (a == other.a) && (b == other.b) || (a == other.b) && (b == other.a);
		}
		bool operator!=(const CollisionPair& other) const {
			return !(*this == other);
		}
	};

	class Broadphase {
	public:
		static std::vector<PseudoActor> sortActorBounds(const std::vector<PhysicsActor>& actors, float dt);
		static std::vector<CollisionPair> buildPairs(const std::vector<PseudoActor>& actors);
	};
}