#include <ReactorPhysicsEngine/bounds.h>

namespace RPhys {
	void Bounds::expand(const std::vector<glm::vec3>& points) {
		for (glm::vec3 pt : points) {
			expand(pt);
		}
	}
	void Bounds::expand(glm::vec3 newsz) {
		glm::bvec3 cmpMin = glm::lessThan(newsz, minExt);
		glm::bvec3 cmpMax = glm::greaterThan(newsz, maxExt);

		if (cmpMin.x) minExt.x = newsz.x;
		if (cmpMin.y) minExt.y = newsz.y;
		if (cmpMin.z) minExt.z = newsz.z;

		if (cmpMax.x) maxExt.x = newsz.x;
		if (cmpMax.y) maxExt.y = newsz.y;
		if (cmpMax.z) maxExt.z = newsz.z;
	}
	void Bounds::expand(Bounds newbds) {
		expand(newbds.minExt);
		expand(newbds.maxExt);
	}
}