#pragma once

#include <iostream>

#ifndef NDEBUG
#define RPHYS_ENALBE_LOG
#endif
#ifdef RPHYS_ENALBE_LOG
#define RPHYS_DEBUG_LOG(string) std::cout << string << "\n";
#else
#define RPHYS_DEBUG_LOG()
#endif

#define PURE_VIRTUAL 0