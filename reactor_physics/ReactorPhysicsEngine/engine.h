#pragma once
#include <ReactorPhysicsEngine/physics_actor.h>
#include <ReactorPhysicsEngine/physics/collision_response.h>
#include <ReactorPhysicsEngine/broadphase.h>
#include <vector>

namespace RPhys {
	struct PhysicsUniverseData {
		glm::vec3 gravity{};
	};
	class PhysicsEngine {
	public:
		PhysicsEngine();

		PhysicsEngine(const PhysicsEngine&) = delete;
		PhysicsEngine& operator=(const PhysicsEngine&) = delete;
		PhysicsEngine(PhysicsEngine&&) = delete;
		PhysicsEngine& operator=(PhysicsEngine&&) = delete;

		size_t addActor(const PhysicsActor& actor);
		void destroyActor(size_t actorIndex);
		void destroyAllActors();
		void registerCollisions(float dt);
		void simulate(float dt);

		size_t getActorCountInScene() {
			return physicsActorList.size();
		}
		PhysicsActor& getActor(size_t index) {
			return physicsActorList.at(index);
		}

		PhysicsUniverseData universeData{};
	private:
		PhysicsEngine(const std::vector<PhysicsActor>& v) :  physicsActorList(v) {}

		std::vector<PhysicsActor> physicsActorList;
	};
}