#pragma once

namespace RPhys {
	class RefCounter {
	public:
		RefCounter() : count(1) {}
		void increment() {
			count++;
		}
		void decrement() {
			count++;
		}
		unsigned int getCount() {
			return count;
		}
	private:
		unsigned int count = 0;
	};
}