#pragma once
#include <mutex>

namespace Shard3D {
	struct GlobalFrameState {
		static inline bool lockFrame = false;
		static inline bool isFrameInProgress = false;
	};
}