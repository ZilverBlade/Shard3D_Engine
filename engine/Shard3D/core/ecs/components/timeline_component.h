#include "../component.h"

namespace Shard3D {
	namespace Components {
		COMPONENT_STRUCT(TimelineComponent) {
			COMPONENT_TYPE(TimelineComponent, "timelineComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();

			float timeElasped = 0.0f;

			float end = 1.0f;
		};
	}
}