#pragma once

#include "../component.h"
#include "../../../core/asset/assetid.h"
namespace Shard3D {
	namespace Components {

		NON_SERIALIZABLE_COMPONENT_STRUCT(BillboardComponent) {
			NON_SERIALIZABLE_COMPONENT_TYPE(BillboardComponent);
			AssetID asset{ AssetID::null() };

			bool hideInGame = false;

			enum class BillboardOrientation {
				SCREEN_VIEW_ALIGNED,
				VIEW_POINT_ALIGNED,
				AXIAL,
			};
			// SCREEN_VIEW_ALIGNED is the only supported orientation at the moment
			BillboardOrientation orientation = BillboardOrientation::SCREEN_VIEW_ALIGNED;

			BillboardComponent(const AssetID & tex) : asset(tex.getAsset()) {}
		};
	}
}