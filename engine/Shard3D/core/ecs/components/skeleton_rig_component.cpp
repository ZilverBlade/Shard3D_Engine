#include "skeleton_rig_component.h"
#include "../level.h"
#include "../actor.h"
#include "../../asset/assetmgr.h"
#include "../../../systems/handlers/render_list.h"

namespace Shard3D::Components {

	COMPONENT_SERIALIZE_OVERRIDE_IMPL(SkeletonRigComponent) {
		assets.push_back(asset.getAsset());

		out["skeletonRigComponent"]["asset"] = asset.getAsset();
	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(SkeletonRigComponent) {
		resourceSystem->loadSkeleton(
			std::string(data["skeletonRigComponent"]["asset"].get_string().value())
		);
		
		asset = AssetID(std::string(data["skeletonRigComponent"]["asset"].get_string().value()));
		
	}

}