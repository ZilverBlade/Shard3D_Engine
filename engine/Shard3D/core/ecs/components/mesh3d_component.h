#pragma once

#include "../component.h"
#include "../../../core/asset/assetid.h"
namespace Shard3D {
	class ResourceSystem;
	namespace Components {
		COMPONENT_STRUCT(Mesh3DComponent) {
			COMPONENT_TYPE(Mesh3DComponent, "mesh3DComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();
			Mesh3DComponent(ResourceSystem* resourceSystem, AssetID mdl);

			AssetID asset{ AssetID::null() };
			bool castShadows = true;
			bool receiveDecals = true;

			// resets materials to be whatever mesh is loaded if the material size doesnt match the asset
			void validate(ResourceSystem * resourceSystem);
			const std::vector<AssetID>& getMaterials();
			void setMaterials(const std::vector<AssetID>& materials);
			void setMaterial(AssetID material, size_t index);

			bool dirty = true;
	private:
			std::vector<AssetID> materials;
		};
	}
}