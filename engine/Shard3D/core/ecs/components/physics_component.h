#pragma once

#include "../component.h"
#include "../../asset/physics_asset.h"
namespace RPhys {
	class PhysicsEngine;
}
namespace Shard3D {
	namespace Components {
		COMPONENT_STRUCT(Rigidbody3DComponent) {
			COMPONENT_TYPE(Rigidbody3DComponent, "rigidbody3DComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();
			
			size_t instantiatedActor = 0; // internal physics body

			bool usePhysicsAssetFromFile = false;
			AssetID physicsAssetFromFile = AssetID::null();
			PhysicsAsset asset;

			glm::vec3 linearVelocity{};
			glm::vec3 angularVelocity{};

			bool enableCollision = true;
			bool finiteRotation = false;
			AssetID material = "engine/physics/materials/stone.s3dasset";
			std::vector<PhysicsCollisionData> collidingWith;
		};
		COMPONENT_STRUCT(GyroscopeComponent) {
			COMPONENT_TYPE(GyroscopeComponent, "gyroscopeComponent");

			glm::vec3 axis;
			float strength;
		};
		COMPONENT_STRUCT(PhysicsConstraintComponent) {
			COMPONENT_TYPE(PhysicsConstraintComponent, "physicsConstraintComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();

			entt::entity actorA;
			entt::entity actorB;
			bool allowActorCollision = false;

			size_t instantiatedActor = 0; // internal physics joint

			PhysicsConstraintType constraintType = PhysicsConstraintType::Fixed;

			glm::vec3 perpendicularAxisOffsetRotation = {0.f, 3.1415926f / 2.f, 0.f};
			float suspensionSoftness;
			float hingeAngle;
			float perpHingeAngle;
			float sliderPosition;

			bool limitConstraint = true;
			float limitMin = -3.1415926f;
			float limitMax = 3.1415926f;

			float fudge = 0.0f;
			float bounce = 0.0f;
			float spring = 0.0f;
		};
		COMPONENT_STRUCT(SwayBarComponent) {
			COMPONENT_TYPE(SwayBarComponent, "swayBarComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();

			std::array<entt::entity, 2> wheels; // LEFT; RIGHT
			std::array<entt::entity, 2> suspension; // LEFT; RIGHT
			entt::entity chassis;
			
			float swayForce = 10.0f;
			float swayForceMAX = 15.0f;
		};
		COMPONENT_STRUCT(IndependentSuspensionComponent) {
			COMPONENT_TYPE(IndependentSuspensionComponent, "independentSuspensionComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();

			size_t suspensionActor = 0; // connect body to axle (piston joint)
			size_t axleActor = 0; // connect axle to wheel (hinge joint)
			size_t axleBodyActor = 0; // body actor, required to connect
			entt::entity wheel;
			entt::entity chassis;

			glm::vec3 perpendicularAxisOffsetRotation = { 0.f, 3.1415926f / 2.f, 0.f };
			float frequency = 1.5f;
			float baseHeight = 0.2f;
			float damping = 1.0f;
			float steeringAngle = 0.0f;
			bool hasAxle = true; // for wheels to connect, disable if not needed
		};
		COMPONENT_STRUCT(VehicleComponent) {
			COMPONENT_TYPE(VehicleComponent, "vehicleComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();

			entt::entity rearSwayBar;
			entt::entity frontSwayBar;
			entt::entity chassis;
		};
	}
}