#pragma once

#include "../component.h"

namespace Shard3D {
	namespace Components {
		COMPONENT_STRUCT(VirtualPointLightComponent) {
			COMPONENT_TYPE(VirtualPointLightComponent, "virtualPointLightComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();

			glm::vec3 color = { 1.f, 1.f, 1.f };
			float lightIntensity = 1.0f;
		};
	}
}