#pragma once

#include "../component.h"
#include "../../misc/UUID.h"
#include "../../../s3dstd.h"
namespace Shard3D {
	class ResourceSystem;
	inline namespace ECS {
		class Prefab;
		class Level;
	}
	namespace Components {
		COMPONENT_STRUCT(PrefabComponent) {
			COMPONENT_TYPE(PrefabComponent, "prefabComponent");
			PrefabComponent(AssetID pf) : prefab(pf) {}
			COMPONENT_SERIALIZATION_OVERRIDE();
			
			AssetID prefab{AssetID::null()};
		};
	}
}