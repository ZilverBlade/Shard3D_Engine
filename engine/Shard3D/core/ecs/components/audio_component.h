#pragma once

#include "../component.h"
#include "../../audio/audio.h"
namespace Shard3D {
namespace Components {

	NON_SERIALIZABLE_COMPONENT_STRUCT(AudioComponent) {
		NON_SERIALIZABLE_COMPONENT_TYPE(AudioComponent);
		
		std::string file{};
		AudioProperties properties{};

		void play() {
			audioEngine = new EngineAudio();
			audioEngine->play(file);
			audioEngine->update(properties);
		}
		void pause() {
			audioEngine->pause();
		}
		void resume() {
			audioEngine->resume();
		}
		void update() {
			audioEngine->update(properties);
		}
		void stop() {
			audioEngine->stop();
			delete audioEngine;
		}
	private:
		EngineAudio* audioEngine{};
	};
}
}