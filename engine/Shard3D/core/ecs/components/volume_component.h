#pragma once

#include "../component.h"
namespace Shard3D {
	namespace Components {
		COMPONENT_STRUCT(BoxVolumeComponent){
			COMPONENT_TYPE(BoxVolumeComponent, "boxVolumeComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();
			float transitionDistance = 0.5f;
			glm::vec3 bounds = { 1.f, 1.f, 1.f };
		};
		COMPONENT_STRUCT(SphereVolumeComponent) {
			COMPONENT_TYPE(SphereVolumeComponent, "sphereVolumeComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();
			float transitionDistance = 0.5f;
			float radius = 1.f;
		};
	}
}