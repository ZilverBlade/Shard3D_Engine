#pragma once

#include "../component.h"
#include "../../misc/UUID.h"
namespace Shard3D {
/*
	Mobility hierarchy is as follows : 
	All dynamic parents must have dynamic children
	All stationary parents must have either stationary or dynamic children
	Static parents may have any type of mobility in their children
*/
	enum class Mobility { 
		Static = 0, Stationary = 1, Dynamic = 2 // NOTE: do not change the order of these numbers since these are used to compare the mobility "hierarchy" e.g. Dynamic > Stationary > Static
	};
	inline namespace ECS {
		class Level;
		class LevelManager;
		class Prefab;
	}
	namespace Components {

		// for iteration optimisations only
		NON_INTERACTIBLE_COMPONENT_STRUCT(IsVisibileComponent);
		NON_INTERACTIBLE_COMPONENT_STRUCT(StaticActorComponent);
		NON_INTERACTIBLE_COMPONENT_STRUCT(StationaryActorComponent);
		NON_INTERACTIBLE_COMPONENT_STRUCT(DynamicActorComponent);
		NON_INTERACTIBLE_COMPONENT_STRUCT(RebuildTransformComponent);

		COMPONENT_STRUCT(MobilityComponent) {
			COMPONENT_TYPE(MobilityComponent, "mobilityComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();

			Mobility mobility = Mobility::Static;
		};
		COMPONENT_STRUCT(VisibilityComponent) {
			COMPONENT_TYPE(VisibilityComponent, "visibilityComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();
			bool isVisible = true; 
			bool exportable = true; 
			bool editorOnly = false; 
		};


		NON_SERIALIZABLE_COMPONENT_STRUCT(UUIDComponent) {
			NON_SERIALIZABLE_COMPONENT_TYPE(UUIDComponent);
			inline UUID getScopedID() { 
				return id; 
			}
			inline UUID getInstancerUUID() {
				return instancer;
			}
			UUIDComponent(UUID instancer_, UUID id_) {
				instancer = instancer_;
				id = id_;
			}
		private:
			UUID instancer;
			UUID id;
			friend class ECS::LevelManager;
			friend class ECS::Level;
			friend class ECS::Prefab;
		};

		NON_SERIALIZABLE_COMPONENT_STRUCT(TagComponent) {
			NON_SERIALIZABLE_COMPONENT_TYPE(TagComponent);
			std::string tag;

			operator std::string() { return tag; };
		};

		COMPONENT_STRUCT(RelationshipComponent) {
			COMPONENT_TYPE(RelationshipComponent, "relationshipComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();
			entt::entity parentActor{ entt::null };
			std::vector<entt::entity> childActors{};
		};

		NON_SERIALIZABLE_COMPONENT_STRUCT(WorldSceneInfoComponent) {	// basically the singleton of the level
			NON_SERIALIZABLE_COMPONENT_TYPE(WorldSceneInfoComponent);
			glm::vec3 worldGravity = {0.0f, -9.8f, 0.f};
			glm::vec3 windForce = { 0.0f, 0.0f, 0.0f };
			float temperatureC = 20.0f;
			float secondsElapsedSinceMidnight = 46800.f;
		};

	}
}