#pragma once

#include "../component.h"

namespace Shard3D {
	struct TerrainPhysicsData;
	namespace Components {
		COMPONENT_STRUCT(TerrainComponent) {
			COMPONENT_TYPE(TerrainComponent, "terrainComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();

			AssetID terrainAsset = AssetID::null();

			TerrainPhysicsData* physData;

			std::vector<AssetID> materials;
			int maxMaterials = 4;

			glm::ivec2 tiles = { 4, 4 };
			uint32_t tileSubdivisions = 64;
			uint32_t tilePhysicsSubdivisions = 32;
			float tileExtent = 64.f;
			bool loadAsset = false;
			bool dirty = false;
			bool allowDualMaterialMap = false;
			bool force32BitHeightMap = false;
		};
	}
}