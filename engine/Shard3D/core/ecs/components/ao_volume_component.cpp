#include "ao_volume_component.h"
#include "../../asset/assetmgr.h"
#include <glm/gtc/type_ptr.hpp>

namespace Shard3D::Components {
	COMPONENT_SERIALIZE_OVERRIDE_IMPL(BoxAmbientOcclusionVolumeComponent) {
		out["boxAmbientOcclusionVolumeComponent"]["tint"] = tint;
		out["boxAmbientOcclusionVolumeComponent"]["minOcclusion"] = minOcclusion;
	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(BoxAmbientOcclusionVolumeComponent) {
		tint = SIMDJSON_READ_VEC3(data["boxAmbientOcclusionVolumeComponent"]["tint"]);
		minOcclusion = data["boxAmbientOcclusionVolumeComponent"]["minOcclusion"].get_double().value();
	}
}