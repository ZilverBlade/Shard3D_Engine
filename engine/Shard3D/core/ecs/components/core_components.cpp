#include "core_components.h"
#include "../actor.h"
#include "../../asset/assetmgr.h"

namespace Shard3D::Components {
	COMPONENT_SERIALIZE_OVERRIDE_IMPL(MobilityComponent) {
		switch (mobility) {
		case (Mobility::Static):
			out["mobilityComponent"]["mobility"] = "static";
			break;
		case (Mobility::Stationary):
			out["mobilityComponent"]["mobility"] = "stationary"; 
			break;
		case (Mobility::Dynamic):
			out["mobilityComponent"]["mobility"] = "dynamic";	
			break;
		}
	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(MobilityComponent) {
		std::string_view mobility = data["mobilityComponent"]["mobility"].get_string().value();
		if (mobility == "static")
			actor->makeStatic();
		else if (mobility  == "stationary")
			actor->makeStationary();
		else if (mobility == "dynamic")
			actor->makeDynamic();
	}

	COMPONENT_SERIALIZE_OVERRIDE_IMPL(VisibilityComponent) {
		out["visibilityComponent"]["visible"] = isVisible;
		out["visibilityComponent"]["editorOnly"] = editorOnly;
		out["visibilityComponent"]["exportable"] = exportable;
	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(VisibilityComponent) {
		isVisible = data["visibilityComponent"]["visible"].get_bool().value();
		exportable = data["visibilityComponent"]["exportable"].get_bool().value();
		editorOnly = data["visibilityComponent"]["editorOnly"].get_bool().value();
		if (!isVisible) {
			actor->makeInvisible();
		}
	}

	COMPONENT_SERIALIZE_OVERRIDE_IMPL(RelationshipComponent) {
		out["relationshipComponent"]["parent"] = static_cast<uint64_t>((parentActor != entt::null) ? Actor(parentActor, level).getScopedID() : UUID(1));
		std::vector<uint64_t> correctedIDs;
		correctedIDs.reserve(childActors.size());

		for (auto& child : childActors)
			correctedIDs.push_back(Actor(child, level).getScopedID());

		out["relationshipComponent"]["children"] = correctedIDs;
	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(RelationshipComponent) {
		uint64_t parentID = data["relationshipComponent"]["parent"].get_uint64().value();
		if (parentID != 1) parentActor = level->getActorFromUUID(0, parentID).actorHandle;

		std::vector<uint64_t> cactors{};
		for (int i = 0; i < data["relationshipComponent"]["children"].get_array().size(); i++) {
			cactors.push_back(data["relationshipComponent"]["children"].get_array().at(i).get_uint64().value());
		}
		childActors.reserve(cactors.size());

		for (auto& child : cactors) {
			Actor retrievedActor = level->getActorFromUUID(0, child);
			if (retrievedActor.isInvalid()) continue; // fail
			childActors.push_back(retrievedActor.actorHandle);
		}
	}
}