#include "physics_component.h"
#include "../level.h"
#include "../actor.h"
#include "../../asset/assetmgr.h"
#include "../../../systems/computational/physics_system.h"

namespace Shard3D::Components {
	COMPONENT_SERIALIZE_OVERRIDE_IMPL(Rigidbody3DComponent) {
		out["rigidbody3DComponent"]["usePhysicsAssetFromFile"] = usePhysicsAssetFromFile;
		if (usePhysicsAssetFromFile) {
			out["rigidbody3DComponent"]["physicsAssetFromFile"] = physicsAssetFromFile.getAsset();
		} else {
			switch (asset.colliderType) {
			case(PhysicsColliderType::Sphere):
				out["rigidbody3DComponent"]["assetData"]["colliderType"] = "sphere";
				out["rigidbody3DComponent"]["assetData"]["collider"]["radius"] = asset.sphereRadius;
				break;
			case(PhysicsColliderType::Box):
				out["rigidbody3DComponent"]["assetData"]["colliderType"] = "box";
				out["rigidbody3DComponent"]["assetData"]["collider"]["extent"] = asset.boxExtent;
				break;
			case(PhysicsColliderType::Plane):
				out["rigidbody3DComponent"]["assetData"]["colliderType"] = "plane";
				break;
			case(PhysicsColliderType::Capsule):
				out["rigidbody3DComponent"]["assetData"]["colliderType"] = "capsule";
				out["rigidbody3DComponent"]["assetData"]["collider"]["radius"] = asset.cylindricalRadius;
				out["rigidbody3DComponent"]["assetData"]["rigidbody3DComponent"]["assetData"]["collider"]["length"] = asset.cylindricalLength;
				break;
			case(PhysicsColliderType::Cylinder):
				out["rigidbody3DComponent"]["assetData"]["colliderType"] = "cylinder";
				out["rigidbody3DComponent"]["assetData"]["collider"]["radius"] = asset.cylindricalRadius;
				out["rigidbody3DComponent"]["assetData"]["collider"]["length"] = asset.cylindricalLength;
				break;
			case(PhysicsColliderType::ConvexHullAsset):
				out["rigidbody3DComponent"]["assetData"]["colliderType"] = "convex_hull";
				out["rigidbody3DComponent"]["assetData"]["collider"]["hullPoints"] = nlohmann::ordered_json::array();
				for (glm::vec3 point : asset.convexPoints) {
					out["rigidbody3DComponent"]["assetData"]["collider"]["hullPoints"].push_back(point);
				}
				break;		
			case(PhysicsColliderType::MeshAsset):
				out["rigidbody3DComponent"]["assetData"]["colliderType"] = "trimesh";
				out["rigidbody3DComponent"]["assetData"]["collider"]["trimeshAsset"] = asset.trimeshAsset.getAsset();
				break;
			case(PhysicsColliderType::Terrain):
				out["rigidbody3DComponent"]["assetData"]["colliderType"] = "terrain";
				break;
			}

			out["rigidbody3DComponent"]["assetData"]["mass"] = asset.mass;
			out["rigidbody3DComponent"]["assetData"]["centerOfMass"] = asset.computedCOM;
			out["rigidbody3DComponent"]["assetData"]["intertiaTensor"] = nlohmann::ordered_json::array();
			out["rigidbody3DComponent"]["assetData"]["intertiaTensor"].push_back(asset.computedInteriaTensor[0]);
			out["rigidbody3DComponent"]["assetData"]["intertiaTensor"].push_back(asset.computedInteriaTensor[1]);
			out["rigidbody3DComponent"]["assetData"]["intertiaTensor"].push_back(asset.computedInteriaTensor[2]);

			out["rigidbody3DComponent"]["assetData"]["positionOffset"] = asset.colliderPositionOffset;
			out["rigidbody3DComponent"]["assetData"]["rotationOffset"] = asset.colliderRotationOffset;
		}

		out["rigidbody3DComponent"]["materialAsset"] = material.getAsset();
		out["rigidbody3DComponent"]["properties"]["enableCollision"] = enableCollision;
		out["rigidbody3DComponent"]["properties"]["finiteRotation"] = finiteRotation;
		out["rigidbody3DComponent"]["linearVelocity"] = linearVelocity;
		out["rigidbody3DComponent"]["angularVelocity"] = angularVelocity;
	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(Rigidbody3DComponent) {
		if (data["rigidbody3DComponent"]["usePhysicsAssetFromFile"].get_bool().value() == true) {
			physicsAssetFromFile = std::string(data["rigidbody3DComponent"]["usePhysicsAssetFromFile"].get_string().value());
			asset = AssetManager::loadPhysicsAsset(physicsAssetFromFile);
		} else {
			if (data["rigidbody3DComponent"]["assetData"]["colliderType"].get_string().value() == "sphere") {
				asset.colliderType = PhysicsColliderType::Sphere;
				asset.sphereRadius = data["rigidbody3DComponent"]["assetData"]["collider"]["radius"].get_double().value();
			}
			if (data["rigidbody3DComponent"]["assetData"]["colliderType"].get_string().value() == "box") {
				asset.colliderType = PhysicsColliderType::Box;
				asset.boxExtent = SIMDJSON_READ_VEC3(data["rigidbody3DComponent"]["assetData"]["collider"]["extent"]);
			}
			if (data["rigidbody3DComponent"]["assetData"]["colliderType"].get_string().value() == "plane") {
				asset.colliderType = PhysicsColliderType::Plane;
			}
			if (data["rigidbody3DComponent"]["assetData"]["colliderType"].get_string().value() == "capsule") {
				asset.colliderType = PhysicsColliderType::Capsule;
				asset.cylindricalRadius = data["rigidbody3DComponent"]["assetData"]["collider"]["radius"].get_double().value();
				asset.cylindricalLength = data["rigidbody3DComponent"]["assetData"]["collider"]["length"].get_double().value();
			}
			if (data["rigidbody3DComponent"]["assetData"]["colliderType"].get_string().value() == "cylinder") {
				asset.colliderType = PhysicsColliderType::Cylinder;
				asset.cylindricalRadius = data["rigidbody3DComponent"]["assetData"]["collider"]["radius"].get_double().value();
				asset.cylindricalLength = data["rigidbody3DComponent"]["assetData"]["collider"]["length"].get_double().value();
			}
			if (data["rigidbody3DComponent"]["assetData"]["colliderType"].get_string().value() == "convex_hull") {
				asset.colliderType = PhysicsColliderType::ConvexHullAsset;

				for (int i = 0; i < data["rigidbody3DComponent"]["assetData"]["collider"]["hullPoints"].get_array().size(); i++) {
					asset.convexPoints.push_back(SIMDJSON_READ_VEC3(data["rigidbody3DComponent"]["assetData"]["collider"]["hullPoints"].get_array().at(i)));
				}
			}
			if (data["rigidbody3DComponent"]["assetData"]["colliderType"].get_string().value() == "trimesh") {
				asset.colliderType = PhysicsColliderType::MeshAsset;
				asset.trimeshAsset = 
					std::string(data["rigidbody3DComponent"]["assetData"]["collider"]["trimeshAsset"].get_string().value());
			}

			asset.mass = data["rigidbody3DComponent"]["assetData"]["mass"].get_double().value();
			asset.computedCOM = SIMDJSON_READ_VEC3(data["rigidbody3DComponent"]["assetData"]["centerOfMass"]);
			asset.computedInteriaTensor[0] = SIMDJSON_READ_VEC3(data["rigidbody3DComponent"]["assetData"]["intertiaTensor"].get_array().at(0));
			asset.computedInteriaTensor[1] = SIMDJSON_READ_VEC3(data["rigidbody3DComponent"]["assetData"]["intertiaTensor"].get_array().at(1));
			asset.computedInteriaTensor[2] = SIMDJSON_READ_VEC3(data["rigidbody3DComponent"]["assetData"]["intertiaTensor"].get_array().at(2));

			asset.colliderPositionOffset = SIMDJSON_READ_VEC3(data["rigidbody3DComponent"]["assetData"]["positionOffset"]);
			asset.colliderRotationOffset = SIMDJSON_READ_VEC3(data["rigidbody3DComponent"]["assetData"]["rotationOffset"]);
		}

		if (data["rigidbody3DComponent"]["materialAsset"].error() == simdjson::SUCCESS) {
			material = std::string(data["rigidbody3DComponent"]["materialAsset"].get_string().value());
		}

		linearVelocity = SIMDJSON_READ_VEC3(data["rigidbody3DComponent"]["linearVelocity"]);
		angularVelocity = SIMDJSON_READ_VEC3(data["rigidbody3DComponent"]["angularVelocity"]);

		if (data["rigidbody3DComponent"]["properties"]["enableCollision"].error() == simdjson::SUCCESS) {
			enableCollision = data["rigidbody3DComponent"]["properties"]["enableCollision"].get_bool().value();
		}
		if (data["rigidbody3DComponent"]["properties"]["finiteRotation"].error() == simdjson::SUCCESS) {
			finiteRotation = data["rigidbody3DComponent"]["properties"]["finiteRotation"].get_bool().value();
		}
	}

	COMPONENT_SERIALIZE_OVERRIDE_IMPL(PhysicsConstraintComponent) {
		switch (constraintType) {
		case(PhysicsConstraintType::Fixed):
			out["physicsConstraintComponent"]["constraintType"] = "fixed";
			break;
		case(PhysicsConstraintType::Ball):
			out["physicsConstraintComponent"]["constraintType"] = "ball";
			break;
		case(PhysicsConstraintType::Hinge):
			out["physicsConstraintComponent"]["constraintType"] = "hinge";
			out["physicsConstraintComponent"]["constraintData"]["baseAngle"] = hingeAngle;
			break;
		case(PhysicsConstraintType::DualHinge):
			out["physicsConstraintComponent"]["constraintType"] = "dual_hinge";
			out["physicsConstraintComponent"]["constraintData"]["perpendicularAxisOffsetRotation"] = perpendicularAxisOffsetRotation;
			out["physicsConstraintComponent"]["constraintData"]["baseAngle"] = hingeAngle;
			out["physicsConstraintComponent"]["constraintData"]["baseAngleSecondary"] = perpHingeAngle;
			out["physicsConstraintComponent"]["constraintData"]["suspensionSoftness"] = suspensionSoftness;
			break;
		}
		out["physicsConstraintComponent"]["actorA"] = (uint64_t)Actor(actorA, level).getScopedID();
		out["physicsConstraintComponent"]["actorB"] = (uint64_t)Actor(actorB, level).getScopedID();
		out["physicsConstraintComponent"]["allowActorCollision"] = allowActorCollision;

		out["physicsConstraintComponent"]["limitConstraint"] = limitConstraint;
		out["physicsConstraintComponent"]["limitMin"] = limitMin;
		out["physicsConstraintComponent"]["limitMax"] = limitMax;
		out["physicsConstraintComponent"]["spring"] = spring;
		out["physicsConstraintComponent"]["bounce"] = bounce;
		out["physicsConstraintComponent"]["fudge"] = fudge;

	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(PhysicsConstraintComponent) {
		if (data["physicsConstraintComponent"]["constraintType"].get_string().value() == "fixed") {
			constraintType = PhysicsConstraintType::Fixed;
		}
		if (data["physicsConstraintComponent"]["constraintType"].get_string().value() == "ball") {
			constraintType = PhysicsConstraintType::Ball;
		}
		if (data["physicsConstraintComponent"]["constraintType"].get_string().value() == "hinge") {
			constraintType= PhysicsConstraintType::Hinge;
			hingeAngle = data["physicsConstraintComponent"]["constraintData"]["baseAngle"].get_double().value();
		}
		if (data["physicsConstraintComponent"]["constraintType"].get_string().value() == "dual_hinge") {
			constraintType = PhysicsConstraintType::DualHinge;
			perpendicularAxisOffsetRotation = SIMDJSON_READ_VEC3(data["physicsConstraintComponent"]["constraintData"]["perpendicularAxisOffsetRotation"]);
			hingeAngle = data["physicsConstraintComponent"]["constraintData"]["baseAngle"].get_double().value();
			perpHingeAngle = data["physicsConstraintComponent"]["constraintData"]["baseAngleSecondary"].get_double().value();
			suspensionSoftness = data["physicsConstraintComponent"]["constraintData"]["suspensionSoftness"].get_double().value();
		}
		if (data["physicsConstraintComponent"]["constraintType"].get_string().value() == "piston") {
			constraintType = PhysicsConstraintType::Piston;
		}
		actorA = level->getActorFromUUID(0, data["physicsConstraintComponent"]["actorA"].get_uint64().value());
		actorB = level->getActorFromUUID(0, data["physicsConstraintComponent"]["actorB"].get_uint64().value());
		allowActorCollision = data["physicsConstraintComponent"]["allowActorCollision"].get_bool().value();

		limitConstraint = data["physicsConstraintComponent"]["limitConstraint"].get_bool().value();
		limitMin = data["physicsConstraintComponent"]["limitMin"].get_double().value();
		limitMax = data["physicsConstraintComponent"]["limitMax"].get_double().value();
		spring = data["physicsConstraintComponent"]["spring"].get_double().value();
		bounce = data["physicsConstraintComponent"]["bounce"].get_double().value();
		fudge = data["physicsConstraintComponent"]["fudge"].get_double().value();
	}

	COMPONENT_SERIALIZE_OVERRIDE_IMPL(SwayBarComponent) {
		out["swayBarComponent"]["swayForce"] = swayForce;
		out["swayBarComponent"]["swayForceMax"] = swayForceMAX;
			
		out["swayBarComponent"]["chassisActor"] = (uint64_t)Actor(chassis, level).getScopedID();
		out["swayBarComponent"]["wheel0Actor"] = (uint64_t)Actor(wheels[0], level).getScopedID();
		out["swayBarComponent"]["wheel1Actor"] = (uint64_t)Actor(wheels[1], level).getScopedID();
		out["swayBarComponent"]["suspension0Actor"] = (uint64_t)Actor(suspension[0], level).getScopedID();
		out["swayBarComponent"]["suspension1Actor"] = (uint64_t)Actor(suspension[1], level).getScopedID();

	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(SwayBarComponent) {
		swayForce = data["swayBarComponent"]["swayForce"].get_double().value();
		swayForceMAX = data["swayBarComponent"]["swayForceMax"].get_double().value();
		chassis = level->getActorFromUUID(0, data["swayBarComponent"]["chassisActor"].get_uint64().value());
		wheels[0] = level->getActorFromUUID(0, data["swayBarComponent"]["wheel0Actor"].get_uint64().value());
		wheels[1] = level->getActorFromUUID(0, data["swayBarComponent"]["wheel1Actor"].get_uint64().value());
		suspension[0] = level->getActorFromUUID(0, data["swayBarComponent"]["suspension0Actor"].get_uint64().value());
		suspension[1] = level->getActorFromUUID(0, data["swayBarComponent"]["suspension1Actor"].get_uint64().value());
	}

	COMPONENT_SERIALIZE_OVERRIDE_IMPL(IndependentSuspensionComponent) {
		out["independentSuspensionComponent"]["perpendicularAxisOffsetRotation"] = perpendicularAxisOffsetRotation;
		out["independentSuspensionComponent"]["springFrequency"] = frequency;
		out["independentSuspensionComponent"]["springDamping"] = damping;
		out["independentSuspensionComponent"]["steeringAngle"] = steeringAngle;

		out["independentSuspensionComponent"]["chassisActor"] = (uint64_t)Actor(chassis, level).getScopedID();
		out["independentSuspensionComponent"]["wheelActor"] = (uint64_t)Actor(wheel, level).getScopedID();

	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(IndependentSuspensionComponent) {
		perpendicularAxisOffsetRotation = SIMDJSON_READ_VEC3(data["independentSuspensionComponent"]["perpendicularAxisOffsetRotation"]);
		frequency = data["independentSuspensionComponent"]["springFrequency"].get_double().value();
		damping = data["independentSuspensionComponent"]["springDamping"].get_double().value();
		steeringAngle = data["independentSuspensionComponent"]["steeringAngle"].get_double().value();

		chassis = level->getActorFromUUID(0, data["independentSuspensionComponent"]["chassisActor"].get_uint64().value());
		wheel = level->getActorFromUUID(0, data["independentSuspensionComponent"]["wheelActor"].get_uint64().value());
	}

	COMPONENT_SERIALIZE_OVERRIDE_IMPL(VehicleComponent) {
		out["vehicleComponent"]["rearSwayBarActor"] = (uint64_t)Actor(rearSwayBar, level).getScopedID();
		out["vehicleComponent"]["frontSwayBarActor"] = (uint64_t)Actor(frontSwayBar, level).getScopedID();
		out["vehicleComponent"]["chassisActor"] = (uint64_t)Actor(chassis, level).getScopedID();
	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(VehicleComponent) {
		rearSwayBar = level->getActorFromUUID(0, data["vehicleComponent"]["rearSwayBarActor"].get_uint64().value());
		frontSwayBar = level->getActorFromUUID(0, data["vehicleComponent"]["frontSwayBarActor"].get_uint64().value());
		chassis = level->getActorFromUUID(0, data["vehicleComponent"]["chassisActor"].get_uint64().value());
	}
}