#pragma once

#include "../component.h"

namespace Shard3D {
	namespace Components {
		COMPONENT_STRUCT(PointLightComponent) {
			COMPONENT_SERIALIZATION_OVERRIDE();
			virtual const char* getKeyJSON() override {
				return "pointLightComponent";
			}
			PointLightComponent() {
				if (ProjectSystem::getEngineSettings().shading.pointShadowMapType == ESET::ShadowMap::PCF) shadowMapBias = 0.022f;
				else if (ProjectSystem::getEngineSettings().shading.pointShadowMapType == ESET::ShadowMap::Variance) shadowMapBias = 0.75f;
			}
			PointLightComponent(const PointLightComponent&) = default;

			glm::vec3 color = { 1.f, 1.f, 1.f };
			float lightIntensity = 1.0f;
			float lightRadius = 5.0f;
			float sourceRadius = 0.0f;
			float sourceLength = 0.0f;

			bool castShadows = true;
			bool castTranslucentShadows = false;
			bool volumetric = false;
			bool volumetricTranslucency = false;
			// TODO: point cookies, from cubemap
			bool hasCookie = false;
			AssetID cookieTexture = AssetID("engine/textures/null_tex.s3dasset");
			AssetID haloTexture = AssetID("engine/textures/halo_bokeh_3.s3dasset");
			int shadowMapResolution = 128;
			bool shadowMapInstancedDraw = true;
			glm::mat4 lightSpaceMatrix{};
			int shadowMapConvention = 4; // cube is default (look at ShadowMappingSystem for value references), other option is Dual Paraboloid
			uint32_t shadowMap_id{};
			uint32_t shadowMapTranslucent_id{};
			uint32_t shadowMapTranslucentColor_id{};
			uint32_t shadowMapTranslucentReveal_id{};
			float shadowMapBias = 0.0f; // 0.75f for variance, 0.022f for PCF
			float shadowMapSlopeBiasFactor = 1.8f; // only PCF
			float shadowMapSharpness = 0.0f; 

			// PCF EXCLUSIVE
			float shadowMapPoissonRadius = 1.5f; // 1.5 is a good radius
			int shadowMapPoissonSamples = 1; // 1 because having multiple shadows can be expensive
		};

		COMPONENT_STRUCT(SpotLightComponent) {
			COMPONENT_SERIALIZATION_OVERRIDE();
			SpotLightComponent() {
				if (ProjectSystem::getEngineSettings().shading.spotShadowMapType == ESET::ShadowMap::PCF) shadowMapBias = 0.022f;
				else if (ProjectSystem::getEngineSettings().shading.spotShadowMapType == ESET::ShadowMap::Variance) shadowMapBias = 0.75f;
			}
			SpotLightComponent(const SpotLightComponent&) = default;
			virtual const char* getKeyJSON() override {
				return "spotLightComponent";
			}

			glm::vec3 color = { 1.f, 1.f, 1.f };
			float lightIntensity = 1.0f;
			float lightRadius = 5.0f;
			float sourceRadius = 0.0f;
			// cosine outerAngle
			float outerAngle = sqrt(2.f) / 2.f; // 45 degrees
			// cosine innerAngle
			float innerAngle = sqrt(3.f) / 2.f; // 30 degrees

			bool castShadows = true;
			bool castTranslucentShadows = false;
			bool indirectLighting = false;
			bool volumetric = false;
			bool volumetricTranslucency = false;
			bool hasCookie = false;
			AssetID cookieTexture = AssetID("engine/textures/null_tex.s3dasset");
			AssetID haloTexture = AssetID("engine/textures/halo_bokeh_3.s3dasset");
			int shadowMapResolution = 256;
			glm::mat4 lightSpaceMatrix{};
			int shadowMapConvention = 1; // conventional is default (look at ShadowMappingSystem for value references), other option is Single Paraboloid
			uint32_t shadowMap_id{};
			uint32_t shadowMapTranslucent_id{};
			uint32_t shadowMapTranslucentColor_id{};
			uint32_t shadowMapTranslucentReveal_id{};
			float shadowMapBias = 0.0f; // 0.75f for variance, 0.022f for PCF
			float shadowMapSlopeBiasFactor = 1.8f; // only PCF
			float shadowMapSharpness = 0.0f; 

			// PCF EXCLUSIVE
			float shadowMapPoissonRadius = 1.5f; // 1.5 is a good radius
			int shadowMapPoissonSamples = 1; // 1 because having multiple shadows can be expensive
		};

		COMPONENT_STRUCT(DirectionalLightComponent) {
			COMPONENT_SERIALIZATION_OVERRIDE();
			DirectionalLightComponent() {
				if (ProjectSystem::getEngineSettings().shading.directionalShadowMapType == ESET::ShadowMap::PCF) shadowMapBias = 0.010f;
				else if (ProjectSystem::getEngineSettings().shading.directionalShadowMapType == ESET::ShadowMap::Variance) shadowMapBias = 0.75f;
			}
			DirectionalLightComponent(const DirectionalLightComponent&) = default;
			virtual const char* getKeyJSON() override {
				return "directionalLightComponent";
			}

			glm::vec3 color = { 1.f, 1.f, 1.f };
			float lightIntensity = 1.0f;

			bool castShadows = true;
			bool castTranslucentShadows = false;
			bool volumetric = false;
			bool volumetricTranslucency = false;
			bool indirectLighting = false;
			int shadowMapResolution = 1024;
			float shadowMapClippingDistance = 35.f;
			glm::mat4 lightSpaceMatrix{};
			int shadowMapConvention = 1; // (7 for pssm4) conventional is default, but value is hidden, as Static && Stationary directional lights will always be conventional style, but Dynamic will always be PSSM (look at ShadowMappingSystem for value references)
			uint32_t shadowMap_id{};
			uint32_t shadowMapTranslucent_id{};
			uint32_t shadowMapTranslucentColor_id{};
			uint32_t shadowMapTranslucentReveal_id{};
			float shadowMapBias = 0.0f; // 0.75f for variance, 0.002f for PCF
			float shadowMapSlopeBiasFactor = 1.8f; // only PCF
			float shadowMapSharpness = 0.0f;

			// PCF EXCLUSIVE
			float shadowMapPoissonRadius = 1.5f; // 1.5 is a good radius
			int shadowMapPoissonSamples = 4; // 4 yields good
		};
	}
}