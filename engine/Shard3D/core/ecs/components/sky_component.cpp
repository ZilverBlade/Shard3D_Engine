#include "sky_component.h"
#include "../../asset/assetmgr.h"
#include "../actor.h"

namespace Shard3D::Components {
	COMPONENT_SERIALIZE_OVERRIDE_IMPL(SkyLightComponent) {
		out["skyLightComponent"]["skyTint"] = skyTint;
		out["skyLightComponent"]["skyIntensity"] = skyIntensity;
		out["skyLightComponent"]["skyboxActor"] = (uint64_t)Actor(skyboxActor, level).getScopedID();
		out["skyLightComponent"]["skyAtmosphereActor"] = (uint64_t)Actor(skyAtmosphereActor, level).getScopedID();
		out["skyLightComponent"]["reflectRayleighScattering"] = reflectRayleighScattering;
		out["skyLightComponent"]["reflectionType"] = reflectionType;
		out["skyLightComponent"]["skyAtmosphereReflectionResolution"] = skyAtmosphereReflectionResolution;
	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(SkyLightComponent) {
		if (data["skyLightComponent"]["skyTint"].error() == simdjson::SUCCESS) {
			skyTint = SIMDJSON_READ_VEC3(data["skyLightComponent"]["skyTint"]);
		}
		skyIntensity = data["skyLightComponent"]["skyIntensity"].get_double().value();
		if (data["skyLightComponent"]["skyboxActor"].error() == simdjson::SUCCESS) {
			skyboxActor = level->getActorFromUUID(0, (UUID)data["skyLightComponent"]["skyboxActor"].get_uint64());
			skyAtmosphereActor = level->getActorFromUUID(0, (UUID)data["skyLightComponent"]["skyAtmosphereActor"].get_uint64());
			reflectRayleighScattering = data["skyLightComponent"]["reflectRayleighScattering"].get_bool();
			reflectionType = data["skyLightComponent"]["reflectionType"].get_uint64();
			skyAtmosphereReflectionResolution = data["skyLightComponent"]["skyAtmosphereReflectionResolution"].get_uint64();
		}
	}
	COMPONENT_SERIALIZE_OVERRIDE_IMPL(SkyboxComponent) {
		assets.push_back(environment.getAsset());
		out["skyboxComponent"]["environmentCubeAsset"] = environment.getAsset();
		out["skyboxComponent"]["irradianceCubeAsset"] = irradiance.getAsset();
		out["skyboxComponent"]["tint"] = tint;
		out["skyboxComponent"]["intensity"] = intensity;
	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(SkyboxComponent) {
		environment = std::string(data["skyboxComponent"]["environmentCubeAsset"].get_string().value());
		if (data["skyboxComponent"]["irradianceCubeAsset"].error() == simdjson::SUCCESS) {
			irradiance = std::string(data["skyboxComponent"]["irradianceCubeAsset"].get_string().value());
		}
		tint = SIMDJSON_READ_VEC3(data["skyboxComponent"]["tint"]);
		intensity = data["skyboxComponent"]["intensity"].get_double().value();
		resourceSystem->loadTextureCube(environment);
		if (irradiance) {
			resourceSystem->loadTextureCube(irradiance);
		}
	}
	COMPONENT_SERIALIZE_OVERRIDE_IMPL(SkyAtmosphereComponent) {
		out["skyAtmosphereComponent"]["directionalLightActor"] = (uint64_t)Actor(directionalLightActor, level).getScopedID();
		out["skyAtmosphereComponent"]["sunIntensity"] = sunIntensity;
		out["skyAtmosphereComponent"]["height"] = height;
		out["skyAtmosphereComponent"]["atmosphereHeight"] = atmosphereHeight;
		out["skyAtmosphereComponent"]["rayleighScattering"] = rayleighScattering;
		out["skyAtmosphereComponent"]["mieScattering"] = mieScattering;
	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(SkyAtmosphereComponent) {
		directionalLightActor = level->getActorFromUUID(0, (UUID)data["skyAtmosphereComponent"]["directionalLightActor"].get_uint64());
		sunIntensity = data["skyAtmosphereComponent"]["sunIntensity"].get_double();
		height = data["skyAtmosphereComponent"]["height"].get_double();
		atmosphereHeight = data["skyAtmosphereComponent"]["atmosphereHeight"].get_double();
		rayleighScattering = data["skyAtmosphereComponent"]["rayleighScattering"].get_double();
		mieScattering =	data["skyAtmosphereComponent"]["mieScattering"].get_double();
	}
}