#pragma once

#include "../component.h"

namespace Shard3D {
	namespace Components {
		COMPONENT_STRUCT(PlanarReflectionComponent) {
			COMPONENT_TYPE(PlanarReflectionComponent, "planarReflectionComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();
			float intensity = 1.0f;
			glm::vec3 tint = { 1.f, 1.f, 1.f };
			VkFormat reflectionFormat = VK_FORMAT_R16G16B16A16_SFLOAT; // VK_FORMAT_R16G16B16A16_SFLOAT, VK_FORMAT_B10G11R11_UFLOAT, VK_FORMAT_R8G8B8A8_UNORM, VK_FORMAT_R5G6B5_UNORM
			float resolutionMultiplier = 0.25f;
			uint32_t reflectionShaderTexture_id;
			uint32_t irradianceShaderTexture_id;
			bool irradiance = false;
			float irradianceSampleRadius = 1.0 / 3.1415926f;

			float maxInfluenceHeight = 0.1f;
			float transitionDistance = 0.5f;
			float distortDuDvFactor = 20.0f;
			float distortionDeltaLimit = 0.1f;
			float clipOffset = 0.0f;
		};
	}
}