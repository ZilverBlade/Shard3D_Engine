#include "group_component.h"

namespace Shard3D::Components {
	COMPONENT_SERIALIZE_OVERRIDE_IMPL(ActorGroupComponent) {
		out["actorGroupComponent"]["tags"] = tags;
	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(ActorGroupComponent) {
		for (int i = 0; i < data["actorGroupComponent"]["tags"].get_array().size(); i++) {
			 tags.push_back(std::string(data["actorGroupComponent"]["tags"].get_array().at(i).get_string().value()));
		}
	}
}