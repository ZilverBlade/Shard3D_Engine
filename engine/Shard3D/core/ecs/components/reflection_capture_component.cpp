#include "reflection_capture_component.h"
#include "../../asset/assetmgr.h"
#include "../actor.h"

namespace Shard3D::Components {
	COMPONENT_SERIALIZE_OVERRIDE_IMPL(BoxReflectionCaptureComponent) {
		if (useAssetInsteadOfCapture) {
			assets.push_back(reflectionAsset.getAsset());
			out["boxReflectionCaptureComponent"]["textureCubeAsset"] = reflectionAsset.getAsset();
		}
		out["boxReflectionCaptureComponent"]["tint"] = tint;
		out["boxReflectionCaptureComponent"]["intensity"] = intensity;
		out["boxReflectionCaptureComponent"]["useAssetInsteadOfCapture"] = useAssetInsteadOfCapture;
		out["boxReflectionCaptureComponent"]["captureResolution"] = resolution;
	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(BoxReflectionCaptureComponent) {
		tint = SIMDJSON_READ_VEC3(data["boxReflectionCaptureComponent"]["tint"]);
		intensity = data["boxReflectionCaptureComponent"]["intensity"].get_double().value();
		useAssetInsteadOfCapture = data["boxReflectionCaptureComponent"]["useAssetInsteadOfCapture"].get_bool().value();
		resolution = data["boxReflectionCaptureComponent"]["captureResolution"].get_uint64().value();
		if (useAssetInsteadOfCapture) {
			reflectionAsset = std::string(data["boxReflectionCaptureComponent"]["textureCubeAsset"].get_string().value());
			resourceSystem->loadTextureCube(reflectionAsset);
		} else {
			reflectionAsset = AssetID(level->bakedDataDirectory.getAsset() + "/brfcpt_" + std::to_string(actor->getScopedID()) + ".s3dbak.s3dasset");
			if (std::filesystem::exists(reflectionAsset.getAbsolute()))
				resourceSystem->loadBakedReflectionCubeAsset(reflectionAsset);
			irradianceAsset = AssetID(level->bakedDataDirectory.getAsset() + "/brfcpt_" + std::to_string(actor->getScopedID()) + "_irr.s3dbak.s3dasset");
			if (std::filesystem::exists(irradianceAsset.getAbsolute())) {
				resourceSystem->loadBakedReflectionCubeAsset(irradianceAsset);
			} else {
				irradianceAsset = AssetID::null();
			}
		}
	}
	COMPONENT_SERIALIZE_OVERRIDE_IMPL(ParabolicReflectionCaptureComponent) {
		out["parabolicReflectionCaptureComponent"]["tint"] = tint;
		out["parabolicReflectionCaptureComponent"]["intensity"] = intensity;
		out["parabolicReflectionCaptureComponent"]["captureResolution"] = resolution;
	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(ParabolicReflectionCaptureComponent) {
		asset = std::string(data["boxReflectionCaptureComponent"]["textureCubeAsset"].get_string().value());
		tint = SIMDJSON_READ_VEC3(data["boxReflectionCaptureComponent"]["tint"]);
		intensity = data["boxReflectionCaptureComponent"]["intensity"].get_double().value();
		resolution = data["boxReflectionCaptureComponent"]["captureResolution"].get_uint64().value();
		asset = AssetID(level->bakedDataDirectory.getAsset() + "/brfcpt_" + std::to_string(actor->getScopedID()) + ".s3dbak.s3dasset");
		if (std::filesystem::exists(asset.getAbsolute()))
			resourceSystem->loadBakedReflectionCubeAsset(asset);
	}
}