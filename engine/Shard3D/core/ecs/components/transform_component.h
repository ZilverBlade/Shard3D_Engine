#pragma once

#include "../component.h"
#include <glm/gtc/matrix_transform.hpp>
namespace Shard3D {
	namespace Components {

		COMPONENT_STRUCT(TransformComponent) {
			COMPONENT_TYPE(TransformComponent, "transformComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();

			void setTranslation(glm::vec3 _t) { dirty = true; translation = _t; }
			void setRotation(glm::vec3 _r) { dirty = true; rotation = _r; }
			void setScale(glm::vec3 _s) { dirty = true; scale = _s; }

			glm::vec3 getTranslation() { return translation; }
			glm::vec3 getRotation() { return rotation; }
			glm::vec3 getScale() { return scale; }

			glm::vec3 getTranslationWorld() { return transformMatrix[3]; }
			//glm::vec3 getRotationWorld() { return glm::vec3(rotation.x, rotation.z, rotation.y); }
			glm::vec3 getScaleWorld() { return glm::vec3(glm::length(glm::vec3(transformMatrix[0])), glm::length(glm::vec3(transformMatrix[1])), glm::length(glm::vec3(transformMatrix[2]))); }

			glm::mat4 transformMatrix{ 1.f };
			glm::mat3 normalMatrix{ 1.f };

			glm::mat4 calculateMat4();
			glm::mat3 calculateNormalMatrix();

			inline glm::mat3 getRotationMatrix() {
				glm::mat3 rc3x3 = transformMatrix;
				rc3x3[0] /= glm::length(transformMatrix[0]);
				rc3x3[1] /= glm::length(transformMatrix[1]);
				rc3x3[2] /= glm::length(transformMatrix[2]);
				return rc3x3;
			}

			inline void recalculateMatrix() {
				transformMatrix = calculateMat4();
				normalMatrix = calculateNormalMatrix();
			}
	
			inline glm::mat4 getInverseTransform() {
				glm::mat4 invTransform = glm::mat4{ glm::transpose(normalMatrix) };
				return glm::translate(invTransform, glm::vec3(-transformMatrix[3]));
			}

			bool dirty = true;
		private:
			glm::vec3 translation{ 0.f, 0.f, 0.f };
			glm::vec3 rotation{ 0.f, 0.f, 0.f };
			glm::vec3 scale{ 1.f, 1.f, 1.f };
		};
	}
}