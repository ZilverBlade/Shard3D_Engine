#include "../component.h"

namespace Shard3D {
	namespace Components {

		COMPONENT_STRUCT(ActorGroupComponent) {
			COMPONENT_TYPE(ActorGroupComponent, "actorGroupComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();

			std::vector<std::string> tags = {};
		};
	}
}