#include "transform_component.h"
#include "../../math/transform.h"

namespace Shard3D::Components {
	COMPONENT_SERIALIZE_OVERRIDE_IMPL(TransformComponent) {
		out["transformComponent"]["translation"] = getTranslation();
		out["transformComponent"]["rotation"] = getRotation();
		out["transformComponent"]["scale"] = getScale();
	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(TransformComponent) {
		setTranslation(SIMDJSON_READ_VEC3(data["transformComponent"]["translation"]));
		setRotation(SIMDJSON_READ_VEC3(data["transformComponent"]["rotation"]));
		setScale(SIMDJSON_READ_VEC3(data["transformComponent"]["scale"]));
	}

	glm::mat4 TransformComponent::calculateMat4() {
		return TransformMath::YXZ(translation, rotation, scale);
	}

	glm::mat3 TransformComponent::calculateNormalMatrix() {
		const float c3 = glm::cos(rotation.z);
		const float s3 = glm::sin(rotation.z);
		const float c2 = glm::cos(rotation.x);
		const float s2 = glm::sin(rotation.x);
		const float c1 = glm::cos(rotation.y);
		const float s1 = glm::sin(rotation.y);

		const glm::vec3 invScale = 1.0f / scale;
		return glm::mat3{
			{
				invScale.x * (c1 * c3 + s1 * s2 * s3),
				invScale.x * (c2 * s3),
				invScale.x * (c1 * s2 * s3 - c3 * s1),
			},
			{
				invScale.y * (c3 * s1 * s2 - c1 * s3),
				invScale.y * (c2 * c3),
				invScale.y * (c1 * c3 * s2 + s1 * s3),
			},
			{
				invScale.z * (c2 * s1),
				invScale.z * (-s2),
				invScale.z * (c1 * c2),
			},
		};
	}

	
}