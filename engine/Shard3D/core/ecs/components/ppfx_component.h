#include "../component.h"
#include "../../rendering/camera.h"
namespace Shard3D {
	namespace Components {
		COMPONENT_STRUCT(PostFXComponent) {
			COMPONENT_TYPE(PostFXComponent, "postFXComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();
			PostFXData postfx{};
		};
	}
}