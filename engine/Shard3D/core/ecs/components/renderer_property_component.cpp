#include "renderer_property_component.h"

namespace Shard3D::Components {
	COMPONENT_SERIALIZE_OVERRIDE_IMPL(RendererPropertyComponent) {
		out["rendererPropertyComponent"]["includeActorGroups"] = includeActorGroups;
		out["rendererPropertyComponent"]["excludeActorGroups"] = excludeActorGroups;
	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(RendererPropertyComponent) {
		for (int i = 0; i < data["rendererPropertyComponent"]["includeActorGroups"].get_array().size(); i++) {
			includeActorGroups.push_back(std::string(data["rendererPropertyComponent"]["includeActorGroups"].get_array().at(i).get_string().value()));
		}
		for (int i = 0; i < data["rendererPropertyComponent"]["excludeActorGroups"].get_array().size(); i++) {
			excludeActorGroups.push_back(std::string(data["rendererPropertyComponent"]["excludeActorGroups"].get_array().at(i).get_string().value()));
		}
	}
}