#include "ppfx_component.h"
#include "../../asset/assetmgr.h"

namespace Shard3D::Components {
	COMPONENT_SERIALIZE_OVERRIDE_IMPL(PostFXComponent) {
		out["postFXComponent"]["hdr"]["exposure"] = postfx.hdr.exposure;
		out["postFXComponent"]["hdr"]["whiteLim"] = postfx.hdr.lim;
		out["postFXComponent"]["hdr"]["toneMappingAlgorithm"] = (int)postfx.hdr.toneMappingAlgorithm;
			 	 
		out["postFXComponent"]["colorGrading"]["contrast"] = postfx.grading.contrast;
		out["postFXComponent"]["colorGrading"]["saturation"] = postfx.grading.saturation;
		out["postFXComponent"]["colorGrading"]["gain"] = postfx.grading.gain;
		out["postFXComponent"]["colorGrading"]["temperature"] = postfx.grading.temperature;
		out["postFXComponent"]["colorGrading"]["hueShift"] = postfx.grading.hueShift;
		out["postFXComponent"]["colorGrading"]["shadows"] = postfx.grading.shadows;
		out["postFXComponent"]["colorGrading"]["midtones"] = postfx.grading.midtones;
		out["postFXComponent"]["colorGrading"]["highlights"] = postfx.grading.highlights;
		
		out["postFXComponent"]["bloom"]["threshold"] = postfx.bloom.threshold;
		out["postFXComponent"]["bloom"]["strength"] = postfx.bloom.strength;
		out["postFXComponent"]["bloom"]["knee"] = postfx.bloom.knee;
		out["postFXComponent"]["bloom"]["enable"] = postfx.bloom.enable;
			  	 
		out["postFXComponent"]["ssao"]["radius"] = postfx.ssao.radius;
		out["postFXComponent"]["ssao"]["intensity"] = postfx.ssao.intensity;
		out["postFXComponent"]["ssao"]["power"] = postfx.ssao.power;
		out["postFXComponent"]["ssao"]["bias"] = postfx.ssao.bias;
		out["postFXComponent"]["ssao"]["enable"] = postfx.ssao.enable;

		out["postFXComponent"]["motionBlur"]["targetFramerate"] = postfx.motionBlur.targetFramerate;
		out["postFXComponent"]["motionBlur"]["maxLength"] = postfx.motionBlur.maxLength;
		out["postFXComponent"]["motionBlur"]["enable"] = postfx.motionBlur.enable;

		out["postFXComponent"]["lightPropagationVolume"]["boost"] = postfx.lightPropagationVolume.boost;
		out["postFXComponent"]["lightPropagationVolume"]["extent"] = postfx.lightPropagationVolume.extent;
		out["postFXComponent"]["lightPropagationVolume"]["numCascades"] = postfx.lightPropagationVolume.numCascades;
		out["postFXComponent"]["lightPropagationVolume"]["numPropagations"] = postfx.lightPropagationVolume.numPropagations;
		out["postFXComponent"]["lightPropagationVolume"]["resolution"] = postfx.lightPropagationVolume.resolution;
		out["postFXComponent"]["lightPropagationVolume"]["fadeRatio"] = postfx.lightPropagationVolume.fadeRatio;
		out["postFXComponent"]["lightPropagationVolume"]["temporalBlend"] = postfx.lightPropagationVolume.temporalBlend;
		out["postFXComponent"]["lightPropagationVolume"]["fixed"] = postfx.lightPropagationVolume.fixed;
		out["postFXComponent"]["lightPropagationVolume"]["fixedPosition"] = postfx.lightPropagationVolume.fixedPosition;
		out["postFXComponent"]["lightPropagationVolume"]["enable"] = postfx.lightPropagationVolume.enable;

		out["postFXComponent"]["mist"]["mieScattering"] = postfx.mist.mieScattering;
		out["postFXComponent"]["mist"]["constantScattering"] = postfx.mist.constantScattering;
		out["postFXComponent"]["mist"]["tint"] = postfx.mist.tint;
		out["postFXComponent"]["mist"]["enable"] = postfx.mist.enable;
	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(PostFXComponent) {
		postfx.hdr.exposure = data["postFXComponent"]["hdr"]["exposure"].get_double().value();
		postfx.hdr.lim = data["postFXComponent"]["hdr"]["whiteLim"].get_double().value();
		postfx.hdr.toneMappingAlgorithm = (PostFXData::HDR::ToneMappingAlgorithm)data["postFXComponent"]["hdr"]["toneMappingAlgorithm"].get_int64().value();

		postfx.grading.contrast = data["postFXComponent"]["colorGrading"]["contrast"].get_double().value();
		postfx.grading.saturation = data["postFXComponent"]["colorGrading"]["saturation"].get_double().value();
		postfx.grading.gain = data["postFXComponent"]["colorGrading"]["gain"].get_double().value();
		postfx.grading.temperature = data["postFXComponent"]["colorGrading"]["temperature"].get_double().value();
		postfx.grading.hueShift = data["postFXComponent"]["colorGrading"]["hueShift"].get_double().value();
		if (data["postFXComponent"]["colorGrading"]["shadows"].error() == simdjson::SUCCESS) {
			postfx.grading.shadows = SIMDJSON_READ_VEC3(data["postFXComponent"]["colorGrading"]["shadows"]);
		}
		if (data["postFXComponent"]["colorGrading"]["midtones"].error() == simdjson::SUCCESS) {
			postfx.grading.midtones = SIMDJSON_READ_VEC3(data["postFXComponent"]["colorGrading"]["midtones"]);
		}
		if (data["postFXComponent"]["colorGrading"]["highlights"].error() == simdjson::SUCCESS) {
			postfx.grading.highlights = SIMDJSON_READ_VEC3(data["postFXComponent"]["colorGrading"]["highlights"]);
		}


		postfx.bloom.threshold = data["postFXComponent"]["bloom"]["threshold"].get_double().value();
		postfx.bloom.strength = data["postFXComponent"]["bloom"]["strength"].get_double().value();
		if (data["postFXComponent"]["bloom"]["kernelSizeCoeficient"].error() == simdjson::SUCCESS) { // tech debt name
			postfx.bloom.knee = data["postFXComponent"]["bloom"]["kernelSizeCoeficient"].get_double().value();
		} else {
			postfx.bloom.knee = data["postFXComponent"]["bloom"]["knee"].get_double().value();
		}
		if (data["postFXComponent"]["bloom"]["enable"].error() == simdjson::SUCCESS) { 
			postfx.bloom.enable = data["postFXComponent"]["bloom"]["enable"].get_bool().value();
		}

		postfx.ssao.radius = data["postFXComponent"]["ssao"]["radius"].get_double().value();
		postfx.ssao.intensity = data["postFXComponent"]["ssao"]["intensity"].get_double().value();
		postfx.ssao.power = data["postFXComponent"]["ssao"]["power"].get_double().value();
		postfx.ssao.bias = data["postFXComponent"]["ssao"]["bias"].get_double().value();
		if (data["postFXComponent"]["ssao"]["enable"].error() == simdjson::SUCCESS) {
			postfx.ssao.enable = data["postFXComponent"]["ssao"]["enable"].get_bool().value();
		}

		if (data["postFXComponent"]["lightPropagationVolume"].error() == simdjson::SUCCESS) {
			postfx.lightPropagationVolume.boost = SIMDJSON_READ_VEC4(data["postFXComponent"]["lightPropagationVolume"]["boost"]);
			postfx.lightPropagationVolume.extent = SIMDJSON_READ_VEC3(data["postFXComponent"]["lightPropagationVolume"]["extent"]);
			postfx.lightPropagationVolume.numCascades = data["postFXComponent"]["lightPropagationVolume"]["numCascades"].get_uint64();
			postfx.lightPropagationVolume.numPropagations = data["postFXComponent"]["lightPropagationVolume"]["numPropagations"].get_uint64();
			postfx.lightPropagationVolume.resolution = SIMDJSON_READ_IVEC3(data["postFXComponent"]["lightPropagationVolume"]["resolution"]);
			if (data["postFXComponent"]["lightPropagationVolume"]["fadeRatio"].error() == simdjson::SUCCESS) {
				postfx.lightPropagationVolume.fadeRatio = data["postFXComponent"]["lightPropagationVolume"]["fadeRatio"].get_double().value();
			}
			if (data["postFXComponent"]["lightPropagationVolume"]["fixed"].error() == simdjson::SUCCESS) {
				postfx.lightPropagationVolume.fixed = data["postFXComponent"]["lightPropagationVolume"]["fixed"].get_bool().value();
			}
			if (data["postFXComponent"]["lightPropagationVolume"]["fixedPosition"].error() == simdjson::SUCCESS) {
				postfx.lightPropagationVolume.fixedPosition = SIMDJSON_READ_VEC3(data["postFXComponent"]["lightPropagationVolume"]["fixedPosition"]);
			}
			postfx.lightPropagationVolume.temporalBlend = data["postFXComponent"]["lightPropagationVolume"]["temporalBlend"].get_double().value();
			postfx.lightPropagationVolume.enable = data["postFXComponent"]["lightPropagationVolume"]["enable"].get_bool().value();
		}

		if (data["postFXComponent"]["motionBlur"]["enable"].error() == simdjson::SUCCESS) {
			postfx.motionBlur.enable = data["postFXComponent"]["motionBlur"]["enable"].get_bool().value();
			postfx.motionBlur.targetFramerate = data["postFXComponent"]["motionBlur"]["targetFramerate"].get_double().value();
			postfx.motionBlur.maxLength = data["postFXComponent"]["motionBlur"]["maxLength"].get_int64().value();
		}
		if (data["postFXComponent"]["mist"].error() == simdjson::SUCCESS) {
			postfx.mist.mieScattering = data["postFXComponent"]["mist"]["mieScattering"].get_double().value();
			postfx.mist.constantScattering = data["postFXComponent"]["mist"]["constantScattering"].get_double().value();
			postfx.mist.tint = SIMDJSON_READ_VEC3(data["postFXComponent"]["mist"]["tint"]);
			postfx.mist.enable = data["postFXComponent"]["mist"]["enable"].get_bool().value();
		}
	}
}