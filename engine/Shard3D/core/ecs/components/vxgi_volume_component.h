#pragma once

#include "../component.h"

namespace Shard3D {
	namespace Components {
		NON_SERIALIZABLE_COMPONENT_STRUCT(VoxelGlobalIlluminationVolumeComponent) {
			glm::vec3 energyConservationFactor = glm::vec3(1.0);
			float rayMarchThresholdrRadius = 0.5f;
			glm::ivec3 resolution = {128, 128, 128};
			bool computeWithRayTracingPipeline = false; // currently only voxels
			VkFormat voxelFormat = VK_FORMAT_B10G11R11_UFLOAT_PACK32; // VK_FORMAT_B10G11R11_UFLOAT_PACK32 (better visuals) or VK_FORMAT_R16_SFLOAT (lower memory)
			uint32_t samples = 1024;
			uint32_t maxBounces = 2;
		};
	}
}