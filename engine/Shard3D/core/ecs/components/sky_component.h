#pragma once

#include "../component.h"

namespace Shard3D {
	namespace Components {
		COMPONENT_STRUCT(SkyLightComponent) {
			COMPONENT_TYPE(SkyLightComponent, "skyLightComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();
			glm::vec3 skyTint{ 1.0f, 1.0f, 1.0f };
			float skyIntensity = 1.0f;
			entt::entity skyboxActor;
			entt::entity skyAtmosphereActor;
			bool reflectRayleighScattering = false; // todo: implement, on the cpu??
			int reflectionType = 0; // 0 = skybox, 1 = sky atmospheres
			uint32_t skyAtmosphereReflectionResolution = 512;
			uint32_t skyAtmosphereReflectionUpdateRate = 6; // faces per frame, (values below 6 require twice the amount of memory!)
			uint32_t reflectionCubemap_id = 0;
			uint32_t irradianceCubemap_id = 0;
		};
		COMPONENT_STRUCT(SkyboxComponent) {
			COMPONENT_TYPE(SkyboxComponent, "skyboxComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();
			AssetID environment = AssetID("engine/textures/cubemaps/sky0.s3dasset"); // skybox
			AssetID irradiance = AssetID("engine/textures/cubemaps/blank_cube.s3dasset"); // irradiance of that skybox
			glm::vec3 tint{ 1.f };
			float intensity = 2.f;
			float rotation = 0.f;
		};

		COMPONENT_STRUCT(SkyAtmosphereComponent) {
			COMPONENT_TYPE(SkyAtmosphereComponent, "skyAtmosphereComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();
			entt::entity directionalLightActor;
			float sunIntensity = 15.0f;
			float height = 6371.f;
			float atmosphereHeight = 6530.f;
			float rotation = 0.f;
			float rayleighScattering = .0025f;
			float mieScattering = .0010f;
		};
		
	}
}