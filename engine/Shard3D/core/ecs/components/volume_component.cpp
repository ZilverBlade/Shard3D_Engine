#include "volume_component.h"
namespace Shard3D::Components {
	COMPONENT_SERIALIZE_OVERRIDE_IMPL(BoxVolumeComponent) {
		out["boxVolumeComponent"]["bounds"] = bounds;
		out["boxVolumeComponent"]["transitionDistance"] = transitionDistance;

	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(BoxVolumeComponent) {
		bounds = SIMDJSON_READ_VEC3(data["boxVolumeComponent"]["bounds"]);
		transitionDistance = data["boxVolumeComponent"]["transitionDistance"].get_double().value();
	}

	COMPONENT_SERIALIZE_OVERRIDE_IMPL(SphereVolumeComponent) {
		out["sphereVolumeComponent"]["radius"] = radius;
		out["sphereVolumeComponent"]["transitionDistance"] = transitionDistance;

	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(SphereVolumeComponent) {
		radius = data["sphereVolumeComponent"]["radius"].get_double().value();
		transitionDistance = data["sphereVolumeComponent"]["transitionDistance"].get_double().value();
	}
}