#pragma once

#include "../component.h"

namespace Shard3D {
	namespace Components {
		COMPONENT_STRUCT(BoxReflectionCaptureComponent) {
			COMPONENT_TYPE(BoxReflectionCaptureComponent, "boxReflectionCaptureComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();
			AssetID reflectionAsset{ AssetID::null() };
			AssetID irradianceAsset{ AssetID::null() };
			float intensity = 1.0f;
			glm::vec3 tint = { 1.f, 1.f, 1.f };
			bool useAssetInsteadOfCapture = false;
			int resolution = 256;

			// future settings
			std::vector<std::string> ignoreActorGroups;
			bool stationaryRenderDirectionalLight;
			bool stationaryRenderDirectionalLightShadow;
			bool stationaryRenderSpotLight;
			bool stationaryRenderSpotLightShadow;
			bool stationaryRenderPointLight;
			bool stationaryRenderPointLightShadow;

			int facesUpdatedPerFrame = 2; // 1 - 6, dynamic and stationary settings only
		};
		COMPONENT_STRUCT(ParabolicReflectionCaptureComponent) {
			COMPONENT_TYPE(ParabolicReflectionCaptureComponent, "parabolicReflectionCaptureComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();
			AssetID asset{ AssetID::null() };
			float intensity = 1.0f;
			glm::vec3 tint = { 1.f, 1.f, 1.f };
			int resolution = 256;
		};
		NON_SERIALIZABLE_COMPONENT_STRUCT(DynamicReflectionCaptureComponent) {
			NON_SERIALIZABLE_COMPONENT_TYPE(DynamicReflectionCaptureComponent);

		};
	}
}