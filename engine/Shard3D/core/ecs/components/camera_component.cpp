#include "camera_component.h"
#include "../../asset/assetmgr.h"

namespace Shard3D::Components {
	COMPONENT_SERIALIZE_OVERRIDE_IMPL(CameraComponent) {
		out["cameraComponent"]["projectionType"] = (int)projectionType;
		out["cameraComponent"]["fov"] = fov;
		out["cameraComponent"]["nearClip"] = nearClip;
		out["cameraComponent"]["farClip"] = farClip;

		// post processing is now stored in a dedicated component
	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(CameraComponent) {
		projectionType = (ProjectionType)data["cameraComponent"]["projectionType"].get_int64().value();
		fov = data["cameraComponent"]["fov"].get_double().value();
		nearClip = data["cameraComponent"]["nearClip"].get_double().value();
		farClip = data["cameraComponent"]["farClip"].get_double().value();
	}
}