#include "fog_component.h"

namespace Shard3D::Components {
	COMPONENT_SERIALIZE_OVERRIDE_IMPL(ExponentialFogComponent) {
		out["exponentialFogComponent"]["color"] = color;
		out["exponentialFogComponent"]["strength"]= strength;
		out["exponentialFogComponent"]["density"] = density;
		out["exponentialFogComponent"]["falloff"] = falloff;
		out["exponentialFogComponent"]["sunScatteringPow"] = sunScatteringPow;

		std::string ddb{};
		switch (upDownBias) {// 0 = up, 1 = down, 2 = both
		case(0): ddb = "Up"; break;
		case(1): ddb = "Down"; break;
		case(2): ddb = "Omni"; break;
		}
		out["exponentialFogComponent"]["densityDirectionBias"] = ddb;

	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(ExponentialFogComponent) {
		color = SIMDJSON_READ_VEC3(data["exponentialFogComponent"]["color"]);
		strength = data["exponentialFogComponent"]["strength"].get_double().value();
		density = data["exponentialFogComponent"]["density"].get_double().value();
		falloff = data["exponentialFogComponent"]["falloff"].get_double().value();
		sunScatteringPow = data["exponentialFogComponent"]["sunScatteringPow"].get_double().value();

		std::string_view ddb = data["exponentialFogComponent"]["densityDirectionBias"].get_string().value();
		if (ddb == "Up") upDownBias = 0;
		if (ddb == "Down") upDownBias = 1;
		if (ddb == "Omni") upDownBias = 2;
	}
}