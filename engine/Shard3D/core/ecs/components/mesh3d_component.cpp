#include "mesh3d_component.h"
#include "../level.h"
#include "../actor.h"
#include "../../asset/assetmgr.h"
#include "../../../systems/handlers/render_list.h"

namespace Shard3D::Components {

	Mesh3DComponent::Mesh3DComponent(ResourceSystem* resourceSystem, AssetID mdl) : asset(mdl) {
		Mesh3D* model = resourceSystem->retrieveMesh(mdl);
		materials = model->materials;
	}
	void Mesh3DComponent::validate(ResourceSystem* resourceSystem) {
		Mesh3D* model = resourceSystem->retrieveMesh(asset);
		if (model->materialSlots.size() != materials.size()) {
			SHARD3D_WARN("Mesh3DComponent validation failed! Resetting materials...");
			materials = model->materials;
		}
	}

	const std::vector<AssetID>& Mesh3DComponent::getMaterials() {
		return materials;
	}
	void Mesh3DComponent::setMaterials(const std::vector<AssetID>& materials) {
		std::copy(materials.begin(), materials.end(), this->materials.begin());
		dirty = true;
	}
	void  Mesh3DComponent::setMaterial(AssetID material, size_t index) {
		materials[index] = material;
		dirty = true;
	}


	COMPONENT_SERIALIZE_OVERRIDE_IMPL(Mesh3DComponent) {
		assets.push_back(asset.getAsset());
		for (auto& material : materials) {
			assets.push_back(material.getAsset());
		}
		out["mesh3DComponent"]["asset"] = asset.getAsset();
		out["mesh3DComponent"]["materials"] = materials;
		out["mesh3DComponent"]["castShadows"] = castShadows;
		out["mesh3DComponent"]["receiveDecals"] = receiveDecals;
		out["mesh3DComponent"]["depthBias"] = 0.0f;
	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(Mesh3DComponent) {
		resourceSystem->loadMesh(
			std::string(data["mesh3DComponent"]["asset"].get_string().value())
		);
		castShadows = data["mesh3DComponent"]["castShadows"].get_bool().value();
		if (data["mesh3DComponent"]["receiveDecals"].error() == simdjson::SUCCESS) {
			receiveDecals = data["mesh3DComponent"]["receiveDecals"].get_bool().value();
		}
		asset = AssetID(std::string(data["mesh3DComponent"]["asset"].get_string().value()));
		materials.clear();
		for (int i = 0; i < data["mesh3DComponent"]["materials"].get_array().size(); i++) {
			materials.push_back(AssetID(std::string(data["mesh3DComponent"]["materials"].get_array().at(i).get_string().value())));
		}

		validate(resourceSystem);
		for (auto& material : materials) {
			resourceSystem->loadMaterialRecursive(material);
		}
	}
	
}