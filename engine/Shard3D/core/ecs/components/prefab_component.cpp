#include "prefab_component.h"
#include "../../asset/assetmgr.h"
#include "../../../systems/handlers/prefab_manager.h"
#include "../prefab.h"

namespace Shard3D::Components {
	COMPONENT_SERIALIZE_OVERRIDE_IMPL(PrefabComponent) {
		assets.push_back(prefab.getAsset());
		out["prefabComponent"]["prefabAsset"] = prefab.getAsset();
	}

	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(PrefabComponent) {
		prefab = std::string(data["prefabComponent"]["prefabAsset"].get_string().value());
		level->getPrefabManager()->cachePrefab(prefab);
	}
}