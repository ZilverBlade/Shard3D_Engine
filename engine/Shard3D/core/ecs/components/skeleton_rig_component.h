#pragma once

#include "../component.h"
#include "../../../core/asset/assetid.h"
#include "../../math/transform.h"
namespace Shard3D {
	class ResourceSystem;
	namespace Components {
		COMPONENT_STRUCT(SkeletonRigComponent) {
			COMPONENT_TYPE(SkeletonRigComponent, "skeletonRigComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();
			SkeletonRigComponent(ResourceSystem* resourceSystem, AssetID skl);

			AssetID asset{ AssetID::null() };

			bool enableBones = false;
			std::vector<QuatTransform> boneTransforms;
			bool enableMorphTargets = false;
			std::vector<float> morphTargetWeights;
		};
	}
}