#include "../component.h"

namespace Shard3D {
	namespace Components {

		COMPONENT_STRUCT(ExponentialFogComponent) {
			COMPONENT_TYPE(ExponentialFogComponent, "exponentialFogComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();

			//https://iquilezles.org/articles/fog/

			glm::vec3 color{ 0.7594f, 0.8235f, 1.0000f }; 
			float strength = 0.05f;
			float density = 0.01f;
			float falloff = 0.38f;
			float sunScatteringPow = 0.80f;
			int upDownBias = 1; // 0 = up, 1 = down, 2 = both
		};
		COMPONENT_STRUCT(AreaFogComponent) {
			COMPONENT_TYPE(AreaFogComponent, "areaFogComponent");
			//https://developer.download.nvidia.com/CgTutorial/cg_tutorial_chapter09.html chapter 9.1.5 and https://vicrucann.github.io/tutorials/osg-shader-fog/
		};
		COMPONENT_STRUCT(DistanceFieldMistComponent) {
			COMPONENT_TYPE(DistanceFieldMistComponent, "DistanceFieldMistComponent");
			// allows for foggy lights with optional volumetrics
		};
	}
}