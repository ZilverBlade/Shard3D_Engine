#pragma once

#include "../component.h"
#include "../../../systems/computational/particle_system.h"

namespace Shard3D {
	namespace Components {
		NON_SERIALIZABLE_COMPONENT_STRUCT(ParticleComponent) {

			NON_SERIALIZABLE_COMPONENT_TYPE(ParticleComponent);
			ParticleProperties particleTemplate;
			uint16_t maxParticles;
		};


	}
}