#include "../component.h"
#include "../../rendering/camera.h"
namespace Shard3D {

	enum class ProjectionType {
		Orthographic = 0,
		Perspective = 1
	};
	namespace Components {
		COMPONENT_STRUCT(CameraComponent) {
			COMPONENT_TYPE(CameraComponent, "cameraComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();

			S3DCamera camera{};

			float ar = 1.f;

			operator S3DCamera& () {
				return camera;
			}
			void setProjection(bool reverseDepth, bool infiniteFarZ) {
				if (projectionType == ProjectionType::Orthographic)
					camera.setOrthographicProjection(-ar, ar, -1, 1, nearClip, farClip, reverseDepth);
				else
					camera.setPerspectiveProjection(glm::radians(fov), ar, nearClip, farClip, reverseDepth, infiniteFarZ);
			}
			float fov = 70.f;
			float nearClip = 0.05f;
			float farClip = 1024.f;
			ProjectionType projectionType = ProjectionType::Perspective;
		};
	}

}