#include "decal_component.h"
#include "../../../systems/handlers/resource_system.h"

namespace Shard3D::Components {
	COMPONENT_SERIALIZE_OVERRIDE_IMPL(DeferredDecalComponent) {
		out["deferredDecalComponent"]["materialAsset"] = material.getAsset();
		out["deferredDecalComponent"]["priority"] = static_cast<uint32_t>(priority);
		out["deferredDecalComponent"]["opacity"] = opacity;
		assets.push_back(material.getAsset());
	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(DeferredDecalComponent) {
		material = std::string(data["deferredDecalComponent"]["materialAsset"].get_string().value());
		priority = data["deferredDecalComponent"]["priority"].get_uint64().value();
		opacity = data["deferredDecalComponent"]["opacity"].get_double().value();
		resourceSystem->loadMaterial(material);
	}
}