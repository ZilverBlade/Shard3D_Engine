#include "virtual_point_light_component.h"
#include "../../asset/assetmgr.h"
#include <glm/gtc/type_ptr.hpp>

namespace Shard3D::Components {
	COMPONENT_SERIALIZE_OVERRIDE_IMPL(VirtualPointLightComponent) {
		out["virtualPointLightComponent"]["emission"] = glm::vec4(color, lightIntensity);
	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(VirtualPointLightComponent) {
		glm::vec4 colorIntensity = SIMDJSON_READ_VEC4(data["virtualPointLightComponent"]["emission"]);
		color = glm::vec3(colorIntensity);
		lightIntensity = colorIntensity.w;
	}
}