#include "../component.h"

namespace Shard3D {
	namespace Components {

		COMPONENT_STRUCT(RendererPropertyComponent) {
			COMPONENT_TYPE(RendererPropertyComponent, "rendererPropertyComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();

			bool renderMesh3D = true;
			bool renderMesh3DDynamic = true;
			bool renderMesh3DStationary = true;
			bool renderMesh3DStatic = true;

			std::vector<std::string> includeActorGroups;
			std::vector<std::string> excludeActorGroups;

			bool renderTerrain = true;
			bool renderParticles = true;

			float cullDistance = 0.0;
			bool cullLengthFromPrimitiveCenter = true;
		};
	}
}