#include "planar_reflection_component.h"
#include "../../asset/assetmgr.h"
#include "../actor.h"

namespace Shard3D::Components {
	COMPONENT_SERIALIZE_OVERRIDE_IMPL(PlanarReflectionComponent) {
		out["planarReflectionComponent"]["tint"] = tint;
		out["planarReflectionComponent"]["intensity"] = intensity;
		out["planarReflectionComponent"]["resolutionMultiplier"] = resolutionMultiplier;
		out["planarReflectionComponent"]["format"] = string_VkFormat(reflectionFormat);
		out["planarReflectionComponent"]["clipOffset"] = clipOffset;
		out["planarReflectionComponent"]["maxInfluenceHeight"] = maxInfluenceHeight;
		out["planarReflectionComponent"]["transitionDistance"] = transitionDistance;
		out["planarReflectionComponent"]["distortDuDvFactor"] = distortDuDvFactor;
		out["planarReflectionComponent"]["distortionDeltaLimit"] = distortionDeltaLimit;
		out["planarReflectionComponent"]["enableSpecularGraze"] = false;
		out["planarReflectionComponent"]["specularGrazeBaseBlur"] = 0.0;
		out["planarReflectionComponent"]["specularGrazeDistanceFactor"] = 10.0;
		out["planarReflectionComponent"]["enableIrradiance"] = irradiance;
		out["planarReflectionComponent"]["irradianceSampleRadius"] = irradianceSampleRadius;
	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(PlanarReflectionComponent) {
		tint = SIMDJSON_READ_VEC3(data["planarReflectionComponent"]["tint"]);
		intensity = data["planarReflectionComponent"]["intensity"].get_double().value();
		resolutionMultiplier = data["planarReflectionComponent"]["resolutionMultiplier"].get_double().value();
		reflectionFormat = enum_VkFormat(std::string(data["planarReflectionComponent"]["format"].get_string().value()));
		clipOffset = data["planarReflectionComponent"]["clipOffset"].get_double().value();
		maxInfluenceHeight = data["planarReflectionComponent"]["maxInfluenceHeight"].get_double().value();
		transitionDistance = data["planarReflectionComponent"]["transitionDistance"].get_double().value();
		distortDuDvFactor = data["planarReflectionComponent"]["distortDuDvFactor"].get_double().value();
		distortionDeltaLimit = data["planarReflectionComponent"]["distortionDeltaLimit"].get_double().value();

		if (data["planarReflectionComponent"]["enableIrradiance"].error() == simdjson::SUCCESS) {
			irradiance = data["planarReflectionComponent"]["enableIrradiance"].get_bool().value();
			irradianceSampleRadius = data["planarReflectionComponent"]["irradianceSampleRadius"].get_double().value();
		}
	}
}