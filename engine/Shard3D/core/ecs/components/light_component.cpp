#include "light_component.h"
#include "../../../systems/handlers/resource_system.h"
namespace Shard3D::Components {
		COMPONENT_SERIALIZE_OVERRIDE_IMPL(PointLightComponent) {
			out["pointLightComponent"]["emission"] = glm::vec4(color, lightIntensity);
			out["pointLightComponent"]["attenuationRadius"] = lightRadius;
			out["pointLightComponent"]["sourceRadius"] = sourceRadius;
			out["pointLightComponent"]["sourceLength"] = sourceLength;
			out["pointLightComponent"]["castShadows"] = castShadows;
			out["pointLightComponent"]["castTranslucentShadows"] = castTranslucentShadows;
			out["pointLightComponent"]["volumetric"] = volumetric;
			out["pointLightComponent"]["volumetricTranslucency"] = volumetricTranslucency;
			out["pointLightComponent"]["shadowMap"]["resolution"] = shadowMapResolution;
			out["pointLightComponent"]["shadowMap"]["convention"] = shadowMapConvention;
			out["pointLightComponent"]["shadowMap"]["sharpness"] = shadowMapSharpness;
			out["pointLightComponent"]["shadowMap"]["pcfBias"] = shadowMapBias;
			out["pointLightComponent"]["shadowMap"]["pcfDepthSlopeBiasFactor"] = shadowMapSlopeBiasFactor;
			out["pointLightComponent"]["shadowMap"]["poissonRadius"] = shadowMapPoissonRadius;
			out["pointLightComponent"]["shadowMap"]["poissonSamples"] = shadowMapPoissonSamples;
		}
		COMPONENT_DESERIALIZE_OVERRIDE_IMPL(PointLightComponent) {
			glm::vec4 colorIntensity = SIMDJSON_READ_VEC4(data["pointLightComponent"]["emission"]);
			if (data["pointLightComponent"]["attenuationRadius"].error() == simdjson::SUCCESS) {
				lightRadius = data["pointLightComponent"]["attenuationRadius"].value().get_double();
				sourceRadius = data["pointLightComponent"]["sourceRadius"].value().get_double();
				sourceLength = data["pointLightComponent"]["sourceLength"].value().get_double();
			}
			color = glm::vec3(colorIntensity);
			lightIntensity = colorIntensity.w;
			castShadows = data["pointLightComponent"]["castShadows"].get_bool().value();
			castTranslucentShadows = data["pointLightComponent"]["castTranslucentShadows"].get_bool().value();
			if (data["pointLightComponent"]["volumetric"].error() == simdjson::SUCCESS)
				volumetric = data["pointLightComponent"]["volumetric"].get_bool().value();
			if (data["pointLightComponent"]["volumetricTranslucency"].error() == simdjson::SUCCESS)
				volumetricTranslucency = data["pointLightComponent"]["volumetricTranslucency"].get_bool().value();
			shadowMapResolution = data["pointLightComponent"]["shadowMap"]["resolution"].get_uint64().value();
			if (data["pointLightComponent"]["shadowMap"]["convention"].error() == simdjson::SUCCESS)
				shadowMapConvention = data["pointLightComponent"]["shadowMap"]["convention"].get_uint64().value();

			shadowMapSharpness = data["pointLightComponent"]["shadowMap"]["sharpness"].get_double().value();
			if (ProjectSystem::getEngineSettings().shading.pointShadowMapType == ESET::ShadowMap::PCF) {
				if (data["pointLightComponent"]["shadowMap"]["pcfBias"].error() == simdjson::SUCCESS)
					shadowMapBias = data["pointLightComponent"]["shadowMap"]["pcfBias"].get_double().value();
				if (data["pointLightComponent"]["shadowMap"]["pcfDepthSlopeBiasFactor"].error() == simdjson::SUCCESS)
					shadowMapSlopeBiasFactor = data["pointLightComponent"]["shadowMap"]["pcfDepthSlopeBiasFactor"].get_double().value();
				if (data["pointLightComponent"]["shadowMap"]["poissonRadius"].error() == simdjson::SUCCESS)
					shadowMapPoissonRadius = data["pointLightComponent"]["shadowMap"]["poissonRadius"].get_double().value();
				if (data["pointLightComponent"]["shadowMap"]["poissonSamples"].error() == simdjson::SUCCESS)
					shadowMapPoissonSamples = data["pointLightComponent"]["shadowMap"]["poissonSamples"].get_uint64().value();
			} else if (ProjectSystem::getEngineSettings().shading.pointShadowMapType == ESET::ShadowMap::Variance)
				if (data["pointLightComponent"]["shadowMap"]["varianceBias"].error() == simdjson::SUCCESS)
					shadowMapBias = data["pointLightComponent"]["shadowMap"]["varianceBias"].get_double().value();
		}

		COMPONENT_SERIALIZE_OVERRIDE_IMPL(SpotLightComponent) {
			out["spotLightComponent"]["emission"] = glm::vec4(color, lightIntensity);
			out["spotLightComponent"]["attenuationRadius"] = lightRadius;
			out["spotLightComponent"]["sourceRadius"] = sourceRadius;
			out["spotLightComponent"]["cosAngle"] = glm::vec2(innerAngle, outerAngle);
			out["spotLightComponent"]["hasCookie"] = hasCookie;
			out["spotLightComponent"]["cookieTexture"] = cookieTexture.getAsset();
			out["spotLightComponent"]["indirectLighting"] = indirectLighting;
			out["spotLightComponent"]["castShadows"] = castShadows;
			out["spotLightComponent"]["castTranslucentShadows"] = castTranslucentShadows;
			out["spotLightComponent"]["volumetric"] = volumetric;
			out["spotLightComponent"]["volumetricTranslucency"] = volumetricTranslucency;
			out["spotLightComponent"]["shadowMap"]["resolution"] = shadowMapResolution;
			out["spotLightComponent"]["shadowMap"]["sharpness"] = shadowMapSharpness;
			out["spotLightComponent"]["shadowMap"]["convention"] = shadowMapConvention;
			if (ProjectSystem::getEngineSettings().shading.spotShadowMapType == ESET::ShadowMap::PCF) {
				out["spotLightComponent"]["shadowMap"]["pcfBias"] = shadowMapBias;
				out["spotLightComponent"]["shadowMap"]["pcfDepthSlopeBiasFactor"] = shadowMapSlopeBiasFactor;
				out["spotLightComponent"]["shadowMap"]["poissonRadius"] = shadowMapPoissonRadius;
				out["spotLightComponent"]["shadowMap"]["poissonSamples"] = shadowMapPoissonSamples;
			} else if (ProjectSystem::getEngineSettings().shading.spotShadowMapType == ESET::ShadowMap::Variance)
				out["spotLightComponent"]["shadowMap"]["varianceBias"] = shadowMapBias;
		}

		COMPONENT_DESERIALIZE_OVERRIDE_IMPL(SpotLightComponent) {
			glm::vec4 colorIntensity = SIMDJSON_READ_VEC4(data["spotLightComponent"]["emission"]);
			color = glm::vec3(colorIntensity);
			lightIntensity = colorIntensity.w;
			if (data["spotLightComponent"]["attenuationRadius"].error() == simdjson::SUCCESS) {
				lightRadius = data["spotLightComponent"]["attenuationRadius"].value().get_double();
				sourceRadius = data["spotLightComponent"]["sourceRadius"].value().get_double();
			}
			glm::vec2 ioCosAngle = SIMDJSON_READ_VEC2(data["spotLightComponent"]["cosAngle"]);
			innerAngle = ioCosAngle.x;
			outerAngle = ioCosAngle.y;
			castShadows = data["spotLightComponent"]["castShadows"].get_bool().value();
			castTranslucentShadows = data["spotLightComponent"]["castTranslucentShadows"].get_bool().value();
			if (data["spotLightComponent"]["indirectLighting"].error() == simdjson::SUCCESS)
				indirectLighting = data["spotLightComponent"]["indirectLighting"].get_bool().value();
			if (data["spotLightComponent"]["volumetric"].error() == simdjson::SUCCESS)
				volumetric = data["spotLightComponent"]["volumetric"].get_bool().value();
			if (data["spotLightComponent"]["volumetricTranslucency"].error() == simdjson::SUCCESS)
				volumetricTranslucency = data["spotLightComponent"]["volumetricTranslucency"].get_bool().value();
			shadowMapResolution = data["spotLightComponent"]["shadowMap"]["resolution"].get_uint64().value();
			if (data["spotLightComponent"]["shadowMap"]["convention"].error() == simdjson::SUCCESS)
				shadowMapConvention = data["spotLightComponent"]["shadowMap"]["convention"].get_uint64().value();

			if (data["spotLightComponent"]["hasCookie"].error() == simdjson::SUCCESS) {
				hasCookie = data["spotLightComponent"]["hasCookie"].get_bool().value();
				if (hasCookie) {
					cookieTexture = std::string(data["spotLightComponent"]["cookieTexture"].get_string().value());
				}
				resourceSystem->loadTexture(cookieTexture);
			}
			if (data["spotLightComponent"]["shadowMap"]["sharpness"].error() == simdjson::SUCCESS)
				shadowMapSharpness = data["spotLightComponent"]["shadowMap"]["sharpness"].get_double().value();
			if (ProjectSystem::getEngineSettings().shading.spotShadowMapType == ESET::ShadowMap::PCF) {
				if (data["spotLightComponent"]["shadowMap"]["pcfBias"].error() == simdjson::SUCCESS)
					shadowMapBias = data["spotLightComponent"]["shadowMap"]["pcfBias"].get_double().value();
				if (data["spotLightComponent"]["shadowMap"]["pcfDepthSlopeBiasFactor"].error() == simdjson::SUCCESS)
					shadowMapSlopeBiasFactor = data["spotLightComponent"]["shadowMap"]["pcfDepthSlopeBiasFactor"].get_double().value();
				if (data["spotLightComponent"]["shadowMap"]["poissonRadius"].error() == simdjson::SUCCESS)
					shadowMapPoissonRadius = data["spotLightComponent"]["shadowMap"]["poissonRadius"].get_double().value();
				if (data["spotLightComponent"]["shadowMap"]["poissonSamples"].error() == simdjson::SUCCESS)
					shadowMapPoissonSamples = data["spotLightComponent"]["shadowMap"]["poissonSamples"].get_uint64().value();
			} else if (ProjectSystem::getEngineSettings().shading.spotShadowMapType == ESET::ShadowMap::Variance)
				if (data["spotLightComponent"]["shadowMap"]["varianceBias"].error() == simdjson::SUCCESS)
					shadowMapBias = data["spotLightComponent"]["shadowMap"]["varianceBias"].get_double().value();
		}

		COMPONENT_SERIALIZE_OVERRIDE_IMPL(DirectionalLightComponent) {
			out["directionalLightComponent"]["emission"] = glm::vec4(color, lightIntensity);
			out["directionalLightComponent"]["indirectLighting"] = indirectLighting;
			out["directionalLightComponent"]["castShadows"] = castShadows;
			out["directionalLightComponent"]["cascadedShadows"] = shadowMapConvention == 7;
			out["directionalLightComponent"]["castTranslucentShadows"] = castTranslucentShadows;
			out["directionalLightComponent"]["volumetric"] = volumetric;
			out["directionalLightComponent"]["volumetricTranslucency"] = volumetricTranslucency;
			out["directionalLightComponent"]["shadowMap"]["resolution"] = shadowMapResolution;
			out["directionalLightComponent"]["shadowMap"]["cascadeCount"] = 2;
			out["directionalLightComponent"]["shadowMap"]["cascadeLogBias"] = 0.0;
			out["directionalLightComponent"]["shadowMap"]["clippingDistance"] = shadowMapClippingDistance;
			out["directionalLightComponent"]["shadowMap"]["sharpness"] = shadowMapSharpness;
			if (ProjectSystem::getEngineSettings().shading.directionalShadowMapType == ESET::ShadowMap::PCF) {
				out["directionalLightComponent"]["shadowMap"]["pcfBias"] = shadowMapBias;
				out["directionalLightComponent"]["shadowMap"]["pcfDepthSlopeBiasFactor"] = shadowMapSlopeBiasFactor;
				out["directionalLightComponent"]["shadowMap"]["poissonRadius"] = shadowMapPoissonRadius;
				out["directionalLightComponent"]["shadowMap"]["poissonSamples"] = shadowMapPoissonSamples;
			} else if (ProjectSystem::getEngineSettings().shading.directionalShadowMapType == ESET::ShadowMap::Variance)
				out["directionalLightComponent"]["shadowMap"]["varianceBias"] = shadowMapBias;	
		}
		COMPONENT_DESERIALIZE_OVERRIDE_IMPL(DirectionalLightComponent) {
			glm::vec4 colorIntensity = SIMDJSON_READ_VEC4(data["directionalLightComponent"]["emission"]);
			color = glm::vec3(colorIntensity);
			lightIntensity = colorIntensity.w;
			castShadows = data["directionalLightComponent"]["castShadows"].get_bool().value();
			if (data["directionalLightComponent"]["cascadedShadows"].error() == simdjson::SUCCESS) {
				shadowMapConvention = data["directionalLightComponent"]["cascadedShadows"].get_bool().value() ? 7 : 1;
			}
			castTranslucentShadows = data["directionalLightComponent"]["castTranslucentShadows"].get_bool().value();
			if (data["directionalLightComponent"]["indirectLighting"].error() == simdjson::SUCCESS)
				indirectLighting = data["directionalLightComponent"]["indirectLighting"].get_bool().value();
			if (data["directionalLightComponent"]["volumetric"].error() == simdjson::SUCCESS)
				volumetric = data["directionalLightComponent"]["volumetric"].get_bool().value();
			if (data["directionalLightComponent"]["volumetricTranslucency"].error() == simdjson::SUCCESS)
				volumetricTranslucency = data["directionalLightComponent"]["volumetricTranslucency"].get_bool().value();
			shadowMapResolution = data["directionalLightComponent"]["shadowMap"]["resolution"].get_uint64().value();
			shadowMapClippingDistance = data["directionalLightComponent"]["shadowMap"]["clippingDistance"].get_double().value();
			shadowMapSharpness = data["directionalLightComponent"]["shadowMap"]["sharpness"].get_double().value();
			if (ProjectSystem::getEngineSettings().shading.directionalShadowMapType == ESET::ShadowMap::PCF) {
				if (data["directionalLightComponent"]["shadowMap"]["pcfBias"].error() == simdjson::SUCCESS)
					shadowMapBias = data["directionalLightComponent"]["shadowMap"]["pcfBias"].get_double().value();
				if (data["directionalLightComponent"]["shadowMap"]["pcfDepthSlopeBiasFactor"].error() == simdjson::SUCCESS)
					shadowMapSlopeBiasFactor = data["directionalLightComponent"]["shadowMap"]["pcfDepthSlopeBiasFactor"].get_double().value();
				if (data["directionalLightComponent"]["shadowMap"]["poissonRadius"].error() == simdjson::SUCCESS)
					shadowMapPoissonRadius = data["directionalLightComponent"]["shadowMap"]["poissonRadius"].get_double().value();
				if (data["directionalLightComponent"]["shadowMap"]["poissonSamples"].error() == simdjson::SUCCESS)
					shadowMapPoissonSamples = data["directionalLightComponent"]["shadowMap"]["poissonSamples"].get_uint64().value();
			} else if (ProjectSystem::getEngineSettings().shading.directionalShadowMapType == ESET::ShadowMap::Variance)
				if (data["directionalLightComponent"]["shadowMap"]["varianceBias"].error() == simdjson::SUCCESS)
					shadowMapBias = data["directionalLightComponent"]["shadowMap"]["varianceBias"].get_double().value();
		}
}