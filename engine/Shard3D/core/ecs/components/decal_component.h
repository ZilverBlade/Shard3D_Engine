#include "../component.h"

namespace Shard3D {
	namespace Components {
		COMPONENT_STRUCT(DeferredDecalComponent) {
			COMPONENT_TYPE(DeferredDecalComponent, "deferredDecalComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();

			AssetID material = AssetID::null();
			uint32_t cachedIndex = 0xffffffff;
			uint32_t priority = 0;
			float opacity = 1.0f;
		};
	}
}