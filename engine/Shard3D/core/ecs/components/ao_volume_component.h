#pragma once

#include "../component.h"

namespace Shard3D {
	namespace Components {
		COMPONENT_STRUCT(BoxAmbientOcclusionVolumeComponent) {
			COMPONENT_TYPE(BoxAmbientOcclusionVolumeComponent, "boxAmbientOcclusionVolumeComponent");
			COMPONENT_SERIALIZATION_OVERRIDE();
			float minOcclusion = 0.1f;
			glm::vec3 tint = { 1.f, 1.f, 1.f };
		};
	}
}