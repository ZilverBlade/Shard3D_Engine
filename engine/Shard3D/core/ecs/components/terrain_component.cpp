#include "terrain_component.h"
#include "../../asset/assetmgr.h"
#include <glm/gtc/type_ptr.hpp>

namespace Shard3D::Components {
	COMPONENT_SERIALIZE_OVERRIDE_IMPL(TerrainComponent) {
		out["terrainComponent"]["terrainAsset"] = terrainAsset.getAsset();

		out["terrainComponent"]["materials"] = materials;
		out["terrainComponent"]["maxMaterials"] = maxMaterials;
		out["terrainComponent"]["tiles"] = tiles;
		out["terrainComponent"]["tileSubdivisions"] = tileSubdivisions;
		out["terrainComponent"]["tilePhysicsSubdivisions"] = tilePhysicsSubdivisions;
		out["terrainComponent"]["tileExtent"] = tileSubdivisions;
		out["terrainComponent"]["allowDualMaterialMap"] = allowDualMaterialMap;
		out["terrainComponent"]["force32BitHeightMap"] = force32BitHeightMap;

		assets.push_back(terrainAsset.getAsset());
	}
	COMPONENT_DESERIALIZE_OVERRIDE_IMPL(TerrainComponent) {
		terrainAsset = std::string(data["terrainComponent"]["terrainAsset"].get_string().value());

		for (int i = 0; i < data["terrainComponent"]["materials"].get_array().size(); i++) {
			AssetID material = std::string(data["terrainComponent"]["materials"].get_array().at(i).get_string().value());
			if (material.getAsset().length()) {
				resourceSystem->loadMaterialRecursive(material);
				materials.push_back(material);
			}
		}
		maxMaterials = data["terrainComponent"]["maxMaterials"].get_int64().value();
		tiles = SIMDJSON_READ_IVEC2(data["terrainComponent"]["tiles"]);
		tileSubdivisions = data["terrainComponent"]["tileSubdivisions"].get_int64().value();
		tilePhysicsSubdivisions = data["terrainComponent"]["tilePhysicsSubdivisions"].get_int64().value();
		tileExtent = data["terrainComponent"]["tileExtent"].get_double().value();
		allowDualMaterialMap = data["terrainComponent"]["allowDualMaterialMap"].get_bool().value();
		force32BitHeightMap = data["terrainComponent"]["force32BitHeightMap"].get_bool().value();

		loadAsset = true;
	}
}