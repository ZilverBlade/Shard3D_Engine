#include "prefab.h"
#include <fstream>
#include "levelmgr.h"
#include "actor.h"

namespace Shard3D::ECS {
	Prefab::Prefab(ResourceSystem* resourceSystem) : localLevel(new Level(resourceSystem)) {
		// instantiate the prefab by copying over the actor to convert to a local level to store the prefab information

	}

	Prefab::~Prefab() {
		delete localLevel;
	}

	Actor Prefab::createActor(Level* toLevel, const std::string& name, UUID scopeUUID, UUID id, AssetID prefabAsset) {
		Actor basePrefabActor = { baseSceneActorHandle, localLevel };
		UUID prefabScope = getTrueActorUUID(scopeUUID, id);
		Actor ractor = toLevel->duplicateActor(basePrefabActor, false, prefabScope, basePrefabActor.getScopedID());
		Actor prefabContainer = toLevel->createActorWithUUID(scopeUUID, id, name);
		prefabContainer.addComponent<Components::PrefabComponent>(prefabAsset);
		toLevel->parentActor(ractor, prefabContainer);
		return prefabContainer;
	}
}