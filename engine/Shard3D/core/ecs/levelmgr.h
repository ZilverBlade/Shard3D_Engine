#pragma once

#include <functional>
#include <nlohmann/json.hpp>
#include "level.h"
#include "../../dynamic_definitions.h"

namespace Shard3D {
	class ResourceSystem;
	inline namespace ECS {
		enum class LevelMgrResults {
			SuccessResult,
			FailedResult,
			UnknownResult,
			ErrorResult,

			WrongFileResult,
			InvalidEntryResult,
			OldEngineVersionResult,
			FutureVersionResult,
		};

		class LevelManager {
		public:
			LevelManager(Level& level, ResourceSystem* resourceSystem);

			void save(const std::string& destinationPath, bool isLevel = true, std::function<void(nlohmann::ordered_json& out, void* userdata)> extraSerializeCall = nullptr, void* userdata = nullptr);
			void saveRuntime(const std::string& destinationPath);

			LevelMgrResults load(const std::string& sourcePath, bool ignoreWarns = false);
			LevelMgrResults loadRuntime(const std::string& sourcePath);

		private:
			Level& mLevel;
			ResourceSystem* resourceSystem;
		};
	}
}