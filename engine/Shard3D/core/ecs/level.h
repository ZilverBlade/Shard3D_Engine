#pragma once
#include <entt.hpp>
#include "../misc/UUID.h"
#include "../../s3dstd.h"
#include "../../core.h"
#include "../rendering/camera.h"
#include "../asset/assetid.h"
#include "prefab.h" 

namespace Shard3D {
	class ResourceSystem;
	class PhysicsSystem;
	class PrefabManager;
	namespace Components {
		struct WorldSceneInfoComponent;
		struct Component;
	}
	inline namespace ECS {
		class Actor;
		class Prefab;
		enum class PlayState {
			Stopped = 0, // no scripts have been instantiated. outside of the editor this should only be this state when loading levels.
			Simulating = 2, // for physics stuff to be previewed.
			Playing = 1, // indicates that the level is being played in the editor.
			Paused = -1, // regular pause mode, for the editor, where all input pauses including HUD, and repossesses editor camera.
			PausedRuntime = -2	// special pause state where everything pauses except UI input, for game runtime like pause menu etc.
		};

		class Level {
		public:
			DELETE_COPY(Level)

			Level(ResourceSystem* resourceSystem);
			~Level();
			void createSystem();

			static sPtr<Level> copy(sPtr<Level> other);
			static void shallowCopy(sPtr<Level>& this_, sPtr<Level>& other);

			Actor createActorWithUUID(UUID scopeUUID, UUID id, const std::string& name = "Some kind of actor");
			Actor createActor(UUID scopeUUID, const std::string& name = "Some kind of actor");
			
			void killEverything();
			void killActor(Actor actor);
			
			Actor duplicateActor(Actor toCopy, bool addCopyTag, UUID scopeUUID = 0, UUID uuid = UUID());


			void runGarbageCollector();

			void rebuildTransforms();
			void cleanup();

			Actor getActorFromUUID(UUID scope, UUID id);
			Actor getActorFromUUID(Actor prefab, UUID id);
			// Unreliable function as multiple actors can have identical tags, use getActorFromUUID() whenever possible.
			Actor getActorFromTag(const std::string& tag);

			void setPossessedCameraActor(Actor actor);
			Actor getPossessedCameraActor();
			S3DCamera& getPossessedCamera();

			Components::WorldSceneInfoComponent* getWorldSceneInfo();

			bool doesActorWithUUIDExist(UUID scopeUUID, UUID id);

			void reparentPrefabOwnership(UUID parentingScope, Actor child);

			void parentActor(Actor child, Actor parent);
			void reparentActor(Actor child, Actor newParent);

			void hideAllEditorOnlyActors();
			void showAllEditorOnlyActors();

			template<typename Component>
			const std::vector<entt::entity>& getActorAddedComponents() {
				return componentsCreated[typeid(Component).hash_code()];
			}
			template<typename Component>
			const std::vector<entt::entity>& getActorRemovedComponents() {
				return componentsDestroyed[typeid(Component).hash_code()];
			}

			/* *
* Call when level events must begin
*/
			//void begin();
			/* *
* Gets called upon every frame
* \param dt - Frame time
*/
			//void tick(float dt);
			/* *
* Call when level events must end
*/
			//void end();

			void simulationStateCallback();

			PlayState simulationState = PlayState::Stopped;

			bool wasPaused;

			entt::registry registry;
			AssetID levelAsset{ AssetID::null() };
			AssetID bakedDataDirectory{ AssetID::null() };

			PrefabManager* getPrefabManager() {
				return prefabManager;
			}

			std::vector<Actor> getAllChildrenBelongingToActor(Actor parent);
			std::unordered_map<size_t, std::vector<entt::entity>> componentsCreated;
		protected:
			entt::entity possessedCameraActor = entt::null;
			entt::entity possessedCameraActor_old = entt::null;
		private:
			void rebuildRelations(Actor child);

			void showActorChildren(Actor parent);
			void hideActorChildren(Actor parent);

			std::vector<Actor> recursiveActorFetch(Actor actor);
			// map
			std::unordered_map<UUID, entt::entity> actorMap{}; // [PREFAB + REGULAR ACTOR (RECURSIVE HASH) ID] ACTOR (no prefab inheritance is 0 by default)

			//void registerPluginComponent(Components::Component* component);
			//std::vector<Components::Component*> registeredComponents;
			
			// queues 
			std::vector<Actor> actorKillQueue{};

			std::unordered_map<entt::entity, std::unordered_set<size_t>> actorOwnedComponents;
			std::unordered_map<size_t, std::vector<entt::entity>> componentsDestroyed;

			ResourceSystem* resourceSystem{};
			PrefabManager* prefabManager{};

			friend class Actor;
			friend class Prefab;
			friend class PrefabManager;
			friend class LevelManager;
		};
	}
}