#pragma once
#include "../../s3dstd.h"
#include "../../core.h"
#include "../asset/assetid.h"
#include "../misc/UUID.h"
#include <entt.hpp>
namespace Shard3D {
	class ResourceSystem;
	inline namespace ECS {
		class Actor;
		class Level;
		class PrefabManager;
		class Prefab {
		public:
			Prefab(ResourceSystem* resourceSystem);
			~Prefab();
			DELETE_COPY(Prefab);

			Actor createActor(Level* toLevel, const std::string& name, UUID scopeUUID, UUID uuid, AssetID prefabAsset);

			Level* getLocalLevel() { return localLevel; }

			bool enableScript = false;
			std::string scriptModule{};
		private:
			entt::entity baseSceneActorHandle;
			Level* localLevel;
			friend class Level;
			friend class PrefabManager;
		};
	}
}