#pragma once
#include <string>
#include "../../s3dstd.h"
#include "../../core.h"
#include "../../core/misc/UUID.h"
#include "level.h"
#include "../../scripting/script_engine.h"
#include "../../systems/rendering/hud_system.h"

namespace Shard3D {
	class LevelSimulationState {
	public:
		static void setData(sPtr<PhysicsSystem> physicsSystem, sPtr<HUDRenderSystem> hudRenderSystem, ResourceSystem* resourceSystem);
		static bool instantiate(sPtr<Level>& level, PlayState playstate);
		
		static void eventBegin();
		static void eventTick(float dt);
		static void eventEnd();

		static sPtr<PhysicsSystem>& getPhysicsSystem();
		static sPtr<HUDRenderSystem>& getHUDSystem();

		static void release();
		static void removeData();
	private:
	};


}