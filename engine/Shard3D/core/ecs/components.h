#pragma once

#include "components/core_components.h"
#include "components/prefab_component.h"
#include "components/transform_component.h"
#include "components/light_component.h"
#include "components/sky_component.h"
#include "components/camera_component.h"
#include "components/ppfx_component.h"

#include "components/particle_component.h"
#include "components/physics_component.h"
#include "components/fog_component.h"
#include "components/decal_component.h"

#include "components/group_component.h"
#include "components/audio_component.h"
#include "components/mesh3d_component.h"
#include "components/skeleton_rig_component.h"
#include "components/billboard_component.h"
#include "components/renderer_property_component.h"
#include "components/reflection_capture_component.h"
#include "components/planar_reflection_component.h"
#include "components/ao_volume_component.h"
#include "components/volume_component.h"
#include "components/virtual_point_light_component.h"

#include "components/terrain_component.h"