#include "../../s3dpch.h"
#include "levelmgr.h"

#include "../../ecs.h"
#include "../../core.h"
#include "../asset/assetmgr.h"
#include "../../systems/handlers/prefab_manager.h"
#include <fstream>
#include <filesystem>
#include <magic_enum.hpp>

namespace Shard3D {
#define TRY_SERIALIZE(component, emitter, emitterAssets) if (actor.hasComponent<Components::##component>()){ \
	actor.getComponent<Components::##component>().serialize(level, emitter, emitterAssets);}
#define TRY_DESERIALIZE(component, node) if (node[Components::##component().getKeyJSON()].error() == simdjson::SUCCESS){ \
	loadedActor.addComponent<Components::##component>().deserialize(&mLevel, &loadedActor, resourceSystem, node);}

	inline namespace ECS {
		LevelManager::LevelManager(Level& level, ResourceSystem* rs) : mLevel(level), resourceSystem(rs) {
			SHARD3D_INFO("Loading Level Manager");
		}

		static void saveActor(nlohmann::ordered_json& out, nlohmann::ordered_json& assets, Actor actor, Level* level) {
			if (actor.isInvalid()) return;
			out["actor"] = static_cast<uint64_t>(actor.getScopedID());
			out["tag"] = actor.getTag();
			 
			// TRANSFORM
			TRY_SERIALIZE(TransformComponent, out, assets);
			TRY_SERIALIZE(VisibilityComponent, out, assets);
			TRY_SERIALIZE(MobilityComponent, out, assets);

			if (!actor.isPrefabChild() && !actor.hasComponent<Components::PrefabComponent>()) {
				TRY_SERIALIZE(RelationshipComponent, out, assets);

				TRY_SERIALIZE(CameraComponent, out, assets);
				TRY_SERIALIZE(PostFXComponent, out, assets);
				TRY_SERIALIZE(BoxVolumeComponent, out, assets);
				TRY_SERIALIZE(BoxReflectionCaptureComponent, out, assets);
				TRY_SERIALIZE(BoxAmbientOcclusionVolumeComponent, out, assets);
				TRY_SERIALIZE(PlanarReflectionComponent, out, assets);
				TRY_SERIALIZE(SkyLightComponent, out, assets);
				TRY_SERIALIZE(SkyboxComponent, out, assets);
				TRY_SERIALIZE(SkyAtmosphereComponent, out, assets);
				TRY_SERIALIZE(ExponentialFogComponent, out, assets);
				TRY_SERIALIZE(Mesh3DComponent, out, assets);
				TRY_SERIALIZE(Rigidbody3DComponent, out, assets);
				TRY_SERIALIZE(PhysicsConstraintComponent, out, assets);
				TRY_SERIALIZE(IndependentSuspensionComponent, out, assets);
				TRY_SERIALIZE(VehicleComponent, out, assets);
				TRY_SERIALIZE(SwayBarComponent, out, assets);
				TRY_SERIALIZE(DeferredDecalComponent, out, assets);
				TRY_SERIALIZE(SkeletonRigComponent, out, assets);
				TRY_SERIALIZE(TerrainComponent, out, assets);
				TRY_SERIALIZE(VirtualPointLightComponent, out, assets);
				TRY_SERIALIZE(PointLightComponent, out, assets);
				TRY_SERIALIZE(SpotLightComponent, out, assets);
				TRY_SERIALIZE(DirectionalLightComponent, out, assets);
			} else {
				TRY_SERIALIZE(PrefabComponent, out, assets);
			}
		}
		void LevelManager::save(const std::string& destinationPath, bool isLevel, std::function<void(nlohmann::ordered_json& out, void* userdata)> extraSerializeCall, void* userdata) {
			std::string newPath = destinationPath;
			if (isLevel) {
				if (!strUtils::hasEnding(destinationPath, ".s3dlevel")) newPath = destinationPath + ".s3dlevel";
			}
			auto relativePath = std::filesystem::relative(newPath, std::filesystem::path(ProjectSystem::getAssetLocation()));
			mLevel.levelAsset = relativePath.string();
			mLevel.bakedDataDirectory = AssetID(relativePath.string().substr(0, relativePath.string().find_last_of(".")) + "_builddata").getAsset();
			nlohmann::ordered_json out{};
			out["version"] = ENGINE_VERSION;
			if (isLevel) {
				if (mLevel.actorMap.find(0) != mLevel.actorMap.end())
					out["lastSavedEditorCameraPos"] = mLevel.getActorFromUUID(0, 0).getTransform().getTranslation();
				else
					out["lastSavedEditorCameraPos"] = glm::vec3(0.f);
				out["worldSceneInfo"] = nlohmann::ordered_json::object();
				out["bakedDataDirectory"] = mLevel.bakedDataDirectory.getAsset();
			}
			if (extraSerializeCall) {
				extraSerializeCall(out, userdata);
			}

			out["assets"] = nlohmann::ordered_json::array();
			out["actors"] = nlohmann::ordered_json::array();

			mLevel.registry.each([&](auto actorGUID) { Actor actor = { actorGUID, &mLevel };
				if (!actor) return;
				if (actor.isPrefabChild()) return; // prefab children must be handled differently
				out["actors"].push_back(nlohmann::ordered_json::object());
				saveActor(out["actors"].back(), out["assets"], actor, &mLevel);
			});


			std::ofstream fout(newPath);
			fout << out.dump(4);
			fout.flush();
			fout.close();
		
			SHARD3D_LOG("Saved level '{0}'", newPath);
		}

		void LevelManager::saveRuntime(const std::string& destinationPath) {
			SHARD3D_ASSERT(false);
		}

		LevelMgrResults LevelManager::load(const std::string& sourcePath, bool ignoreWarns) {
			std::ifstream stream(sourcePath);
			std::stringstream strStream;
			strStream << stream.rdbuf(); 

			simdjson::dom::parser parser;
			simdjson::padded_string json = simdjson::padded_string::load(sourcePath);
			const auto& data = parser.parse(json);
			if (simdjson::error_code ec = data.error(); ec != simdjson::error_code::SUCCESS) {
				SHARD3D_ERROR("simdjson parser error. error code: {0} ({1})", magic_enum::enum_name(ec), (int)ec);
				return LevelMgrResults::FailedResult;
			}
			auto relativePath = std::filesystem::relative(sourcePath, std::filesystem::path(ProjectSystem::getAssetLocation()));
			mLevel.levelAsset = relativePath.string();
			mLevel.bakedDataDirectory = AssetID(relativePath.string().substr(0, relativePath.string().find_last_of(".")) + "_builddata").getAsset();

			if (SIMDJSON_READ_S3DVERSION(data["version"]) < ENGINE_VERSION) {
				SHARD3D_WARN("Old engine version");
			}

			resourceSystem->clearAllAssets(); // remove since new stuff will be loaded into memory
			
			if (data["lastSavedEditorCameraPos"].get_array().error() == simdjson::SUCCESS) {
				mLevel.getActorFromUUID(0, 0).getTransform().setTranslation(SIMDJSON_READ_VEC3(data["lastSavedEditorCameraPos"]));
			}
			auto newTime = std::chrono::high_resolution_clock::now();
			if (data["actors"].error() == simdjson::SUCCESS) {
				for (auto actor : data["actors"]) {
					if (actor["actor"].error() != simdjson::SUCCESS) continue; // invalid actor
					Actor loadedActor{};
					loadedActor = mLevel.createActorWithUUID(0, actor["actor"].get_uint64().value(), std::string(actor["tag"].get_string().value()));
					SHARD3D_LOG("Loading actor {0}, UUID {1}, handle {2}", loadedActor.getTag(), loadedActor.getScopedID(), (uint32_t)loadedActor.actorHandle);
					
					TRY_DESERIALIZE(TransformComponent, actor);
					TRY_DESERIALIZE(PrefabComponent, actor);
					if (loadedActor.hasComponent<Components::PrefabComponent>()) {
						Components::TransformComponent tc = loadedActor.getTransform();
						std::string tag = loadedActor.getTag();
						UUID uc = loadedActor.getScopedID();
						AssetID prefabAsset = loadedActor.getComponent<Components::PrefabComponent>().prefab;
						mLevel.actorMap.erase(loadedActor.getGlobalUUID());
						mLevel.registry.destroy(loadedActor.actorHandle);
						Actor ct = mLevel.getPrefabManager()->instPrefab(&mLevel, 0, uc, prefabAsset, tag);
						if (ct) {
							loadedActor = ct;
							loadedActor.getTransform() = tc;
						}
						else {
							loadedActor.killComponent<Components::PrefabComponent>();
						}
					}
				}
				for (auto actor : data["actors"]) {
					if (actor["actor"].error() != simdjson::SUCCESS) continue; // invalid actor
					Actor loadedActor{};
					loadedActor = mLevel.getActorFromUUID(0, actor["actor"].get_uint64().value());

					TRY_DESERIALIZE(VisibilityComponent, actor);
					TRY_DESERIALIZE(MobilityComponent, actor);
					TRY_DESERIALIZE(PostFXComponent, actor);
					TRY_DESERIALIZE(BoxVolumeComponent, actor);
					TRY_DESERIALIZE(BoxReflectionCaptureComponent, actor);
					TRY_DESERIALIZE(PlanarReflectionComponent, actor);
					TRY_DESERIALIZE(BoxAmbientOcclusionVolumeComponent, actor);
					TRY_DESERIALIZE(ExponentialFogComponent, actor);
					TRY_DESERIALIZE(SkyLightComponent, actor);
					TRY_DESERIALIZE(SkyboxComponent, actor);
					TRY_DESERIALIZE(SkyAtmosphereComponent, actor);
					TRY_DESERIALIZE(CameraComponent, actor);
					TRY_DESERIALIZE(Mesh3DComponent, actor);
					TRY_DESERIALIZE(Rigidbody3DComponent, actor);
					TRY_DESERIALIZE(PhysicsConstraintComponent, actor);
					TRY_DESERIALIZE(IndependentSuspensionComponent, actor);
					TRY_DESERIALIZE(VehicleComponent, actor);
					TRY_DESERIALIZE(SwayBarComponent, actor);
					TRY_DESERIALIZE(DeferredDecalComponent, actor);
					TRY_DESERIALIZE(SkeletonRigComponent, actor);
					TRY_DESERIALIZE(TerrainComponent, actor);
					TRY_DESERIALIZE(VirtualPointLightComponent, actor);
					TRY_DESERIALIZE(PointLightComponent, actor);
					TRY_DESERIALIZE(SpotLightComponent, actor);
					TRY_DESERIALIZE(DirectionalLightComponent, actor);
					TRY_DESERIALIZE(RelationshipComponent, actor);
				}
			}
			SHARD3D_INFO("Duration of loading level {0}: {1} ms", sourcePath,
				std::chrono::duration<float, std::chrono::milliseconds::period>
				(std::chrono::high_resolution_clock::now() - newTime).count());
			//}
			//catch (YAML::Exception ex) {
			//	SHARD3D_ERROR("Unable to load level {0}. Reason: {1}", sourcePath, ex.msg);
			//	return LevelMgrResults::FailedResult;
			//}
			return LevelMgrResults::SuccessResult;
		}

		LevelMgrResults LevelManager::loadRuntime(const std::string& sourcePath) {
			SHARD3D_ASSERT(false);
			return LevelMgrResults::ErrorResult;
		}
	}
}