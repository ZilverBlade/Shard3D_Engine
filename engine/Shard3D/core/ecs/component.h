#pragma once

#include <entt.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include "../../utils/json_ext.h"

namespace Shard3D {
	class ResourceSystem;
	inline namespace ECS {
		class Level;
		class Actor;
	}
#define COMPONENT_TYPE(component, jsonkey_) \
	component() = default;	\
	component(const component&) = default; \
	virtual const char* getKeyJSON() override{ return jsonkey_; }

#define NON_SERIALIZABLE_COMPONENT_TYPE(component) \
	component() = default;	\
	component(const component&) = default;

	namespace Components {
		struct TransformComponent;	// for macro declaration
		struct IsVisibileComponent;	// for macro declaration
		struct NullComponent {}; // error
		struct Component {
			virtual const char* getKeyJSON() {
				return "nullComponent";
			}
			virtual void serialize(Level* level, nlohmann::ordered_json& out, nlohmann::ordered_json& assets) {}
			virtual void deserialize(Level* level, Actor* actor, ResourceSystem* resourceSystem, simdjson::dom::element& data) {}
		};

#define COMPONENT_STRUCT(component) struct component : public Component
#define NON_SERIALIZABLE_COMPONENT_STRUCT(component) struct component
#define NON_INTERACTIBLE_COMPONENT_STRUCT(component) struct component { NON_SERIALIZABLE_COMPONENT_TYPE(component); char dummy{};};

#define COMPONENT_SERIALIZATION_OVERRIDE() \
		virtual void serialize(Level* level, nlohmann::ordered_json& out, nlohmann::ordered_json& assets) override; \
		virtual void deserialize(Level* level,Actor* actor, ResourceSystem* resourceSystem, simdjson::dom::element& data) override; 

#define COMPONENT_SERIALIZE_OVERRIDE_IMPL(component_struct) void component_struct::serialize(Level* level, nlohmann::ordered_json& out, nlohmann::ordered_json& assets)

#define COMPONENT_DESERIALIZE_OVERRIDE_IMPL(component_struct) void component_struct::deserialize(Level* level, Actor* actor, ResourceSystem* resourceSystem, simdjson::dom::element& data)
#define RENDER_ITER Components::TransformComponent, Components::IsVisibileComponent
	}
}