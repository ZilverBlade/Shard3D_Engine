#include "level_simulation.h"
#include "../../ecs.h"
#include "../../systems/computational/physics_system.h"
#include "../../systems/handlers/resource_system.h"

namespace Shard3D {

	static bool occupied = false;
	static sPtr<Level> level = nullptr;
	static sPtr<PhysicsSystem> physicsSystem = nullptr;
	static sPtr<HUDRenderSystem> hudRenderSystem = nullptr;
	static ResourceSystem* resourceSystem = nullptr;

	static float accumTime = 0.0f;

	static PlayState playState;

	void LevelSimulationState::setData(sPtr<PhysicsSystem> physicsSystem_, sPtr<HUDRenderSystem> hudRenderSystem_, ResourceSystem* resourceSystem_) {
		physicsSystem = physicsSystem_;
		hudRenderSystem = hudRenderSystem_;
		resourceSystem = resourceSystem_;
	}

	bool LevelSimulationState::instantiate(sPtr<Level>& level_, PlayState playState_) {
		if (occupied) return false;
		occupied = true;
		level = level_;
		playState = playState_;
		accumTime = 0.0f;
		return true;
	}
	void LevelSimulationState::eventBegin() {
		level->simulationState = playState;
		if (level->simulationState == PlayState::Playing) {
			ScriptEngine::runtimeStart(level, resourceSystem);
			ScriptEngine::instantiateActors();
			ScriptEngine::actorScript().beginEvent();
			level->runGarbageCollector(); 
			level->rebuildTransforms();
		}
		physicsSystem->begin(level);
	}
	void LevelSimulationState::eventTick(float dt) {
		if (!level) return;
		if (level->simulationState == PlayState::Simulating || level->simulationState == PlayState::Playing) {
			accumTime += dt;
			physicsSystem->createPhysicsActors(level);
			while (accumTime >= physicsSystem->getStepSize()) {
				physicsSystem->simulate(level);
				if (playState == PlayState::Playing) {
					ScriptEngine::actorScript().physicsTickEvent(physicsSystem->getStepSize());
				}
				accumTime -= physicsSystem->getStepSize();
			}

			// frame rate is different from physics update rate, interpolate positions
			float factor = accumTime / physicsSystem->getStepSize();
			SHARD3D_ASSERT(factor <= 1.0f && factor >= 0.0f);
			physicsSystem->applyTransform(level, factor);

			if (playState == PlayState::Playing) {
				ScriptEngine::updateSkeletonAnimations(dt);
				for (HUDElementData* element : hudRenderSystem->getElementActionList()) {
					if (element->state.IsClicked()) {
						ScriptEngine::hudElementScript().clickEvent(element->genericBase);
					}
					if (element->state.IsHovering()) {
						ScriptEngine::hudElementScript().hoverEvent(element->genericBase);
					}
					if (element->state.IsPressed()) {
						ScriptEngine::hudElementScript().pressEvent(element->genericBase);
					}
				}

				ScriptEngine::surfaceMaterialScript().update(dt);
				ScriptEngine::actorScript().tickEvent(dt);
			}
		}
	}
	void LevelSimulationState::eventEnd() {
		if (!occupied) return;

		playState = level->simulationState = PlayState::Stopped;
		ScriptEngine::actorScript().endEvent();
		ScriptEngine::runtimeStop();
		physicsSystem->end(level);
		if (resourceSystem)
			resourceSystem->destroyAllMaterialInstances();

		if (hudRenderSystem) {
			for (std::string& layer : hudRenderSystem->getAllLayers()) {
				hudRenderSystem->eraseHUDLayer(layer);
			}
		}
	}

	sPtr<PhysicsSystem>& LevelSimulationState::getPhysicsSystem() {
		SHARD3D_ASSERT(physicsSystem);
		return physicsSystem;
	}

	sPtr<HUDRenderSystem>& LevelSimulationState::getHUDSystem() {
		SHARD3D_ASSERT(hudRenderSystem);
		return hudRenderSystem;
	}

	void LevelSimulationState::release() {
		level = nullptr;
		occupied = false;
		accumTime = 0.0f;
	}
	void LevelSimulationState::removeData() {
		physicsSystem = nullptr;
		hudRenderSystem = nullptr;
	}
}
