#pragma once
#ifdef NDEBUG
#define ENTT_ASSERT(...) ((void)1)
#endif
#include <entt.hpp>
#include "level.h"
#include "components.h"
#include "../../core.h"

namespace Shard3D {
	inline namespace ECS {
		class Prefab;

		static std::size_t getTrueActorUUID(std::size_t bpivk, std::size_t actor) {
			std::size_t seed = 0;
			if (bpivk != 0) {
				seed = bpivk;
				Shard3D::hashCombine(seed, actor);
			} else {
				seed = actor;
			}
			return seed;
		}
		class Actor {
		public:
			Actor() = default;
			Actor(entt::entity _handle, Level* _level);

			Actor(const Actor& other) = default;

			template<typename T, typename... Args>
			inline T& addComponent(Args&&... args) const {
				if (hasComponent<T>()) {	// return getComponent if component exists
					SHARD3D_VERBOSE("Actor {0} already has component {1}!", this->getScopedID(), typeid(T).name());
					return getComponent<T>();
				}
				level->componentsCreated[typeid(T).hash_code()].push_back(actorHandle);
				level->actorOwnedComponents[actorHandle].insert(typeid(T).hash_code());
				return level->registry.emplace<T>(actorHandle, std::forward<Args>(args)...);
			}
			template<typename T>
			inline T& getComponent() const {
				if (!hasComponent<T>()) {
					SHARD3D_ERROR("Actor {0} does not have component '{1}'!", this->getScopedID(), typeid(T).name());
					SHARD3D_FATAL("Tried to get a component that does not exist!");
				}
				return level->registry.get<T>(actorHandle);
			}

			template<typename T>
			inline bool hasComponent() const {
				return level->registry.all_of<T>(actorHandle);
			}

			template<typename T>
			inline void killComponent() const { // is warn since it has the chance of not causing a crash or undefined behaviour
				if (!hasComponent<T>()) { // no #if !ENSET_UNSAFE_COMPONENTS because this error is negligible;
					SHARD3D_WARN("Actor {0} does not have component '{1}'!", this->getScopedID(), typeid(T).name());
					return;
				}
				level->registry.remove<T>(actorHandle);
				level->componentsDestroyed[typeid(T).hash_code()].push_back(actorHandle);
				level->actorOwnedComponents[actorHandle].erase(typeid(T).hash_code());
			}

			inline bool hasRelationship() const {
				return hasComponent<Components::RelationshipComponent>();
			}

			inline bool isInvalid() const {
				if (actorHandle == entt::null || !level->registry.valid(actorHandle))
					return true;
				return getScopedID() == 0 || getScopedID() == 1 || getScopedID() == UINT64_MAX;
			}
			inline void makeDynamic() const { 
				auto& mobCmp = nocheck_getComponent<Components::MobilityComponent>();
				uint32_t mobBits = 0x0;
				if (hasComponent<Components::StaticActorComponent>()) {
					killComponent<Components::StaticActorComponent>();
					mobBits = 0x0u;
				}
				else if (hasComponent<Components::StationaryActorComponent>()) {
					killComponent<Components::StationaryActorComponent>();
					mobBits = 0x1u;
				}
				mobCmp.mobility = Mobility::Dynamic;
				addComponent<Components::DynamicActorComponent>();
				SHARD3D_LOG("Mobility changed for actor {0} ({1}) to Dynamic", getTag(), getScopedID());
				if (hasRelationship()) {
					for (auto& child : nocheck_getComponent<Components::RelationshipComponent>().childActors) {
						Actor childActor = { child, level };
						if (Mobility::Dynamic > childActor.getMobility()) childActor.makeDynamic();
					}
				}
			}
			inline void makeStationary() const {
				auto& mobCmp = nocheck_getComponent<Components::MobilityComponent>();
				uint32_t mobBits = 0x0;
				if (hasComponent<Components::StaticActorComponent>()) {
					killComponent<Components::StaticActorComponent>();
					mobBits = 0x0u;
				} else if (hasComponent<Components::DynamicActorComponent>()) {
					killComponent<Components::DynamicActorComponent>();
					mobBits = 0x2u;
				}
				mobCmp.mobility = Mobility::Stationary;
				addComponent<Components::StationaryActorComponent>();
				SHARD3D_LOG("Mobility changed for actor {0} ({1}) to Stationary", getTag(), getScopedID());
				if (hasRelationship()) {
					for (auto& child : nocheck_getComponent<Components::RelationshipComponent>().childActors) {
						Actor childActor = { child, level };
						if (Mobility::Stationary > childActor.getMobility()) childActor.makeStationary();					
					}
				}
			}
			inline void makeStatic() const {
				auto& mobCmp = nocheck_getComponent<Components::MobilityComponent>();
				uint32_t mobBits = 0x0;
				if (hasComponent<Components::DynamicActorComponent>()) {
					killComponent<Components::DynamicActorComponent>();
					mobBits = 0x2u;
				}
				else if (hasComponent<Components::StationaryActorComponent>()) {
					killComponent<Components::StationaryActorComponent>();
					mobBits = 0x1u;
				}
				mobCmp.mobility = Mobility::Static;
				addComponent<Components::StaticActorComponent>();
				SHARD3D_LOG("Mobility changed for actor {0} ({1}) to Static", getTag(), getScopedID());
			}
			inline Mobility getMobility() const {
				return nocheck_getComponent<Components::MobilityComponent>().mobility;
			}
			inline void makeVisible() const {
				nocheck_getComponent<Components::VisibilityComponent>().isVisible = true;
				addComponent<Components::IsVisibileComponent>();
				if (hasRelationship()) level->showActorChildren(*this);
			}
			inline void makeInvisible() const {
				nocheck_getComponent<Components::VisibilityComponent>().isVisible = false;
				killComponent<Components::IsVisibileComponent>();
				if (hasRelationship()) level->hideActorChildren(*this);
			}
			inline bool isVisible() const {
				return nocheck_getComponent<Components::VisibilityComponent>().isVisible;
			}
			inline UUID getScopedID() const { 
				SHARD3D_ASSERT(hasComponent<Components::UUIDComponent>() && "All actors must have a UUID Component!"); 
				return nocheck_getComponent<Components::UUIDComponent>().getScopedID(); 
			}
			inline UUID getInstancerUUID() const {
				SHARD3D_ASSERT(hasComponent<Components::UUIDComponent>() && "All actors must have a UUID Component!");
				return nocheck_getComponent<Components::UUIDComponent>().getInstancerUUID();
			}
			inline UUID getGlobalUUID() const {
				return getTrueActorUUID(getInstancerUUID(), getScopedID());
			}

			inline bool isPrefabChild() const {
				return getInstancerUUID() != 0;
			}

			inline std::string& getTag() const { 
				SHARD3D_ASSERT(hasComponent<Components::TagComponent>() && "All actors must have a Tag Component!"); 
				return nocheck_getComponent<Components::TagComponent>().tag; 
			}
			inline void setTag(const std::string& tag) const { 
				nocheck_getComponent<Components::TagComponent>().tag = tag; 
			};
			inline Components::TransformComponent& getTransform() const {
				return nocheck_getComponent<Components::TransformComponent>(); 
			}
			
			inline operator bool() const { return actorHandle != entt::null; }
			inline operator entt::entity() const { return actorHandle; };
			inline operator uint32_t() const { return static_cast<uint32_t>(actorHandle); };
			
			inline bool valid() {
				return level->registry.valid(actorHandle);
			}

			inline bool operator==(const Actor& other) const {
				return actorHandle == other.actorHandle && level == other.level;
			}
			inline bool operator!=(const Actor& other) const {
				return !(*this == other);
			}

			entt::entity actorHandle{ entt::null };
		private:
			template<typename T>
			inline T& nocheck_getComponent() const {
				return level->registry.get<T>(actorHandle);
			}

			Level* level = nullptr; // 8 bytes (use it as much as needed)

			friend class Level;
			friend class RenderList;
			friend class Prefab;
		};
	}
}