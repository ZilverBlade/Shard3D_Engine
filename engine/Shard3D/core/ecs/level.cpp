#include "../../s3dpch.h"
#include "level.h"
#include "actor.h" 
#include "levelmgr.h"

#include "../asset/assetmgr.h"
#include "../../systems/handlers/render_list.h"
#include "../../systems/handlers/prefab_manager.h"
#include "../misc/engine_settings.h"
#include <magic_enum.hpp>

#define NO_PREFAB_INHERIT_ACTOR 0
namespace Shard3D {
	inline namespace ECS {
		Level::Level(ResourceSystem* rs) : resourceSystem(rs){
			prefabManager = new PrefabManager(resourceSystem);
		}
		Level::~Level() {
			SHARD3D_LOG_D("Destroying level");
			registry.clear();
			delete prefabManager;
		}

		void Level::createSystem() {
			SHARD3D_LOG_D("Loading Editor Actor");
			ECS::Actor editor_cameraActor = createActorWithUUID(NO_PREFAB_INHERIT_ACTOR, 0, "Editor Camera Actor (SYSTEM RESERVED)");
			editor_cameraActor.addComponent<Components::CameraComponent>();
			editor_cameraActor.addComponent<Components::PostFXComponent>();
			editor_cameraActor.makeDynamic();

			setPossessedCameraActor(editor_cameraActor);
			editor_cameraActor.getComponent<Components::TransformComponent>().setTranslation(glm::vec3(0.f, -1.f, 1.f));
			SHARD3D_LOG_D("Loading WorldScene Actor");
			ECS::Actor worldSceneInfo = createActorWithUUID(NO_PREFAB_INHERIT_ACTOR, 1, "WorldSceneInfo (SYSTEM RESERVED)");
			worldSceneInfo.addComponent<Components::WorldSceneInfoComponent>();
		}

		template<typename _MyComponent>
		static void copyComponent(Level& dstlvl, entt::registry& dst, entt::registry& src, std::unordered_map<UUID, entt::entity>& map) {
			auto view = src.view<_MyComponent>();
			for (auto e : view) {
				if (src.get<Components::UUIDComponent>(e).getInstancerUUID() != 0) continue; // skip prefab children
				UUID uuid = getTrueActorUUID(
					src.get<Components::UUIDComponent>(e).getInstancerUUID(),
					src.get<Components::UUIDComponent>(e).getScopedID()
				);

				SHARD3D_ASSERT(map.find(uuid) != map.end());

				entt::entity dstEnttID = map.at(uuid);

				_MyComponent& component = src.get<_MyComponent>(e);

				dst.emplace_or_replace<_MyComponent>(dstEnttID, component);
				dstlvl.componentsCreated[typeid(_MyComponent).hash_code()].push_back(e);
			}
		}

		template<typename... _MyComponent>
		static void copyComponentIfExists(Actor src, Actor dst) {
			([&]()
			{
				if (src.hasComponent<_MyComponent>())
					dst.addComponent<_MyComponent>(src.getComponent<_MyComponent>());
			}(), ...);
		}

		void Level::shallowCopy(sPtr<Level>& this_, sPtr<Level>& other) {
			this_->possessedCameraActor = other->getPossessedCameraActor();
			this_->levelAsset = other->levelAsset;
		}

		sPtr<Level> Level::copy(sPtr<Level> other) {
			sPtr<Level> newLvl = make_sPtr<Level>(other->resourceSystem);
			std::unordered_map<UUID, entt::entity> enttMap;

			entt::registry& srcLvlRegistry = other->registry;
			entt::registry& dstLvlRegistry = newLvl->registry;

			shallowCopy(newLvl, other);

			auto idView = srcLvlRegistry.view<Components::UUIDComponent>();
			for (auto it = idView.begin(); it != idView.end(); ++it) {
				if (srcLvlRegistry.get<Components::UUIDComponent>(idView[it.index()]).getInstancerUUID() != 0) continue; // skip prefab children
				
				UUID scopeID = srcLvlRegistry.get<Components::UUIDComponent>(idView[it.index()]).getInstancerUUID();
				UUID id = srcLvlRegistry.get<Components::UUIDComponent>(idView[it.index()]).getScopedID();

				UUID uuid = getTrueActorUUID(scopeID, id);
				Actor newActor;
				if (srcLvlRegistry.all_of<Components::PrefabComponent>(idView[it.index()])) {
					enttMap[uuid] = newActor.actorHandle;
					Components::PrefabComponent& bpComp = srcLvlRegistry.get<Components::PrefabComponent>(idView[it.index()]);
					newActor = newLvl->prefabManager->instPrefab(newLvl.get(), scopeID, id, bpComp.prefab, srcLvlRegistry.get<Components::TagComponent>(idView[it.index()]).tag);
				} else {
					newActor = newLvl->createActorWithUUID(scopeID, uuid, srcLvlRegistry.get<Components::TagComponent>(idView[it.index()]).tag);
				}
				switch (srcLvlRegistry.get<Components::MobilityComponent>(idView[it.index()]).mobility) {
					case(Mobility::Static): 
						newActor.makeStatic();
						break;
					case(Mobility::Stationary):
						newActor.makeStationary();
						break;
					case(Mobility::Dynamic):
						newActor.makeDynamic();
						break;
				}
				switch (srcLvlRegistry.get<Components::VisibilityComponent>(idView[it.index()]).isVisible) {
				case(true):
					newActor.makeVisible();
					break;
				case(false):
					newActor.makeInvisible();
					break;
				}
				newActor.getComponent<Components::VisibilityComponent>().editorOnly = srcLvlRegistry.get<Components::VisibilityComponent>(idView[it.index()]).editorOnly;
				newActor.getComponent<Components::VisibilityComponent>().exportable = srcLvlRegistry.get<Components::VisibilityComponent>(idView[it.index()]).exportable;
				enttMap[uuid] = newActor.actorHandle;
			}
			for (auto& [uuid, entity] : enttMap) {
				entt::entity handle = other->actorMap[uuid]; // prefab inherited actors will be regenerated
				Actor oldActor = { handle , other.get()};
				if (oldActor.hasRelationship()) { // handle relationships due to special copy/unload case		
					Components::RelationshipComponent& oldRelation = srcLvlRegistry.get<Components::RelationshipComponent>(handle);
					Components::RelationshipComponent newRelation{};
					if (oldRelation.parentActor != entt::null) {
						UUID scopeID = Actor(oldRelation.parentActor, other.get()).getInstancerUUID();
						UUID id = Actor(oldRelation.parentActor, other.get()).getScopedID();

						UUID uuid = getTrueActorUUID(scopeID, id);
						newRelation.parentActor = enttMap[uuid];
					}
					for (entt::entity& child : oldRelation.childActors) {
						Actor childActor = Actor(child, other.get());
						if (childActor.getInstancerUUID() != 0) continue; // skip prefab children
						newRelation.childActors.push_back(enttMap[getTrueActorUUID(
							childActor.getInstancerUUID(),
							childActor.getScopedID()
						)]);
					}
					if (oldActor.hasComponent<Components::PrefabComponent>()) {
						VectorUtils::merge(dstLvlRegistry.get<Components::RelationshipComponent>(handle).childActors, newRelation.childActors);
					} else {
						dstLvlRegistry.emplace_or_replace<Components::RelationshipComponent>(entity, newRelation);
					}
				}
			}

			copyComponent<Components::TransformComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::BillboardComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::Mesh3DComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::AudioComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::CameraComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::PostFXComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::SkyLightComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::SkyAtmosphereComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::DirectionalLightComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::PointLightComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::SpotLightComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::Rigidbody3DComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::PhysicsConstraintComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::SwayBarComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::IndependentSuspensionComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::VehicleComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::BoxReflectionCaptureComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::PlanarReflectionComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::BoxAmbientOcclusionVolumeComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::BoxVolumeComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::SkyboxComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::VirtualPointLightComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::ExponentialFogComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::TerrainComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::DeferredDecalComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			copyComponent<Components::SkeletonRigComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);

			

			auto& viewSkyLights = srcLvlRegistry.view<Components::SkyLightComponent>();
			for (auto constraintActor : viewSkyLights) {
				if (srcLvlRegistry.get<Components::UUIDComponent>(constraintActor).getInstancerUUID() != 0) continue; // skip prefab children
				UUID uuid = getTrueActorUUID(
					srcLvlRegistry.get<Components::UUIDComponent>(constraintActor).getInstancerUUID(),
					srcLvlRegistry.get<Components::UUIDComponent>(constraintActor).getScopedID()
				);

				auto& cnstr = srcLvlRegistry.get<Components::SkyLightComponent>(constraintActor);
				if (!srcLvlRegistry.all_of(cnstr.skyboxActor)) {
					dstLvlRegistry.get<Components::SkyLightComponent>(enttMap[uuid]).skyboxActor = entt::null;
				} else {
					UUID oldSkyboxActor = getTrueActorUUID(
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.skyboxActor).getInstancerUUID(),
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.skyboxActor).getScopedID()
					);
					dstLvlRegistry.get<Components::SkyLightComponent>(enttMap[uuid]).skyboxActor = enttMap[oldSkyboxActor];
				}
				if (!srcLvlRegistry.all_of(cnstr.skyAtmosphereActor)) {
					dstLvlRegistry.get<Components::SkyLightComponent>(enttMap[uuid]).skyAtmosphereActor = entt::null;
				} else {
					UUID oldSkyAtmosphereActor = getTrueActorUUID(
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.skyAtmosphereActor).getInstancerUUID(),
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.skyAtmosphereActor).getScopedID()
					);
					dstLvlRegistry.get<Components::SkyLightComponent>(enttMap[uuid]).skyAtmosphereActor = enttMap[oldSkyAtmosphereActor];
				}
			}

			auto& viewSkyAtmospheres = srcLvlRegistry.view<Components::SkyAtmosphereComponent>();
			for (auto constraintActor : viewSkyAtmospheres) {
				if (srcLvlRegistry.get<Components::UUIDComponent>(constraintActor).getInstancerUUID() != 0) continue; // skip prefab children
				UUID uuid = getTrueActorUUID(
					srcLvlRegistry.get<Components::UUIDComponent>(constraintActor).getInstancerUUID(),
					srcLvlRegistry.get<Components::UUIDComponent>(constraintActor).getScopedID()
				);
				auto& cnstr = srcLvlRegistry.get<Components::SkyAtmosphereComponent>(constraintActor);
				if (!srcLvlRegistry.all_of(cnstr.directionalLightActor)) {
					dstLvlRegistry.get<Components::SkyAtmosphereComponent>(enttMap[uuid]).directionalLightActor = entt::null;
				} else {
					UUID oldDirectionalLightActor = getTrueActorUUID(
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.directionalLightActor).getInstancerUUID(),
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.directionalLightActor).getScopedID()
					);
					dstLvlRegistry.get<Components::SkyAtmosphereComponent>(enttMap[uuid]).directionalLightActor = enttMap[oldDirectionalLightActor];
				}
			}

			for (auto& [uuid, entity] : enttMap) {
				entt::entity handle = other->actorMap[getTrueActorUUID(NO_PREFAB_INHERIT_ACTOR, uuid)]; // prefab inherited actors will be regenerated
				Actor oldActor = { handle , other.get() };
				if (oldActor.hasRelationship()) { // handle relationships due to special copy/unload case		
					Components::RelationshipComponent& oldRelation = srcLvlRegistry.get<Components::RelationshipComponent>(handle);
					Components::RelationshipComponent newRelation{};
					if (oldRelation.parentActor != entt::null)
						newRelation.parentActor = enttMap[Actor(oldRelation.parentActor, other.get()).getGlobalUUID()];
					for (entt::entity& child : oldRelation.childActors) {
						Actor childActor = Actor(child, other.get());
						if (childActor.isPrefabChild() != 0) continue;
						newRelation.childActors.push_back(enttMap[childActor.getGlobalUUID()]);
					}
					if (oldActor.hasComponent<Components::PrefabComponent>()) {
						VectorUtils::merge(dstLvlRegistry.get<Components::RelationshipComponent>(handle).childActors, newRelation.childActors);
					} else {
						dstLvlRegistry.emplace_or_replace<Components::RelationshipComponent>(entity, newRelation);
					}
				}
			}
			auto& viewPhysicsConstraint = srcLvlRegistry.view<Components::PhysicsConstraintComponent>();
			for (auto constraintActor : viewPhysicsConstraint) {
				if (srcLvlRegistry.get<Components::UUIDComponent>(constraintActor).getInstancerUUID() != 0) continue; // skip prefab children
				UUID uuid = getTrueActorUUID(
					srcLvlRegistry.get<Components::UUIDComponent>(constraintActor).getInstancerUUID(),
					srcLvlRegistry.get<Components::UUIDComponent>(constraintActor).getScopedID()
				);
				auto& cnstr = srcLvlRegistry.get<Components::PhysicsConstraintComponent>(constraintActor);
				if (!srcLvlRegistry.all_of(cnstr.actorA) || !srcLvlRegistry.all_of(cnstr.actorB)) {
					dstLvlRegistry.get<Components::PhysicsConstraintComponent>(enttMap[uuid]).actorA = entt::null;
					dstLvlRegistry.get<Components::PhysicsConstraintComponent>(enttMap[uuid]).actorB = entt::null;
					continue;
				}

				UUID oldActorA = getTrueActorUUID(
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.actorA).getInstancerUUID(),
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.actorA).getScopedID()
				); 
				UUID oldActorB = getTrueActorUUID(
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.actorB).getInstancerUUID(),
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.actorB).getScopedID()
				);
				dstLvlRegistry.get<Components::PhysicsConstraintComponent>(enttMap[uuid]).actorA = enttMap[oldActorA];
				dstLvlRegistry.get<Components::PhysicsConstraintComponent>(enttMap[uuid]).actorB = enttMap[oldActorB];
			}
			auto& viewSwaybars = srcLvlRegistry.view<Components::SwayBarComponent>();
			for (auto constraintActor : viewSwaybars) {
				if (srcLvlRegistry.get<Components::UUIDComponent>(constraintActor).getInstancerUUID() != 0)continue; // skip prefab children
				UUID uuid = getTrueActorUUID(
					srcLvlRegistry.get<Components::UUIDComponent>(constraintActor).getInstancerUUID(),
					srcLvlRegistry.get<Components::UUIDComponent>(constraintActor).getScopedID()
				);
				auto& cnstr = srcLvlRegistry.get<Components::SwayBarComponent>(constraintActor);
				if (!srcLvlRegistry.all_of(cnstr.chassis)) {
					dstLvlRegistry.get<Components::SwayBarComponent>(enttMap[uuid]).chassis = entt::null;
				} else {
					UUID oldActor = getTrueActorUUID(
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.chassis).getInstancerUUID(),
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.chassis).getScopedID()
					);
					dstLvlRegistry.get<Components::SwayBarComponent>(enttMap[uuid]).chassis = enttMap[oldActor];
				}
				if (!srcLvlRegistry.all_of(cnstr.wheels[0])) {
					dstLvlRegistry.get<Components::SwayBarComponent>(enttMap[uuid]).wheels[0] = entt::null;
				} else {
					UUID oldActor = getTrueActorUUID(
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.wheels[0]).getInstancerUUID(),
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.wheels[0]).getScopedID()
					);
					dstLvlRegistry.get<Components::SwayBarComponent>(enttMap[uuid]).wheels[0] = enttMap[oldActor];
				}
				if (!srcLvlRegistry.all_of(cnstr.wheels[1])) {
					dstLvlRegistry.get<Components::SwayBarComponent>(enttMap[uuid]).wheels[1] = entt::null;
				} else {
					UUID oldActor = getTrueActorUUID(
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.wheels[1]).getInstancerUUID(),
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.wheels[1]).getScopedID()
					);
					dstLvlRegistry.get<Components::SwayBarComponent>(enttMap[uuid]).wheels[1] = enttMap[oldActor];
				}
				if (!srcLvlRegistry.all_of(cnstr.suspension[0])) {
					dstLvlRegistry.get<Components::SwayBarComponent>(enttMap[uuid]).suspension[0] = entt::null;
				} else {
					UUID oldActor = getTrueActorUUID(
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.suspension[0]).getInstancerUUID(),
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.suspension[0]).getScopedID()
					);
					dstLvlRegistry.get<Components::SwayBarComponent>(enttMap[uuid]).suspension[0] = enttMap[oldActor];
				}
				if (!srcLvlRegistry.all_of(cnstr.suspension[1])) {
					dstLvlRegistry.get<Components::SwayBarComponent>(enttMap[uuid]).suspension[1] = entt::null;
				} else {
					UUID oldActor = getTrueActorUUID(
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.suspension[1]).getInstancerUUID(),
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.suspension[1]).getScopedID()
					);
					dstLvlRegistry.get<Components::SwayBarComponent>(enttMap[uuid]).suspension[1] = enttMap[oldActor];
				}
			}
			auto& viewSuspensions = srcLvlRegistry.view<Components::IndependentSuspensionComponent>();
			for (auto constraintActor : viewSuspensions) {
				if (srcLvlRegistry.get<Components::UUIDComponent>(constraintActor).getInstancerUUID() != 0) continue; // skip prefab children
				UUID uuid = getTrueActorUUID(
					srcLvlRegistry.get<Components::UUIDComponent>(constraintActor).getInstancerUUID(),
					srcLvlRegistry.get<Components::UUIDComponent>(constraintActor).getScopedID()
				);
				auto& cnstr = srcLvlRegistry.get<Components::IndependentSuspensionComponent>(constraintActor);
				if (!srcLvlRegistry.all_of(cnstr.chassis)) {
					dstLvlRegistry.get<Components::IndependentSuspensionComponent>(enttMap[uuid]).chassis = entt::null;
				} else {
					UUID oldActor = getTrueActorUUID(
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.chassis).getInstancerUUID(),
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.chassis).getScopedID()
					);
					dstLvlRegistry.get<Components::IndependentSuspensionComponent>(enttMap[uuid]).chassis = enttMap[oldActor];
				}
				if (!srcLvlRegistry.all_of(cnstr.wheel)) {
					dstLvlRegistry.get<Components::IndependentSuspensionComponent>(enttMap[uuid]).wheel = entt::null;
				} else {
					UUID oldActor = getTrueActorUUID(
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.wheel).getInstancerUUID(),
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.wheel).getScopedID()
					);
					dstLvlRegistry.get<Components::IndependentSuspensionComponent>(enttMap[uuid]).wheel = enttMap[oldActor];
				}
			}
			auto& viewVehicles = srcLvlRegistry.view<Components::VehicleComponent>();
			for (auto constraintActor : viewSuspensions) {
				if (srcLvlRegistry.get<Components::UUIDComponent>(constraintActor).getInstancerUUID() != 0) continue; // skip prefab children
				UUID uuid = getTrueActorUUID(
					srcLvlRegistry.get<Components::UUIDComponent>(constraintActor).getInstancerUUID(),
					srcLvlRegistry.get<Components::UUIDComponent>(constraintActor).getScopedID()
				);
				auto& cnstr = srcLvlRegistry.get<Components::VehicleComponent>(constraintActor);
				if (!srcLvlRegistry.all_of(cnstr.chassis)) {
					dstLvlRegistry.get<Components::VehicleComponent>(enttMap[uuid]).chassis = entt::null;
				} else {
					UUID oldActor = getTrueActorUUID(
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.chassis).getInstancerUUID(),
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.chassis).getScopedID()
					);
					dstLvlRegistry.get<Components::VehicleComponent>(enttMap[uuid]).chassis = enttMap[oldActor];
				}
				if (!srcLvlRegistry.all_of(cnstr.rearSwayBar)) {
					dstLvlRegistry.get<Components::VehicleComponent>(enttMap[uuid]).rearSwayBar = entt::null;
				} else {
					UUID oldActor = getTrueActorUUID(
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.rearSwayBar).getInstancerUUID(),
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.rearSwayBar).getScopedID()
					);
					dstLvlRegistry.get<Components::VehicleComponent>(enttMap[uuid]).rearSwayBar = enttMap[oldActor];
				}
				if (!srcLvlRegistry.all_of(cnstr.frontSwayBar)) {
					dstLvlRegistry.get<Components::VehicleComponent>(enttMap[uuid]).frontSwayBar = entt::null;
				} else {
					UUID oldActor = getTrueActorUUID(
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.frontSwayBar).getInstancerUUID(),
					srcLvlRegistry.get<Components::UUIDComponent>(cnstr.frontSwayBar).getScopedID()
					);
					dstLvlRegistry.get<Components::VehicleComponent>(enttMap[uuid]).frontSwayBar = enttMap[oldActor];
				}
			}
			// Secret components
			copyComponent<Components::WorldSceneInfoComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			//copyComponent<Components::VisibilityComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			//copyComponent<Components::IsVisibileComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			//copyComponent<Components::MobilityComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			//copyComponent<Components::StaticActorComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			//copyComponent<Components::DynamicActorComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap);
			//copyComponent<Components::RelationshipComponent>(*newLvl, dstLvlRegistry, srcLvlRegistry, enttMap); // already handled

			auto tcViewDst = dstLvlRegistry.view<Components::TransformComponent>();
			for (auto ent : tcViewDst) {
				Components::TransformComponent& tcComp = dstLvlRegistry.get<Components::TransformComponent>(ent);
				tcComp.dirty = true;
			}
			
			newLvl->registry.each([&](auto actorGUID) { Actor actor = { actorGUID, newLvl.get() };
				switch (actor.getComponent<Components::MobilityComponent>().mobility) {
				case(Mobility::Static):
					actor.makeStatic();
					break;
				case(Mobility::Stationary):
					actor.makeStationary();
					break;
				case(Mobility::Dynamic):
					actor.makeDynamic();
					break;
				}
				switch (actor.getComponent<Components::VisibilityComponent>().isVisible) {
				case(true):
					actor.makeVisible();
					break;
				case(false):
					actor.makeInvisible();
					break;
				}
			});

			newLvl->rebuildTransforms();

			return newLvl;
		}

		Actor Level::createActor(UUID scopeUUID, const std::string& name) {
			return createActorWithUUID(scopeUUID, UUID(), name);
		}

		Actor Level::createActorWithUUID(UUID scopeUUID, UUID id, const std::string& name) {
			SHARD3D_ASSERT(this != nullptr && "Level does not exist! Cannot create actors!");
			SHARD3D_ASSERT(!doesActorWithUUIDExist(scopeUUID, id) && "Actor MUST have a unique combination of instancer UUID ({0}) and scoped ID ({1})!!!", scopeUUID, id);
			Actor actor = { registry.create(), this };
			actor.addComponent<Components::UUIDComponent>(scopeUUID, id);
			actor.addComponent<Components::TagComponent>().tag = name;
			actor.addComponent<Components::TransformComponent>();
			actor.addComponent<Components::VisibilityComponent>();
			actor.addComponent<Components::IsVisibileComponent>();
			actor.addComponent<Components::MobilityComponent>();
			actor.makeStatic();// actors static by default

			actorMap[getTrueActorUUID(scopeUUID, id)] = actor;

			return actor;
		}

		Actor Level::duplicateActor(Actor actor, bool addCopyTag, UUID scopeUUID, UUID id) {
			if (doesActorWithUUIDExist(scopeUUID, id)) { // already duplicated (cases where referenced actors are needed before their planned creation)
				return getActorFromUUID(scopeUUID, id);
			}
			std::string nTag = actor.getTag();
			if (addCopyTag) {
				if (strUtils::contains(nTag, "(Copy")) {
					auto UUIDView = registry.view<Components::TagComponent>();
					int copyIndex{};
					for (;;) {
						copyIndex++;
						bool exists = false;
						for (auto ac : UUIDView) {
							if (UUIDView.get<Components::TagComponent>(ac).tag == nTag) {
								nTag = actor.getTag().substr(0, actor.getTag().find_last_of("(Copy") - 5) + " (Copy " + std::to_string(copyIndex) + ")";
								exists = true;
								break;
							}
						}
						if (!exists) { break; }
					}
				}
				else {
					nTag = actor.getTag() + " (Copy)";
				}
			}
			if (actor.hasComponent<Components::PrefabComponent>()) {
				Actor newActor = prefabManager->instPrefab(this, scopeUUID, id, actor.getComponent<Components::PrefabComponent>().prefab, nTag);
				newActor.killComponent<Components::TransformComponent>(); // copy transform
				newActor.addComponent<Components::TransformComponent>(actor.getTransform());
				return newActor;
			}
			Actor newActor = createActorWithUUID(scopeUUID, id, nTag);

			if (actor.hasRelationship()) { // handle relationships due to special copy/unload case		
				Components::RelationshipComponent& oldRelation = actor.getComponent<Components::RelationshipComponent>();
				Components::RelationshipComponent newRelation{};
				for (entt::entity oldChild : oldRelation.childActors) {
					Actor oldChildActor = Actor(oldChild, actor.level);  // use old level in case copying to new level
					UUID childID = getTrueActorUUID(oldChildActor.getInstancerUUID(), oldChildActor.getScopedID());
					bool hasBP = oldChildActor.hasComponent<Components::PrefabComponent>();
					Actor newChild = duplicateActor(oldChildActor, addCopyTag, scopeUUID, childID);
					newRelation.childActors.push_back(newChild);
					newChild.getComponent<Components::RelationshipComponent>().parentActor = newActor.actorHandle;
				}
				newActor.addComponent<Components::RelationshipComponent>(newRelation);
			}

			newActor.killComponent<Components::TransformComponent>(); // copy transform
			newActor.addComponent<Components::TransformComponent>(actor.getTransform());
			
			copyComponentIfExists<Components::VisibilityComponent>(actor, newActor);

			switch (actor.getComponent<Components::MobilityComponent>().mobility) {
			case(Mobility::Static):
				newActor.makeStatic();
				break;
			case(Mobility::Stationary):
				newActor.makeStationary();
				break;
			case(Mobility::Dynamic):
				newActor.makeDynamic();
				break;
			}
			switch (actor.getComponent<Components::VisibilityComponent>().isVisible) {
			case(true):
				newActor.makeVisible();
				break;
			case(false):
				newActor.makeInvisible();
				break;
			}


			copyComponentIfExists<Components::BillboardComponent>(actor, newActor);
			copyComponentIfExists<Components::Mesh3DComponent>(actor, newActor);
			copyComponentIfExists<Components::AudioComponent>(actor, newActor);
			copyComponentIfExists<Components::CameraComponent>(actor, newActor);
			copyComponentIfExists<Components::SkyAtmosphereComponent>(actor, newActor);
			copyComponentIfExists<Components::SkyLightComponent>(actor, newActor);
			copyComponentIfExists<Components::DirectionalLightComponent>(actor, newActor);
			copyComponentIfExists<Components::PointLightComponent>(actor, newActor);
			copyComponentIfExists<Components::SpotLightComponent>(actor, newActor);
			copyComponentIfExists<Components::Rigidbody3DComponent>(actor, newActor);
			copyComponentIfExists<Components::PhysicsConstraintComponent>(actor, newActor);
			copyComponentIfExists<Components::SwayBarComponent>(actor, newActor);
			copyComponentIfExists<Components::IndependentSuspensionComponent>(actor, newActor);
			copyComponentIfExists<Components::VehicleComponent>(actor, newActor);
			copyComponentIfExists<Components::PostFXComponent>(actor, newActor);
			copyComponentIfExists<Components::BoxReflectionCaptureComponent>(actor, newActor);
			copyComponentIfExists<Components::VirtualPointLightComponent>(actor, newActor);
			copyComponentIfExists<Components::PlanarReflectionComponent>(actor, newActor);
			copyComponentIfExists<Components::BoxAmbientOcclusionVolumeComponent>(actor, newActor);
			copyComponentIfExists<Components::BoxVolumeComponent>(actor, newActor);
			copyComponentIfExists<Components::DeferredDecalComponent>(actor, newActor);
			copyComponentIfExists<Components::SkeletonRigComponent>(actor, newActor);

			if (actor.hasComponent<Components::PhysicsConstraintComponent>()) { // they reference actors so they need to be fixed too
				Components::PhysicsConstraintComponent& oldConstraint = actor.getComponent<Components::PhysicsConstraintComponent>();
				Components::PhysicsConstraintComponent& newConstraint = newActor.getComponent<Components::PhysicsConstraintComponent>();
				for (int i = 0; i < 2; i++) {
					Actor oldActor = Actor(i == 0 ? oldConstraint.actorA : oldConstraint.actorB, actor.level);  // use old level in case copying to new level

					UUID globalUUID = getTrueActorUUID(scopeUUID, oldActor.getScopedID());
					bool createdYet = doesActorWithUUIDExist(scopeUUID, oldActor.getScopedID());
					Actor newReferencedActor;
					if (!createdYet) {
						newReferencedActor = duplicateActor(oldActor, addCopyTag, scopeUUID, oldActor.getScopedID());
					} else {
						newReferencedActor = { actorMap.at(globalUUID), this };
					}
					if (i == 0) {
						newConstraint.actorA = newReferencedActor.actorHandle;
					} else {
						newConstraint.actorB = newReferencedActor.actorHandle;
					}
				}
			}
			if (actor.hasComponent<Components::SwayBarComponent>()) { // they reference actors so they need to be fixed too
				Components::SwayBarComponent& oldSwaybar = actor.getComponent<Components::SwayBarComponent>();
				Components::SwayBarComponent& newSwaybar = newActor.getComponent<Components::SwayBarComponent>();
				entt::entity partSrc[5]{
					oldSwaybar.chassis,
					oldSwaybar.wheels[0],
					oldSwaybar.wheels[1],
					oldSwaybar.suspension[0],
					oldSwaybar.suspension[1]
				};
				entt::entity* partDst[5]{
					&newSwaybar.chassis,
					&newSwaybar.wheels[0],
					&newSwaybar.wheels[1],
					&newSwaybar.suspension[0],
					&newSwaybar.suspension[1]
				};
				for (int i = 0; i < 5; i++) {
					Actor oldActor = Actor(partSrc[i], actor.level);  // use old level in case copying to new level
					UUID globalUUID = getTrueActorUUID(scopeUUID, oldActor.getScopedID());
					bool createdYet = doesActorWithUUIDExist(scopeUUID, oldActor.getScopedID());
					Actor newReferencedActor;
					if (!createdYet) {
						newReferencedActor = duplicateActor(oldActor, addCopyTag, scopeUUID, oldActor.getScopedID());
					} else {
						newReferencedActor = { actorMap.at(globalUUID), this };
					}
					*partDst[i] = newReferencedActor.actorHandle;
				}
			}
			if (actor.hasComponent<Components::IndependentSuspensionComponent>()) { // they reference actors so they need to be fixed too
				Components::IndependentSuspensionComponent& oldSus= actor.getComponent<Components::IndependentSuspensionComponent>();
				Components::IndependentSuspensionComponent& newSus= newActor.getComponent<Components::IndependentSuspensionComponent>();
				for (int i = 0; i < 2; i++) {
					Actor oldActor = Actor(i == 0 ? oldSus.chassis : oldSus.wheel, actor.level);  // use old level in case copying to new level

					UUID globalUUID = getTrueActorUUID(scopeUUID, oldActor.getScopedID());
					bool createdYet = doesActorWithUUIDExist(scopeUUID, oldActor.getScopedID());
					Actor newReferencedActor;
					if (!createdYet) {
						newReferencedActor = duplicateActor(oldActor, addCopyTag, scopeUUID, oldActor.getScopedID());
					} else {
						newReferencedActor = { actorMap.at(globalUUID), this };
					}
					if (i == 0) {
						newSus.chassis = newReferencedActor.actorHandle;
					} else {
						newSus.wheel = newReferencedActor.actorHandle;
					}
				}
			}
			if (actor.hasComponent<Components::VehicleComponent>()) { // they reference actors so they need to be fixed too
				Components::VehicleComponent& oldVeh = actor.getComponent<Components::VehicleComponent>();
				Components::VehicleComponent& newVeh = newActor.getComponent<Components::VehicleComponent>();
				entt::entity partSrc[3]{
					oldVeh.chassis,
					oldVeh.rearSwayBar,
					oldVeh.frontSwayBar
				};
				entt::entity* partDst[3]{
					&newVeh.chassis,
					&newVeh.rearSwayBar,
					&newVeh.frontSwayBar
				};
				for (int i = 0; i < 3; i++) {
					Actor oldActor = Actor(partSrc[i], actor.level);  // use old level in case copying to new level
					UUID globalUUID = getTrueActorUUID(scopeUUID, oldActor.getScopedID());
					bool createdYet = doesActorWithUUIDExist(scopeUUID, oldActor.getScopedID());
					Actor newReferencedActor;
					if (!createdYet) {
						newReferencedActor = duplicateActor(oldActor, addCopyTag, scopeUUID, oldActor.getScopedID());
					} else {
						newReferencedActor = { actorMap.at(globalUUID), this };
					}
					*partDst[i] = newReferencedActor.actorHandle;
				}
			}

			return newActor;
		}

		void Level::runGarbageCollector() {
			for (int i = actorKillQueue.size() - 1; i >= 0; i--) { // reverse iterator due to pop back
				Actor actor = actorKillQueue[i];
				if (actor.isInvalid()) {
					actorKillQueue.pop_back();
					continue;
				}
				UUID uuid = getTrueActorUUID(actor.getInstancerUUID(), actor.getScopedID());
				actorMap.erase(uuid);
				SHARD3D_LOG("Destroying actor '{0}'", actor.getTag());
				registry.destroy(actor.actorHandle);
				actorKillQueue.pop_back();
			}
		}

		void Level::cleanup() {
			componentsCreated.clear();
			componentsDestroyed.clear();
		}

		Actor Level::getActorFromUUID(UUID scope, UUID id) {
			UUID uuid = getTrueActorUUID(scope, id);
			if (actorMap.find(uuid) != actorMap.end()) {
				return { actorMap.at(uuid), this };
			} else {
				SHARD3D_ERROR("Failed to find actor with UUID '{0}'!", uuid);
				throw std::exception("kys");
				return Actor();
			}
		}

		Actor Level::getActorFromUUID(Actor prefab, UUID id) {
			UUID uuid = getTrueActorUUID(getTrueActorUUID(prefab.getInstancerUUID(), prefab.getScopedID()), id);
			if (actorMap.find(uuid) != actorMap.end()) {
				return { actorMap.at(uuid), this };
			} else {
				SHARD3D_ERROR("Failed to find actor with ID '{0}' in prefab `{1}`!", id, prefab.getTag());
				return Actor();
			}
		}

		Actor Level::getActorFromTag(const std::string& tag) {
			auto UUIDView = registry.view<Components::TagComponent>();
			for (auto actor : UUIDView) {
				if (UUIDView.get<Components::TagComponent>(actor).tag == tag) {
					return Actor{ actor, this };
				}
			}
			SHARD3D_ERROR("Failed to find actor {0}!", tag);
			return Actor();
		}

		void Level::setPossessedCameraActor(Actor actor) {
			if (!actor.hasComponent<Components::CameraComponent>()) {
				SHARD3D_ERROR("Cannot possess an actor without a camera!!");
				return;
			}
			possessedCameraActor = actor;
		}

		Actor Level::getPossessedCameraActor() {
			if (possessedCameraActor != entt::null) return { possessedCameraActor, this };
			SHARD3D_FATAL("No possessed camera found!!!!");
		}

		S3DCamera& Level::getPossessedCamera() {
			return getPossessedCameraActor().getComponent<Components::CameraComponent>().camera;
		}
		Components::WorldSceneInfoComponent* Level::getWorldSceneInfo() {
			return &getActorFromUUID(NO_PREFAB_INHERIT_ACTOR, 1).getComponent<Components::WorldSceneInfoComponent>();
		}

		bool Level::doesActorWithUUIDExist(UUID scopeUUID, UUID id) {
			UUID globalUUID = getTrueActorUUID(scopeUUID, id);
			return actorMap.find(globalUUID) != actorMap.end();
		}

		void Level::reparentPrefabOwnership(UUID parentingScope, Actor child) {
			actorMap.erase(child.getGlobalUUID());
			child.nocheck_getComponent<Components::UUIDComponent>().instancer = parentingScope;
			actorMap[child.getGlobalUUID()] = child;

			UUID nextScope = parentingScope;
			if (child.hasComponent<Components::PrefabComponent>()) {
				nextScope = getTrueActorUUID(parentingScope, child.getScopedID());
			}
			if (child.hasRelationship()) {
				for (entt::entity child : child.getComponent<Components::RelationshipComponent>().childActors) {
					reparentPrefabOwnership(nextScope, { child , this });
				}
			}
		}

		void Level::parentActor(Actor child, Actor parent){
			SHARD3D_ASSERT(child != parent && "Cannot parent actor to itself!");
			child.addComponent<Components::RelationshipComponent>();
			parent.addComponent<Components::RelationshipComponent>();
			child.getComponent<Components::RelationshipComponent>().parentActor = parent.actorHandle;
			parent.getComponent<Components::RelationshipComponent>().childActors.push_back(child.actorHandle);
			if (auto mobility = parent.getMobility(); mobility > child.getMobility()) {
				switch (mobility) {
				case (Mobility::Stationary): child.makeStationary(); break;
				case (Mobility::Dynamic): child.makeDynamic(); break;
				// static can have anything
				}
			}
			if (parent.isPrefabChild() || parent.hasComponent<Components::PrefabComponent>()) {
				reparentPrefabOwnership(parent.getGlobalUUID(), child);
			}

			SHARD3D_LOG("Parented actor {0} to {1}", parent.getScopedID(), child.getScopedID());
		}
		void Level::reparentActor(Actor child, Actor newParent) {
			SHARD3D_ASSERT(child != newParent && "Cannot reparent actor to itself!");
			SHARD3D_ASSERT(child.hasRelationship() && "Cannot reparent actor with no relationship component!");
			SHARD3D_ASSERT(child.getComponent<Components::RelationshipComponent>().parentActor != newParent.actorHandle && "why?");
			auto& crc = child.getComponent<Components::RelationshipComponent>();
			VectorUtils::eraseItem(Actor(crc.parentActor, this).getComponent<Components::RelationshipComponent>().childActors, child.actorHandle);
			
			parentActor(child, newParent);
		}

		void Level::hideAllEditorOnlyActors() {
			auto visView = registry.view<Components::VisibilityComponent>();
			for (auto& ent : visView) {
				Actor actor = { ent, this };
				auto& vsc = actor.getComponent<Components::VisibilityComponent>();
				if (vsc.editorOnly) actor.killComponent<Components::IsVisibileComponent>();
			}
		}

		void Level::showAllEditorOnlyActors() {
			auto visView = registry.view<Components::VisibilityComponent>();
			for (auto& ent : visView) {
				Actor actor = { ent, this };
				auto& vsc = actor.getComponent<Components::VisibilityComponent>();
				if (!vsc.editorOnly) continue;
				actor.addComponent<Components::IsVisibileComponent>();
			}
		}

		void Level::showActorChildren(Actor parent) {
			SHARD3D_ASSERT(parent.hasRelationship() && "Cannot hide children of an actor without relationships!");
			for (auto& child : parent.getComponent<Components::RelationshipComponent>().childActors) {
				Actor childActor = { child, this };
				showActorChildren(childActor);
				childActor.addComponent<Components::IsVisibileComponent>();
			}
		}

		void Level::hideActorChildren(Actor parent) {
			SHARD3D_ASSERT(parent.hasRelationship() && "Cannot hide children of an actor without relationships!");
			for (auto& child : parent.getComponent<Components::RelationshipComponent>().childActors) {
				Actor childActor = { child, this };
				hideActorChildren(childActor);
				childActor.killComponent<Components::IsVisibileComponent>();
			}
		}

		//void Level::begin() {
		//	SHARD3D_LOG("Level: Initializing scripts");
		//
		//	SHARD3D_LOG("Beginning simulation");
		//}

		//void Level::tick(float dt) {
		//
		//}

		void Level::simulationStateCallback() {
			if (simulationState == PlayState::Paused) {
				possessedCameraActor_old = possessedCameraActor;
				setPossessedCameraActor(getActorFromUUID(NO_PREFAB_INHERIT_ACTOR, 0));
				auto audioView = registry.view<Components::AudioComponent>();
				for (auto actor : audioView) {
					audioView.get<Components::AudioComponent>(actor).pause();
				}
				wasPaused = true;
				SHARD3D_LOG("LEVEL: PAUSED");
			}
			if (simulationState == PlayState::Playing) {
				setPossessedCameraActor({ possessedCameraActor_old, this });
				auto audioView = registry.view<Components::AudioComponent>();
				for (auto actor : audioView) {
					audioView.get<Components::AudioComponent>(actor).resume();
				}
				wasPaused = false;
				SHARD3D_LOG("Level: Resumed");
			}
		}

		//void Level::end() {
		//	SHARD3D_LOG("Ending Level Simulation");
		//	SHARD3D_LOG("Reloading level");
		//}
		void Level::killActor(Actor actor) {
			actorKillQueue.emplace_back(actor);
			for (size_t componentHashCode : actorOwnedComponents.at(actor)) {
				componentsDestroyed[componentHashCode].push_back(actor);
			}
			actorOwnedComponents.erase(actor);
			if (simulationState == PlayState::Playing) {
				if (actor.hasComponent<Components::PrefabComponent>()) {
					//ScriptEngine::actorScript().killEvent(actor);
				}
			}

			if (actor.hasComponent<Components::RelationshipComponent>()) {
				if (actor.getComponent<Components::RelationshipComponent>().parentActor != entt::null) {
					Actor parentActor = Actor(actor.getComponent<Components::RelationshipComponent>().parentActor, this);
					if (std::find(actorKillQueue.begin(), actorKillQueue.end(), parentActor) == actorKillQueue.end()) {
						std::vector<entt::entity>& childVector = parentActor.getComponent<Components::RelationshipComponent>().childActors;
						std::vector<entt::entity>::iterator q = childVector.begin();	
						for (int i = 0; i < childVector.size(); i++) {
							if (childVector.at(i) == actor) break;
							q++;
						}
						childVector.erase(q);
					}
				}
				std::vector<entt::entity> childActors = actor.getComponent<Components::RelationshipComponent>().childActors; // copy vector
				for (auto child : childActors) {
					killActor({ child, this });
				}
			}
		}

		void Level::rebuildTransforms() {
			auto view = registry.view<Components::TransformComponent>();
			for (auto obj : view) {
				ECS::Actor actor = { obj, this };
				auto& tc = actor.getTransform();
				if (!tc.dirty) {
					continue;
				}
				if (actor.hasComponent<Components::RelationshipComponent>()) {
					rebuildRelations(actor);
					continue;
				}
				// regular actors with no relations get calculated normally if dirty
				tc.transformMatrix = tc.calculateMat4();
				tc.normalMatrix = tc.calculateNormalMatrix();
				tc.dirty = false;
			}
			
		}

		void Level::rebuildRelations(Actor child) {
			auto& crc = child.getComponent<Components::RelationshipComponent>();
			auto& ctc = child.getComponent<Components::TransformComponent>();
			{
				glm::mat4 parentMatrix{ 1.f };
				glm::mat4 parentMatrixNoScale{ 1.f };
				glm::mat3 parentNormalNoScale{ 1.f };
				glm::vec3 parentScale{ 1.f };
				glm::vec3 parentInvScale{ 1.f };
				if (crc.parentActor != entt::null) {
					Actor parentActor = { crc.parentActor, this };
					auto& ptc = parentActor.getComponent<Components::TransformComponent>();

					parentScale.x = glm::length(ptc.transformMatrix[0]);
					parentScale.y = glm::length(ptc.transformMatrix[1]);
					parentScale.z = glm::length(ptc.transformMatrix[2]);

					parentMatrix = ptc.transformMatrix;
					parentInvScale = 1.f / parentScale;
					parentMatrixNoScale = glm::mat4(
						{ ptc.transformMatrix[0] * parentInvScale.x },
						{ ptc.transformMatrix[1] * parentInvScale.y },
						{ ptc.transformMatrix[2] * parentInvScale.z },
						ptc.transformMatrix[3]
					);
					parentNormalNoScale = glm::mat3(
						{ ptc.normalMatrix[0] * parentScale.x },
						{ ptc.normalMatrix[1] * parentScale.y },
						{ ptc.normalMatrix[2] * parentScale.z }
					);
				}

				glm::mat4 transform = ctc.calculateMat4();
				ctc.transformMatrix = glm::mat3(parentMatrixNoScale) * glm::mat3(transform);
				ctc.transformMatrix[3] = 
					parentMatrix[0] * transform[3].x + 
					parentMatrix[1] * transform[3].y + 
					parentMatrix[2] * transform[3].z + 
					parentMatrix[3];
				 ctc.transformMatrix = glm::mat4(
					{ ctc.transformMatrix[0] * parentScale.x },
					{ ctc.transformMatrix[1] * parentScale.y },
					{ ctc.transformMatrix[2] * parentScale.z },
					ctc.transformMatrix[3]
				);
				glm::mat3 normal = parentNormalNoScale * ctc.calculateNormalMatrix();
				ctc.normalMatrix[0] = normal[0] * parentInvScale[0];
				ctc.normalMatrix[1] = normal[1] * parentInvScale[1];
				ctc.normalMatrix[2] = normal[2] * parentInvScale[2];

				ctc.dirty = false;
			}
			for (auto& subchild : crc.childActors) {
				rebuildRelations({ subchild, this });
			}
		}

		std::vector<Actor> Level::getAllChildrenBelongingToActor(Actor actor) {
			std::vector<Actor> vec{};
			if (actor.hasRelationship()) {
				for (auto& child : actor.getComponent<Components::RelationshipComponent>().childActors) {
					Actor childActor = { child, this };
					vec.push_back(childActor);
					VectorUtils::merge(vec, getAllChildrenBelongingToActor(childActor));
				}
			}
			return vec;
		}
		std::vector<Actor> Level::recursiveActorFetch(Actor actor) {
			std::vector<Actor> vec{};
			if (actor.hasRelationship()) {
				for (auto& child : actor.getComponent<Components::RelationshipComponent>().childActors) {
					Actor childActor = { child, this };
					if (childActor.hasComponent<Components::PrefabComponent>()) continue; // ignore prefabs, they are special
					vec.push_back(childActor);
					VectorUtils::merge(vec, recursiveActorFetch(childActor));
				}
			}
			return vec;
		}

		void Level::killEverything() {
			registry.each([&](auto actorUUID) { ECS::Actor actor = { actorUUID, this };
				if (actor.isInvalid()) return;
				actorKillQueue.emplace_back(actor);
			});
		}
	}
}