#pragma once
#include "../../vulkan_abstr.h"
#include "material.h"
#include "primitives.h"
#include "../asset/assetid.h"
#include <half.h>
extern "C" {
	struct aiNode;
	struct aiScene;
	struct aiMesh;
}
namespace Shard3D {
	inline namespace Resources {
		struct Model3DImportInfo {
			bool calculateTangents = true;
			bool combineMeshes = true;
			bool convertPBRtoSRM3D = true; // physically based rendering to simplified render model 3D
			bool createMaterials = true;
			float scaleFactor = 1.0f;
			glm::vec3 importEulerRotation{ 0.f, 0.f, 0.f };
			glm::vec3 importTranslation{ 0.f, 0.f, 0.f };

			bool skeletalMesh = false;
			bool importMorphTargets = true;
			bool importBones = true;
			bool importAnimations = false;
		};

		struct SubmeshData {
			std::vector<Vertex> vertices{};
			std::vector<uint32_t> indices{};
			AssetID materialAsset{ AssetID::null() };
			VolumeBounds volumeBounds{};
			SphereBounds sphereBounds{};
		};
		struct ModelInfo {
			std::vector<std::string> materialSlots{};
			std::vector<AssetID> defaultMaterials{};
			std::vector<VolumeBounds> volumeBoundsSubmeshes{};
			VolumeBounds mainBounds{};
			SphereBounds mainSphereBounds{};
			AssetID meshAsset{ AssetID::null() };
			AssetID skeletonAsset{ AssetID::null() };
			std::vector<AssetID> animationAssets;
		};

		struct RiggedMorphData {
			uint32_t id;
			std::vector<Vertex> targetVertices; // position is a target direction
			VolumeBounds volumeBounds{};
			SphereBounds sphereBounds{};
		};

		struct RiggedBoneData {
			uint32_t id;
			VolumeBounds volumeBounds{};
			SphereBounds sphereBounds{};
			glm::mat4 offset;
			std::vector<std::string> children;
			std::string parent;
		};

		struct BoneAnimationData {
			std::vector<glm::vec3>	translationKeyFrames;
			std::vector<float>		translationKeyFrameTimeStamps;
			std::vector<glm::vec4>	quaternionKeyFrames;
			std::vector<float>		quaternionKeyFrameTimeStamps;
			std::vector<glm::vec3>	scaleKeyFrames;
			std::vector<float>		scaleKeyFrameTimeStamps;
		};
		struct MorphAnimationData {
			std::vector<float>	factorKeyFrames;
			std::vector<float>	factorKeyFrameTimeStamps;
		};

		struct RiggedAnimationData {
			float duration;
			std::string name;
			std::unordered_map<std::string, BoneAnimationData> boneAnimations;
			std::unordered_map<std::string, MorphAnimationData> morphAnimations;
		};

		class ModelImporter {
		public:
			struct Builder {
				ResourceSystem* resourceSystem;

				struct MeshLoadData {
					aiNode* rootMeshNode{};
					std::vector<aiNode*> uniqueArmatures;
					//		<Material Slot>	 <Material Slot Index>
					std::unordered_map<std::string, uint32_t> slotToIndex;
					//		<Material Slot Index> <Submesh>
					std::map<uint32_t, SubmeshData> submeshes;
					VolumeBounds volumeBounds{};
					SphereBounds sphereBounds{};

					std::vector<RiggedAnimationData> animations;

					//		<Material Slot Index> <MorphTarget>
					std::map<uint32_t, std::unordered_map<std::string, RiggedMorphData>> morphTargets;
					//		<BoneName>
					std::unordered_map<std::string, RiggedBoneData> bones;
					std::map<uint32_t, VertexSkin> skins; // index, skin

					uint32_t boneCount;

					std::string meshName;

					glm::mat4 modelMatrix;
					glm::mat3 normalMatrix;
				};
				std::vector<MeshLoadData> loadedMeshes{};
				

				bool loadScene(const std::string& filepath, const std::string& assetpath, Model3DImportInfo importInfo);
				void processNode(aiNode* node, const aiScene* scene, Model3DImportInfo importInfo, MeshLoadData& toLoadMesh);
				void loadSubmesh(aiMesh* mesh, const aiScene* scene, Model3DImportInfo importInfo, MeshLoadData& toLoadMesh);
				void processBoneHierarchy(aiNode* node, MeshLoadData& toLoadMesh);
			private:
				AssetID workingDir{AssetID::null()};
				std::string format;
				std::string originalDir;
			};
			static MeshRawData* loadMeshData(
				AssetID modelLocation
			);
			static void freeMeshData(
				MeshRawData* meshData
			);

			static SkeletonRawData* loadSkeletalData(
				AssetID skeletonLocation
			);
			static void freeSkeletalData(
				SkeletonRawData* skeletalData
			);

			static SkeletalAnimationRawData* loadAnimationData(
				AssetID animationLocation
			);
			static void freeAnimationData(
				SkeletalAnimationRawData* animationData
			);


			static std::vector<ModelInfo> importModelsFromFile(
				Model3DImportInfo importInfo,
				ResourceSystem* resourceSystem,
				const std::string& modelSource,
				AssetID modelDest
			);
		};
	}
}
