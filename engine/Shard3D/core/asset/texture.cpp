#include "../../s3dpch.h" 
#include "texture.h"

// std
#include <cmath>
#include "../../systems/handlers/project_system.h"
#include "../vulkan_api/bindless.h"

namespace Shard3D::Resources {
    Texture::Texture(S3DDevice& device, uPtr<SmartDescriptorSet>& indexedDescriptor, ktxTexture2* texture, TextureLoadInfo loadInfo)
        : bindless(indexedDescriptor), mDevice(device) {
        mLoadInfo = loadInfo;

        mFormat = getVkFormatTextureFormat(loadInfo.format, loadInfo.colorSpace, loadInfo.channels, loadInfo.compression);
    }

    Texture::~Texture() {
        vkDestroySampler(mDevice.device(), mTextureSampler, nullptr);
        vkDestroyImageView(mDevice.device(), mTextureImageView, nullptr);
        vkDestroyImage(mDevice.device(), mTextureImage, nullptr);
        vkFreeMemory(mDevice.device(), mTextureImageMemory, nullptr);
        bindless->freeIndex(BINDLESS_SAMPLER_BINDING, shaderResouceIndex);
        SHARD3D_LOG("Destroying Texture {0}", (void*)this);
    }

    void Texture::updateDescriptor() {
        mDescriptor.sampler = mTextureSampler;
        mDescriptor.imageView = mTextureImageView;
        mDescriptor.imageLayout = mTextureLayout;
    }
   
    void Texture::createTextureImageView(VkImageViewType viewType, TextureChannels channels) {
        VkImageViewCreateInfo viewInfo{};
        viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        viewInfo.image = mTextureImage;
        viewInfo.viewType = viewType;
        viewInfo.format = mFormat;
        viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        viewInfo.subresourceRange.baseMipLevel = 0;
        viewInfo.subresourceRange.levelCount = mMipLevels;
        viewInfo.subresourceRange.baseArrayLayer = 0;
        viewInfo.subresourceRange.layerCount = mLayerCount;
        switch (channels) {
        case (TextureChannels::R):
            viewInfo.components.r = VK_COMPONENT_SWIZZLE_R;
            viewInfo.components.g = VK_COMPONENT_SWIZZLE_R;
            viewInfo.components.b = VK_COMPONENT_SWIZZLE_R;
            viewInfo.components.a = VK_COMPONENT_SWIZZLE_ONE;
            break;
        case (TextureChannels::RG):
            viewInfo.components.r = VK_COMPONENT_SWIZZLE_R;
            viewInfo.components.g = VK_COMPONENT_SWIZZLE_G;
            viewInfo.components.b = VK_COMPONENT_SWIZZLE_ONE; // if normal maps are compressed with BC5 the Z value is 1 instead of -1
            viewInfo.components.a = VK_COMPONENT_SWIZZLE_ONE;
            break;
        case (TextureChannels::RGB):
            viewInfo.components.r = VK_COMPONENT_SWIZZLE_R;
            viewInfo.components.g = VK_COMPONENT_SWIZZLE_G;
            viewInfo.components.b = VK_COMPONENT_SWIZZLE_B;
            viewInfo.components.a = VK_COMPONENT_SWIZZLE_ONE;
            break;
        case (TextureChannels::RGBA):
            viewInfo.components.r = VK_COMPONENT_SWIZZLE_R;
            viewInfo.components.g = VK_COMPONENT_SWIZZLE_G;
            viewInfo.components.b = VK_COMPONENT_SWIZZLE_B;
            viewInfo.components.a = VK_COMPONENT_SWIZZLE_A;
            break;
        }
        // texture(bla, uv).rgb
        VK_ASSERT(vkCreateImageView(mDevice.device(), &viewInfo, nullptr, &mTextureImageView), "failed to create texture image view!");
    }

    void Texture::createTextureSampler() {
        VkSamplerCreateInfo samplerInfo{};
        samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
        samplerInfo.magFilter = static_cast<VkFilter>(mLoadInfo.filter);
        samplerInfo.minFilter = static_cast<VkFilter>(mLoadInfo.filter);

        samplerInfo.addressModeU = static_cast<VkSamplerAddressMode>(mLoadInfo.addressMode);
        samplerInfo.addressModeV = static_cast<VkSamplerAddressMode>(mLoadInfo.addressMode);
        samplerInfo.addressModeW = static_cast<VkSamplerAddressMode>(mLoadInfo.addressMode);

        samplerInfo.anisotropyEnable = forceNoAnisotropicFiltering ? false :  ProjectSystem::getGraphicsSettings().texture.anisotropy > 1.0f;
        samplerInfo.maxAnisotropy = ProjectSystem::getGraphicsSettings().texture.anisotropy; // 16.0f is highest
        samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
        samplerInfo.unnormalizedCoordinates = VK_FALSE;

        // this fields useful for percentage close filtering for shadow maps
        samplerInfo.compareEnable = VK_FALSE;
        samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;

        samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
        samplerInfo.mipLodBias = 0.0f;
        samplerInfo.minLod = 0.0f;
        samplerInfo.maxLod = static_cast<float>(mMipLevels - 1);

        VK_ASSERT(vkCreateSampler(mDevice.device(), &samplerInfo, nullptr, &mTextureSampler), "failed to create texture sampler!");
    }
}