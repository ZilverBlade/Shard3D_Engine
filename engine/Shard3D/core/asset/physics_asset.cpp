#include "physics_asset.h"
#include <fstream>
#include "../../utils/json_ext.h"
#include "../../static_definitions.h"
#include "../../utils/logger.h"

namespace Shard3D {
	void PhysicsMaterial::serialize(const std::string& path) {
		nlohmann::ordered_json out{};
		std::ofstream fout(path);

		out["version"] = ENGINE_VERSION;
		out["assetType"] = "physics_material";
		out["type"] = "standard";
		out["material"]["config"]["frictionCoef"] = this->frictionCoef;
		out["material"]["config"]["looseFrictionCoef"] = this->looseFrictionCoef;
		out["material"]["config"]["elasticity"] = this->elasticity;
		out["material"]["config"]["softness"] = this->softness;
		out["material"]["config"]["absorption"] = this->absorption;
		out["material"]["config"]["conveyorDirection"] = this->conveyorDirection;

		fout << out.dump(4);
		fout.flush();
		fout.close();
	}

	void PhysicsMaterial::deserialize(const std::string& path) {
		simdjson::dom::parser parser;
		simdjson::padded_string json = simdjson::padded_string::load(path);
		auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			return;
		}

		if (data["material"]["config"]["frictionCoef"].error() == simdjson::SUCCESS) {
			this->frictionCoef = data["material"]["config"]["frictionCoef"].get_double().value();
		}
		if (data["material"]["config"]["looseFrictionCoef"].error() == simdjson::SUCCESS) {
			this->looseFrictionCoef = data["material"]["config"]["looseFrictionCoef"].get_double().value();
		}
		if (data["material"]["config"]["elasticity"].error() == simdjson::SUCCESS) {
			this->elasticity = data["material"]["config"]["elasticity"].get_double().value();
		}
		if (data["material"]["config"]["softness"].error() == simdjson::SUCCESS) {
			this->softness = data["material"]["config"]["softness"].get_double().value();
		}
		if (data["material"]["config"]["absorption"].error() == simdjson::SUCCESS) {
			this->absorption = data["material"]["config"]["absorption"].get_double().value();
		}
		if (data["material"]["config"]["conveyorDirection"].error() == simdjson::SUCCESS) {
			this->conveyorDirection = SIMDJSON_READ_VEC3(data["material"]["config"]["conveyorDirection"]);
		}
	}
	void PhysicsTireMaterial::deserializeTire(const std::string& path) {
		simdjson::dom::parser parser;
		simdjson::padded_string json = simdjson::padded_string::load(path);
		auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			return;
		}
		if (data["material"]["tireConfig"].error() != simdjson::SUCCESS) {
			SHARD3D_ERROR("'{0}' is not a tire material!!", path);
			return;
		}
		this->longSlipStiffness = data["material"]["tireConfig"]["longSlipStiffness"].get_double().value();
		this->longSlipShape = data["material"]["tireConfig"]["longSlipShape"].get_double().value();
		this->longSlipPeak = data["material"]["tireConfig"]["longSlipPeak"].get_double().value();
		this->longSlipCurvature = data["material"]["tireConfig"]["longSlipCurvature"].get_double().value();
		this->latSlipStiffness = data["material"]["tireConfig"]["latSlipStiffness"].get_double().value();
		this->latSlipShape = data["material"]["tireConfig"]["latSlipShape"].get_double().value();
		this->latSlipPeak = data["material"]["tireConfig"]["latSlipPeak"].get_double().value();
		this->latSlipCurvature = data["material"]["tireConfig"]["latSlipCurvature"].get_double().value();
	}
}