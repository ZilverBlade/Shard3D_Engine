#pragma once
#include "texture.h"

namespace Shard3D {
    inline namespace Resources {
        class TextureCube : public Texture {
        public:
            TextureCube(S3DDevice& device, uPtr<SmartDescriptorSet>& indexedDescriptor, ktxTexture2* texture, TextureLoadInfo loadInfo);
            ~TextureCube() {}
        protected:
            virtual void createTextureImage(ktxTexture2* texture) override;
        };
    }
}