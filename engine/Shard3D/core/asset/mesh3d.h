#pragma once
#include "../../vulkan_abstr.h"
#include "material.h"
#include "primitives.h"
#include "../asset/assetid.h"
#include "resource.h"

namespace Shard3D {
	class VertexArenaAllocator;
	inline namespace Resources {
		enum class VertexBindings {
			Position = 1,
			PositionUV = 2,
			PositionUVNormal = 3,
			PositionUVNormalTangent = 4
		};
		class Mesh3D : public Resource {
		public:
			struct Submesh {
				VolumeBounds volumeBounds{};
				uint32_t vertexCount{};
				uint32_t indexCount{};
				uint32_t vertexOffset{};
				uint32_t indexOffset{};
			};

			Mesh3D(S3DDevice& dvc, VertexArenaAllocator* allocator, const MeshRawData* data, const std::vector<AssetID>& default_materials);
			virtual ~Mesh3D();

			Mesh3D(const Mesh3D&) = delete;
			Mesh3D& operator=(const Mesh3D&) = delete;

			void draw(VkCommandBuffer commandBuffer, uint32_t index, uint32_t instances = 1, uint32_t instanceOffset = 0);
			void drawAll(VkCommandBuffer commandBuffer, uint32_t instances = 1, uint32_t instanceOffset = 0);
		
			uint32_t meshID{};
			uint32_t vtxCount{};
			uint32_t idxCount{};
			std::vector<Submesh> submeshes{};
			bool hasIndexBuffer = false;

			//		materials
			std::vector<std::string> materialSlots{};
			std::vector<AssetID> materials{};
			//		bounding boxes
			VolumeBounds volumeBounds{};
			SphereBounds sphereBounds{};
			VertexArenaAllocator* vertexAllocator{};
		private:
			void createBuffers(const MeshRawData* data);
			S3DDevice& device;
		};
	}
}
