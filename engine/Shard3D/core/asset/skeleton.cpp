#pragma once
#include "../../systems/handlers/resource_system.h"
#include "skeleton.h"

namespace Shard3D::Resources {
	Skeleton::Skeleton(S3DDevice& device,
				ResourceSystem* resourceSystem,
				SkeletonRawData* data) : device(device), resourceSystem(resourceSystem) {
		
		vertexCount = data->vertexCount;
		if (data->morphTargetCount > 0) {
			VkDeviceSize morphBufferSize = data->morphTargetCount * sizeof(glm::uvec4) * data->vertexCount;
			std::vector<std::vector<glm::uvec4>> morphs(data->morphTargetCount);
			morphTargetNames.resize(data->morphTargetCount);
			for (int i = 0; i < data->morphTargetCount; i++) {
				morphTargetNames[data->morphTargets[i]->targetIndex] = data->morphTargetNames[i];

				morphs[data->morphTargets[i]->targetIndex].reserve(data->vertexCount);
				for (int j = 0; j < data->vertexCount; j++) {
					morphs[i].push_back(
						glm::uvec4(
							data->morphTargets[data->morphTargets[i]->targetIndex]->targetVectorPositions[j],
							0,
							data->morphTargets[data->morphTargets[i]->targetIndex]->targetVectorNormals[j],
							data->morphTargets[data->morphTargets[i]->targetIndex]->targetVectorTangents[j]
						)
					);
				}
			}

			S3DBuffer morphTargetStagingBuffer = S3DBuffer(
				device,
				sizeof(glm::uvec4) * data->vertexCount,
				data->morphTargetCount,
				VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
				true
			);
			morphTargetVectorBuffer = make_uPtr<S3DBuffer>(
				device,
				sizeof(glm::uvec4) * data->vertexCount,
				data->morphTargetCount,
				VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
				VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
				true
			);
			morphTargetStagingBuffer.map();
			for (int i = 0; i < data->morphTargetCount; i++) {
				morphTargetStagingBuffer.writeToBuffer(morphs[i].data(), i * data->vertexCount, data->vertexCount);
			}
			morphTargetStagingBuffer.flush();

			VkCommandBuffer commandBuffer = device.beginSingleTimeCommands();
			device.copyBuffer(commandBuffer, morphTargetStagingBuffer.getBuffer(), morphTargetVectorBuffer->getBuffer(), morphBufferSize);
			device.endSingleTimeCommands(commandBuffer);

			morphTargetBufferID = resourceSystem->getDescriptor()->allocateIndex(BINDLESS_SSBO_BINDING);
			auto morphTargetDescriptorInfo = morphTargetVectorBuffer->descriptorInfo();
			SmartDescriptorWriter(*resourceSystem->getDescriptor())
				.writeBuffer(BINDLESS_SSBO_BINDING, morphTargetBufferID, &morphTargetDescriptorInfo)
				.build();
		}

		if (data->boneCount > 0) {
			boneNames.resize(data->boneCount);
			offsetMatrices.resize(data->boneCount);
			relationships.resize(data->boneCount);
			for (int i = 0; i < data->boneCount; i++) {
				boneNames[data->bones[i]->boneIndex] = data->boneNames[i];
				offsetMatrices[data->bones[i]->boneIndex] = data->bones[i]->offsetMatrix;
				relationships[data->bones[i]->boneIndex].parent = data->bones[i]->parentBone;
				relationships[data->bones[i]->boneIndex].children.resize(data->bones[i]->childBoneCount);
				memcpy(relationships[data->bones[i]->boneIndex].children.data(), data->bones[i]->childBones, data->bones[i]->childBoneCount * sizeof(uint8_t));
			}

			uint32_t alignedVertexCount = ceil(data->vertexCount / sizeof(glm::uvec4));
			VkDeviceSize vertexWeightIDBufferSize = sizeof(glm::uvec4) * alignedVertexCount;

			S3DBuffer vertexIDStagingBuffer = S3DBuffer(
				device,
				sizeof(glm::uvec4),
				alignedVertexCount,
				VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
				true
			);
			S3DBuffer vertexWeightStagingBuffer = S3DBuffer(
				device,
				sizeof(glm::uvec4),
				alignedVertexCount,
				VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
				true
			);
			vertexIDBuffer = make_uPtr<S3DBuffer>(
				device,
				sizeof(glm::uvec4),
				alignedVertexCount,
				VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
				VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
				true
			);
			vertexWeightBuffer = make_uPtr<S3DBuffer>(
				device,
				sizeof(glm::uvec4),
				alignedVertexCount,
				VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
				VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
				true
			);

			vertexIDStagingBuffer.map();
			vertexIDStagingBuffer.writeToBuffer(data->skinData->boneIDs, 0, alignedVertexCount * sizeof(glm::uvec4));
			vertexIDStagingBuffer.flush();

			vertexWeightStagingBuffer.map(); 
			vertexWeightStagingBuffer.writeToBuffer(data->skinData->boneWeights, 0, alignedVertexCount * sizeof(glm::uvec4));
			vertexWeightStagingBuffer.flush();

			VkCommandBuffer commandBuffer = device.beginSingleTimeCommands();
			device.copyBuffer(commandBuffer, vertexIDStagingBuffer.getBuffer(), vertexIDBuffer->getBuffer(), vertexWeightIDBufferSize);
			device.copyBuffer(commandBuffer, vertexWeightStagingBuffer.getBuffer(), vertexWeightBuffer->getBuffer(), vertexWeightIDBufferSize);
			device.endSingleTimeCommands(commandBuffer);

			vertexIDBufferID = resourceSystem->getDescriptor()->allocateIndex(BINDLESS_SSBO_BINDING);
			vertexWeightBufferID = resourceSystem->getDescriptor()->allocateIndex(BINDLESS_SSBO_BINDING);

			auto vertexIDDescriptorInfo = vertexIDBuffer->descriptorInfo();
			auto vertexWeightDescriptorInfo = vertexWeightBuffer->descriptorInfo();

			SmartDescriptorWriter(*resourceSystem->getDescriptor())
				.writeBuffer(BINDLESS_SSBO_BINDING, vertexIDBufferID, &vertexIDDescriptorInfo)
				.writeBuffer(BINDLESS_SSBO_BINDING, vertexWeightBufferID, &vertexWeightDescriptorInfo)
				.build();
		}

	}
	Skeleton::~Skeleton() {
		if (morphTargetBufferID) {
			resourceSystem->getDescriptor()->freeIndex(BINDLESS_SSBO_BINDING, morphTargetBufferID);
		}
		if (vertexIDBufferID) {
			resourceSystem->getDescriptor()->freeIndex(BINDLESS_SSBO_BINDING, vertexIDBufferID);
			resourceSystem->getDescriptor()->freeIndex(BINDLESS_SSBO_BINDING, vertexWeightBufferID);
		}
	}
}
