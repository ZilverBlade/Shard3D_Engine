#pragma once
#include "../vulkan_api/device.h"
#include "../vulkan_api/bindless.h"
#include "../vulkan_api/abstypes.h"
#include "resource.h"

extern "C" {
    typedef struct ktxTexture2;
}
namespace Shard3D {
    struct TextureLoadInfo {
        TextureFilter filter = TextureFilter::Linear;
        TextureSamplerMode addressMode = TextureSamplerMode::Repeat;
        TextureFormat format = TextureFormat::Unorm8;
        TextureColorSpace colorSpace = TextureColorSpace::sRGB;
        TextureChannels channels = TextureChannels::RGBA;
        TextureCompression compression = TextureCompression::BC7;
        bool enableMipMaps = true; // does nothing for cubemaps
        bool isNormalMap = false;
        int trueResolutionWidth = -1;
        int trueResolutionHeight = -1;
    };
    inline namespace Resources {
        class Texture : public Resource {
        public:
            Texture(S3DDevice& device, uPtr<SmartDescriptorSet>& indexedDescriptor, ktxTexture2* texture, TextureLoadInfo loadInfo);
            virtual ~Texture();

            inline VkSampler getSampler() const {
                return mTextureSampler;
            }
            inline VkImage getImage() const {
                return mTextureImage;
            }
            inline VkImageView getImageView() const {
                return mTextureImageView;
            }
            inline VkDescriptorImageInfo getImageInfo() const {
                return mDescriptor;
            }
            inline VkImageLayout getImageLayout() const {
                return mTextureLayout;
            }
            inline VkExtent3D getExtent() const {
                return mExtent;
            }
            inline VkFormat getFormat() const {
                return mFormat;
            }

            void updateDescriptor();

            uint32_t getResourceIndex() {
                return shaderResouceIndex;
            }

            TextureLoadInfo getLoadInfo() {
                return mLoadInfo;
            }

            size_t getResourceSize() {
                return memorySize;
            }
        protected:
            virtual void createTextureImage(ktxTexture2* texture) = 0;

            void createTextureImageView(VkImageViewType viewType, TextureChannels channels);
            void createTextureSampler();

            VkDescriptorImageInfo mDescriptor{};
            uint32_t shaderResouceIndex = 0xffffffffu;

            S3DDevice& mDevice;
            VkImage mTextureImage = nullptr;
            VkDeviceMemory mTextureImageMemory = nullptr;
            VkImageView mTextureImageView = nullptr;
            VkSampler mTextureSampler = nullptr;
            VkFormat mFormat;
            TextureLoadInfo mLoadInfo;
            VkImageLayout mTextureLayout;
            uint32_t mMipLevels{ 1 };
            uint32_t mLayerCount{ 1 };
            VkExtent3D mExtent{};
            uPtr<SmartDescriptorSet>& bindless;
            size_t memorySize = ~0;
            bool forceNoAnisotropicFiltering = false;
        };
    }
}