#pragma once

#include "../../core.h"
#include "../../s3dstd.h"

namespace Shard3D {
	class ImageResource {
	public:
		static uint8_t* getSTBImage(const std::string& filepath, int* x, int* y, int* comp, int req_comp);
		static uint8_t* getSTBImage(const void* memory, int pixelCount, int* x, int* y, int* comp, int req_comp);
		static void freeSTBImage(void* pixels);
	/*
		Stores image from memory to a bitmap (BMP) image format
		@param pixel (void*): Image data
		@param path (string): bitmap output
	*/
		static void storeImage(const void* pixels, int width, int height, int channelCount, const std::string& path);
	};
}