#pragma once

#include "../../core.h"
#include "../../vulkan_abstr.h"
#include "../asset/assetid.h"
#include "../vulkan_api/compute_pipeline.h"
#include "../../systems/handlers/material_system.h"
#include "resource.h"
#include "../../utils/json_ext.h"
/*
* Shard 3D Material system
*
*/

namespace Shard3D {
	struct alignas(16)MaterialTextureData_ {
		ShaderResourceIndex emissiveTex_id;

		ShaderResourceIndex diffuseTex_id;

		ShaderResourceIndex specularTex_id;
		ShaderResourceIndex glossinessTex_id;
		ShaderResourceIndex reflectivityTex_id;

		ShaderResourceIndex normalTex_id;

		ShaderResourceIndex maskTex_id;
		ShaderResourceIndex opacityTex_id;
	};

	struct alignas(16)MaterialFactorData_ {
		glm::vec2 texCoordOffset;
		glm::vec2 texCoordMultiplier;
		glm::vec4 emission;
		glm::vec4 diffuse;
		float normalStrength;
		float specular;
		float glossiness;
		float reflectivity;
		// optional by material setting
		float opacity;
		float forwardBlendingMulBias;
		float sfExt_ForwardRefractive_IOR;
		float sfExt_ForwardRefractive_Dispersion;
	};

	struct MaterialBufferData {
		uint32_t textureFlags;
		uint32_t surfaceShadingModel;
		uint32_t dbufferModel;
		uint32_t align4;
		alignas(16)MaterialTextureData_ textureData;
		alignas(16)MaterialFactorData_ factorData;
	};
	struct TerrainMaterialBufferData {
		ShaderResourceIndex diffuseTex_id;
		ShaderResourceIndex specularTex_id;
		ShaderResourceIndex glossinessTex_id;
		ShaderResourceIndex reflectivityTex_id;
		ShaderResourceIndex heightTex_id;
		float bumpStrength;
		float parallaxDepth;
		float specular;
		glm::vec2 texCoordMultiplier;
		float glossiness;
		float reflectivity;
		glm::vec4 diffuse;
	};


	enum class MaterialClass {
		None = 0,
		Surface = 1,
		DeferredDecal = 2,
		Terrain = 3
	};

	class Material : public Resource {
	public:
		Material(ResourceSystem* resourceSystem, MaterialClass materialClass);
		virtual ~Material();

		DELETE_COPY(Material);

		const uint32_t getShaderMaterialID();
		void createMaterial(S3DDevice& device);
		virtual void updateMaterial() = 0;
		virtual void serialize(const std::string& path) = 0;
		virtual void deserialize(const std::string& path) = 0;
		virtual std::vector<AssetID*> getAllTexturesReference();
		virtual std::vector<AssetID> getAllTextures();
		virtual void loadAllTextures();

		static MaterialClass discoverMaterialClass(AssetID asset);

		inline bool isBuilt() {
			return built;
		}

		uPtr<S3DBuffer>& getBuffer() {
			return buffer;
		}

		AssetID maskTex = AssetID::null();
		AssetID opacityTex = AssetID::null();
		float opacity{ 1.f };

		AssetID normalTex = AssetID::null();

		glm::vec4 emissiveColor{ 0.f, 0.f, 0.f, 1.f };
		AssetID emissiveTex = AssetID::null();

		glm::vec3 diffuseColor{ 1.f };
		AssetID diffuseTex = AssetID::null();

		float specular = 0.5f;
		AssetID specularTex = AssetID::null();

		float glossiness = 0.5f;
		AssetID glossinessTex = AssetID::null();

		float reflectivity = 0.f;
		AssetID reflectivityTex = AssetID::null();

		float forwardBlendingMulBias = 1.0f;

		float sfExt_ForwardRefractive_IOR = 1.4f;
		float sfExt_ForwardRefractive_Dispersion = 0.0f;
		float sfExt_DeferredFoliage_Thickness = 1.f;

		glm::vec2 texCoordOffset{ 0.0f };
		glm::vec2 texCoordMultiplier{ 1.0f };

		glm::uvec2 turingShadingRate = { 1, 1 };

		MaterialClass getClass() {
			return materialClass;
		}
	protected:
		MaterialBufferData getBaseMaterialBufferData();

		bool built = false;
		uPtr<S3DBuffer> buffer;
		ResourceSystem* resourceSystem;
		MaterialTextureData_ textureDescriptorData{};
		uint32_t material_id{};
		void free();
		MaterialClass materialClass;
	};
	enum class SurfaceShadingModel {
		Unshaded = 0,
		Shaded = 1,
		Emissive = 2,
		DeferredFoliage = 3,
		DeferredDualLayerClearCoat = 4,
		ForwardPureRefractiveGlass = 5,
		ForwardPureScreenspaceRefractiveGlass = 6
	};

	/*
	*	Material that can be used on meshes.
	*	Can have specular, shininness and reflectivity properties, texture maps, masked, and be transparent.
	*/
	class SurfaceMaterial : public Material {
	public:
		SurfaceMaterial(ResourceSystem* resourceSystem)
			: Material(resourceSystem, MaterialClass::Surface) {
			shadingModel = SurfaceShadingModel::Shaded;
		}

		void overrideClassAdd(SurfaceMaterialPipelineClassPermutationFlags flags) {
			pClassFlags |= flags;
		}
		void overrideClassRmv(SurfaceMaterialPipelineClassPermutationFlags flags) {
			pClassFlags &= ~flags;
		}

		SurfaceShadingModel shadingModel = SurfaceShadingModel::Shaded;

		MaterialBufferData getSurfaceMaterialBufferData();
		void updateMaterial() override;

		void setTranslucent(bool translucent);
		bool isTranslucent();
		void setMasked(bool masked);
		bool isMasked();
		void setDoubleSided(bool doubleSided);
		bool isDoubleSided();

		SurfaceMaterialPipelineClassPermutationFlags getPipelineClass() {
			return pClassFlags;
		}

		SurfaceMaterialPipelineClassPermutationFlags pClassFlags = SurfaceMaterialPipelineClassPermutationOptions_Opaque;

		void serialize(const std::string& path) override;
		void deserialize(const std::string& path) override;
	};

	enum class DeferredDecalModel {
		DBufferDiffuse = 0x01,
		DBufferSpecularGlossinessReflectivity = 0x02,
		DBufferNormal = 0x04,
		DBufferDiffuseSpecularGlossinessReflectivity = 0x08,
		DBufferDiffuseSpecularGlossinessReflectivityNormal = 0x10,
		DBufferDiffuseNormal = 0x20,
		DBufferSpecularGlossinessReflectivityNormal = 0x40,
		GBufferMultiplyDiffuse = 0x80,
		GBufferMultiplySpecularGlossinessReflectivity = 0x100,
		GBufferMultiplyDiffuseSpecularGlossinessReflectivity = 0x200,
		GBufferAddEmission = 0x400
	};

	// normals are always blended as normal
	enum class DBufferBlendMode {
		None = 0x00,
		Normal = 0x1000,
		Multiplicative = 0x2000,
		Additive = 0x4000,
		Erase = 0x8000
	};

	typedef uint32_t DBufferFlags;

	/*
	*	Material that can be used for projecting textures onto meshes.
	*	It will overlay whatever previous material with it's properties and render over it.
	*/
	class DecalMaterial : public Material {
	public:
		DecalMaterial(ResourceSystem* resourceSystem) : Material(resourceSystem, MaterialClass::DeferredDecal) {
			model = DeferredDecalModel::DBufferDiffuse;
		}
		bool directGBuffer = false;
		DeferredDecalModel model = DeferredDecalModel::DBufferDiffuse;
		DBufferBlendMode dbufferBlendMode = DBufferBlendMode::Normal;

		MaterialBufferData getDecalMaterialBufferData();
		void updateMaterial() override;

		void serialize(const std::string& path) override;
		void deserialize(const std::string& path) override;
	};

	/*
	*	Material used for blending in terrain
	*/
	class TerrainMaterial : public Material {
	public:
		TerrainMaterial(ResourceSystem* resourceSystem) : Material(resourceSystem, MaterialClass::Terrain) {
	
		}

		virtual std::vector<AssetID*> getAllTexturesReference() override;
		virtual std::vector<AssetID> getAllTextures() override;
		virtual void loadAllTextures() override;

		AssetID heightTex = AssetID::null();
		float parallaxDepth = 0.1f;
		float bumpIntensity = 2.f;

		TerrainMaterialBufferData getTerrainMaterialBufferData();
		void updateMaterial() override;

		void serialize(const std::string& path) override;
		void deserialize(const std::string& path) override;
	};
}