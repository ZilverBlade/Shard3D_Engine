#include "animation.h"
#include "importer_model.h"
namespace Shard3D {
	namespace Resources {
		SkeletalAnimation::SkeletalAnimation(SkeletalAnimationRawData* data) {
			maxDuration = data->maxDuration;
			boneAnimations.resize(data->boneAnimationCount);
			for (int i = 0; i < data->boneAnimationCount; i++) {
				BoneAnimationRawData* boneAnimationData = data->boneAnimations[i];
				boneAnimations[i].translationKeyFrames.resize(boneAnimationData->translationKeyFrameCount);
				boneAnimations[i].translationKeyFrameTimeStamps.resize(boneAnimationData->translationKeyFrameCount);

				boneAnimations[i].quaternionKeyFrames.resize(boneAnimationData->quaternionKeyFrameCount);
				boneAnimations[i].quaternionKeyFrameTimeStamps.resize(boneAnimationData->quaternionKeyFrameCount);

				boneAnimations[i].scaleKeyFrames.resize(boneAnimationData->scaleKeyFrameCount);
				boneAnimations[i].scaleKeyFrameTimeStamps.resize(boneAnimationData->scaleKeyFrameCount);

				memcpy(boneAnimations[i].translationKeyFrames.data(), boneAnimationData->translationKeyFrames,
					sizeof(glm::vec3) * boneAnimationData->translationKeyFrameCount);
				memcpy(boneAnimations[i].translationKeyFrameTimeStamps.data(), boneAnimationData->translationKeyFrameTimeStamps,
					sizeof(float) * boneAnimationData->translationKeyFrameCount);

				memcpy(boneAnimations[i].quaternionKeyFrames.data(), boneAnimationData->quaternionKeyFrames,
					sizeof(glm::vec4) * boneAnimationData->quaternionKeyFrameCount);
				memcpy(boneAnimations[i].quaternionKeyFrameTimeStamps.data(), boneAnimationData->quaternionKeyFrameTimeStamps,
					sizeof(float) * boneAnimationData->quaternionKeyFrameCount);

				memcpy(boneAnimations[i].scaleKeyFrames.data(), boneAnimationData->scaleKeyFrames,
					sizeof(glm::vec3) * boneAnimationData->scaleKeyFrameCount);
				memcpy(boneAnimations[i].scaleKeyFrameTimeStamps.data(), boneAnimationData->scaleKeyFrameTimeStamps,
					sizeof(float) * boneAnimationData->scaleKeyFrameCount);
			}
		}
		SkeletalAnimation::~SkeletalAnimation() {}

		static const uint32_t getLowerKeyFrameIndex(const std::vector<float>& timeStamps, float time) {
			for (int i = 0; i < timeStamps.size(); i++) {
				if (timeStamps[i] > time) {
					return std::max(i - 1, 0);
				}
			}
			return 0;
		}

		glm::vec3 SkeletalAnimation::BoneAnimation::getTranslation(float timeStamp) const {
			uint32_t lkfi = getLowerKeyFrameIndex(translationKeyFrameTimeStamps, timeStamp);
			const glm::vec3& translationP = translationKeyFrames[lkfi];
			if (lkfi == translationKeyFrames.size() - 1) {
				return translationP;
			}
			const glm::vec3& translationN = translationKeyFrames[lkfi + 1];
			float translationTsP = translationKeyFrameTimeStamps[lkfi];
			float translationTsN = translationKeyFrameTimeStamps[lkfi + 1];

			float factor = (timeStamp - translationTsP) / (translationTsN - translationTsP);

			return glm::mix(translationP, translationN, factor);
		}
		Quaternion SkeletalAnimation::BoneAnimation::getQuaternion(float timeStamp) const {
			uint32_t lkfi = getLowerKeyFrameIndex(quaternionKeyFrameTimeStamps, timeStamp);
			const Quaternion& quaternionP = quaternionKeyFrames[lkfi];
			if (lkfi == quaternionKeyFrames.size() - 1) {
				return quaternionP;
			}
			const Quaternion& quaternionN = quaternionKeyFrames[lkfi + 1];
			float quaternionTsP = quaternionKeyFrameTimeStamps[lkfi];
			float quaternionTsN = quaternionKeyFrameTimeStamps[lkfi + 1];

			float factor = (timeStamp - quaternionTsP) / (quaternionTsN - quaternionTsP);

			return (Quaternion&)glm::slerp((glm::quat&)quaternionP, (glm::quat&)quaternionN, factor);
		}
		glm::vec3 SkeletalAnimation::BoneAnimation::getScale(float timeStamp) const {
			uint32_t lkfi = getLowerKeyFrameIndex(scaleKeyFrameTimeStamps, timeStamp);
			const glm::vec3& scaleP = scaleKeyFrames[lkfi];
			if (lkfi == scaleKeyFrames.size() - 1) {
				return scaleP;
			}
			const glm::vec3& scaleN = scaleKeyFrames[lkfi + 1];
			float scaleTsP = scaleKeyFrameTimeStamps[lkfi];
			float scaleTsN = scaleKeyFrameTimeStamps[lkfi + 1];

			float factor = (timeStamp - scaleTsP) / (scaleTsN - scaleTsP);

			return glm::mix(scaleP, scaleN, factor);
		}
	
	
		float SkeletalAnimation::MorphTargetAnimation::getFactor(float timeStamp) {
			return 0.0f;
		}
	}

	SkeletalAnimationManager::SkeletalAnimationManager() {

	}
	SkeletalAnimationManager::~SkeletalAnimationManager() {

	}
	void SkeletalAnimationManager::clearAllAnimations() {
		animations.clear();
	}
	void SkeletalAnimationManager::loadAnimation(AssetID animation) {
		animations[animation] = make_uPtr<SkeletalAnimation>(ModelImporter::loadAnimationData(animation));
	}
	const SkeletalAnimation& SkeletalAnimationManager::retrieveAnimation(AssetID animation) {
		auto& iter = animations.find(animation);
		if (iter == animations.end()) {
			loadAnimation(animation);
			SHARD3D_ASSERT(animations.find(animation) != animations.end());
			return *animations.at(animation);
		}
		return *iter->second;
	}
}