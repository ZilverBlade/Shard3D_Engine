#include "primitives.h"

//namespace std {
//	template <>
//	struct hash<Shard3D::Vertex> {
//		size_t operator()(Shard3D::Vertex const& vertex) const {
//			size_t seed = 0;
//			Shard3D::hashCombine(seed, vertex.position, vertex.normal, vertex.uv);
//			return seed;
//		}
//	};
//}
namespace Shard3D {
	std::vector<VkVertexInputBindingDescription> Vertex::getPositionBindingDescriptions() {
		uint32_t size = sizeof(VTX_pos_offset_12b_);
		return { {0, size, VK_VERTEX_INPUT_RATE_VERTEX} };
	}
	std::vector<VkVertexInputAttributeDescription> Vertex::getPositionAttributeDescriptions() {
		std::vector<VkVertexInputAttributeDescription> attributeDescriptions{};
		attributeDescriptions.reserve(1);
		attributeDescriptions.push_back(VkVertexInputAttributeDescription{ 0, 0, VK_FORMAT_R32G32B32_SFLOAT, 0 });
		return attributeDescriptions;
	}

	std::vector<VkVertexInputBindingDescription> Vertex::getUVBindingDescriptions() {
		uint32_t size = sizeof(VTX_uv_cmpr_offset_4b_);
		return { {1, size, VK_VERTEX_INPUT_RATE_VERTEX} };
	}

	std::vector<VkVertexInputAttributeDescription> Vertex::getUVAttributeDescriptions() {
		std::vector<VkVertexInputAttributeDescription> attributeDescriptions{};
		attributeDescriptions.reserve(1);
		attributeDescriptions.push_back(VkVertexInputAttributeDescription{ 1, 1, VK_FORMAT_R16G16_SFLOAT, 0 });
		return attributeDescriptions;
	}

	std::vector<VkVertexInputBindingDescription> Vertex::getNormalBindingDescriptions() {
		uint32_t size = sizeof(VTX_norm_cmpr_offset_4b_);
		return { {2, size, VK_VERTEX_INPUT_RATE_VERTEX} };
	}

	std::vector<VkVertexInputAttributeDescription> Vertex::getNormalAttributeDescriptions() {
		std::vector<VkVertexInputAttributeDescription> attributeDescriptions{};
		attributeDescriptions.reserve(1);
		attributeDescriptions.push_back(VkVertexInputAttributeDescription{ 2, 2, VK_FORMAT_R32_UINT, 0 });
		return attributeDescriptions;
	}

	std::vector<VkVertexInputBindingDescription> Vertex::getTangentBindingDescriptions() {
		uint32_t size = sizeof(VTX_tg_cmpr_offset_4b_);
		return { {3, size, VK_VERTEX_INPUT_RATE_VERTEX} };
	}

	std::vector<VkVertexInputAttributeDescription> Vertex::getTangentAttributeDescriptions() {
		std::vector<VkVertexInputAttributeDescription> attributeDescriptions{};
		if (ProjectSystem::getEngineSettings().memory.enableVertexTangents) {
			attributeDescriptions.reserve(1);
			attributeDescriptions.push_back(VkVertexInputAttributeDescription{ 3, 3, VK_FORMAT_R32_UINT, 0 });
		}
		return attributeDescriptions;
	}

}