#include "../../s3dpch.h" 
#include "texture_cube.h"
#include <ktx.h>
#include <ktxvulkan.h>

namespace Shard3D::Resources {
    TextureCube::TextureCube(S3DDevice& device, uPtr<SmartDescriptorSet>& indexedDescriptor, ktxTexture2* texture, TextureLoadInfo loadInfo)
        : Texture(device, indexedDescriptor, texture, loadInfo) {
        mLayerCount = 6;
        forceNoAnisotropicFiltering = true;
        createTextureImage(texture);
        createTextureImageView(VK_IMAGE_VIEW_TYPE_CUBE, loadInfo.channels);
        createTextureSampler();
        updateDescriptor();
        shaderResouceIndex = bindless->allocateIndex(BINDLESS_SAMPLER_BINDING);
        SmartDescriptorWriter(*bindless)
            .writeImage(BINDLESS_SAMPLER_BINDING, shaderResouceIndex, &mDescriptor)
            .build();
    }

    void TextureCube::createTextureImage(ktxTexture2* texture) {
        void* pixels = texture->pData;
        VkDeviceSize imageSize = texture->dataSize;
        mMipLevels = texture->numLevels;
        texture->generateMipmaps = KTX_FALSE;

        mTextureLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        S3DQueue* queue = mDevice.getAvailableQueue(QueueType::Graphics);
        ktxVulkanDeviceInfo* dvcInfo = ktxVulkanDeviceInfo_Create(mDevice.physicalDevice(), mDevice.device(), queue->queue, mDevice.getCommandPool(), nullptr);
        ktxVulkanTexture ktxVkTex{};
        ktxVkTex.depth = 1;
        ktxVkTex.width = texture->baseWidth;
        ktxVkTex.height = texture->baseHeight;
        ktxVkTex.imageFormat = mFormat;
        ktxVkTex.imageLayout = mTextureLayout;
        ktxVkTex.viewType = VK_IMAGE_VIEW_TYPE_2D;
        ktxVkTex.levelCount = mMipLevels;
        ktxVkTex.layerCount = 6;

        auto erc = ktxTexture2_VkUpload(texture, dvcInfo, &ktxVkTex);
        SHARD3D_ASSERT(erc == KTX_SUCCESS);
        mTextureImage = ktxVkTex.image;
        mTextureImageMemory = ktxVkTex.deviceMemory;

        ktxVulkanDeviceInfo_Destroy(dvcInfo);
        mDevice.freeAvailableQueue(queue);

        memorySize = texture->dataSize;
    }
}