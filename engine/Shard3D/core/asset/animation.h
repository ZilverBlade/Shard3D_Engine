#pragma once
#include "../../vulkan_abstr.h"
#include "primitives.h"
#include "../math/transform.h"

namespace Shard3D {
	inline namespace Resources {
		class SkeletalAnimation : public Resource {
		public:
			SkeletalAnimation(SkeletalAnimationRawData* data);
			~SkeletalAnimation();

			class BoneAnimation {
			public:
				glm::vec3 getTranslation(float timeStamp) const;
				Quaternion getQuaternion(float timeStamp) const;
				glm::vec3 getScale(float timeStamp) const;
			private:

				std::vector<glm::vec3>	translationKeyFrames;
				std::vector<float>		translationKeyFrameTimeStamps;

				std::vector<Quaternion>	quaternionKeyFrames;
				std::vector<float>		quaternionKeyFrameTimeStamps;

				std::vector<glm::vec3>	scaleKeyFrames;
				std::vector<float>		scaleKeyFrameTimeStamps;

				friend class SkeletalAnimation;
			};
			class MorphTargetAnimation {
			public:
				float getFactor(float timeStamp);
			private:
				std::vector<float> factorKeyFrames;
				std::vector<float> factorKeyFrameTimeStamps;
			};

			const float getMaxDuration() const {
				return maxDuration;
			}

			std::vector<BoneAnimation> boneAnimations;
			std::vector<MorphTargetAnimation> morphTargetAnimations;
		private:

			float maxDuration = 0.f;
		};
	}

	class SkeletalAnimationManager {
	public:
		SkeletalAnimationManager();
		~SkeletalAnimationManager();

		void clearAllAnimations();

		void loadAnimation(AssetID animation);
		const SkeletalAnimation& retrieveAnimation(AssetID animation);
	private:
		std::unordered_map<AssetID, uPtr<SkeletalAnimation>> animations;
	};
}
