#include "../../s3dpch.h"
#include "material.h"
#include "assetmgr.h"
#include "../../systems/handlers/material_system.h"
#include "../../systems/post_fx/post_processing_system.h"
#include "../misc/graphics_settings.h"
#include <fstream>
#include "../../systems/computational/shader_system.h"
#include "../vulkan_api/bindless.h"
#include "../../utils/json_ext.h"

namespace Shard3D {

	Material::Material(
		ResourceSystem* rs,
		MaterialClass mc
	) : resourceSystem(rs), materialClass(mc) {}

	Material::~Material() {
		if (!built) {
			SHARD3D_LOG("Material {0} has never been built, yet is being destroyed!", (void*)this);
			return;
		}
		SHARD3D_LOG("Destroying Material {0}", (void*)this);
		free();
	}

	void Material::free() {
		resourceSystem->getDescriptor()->freeIndex(BINDLESS_SSBO_BINDING, material_id);
	}

	const uint32_t Material::getShaderMaterialID() {
		return material_id;
	}

	const uint32_t MAT_ALPHA_MASK_BIT = 0x4u;
	const uint32_t MAT_DIFFUSE_TEXTURE_BIT = 0x8u;
	const uint32_t MAT_SPECULAR_TEXTURE_BIT = 0x10u;
	const uint32_t MAT_GLOSSINESS_TEXTURE_BIT = 0x20u;
	const uint32_t MAT_REFLECTIVITY_TEXTURE_BIT = 0x40u;
	const uint32_t MAT_EMISSIVE_TEXTURE_BIT = 0x80u;
	const uint32_t MAT_NORMAL_MAP_BIT = 0x100u;
	const uint32_t MAT_OPACITY_MAP_BIT = 0x200u;
	const uint32_t MAT_RANDOM_TILING_BIT = 0x400u;

	MaterialClass Material::discoverMaterialClass(AssetID asset) {
		simdjson::dom::parser parser;
		if (std::filesystem::exists(asset.getAbsolute())) {
			simdjson::padded_string json = simdjson::padded_string::load(asset.getAbsolute());
			const auto& data = parser.parse(json);
			if (int ec = data.error(); ec == simdjson::error_code::SUCCESS) {
				std::string itemType = std::string(data["assetType"].get_string().value());
				if (itemType == std::string("surface_material") /*pre 0.13.0-b230923 versions*/)
					return MaterialClass::Surface;
				else if (itemType == std::string("material"))
					if (data["material"]["class"].get_string().value() == "surface")
						return MaterialClass::Surface;
					else if (data["material"]["class"].get_string().value() == "deferredDecal")
						return MaterialClass::DeferredDecal;
					else if (data["material"]["class"].get_string().value() == "terrain")
						return MaterialClass::Terrain;
			}
		}
		return MaterialClass::None; // error
	}

	MaterialBufferData Material::getBaseMaterialBufferData() {
		MaterialBufferData matData{};
		matData.factorData.emission = this->emissiveColor;
		matData.factorData.diffuse = { this->diffuseColor.x, this->diffuseColor.y, this->diffuseColor.z, 0.f };
		matData.factorData.specular = this->specular;
		matData.factorData.glossiness = this->glossiness;
		matData.factorData.reflectivity = this->reflectivity;

		matData.factorData.diffuse = { this->diffuseColor.x, this->diffuseColor.y, this->diffuseColor.z, 0.f };
		matData.factorData.specular = this->specular;
		matData.factorData.glossiness = this->glossiness;
		matData.factorData.reflectivity = this->reflectivity;
		matData.factorData.opacity = this->opacity;
		matData.factorData.forwardBlendingMulBias = this->forwardBlendingMulBias;
		matData.factorData.sfExt_ForwardRefractive_IOR = this->sfExt_ForwardRefractive_IOR;
		matData.factorData.sfExt_ForwardRefractive_Dispersion = this->sfExt_ForwardRefractive_Dispersion;

		matData.factorData.texCoordOffset = this->texCoordOffset;
		matData.factorData.texCoordMultiplier = this->texCoordMultiplier;
		matData.textureFlags = 0; // reset

		if (emissiveTex) {
			matData.textureFlags |= MAT_EMISSIVE_TEXTURE_BIT;
			matData.textureData.emissiveTex_id = resourceSystem->retrieveTexture(this->emissiveTex)->getResourceIndex();
		}
		if (diffuseTex) {
			matData.textureFlags |= MAT_DIFFUSE_TEXTURE_BIT;
			matData.textureData.diffuseTex_id = resourceSystem->retrieveTexture(this->diffuseTex)->getResourceIndex();
		}
		if (specularTex) {
			matData.textureFlags |= MAT_SPECULAR_TEXTURE_BIT;
			matData.textureData.specularTex_id = resourceSystem->retrieveTexture(this->specularTex)->getResourceIndex();
		}
		if (glossinessTex) {
			matData.textureFlags |= MAT_GLOSSINESS_TEXTURE_BIT;
			matData.textureData.glossinessTex_id = resourceSystem->retrieveTexture(this->glossinessTex)->getResourceIndex();
		}
		if (reflectivityTex) {
			matData.textureFlags |= MAT_REFLECTIVITY_TEXTURE_BIT;
			matData.textureData.reflectivityTex_id = resourceSystem->retrieveTexture(this->reflectivityTex)->getResourceIndex();
		}
		if (normalTex) {
			matData.textureFlags |= MAT_NORMAL_MAP_BIT;
			matData.textureData.normalTex_id = resourceSystem->retrieveTexture(this->normalTex)->getResourceIndex();
		}
		if (maskTex) {
			matData.textureFlags |= MAT_ALPHA_MASK_BIT;
			matData.textureData.maskTex_id = resourceSystem->retrieveTexture(this->maskTex)->getResourceIndex();
		}
		if (opacityTex) {
			matData.textureFlags |= MAT_OPACITY_MAP_BIT;
			matData.textureData.opacityTex_id = resourceSystem->retrieveTexture(this->opacityTex)->getResourceIndex();
		}
		return matData;
	}
	MaterialBufferData SurfaceMaterial::getSurfaceMaterialBufferData() {
		MaterialBufferData sfData = getBaseMaterialBufferData();
		sfData.surfaceShadingModel = static_cast<uint32_t>(shadingModel);
		return sfData;
	}

	void Material::createMaterial(S3DDevice& device) {
		if (materialClass == MaterialClass::Terrain) return;
		if (built) this->free();

		SHARD3D_VERBOSE("Material {0} flags:", (void*)this);

		textureDescriptorData = MaterialTextureData_{};

		buffer =
			make_uPtr<S3DBuffer>(
				device,
				sizeof(MaterialBufferData),
				1,
				VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
			);

		buffer->map();

		material_id = resourceSystem->getDescriptor()->allocateIndex(BINDLESS_SSBO_BINDING);


		VkDescriptorBufferInfo idInfo = buffer->descriptorInfo();
		SHARD3D_VERBOSE("Material {0} allocated shader ID {1}", (void*)this, material_id);
		SmartDescriptorWriter(*resourceSystem->getDescriptor())
			.writeBuffer(BINDLESS_SSBO_BINDING, material_id, &idInfo)
			.build();
		updateMaterial();

		built = true;
	}

	void SurfaceMaterial::updateMaterial() {
		MaterialBufferData sfData = getSurfaceMaterialBufferData();
		buffer->writeToBuffer(&sfData);
		buffer->flush();
	}

	void SurfaceMaterial::serialize(const std::string& path) {
		nlohmann::ordered_json out{};
		std::ofstream fout(path);

		out["version"] = ENGINE_VERSION;
		out["assetType"] = "material";
		const char* smodel;
		switch (shadingModel) {
		case(SurfaceShadingModel::Unshaded):
			smodel = "unshaded";
			break;
		case(SurfaceShadingModel::Shaded):
			smodel = "shaded";
			break;
		case(SurfaceShadingModel::Emissive):
			smodel = "emissive";
			break;
		case(SurfaceShadingModel::DeferredFoliage):
			smodel = "deferredFoliage";
			break;
		case(SurfaceShadingModel::DeferredDualLayerClearCoat):
			smodel = "deferredDualLayerClearCoat";
			break;
		case(SurfaceShadingModel::ForwardPureRefractiveGlass):
		case(SurfaceShadingModel::ForwardPureScreenspaceRefractiveGlass):
			smodel = "forwardRefractiveGlass";
			break;
			break;
		default:
			smodel = "null";
		}
		out["material"]["class"] = "surface";
		out["material"]["shadingModel"] = smodel;
		out["material"]["blendMode"] = isTranslucent() ? "translucent" : "opaque";
		out["material"]["doubleSided"] = isDoubleSided();

		out["material"]["config"]["diffuseColor"] = this->diffuseColor;
		out["material"]["config"]["emissiveColor"] = this->emissiveColor;

		out["material"]["config"]["specularFactor"] = this->specular;
		out["material"]["config"]["glossinessFactor"] = this->glossiness;
		out["material"]["config"]["reflectivityFactor"] = this->reflectivity;

		if (this->emissiveTex)
			out["material"]["config"]["emissiveTex"] = this->emissiveTex.getAsset();
		if (this->diffuseTex)
			out["material"]["config"]["diffuseTex"] = this->diffuseTex.getAsset();
		if (this->normalTex)
			out["material"]["config"]["normalTex"] = this->normalTex.getAsset();
		if (this->specularTex)
			out["material"]["config"]["specularTex"] = this->specularTex.getAsset();
		if (this->glossinessTex)
			out["material"]["config"]["glossinessTex"] = this->glossinessTex.getAsset();
		if (this->reflectivityTex)
			out["material"]["config"]["reflectivityTex"] = this->reflectivityTex.getAsset();

		if (this->maskTex)
			out["material"]["config"]["maskTex"] = this->maskTex.getAsset();
		if (isTranslucent()) {
			out["material"]["config"]["opacityFactor"] = this->opacity;
			if (this->opacityTex)
				out["material"]["config"]["opacityTex"] = this->opacityTex.getAsset();
		}

		if (shadingModel == SurfaceShadingModel::ForwardPureRefractiveGlass || shadingModel == SurfaceShadingModel::ForwardPureScreenspaceRefractiveGlass) {
			out["material"]["config"]["forwardRefractiveIOR"] = this->sfExt_ForwardRefractive_IOR;
			out["material"]["config"]["forwardRefractiveDispersion"] = this->sfExt_ForwardRefractive_Dispersion;

			out["material"]["config"]["onlyRefraction"] = shadingModel == SurfaceShadingModel::ForwardPureScreenspaceRefractiveGlass;
		}

		out["material"]["uvScale"] = this->texCoordMultiplier;
		out["material"]["uvOffset"] = this->texCoordOffset;

		out["material"]["turingShadingRate"] = nlohmann::ordered_json::array();
		out["material"]["turingShadingRate"] = { this->turingShadingRate.x, this->turingShadingRate.y };
		fout << out.dump(4);
		fout.flush();
		fout.close();
	}

	void SurfaceMaterial::deserialize(const std::string& path) {
		simdjson::dom::parser parser;
		simdjson::padded_string json = simdjson::padded_string::load(path);
		auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			return;
		}

		std::string_view shadingModelRead = data["material"]["shadingModel"].get_string().value();
		if (shadingModelRead == "unshaded") shadingModel = SurfaceShadingModel::Unshaded;
		if (shadingModelRead == "shaded") shadingModel = SurfaceShadingModel::Shaded;
		if (shadingModelRead == "emissive") shadingModel = SurfaceShadingModel::Emissive;
		if (shadingModelRead == "deferredFoliage") shadingModel = SurfaceShadingModel::DeferredFoliage;
		if (shadingModelRead == "deferredDualLayerClearCoat") shadingModel = SurfaceShadingModel::DeferredDualLayerClearCoat;
		if (shadingModelRead == "forwardRefractiveGlass") shadingModel = SurfaceShadingModel::ForwardPureRefractiveGlass;

		setTranslucent(data["material"]["blendMode"].get_string().value() == "translucent");
		setDoubleSided(data["material"]["doubleSided"].get_bool().value());
		auto config = data["material"]["config"];
		if (config["diffuseColor"].error() == simdjson::SUCCESS) {
			this->diffuseColor = SIMDJSON_READ_VEC3(config["diffuseColor"]);
		}
		if (config["specularFactor"].error() == simdjson::SUCCESS) {
			this->specular = config["specularFactor"].get_double().value();
		}
		if (config["glossinessFactor"].error() == simdjson::SUCCESS) {
			this->glossiness = config["glossinessFactor"].get_double().value();
		} else if (config["shininessFactor"].error() == simdjson::SUCCESS) { // old param
			this->glossiness = config["shininessFactor"].get_double().value() / 256.0f;
		}
		if (config["reflectivityFactor"].error() == simdjson::SUCCESS) {
			this->reflectivity = config["reflectivityFactor"].get_double().value();
		} else if (config["chrominessFactor"].error() == simdjson::SUCCESS) { // old param
			this->reflectivity = config["chrominessFactor"].get_double().value();
		}

		if (shadingModel == SurfaceShadingModel::ForwardPureRefractiveGlass) {
			this->sfExt_ForwardRefractive_IOR = config["forwardRefractiveIOR"].get_double().value();
			this->sfExt_ForwardRefractive_Dispersion = config["forwardRefractiveDispersion"].get_double().value();
			if (config["onlyRefraction"].get_bool().value()) {
				shadingModel = SurfaceShadingModel::ForwardPureScreenspaceRefractiveGlass;
			}
		}

		if (config["emissiveTex"].error() == simdjson::error_code::SUCCESS)
			this->emissiveTex = std::string(config["emissiveTex"].get_string().value());
		if (config["diffuseTex"].error() == simdjson::error_code::SUCCESS)
			this->diffuseTex = std::string(config["diffuseTex"].get_string().value());
		if (config["normalTex"].error() == simdjson::error_code::SUCCESS)
			this->normalTex = std::string(config["normalTex"].get_string().value());
		if (config["specularTex"].error() == simdjson::error_code::SUCCESS)
			this->specularTex = std::string(config["specularTex"].get_string().value());
		if (config["glossinessTex"].error() == simdjson::error_code::SUCCESS)
			this->glossinessTex = std::string(config["glossinessTex"].get_string().value());
		else if (config["shininessTex"].error() == simdjson::error_code::SUCCESS) // old param
			this->glossinessTex = std::string(config["shininessTex"].get_string().value());
		if (config["reflectivityTex"].error() == simdjson::error_code::SUCCESS)
			this->reflectivityTex = std::string(config["reflectivityTex"].get_string().value());
		else if (config["chrominessTex"].error() == simdjson::error_code::SUCCESS) // old param
			this->reflectivityTex = std::string(config["chrominessTex"].get_string().value());

		this->emissiveColor = SIMDJSON_READ_VEC4(config["emissiveColor"]);
		if (config["maskTex"].error() == simdjson::error_code::SUCCESS) {
			setMasked(true);
			this->maskTex = std::string(config["maskTex"].get_string().value());
		} else {
			setMasked(false);
		}
		if (isTranslucent()) {
			this->opacity = config["opacityFactor"].get_double().value();
			if (config["opacityTex"].error() == simdjson::error_code::SUCCESS)
				this->opacityTex = std::string(config["opacityTex"].get_string().value());
			if (config["forwardBlendingMulBias"].error() == simdjson::error_code::SUCCESS)
				this->forwardBlendingMulBias = config["forwardBlendingMulBias"].get_double().value();
		}

		this->texCoordMultiplier = {
			data["material"]["uvScale"].get_array().at(0).get_double().value(),
			data["material"]["uvScale"].get_array().at(1).get_double().value()
		};
		this->texCoordOffset = {
			data["material"]["uvOffset"].get_array().at(0).get_double().value(),
			data["material"]["uvOffset"].get_array().at(1).get_double().value()
		};
		if (data["material"]["turingShadingRate"].error() == simdjson::SUCCESS) {
			this->turingShadingRate = {
				data["material"]["turingShadingRate"].get_array().at(0).get_uint64().value(),
				data["material"]["turingShadingRate"].get_array().at(1).get_uint64().value()
			};
		}
	}

	void SurfaceMaterial::setTranslucent(bool translucent) {
		if (translucent) {
			pClassFlags |= SurfaceMaterialPipelineClassPermutationOptions_Translucent;
		} else {
			pClassFlags &= ~SurfaceMaterialPipelineClassPermutationOptions_Translucent;
		}
	}

	bool SurfaceMaterial::isTranslucent() {
		if (pClassFlags & SurfaceMaterialPipelineClassPermutationOptions_Translucent)
			return true;
		return false;
	}

	void SurfaceMaterial::setMasked(bool masked) {
		if (masked) {
			pClassFlags |= SurfaceMaterialPipelineClassPermutationOptions_AlphaTest;
		} else {
			pClassFlags &= ~SurfaceMaterialPipelineClassPermutationOptions_AlphaTest;
		}
	}
	bool SurfaceMaterial::isMasked() {
		if (pClassFlags & SurfaceMaterialPipelineClassPermutationOptions_AlphaTest)
			return true;
		return false;
	}
	void SurfaceMaterial::setDoubleSided(bool doubleSided) {
		if (doubleSided) {
			pClassFlags &= ~SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling;
		} else {
			pClassFlags |= SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling;
		}
	}

	bool SurfaceMaterial::isDoubleSided() {
		return !(pClassFlags & SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling);
	}

	void Material::loadAllTextures() {
		if (this->diffuseTex) resourceSystem->loadTexture(this->diffuseTex);
		if (this->emissiveTex) resourceSystem->loadTexture(this->emissiveTex);
		if (this->normalTex) resourceSystem->loadTexture(this->normalTex);
		if (this->specularTex) resourceSystem->loadTexture(this->specularTex);
		if (this->glossinessTex) resourceSystem->loadTexture(this->glossinessTex);
		if (this->reflectivityTex) resourceSystem->loadTexture(this->reflectivityTex);
		if (this->maskTex) resourceSystem->loadTexture(this->maskTex);
		if (this->opacityTex) resourceSystem->loadTexture(this->opacityTex);
	}

	std::vector<AssetID*> Material::getAllTexturesReference() {
		std::vector<AssetID*> textures;
		textures.reserve(8);
		if (this->diffuseTex) textures.push_back(&this->diffuseTex);
		if (this->emissiveTex) textures.push_back(&this->emissiveTex);
		if (this->normalTex) textures.push_back(&this->normalTex);
		if (this->specularTex) textures.push_back(&this->specularTex);
		if (this->glossinessTex) textures.push_back(&this->glossinessTex);
		if (this->reflectivityTex) textures.push_back(&this->reflectivityTex);
		if (this->maskTex) textures.push_back(&this->maskTex);
		if (this->opacityTex) textures.push_back(&this->opacityTex);

		return textures;
	}
	std::vector<AssetID> Material::getAllTextures() {
		std::vector<AssetID> textures;
		textures.reserve(8);
		if (this->diffuseTex) textures.push_back(this->diffuseTex);
		if (this->emissiveTex) textures.push_back(this->emissiveTex);
		if (this->normalTex) textures.push_back(this->normalTex);
		if (this->specularTex) textures.push_back(this->specularTex);
		if (this->glossinessTex) textures.push_back(this->glossinessTex);
		if (this->reflectivityTex) textures.push_back(this->reflectivityTex);
		if (this->maskTex) textures.push_back(this->maskTex);
		if (this->opacityTex) textures.push_back(this->opacityTex);

		return textures;
	}

	MaterialBufferData DecalMaterial::getDecalMaterialBufferData() {
		MaterialBufferData dcData = getBaseMaterialBufferData();
		dcData.dbufferModel = static_cast<uint32_t>(model);
		return dcData;
	}
	void DecalMaterial::updateMaterial() {
		MaterialBufferData sfData = getDecalMaterialBufferData();
		buffer->writeToBuffer(&sfData);
		buffer->flush();
	}

	void DecalMaterial::serialize(const std::string& path) {
		nlohmann::ordered_json out{};
		std::ofstream fout(path);

		out["version"] = ENGINE_VERSION;
		out["assetType"] = "material";
		out["material"]["class"] = "deferredDecal";
		const char* dmodel;
		if (!directGBuffer) {
			switch (model) {
			case(DeferredDecalModel::DBufferDiffuse):
				dmodel = "diffuse";
				break;
			case(DeferredDecalModel::DBufferDiffuseNormal):
				dmodel = "diffuseNormal";
				break;
			case(DeferredDecalModel::DBufferDiffuseSpecularGlossinessReflectivity):
				dmodel = "diffuseSpecularGlossinessReflectivity";
				break;
			case(DeferredDecalModel::DBufferDiffuseSpecularGlossinessReflectivityNormal):
				dmodel = "diffuseSpecularGlossinessReflectivityNormal";
				break;
			case(DeferredDecalModel::DBufferSpecularGlossinessReflectivity):
				dmodel = "specularGlossinessReflectivity";
				break;
			case(DeferredDecalModel::DBufferSpecularGlossinessReflectivityNormal):
				dmodel = "specularGlossinessReflectivityNormal";
				break;
			case(DeferredDecalModel::DBufferNormal):
				dmodel = "normal";
				break;
			default:
				dmodel = "null";
			}
		} else {
			switch (model) {
			case(DeferredDecalModel::GBufferMultiplyDiffuse):
				dmodel = "multiplyDiffuse";
				break;
			case(DeferredDecalModel::GBufferMultiplySpecularGlossinessReflectivity):
				dmodel = "multiplySpecularGlossinessReflectivity";
				break;
			case(DeferredDecalModel::GBufferMultiplyDiffuseSpecularGlossinessReflectivity):
				dmodel = "multiplyDiffuseSpecularGlossinessReflectivity";
				break;
			case(DeferredDecalModel::GBufferAddEmission):
				dmodel = "addEmission";
				break;
			default:
				dmodel = "null";
			}
		}
		out["material"]["model"] = dmodel;
		out["material"]["directGBuffer"] = directGBuffer;
		const char* blendMode;
		switch (dbufferBlendMode) {
		case(DBufferBlendMode::Normal):
			blendMode = "normal";
			break;
		case(DBufferBlendMode::Multiplicative):
			blendMode = "multiplicative";
			break;
		case(DBufferBlendMode::Additive):
			blendMode = "additive";
			break;
		case(DBufferBlendMode::Erase):
			blendMode = "erase";
			break;
		default:
			dmodel = "null";
		}
		out["material"]["dbufferBlendMode"] = blendMode;

		out["material"]["config"]["diffuseColor"] = this->diffuseColor;
		out["material"]["config"]["emissiveColor"] = this->emissiveColor;

		out["material"]["config"]["specularFactor"] = this->specular;
		out["material"]["config"]["glossinessFactor"] = this->glossiness;
		out["material"]["config"]["reflectivityFactor"] = this->reflectivity;

		if (this->emissiveTex)
			out["material"]["config"]["emissiveTex"] = this->emissiveTex.getAsset();
		if (this->diffuseTex)
			out["material"]["config"]["diffuseTex"] = this->diffuseTex.getAsset();
		if (this->normalTex)
			out["material"]["config"]["normalTex"] = this->normalTex.getAsset();
		if (this->specularTex)
			out["material"]["config"]["specularTex"] = this->specularTex.getAsset();
		if (this->glossinessTex)
			out["material"]["config"]["glossinessTex"] = this->glossinessTex.getAsset();
		if (this->reflectivityTex)
			out["material"]["config"]["reflectivityTex"] = this->reflectivityTex.getAsset();

		out["material"]["uvScale"] = this->texCoordMultiplier;
		out["material"]["uvOffset"] = this->texCoordOffset;

		out["material"]["turingShadingRate"] = nlohmann::ordered_json::array();
		out["material"]["turingShadingRate"] = { this->turingShadingRate.x, this->turingShadingRate.y };
		fout << out.dump(4);
		fout.flush();
		fout.close();
	}

	void DecalMaterial::deserialize(const std::string& path) {
		simdjson::dom::parser parser;
		simdjson::padded_string json = simdjson::padded_string::load(path);
		auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			return;
		}

		directGBuffer = data["material"]["directGBuffer"].get_bool().value();
		std::string_view modelRead = data["material"]["model"].get_string().value();
		if (!directGBuffer) {
			if (modelRead == "diffuse") model = DeferredDecalModel::DBufferDiffuse;
			if (modelRead == "diffuseNormal") model = DeferredDecalModel::DBufferDiffuseNormal;
			if (modelRead == "diffuseSpecularGlossinessReflectivity") model = DeferredDecalModel::DBufferDiffuseSpecularGlossinessReflectivity;
			if (modelRead == "diffuseSpecularGlossinessReflectivityNormal") model = DeferredDecalModel::DBufferDiffuseSpecularGlossinessReflectivityNormal;
			if (modelRead == "specularGlossinessReflectivity") model = DeferredDecalModel::DBufferSpecularGlossinessReflectivity;
			if (modelRead == "specularGlossinessReflectivityNormal") model = DeferredDecalModel::DBufferSpecularGlossinessReflectivityNormal;
			if (modelRead == "normal") model = DeferredDecalModel::DBufferNormal;
		} else {
			if (modelRead == "multiplyDiffuse") model = DeferredDecalModel::GBufferMultiplyDiffuse;
			if (modelRead == "multiplySpecularGlossinessReflectivity") model = DeferredDecalModel::GBufferMultiplySpecularGlossinessReflectivity;
			if (modelRead == "multiplyDiffuseSpecularGlossinessReflectivity") model = DeferredDecalModel::GBufferMultiplyDiffuseSpecularGlossinessReflectivity;
			if (modelRead == "addEmission")  model = DeferredDecalModel::GBufferAddEmission;
		}
		std::string_view dbufferBlendModeRead = data["material"]["dbufferBlendMode"].get_string().value();
		if (dbufferBlendModeRead == "normal") dbufferBlendMode = DBufferBlendMode::Normal;
		if (dbufferBlendModeRead == "multiplicative") dbufferBlendMode = DBufferBlendMode::Multiplicative;
		if (dbufferBlendModeRead == "additive") dbufferBlendMode = DBufferBlendMode::Additive;
		if (dbufferBlendModeRead == "erase") dbufferBlendMode = DBufferBlendMode::Erase;

		auto config = data["material"]["config"];
		if (config["diffuseColor"].error() == simdjson::SUCCESS) {
			this->diffuseColor = SIMDJSON_READ_VEC3(config["diffuseColor"]);
		}
		if (config["specularFactor"].error() == simdjson::SUCCESS) {
			this->specular = config["specularFactor"].get_double().value();
		}
		if (config["glossinessFactor"].error() == simdjson::SUCCESS) {
			this->glossiness = config["glossinessFactor"].get_double().value();
		}
		if (config["reflectivityFactor"].error() == simdjson::SUCCESS) {
			this->reflectivity = config["reflectivityFactor"].get_double().value();
		}
		if (config["diffuseTex"].error() == simdjson::error_code::SUCCESS)
			this->diffuseTex = std::string(config["diffuseTex"].get_string().value());
		if (config["normalTex"].error() == simdjson::error_code::SUCCESS)
			this->normalTex = std::string(config["normalTex"].get_string().value());
		if (config["specularTex"].error() == simdjson::error_code::SUCCESS)
			this->specularTex = std::string(config["specularTex"].get_string().value());
		if (config["glossinessTex"].error() == simdjson::error_code::SUCCESS)
			this->glossinessTex = std::string(config["glossinessTex"].get_string().value());
		if (config["reflectivityTex"].error() == simdjson::error_code::SUCCESS)
			this->reflectivityTex = std::string(config["reflectivityTex"].get_string().value());

		if (config["opacityFactor"].error() == simdjson::SUCCESS) {
			this->opacity = config["opacityFactor"].get_double().value();
		}
		if (config["opacityTex"].error() == simdjson::error_code::SUCCESS)
			this->opacityTex = std::string(config["opacityTex"].get_string().value());
		
		this->texCoordMultiplier = {
			data["material"]["uvScale"].get_array().at(0).get_double().value(),
			data["material"]["uvScale"].get_array().at(1).get_double().value()
		};
		this->texCoordOffset = {
			data["material"]["uvOffset"].get_array().at(0).get_double().value(),
			data["material"]["uvOffset"].get_array().at(1).get_double().value()
		};
		if (data["material"]["turingShadingRate"].error() == simdjson::SUCCESS) {
			this->turingShadingRate = {
				data["material"]["turingShadingRate"].get_array().at(0).get_uint64().value(),
				data["material"]["turingShadingRate"].get_array().at(1).get_uint64().value()
			};
		}
	}

	std::vector<AssetID*> TerrainMaterial::getAllTexturesReference() {
		std::vector<AssetID*> textures;
		textures.reserve(5);
		if (this->diffuseTex) textures.push_back(&this->diffuseTex);
		if (this->specularTex) textures.push_back(&this->specularTex);
		if (this->glossinessTex) textures.push_back(&this->glossinessTex);
		if (this->reflectivityTex) textures.push_back(&this->reflectivityTex);
		if (this->heightTex) textures.push_back(&this->heightTex);
		return textures;
	}

	std::vector<AssetID> TerrainMaterial::getAllTextures() {
		std::vector<AssetID> textures;
		textures.reserve(5);
		if (this->diffuseTex) textures.push_back(this->diffuseTex);
		if (this->specularTex) textures.push_back(this->specularTex);
		if (this->glossinessTex) textures.push_back(this->glossinessTex);
		if (this->reflectivityTex) textures.push_back(this->reflectivityTex);
		if (this->heightTex) textures.push_back(this->heightTex);
		return textures;
	}

	void TerrainMaterial::loadAllTextures() {
		if (this->diffuseTex) resourceSystem->loadTexture(this->diffuseTex);
		if (this->specularTex) resourceSystem->loadTexture(this->specularTex);
		if (this->glossinessTex) resourceSystem->loadTexture(this->glossinessTex);
		if (this->reflectivityTex) resourceSystem->loadTexture(this->reflectivityTex);
		if (this->heightTex) resourceSystem->loadTexture(this->heightTex);
	}

	TerrainMaterialBufferData TerrainMaterial::getTerrainMaterialBufferData() {
		TerrainMaterialBufferData matData{};

		matData.diffuse = glm::vec4(this->diffuseColor, 1.0f);
		matData.specular = this->specular;
		matData.glossiness = this->glossiness;
		matData.reflectivity = this->reflectivity;
		matData.parallaxDepth = this->parallaxDepth;
		matData.bumpStrength = this->bumpIntensity;
		matData.texCoordMultiplier = this->texCoordMultiplier;

		if (diffuseTex) {
			matData.diffuseTex_id = resourceSystem->retrieveTexture(this->diffuseTex)->getResourceIndex();
		}
		if (specularTex) {
			matData.specularTex_id = resourceSystem->retrieveTexture(this->specularTex)->getResourceIndex();
		}
		if (glossinessTex) {
			matData.glossinessTex_id = resourceSystem->retrieveTexture(this->glossinessTex)->getResourceIndex();
		}
		if (reflectivityTex) {
			matData.reflectivityTex_id = resourceSystem->retrieveTexture(this->reflectivityTex)->getResourceIndex();
		}
		if (heightTex) {
			matData.heightTex_id = resourceSystem->retrieveTexture(this->heightTex)->getResourceIndex();
		}
		return matData;
	}

	void TerrainMaterial::updateMaterial() {
		TerrainMaterialBufferData trData = getTerrainMaterialBufferData();
		buffer->writeToBuffer(&trData);
		buffer->flush();
	}

	void TerrainMaterial::serialize(const std::string& path) {
		nlohmann::ordered_json out{};
		std::ofstream fout(path);

		out["version"] = ENGINE_VERSION;
		out["material"]["class"] = "terrain";		
		
		out["material"]["config"]["diffuseColor"] = this->diffuseColor;

		out["material"]["config"]["specularFactor"] = this->specular;
		out["material"]["config"]["glossinessFactor"] = this->glossiness;
		out["material"]["config"]["reflectivityFactor"] = this->reflectivity;
		out["material"]["config"]["parallaxDepthFactor"] = this->parallaxDepth;
		out["material"]["config"]["bumpIntensityFactor"] = this->bumpIntensity;

		if (this->diffuseTex)
			out["material"]["config"]["diffuseTex"] = this->diffuseTex.getAsset();
		if (this->specularTex)
			out["material"]["config"]["specularTex"] = this->specularTex.getAsset();
		if (this->glossinessTex)
			out["material"]["config"]["glossinessTex"] = this->glossinessTex.getAsset();
		if (this->reflectivityTex)
			out["material"]["config"]["reflectivityTex"] = this->reflectivityTex.getAsset();
		if (this->heightTex)
			out["material"]["config"]["heightTex"] = this->heightTex.getAsset();

		out["material"]["uvScale"] = this->texCoordMultiplier;

		out["material"]["turingShadingRate"] = nlohmann::ordered_json::array();
		out["material"]["turingShadingRate"] = { this->turingShadingRate.x, this->turingShadingRate.y };
		fout << out.dump(4);
		fout.flush();
		fout.close();
	}

	void TerrainMaterial::deserialize(const std::string& path) {
		simdjson::dom::parser parser;
		simdjson::padded_string json = simdjson::padded_string::load(path);
		auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			return;
		}

		auto config = data["material"]["config"];
		if (config["diffuseColor"].error() == simdjson::SUCCESS) {
			this->diffuseColor = SIMDJSON_READ_VEC3(config["diffuseColor"]);
		}
		if (config["specularFactor"].error() == simdjson::SUCCESS) {
			this->specular = config["specularFactor"].get_double().value();
		}
		if (config["glossinessFactor"].error() == simdjson::SUCCESS) {
			this->glossiness = config["glossinessFactor"].get_double().value();
		}
		if (config["reflectivityFactor"].error() == simdjson::SUCCESS) {
			this->reflectivity = config["reflectivityFactor"].get_double().value();
		}
		if (config["parallaxDepthFactor"].error() == simdjson::SUCCESS) {
			this->parallaxDepth = config["parallaxDepthFactor"].get_double().value();
		}
		if (config["bumpIntensityFactor"].error() == simdjson::SUCCESS) {
			this->bumpIntensity = config["bumpIntensityFactor"].get_double().value();
		}

		if (config["diffuseTex"].error() == simdjson::error_code::SUCCESS)
			this->diffuseTex = std::string(config["diffuseTex"].get_string().value());
		if (config["specularTex"].error() == simdjson::error_code::SUCCESS)
			this->specularTex = std::string(config["specularTex"].get_string().value());
		if (config["glossinessTex"].error() == simdjson::error_code::SUCCESS)
			this->glossinessTex = std::string(config["glossinessTex"].get_string().value());
		if (config["reflectivityTex"].error() == simdjson::error_code::SUCCESS)
			this->reflectivityTex = std::string(config["reflectivityTex"].get_string().value());
		if (config["heightTex"].error() == simdjson::error_code::SUCCESS)
			this->heightTex = std::string(config["heightTex"].get_string().value());

		this->texCoordMultiplier = {
			data["material"]["uvScale"].get_array().at(0).get_double().value(),
			data["material"]["uvScale"].get_array().at(1).get_double().value()
		};
		if (data["material"]["turingShadingRate"].error() == simdjson::SUCCESS) {
			this->turingShadingRate = {
				data["material"]["turingShadingRate"].get_array().at(0).get_uint64().value(),
				data["material"]["turingShadingRate"].get_array().at(1).get_uint64().value()
			};
		}
	}
}