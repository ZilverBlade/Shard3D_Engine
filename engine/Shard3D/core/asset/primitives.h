#pragma once

#include <glm/glm.hpp>
#include <vector>
#include <array>
#include "assetid.h"
#include "../../s3dstd.h"
#include <half.h>
#include "../rendering/bvh.h"

namespace Shard3D {
	struct VTX_pos_offset_12b_ {
		glm::vec3 position{};
	};
	struct VTX_uv_cmpr_offset_4b_ {
		half uv_x, uv_y;
	};
	struct VTX_norm_cmpr_offset_4b_ {
		uint32_t cmp_normal;
	};
	struct VTX_tg_cmpr_offset_4b_ {
		uint32_t cmp_tangent;
	};
	struct Vertex {
		glm::vec3 position{};
		glm::vec3 normal{};
		glm::vec3 tangent{};
		glm::vec2 uv{};

		static std::vector<VkVertexInputBindingDescription> getPositionBindingDescriptions();
		static std::vector<VkVertexInputAttributeDescription> getPositionAttributeDescriptions();

		static std::vector<VkVertexInputBindingDescription> getUVBindingDescriptions();
		static std::vector<VkVertexInputAttributeDescription> getUVAttributeDescriptions();

		static std::vector<VkVertexInputBindingDescription> getNormalBindingDescriptions();
		static std::vector<VkVertexInputAttributeDescription> getNormalAttributeDescriptions();

		static std::vector<VkVertexInputBindingDescription> getTangentBindingDescriptions();
		static std::vector<VkVertexInputAttributeDescription> getTangentAttributeDescriptions();

		bool operator==(const Vertex& other) const {
			return position == other.position && normal == other.normal && uv == other.uv;
		}
	};
#define MAX_VERTEX_BONES 4
	struct VertexSkin {
		std::array<uint8_t, 4> weights = std::array<uint8_t, 4>({ 0, 0, 0, 0 });
		std::array<uint8_t, 4> bones = std::array<uint8_t, 4>({ 0xFF, 0xFF, 0xFF, 0xFF });
	};
	
	struct MorphTargetRawData {
		uint32_t targetIndex;
		VolumeBounds bounds;
		SphereBounds sphereBounds;
		uint32_t* targetVectorPositions;
		uint32_t* targetVectorNormals;
		uint32_t* targetVectorTangents;
	};
	struct BoneRawData {
		uint8_t boneIndex;
		uint8_t parentBone;
		uint32_t childBoneCount;
		uint8_t* childBones;
		VolumeBounds bounds;
		SphereBounds sphereBounds;
		glm::mat4 offsetMatrix;
	};
	struct VertexSkinData {
		uint32_t maxBonesPerVertex = MAX_VERTEX_BONES;
		uint8_t* boneIDs;
		uint8_t* boneWeights;
	};
	struct SkeletonRawData {
		uint32_t vertexCount;
		VertexSkinData* skinData;
		uint32_t morphTargetCount;
		MorphTargetRawData** morphTargets;
		uint32_t boneCount;
		BoneRawData** bones;
		char** morphTargetNames;
		char** boneNames;
	};
	struct BoneAnimationRawData {
		uint32_t translationKeyFrameCount;
		glm::vec3* translationKeyFrames;
		float* translationKeyFrameTimeStamps;

		uint32_t quaternionKeyFrameCount;
		glm::vec4* quaternionKeyFrames;
		float* quaternionKeyFrameTimeStamps;

		uint32_t scaleKeyFrameCount;
		glm::vec3* scaleKeyFrames;
		float* scaleKeyFrameTimeStamps;
	};
	struct MorphAnimationRawData {
		uint32_t factorKeyFrameCount;
		float* factorKeyFrames;
		float* factorKeyFrameTimeStamps;
	};
	struct SkeletalAnimationRawData {
		uint32_t boneAnimationCount;
		BoneAnimationRawData** boneAnimations;
		uint32_t morphAnimationCount;
		MorphAnimationRawData** morphAnimations;
		float maxDuration;
	};
	struct SubmeshRawData {
		VolumeBounds bounds;
		SphereBounds sphereBounds;
		uint32_t vtxCount;
		VTX_pos_offset_12b_* vtxPositions;
		VTX_norm_cmpr_offset_4b_* vtxNormals;
		VTX_tg_cmpr_offset_4b_* vtxTangents;
		uint32_t uvCount; // future proof in case meshes use multiple UVs 
		VTX_uv_cmpr_offset_4b_** vtxUVs;

		uint32_t indCount;
		uint32_t* vtxIndices;

		uint32_t materialIndex;
	};
	struct MeshRawData {
		uint32_t submeshCount;
		SubmeshRawData** submeshes;
		char** materialNames;
		VolumeBounds entireBounds;
		SphereBounds entireSphereBounds;
	};

}