#pragma once

namespace Shard3D {
	class Resource {
	public:
		Resource() = default;
		virtual ~Resource() {}
		// delete copy constructors
		Resource(const Resource&) = delete;
		Resource& operator=(const Resource&) = delete;
	};

}