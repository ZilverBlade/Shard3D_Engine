#include "image_tools.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include <stb_image_write.h>
#include <bitmap.h>

namespace Shard3D {
	uint8_t* ImageResource::getSTBImage(const std::string& filepath, int* x, int* y, int* comp, int req_comp) {
		SHARD3D_LOG("Loading STBImage {0}", filepath);
		return stbi_load(filepath.c_str(), x, y, comp, req_comp);
	}
	uint8_t* ImageResource::getSTBImage(const void* memory, int pixelCount, int* x, int* y, int* comp, int req_comp) {
		SHARD3D_LOG("Loading STBImage {0}", memory);
		return stbi_load_from_memory(reinterpret_cast<const unsigned char*>(memory), pixelCount, x, y, comp, req_comp);
	}
	void ImageResource::freeSTBImage(void* pixels) {
		stbi_image_free(pixels);
	}
	void ImageResource::storeImage(const void* pixels, int width, int height, int channelCount, const std::string& path) {
		Bitmap bitmap;
		PixelMatrix matrix;
		matrix.resize(height);
		for (size_t y = 0; y < height; y++) {
			std::vector<Pixel> row{};
			row.resize(width);
			for (size_t x = 0; x < width; x++) {
				Pixel pixel{};
				size_t pixelIndex = width * y * 4 + x * 4;

				pixel.red = static_cast<int>(reinterpret_cast<const uint8_t*>(pixels)[pixelIndex + 0u]);
				pixel.green = static_cast<int>(reinterpret_cast<const uint8_t*>(pixels)[pixelIndex + 1u]);
				pixel.blue = static_cast<int>(reinterpret_cast<const uint8_t*>(pixels)[pixelIndex + 2u]);
				pixel.alpha = static_cast<int>(reinterpret_cast<const uint8_t*>(pixels)[pixelIndex + 3u]) * (channelCount == 4u) | (channelCount == 3u) * 255;

				row[x] = pixel;
			}
			matrix[y] = row;
		}
		bitmap.fromPixelMatrix(matrix);
		bitmap.save(path, channelCount * 8);
	}
}