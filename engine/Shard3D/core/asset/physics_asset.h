#pragma once

#include <glm/glm.hpp>
#include "assetid.h" 
#include <vector>
#include <entt.hpp>
#include <string>
namespace Shard3D {
	enum class PhysicsColliderType {
		Sphere,
		Box,
		Plane,
		Capsule,
		Cylinder,
		ConvexHullAsset,
		MeshAsset,
		Terrain,
	};
	enum class PhysicsConstraintType {
		Fixed,
		Ball,
		Hinge,
		DualHinge,
		Slider,
		AngularMotor,
		LinearMotor,
		Piston
	};
	struct PhysicsCollisionData {
		entt::entity actor = entt::null;
		glm::vec3 position;
		glm::vec3 normal;
	};
	class PhysicsMaterial {
	public:
		void serialize(const std::string& path);
		void deserialize(const std::string& path);
		float frictionCoef = 1.0f; // contact to rigid surface e.g. stone, asphalt, rubber
		float looseFrictionCoef = 0.0f; // contact to loose surface e.g. grass, dirt, gravel
		float elasticity = 0.0f; // 0.0 - 1.0
		float softness = 0.0f; // magic values above 0.0
		float absorption = 0.0f; // magic values above 0.0
		glm::vec3 conveyorDirection = glm::vec3(0.0);
	};
	class PhysicsTireMaterial : public PhysicsMaterial {
	public:
		void deserializeTire(const std::string& path);
		float rollingResistance = 0.2f;

		float longSlipStiffness = 2.5f; // B
		float longSlipShape = 2.0f; // C
		float longSlipPeak = 1.0f; // D
		float longSlipCurvature = 0.6f; // E

		float latSlipStiffness = 0.714f; // B
		float latSlipShape = 1.4f; // C
		float latSlipPeak = 1.0f; // D
		float latSlipCurvature = -0.2f; // E
	};
	struct PhysicsAsset {
		PhysicsColliderType colliderType = PhysicsColliderType::Sphere;

		std::vector<glm::vec3> convexPoints;
		AssetID trimeshAsset = AssetID::null();
		glm::vec3 colliderPositionOffset;
		glm::vec3 colliderRotationOffset;

		glm::vec3 computedCOM;
		glm::mat3 computedInteriaTensor;
		bool enableCustomCOM = false;

		glm::vec3 boxExtent{ 1.0f };
		float sphereRadius = 1.0f;
		float cylindricalRadius = 0.5f;
		float cylindricalLength = 2.0f;
		float mass = 1.0f;
	};
}