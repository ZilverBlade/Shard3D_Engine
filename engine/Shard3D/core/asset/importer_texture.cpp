#include "importer_texture.h"
#include "image_tools.h"
#include "texture2d.h"
#include <ktx.h>
#include <fstream>
#include <zstd.h>
#include "../../systems/handlers/resource_system.h"
//#include "../../systems/post_fx/post_processing_fx.h"
//#include <glm/glm.hpp>
//#include <glm/gtc/constants.hpp>

namespace Shard3D::Resources {
	ktxTexture2* TextureImporter::loadTextureKTX(
		S3DDevice& device,
		ResourceSystem* resourceSystem,
		TextureLoadInfo loadInfo,
		AssetID ktxLocation
	) {
		ktxTexture2* ktxTex = loadCompressedCachedTextureKTX(ktxLocation.getAbsolute().c_str());

		bool needsModifications = loadInfo.trueResolutionWidth != -1 || loadInfo.trueResolutionHeight != -1;
		Texture2D* temporary;
		if (needsModifications) {
			auto loadInfoTemp = loadInfo;
			loadInfoTemp.compression = TextureCompression::Lossless;
			loadInfoTemp.channels = TextureChannels::RGBA;
			temporary = new Texture2D(device, resourceSystem->getDescriptor(), ktxTex, loadInfoTemp);
		}

		if (loadInfo.trueResolutionWidth != -1 || loadInfo.trueResolutionHeight != -1) {
			VkImage image = temporary->getImage();

			VkCommandBuffer commandBuffer = device.beginSingleTimeCommands();
			device.resizeTexture(commandBuffer, image, VK_FORMAT_R8G8B8A8_UNORM,
				ktxTex->baseWidth, ktxTex->baseHeight,
				loadInfo.trueResolutionWidth == -1 ? ktxTex->baseWidth: loadInfo.trueResolutionWidth, loadInfo.trueResolutionHeight == -1 ? ktxTex->baseHeight : loadInfo.trueResolutionHeight,
				VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
			);
			device.endSingleTimeCommands(commandBuffer);
		}

		if (needsModifications) {
			ktxTextureCreateInfo txCreateInfo{};
			memset(&txCreateInfo, 0, sizeof(ktxTextureCreateInfo));
			txCreateInfo.baseWidth = ktxTex->baseWidth;
			txCreateInfo.baseHeight = ktxTex->baseHeight;
			txCreateInfo.baseDepth = 1;
			txCreateInfo.generateMipmaps = KTX_FALSE;
			txCreateInfo.vkFormat = VK_FORMAT_R8G8B8A8_UNORM;
			txCreateInfo.numDimensions = 1;
			txCreateInfo.isArray = KTX_FALSE;
			txCreateInfo.numLevels = 1;
			txCreateInfo.numLayers = 1;
			txCreateInfo.numFaces = 1;
			txCreateInfo.numDimensions = 2;
			ktxTexture_Destroy(ktxTexture(ktxTex));
			ktxResult cErr = ktxTexture2_Create(&txCreateInfo, KTX_TEXTURE_CREATE_ALLOC_STORAGE, &ktxTex);
			VkBuffer mipBuffer;
			VkDeviceMemory mipBufferMemory;
			device.createBuffer(
				txCreateInfo.baseWidth * txCreateInfo.baseHeight * 4,
				VK_BUFFER_USAGE_TRANSFER_DST_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
				mipBuffer,
				mipBufferMemory
			);
			VkImage image = temporary->getImage();

			VkCommandBuffer commandBuffer = device.beginSingleTimeCommands();
			device.transitionImageLayout(commandBuffer, temporary->getImage(), temporary->getFormat(), temporary->getImageLayout(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, 0, 1, 0, 1);
			device.copyImageToBuffer(commandBuffer, image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, mipBuffer, txCreateInfo.baseWidth, txCreateInfo.baseHeight, 1, 0);
			device.endSingleTimeCommands(commandBuffer);
			void* mipData;
			vkMapMemory(device.device(), mipBufferMemory, 0, txCreateInfo.baseWidth * txCreateInfo.baseHeight * 4, 0, &mipData);
			ktxResult sifmMipErr = ktxTexture_SetImageFromMemory(ktxTexture(ktxTex), 0, 0, 0, reinterpret_cast<ktx_uint8_t*>(mipData), txCreateInfo.baseWidth * txCreateInfo.baseHeight * 4);
			vkUnmapMemory(device.device(), mipBufferMemory);
			vkDestroyBuffer(device.device(), mipBuffer, nullptr);
			vkFreeMemory(device.device(), mipBufferMemory, nullptr);

			// clean up
			delete temporary;
		}


		if (loadInfo.enableMipMaps) {
			if (ktxTex->numFaces > 1 || ktxTex->numDimensions != 2) SHARD3D_NOIMPL_C;

			VkBuffer stagingBuffer;
			VkDeviceMemory stagingBufferMemory;

			device.createBuffer(
			ktxTex->dataSize,
			VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			stagingBuffer,
			stagingBufferMemory
			); // full image size (aka mip level 0)

			size_t dataSize = ktxTex->dataSize;
			void* data;
			vkMapMemory(device.device(), stagingBufferMemory, 0, dataSize, 0, &data);
			memcpy(data, ktxTex->pData, static_cast<size_t>(ktxTex->dataSize));
			
			uint32_t componentCount;
			uint32_t componentSize;

			ktxTexture2_GetComponentInfo(ktxTex, &componentCount, &componentSize);
			size_t sizePerPixel = componentCount * componentSize;

			VkFormat mFormat = getVkFormatTextureFormat(loadInfo.format, loadInfo.colorSpace, static_cast<TextureChannels>(componentCount - 1), TextureCompression::Lossless); // only can blit lossless images
			VkExtent3D mExtent = { ktxTex->baseWidth, ktxTex->baseHeight, 1U };

			VkImage mTextureImage;
			VkDeviceMemory mTextureImageMemory;

			uint32_t mMipLevels = std::floor(std::log2(std::max(mExtent.width, mExtent.height))) + 1U;

			VkImageCreateInfo imageInfo{};
			imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
			imageInfo.imageType = VK_IMAGE_TYPE_2D;
			imageInfo.extent = mExtent;
			imageInfo.mipLevels = mMipLevels;
			imageInfo.arrayLayers = ktxTex->numLayers;
			imageInfo.format = mFormat;
			imageInfo.flags = 0;
			imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
			imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			imageInfo.usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT |
				VK_IMAGE_USAGE_SAMPLED_BIT;
			imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
			imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

			device.createImageWithInfo(
				imageInfo,
				VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
				mTextureImage,
				mTextureImageMemory
			);

			VkCommandBuffer commandBuffer = device.beginSingleTimeCommands();
			device.transitionImageLayout(
				commandBuffer,
				mTextureImage,
				mFormat,
				VK_IMAGE_LAYOUT_UNDEFINED,
				VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
				0,
				mMipLevels,
				0,
				ktxTex->numLayers
			);
			device.copyBufferToImage(
				commandBuffer,
				stagingBuffer,
				mTextureImage,
				ktxTex->baseWidth, 
				ktxTex->baseHeight,
				ktxTex->numLayers
			);
			device.generateMipMaps(commandBuffer, mTextureImage, mFormat, mExtent.width, mExtent.height, 1, mMipLevels, 1, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

			device.endSingleTimeCommands(commandBuffer);
			ktxTexture_Destroy(ktxTexture(ktxTex));

			ktxTextureCreateInfo txCreateInfo{};
			memset(&txCreateInfo, 0, sizeof(ktxTextureCreateInfo));
			txCreateInfo.baseWidth = mExtent.width;
			txCreateInfo.baseHeight = mExtent.height;
			txCreateInfo.baseDepth = 1;
			txCreateInfo.generateMipmaps = KTX_FALSE;
			txCreateInfo.vkFormat = mFormat;
			txCreateInfo.numDimensions = 1;
			txCreateInfo.isArray = KTX_FALSE;
			txCreateInfo.numLevels = mMipLevels;
			txCreateInfo.numLayers = 1;
			txCreateInfo.numFaces = 1;
			txCreateInfo.numDimensions = 2;
			ktxResult cErr = ktxTexture2_Create(&txCreateInfo, KTX_TEXTURE_CREATE_ALLOC_STORAGE, &ktxTex);
			ktxResult sifmErr = ktxTexture_SetImageFromMemory(ktxTexture(ktxTex), 0, 0, 0, reinterpret_cast<ktx_uint8_t*>(data), dataSize);
			vkUnmapMemory(device.device(), stagingBufferMemory);
			vkDestroyBuffer(device.device(), stagingBuffer, nullptr);
			vkFreeMemory(device.device(), stagingBufferMemory, nullptr);

			uint32_t mipWidth = mExtent.width / 2;
			uint32_t mipHeight = mExtent.height / 2;

			for (uint32_t i = 1; i < mMipLevels; i++) {
				size_t mipDataSize = mipWidth * mipHeight * sizePerPixel;
				VkBuffer mipBuffer;
				VkDeviceMemory mipBufferMemory;
				device.createBuffer(
					mipDataSize,
					VK_BUFFER_USAGE_TRANSFER_DST_BIT,
					VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
					mipBuffer,
					mipBufferMemory
				);
				VkCommandBuffer commandBuffer = device.beginSingleTimeCommands();
				device.copyImageToBuffer(commandBuffer, mTextureImage, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, mipBuffer, mipWidth, mipHeight, 1, i);
				device.endSingleTimeCommands(commandBuffer);
				void* mipData;
				vkMapMemory(device.device(), mipBufferMemory, 0, mipDataSize, 0, &mipData);
				ktxResult sifmMipErr = ktxTexture_SetImageFromMemory(ktxTexture(ktxTex), i, 0, 0, reinterpret_cast<ktx_uint8_t*>(mipData), mipDataSize);
				vkUnmapMemory(device.device(), mipBufferMemory);
				vkDestroyBuffer(device.device(), mipBuffer, nullptr);
				vkFreeMemory(device.device(), mipBufferMemory, nullptr);
				if (mipWidth > 1) mipWidth /= 2;
				if (mipHeight > 1) mipHeight /= 2;
			}

			vkDestroyImage(device.device(), mTextureImage, nullptr);
		}

		if (loadInfo.compression != TextureCompression::Lossless) {
			ktxBasisParams params{};
			memset(&params, 0, sizeof(ktxBasisParams));
			params.structSize = sizeof(ktxBasisParams);
			params.compressionLevel = 2;
			params.qualityLevel = 128; // faster, comprimise quality for speed, max quality can be set when packaging game
			params.noSSE = false;
			params.threadCount = 4; 
			params.normalMap = loadInfo.isNormalMap;
			params.maxSelectors = 9000;
			params.maxEndpoints = 9000;
			strncpy(params.inputSwizzle, loadInfo.compression == TextureCompression::BC5 ? "rrrg" : "rgba", 4);
			ktxResult cbErr = ktxTexture2_CompressBasisEx(ktxTex, &params);
			ktx_transcode_fmt_e tcFormat = KTX_TTF_NOSELECTION;
			switch (loadInfo.compression) {
			case (TextureCompression::BC1): tcFormat = KTX_TTF_BC1_RGB; break;
			case (TextureCompression::BC3): tcFormat = KTX_TTF_BC3_RGBA; break;
			case (TextureCompression::BC4): tcFormat = KTX_TTF_BC4_R; break;
			case (TextureCompression::BC5): tcFormat = KTX_TTF_BC5_RG; break;
			case (TextureCompression::BC7): tcFormat = KTX_TTF_BC7_RGBA; break;
			default: SHARD3D_ERROR("Invalid/unsupported texture compression chosen! Falling back to BC7 compression..."); tcFormat = KTX_TTF_BC7_RGBA; break;
			}
			ktxResult tbErr = ktxTexture2_TranscodeBasis(ktxTex, tcFormat, 0);
			if (tbErr != KTX_SUCCESS) SHARD3D_ERROR("Failed to transcode block compressed texture!");
		}
		
		return ktxTex;
	}

	//ktxTexture2* TextureImporter::loadCachedTextureKTX(const char* cacheloc) {
	//	ktxTexture2* ktxTex = nullptr;
	//
	//	ktxResult cfnfErr = ktxTexture2_CreateFromNamedFile(cacheloc, KTX_TEXTURE_CREATE_LOAD_IMAGE_DATA_BIT, &ktxTex);
	//	return ktxTex;
	//}

	ktxTexture2* TextureImporter::loadCompressedCachedTextureKTX(const char* cacheloc) {
		ktxTexture2* ktxTex = nullptr;

		auto data = IOUtils::readBinary(cacheloc);

		ZSTD_DCtx* decompressionContext = ZSTD_createDCtx();
		size_t deflatedSize = ZSTD_getFrameContentSize(data.data(), data.size());
		uint8_t* deflatedBytes = new uint8_t[deflatedSize];
		size_t readSize = ZSTD_decompressDCtx(decompressionContext, deflatedBytes, deflatedSize, data.data(), data.size());

		ktxResult cfnfErr = ktxTexture2_CreateFromMemory(deflatedBytes, readSize, KTX_TEXTURE_CREATE_LOAD_IMAGE_DATA_BIT, &ktxTex);

		ZSTD_freeDCtx(decompressionContext);
		delete[] deflatedBytes;
		return ktxTex;
	}

	//bool TextureImporter::exportTextureKTX(ktxTexture2* textureData, const char* location) {
	//	ktxResult expErr = ktxTexture_WriteToNamedFile(ktxTexture(textureData), location);
	//	if (expErr == KTX_SUCCESS) {
	//		return true;
	//	} else {
	//		SHARD3D_ERROR("Failed to export KTX2 texture! KTX Error code: {0}", static_cast<uint32_t>(expErr));
	//		return false;
	//	}
	//}

	bool TextureImporter::exportCompressedTextureKTX(ktxTexture2* textureData, const char* location, int compressionLevel) {
		uint8_t* bytes;
		size_t size;
		ktxResult expErr = ktxTexture_WriteToMemory(ktxTexture(textureData), &bytes, &size);
		if (expErr != KTX_SUCCESS) {
			SHARD3D_ERROR("Failed to export KTX2 texture! KTX Error code: {0}", static_cast<uint32_t>(expErr));
			return false;
		}

		ZSTD_CCtx* compressionContext = ZSTD_createCCtx();
		size_t dstSize = ZSTD_compressBound(size);
		uint8_t* dstBytes = new uint8_t[dstSize];
		size_t writtenSize = ZSTD_compressCCtx(compressionContext, dstBytes, dstSize, bytes, size, compressionLevel);

		std::ofstream out(location, std::ios::binary | std::ios::out);
		out.write(reinterpret_cast<const char*>(dstBytes), writtenSize);
		out.flush();
		out.close();
		delete[] dstBytes;
		ZSTD_freeCCtx(compressionContext);
		return true;
	}

	void TextureImporter::freeKTX(ktxTexture2* texture) {
		ktxTexture_Destroy(ktxTexture(texture));
	}

	void TextureImporter::importTextureFromFile(TextureImportInfo importInfo, ResourceSystem* resourceSystem, const std::vector<std::string>& textureSources, AssetID ktxDest, int compressionLevel) {
		ktxTexture2* ktxTex = nullptr;

		if (!importInfo.isCubemap) {
			int texWidth, texHeight, texChannels;
			uint8_t* pixels = ImageResource::getSTBImage(textureSources[0].c_str(), &texWidth, &texHeight, &texChannels, 4);
			size_t imageSize = texWidth * texHeight * 4;

			if (!pixels) {
				SHARD3D_FATAL(std::string("failed to load texture image! Tried to load '" + textureSources[0] + "'"));
			}

			if (importInfo.flipNormalMapY) {
				for (uint32_t i = 0; i < imageSize; i += 4) {
					uint8_t unorm = pixels[i];
					float value = static_cast<float>(unorm) / 255.0f;
					float result = -(value * 2.0 - 1.0) * 0.5 + 0.5;
					pixels[i] = static_cast<uint8_t>(result * 255.0f);
				}
			}
			
			ktxTextureCreateInfo txCreateInfo{};
			memset(&txCreateInfo, 0, sizeof(ktxTextureCreateInfo));
			txCreateInfo.baseWidth = texWidth;
			txCreateInfo.baseHeight = texHeight;
			txCreateInfo.baseDepth = 1;
			txCreateInfo.generateMipmaps = KTX_FALSE;
			txCreateInfo.vkFormat = VK_FORMAT_R8G8B8A8_UNORM;
			txCreateInfo.numDimensions = 1;
			txCreateInfo.isArray = KTX_FALSE;
			txCreateInfo.numLevels = 1;
			txCreateInfo.numLayers = 1;
			txCreateInfo.numFaces = 1;
			txCreateInfo.numDimensions = 2;
			ktxResult cErr = ktxTexture2_Create(&txCreateInfo, KTX_TEXTURE_CREATE_ALLOC_STORAGE, &ktxTex);
			ktxResult sifmErr = ktxTexture_SetImageFromMemory(ktxTexture(ktxTex), 0, 0, 0, reinterpret_cast<ktx_uint8_t*>(pixels), imageSize);
		} else {
			if (importInfo.cubeFormat == TextureCubeFormat::Cube) {
				int texWidth, texHeight, texChannels;
				uint8_t* pixels = ImageResource::getSTBImage(textureSources[0].c_str(), &texWidth, &texHeight, &texChannels, 4);
				size_t imageSize = texWidth * texHeight * 4;

				if (!pixels) {
					SHARD3D_FATAL(std::string("failed to load texture image! Tried to load '" + textureSources[0] + "'"));
				}

				ktxTextureCreateInfo txCreateInfo{};
				memset(&txCreateInfo, 0, sizeof(ktxTextureCreateInfo));
				txCreateInfo.baseWidth = texWidth;
				txCreateInfo.baseHeight = texHeight;
				txCreateInfo.baseDepth = 1;
				txCreateInfo.generateMipmaps = KTX_FALSE; // we will disable mip maps for cubemaps as they dont make much of a visual benefit
				txCreateInfo.vkFormat = VK_FORMAT_R8G8B8A8_UNORM;
				txCreateInfo.numDimensions = 1;
				txCreateInfo.isArray = KTX_FALSE;
				txCreateInfo.numLevels = 1;
				txCreateInfo.numLayers = 1;
				txCreateInfo.numFaces = 6;
				txCreateInfo.numDimensions = 2;

				ktxResult cErr = ktxTexture2_Create(&txCreateInfo, KTX_TEXTURE_CREATE_ALLOC_STORAGE, &ktxTex);
				if (cErr != KTX_SUCCESS) SHARD3D_ERROR("Failed to allocate ktx texture!");

				if (importInfo.generateCubemapGlossinessMap) {


					//static glm::vec3 orientations2[6]{
					//	{ 0.f, glm::half_pi<float>(), 0.f },	// right
					//	{ 0.f, -glm::half_pi<float>(), 0.f },	// left
					//	{ glm::half_pi<float>(), 0.f, 0.f },	// down
					//	{ -glm::half_pi<float>(), 0.f, 0.f },	// up
					//	{ 0.f, 0.f, 0.f },	// front
					//	{ 0.f, glm::pi<float>(), 0.f }		// back
					//};
					//
					//VkImage cubemapImage;
					//VkImageView cubemapImageView;
					//VkSampler cubemapSampler;
					//VkDeviceMemory cubemapImageMemory;
					//VkImageCreateInfo cubemapImageInfo{};
					//cubemapImageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
					//cubemapImageInfo.imageType = VK_IMAGE_TYPE_2D;
					//cubemapImageInfo.extent = { txCreateInfo.baseWidth, txCreateInfo.baseHeight, 1 };
					//cubemapImageInfo.mipLevels = 1;
					//cubemapImageInfo.arrayLayers = 6;
					//cubemapImageInfo.format = VK_FORMAT_E5B9G9R9_UFLOAT_PACK32;
					//cubemapImageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
					//cubemapImageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
					//cubemapImageInfo.usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT |
					//	VK_IMAGE_USAGE_SAMPLED_BIT;
					//cubemapImageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
					//cubemapImageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
					//cubemapImageInfo.flags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
					//
					//device.createImageWithInfo(
					//	cubemapImageInfo,
					//	VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
					//	cubemapImage,
					//	cubemapImageMemory
					//);
					//
					//VkImageViewCreateInfo cubemapImageViewInfo{};
					//cubemapImageViewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
					//cubemapImageViewInfo.image = cubemapImage;
					//cubemapImageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_CUBE;
					//cubemapImageViewInfo.format = VK_FORMAT_E5B9G9R9_UFLOAT_PACK32;
					//cubemapImageViewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
					//cubemapImageViewInfo.subresourceRange.baseMipLevel = 0;
					//cubemapImageViewInfo.subresourceRange.levelCount = 1;
					//cubemapImageViewInfo.subresourceRange.baseArrayLayer = 0;
					//cubemapImageViewInfo.subresourceRange.layerCount = 6;
					//VK_ASSERT(vkCreateImageView(device.device(), &cubemapImageViewInfo, nullptr, &cubemapImageView), "failed to create texture image view!");
					//
					//VkSamplerCreateInfo cubemapSamplerInfo{};
					//cubemapSamplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
					//cubemapSamplerInfo.magFilter = VK_FILTER_LINEAR;
					//cubemapSamplerInfo.minFilter = VK_FILTER_LINEAR;
					//cubemapSamplerInfo.anisotropyEnable = VK_FALSE;
					//cubemapSamplerInfo.maxAnisotropy = 1.0f;
					//cubemapSamplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
					//cubemapSamplerInfo.unnormalizedCoordinates = VK_FALSE;
					//cubemapSamplerInfo.compareEnable = VK_FALSE;
					//cubemapSamplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
					//cubemapSamplerInfo.mipLodBias = 0.0f;
					//cubemapSamplerInfo.minLod = 0.0f;
					//cubemapSamplerInfo.maxLod = 0.0f;
					//
					//VK_ASSERT(vkCreateSampler(device.device(), &cubemapSamplerInfo, nullptr, &cubemapSampler), "failed to create texture sampler!");
					//
					//
					//VkCommandBuffer commandBuffer = device.beginSingleTimeCommands();
					//device.transitionImageLayout(
					//	commandBuffer,
					//	cubemapImage,
					//	VK_FORMAT_E5B9G9R9_UFLOAT_PACK32,
					//	VK_IMAGE_LAYOUT_UNDEFINED,
					//	VK_IMAGE_LAYOUT_GENERAL,
					//	0,
					//	1,
					//	0,
					//	6
					//);
					//for (int f = 0; f < 6; f++) {
					//	VkImageSubresourceLayers subsrc;
					//	subsrc.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
					//	subsrc.baseArrayLayer = 0;
					//	subsrc.layerCount = 1;
					//	subsrc.mipLevel = 0;
					//	VkImageSubresourceLayers subdst;
					//	subdst.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
					//	subdst.baseArrayLayer = f;
					//	subdst.layerCount = 1;
					//	subdst.mipLevel = 0;
					//	device.copyImage(commandBuffer, { txCreateInfo.baseWidth, txCreateInfo.baseHeight, 1 },
					//		 processors[f]->getWrittenAttachment()->getImage(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, subsrc, {},
					//		cubemapImage, VK_IMAGE_LAYOUT_GENERAL, subdst, {}
					//	);
					//}
					//device.endSingleTimeCommands(commandBuffer);
					//
					//S3DShaderSpecialization specialization;
					//specialization = {};
					//const int sampleCount = 1024; // extra large sample count, we can afford extra precision for baking
					//specialization.addConstant(0, sampleCount);
					//
					//VkDescriptorImageInfo inCubeDescriptorInfo{};
					//inCubeDescriptorInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
					//inCubeDescriptorInfo.imageView = cubemapImageView;
					//inCubeDescriptorInfo.sampler = cubemapSampler;
					//
					//PostProcessingEffect* envSpecularGenPrePass = new PostProcessingEffect(
					//	device,
					//	{ txCreateInfo.baseWidth, txCreateInfo.baseHeight },
					//	S3DShader(ShaderType::Fragment, "resources/shaders/processing/envspecgen.frag", "misc", specialization),
					//	{ inCubeDescriptorInfo },
					//	VK_FORMAT_R16G16B16A16_SFLOAT,
					//	VK_IMAGE_VIEW_TYPE_CUBE,
					//	6,
					//	GLOSSINESS_MIP_LEVELS
					//);
					//PostProcessingEffect* envSpecularGenFinalPass = new PostProcessingEffect(
					//	device,
					//	{ txCreateInfo.baseWidth, txCreateInfo.baseHeight },
					//	S3DShader(ShaderType::Fragment, "resources/shaders/processing/envspecgen_post.frag", "misc", specialization),
					//	{ envSpecularGenPrePass->getAttachment()->getDescriptor() },
					//	VK_FORMAT_R32_UINT,
					//	VK_IMAGE_VIEW_TYPE_CUBE,
					//	6,
					//	GLOSSINESS_MIP_LEVELS
					//);
					//commandBuffer = device.beginSingleTimeCommands();
					//// pre pass
					//for (int i = 0; i < 6; i++) {
					//	struct Push {
					//		glm::mat4 mvp;
					//		float glossiness;
					//	} push;
					//	S3DCamera camera{};
					//	camera.setViewYXZ({}, orientations2[i]);
					//	push.mvp = camera.getView();
					//	for (int j = 0; j < GLOSSINESS_MIP_LEVELS; j++) {
					//		push.glossiness = 1.0f - static_cast<float>(j) / static_cast<float>(GLOSSINESS_MIP_LEVELS - 1);
					//		envSpecularGenPrePass->render(commandBuffer, push, i, j);
					//	}
					//}
					//// high fidelity pass
					//for (int i = 0; i < 6; i++) {
					//	struct Push {
					//		glm::mat4 mvp;
					//		float glossiness;
					//	} push;
					//	S3DCamera camera{};
					//	camera.setViewYXZ({}, orientations2[i]);
					//	push.mvp = camera.getView();
					//	for (int j = 0; j < GLOSSINESS_MIP_LEVELS; j++) {
					//		push.glossiness = 1.0f - static_cast<float>(j) / static_cast<float>(GLOSSINESS_MIP_LEVELS - 1);
					//		envSpecularGenFinalPass->render(commandBuffer, push, i, j);
					//	}
					//}
					//device.transitionImageLayout(commandBuffer, envSpecularGenFinalPass->getAttachment()->getImage(), VK_FORMAT_R32_UINT,
					//	VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
					//	VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
					//	0,
					//	GLOSSINESS_MIP_LEVELS,
					//	0,
					//	6
					//);
					//device.endSingleTimeCommands(commandBuffer);
					//
					//for (int l = 0; l < GLOSSINESS_MIP_LEVELS; l++) {
					//	VkBuffer copyBuffer;
					//	VkDeviceMemory copyBufferMemory;
					//
					//	glm::ivec2 mipResolution = glm::ivec2(std::pow(2.0f, std::log2(txCreateInfo.baseWidth) - l));
					//	size_t imageSize = mipResolution.x * mipResolution.y * 4.0f * 6.0f;
					//	device.createBuffer(
					//		imageSize,
					//		VK_BUFFER_USAGE_TRANSFER_DST_BIT,
					//		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
					//		copyBuffer,
					//		copyBufferMemory
					//	);
					//	void* pixels{};
					//	vkMapMemory(device.device(), copyBufferMemory, 0, imageSize, 0, &pixels);
					//	VkCommandBuffer commandBuffer = device.beginSingleTimeCommands();
					//
					//
					//	device.copyImageToBuffer(commandBuffer, envSpecularGenFinalPass->getAttachment()->getImage(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
					//		copyBuffer, mipResolution.x, mipResolution.y, 6, l);
					//	device.endSingleTimeCommands(commandBuffer);
					//
					//	for (int f = 0; f < 6; f++) {
					//		ktxResult sifmErr = ktxTexture_SetImageFromMemory(ktxTexture(ktxTex), l, 0, f, reinterpret_cast<ktx_uint8_t*>(pixels) + (imageSize / 6) * f, imageSize / 6);
					//		if (sifmErr != KTX_SUCCESS) SHARD3D_ERROR("Failed to save texture! {0}", sifmErr);
					//	}
					//
					//	vkUnmapMemory(device.device(), copyBufferMemory);
					//	vkDestroyBuffer(device.device(), copyBuffer, nullptr);
					//	vkFreeMemory(device.device(), copyBufferMemory, nullptr);
					//}

				}


				ktxResult sifmErr = ktxTexture_SetImageFromMemory(ktxTexture(ktxTex), 0, 0, 0, reinterpret_cast<ktx_uint8_t*>(pixels), imageSize); // first face
				if (sifmErr != KTX_SUCCESS) SHARD3D_ERROR("Failed to load texture!");

				for (int i = 1; i < 6; i++) { // do the rest of the 5 faces
					uint8_t* fpixels = ImageResource::getSTBImage(textureSources[i].c_str(), &texWidth, &texHeight, &texChannels, 4);
					sifmErr = ktxTexture_SetImageFromMemory(ktxTexture(ktxTex), 0, 0, i, reinterpret_cast<ktx_uint8_t*>(fpixels), imageSize); // first face
					if (sifmErr != KTX_SUCCESS) SHARD3D_ERROR("Failed to load texture!");
				}
			} else {
				SHARD3D_NOIMPL_C;
			}
		}
		exportCompressedTextureKTX(ktxTex, ktxDest.getAbsolute().c_str(), compressionLevel);
	}

	void TextureImporter::importTextureFromMemory(TextureImportInfo importInfo, ResourceSystem* resourceSystem, const void* textureMemory, VkFormat format, int width, int height, size_t textureSizeBytes, AssetID ktxDest, int compressionLevel) {
		ktxTexture2* ktxTex = nullptr;

		int texWidth = width, texHeight = height;
		uint8_t* pixels = (uint8_t*)textureMemory;
		if (!importInfo.isCubemap) {
			size_t imageSize = texWidth * texHeight * 4;

			if (importInfo.flipNormalMapY) {
				for (uint32_t i = 0; i < imageSize; i += 4) {
					uint8_t unorm = pixels[i];
					float value = static_cast<float>(unorm) / 255.0f;
					float result = -(value * 2.0 - 1.0) * 0.5 + 0.5;
					pixels[i] = static_cast<uint8_t>(result * 255.0f);
				}
			}

			ktxTextureCreateInfo txCreateInfo{};
			memset(&txCreateInfo, 0, sizeof(ktxTextureCreateInfo));
			txCreateInfo.baseWidth = texWidth;
			txCreateInfo.baseHeight = texHeight;
			txCreateInfo.baseDepth = 1;
			txCreateInfo.generateMipmaps = KTX_FALSE;
			txCreateInfo.vkFormat = format;
			txCreateInfo.numDimensions = 1;
			txCreateInfo.isArray = KTX_FALSE;
			txCreateInfo.numLevels = 1;
			txCreateInfo.numLayers = 1;
			txCreateInfo.numFaces = 1;
			txCreateInfo.numDimensions = 2;
			ktxResult cErr = ktxTexture2_Create(&txCreateInfo, KTX_TEXTURE_CREATE_ALLOC_STORAGE, &ktxTex);
			ktxResult sifmErr = ktxTexture_SetImageFromMemory(ktxTexture(ktxTex), 0, 0, 0, reinterpret_cast<ktx_uint8_t*>(pixels), imageSize);
		} else {
			size_t imageSize = textureSizeBytes;
			ktxTextureCreateInfo txCreateInfo{};
			memset(&txCreateInfo, 0, sizeof(ktxTextureCreateInfo));
			txCreateInfo.baseWidth = texWidth;
			txCreateInfo.baseHeight = texHeight;
			txCreateInfo.baseDepth = 1;
			txCreateInfo.generateMipmaps = KTX_FALSE;
			txCreateInfo.vkFormat = format;
			txCreateInfo.numDimensions = 1;
			txCreateInfo.isArray = KTX_FALSE;
			txCreateInfo.numLevels = 1;
			txCreateInfo.numLayers = 1;
			txCreateInfo.numFaces = 6;
			txCreateInfo.numDimensions = 2;
			ktxResult cErr = ktxTexture2_Create(&txCreateInfo, KTX_TEXTURE_CREATE_ALLOC_STORAGE, &ktxTex);
			for (int i = 0; i < 6; i++) {
				ktxResult sifmErr = ktxTexture_SetImageFromMemory(ktxTexture(ktxTex), 0, 0, i,
					reinterpret_cast<ktx_uint8_t*>(pixels) + (imageSize / 6) * i, imageSize / 6);
			}
		}
		exportCompressedTextureKTX(ktxTex, ktxDest.getAbsolute().c_str(), compressionLevel);
	}

	Texture2D* TextureImporter::createTexture2DFromFile(S3DDevice& device, uPtr<SmartDescriptorSet>& bindless, TextureLoadInfo loadInfo, ResourceSystem* resourceSystem, const std::string& textureSource) {
		int c{};
		int w{};
		int h{};
		uint8_t* pixels = ImageResource::getSTBImage(textureSource.c_str(), &w, &h, &c, 4);
		ktxTextureCreateInfo txCreateInfo{};
		memset(&txCreateInfo, 0, sizeof(ktxTextureCreateInfo));
		txCreateInfo.baseWidth = w;
		txCreateInfo.baseHeight = h;
		txCreateInfo.baseDepth = 1;
		txCreateInfo.generateMipmaps = KTX_FALSE; // generate mip maps even if not used
		txCreateInfo.vkFormat = VK_FORMAT_R8G8B8A8_SRGB;
		txCreateInfo.numDimensions = 1;
		txCreateInfo.isArray = KTX_FALSE;
		txCreateInfo.numLevels = 1;
		txCreateInfo.numLayers = 1;
		txCreateInfo.numFaces = 1;
		txCreateInfo.numDimensions = 2;
		ktxTexture2* ktxTex = nullptr;
		ktxResult cErr = ktxTexture2_Create(&txCreateInfo, KTX_TEXTURE_CREATE_ALLOC_STORAGE, &ktxTex);
		ktxResult sifmErr = ktxTexture_SetImageFromMemory(ktxTexture(ktxTex), 0, 0, 0, reinterpret_cast<ktx_uint8_t*>(pixels), w*h*4);
		ImageResource::freeSTBImage(pixels);
		Texture2D* tex =
			new Texture2D(device, bindless, ktxTex,
				loadInfo);
		ktxTexture_Destroy(ktxTexture(ktxTex));
		return tex;
	}

}