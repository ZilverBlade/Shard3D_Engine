#pragma once
#include "../../vulkan_abstr.h"
#include "primitives.h"

namespace Shard3D {
	inline namespace Resources {
		struct SkeletonBoneRelationship {
			uint8_t parent;
			std::vector<uint8_t> children;
		};
		class Skeleton : public Resource {
		public:
			Skeleton(
				S3DDevice& device,
				ResourceSystem* resourceSystem,
				SkeletonRawData* data
			);
			~Skeleton();

			std::vector<glm::mat4> offsetMatrices;

			std::vector<SkeletonBoneRelationship> relationships;

			std::vector<std::string> morphTargetNames;
			std::vector<std::string> boneNames;

			ShaderResourceIndex morphTargetBufferID{};
			ShaderResourceIndex vertexIDBufferID{};
			ShaderResourceIndex vertexWeightBufferID{};

		private:
			S3DDevice& device;
			ResourceSystem* resourceSystem;

			uPtr<S3DBuffer> morphTargetVectorBuffer;
			uPtr<S3DBuffer> vertexIDBuffer;
			uPtr<S3DBuffer> vertexWeightBuffer;

			uint32_t vertexCount;
		};
	}
}
