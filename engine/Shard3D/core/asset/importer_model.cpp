#include "../../s3dpch.h" 
#include <glm/gtx/hash.hpp>

#include "importer_model.h"

#include "../../core.h"
#include "../../ecs.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>           
#include <assimp/postprocess.h>    
#include <assimp/material.h>
#include <fstream>

#include "../misc/engine_settings.h"
#include "../../utils/binary.h"
#include "../../../resources/shaders/include/compression.glsl"
#include "image_tools.h"
#include "../../systems/rendering/simple_processing_system.h"
#include "../rendering/framebuffer.h"
#include "../../systems/handlers/resource_system.h"
#include "assetmgr.h"



namespace Shard3D::Resources {
	struct StrideData {
		uint32_t stride;
		uint32_t size;
	};

	static void replaceIllegalChars(std::string& s) {
		std::string illegalChars = "\\/:?\"<>|";
		for (auto it = s.begin(); it < s.end(); it++) {
			bool found = illegalChars.find(*it) != std::string::npos;
			if (found) {
				*it = '_';
			}
		}
	}


	/*
	*.s3dmesh file format
		>> Mesh
			-- .S3DMESH. (validation text, 10 bytes)
			-- Engine Version (20 byte)

			-- Submesh Count (4 byte)
			-- Submesh Strides & Sizes (4 byte stride length, 4 byte submesh size) (submesh count * 8 bytes)
			-- Material Name Strides & Sizes (4 byte stride length, 4 byte material name size) (submesh count * 8 bytes)

			-- VolumeBounds (24 bytes)

			-- Submesh0, Submesh1, Submesh2, Submesh3, [...], SubmeshN
			-- MaterialName0, MaterialName1, MaterialName2, MaterialName3, [...], MaterialNameN (array of 1 byte char array)
	============================================================================================================================
				>> Submesh
					-- MaterialIndex (4 byte)
					-- Vertex Count (4 byte)
					-- Index Count (4 byte)
					-- UV count (4 byte)
					-- VolumeBounds (24 bytes) (NEW SINCE v0.13.0-alpha)
					-- SphereBounds (4 bytes) (NEW SINCE v0.13.1-alpha)
					-- Index0, Index1, Index2, Index3, Index4, [...], IndexN (4 byte array)
					-- Position0, Position1, [...], PositionN (12 byte array)
					-- Normal0, Normal1, [...], NormalN (4 byte array)
					-- Tangent0, Tangent1, [...], TangentN (4 byte array)
					(AOA) -- UV0, UV1, [...], UVN

	*/
	MeshRawData* ModelImporter::loadMeshData(AssetID modelLocation) {
		MeshRawData* data = new MeshRawData();
		if (!IOUtils::doesFileExist(modelLocation.getAbsolute())) {
			SHARD3D_ERROR("Failed to load mesh data! File does not exist!");
			return nullptr;
		}
		std::vector<char> rdw = IOUtils::readBinary(modelLocation.getAbsolute());
		void* raw = reinterpret_cast<void*>(rdw.data());
		size_t baseAddress = (size_t)raw;
		const char* txt = ".S3DMESH.";
		if (memcmp(raw, txt, 10) != 0) {
			SHARD3D_ERROR("Failed to load mesh data! Incorrect file format! Requires .s3dmesh file!");
			return nullptr;
		}
		auto newTime = std::chrono::high_resolution_clock::now();
		Version version = *reinterpret_cast<Version*>(baseAddress + 10);
		data->submeshCount = *reinterpret_cast<uint32_t*>(baseAddress + 10 + 20);
		SubmeshRawData** submeshes = reinterpret_cast<SubmeshRawData**>(calloc(data->submeshCount, sizeof(SubmeshRawData*)));
		char** matnames = reinterpret_cast<char**>(calloc(data->submeshCount, sizeof(char*)));

		data->submeshes = submeshes;
		data->materialNames = matnames;
		data->entireBounds = *reinterpret_cast<VolumeBounds*>(baseAddress + 10 + 20 + 4 + data->submeshCount * sizeof(StrideData) * 2);

		if (version < Version(0, 13, 1)) {
			data->entireSphereBounds = 9999.0f;
		}else {
			data->entireSphereBounds = *reinterpret_cast<SphereBounds*>(baseAddress + 10 + 20 + 4 + data->submeshCount * sizeof(StrideData) * 2 + 24);
		}

		size_t submeshStartVertexSeek;
		if (version < Version(0, 13, 0)) {
			submeshStartVertexSeek = 16;
		} else if (version == Version(0, 13, 0)) { // new format, include bounds per submesh
			submeshStartVertexSeek = 40;
		} else { // new format, include sphere bounds
			submeshStartVertexSeek = 44;
		}

		for (int i = 0; i < data->submeshCount; i++) {
			submeshes[i] = new SubmeshRawData();
			StrideData& submeshStrideInfo = *reinterpret_cast<StrideData*>(baseAddress + 10 + 20 + 4 + i * sizeof(StrideData));
			StrideData& matnameStrideInfo = *reinterpret_cast<StrideData*>(baseAddress + 10 + 20 + 4 + data->submeshCount * sizeof(StrideData) + i * sizeof(StrideData));
			submeshes[i]->materialIndex = *reinterpret_cast<uint32_t*>(baseAddress + submeshStrideInfo.stride);
			submeshes[i]->vtxCount = *reinterpret_cast<uint32_t*>(baseAddress + submeshStrideInfo.stride + 4);
			submeshes[i]->indCount = *reinterpret_cast<uint32_t*>(baseAddress + submeshStrideInfo.stride + 8);
			submeshes[i]->uvCount = *reinterpret_cast<uint32_t*>(baseAddress + submeshStrideInfo.stride + 12);

			if (version >= Version(0, 13, 0)) {
				submeshes[i]->bounds = *reinterpret_cast<VolumeBounds*>(baseAddress + submeshStrideInfo.stride + 16);
				if (version >= Version(0, 13, 1)) {
					submeshes[i]->sphereBounds = *reinterpret_cast<SphereBounds*>(baseAddress + submeshStrideInfo.stride + 16 + sizeof(VolumeBounds));
				} else {
					submeshes[i]->sphereBounds = 9999.0f;
				}
			}

			matnames[i] = reinterpret_cast<char*>(calloc(matnameStrideInfo.size + 1, sizeof(char)));
			strncpy(matnames[i], reinterpret_cast<char*>(baseAddress + matnameStrideInfo.stride), matnameStrideInfo.size);

			uint32_t indSize = submeshes[i]->indCount * sizeof(uint32_t);
			uint32_t posSize = submeshes[i]->vtxCount * sizeof(VTX_pos_offset_12b_);
			uint32_t nrmSize = submeshes[i]->vtxCount * sizeof(VTX_norm_cmpr_offset_4b_);
			uint32_t tgSize = submeshes[i]->vtxCount * sizeof(VTX_tg_cmpr_offset_4b_);
			uint32_t uvSize = submeshes[i]->vtxCount * sizeof(VTX_uv_cmpr_offset_4b_);
			submeshes[i]->vtxIndices = reinterpret_cast<uint32_t*>(malloc(indSize));
			submeshes[i]->vtxPositions = reinterpret_cast<VTX_pos_offset_12b_*>(malloc(posSize));
			submeshes[i]->vtxNormals = reinterpret_cast<VTX_norm_cmpr_offset_4b_*>(malloc(nrmSize));
			submeshes[i]->vtxTangents = reinterpret_cast<VTX_tg_cmpr_offset_4b_*>(malloc(tgSize));
			submeshes[i]->vtxUVs = reinterpret_cast<VTX_uv_cmpr_offset_4b_**>(malloc(submeshes[i]->uvCount * sizeof(VTX_uv_cmpr_offset_4b_*)));
			
			memcpy(submeshes[i]->vtxIndices, (void*)(baseAddress + submeshStrideInfo.stride + submeshStartVertexSeek), indSize);
			memcpy(submeshes[i]->vtxPositions, (void*)(baseAddress + submeshStrideInfo.stride + submeshStartVertexSeek + indSize), posSize);
			memcpy(submeshes[i]->vtxNormals, (void*)(baseAddress + submeshStrideInfo.stride + submeshStartVertexSeek + indSize + posSize), nrmSize);
			memcpy(submeshes[i]->vtxTangents, (void*)(baseAddress + submeshStrideInfo.stride + submeshStartVertexSeek + indSize + posSize + nrmSize), tgSize);
			for (uint32_t j = 0; j < submeshes[i]->uvCount; j++) {
				submeshes[i]->vtxUVs[j] = reinterpret_cast<VTX_uv_cmpr_offset_4b_*>(malloc(uvSize));
				memcpy(submeshes[i]->vtxUVs[j], (void*)(baseAddress + submeshStrideInfo.stride + submeshStartVertexSeek + indSize + posSize + nrmSize + tgSize + uvSize * j), uvSize);
			}
		}
		SHARD3D_LOG("Duration of loading s3dmesh {0}: {1} ms", modelLocation.getAsset(),
			std::chrono::duration<float, std::chrono::milliseconds::period>
			(std::chrono::high_resolution_clock::now() - newTime).count());

		return data;
	}

	void ModelImporter::freeMeshData(MeshRawData* meshData) {
		// clean up memory
		for (int i = 0; i < meshData->submeshCount; i++) {
			SubmeshRawData* submesh = meshData->submeshes[i];
			std::free(submesh->vtxIndices);
			std::free(submesh->vtxPositions);
			std::free(submesh->vtxNormals);
			std::free(submesh->vtxTangents);
			for (int j = 0; j < submesh->uvCount; j++) {
				std::free(submesh->vtxUVs[j]);
			}
			std::free(submesh->vtxUVs);
			delete submesh;
			std::free(meshData->materialNames[i]);
		}
		std::free(meshData->submeshes);
		std::free(meshData->materialNames);
		delete meshData;
	}

	static ModelInfo loadIndividualMesh(const ModelImporter::Builder::MeshLoadData& load, AssetID modelDest) {
		ModelInfo modelInfo{};
		std::vector<std::pair<uint32_t, std::string>> materials;
		for (const auto& [index, smd] : load.submeshes) {
			modelInfo.defaultMaterials.push_back(load.submeshes.at(index).materialAsset);
			std::string slot;
			for (const auto& [s, i] : load.slotToIndex) {
				if (index == i) {
					slot = s;
					break;
				}
			}
			modelInfo.materialSlots.push_back(slot);
			materials.push_back({ index, slot });
		}
		modelInfo.mainBounds = load.volumeBounds;
		modelInfo.mainSphereBounds = load.sphereBounds;
		size_t submeshCount = load.slotToIndex.size();
		SubmeshRawData** meshesOut = reinterpret_cast<SubmeshRawData**>(calloc(submeshCount, sizeof(SubmeshRawData*)));
		char** materialNames = reinterpret_cast<char**>(calloc(submeshCount, sizeof(char*)));
		for (int i = 0; i < materials.size(); i++) { // i = material index
			auto& submesh = load.submeshes.at(materials[i].first);
			meshesOut[i] = new SubmeshRawData();

			meshesOut[i]->bounds = submesh.volumeBounds;
			meshesOut[i]->sphereBounds = submesh.sphereBounds;
			meshesOut[i]->materialIndex = i;
			materialNames[i] = reinterpret_cast<char*>(malloc(materials[i].second.length()));
			strncpy(materialNames[i], materials[i].second.data(), materials[i].second.length());

			meshesOut[i]->uvCount = 1; // 1 uv map
			meshesOut[i]->vtxCount = submesh.vertices.size();
			meshesOut[i]->vtxPositions = reinterpret_cast<VTX_pos_offset_12b_*>(malloc(meshesOut[i]->vtxCount * sizeof(VTX_pos_offset_12b_)));
			meshesOut[i]->vtxNormals = reinterpret_cast<VTX_norm_cmpr_offset_4b_*>(malloc(meshesOut[i]->vtxCount * sizeof(VTX_norm_cmpr_offset_4b_)));
			meshesOut[i]->vtxTangents = reinterpret_cast<VTX_tg_cmpr_offset_4b_*>(malloc(meshesOut[i]->vtxCount * sizeof(VTX_tg_cmpr_offset_4b_)));
			meshesOut[i]->vtxUVs = reinterpret_cast<VTX_uv_cmpr_offset_4b_**>(malloc(meshesOut[i]->uvCount * sizeof(VTX_uv_cmpr_offset_4b_*)));
			meshesOut[i]->vtxUVs[0] = reinterpret_cast<VTX_uv_cmpr_offset_4b_*>(malloc(meshesOut[i]->vtxCount * sizeof(VTX_uv_cmpr_offset_4b_)));
			meshesOut[i]->indCount = submesh.indices.size();
			meshesOut[i]->vtxIndices = reinterpret_cast<uint32_t*>(malloc(meshesOut[i]->indCount * sizeof(uint32_t)));

			for (int j = 0; j < meshesOut[i]->vtxCount; j++) {
				const Vertex& vertex = submesh.vertices[j];
				meshesOut[i]->vtxPositions[j] = VTX_pos_offset_12b_{ vertex.position };
				meshesOut[i]->vtxUVs[0][j] = VTX_uv_cmpr_offset_4b_{
					half::ToFloat16Fast(vertex.uv.x), half::ToFloat16Fast(vertex.uv.y)
				};
				meshesOut[i]->vtxNormals[j] = VTX_norm_cmpr_offset_4b_{ cram_bytes_x11y11z10(
					pack_float11b_signed(vertex.normal.x), pack_float11b_signed(vertex.normal.y), pack_float10b_signed(vertex.normal.z)
				) };
				meshesOut[i]->vtxTangents[j] = VTX_tg_cmpr_offset_4b_{ cram_bytes_x11y11z10(
					pack_float11b_signed(vertex.tangent.x), pack_float11b_signed(vertex.tangent.y), pack_float10b_signed(vertex.tangent.z)
				) };
			}
			for (int j = 0; j < meshesOut[i]->indCount; j++) {
				meshesOut[i]->vtxIndices[j] = submesh.indices[j];
			}
		}

		MeshRawData* modelOut = new MeshRawData();
		modelOut->submeshCount = static_cast<uint32_t>(submeshCount);
		modelOut->submeshes = meshesOut;
		modelOut->materialNames = materialNames;
		modelOut->entireBounds = load.volumeBounds;
		modelOut->entireSphereBounds = load.sphereBounds;

		std::vector<BinaryStream> submeshBwr{};
		submeshBwr.resize(modelOut->submeshCount);
		for (int i = 0; i < modelOut->submeshCount; i++) {
			SubmeshRawData* data = modelOut->submeshes[i];
			submeshBwr[i] << data->materialIndex;
			submeshBwr[i] << data->vtxCount;
			submeshBwr[i] << data->indCount;
			submeshBwr[i] << data->uvCount;
			submeshBwr[i] << data->bounds;
			submeshBwr[i] << data->sphereBounds;

			submeshBwr[i].array_back(data->vtxIndices, sizeof(uint32_t), data->indCount);
			submeshBwr[i].array_back(data->vtxPositions, sizeof(VTX_pos_offset_12b_), data->vtxCount);
			submeshBwr[i].array_back(data->vtxNormals, sizeof(VTX_norm_cmpr_offset_4b_), data->vtxCount);
			submeshBwr[i].array_back(data->vtxTangents, sizeof(VTX_tg_cmpr_offset_4b_), data->vtxCount);
			for (int j = 0; j < data->uvCount; j++) {
				submeshBwr[i].array_back(data->vtxUVs[j], sizeof(VTX_uv_cmpr_offset_4b_), data->vtxCount);
			}
		}
		std::vector<BinaryStream> materialBwr{};
		materialBwr.resize(modelOut->submeshCount);
		for (int i = 0; i < materials.size(); i++) {
			materialBwr[i].array_back(modelOut->materialNames[i], sizeof(char), materials[i].second.length());
		}

		uint32_t strideArraySize = modelOut->submeshCount * sizeof(StrideData);

		StrideData* submeshStrideInfo = reinterpret_cast<StrideData*>(malloc(strideArraySize));
		StrideData* materialNameStrideInfo = reinterpret_cast<StrideData*>(malloc(strideArraySize));

		uint32_t currentStrideSize = 10 + 20 + 4 + strideArraySize * 2 + 24 + 4; // validation, version, submesh count, 2 stride arrays, volume bounds, sphere bounds

		for (int i = 0; i < modelOut->submeshCount; i++) {
			uint32_t submeshStride = currentStrideSize;
			currentStrideSize += static_cast<uint32_t>(submeshBwr[i].size()); // add for next stride
			reinterpret_cast<StrideData*>(submeshStrideInfo)[i] = { submeshStride, static_cast<uint32_t>(submeshBwr[i].size()) };
		}
		for (int i = 0; i < modelOut->submeshCount; i++) {
			uint32_t matnameStride = currentStrideSize;
			currentStrideSize += static_cast<uint32_t>(materialBwr[i].size()); // add for next stride
			reinterpret_cast<StrideData*>(materialNameStrideInfo)[i] = { matnameStride, static_cast<uint32_t>(materialBwr[i].size()) };
		}

		BinaryStream bwriter = BinaryStream();
		Version ev = ENGINE_VERSION;
		char txt[10] = ".S3DMESH.";
		bwriter.array_back((char*)txt, sizeof(char), 10);
		bwriter << ev;
		bwriter << modelOut->submeshCount;
		bwriter.array_back(submeshStrideInfo, sizeof(StrideData), modelOut->submeshCount);
		bwriter.array_back(materialNameStrideInfo, sizeof(StrideData), modelOut->submeshCount);
		bwriter << modelOut->entireBounds;
		bwriter << modelOut->entireSphereBounds;
		for (int i = 0; i < modelOut->submeshCount; i++) {
			bwriter << submeshBwr[i];
		}
		for (int i = 0; i < modelOut->submeshCount; i++) {
			bwriter << materialBwr[i];
		}
		SHARD3D_INFO("Imported Mesh Size (kB) {0}", static_cast<float>(bwriter.size()) / 1024.f);

		IOUtils::writeStackBinary(bwriter.get(), bwriter.size(), modelDest.getAbsolute());

		// cleanup
		std::free(submeshStrideInfo);
		std::free(materialNameStrideInfo);
		ModelImporter::freeMeshData(modelOut);

		return modelInfo;
	}
	/*
*.s3dskeleton file format
	>> Skeleton
		-- .S3DSKELETON. (validation text, 14 bytes)
		-- Engine Version (20 byte)

		-- Vertex Count (4 byte)
		-- Morph Target Count (4 byte)
		-- Morph Target Strides & Sizes (4 byte stride length, 4 byte morph target size) (morph target count * 8 bytes)
		-- Morph Target Name Strides & Sizes (4 byte stride length, 4 byte target name size) (morph target count * 8 bytes)

		-- Bone Count (4 byte)
		-- Bone Strides & Sizes (4 byte stride length, 4 byte morph target size) (bone count * 8 bytes)
		-- Bone Name Strides & Sizes (4 byte stride length, 4 byte bone name size) (bone count * 8 bytes)

		-- Vertex Skinning
			BonesPerVertex (4 bytes);
			(PACKED NESTED ARRAY) -- BoneID0[0-BonesPerVertex], BoneID1[0-BonesPerVertex], [...] BoneIDN[0-BonesPerVertex] (1 * BonesPerVertex byte array, 0xFF == no bone)
			(PACKED NESTED ARRAY) -- BoneWeight0[0-BonesPerVertex], BoneWeight1[0-BonesPerVertex], [...] BoneWeightN[0-BonesPerVertex] (1 * BonesPerVertex byte array)


		-- MorphTarget0, MorphTarget1, MorphTarget2,  MorphTarget3, [...], MorphTargetN
		-- Bone0, Bone1, Bone2, Bone3, [...], BoneN
		-- MorphTargetName0, MorphTargetName1, MorphTargetName2, MorphTargetName3, [...], MorphTargetNameN (array of 1 byte char array)
		-- BoneName0, BoneName1, BoneName2, BoneName3, [...], BoneNameN (array of 1 byte char array)
============================================================================================================================
			>> MorphTarget
				-- TargetIndex (4 byte)
				-- VolumeBounds (24 bytes)
				-- SphereBounds (4 bytes)
				-- TargetPostitionVector0, TargetPositionVector1, [...], TargetPositionVectorN (4 byte array)
				-- TargetNormal0, TargetNormal1, [...], TargetNormalN (4 byte array)
				-- TargetTangent0, TargetTangent1, [...], TargetTangentN (4 byte array)

			>> Bone
				-- BoneIndex (1 byte)
				-- ParentBone (1 Byte)
				-- Child Bone Count (4 byte)
				-- ChildBone0, ChildBone1, [...], ChildBoneN (1 Byte array)
				-- VolumeBounds (24 bytes)
				-- SphereBounds (4 bytes)
				-- OffsetMatrix (64 bytes)
*/
	SkeletonRawData* ModelImporter::loadSkeletalData(AssetID skeletonLocation) {
		SkeletonRawData* data = new SkeletonRawData();
		if (!IOUtils::doesFileExist(skeletonLocation.getAbsolute())) {
			SHARD3D_ERROR("Failed to load skeletal data! File does not exist!");
			return nullptr;
		}
		std::vector<char> rdw = IOUtils::readBinary(skeletonLocation.getAbsolute());
		void* raw = reinterpret_cast<void*>(rdw.data());
		size_t baseAddress = (size_t)raw;
		const char* txt = ".S3DSKELETON.";
		if (memcmp(raw, txt, 14) != 0) {
			SHARD3D_ERROR("Failed to load skeletal data! Incorrect file format! Requires .s3dskeleton file!");
			return nullptr;
		}
		auto newTime = std::chrono::high_resolution_clock::now();

		Version version = *reinterpret_cast<Version*>(baseAddress + 14);
		data->vertexCount = *reinterpret_cast<uint32_t*>(baseAddress + 14 + 20);
		data->morphTargetCount = *reinterpret_cast<uint32_t*>(baseAddress + 14 + 20 + 4);
		size_t morphTargetStrideLength = data->morphTargetCount * sizeof(StrideData);
		data->boneCount = *reinterpret_cast<uint32_t*>(baseAddress + 14 + 20 + 4 + 4 + morphTargetStrideLength * 2);
		size_t boneStrideLength = data->boneCount * sizeof(StrideData);

		VertexSkinData* vertexSkin = new VertexSkinData();
		data->skinData = vertexSkin;

		vertexSkin->maxBonesPerVertex = *reinterpret_cast<uint32_t*>(baseAddress + 14 + 20 + 4 + 4 + morphTargetStrideLength * 2 + 4 + boneStrideLength * 2);
		size_t boneIDArraySize = vertexSkin->maxBonesPerVertex * sizeof(uint8_t) * data->vertexCount;
		vertexSkin->boneIDs = reinterpret_cast<uint8_t*>(malloc(boneIDArraySize));
		size_t boneWeightArraySize = vertexSkin->maxBonesPerVertex * sizeof(uint8_t) * data->vertexCount;
		vertexSkin->boneWeights = reinterpret_cast<uint8_t*>(malloc(boneWeightArraySize));

		size_t skinDataOffset = 14 + 20 + 4 + 4 + morphTargetStrideLength * 2 + 4 + boneStrideLength * 2 + 4;
		memcpy(vertexSkin->boneIDs, (void*)(baseAddress + skinDataOffset), boneIDArraySize);
		memcpy(vertexSkin->boneWeights, (void*)(baseAddress + skinDataOffset + boneIDArraySize), boneWeightArraySize);

		data->morphTargets = reinterpret_cast<MorphTargetRawData**>(calloc(data->morphTargetCount, sizeof(MorphTargetRawData*)));
		data->morphTargetNames = reinterpret_cast<char**>(calloc(data->morphTargetCount, sizeof(char*)));
		data->bones = reinterpret_cast<BoneRawData**>(calloc(data->boneCount, sizeof(BoneRawData*)));
		data->boneNames = reinterpret_cast<char**>(calloc(data->boneCount, sizeof(char*)));

		for (int i = 0; i < data->morphTargetCount; i++) {
			data->morphTargets[i] = new MorphTargetRawData();
			StrideData& morphTargetStrideInfo = *reinterpret_cast<StrideData*>(baseAddress + 14 + 20 + 4 + 4 + i * sizeof(StrideData));
			StrideData& morphTargetNameStrideInfo = *reinterpret_cast<StrideData*>(baseAddress + 14 + 20 + 4 + 4 + morphTargetStrideLength + i * sizeof(StrideData));
			data->morphTargets[i]->targetIndex = *reinterpret_cast<uint32_t*>(baseAddress + morphTargetStrideInfo.stride);
			data->morphTargets[i]->bounds = *reinterpret_cast<VolumeBounds*>(baseAddress + morphTargetStrideInfo.stride + 4);
			data->morphTargets[i]->sphereBounds = *reinterpret_cast<SphereBounds*>(baseAddress + morphTargetStrideInfo.stride + 28);

			data->morphTargets[i]->targetVectorPositions = reinterpret_cast<uint32_t*>(malloc(data->vertexCount * sizeof(uint32_t)));
			data->morphTargets[i]->targetVectorNormals = reinterpret_cast<uint32_t*>(malloc(data->vertexCount * sizeof(uint32_t)));
			data->morphTargets[i]->targetVectorTangents = reinterpret_cast<uint32_t*>(malloc(data->vertexCount * sizeof(uint32_t)));
			memcpy(data->morphTargets[i]->targetVectorPositions, (void*)(baseAddress + morphTargetStrideInfo.stride + 32), data->vertexCount * sizeof(uint32_t));
			memcpy(data->morphTargets[i]->targetVectorNormals, (void*)(baseAddress + morphTargetStrideInfo.stride + 32 + data->vertexCount * sizeof(uint32_t)), data->vertexCount * sizeof(uint32_t));
			memcpy(data->morphTargets[i]->targetVectorTangents, (void*)(baseAddress + morphTargetStrideInfo.stride + 32 + data->vertexCount * sizeof(uint32_t) + data->vertexCount * sizeof(uint32_t)), data->vertexCount * sizeof(uint32_t));

			data->morphTargetNames[i] = reinterpret_cast<char*>(calloc(morphTargetNameStrideInfo.size + 1, sizeof(char)));
			strncpy(data->morphTargetNames[i], reinterpret_cast<char*>(baseAddress + morphTargetNameStrideInfo.stride), morphTargetNameStrideInfo.size);
		}

		for (int i = 0; i < data->boneCount; i++) {
			data->bones[i] = new BoneRawData();
			StrideData& boneStrideInfo = *reinterpret_cast<StrideData*>(baseAddress + 14 + 20 + 4 + 4 + morphTargetStrideLength * 2 + 4 + i * sizeof(StrideData));
			StrideData& boneNameStrideInfo = *reinterpret_cast<StrideData*>(baseAddress + 14 + 20 + 4 + 4 + morphTargetStrideLength * 2 + 4 + boneStrideLength + i * sizeof(StrideData));
			data->bones[i]->boneIndex = *reinterpret_cast<uint8_t*>(baseAddress + boneStrideInfo.stride);
			data->bones[i]->parentBone = *reinterpret_cast<uint8_t*>(baseAddress + boneStrideInfo.stride + 1);
			data->bones[i]->childBoneCount = *reinterpret_cast<uint32_t*>(baseAddress + boneStrideInfo.stride + 2);
			if (data->bones[i]->childBoneCount != 0) data->bones[i]->childBones = reinterpret_cast<uint8_t*>(malloc(data->bones[i]->childBoneCount));
			for (int j = 0; j < data->bones[i]->childBoneCount; j++) {
				data->bones[i]->childBones[j] = *reinterpret_cast<uint8_t*>(baseAddress + boneStrideInfo.stride + 6 + j);
			}
			data->bones[i]->bounds = *reinterpret_cast<VolumeBounds*>(baseAddress + boneStrideInfo.stride + 6 + data->bones[i]->childBoneCount);
			data->bones[i]->sphereBounds = *reinterpret_cast<SphereBounds*>(baseAddress + boneStrideInfo.stride + 30 + data->bones[i]->childBoneCount);
			data->bones[i]->offsetMatrix = *reinterpret_cast<glm::mat4*>(baseAddress + boneStrideInfo.stride + 34 + data->bones[i]->childBoneCount);

			data->boneNames[i] = reinterpret_cast<char*>(calloc(boneNameStrideInfo.size + 1, sizeof(char)));
			strncpy(data->boneNames[i], reinterpret_cast<char*>(baseAddress + boneNameStrideInfo.stride), boneNameStrideInfo.size);
		}
		SHARD3D_LOG("Duration of loading s3dskeleton {0}: {1} ms", skeletonLocation.getAsset(),
			std::chrono::duration<float, std::chrono::milliseconds::period>
			(std::chrono::high_resolution_clock::now() - newTime).count());

		return data;
	}

	void ModelImporter::freeSkeletalData(SkeletonRawData* skeletalData) {
		// clean up memory
		std::free(skeletalData->skinData->boneIDs);
		std::free(skeletalData->skinData->boneWeights);
		delete skeletalData->skinData;

		for (int i = 0; i < skeletalData->morphTargetCount; i++) {
			MorphTargetRawData* morphTarget = skeletalData->morphTargets[i];
			std::free(morphTarget->targetVectorPositions);
			std::free(morphTarget->targetVectorNormals);
			std::free(morphTarget->targetVectorTangents);
			delete morphTarget;
			std::free(skeletalData->morphTargetNames[i]);
		}
		std::free(skeletalData->morphTargets);
		std::free(skeletalData->morphTargetNames);

		for (int i = 0; i < skeletalData->boneCount; i++) {
			BoneRawData* bone = skeletalData->bones[i];
			if (bone->childBoneCount) {
				std::free(bone->childBones);
			}
			delete bone;
			std::free(skeletalData->boneNames[i]);
		}
		std::free(skeletalData->bones);
		std::free(skeletalData->boneNames);
		delete skeletalData;
	}

	static void exportSkeletalData(const ModelImporter::Builder::MeshLoadData& load, AssetID skeletonDest) {
		int vertexCount = 0;
		for (auto& submesh : load.submeshes) {
			vertexCount += submesh.second.indices.size();
		}

		uint32_t morphCount = load.morphTargets.size() ? load.morphTargets.at(0).size() : 0;
		uint32_t boneCount = load.bones.size();

		std::vector<std::pair<uint32_t, std::string>> materials;
		for (const auto& [slot, index] : load.slotToIndex) {
			materials.push_back({ index, slot });
		}

		std::unordered_map<std::string, RiggedMorphData> morphs;
		if (morphCount) {
			for (int i = 0; i < materials.size(); i++) {
				for (const auto& [name, target] : load.morphTargets.at(i)) {
					if (morphs.find(name) == morphs.end()) {
						morphs[name] = target;
					} else {
						for (int i = 0; i < target.targetVertices.size(); i++) {
							morphs[name].targetVertices.push_back(target.targetVertices[i]);
						}
					}
				}
			}
		}
		MorphTargetRawData** morphsOut = reinterpret_cast<MorphTargetRawData**>(calloc(morphCount, sizeof(MorphTargetRawData*)));
		char** morphNames = reinterpret_cast<char**>(calloc(morphCount, sizeof(char*)));
		std::vector<size_t> morphNameLengths(morphCount);
		if (morphCount) {
			for (auto& [name, morphTarget] : morphs) {
				morphsOut[morphTarget.id] = new MorphTargetRawData();
				morphsOut[morphTarget.id]->targetIndex = morphTarget.id;
				morphNames[morphTarget.id] = reinterpret_cast<char*>(malloc(name.length()));
				strncpy(morphNames[morphTarget.id], name.data(), name.length());
				morphNameLengths[morphTarget.id] = name.length();

				morphsOut[morphTarget.id]->bounds = morphTarget.volumeBounds;
				morphsOut[morphTarget.id]->sphereBounds = morphTarget.sphereBounds;

				morphsOut[morphTarget.id]->targetVectorPositions = reinterpret_cast<uint32_t*>(malloc(vertexCount * sizeof(uint32_t)));
				morphsOut[morphTarget.id]->targetVectorNormals = reinterpret_cast<uint32_t*>(malloc(vertexCount * sizeof(uint32_t)));
				morphsOut[morphTarget.id]->targetVectorTangents = reinterpret_cast<uint32_t*>(malloc(vertexCount * sizeof(uint32_t)));

				for (int j = 0; j < morphTarget.targetVertices.size(); j++) {
					// convert positions to target vectors
					const Vertex& vertex = morphTarget.targetVertices[j];
					morphsOut[morphTarget.id]->targetVectorPositions[j] = POLAR_VEC3_PACK(vertex.position);
					morphsOut[morphTarget.id]->targetVectorNormals[j] = POLAR_VEC3_PACK(vertex.normal);
					morphsOut[morphTarget.id]->targetVectorTangents[j] = POLAR_VEC3_PACK(vertex.tangent);
				}
			}
		}
		std::vector<BinaryStream> morphNameBwr{};
		morphNameBwr.resize(morphCount);
		for (int i = 0; i < morphCount; i++) {
			morphNameBwr[i].array_back(morphNames[i], sizeof(char), morphNameLengths[i]);
		}

		VertexSkinData* skinData = new VertexSkinData();
		size_t vertexSkinArraySize = vertexCount * sizeof(uint8_t) * MAX_VERTEX_BONES;
		skinData->boneIDs = reinterpret_cast<uint8_t*>(malloc(vertexSkinArraySize));
		skinData->boneWeights = reinterpret_cast<uint8_t*>(malloc(vertexSkinArraySize));
		for (int i = 0; i < vertexSkinArraySize; i++) {
			skinData->boneIDs[i] = 0xFF;
		}
		for (auto& [index, skin] : load.skins) {
			// normalize weights
			float weightSum = 0.0f;
			std::array<float, MAX_VERTEX_BONES> weights;
			for (int i = 0; i < MAX_VERTEX_BONES; i++) {
				weights[i] = static_cast<float>(skin.weights[i]) / 255.0f;
				weightSum += weights[i];
			}
			if (weightSum == 1.0) {
				for (int i = 0; i < MAX_VERTEX_BONES; i++) {
					skinData->boneIDs[index * MAX_VERTEX_BONES + i] = skin.bones[i];
					skinData->boneWeights[index * MAX_VERTEX_BONES + i] = skin.weights[i];
				}
				continue;
			} else if (weightSum > 0.0) {
				int integerWeightSum = 0;
				for (int i = 0; i < MAX_VERTEX_BONES; i++) {
					weights[i] /= weightSum;
					weights[i] = static_cast<float>(static_cast<int>(weights[i] * 255.0f));
					integerWeightSum += weights[i];
				}
				// check for rounding errors
				if (integerWeightSum != 255) {
					int greatestIndex = 0;
					float greatestWeight = 0.0f;
					for (int i = 0; i < MAX_VERTEX_BONES; i++) {
						if (weights[i] > greatestWeight) {
							greatestWeight = weights[i];
							greatestIndex = i;
						}
					}
					if (integerWeightSum > 255) {
						while (integerWeightSum > 255) {
							integerWeightSum--;
							weights[greatestIndex]--;
						}
					} else {
						while (integerWeightSum < 255) {
							integerWeightSum++;
							weights[greatestIndex]++;
						}
					}
				}
				for (int i = 0; i < MAX_VERTEX_BONES; i++) {
					skinData->boneIDs[index * MAX_VERTEX_BONES + i] = skin.bones[i];
					skinData->boneWeights[index * MAX_VERTEX_BONES + i] = weights[i];
				}
#ifndef NDEBUG
				int debugSum = 0;
				for (int i = 0; i < MAX_VERTEX_BONES; i++) {
					debugSum += weights[i];
				}
				SHARD3D_ASSERT(debugSum == 255 || debugSum == 0);
#endif

			} else {
				for (int i = 0; i < MAX_VERTEX_BONES; i++) {
					skinData->boneIDs[index * MAX_VERTEX_BONES + i] = 0xFF;
				}
			}

		}

		BoneRawData** bonesOut = reinterpret_cast<BoneRawData**>(calloc(boneCount, sizeof(BoneRawData*)));
		char** boneNames = reinterpret_cast<char**>(calloc(boneCount, sizeof(char*)));
		std::vector<size_t> boneNameLengths(boneCount);
		for (auto& [name, bone] : load.bones) {
			bonesOut[bone.id] = new BoneRawData();
			bonesOut[bone.id]->boneIndex = bone.id;
			boneNames[bone.id] = reinterpret_cast<char*>(malloc(name.length()));
			strncpy(boneNames[bone.id], name.data(), name.length());
			boneNameLengths[bone.id] = name.length();

			bonesOut[bone.id]->parentBone = bone.parent.empty() ? 0xFF : load.bones.at(bone.parent).id;
			bonesOut[bone.id]->childBoneCount = bone.children.size();
			bonesOut[bone.id]->childBones = reinterpret_cast<uint8_t*>(calloc(bone.children.size(), sizeof(uint8_t)));
			memcpy(bonesOut[bone.id]->childBones, bone.children.data(), bone.children.size());

			bonesOut[bone.id]->bounds = bone.volumeBounds;
			bonesOut[bone.id]->sphereBounds = bone.sphereBounds;
			bonesOut[bone.id]->offsetMatrix = bone.offset;
		}

		std::vector<BinaryStream> boneNameBwr{};
		boneNameBwr.resize(boneCount);
		for (int i = 0; i < boneCount; i++) {
			boneNameBwr[i].array_back(boneNames[i], sizeof(char), boneNameLengths[i]);
		}

		std::vector<BinaryStream> morphBwr{};
		morphBwr.resize(morphCount);
		for (int i = 0; i < morphCount; i++) {
			MorphTargetRawData* data = morphsOut[i];
			morphBwr[i] << data->targetIndex;
			morphBwr[i] << data->bounds;
			morphBwr[i] << data->sphereBounds;

			morphBwr[i].array_back(data->targetVectorPositions, sizeof(uint32_t), vertexCount);
			morphBwr[i].array_back(data->targetVectorNormals, sizeof(uint32_t), vertexCount);
			morphBwr[i].array_back(data->targetVectorTangents, sizeof(uint32_t), vertexCount);
		}
		std::vector<BinaryStream> boneBwr{};
		boneBwr.resize(boneCount);
		for (int i = 0; i < boneCount; i++) {
			BoneRawData* data = bonesOut[i];
			boneBwr[i] << data->boneIndex;
			boneBwr[i] << data->parentBone;
			boneBwr[i] << data->childBoneCount;
			if (data->childBoneCount) {
				boneBwr[i].array_back(data->childBones, sizeof(uint8_t), data->childBoneCount);
			}
			boneBwr[i] << data->bounds;
			boneBwr[i] << data->sphereBounds;
			boneBwr[i] << data->offsetMatrix;
		}

		uint32_t boneStrideArraySize = boneCount * sizeof(StrideData);
		uint32_t morphStrideArraySize = morphCount * sizeof(StrideData);

		StrideData* boneStrideInfo = reinterpret_cast<StrideData*>(malloc(boneStrideArraySize));
		StrideData* boneNameStrideInfo = reinterpret_cast<StrideData*>(malloc(boneStrideArraySize));
		StrideData* morphStrideInfo = reinterpret_cast<StrideData*>(malloc(morphStrideArraySize));
		StrideData* morphNameStrideInfo = reinterpret_cast<StrideData*>(malloc(morphStrideArraySize));

		uint32_t currentStrideSize = 14 + 20 + 4 + 4 + morphStrideArraySize * 2 + 4 + boneStrideArraySize * 2 + 4 + vertexCount * MAX_VERTEX_BONES * (1 + 1);

		for (int i = 0; i < morphCount; i++) {
			uint32_t  morphStride = currentStrideSize;
			currentStrideSize += static_cast<uint32_t>(morphBwr[i].size()); // add for next stride
			reinterpret_cast<StrideData*>(morphStrideInfo)[i] = { morphStride, static_cast<uint32_t>(morphBwr[i].size()) };
		}
		for (int i = 0; i < boneCount; i++) {
			uint32_t boneStride = currentStrideSize;
			currentStrideSize += static_cast<uint32_t>(boneBwr[i].size()); // add for next stride
			reinterpret_cast<StrideData*>(boneStrideInfo)[i] = { boneStride, static_cast<uint32_t>(boneBwr[i].size()) };
		}
		for (int i = 0; i < morphCount; i++) {
			uint32_t morphNameStride = currentStrideSize;
			currentStrideSize += static_cast<uint32_t>(morphNameBwr[i].size()); // add for next stride
			reinterpret_cast<StrideData*>(morphNameStrideInfo)[i] = { morphNameStride, static_cast<uint32_t>(morphNameBwr[i].size()) };
		}
		for (int i = 0; i < boneCount; i++) {
			uint32_t boneNameStride = currentStrideSize;
			currentStrideSize += static_cast<uint32_t>(boneNameBwr[i].size()); // add for next stride
			reinterpret_cast<StrideData*>(boneNameStrideInfo)[i] = { boneNameStride, static_cast<uint32_t>(boneNameBwr[i].size()) };
		}

		BinaryStream bwriter = BinaryStream();
		Version ev = ENGINE_VERSION;
		char txt[14] = ".S3DSKELETON.";
		bwriter.array_back((char*)txt, sizeof(char), 14);
		bwriter << ev;
		bwriter << vertexCount;
		bwriter << morphCount;
		bwriter.array_back(morphStrideInfo, sizeof(StrideData), morphCount);
		bwriter.array_back(morphNameStrideInfo, sizeof(StrideData), morphCount);
		bwriter << boneCount;
		bwriter.array_back(boneStrideInfo, sizeof(StrideData), boneCount);
		bwriter.array_back(boneNameStrideInfo, sizeof(StrideData), boneCount);
		bwriter << MAX_VERTEX_BONES;
		bwriter.array_back(skinData->boneIDs, sizeof(uint8_t) * MAX_VERTEX_BONES, vertexCount);
		bwriter.array_back(skinData->boneWeights, sizeof(uint8_t) * MAX_VERTEX_BONES, vertexCount);
		for (int i = 0; i < morphCount; i++) {
			bwriter << morphBwr[i];
		}
		for (int i = 0; i < boneCount; i++) {
			bwriter << boneBwr[i];
		}
		for (int i = 0; i < morphCount; i++) {
			bwriter << morphNameBwr[i];
		}
		for (int i = 0; i < boneCount; i++) {
			bwriter << boneNameBwr[i];
		}

		SHARD3D_INFO("Imported Skeleton Size (kB) {0}", static_cast<float>(bwriter.size()) / 1024.f);

		IOUtils::writeStackBinary(bwriter.get(), bwriter.size(), skeletonDest.getAbsolute());

	}

	/*
	*.s3danimation file format
		>> animation
			-- .S3DANIMATION. (validation text, 15 bytes)
			-- Engine Version (20 byte)

			-- Max Duration (4 bytes)
			-- Bone Anim Count (4 byte) (MUST MATCH SKELETON)
			-- Bone Anim Strides & Sizes (4 byte stride length, 4 byte bone anim size) (sum of all bone animation sizes)
			-- Morph Anim Count (4 byte) (MUST MATCH SKELETON)
			-- Morph Anim Strides & Sizes (4 byte stride length, 4 byte morph anim size) (sum of all morph animation sizes)

			-- BoneAnim0, BoneAnim1, BoneAnim2, BoneAnim3, [...], BoneAnimN
			-- MorphAnim0, MorphAnim1, MorphAnim2, MorphAnim3, [...], MorphAnimN
	============================================================================================================================
				>> Bone Anim
					-- TranslationKeyFrameCount (4 bytes);
					-- QuaternionKeyFrameCount (4 bytes);
					-- ScaleKeyFrameCount (4 bytes);
					-- TranslationKeyFrames (12 byte array);
					-- TranslationKeyFrameTimeStamps (4 byte array);
					-- QuaternionKeyFrames (16 byte array);
					-- QuaternionKeyFrameTimeStamps (4 byte array);
					-- ScaleKeyFrames (12 byte array);
					-- ScaleKeyFrameTimeStamps (4 byte array);
				>> Morph Anim
					-- FactorKeyFrameCount (4 bytes);
					-- FactorKeyFrames (4 byte array);
					-- FactorKeyFrameTimeStamps (4 byte array);

	*/

	SkeletalAnimationRawData* ModelImporter::loadAnimationData(AssetID animationLocation) {
		SkeletalAnimationRawData* data = new SkeletalAnimationRawData();
		if (!IOUtils::doesFileExist(animationLocation.getAbsolute())) {
			SHARD3D_ERROR("Failed to load animation data! File does not exist!");
			return nullptr;
		}
		std::vector<char> rdw = IOUtils::readBinary(animationLocation.getAbsolute());
		void* raw = reinterpret_cast<void*>(rdw.data());
		size_t baseAddress = (size_t)raw;
		const char* txt = ".S3DANIMATION.";
		if (memcmp(raw, txt, 15) != 0) {
			SHARD3D_ERROR("Failed to load animation data! Incorrect file format! Requires .s3danimation file!");
			return nullptr;
		}
		auto newTime = std::chrono::high_resolution_clock::now();
		Version version = *reinterpret_cast<Version*>(baseAddress + 15);
		data->maxDuration = *reinterpret_cast<float*>(baseAddress + 15 + 20);
		data->boneAnimationCount = *reinterpret_cast<uint32_t*>(baseAddress + 15 + 20 + 4);
		data->morphAnimationCount = *reinterpret_cast<uint32_t*>(baseAddress + 15 + 20 + 4 + 4);
		BoneAnimationRawData** boneAnimations = reinterpret_cast<BoneAnimationRawData**>(calloc(data->boneAnimationCount, sizeof(BoneAnimationRawData*)));

		data->boneAnimations = boneAnimations;
		data->morphAnimations = nullptr;

		for (int i = 0; i < data->boneAnimationCount; i++) {
			boneAnimations[i] = new BoneAnimationRawData();
			StrideData& boneAnimStrideInfo = *reinterpret_cast<StrideData*>(baseAddress + 15 + 20 + 4 + 4 + 4 + i * sizeof(StrideData));

			boneAnimations[i]->translationKeyFrameCount = *reinterpret_cast<uint32_t*>(baseAddress + boneAnimStrideInfo.stride);
			boneAnimations[i]->quaternionKeyFrameCount = *reinterpret_cast<uint32_t*>(baseAddress + boneAnimStrideInfo.stride + 4);
			boneAnimations[i]->scaleKeyFrameCount = *reinterpret_cast<uint32_t*>(baseAddress + boneAnimStrideInfo.stride + 8);

			uint32_t translationKeyFramesSize = boneAnimations[i]->translationKeyFrameCount * sizeof(glm::vec3);
			uint32_t translationKeyFrameTimeStampsSize = boneAnimations[i]->translationKeyFrameCount * sizeof(float);
			uint32_t quaternionKeyFramesSize = boneAnimations[i]->quaternionKeyFrameCount * sizeof(glm::vec4);
			uint32_t quaternionKeyFrameTimeStampsSize = boneAnimations[i]->quaternionKeyFrameCount * sizeof(float);
			uint32_t scaleKeyFramesSize = boneAnimations[i]->scaleKeyFrameCount * sizeof(glm::vec3);
			uint32_t scaleKeyFrameTimeStampsSize = boneAnimations[i]->scaleKeyFrameCount * sizeof(float);
			boneAnimations[i]->translationKeyFrames = reinterpret_cast<glm::vec3*>(malloc(translationKeyFramesSize));
			boneAnimations[i]->translationKeyFrameTimeStamps = reinterpret_cast<float*>(malloc(translationKeyFrameTimeStampsSize));
			boneAnimations[i]->quaternionKeyFrames = reinterpret_cast<glm::vec4*>(malloc(quaternionKeyFramesSize));
			boneAnimations[i]->quaternionKeyFrameTimeStamps = reinterpret_cast<float*>(malloc(quaternionKeyFrameTimeStampsSize));
			boneAnimations[i]->scaleKeyFrames = reinterpret_cast<glm::vec3*>(malloc(scaleKeyFramesSize));
			boneAnimations[i]->scaleKeyFrameTimeStamps = reinterpret_cast<float*>(malloc(scaleKeyFrameTimeStampsSize));

			memcpy(boneAnimations[i]->translationKeyFrames, 
				(void*)(baseAddress + boneAnimStrideInfo.stride + 12), translationKeyFramesSize);
			memcpy(boneAnimations[i]->translationKeyFrameTimeStamps, 
				(void*)(baseAddress + boneAnimStrideInfo.stride + 12 + translationKeyFramesSize), translationKeyFrameTimeStampsSize);

			memcpy(boneAnimations[i]->quaternionKeyFrames,
				(void*)(baseAddress + boneAnimStrideInfo.stride + 12 + translationKeyFramesSize + translationKeyFrameTimeStampsSize), quaternionKeyFramesSize);
			memcpy(boneAnimations[i]->quaternionKeyFrameTimeStamps,
				(void*)(baseAddress + boneAnimStrideInfo.stride + 12 + translationKeyFramesSize + translationKeyFrameTimeStampsSize + quaternionKeyFramesSize), quaternionKeyFrameTimeStampsSize);
			
			memcpy(boneAnimations[i]->scaleKeyFrames,
				(void*)(baseAddress + boneAnimStrideInfo.stride + 12 + translationKeyFramesSize + translationKeyFrameTimeStampsSize + quaternionKeyFramesSize + quaternionKeyFrameTimeStampsSize), scaleKeyFramesSize);
			memcpy(boneAnimations[i]->scaleKeyFrameTimeStamps,
				(void*)(baseAddress + boneAnimStrideInfo.stride + 12 + translationKeyFramesSize + translationKeyFrameTimeStampsSize + quaternionKeyFramesSize + quaternionKeyFrameTimeStampsSize + scaleKeyFramesSize), scaleKeyFrameTimeStampsSize);
		}
		SHARD3D_LOG("Duration of loading s3danim {0}: {1} ms", animationLocation.getAsset(),
			std::chrono::duration<float, std::chrono::milliseconds::period>
			(std::chrono::high_resolution_clock::now() - newTime).count());

		return data;
	}

	void ModelImporter::freeAnimationData(SkeletalAnimationRawData* skeletonData) {
		// clean up memory
		for (int i = 0; i < skeletonData->boneAnimationCount; i++) {
			BoneAnimationRawData* animation = skeletonData->boneAnimations[i];
			std::free(animation->translationKeyFrames);
			std::free(animation->translationKeyFrameTimeStamps);
			std::free(animation->quaternionKeyFrames);
			std::free(animation->quaternionKeyFrameTimeStamps);
			std::free(animation->scaleKeyFrames);
			std::free(animation->scaleKeyFrameTimeStamps);
			delete animation;
		}
		for (int i = 0; i < skeletonData->morphAnimationCount; i++) {
			MorphAnimationRawData* animation = skeletonData->morphAnimations[i];
			std::free(animation->factorKeyFrames);
			std::free(animation->factorKeyFrameTimeStamps);
			delete animation;
		}
		std::free(skeletonData->boneAnimations);
		std::free(skeletonData->morphAnimations);
		delete skeletonData;
	}

	static void exportAnimationData(const ModelImporter::Builder::MeshLoadData& load, AssetID animDest, int animIndex) {
		std::vector<std::pair<int, std::string>> bones;
		std::vector<std::pair<int, std::string>> morphs;

		bones.resize(load.bones.size());
		morphs.resize(load.morphTargets.size());

		for (auto& [boneName, bone] : load.bones) { // i = bone index
			bones[bone.id] = { bone.id, boneName };
		}
		const RiggedAnimationData& animation = load.animations[animIndex];
		size_t boneAnimCount = animation.boneAnimations.size();
		BoneAnimationRawData** boneAnimsOut = reinterpret_cast<BoneAnimationRawData**>(calloc(boneAnimCount, sizeof(BoneAnimationRawData*)));
		for (int i = 0; i < boneAnimCount; i++) { // i = bone index
			auto& boneAnim = animation.boneAnimations.at(bones[i].second);
			boneAnimsOut[i] = new BoneAnimationRawData();

			boneAnimsOut[i]->translationKeyFrameCount = boneAnim.translationKeyFrames.size();
			boneAnimsOut[i]->quaternionKeyFrameCount = boneAnim.quaternionKeyFrames.size();
			boneAnimsOut[i]->scaleKeyFrameCount = boneAnim.scaleKeyFrames.size();

			boneAnimsOut[i]->translationKeyFrames = reinterpret_cast<glm::vec3*>(malloc(boneAnimsOut[i]->translationKeyFrameCount * sizeof(glm::vec3)));
			boneAnimsOut[i]->translationKeyFrameTimeStamps = reinterpret_cast<float*>(malloc(boneAnimsOut[i]->translationKeyFrameCount * sizeof(float)));
			boneAnimsOut[i]->quaternionKeyFrames = reinterpret_cast<glm::vec4*>(malloc(boneAnimsOut[i]->quaternionKeyFrameCount * sizeof(glm::vec4)));
			boneAnimsOut[i]->quaternionKeyFrameTimeStamps = reinterpret_cast<float*>(malloc(boneAnimsOut[i]->quaternionKeyFrameCount * sizeof(float)));
			boneAnimsOut[i]->scaleKeyFrames = reinterpret_cast<glm::vec3*>(malloc(boneAnimsOut[i]->scaleKeyFrameCount * sizeof(glm::vec3)));
			boneAnimsOut[i]->scaleKeyFrameTimeStamps = reinterpret_cast<float*>(malloc(boneAnimsOut[i]->scaleKeyFrameCount * sizeof(float)));

			memcpy(boneAnimsOut[i]->translationKeyFrames, boneAnim.translationKeyFrames.data(), 
				boneAnimsOut[i]->translationKeyFrameCount * sizeof(glm::vec3));
			memcpy(boneAnimsOut[i]->translationKeyFrameTimeStamps, boneAnim.translationKeyFrameTimeStamps.data(), 
				boneAnimsOut[i]->translationKeyFrameCount * sizeof(float));

			memcpy(boneAnimsOut[i]->quaternionKeyFrames, boneAnim.quaternionKeyFrames.data(),
				boneAnimsOut[i]->quaternionKeyFrameCount * sizeof(glm::vec4));
			memcpy(boneAnimsOut[i]->quaternionKeyFrameTimeStamps, boneAnim.quaternionKeyFrameTimeStamps.data(),
				boneAnimsOut[i]->quaternionKeyFrameCount * sizeof(float));

			memcpy(boneAnimsOut[i]->scaleKeyFrames, boneAnim.scaleKeyFrames.data(),
				boneAnimsOut[i]->scaleKeyFrameCount * sizeof(glm::vec3));
			memcpy(boneAnimsOut[i]->scaleKeyFrameTimeStamps, boneAnim.scaleKeyFrameTimeStamps.data(),
				boneAnimsOut[i]->scaleKeyFrameCount * sizeof(float));
		}

		SkeletalAnimationRawData* animOut = new SkeletalAnimationRawData();
		animOut->maxDuration = animation.duration;
		animOut->boneAnimationCount = static_cast<uint32_t>(boneAnimCount);
		animOut->boneAnimations = boneAnimsOut;
		animOut->morphAnimationCount = static_cast<uint32_t>(0);

		std::vector<BinaryStream> boneAnimBwr{};
		boneAnimBwr.resize(animOut->boneAnimationCount);
		for (int i = 0; i < animOut->boneAnimationCount; i++) {
			BoneAnimationRawData* data = animOut->boneAnimations[i];
			boneAnimBwr[i] << data->translationKeyFrameCount;
			boneAnimBwr[i] << data->quaternionKeyFrameCount;
			boneAnimBwr[i] << data->scaleKeyFrameCount;
			boneAnimBwr[i].array_back(data->translationKeyFrames, sizeof(glm::vec3), data->translationKeyFrameCount);
			boneAnimBwr[i].array_back(data->translationKeyFrameTimeStamps, sizeof(float), data->translationKeyFrameCount);
			boneAnimBwr[i].array_back(data->quaternionKeyFrames, sizeof(glm::vec4), data->quaternionKeyFrameCount);
			boneAnimBwr[i].array_back(data->quaternionKeyFrameTimeStamps, sizeof(float), data->quaternionKeyFrameCount);
			boneAnimBwr[i].array_back(data->scaleKeyFrames, sizeof(glm::vec3), data->scaleKeyFrameCount);
			boneAnimBwr[i].array_back(data->scaleKeyFrameTimeStamps, sizeof(float), data->scaleKeyFrameCount);
		}
		std::vector<BinaryStream> morphAnimBwr{};
		morphAnimBwr.resize(animOut->morphAnimationCount);
		for (int i = 0; i < animOut->morphAnimationCount; i++) {
			MorphAnimationRawData* data = animOut->morphAnimations[i];
			morphAnimBwr[i] << data->factorKeyFrameCount;
			morphAnimBwr[i].array_back(data->factorKeyFrames, sizeof(float), data->factorKeyFrameCount);
			morphAnimBwr[i].array_back(data->factorKeyFrameTimeStamps, sizeof(float), data->factorKeyFrameCount);
		}
		uint32_t strideBoneAnimArraySize = animOut->boneAnimationCount * sizeof(StrideData);
		uint32_t strideMorphAnimArraySize = animOut->morphAnimationCount * sizeof(StrideData);

		StrideData* boneAnimStrideInfo = reinterpret_cast<StrideData*>(malloc(strideBoneAnimArraySize));
		StrideData* morphAnimStrideInfo = reinterpret_cast<StrideData*>(malloc(strideMorphAnimArraySize));

		uint32_t currentStrideSize = 15 + 20 + 4 + 4 + 4 + strideBoneAnimArraySize + strideMorphAnimArraySize; // validation, version, max duration, bone anim count, morph anim count, 2 stride arrays

		for (int i = 0; i < animOut->boneAnimationCount; i++) {
			uint32_t boneAnimStride = currentStrideSize;
			currentStrideSize += static_cast<uint32_t>(boneAnimBwr[i].size()); // add for next stride
			reinterpret_cast<StrideData*>(boneAnimStrideInfo)[i] = { boneAnimStride, static_cast<uint32_t>(boneAnimBwr[i].size()) };
		}
		for (int i = 0; i < animOut->morphAnimationCount; i++) {
			uint32_t morphAnimStride = currentStrideSize;
			currentStrideSize += static_cast<uint32_t>(morphAnimBwr[i].size()); // add for next stride
			reinterpret_cast<StrideData*>(morphAnimStrideInfo)[i] = { morphAnimStride, static_cast<uint32_t>(morphAnimBwr[i].size()) };
		}

		BinaryStream bwriter = BinaryStream();
		Version ev = ENGINE_VERSION;
		char txt[15] = ".S3DANIMATION.";
		bwriter.array_back((char*)txt, sizeof(char), 15);
		bwriter << ev;
		bwriter << animOut->maxDuration;
		bwriter << animOut->boneAnimationCount;
		bwriter << animOut->morphAnimationCount;
		bwriter.array_back(boneAnimStrideInfo, sizeof(StrideData), animOut->boneAnimationCount);
		bwriter.array_back(morphAnimStrideInfo, sizeof(StrideData), animOut->morphAnimationCount);
		for (int i = 0; i < animOut->boneAnimationCount; i++) {
			bwriter << boneAnimBwr[i];
		}
		for (int i = 0; i < animOut->morphAnimationCount; i++) {
			bwriter << morphAnimBwr[i];
		}
		SHARD3D_INFO("Imported Animation Size (kB) {0}", static_cast<float>(bwriter.size()) / 1024.f);

		IOUtils::writeStackBinary(bwriter.get(), bwriter.size(), animDest.getAbsolute());

		// cleanup
		std::free(boneAnimStrideInfo);
		std::free(morphAnimStrideInfo);
		ModelImporter::freeAnimationData(animOut);
	}

	std::vector<ModelInfo> ModelImporter::importModelsFromFile(Model3DImportInfo importInfo, ResourceSystem* resourceSystem, const std::string& modelSource, AssetID modelDest) {
		ModelImporter::Builder builder{};
		
		std::ifstream f(modelSource);
		if (!f.good()) {
			SHARD3D_ERROR("Invalid model, file '{0}' not found", modelSource);
			return {};
		};
		builder.resourceSystem = resourceSystem;

		importInfo.importBones &= importInfo.skeletalMesh;
		importInfo.importMorphTargets &= importInfo.skeletalMesh;
		importInfo.importAnimations &= importInfo.skeletalMesh;

		importInfo.combineMeshes &= !importInfo.skeletalMesh;

		bool result = builder.loadScene(modelSource, modelDest.getAsset(), importInfo);

		if (result == false) {
			SHARD3D_ERROR("Failed to import 3D model ({0})! Scene importer failed!", modelSource);
			return {};
		}
		auto newTime = std::chrono::high_resolution_clock::now();
		
		std::vector<ModelInfo> modelInfos{};
		for (auto& mesh : builder.loadedMeshes) {
			if (mesh.submeshes.empty()) continue;
			std::string base;
			if (importInfo.combineMeshes) {
				base = modelDest.getAsset();
			} else {
				base = modelDest.getAsset().substr(0, modelDest.getAsset().find_last_of(".")) + "_" + mesh.meshName;
			}
			
			modelInfos.push_back(loadIndividualMesh(mesh, base + ".s3dmesh"));
			modelInfos.back().meshAsset = base + ".s3dmesh";
			if (importInfo.skeletalMesh) {
				exportSkeletalData(mesh, base + ".s3dskeleton");
				modelInfos.back().skeletonAsset = base + ".s3dskeleton";
			}
			if (importInfo.importAnimations) {
				for (int i = 0; i < mesh.animations.size(); i++) {
					std::string isolatedMeshName = base.substr(base.find_last_of("/") + 1);
					std::string animationFolder = base.substr(0, base.find_last_of("/")) + "/" + isolatedMeshName + "_animations";
					std::filesystem::create_directories(AssetID(animationFolder).getAbsolute());
					std::string animationAsset = animationFolder + "/" + mesh.animations[i].name + ".s3danimation";
					exportAnimationData(mesh, animationAsset, i);
					modelInfos.back().animationAssets.push_back(animationAsset);
				}
			}
		}

		SHARD3D_INFO("Duration of converting to s3dmesh (&& s3dskeleton && s3danimation(s)?) {0}: {1} ms", modelDest.getAsset(),
			std::chrono::duration<float, std::chrono::milliseconds::period>
			(std::chrono::high_resolution_clock::now() - newTime).count());

		return modelInfos;
	}	


	void ModelImporter::Builder::processBoneHierarchy(aiNode* node, MeshLoadData& toLoadMesh) {
		if (node->mParent) {
			if (std::string parent = node->mParent->mName.C_Str(); parent != std::string("RootNode") && parent != std::string("Armature") && parent != std::string("ROOT")) {
				toLoadMesh.bones.at(node->mName.C_Str()).parent = parent;
			}
		}
		for (int i = 0; i < node->mNumChildren; i++) {
			if (std::string(node->mName.C_Str()) != "Armature") {
				toLoadMesh.bones.at(node->mName.C_Str()).children.push_back(node->mChildren[i]->mName.C_Str());
			}
			processBoneHierarchy(node->mChildren[i], toLoadMesh);
		}
	}

	bool ModelImporter::Builder::loadScene(const std::string& filepath, const std::string& assetpath, Model3DImportInfo importInfo) {
		auto newTime = std::chrono::high_resolution_clock::now();

		Assimp::Importer importer;
		uint32_t processFlags =
			aiProcess_Triangulate |
			aiProcess_OptimizeMeshes |
			aiProcess_JoinIdenticalVertices |
			aiProcess_GenNormals |
			aiProcess_ImproveCacheLocality |
			aiProcess_FlipWindingOrder |
			aiProcess_FlipUVs |
			aiProcess_FindInvalidData |
			aiProcess_ValidateDataStructure;
		if (importInfo.combineMeshes) {
			processFlags |= aiProcess_PreTransformVertices;
		}
		if (importInfo.calculateTangents) {
			processFlags |= aiProcess_CalcTangentSpace;
		}
		if (importInfo.importBones) {
			processFlags |= aiProcess_LimitBoneWeights | aiProcess_PopulateArmatureData;
		}
		const aiScene* scene = importer.ReadFile(filepath, processFlags);
		
		if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
			SHARD3D_ERROR("Error importing file '{0}': {1}", filepath, importer.GetErrorString());
			return false;
		}
		loadedMeshes.clear();
		loadedMeshes.resize(1);

		workingDir = AssetID(assetpath.substr(0, assetpath.find_last_of("/")));
		originalDir = IOUtils::convertWinPathToUnixPath(filepath);
		originalDir = originalDir.substr(0, originalDir.find_last_of("/"));
		format = filepath.substr(filepath.find_last_of(".") + 1);

		processNode(scene->mRootNode, scene, importInfo, loadedMeshes[0]);
		if (importInfo.importBones) {
			for (auto& loadedMesh : loadedMeshes) {
				for (aiNode* armature : loadedMesh.uniqueArmatures) {
					processBoneHierarchy(armature, loadedMesh);
				}
			}
		}
		SHARD3D_INFO("Duration of processing 3D model {0}: {1} ms", filepath,
			std::chrono::duration<float, std::chrono::milliseconds::period>
			(std::chrono::high_resolution_clock::now() - newTime).count());
		return true;
	}

	void ModelImporter::Builder::processNode(aiNode* node, const aiScene* scene, Model3DImportInfo importInfo, MeshLoadData& toLoadMesh) {
		toLoadMesh.modelMatrix = TransformMath::YXZ(glm::radians(importInfo.importEulerRotation));
		toLoadMesh.normalMatrix = glm::inverse(glm::transpose(glm::mat3(toLoadMesh.modelMatrix)));
		toLoadMesh.modelMatrix[3] = { importInfo.importTranslation, 1.0f };

		toLoadMesh.meshName = node->mName.C_Str();

		// process all the node's meshes (if any)
		for (uint32_t i = 0; i < node->mNumMeshes; i++) {
			if (node->mParent) {
				if (const char* parent = node->mParent->mName.C_Str(); std::string(parent) != std::string("RootNode")) {
					loadedMeshes[0].rootMeshNode = node;
				}
			}
			aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
			loadSubmesh(mesh, scene, importInfo, toLoadMesh);
			if (importInfo.importAnimations) {
				toLoadMesh.animations.resize(scene->mNumAnimations);
				for (uint32_t i = 0; i < scene->mNumAnimations; i++) {
					aiAnimation* anim = scene->mAnimations[i];
					RiggedAnimationData& animation = toLoadMesh.animations[i];
					animation.name = anim->mName.length ? anim->mName.C_Str() : "animation";
					replaceIllegalChars(animation.name);
					double tps = anim->mTicksPerSecond == 0.0 ? 1.0 : anim->mTicksPerSecond;
					animation.duration = static_cast<float>(anim->mDuration / tps);
					for (int b = 0; b < anim->mNumChannels; b++) {
						aiNodeAnim* channel = anim->mChannels[b];
						std::string boneName = channel->mNodeName.C_Str();

						if (toLoadMesh.bones.find(boneName) == toLoadMesh.bones.end()) {
							toLoadMesh.bones[boneName].id = toLoadMesh.boneCount;
							toLoadMesh.boneCount++;
						}
						BoneAnimationData& boneAnimation = animation.boneAnimations[boneName];
						boneAnimation.translationKeyFrames.resize(channel->mNumPositionKeys);
						boneAnimation.translationKeyFrameTimeStamps.resize(channel->mNumPositionKeys);
						boneAnimation.quaternionKeyFrames.resize(channel->mNumRotationKeys);
						boneAnimation.quaternionKeyFrameTimeStamps.resize(channel->mNumRotationKeys);
						boneAnimation.scaleKeyFrames.resize(channel->mNumScalingKeys);
						boneAnimation.scaleKeyFrameTimeStamps.resize(channel->mNumScalingKeys);

						for (int k = 0; k < channel->mNumPositionKeys; k++) {
							boneAnimation.translationKeyFrames[k] = (glm::vec3&)channel->mPositionKeys[k].mValue;
							boneAnimation.translationKeyFrameTimeStamps[k] = static_cast<float>(channel->mPositionKeys[k].mTime / tps);
						}
						for (int k = 0; k < channel->mNumRotationKeys; k++) {
							boneAnimation.quaternionKeyFrames[k] = (glm::vec4&)channel->mRotationKeys[k].mValue;
							boneAnimation.quaternionKeyFrameTimeStamps[k] = static_cast<float>(channel->mRotationKeys[k].mTime / tps);
						}
						for (int k = 0; k < channel->mNumScalingKeys; k++) {
							boneAnimation.scaleKeyFrames[k] = (glm::vec3&)channel->mScalingKeys[k].mValue;
							boneAnimation.scaleKeyFrameTimeStamps[k] = static_cast<float>(channel->mScalingKeys[k].mTime / tps);
						}
					}
				}
			}
		}
		// then do the same for each of its children
		for (uint32_t i = 0; i < node->mNumChildren; i++) {
			if (importInfo.combineMeshes) {
				processNode(node->mChildren[i], scene, importInfo, toLoadMesh);
			} else {
				loadedMeshes.push_back({ node->mChildren[i] });
				processNode(node->mChildren[i], scene, importInfo, loadedMeshes.back());
			}
		}
	}

	void ModelImporter::Builder::loadSubmesh(aiMesh* mesh, const aiScene* scene, Model3DImportInfo importInfo, MeshLoadData& toLoadMesh) {
		const uint32_t index = mesh->mMaterialIndex;
		const aiMaterial* material = scene->mMaterials[index];
		std::string materialSlot = material->GetName().C_Str(); 
		float factor{ importInfo.scaleFactor };
		if (importInfo.combineMeshes) {
			if (this->format == "fbx") factor *= 0.01f; // fbx unit is in cm for some reason when pretransformvertices is turned on 
		}
		if (this->format == "gltf") materialSlot = "material_" + std::to_string(index); // gltf does not guarantee a material name
		
		replaceIllegalChars(materialSlot);

		if (toLoadMesh.submeshes.find(index) == toLoadMesh.submeshes.end()) { // create new slot if none exist
			SubmeshData submesh{};
			toLoadMesh.slotToIndex[materialSlot] = index;
			toLoadMesh.submeshes[index] = submesh;

			SHARD3D_LOG("Creating primitive data for slot {0} with index {1}", materialSlot, index);
		}
		
		for (uint32_t i = 0; i < mesh->mNumVertices; i++) {
			Vertex vertex{};
			vertex.position = toLoadMesh.modelMatrix * glm::vec4{
				factor * mesh->mVertices[i].x,
				factor * mesh->mVertices[i].y,
				factor * mesh->mVertices[i].z,
				1.0f
			};
			toLoadMesh.volumeBounds.setAndCheck(vertex.position);
			toLoadMesh.sphereBounds.setAndCheck(vertex.position);

			toLoadMesh.submeshes.at(index).volumeBounds.setAndCheck(vertex.position);
			toLoadMesh.submeshes.at(index).sphereBounds.setAndCheck(vertex.position);
			vertex.normal = toLoadMesh.normalMatrix * glm::vec3{
				mesh->mNormals[i].x,
				mesh->mNormals[i].y,
				mesh->mNormals[i].z
			};
			if (mesh->mTangents)
				vertex.tangent = toLoadMesh.normalMatrix * glm::vec3{
					mesh->mTangents[i].x,
					mesh->mTangents[i].y,
					mesh->mTangents[i].z
				};
			else {
				vertex.tangent = toLoadMesh.normalMatrix * glm::vec3{ // pain
					vertex.normal.x,
					vertex.normal.y,
					vertex.normal.z
				};
			}
			if (mesh->mTextureCoords[0])
				vertex.uv = {
					mesh->mTextureCoords[0][i].x,
					mesh->mTextureCoords[0][i].y
				};
			else vertex.uv = { 0.f, 0.f };

			toLoadMesh.submeshes[index].vertices.push_back(vertex);
		}


		toLoadMesh.submeshes[index].indices.reserve(mesh->mNumFaces * 3u);
		for (uint32_t i = 0; i < mesh->mNumFaces; i++) {
			aiFace face = mesh->mFaces[i];
			for (uint32_t j = 0; j < face.mNumIndices; j++)
				toLoadMesh.submeshes[index].indices.push_back(face.mIndices[j]);
		}

		if (importInfo.importMorphTargets) {
			for (uint32_t i = 0; i < mesh->mNumAnimMeshes; i++) {
				std::string morphTargetName = mesh->mAnimMeshes[i]->mName.C_Str();
				toLoadMesh.morphTargets[index][morphTargetName] = {};
				RiggedMorphData& morphTarget = toLoadMesh.morphTargets[index][morphTargetName];
				morphTarget.id = i;
				for (uint32_t j = 0; j < mesh->mAnimMeshes[i]->mNumVertices; j++) {
					Vertex vertex{}; 
					vertex.position = toLoadMesh.modelMatrix * glm::vec4{
						factor * mesh->mAnimMeshes[i]->mVertices[j].x,
						factor * mesh->mAnimMeshes[i]->mVertices[j].y,
						factor * mesh->mAnimMeshes[i]->mVertices[j].z,
						1.0f
					};
					
					morphTarget.volumeBounds.setAndCheck(vertex.position);
					morphTarget.sphereBounds.setAndCheck(vertex.position);

		
					//SHARD3D_LOG("({0}, {1}, {2})\t\t({3}, {4}, {5})",
					//	vertex.position.x, vertex.position.y, vertex.position.z,
					//	 toLoadMesh.submeshes[index].vertices[j].position.x, toLoadMesh.submeshes[index].vertices[j].position.y, toLoadMesh.submeshes[index].vertices[j].position.z
					//);

					// convert to vector for compression
					vertex.position -= toLoadMesh.submeshes[index].vertices[j].position;

					vertex.normal = toLoadMesh.normalMatrix * glm::vec3{
						 mesh->mAnimMeshes[i]->mNormals[j].x,
						 mesh->mAnimMeshes[i]->mNormals[j].y,
						 mesh->mAnimMeshes[i]->mNormals[j].z
					};
					if (mesh->mAnimMeshes[i]->mTangents)
						vertex.tangent = toLoadMesh.normalMatrix * glm::vec3{
							mesh->mAnimMeshes[i]->mTangents[j].x,
							mesh->mAnimMeshes[i]->mTangents[j].y,
							mesh->mAnimMeshes[i]->mTangents[j].z
					};
					else {
						vertex.tangent = toLoadMesh.normalMatrix * glm::vec3{
							mesh->mTangents[i].x,
							mesh->mTangents[i].y,
							mesh->mTangents[i].z
						};
					}
					if (mesh->mAnimMeshes[i]->mTextureCoords[0])
						vertex.uv = {
							mesh->mAnimMeshes[i]->mTextureCoords[0][j].x,
							mesh->mAnimMeshes[i]->mTextureCoords[0][j].y
					};
					else vertex.uv = {
						mesh->mTextureCoords[0][i].x,
						mesh->mTextureCoords[0][i].y
					};

					morphTarget.targetVertices.push_back(vertex);
				}
			}
		}
		if (importInfo.importBones) {
			for (uint32_t i = 0; i < mesh->mNumBones && i < 254; i++) {
				int boneID = 0xFF;
				if (std::find(toLoadMesh.uniqueArmatures.begin(), toLoadMesh.uniqueArmatures.end(), mesh->mBones[i]->mArmature) == toLoadMesh.uniqueArmatures.end()) {
					toLoadMesh.uniqueArmatures.push_back(mesh->mBones[i]->mArmature);
				}
				std::string boneName = mesh->mBones[i]->mName.C_Str();
				if (toLoadMesh.bones.find(boneName) == toLoadMesh.bones.end()) {
					toLoadMesh.bones[boneName] = {};
					RiggedBoneData& bone = toLoadMesh.bones[boneName];
					boneID = toLoadMesh.boneCount;
					bone.id = boneID;
					bone.offset = (glm::mat4&)mesh->mBones[i]->mOffsetMatrix.Transpose(); // convert row major to column major

					toLoadMesh.boneCount++;
				} else {
					boneID = toLoadMesh.bones.at(boneName).id;
				}
				for (uint32_t j = 0; j < mesh->mBones[i]->mNumWeights; j++) {
					for (int k = 0; k < MAX_VERTEX_BONES; k++) {
						VertexSkin& sv = toLoadMesh.skins[mesh->mBones[i]->mWeights[j].mVertexId];
					//	if (sv.bones[k] == boneID) break; 
						if (sv.weights[k] == 0) {
							sv.bones[k] = boneID;
							sv.weights[k] = mesh->mBones[i]->mWeights[j].mWeight * 255; // scale to normalized unsigned uint8
							break;
						}
					}
				}
				
			}
		}
		std::string materialName = strUtils::removeIllegalPathChars(materialSlot);
		if (importInfo.createMaterials) {
			aiColor4D color{ 1.f, 1.f, 1.f, 1.f };
			aiGetMaterialColor(material, AI_MATKEY_COLOR_DIFFUSE, &color);
			float specular{ 0.5f };
			aiGetMaterialFloat(material, AI_MATKEY_SPECULAR_FACTOR, &specular);
			float glossiness{ 0.5f };
			float reflectivity{ 0.f };
			if (importInfo.convertPBRtoSRM3D) {
				float roughness{ 0.f };
				aiGetMaterialFloat(material, AI_MATKEY_ROUGHNESS_FACTOR, &roughness);
				float metallic{ 0.f };
				aiGetMaterialFloat(material, AI_MATKEY_METALLIC_FACTOR, &metallic);
				specular = 0.5f;
				// convert roughness into glossiness
				glossiness = 1.0f - roughness;
				// convert specular, glossiness, and metallic into reflectivity
				float ss = std::max(specular * glossiness * pow((1.01f - pow(roughness, 1.f / 2.78f)) / 1.01f, 1.05f), 0.01f);
				reflectivity = pow(((ss + metallic * 0.5f) / ss), ss * metallic) * ss * metallic * 0.5f + pow(ss, 8.f) * 0.25f;	
			}
			else {
				aiGetMaterialFloat(material, AI_MATKEY_SHININESS, &glossiness);
				aiGetMaterialFloat(material, AI_MATKEY_REFLECTIVITY, &reflectivity);
			}
			float opacity{ 1.f };
			aiGetMaterialFloat(material, AI_MATKEY_OPACITY, &opacity);
			int doubleSided{ false };
			aiGetMaterialInteger(material, AI_MATKEY_TWOSIDED, &doubleSided);
			aiColor4D emission{ 0.f, 0.f, 0.f, 0.f };
			aiGetMaterialColor(material, AI_MATKEY_COLOR_EMISSIVE, &emission);

			rPtr<SurfaceMaterial> grid_material = make_rPtr<SurfaceMaterial>(resourceSystem);
			grid_material->diffuseColor = { color.r, color.g, color.b };
			grid_material->specular = specular;
			grid_material->glossiness = glossiness;
			grid_material->reflectivity = reflectivity;
			grid_material->setDoubleSided(doubleSided);
			if (opacity != 1.f) {
				grid_material->setTranslucent(true);
				grid_material->opacity = 1.f;
			}
			if (*reinterpret_cast<aiColor3D*>(&emission) != aiColor3D{ 0.f, 0.f, 0.f }) {
				glm::vec3 pureEmission = glm::normalize(*reinterpret_cast<glm::vec3*>(&emission));
				float emissionStrength = (emission.a > 0.f ? emission.a : 1.f) * glm::length(*reinterpret_cast<glm::vec3*>(&emission));
				grid_material->emissiveColor = glm::vec4(*reinterpret_cast<glm::vec3*>(&emission), emissionStrength);
			}

			// TODO: rewrite all this garbage to write to KTX directly instead
			if (aiString diffuseTex{}; aiGetMaterialTexture(material, aiTextureType_DIFFUSE, 0, &diffuseTex) == AI_SUCCESS) {
				std::string texture = originalDir + "/" + diffuseTex.C_Str();
				if (const aiTexture* texData = scene->GetEmbeddedTexture(diffuseTex.C_Str()); texData) { //embedded texture, get index from string and access scene->mTextures
					texture = workingDir.getAbsolute() + "/" + IOUtils::getFileOnly(diffuseTex.C_Str()) + "_"  + materialName + "_diffuse.bmp";
					void* px{};
					int w, h, c{};
					if (texData->mHeight == 0) 
						px = ImageResource::getSTBImage(texData->pcData, texData->mWidth, &w, &h, &c, 0);
					else
						px = ImageResource::getSTBImage(texData->pcData, texData->mWidth * texData->mHeight, &w, &h, &c, 0);
					ImageResource::storeImage(px, w, h, c, texture);
				}
				if (IOUtils::doesFileExist(texture)) {		
					TextureLoadInfo tlif = {};
					tlif.enableMipMaps = true;
					tlif.compression = TextureCompression::BC7;
					tlif.channels = TextureChannels::RGB;
					tlif.colorSpace = TextureColorSpace::sRGB;
					TextureImportInfo timp = {};
					grid_material->diffuseTex = AssetManager::importTexture2D(texture, workingDir.getAbsolute() + "/" + diffuseTex.C_Str(), resourceSystem, timp, tlif);
				}
			}
	

			if (importInfo.convertPBRtoSRM3D) {
				if (aiString roughnessTex{}; aiGetMaterialTexture(material, aiTextureType_DIFFUSE_ROUGHNESS, 0, &roughnessTex) == AI_SUCCESS) {
					std::string texture = originalDir + "/" + roughnessTex.C_Str();
					std::string dest = workingDir.getAbsolute() + "/" + roughnessTex.C_Str() + ".bmp";
					std::string imptex = texture;
					bool eTex = false;
					if (const aiTexture* texData = scene->GetEmbeddedTexture(roughnessTex.C_Str()); texData) { //embedded texture
						texture = workingDir.getAbsolute() + "/" + IOUtils::getFileOnly(roughnessTex.C_Str()) + "_" + materialName + "_specular.bmp";
						void* px{};
						int w, h, c{};
						if (texData->mHeight == 0)
							px = ImageResource::getSTBImage(texData->pcData, texData->mWidth, &w, &h, &c, 0);
						else
							px = ImageResource::getSTBImage(texData->pcData, texData->mWidth * texData->mHeight, &w, &h, &c, 0);
						ImageResource::storeImage(px, w, h, c, texture + ".temprough.bmp");
						eTex = true;
						dest = texture;
						imptex = texture + ".temprough.bmp";
					}
					if (IOUtils::doesFileExist(texture)) {
						TextureLoadInfo tlif = {};
						tlif.enableMipMaps = false;
						tlif.compression = TextureCompression::Lossless;
						tlif.channels = TextureChannels::RGBA;
						tlif.colorSpace = TextureColorSpace::Linear;
						Texture2D* t2d = TextureImporter::createTexture2DFromFile(resourceSystem->engineDevice, resourceSystem->getDescriptor(), tlif, resourceSystem, imptex);
						if (eTex) {
							std::remove((texture + ".temprough.bmp").c_str());
						}
						SimpleProcessingSystem textureProcess{
							resourceSystem->engineDevice, 
							S3DShader(ShaderType::Pixel,
							"resources/shaders/editor/pbr_roughness_specular_processing.frag", "editor"),
							&t2d->getImageInfo(),
							glm::vec2{t2d->getExtent().width,t2d->getExtent().height},
							VK_FORMAT_R8G8B8A8_UNORM
						};
						auto cmb = resourceSystem->engineDevice.beginSingleTimeCommands();
						textureProcess.process(cmb);
						resourceSystem->engineDevice.endSingleTimeCommands(cmb);
						auto& attachment = textureProcess.getWrittenAttachment();
						VkBuffer copyBuffer;
						VkDeviceMemory copyBufferMemory;
						void* imgb{};
						size_t imageSize = static_cast<size_t>(t2d->getExtent().width) * t2d->getExtent().height * 4;
						resourceSystem->engineDevice.createBuffer(
							imageSize,
							VK_BUFFER_USAGE_TRANSFER_DST_BIT,
							VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
							copyBuffer,
							copyBufferMemory
						);
						vkMapMemory(resourceSystem->engineDevice.device(), copyBufferMemory, 0, imageSize, 0, &imgb);
						VkCommandBuffer commandBuffer = resourceSystem->engineDevice.beginSingleTimeCommands();
						resourceSystem->engineDevice.copyImageToBuffer(commandBuffer,attachment->getImage(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, copyBuffer, t2d->getExtent().width, t2d->getExtent().height, 1, 0);
						resourceSystem->engineDevice.endSingleTimeCommands(commandBuffer);
						TextureImportInfo timp = {};
						grid_material->specularTex = AssetManager::importTexture2D(imgb, imageSize, { t2d->getExtent().width , t2d->getExtent().height }, dest, resourceSystem, timp, tlif);

						vkUnmapMemory(resourceSystem->engineDevice.device(), copyBufferMemory);
						vkDestroyBuffer(resourceSystem->engineDevice.device(), copyBuffer, nullptr);
						vkFreeMemory(resourceSystem->engineDevice.device(), copyBufferMemory, nullptr);
						delete t2d;
					}
				}	
			}
			else if (aiString specularTex{}; aiGetMaterialTexture(material, aiTextureType_SPECULAR, 0, &specularTex) == AI_SUCCESS) {
				std::string texture = originalDir + "/" + specularTex.C_Str();
				std::string dest = workingDir.getAbsolute() + "/" + specularTex.C_Str();
				if (const aiTexture* texData = scene->GetEmbeddedTexture(specularTex.C_Str()); texData) { //embedded texture
					texture = workingDir.getAbsolute() + "/" + IOUtils::getFileOnly(specularTex.C_Str()) + "_" + materialName + "_specular.bmp";
					void* px{};
					int w, h, c{};
					if (texData->mHeight == 0)
						px = ImageResource::getSTBImage(texData->pcData, texData->mWidth, &w, &h, &c, 0);
					else
						px = ImageResource::getSTBImage(texData->pcData, texData->mWidth * texData->mHeight, &w, &h, &c, 0);
					ImageResource::storeImage(px, w, h, c, texture);
					dest = texture;
				}
				if (IOUtils::doesFileExist(texture)) {
					TextureLoadInfo tlif = {};
					tlif.enableMipMaps = true;
					tlif.compression = TextureCompression::BC4;
					tlif.channels = TextureChannels::R;
					tlif.colorSpace = TextureColorSpace::Linear;
					TextureImportInfo timp = {};
					grid_material->specularTex = AssetManager::importTexture2D(texture, workingDir.getAbsolute() + "/" + specularTex.C_Str(), resourceSystem, timp, tlif);
				}
			}
			
			if (aiString normalTex{}; aiGetMaterialTexture(material, aiTextureType_NORMALS, 0, &normalTex) == AI_SUCCESS) {
				std::string texture = originalDir + "/" + normalTex.C_Str();
				std::string dest = workingDir.getAbsolute() + "/" + normalTex.C_Str();
				if (const aiTexture* texData = scene->GetEmbeddedTexture(normalTex.C_Str()); texData) { //embedded texture
					texture = workingDir.getAbsolute() + "/" + IOUtils::getFileOnly(normalTex.C_Str()) + "_" + materialName + "_normalvk.bmp";
					void* px{};
					int w, h, c{};
					if (texData->mHeight == 0)
						px = ImageResource::getSTBImage(texData->pcData, texData->mWidth, &w, &h, &c, 0);
					else
						px = ImageResource::getSTBImage(texData->pcData, texData->mWidth * texData->mHeight, &w, &h, &c, 0);
					ImageResource::storeImage(px, w, h, c, texture);
					dest = texture;
				}
				if (IOUtils::doesFileExist(texture)) {
					TextureLoadInfo tlif = {};
					tlif.enableMipMaps = true;
					tlif.compression = TextureCompression::BC5;
					tlif.channels = TextureChannels::RG;
					tlif.colorSpace = TextureColorSpace::Linear;
					TextureImportInfo timp = {};
					grid_material->normalTex = AssetManager::importTexture2D(texture, workingDir.getAbsolute() + "/" + normalTex.C_Str(), resourceSystem, timp, tlif);
				}
			}
			
			if (aiString maskTex{}; aiGetMaterialTexture(material, aiTextureType_OPACITY, 0, &maskTex) == AI_SUCCESS) {
				std::string texture = originalDir + "/" + maskTex.C_Str();
				std::string dest = workingDir.getAbsolute() + "/" + maskTex.C_Str();
				if (const aiTexture* texData = scene->GetEmbeddedTexture(maskTex.C_Str()); texData) { //embedded texture
					texture = workingDir.getAbsolute() + "/" + IOUtils::getFileOnly(maskTex.C_Str()) + "_" + materialName + "_opacity.bmp";
					void* px{};
					int w, h, c{};
					if (texData->mHeight == 0)
						px = ImageResource::getSTBImage(texData->pcData, texData->mWidth, &w, &h, &c, 0);
					else
						px = ImageResource::getSTBImage(texData->pcData, texData->mWidth * texData->mHeight, &w, &h, &c, 0);
					ImageResource::storeImage(px, w, h, 3, texture);
					dest = texture;
				}
				if (IOUtils::doesFileExist(texture)) {
					TextureLoadInfo tlif = {};
					tlif.enableMipMaps = false;
					tlif.compression = TextureCompression::BC4;
					tlif.channels = TextureChannels::R;
					tlif.colorSpace = TextureColorSpace::Linear;
					TextureImportInfo timp = {};
					grid_material->maskTex = AssetManager::importTexture2D(texture, workingDir.getAbsolute() + "/" + maskTex.C_Str(), resourceSystem, timp, tlif);
				}
			}
			AssetManager::createMaterial(workingDir.getAbsolute() + "/" + materialName + ".s3dasset", grid_material.get());
		}
		AssetID m_asset = ResourceSystem::coreAssets.sm_errorMaterial;
		if (IOUtils::doesFileExist(workingDir.getAbsolute() + "/"  + materialName + ".s3dasset")) {
			m_asset = AssetID(workingDir.getAsset() + "/"  + materialName + ".s3dasset");
		}

		toLoadMesh.submeshes[index].materialAsset = m_asset;
	}
}
