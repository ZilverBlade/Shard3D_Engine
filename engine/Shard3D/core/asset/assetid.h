#pragma once
#include <string>
#include <xhash>
#include "../../systems/handlers/project_system.h"

namespace Shard3D {
	using AssetKey = unsigned long long;

	class AssetID {
	public:
		AssetID() : assetFile("\0"), assetID(0) {}
		AssetID(const std::string& asset);
		static AssetID null() { return AssetID(); }
		// Returned hashed value
		inline uint64_t getID() const { return assetID; }
		// Returns absolute location of file (assumes asset is relative to the project directory)
		inline std::string getAbsolute() const { return ProjectSystem::getAssetLocation() + assetFile; }
		// Returns asset file (.s3dasset)
		inline std::string getAsset() const { return assetFile; }

		inline operator AssetKey() const { return getID(); }
		inline operator bool() const { return assetID != 0; }
		inline bool operator==(const AssetID& other) const { return assetID == other.assetID; }
		inline bool operator!=(const AssetID& other) const { return assetID != other.assetID; }

		//AssetID(const AssetID& other) : assetFile(other.assetFile), assetID(other.assetID) {}
		//AssetID& operator=(const AssetID& other) { return AssetID(other.assetFile, other.assetID); }
	private:
		AssetID(const std::string& asset, uint64_t prehash) : assetFile(asset), assetID(prehash) {}
		std::string assetFile;
		uint64_t assetID;
	};
}
namespace std {
	template<>
	struct hash<Shard3D::AssetID> {
		size_t operator()(const Shard3D::AssetID& id) const {
			return id.getID();
		}
	};
}