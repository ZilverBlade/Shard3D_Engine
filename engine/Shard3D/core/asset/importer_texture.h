#pragma once
#include "../../vulkan_abstr.h"
#include "../asset/assetid.h"
#include "texture.h"
namespace Shard3D {
	inline namespace Resources {
		class Texture2D;
		struct TextureImportInfo {
			bool flipNormalMapY = false;
			bool isCubemap = false;
			bool generateCubemapGlossinessMap = false;
			TextureCubeFormat cubeFormat = TextureCubeFormat::Cube;
		};

		class TextureImporter {
		public:
			// loads ktx texture, compresses and generates mip maps if enabled
			static ktxTexture2* loadTextureKTX(
				S3DDevice& device,
				ResourceSystem* resourceSystem,
				TextureLoadInfo loadInfo,
				AssetID ktxLocation
			);
			// loads ktx texture, pre-compressed and loads mip maps
			//static ktxTexture2* loadCachedTextureKTX(
			//	const char* cacheloc
			//);
			static ktxTexture2* loadCompressedCachedTextureKTX(
				const char* cacheloc
			);
			//static bool exportTextureKTX(
			//	ktxTexture2* textureData,
			//	const char* location
			//);
			static bool exportCompressedTextureKTX(
				ktxTexture2* textureData, 
				const char* location,
				int compressionLevel = 14
			);
			static void freeKTX(
				ktxTexture2* texture
			);
			static void importTextureFromFile(
				TextureImportInfo importInfo,
				ResourceSystem* resourceSystem,
				const std::vector<std::string>& textureSources, // multiple textures if is a cubemap
				AssetID ktxDest,
				int compressionLevel = 14
			);
			static void importTextureFromMemory(
				TextureImportInfo importInfo,
				ResourceSystem* resourceSystem,
				const void* textureMemory,
				VkFormat format,
				int width,
				int height,
				size_t textureSizeBytes,
				AssetID ktxDest,
				int compressionLevel = 14
			);

			static Texture2D* createTexture2DFromFile(
				S3DDevice& device,
				uPtr<SmartDescriptorSet>& bindless,
				TextureLoadInfo loadInfo,
				ResourceSystem* resourceSystem,
				const std::string& textureSource
			);
		};
	}
}
