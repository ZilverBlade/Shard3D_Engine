#include "../../s3dpch.h" 
#include "texture2d.h"
#include <ktx.h>
#include <ktxvulkan.h>

namespace Shard3D::Resources {
	Texture2D::Texture2D(S3DDevice& device, uPtr<SmartDescriptorSet>& indexedDescriptor, ktxTexture2* texture, TextureLoadInfo loadInfo)
		: Texture(device, indexedDescriptor, texture, loadInfo)  {
		createTextureImage(texture);
		createTextureImageView(VK_IMAGE_VIEW_TYPE_2D, loadInfo.channels);
		createTextureSampler();
		updateDescriptor();

		shaderResouceIndex = bindless->allocateIndex(BINDLESS_SAMPLER_BINDING);
		SmartDescriptorWriter(*bindless)
			.writeImage(BINDLESS_SAMPLER_BINDING, shaderResouceIndex, &mDescriptor)
			.build();
	}

	void Texture2D::createTextureImage(ktxTexture2* texture_) {  
		ktxTexture2* texture;
		bool allowMips = ProjectSystem::getGraphicsSettings().texture.mipMapping;
		bool dontCopyFirstMip = allowMips || mLoadInfo.enableMipMaps == false;
		if (dontCopyFirstMip) { // texture should not load with mip maps anyway if the texture properties say so, this is to avoid unnecessary copies
			mMipLevels = texture_->numLevels;
			texture = texture_;
		} else {
			ktxTextureCreateInfo txCreateInfo{};
			memset(&txCreateInfo, 0, sizeof(ktxTextureCreateInfo));
			txCreateInfo.baseWidth = texture_->baseWidth;
			txCreateInfo.baseHeight = texture_->baseHeight;
			txCreateInfo.baseDepth = 1;
			txCreateInfo.generateMipmaps = KTX_FALSE;
			txCreateInfo.vkFormat = texture_->vkFormat;
			txCreateInfo.isArray = KTX_FALSE;
			txCreateInfo.numLevels = 1;
			txCreateInfo.numLayers = 1;
			txCreateInfo.numFaces = 1;
			txCreateInfo.numDimensions = 2;
			ktxResult cErr = ktxTexture2_Create(&txCreateInfo, KTX_TEXTURE_CREATE_ALLOC_STORAGE, &texture);
			ktx_uint8_t* copyMemAddr = texture_->pData;
			copyMemAddr += (texture_->dataSize - texture->dataSize);
			ktxResult sifmErr = ktxTexture_SetImageFromMemory(ktxTexture(texture), 0, 0, 0, copyMemAddr, texture->dataSize);
			SHARD3D_ASSERT(sifmErr == KTX_SUCCESS);
			mMipLevels = 1;
		}

		mTextureLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		S3DQueue* queue = mDevice.getAvailableQueue(QueueType::Graphics);
		ktxVulkanDeviceInfo* dvcInfo = ktxVulkanDeviceInfo_Create(mDevice.physicalDevice(), mDevice.device(), queue->queue, mDevice.getCommandPool(), nullptr);
		ktxVulkanTexture ktxVkTex{};
		ktxVkTex.depth = 1;
		ktxVkTex.width = texture->baseWidth;
		ktxVkTex.height = texture->baseHeight;
		ktxVkTex.imageFormat = mFormat;
		ktxVkTex.imageLayout = mTextureLayout;
		ktxVkTex.viewType = VK_IMAGE_VIEW_TYPE_2D;
		ktxVkTex.levelCount = mMipLevels;

		auto erc = ktxTexture2_VkUpload(texture, dvcInfo, &ktxVkTex);
		SHARD3D_ASSERT(erc == KTX_SUCCESS);
		mTextureImage = ktxVkTex.image;
		mTextureImageMemory = ktxVkTex.deviceMemory;

		ktxVulkanDeviceInfo_Destroy(dvcInfo);
		mDevice.freeAvailableQueue(queue);

		memorySize = texture_->dataSize;

		if (!dontCopyFirstMip) {
			ktxTexture_Destroy(ktxTexture(texture));
		}
	}

}