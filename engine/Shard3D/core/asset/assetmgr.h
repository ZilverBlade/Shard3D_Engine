#pragma once

#include "../../systems/handlers/resource_system.h"
#include "physics_asset.h"
#include "animation.h"

namespace Shard3D {
	class AssetManager {
	public:
		static AssetID importTexture2D(const std::string& sourcepath, const std::string& destpath, ResourceSystem* rSys, TextureImportInfo importInfo, TextureLoadInfo loadInfo, PackageInvocationIndex = PackageInvocationIndex());
		static AssetID importTexture2D(const void* data, size_t textureSize, glm::ivec2 resolution, const std::string& destpath, ResourceSystem* rSys, TextureImportInfo importInfo, TextureLoadInfo loadInfo, PackageInvocationIndex = PackageInvocationIndex());
		static AssetID modifyTexture2D(const AssetID& asset, TextureLoadInfo loadInfo);
		static AssetID importTextureCube(const std::vector<std::string>& sourcetextures, const std::string& destpath, ResourceSystem* rSys, TextureImportInfo importInfo, TextureLoadInfo loadInfo, PackageInvocationIndex = PackageInvocationIndex());
		static AssetID importTextureCube(const void* data, size_t textureSize, glm::ivec2 resolution, const std::string& destpath, ResourceSystem* rSys, TextureImportInfo importInfo, TextureLoadInfo loadInfo, PackageInvocationIndex = PackageInvocationIndex());
		static std::vector<AssetID> importMeshes(const std::string& sourcepath, const std::string& destpath, ResourceSystem* rSys, Model3DImportInfo info, PackageInvocationIndex = PackageInvocationIndex());
		static AssetID modifyMesh(const AssetID& asset, Resources::Mesh3D* model);
		static AssetID createMaterial(const std::string& destpath, SurfaceMaterial* material);

		static AssetID createPhysicsAsset(const std::string& destpath, const PhysicsAsset& asset);
		static PhysicsAsset loadPhysicsAsset(const AssetID& asset);
		static sPtr<SkeletalAnimation> loadSkeletalAnimation(const AssetID& asset);

		static AssetID exportBakedReflectionCube(ktxTexture2* textureData, const std::string& destpath, PackageInvocationIndex = PackageInvocationIndex());

		static bool reimportAsset(const AssetID& asset);
		static void renameVirtualResource(AssetType type, const AssetID& oldAsset, const AssetID& newAsset);

		static void purgeAsset(const AssetID& asset);
	private:
		friend class _special_assets;
	}; 
}