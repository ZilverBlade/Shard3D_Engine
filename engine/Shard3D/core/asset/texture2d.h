#pragma once
#include "texture.h"

namespace Shard3D {
    inline namespace Resources {
        class Texture2D : public Texture {
        public:
            Texture2D(S3DDevice& device, uPtr<SmartDescriptorSet>& indexedDescriptor, ktxTexture2* texture, TextureLoadInfo loadInfo);
            ~Texture2D() {}
        protected:
            virtual void createTextureImage(ktxTexture2* texture) override;
        };
    }
}