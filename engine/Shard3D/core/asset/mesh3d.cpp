#include "mesh3d.h"
#include "../../systems/handlers/vertex_arena_allocator.h"

namespace Shard3D::Resources {
	Mesh3D::Mesh3D(S3DDevice& dvc, VertexArenaAllocator* allocator, const MeshRawData* data, const std::vector<AssetID>& default_materials) : device(dvc), vertexAllocator(allocator) {
		volumeBounds = data->entireBounds;
		sphereBounds = data->entireSphereBounds;
		 
		createBuffers(data);
		
		SHARD3D_ENSURE(default_materials.size() == data->submeshCount, "Material count does not match submesh count!");
		for (int i = 0; i < data->submeshCount; i++) {
			materialSlots.push_back(std::string(data->materialNames[i]));
			materials.push_back(default_materials[i]);
			submeshes[i].volumeBounds = data->submeshes[i]->bounds;

			SHARD3D_LOG("Creating buffer for slot {0} with index {1}", data->materialNames[i], i);
		}
	}

	Mesh3D::~Mesh3D() {
		vertexAllocator->deallocate(meshID);
		SHARD3D_LOG("Destroying (3D) Mesh {0}", (void*)this);
	}

	void Mesh3D::createBuffers(const MeshRawData* data) {
		uint32_t fullVertexCount{};
		for (int i = 0; i < data->submeshCount; i++) {
			fullVertexCount += data->submeshes[i]->vtxCount;
		}
		vtxCount = fullVertexCount;
		
		VTX_pos_offset_12b_* vertexDataPos  = reinterpret_cast<VTX_pos_offset_12b_*>(malloc(fullVertexCount * sizeof(VTX_pos_offset_12b_)));
		VTX_norm_cmpr_offset_4b_* vertexDataNorm  = reinterpret_cast<VTX_norm_cmpr_offset_4b_*>(malloc(fullVertexCount * sizeof(VTX_norm_cmpr_offset_4b_)));
		VTX_tg_cmpr_offset_4b_* vertexDataTg = reinterpret_cast<VTX_tg_cmpr_offset_4b_*>(malloc(fullVertexCount * sizeof(VTX_tg_cmpr_offset_4b_)));
		VTX_uv_cmpr_offset_4b_* vertexDataUV = reinterpret_cast<VTX_uv_cmpr_offset_4b_*>(malloc(fullVertexCount * sizeof(VTX_uv_cmpr_offset_4b_)));

		uint32_t vxoffset = 0;
		submeshes.resize(data->submeshCount);
		for (int i = 0; i < data->submeshCount; i++) {
			uint32_t vertexCount = data->submeshes[i]->vtxCount;
			SHARD3D_ENSURE(vertexCount >= 3, "Vertex count must be at least 3");
			submeshes[i].vertexCount = vertexCount;

			size_t seekOffsetPos = (size_t)vertexDataPos + vxoffset * sizeof(VTX_pos_offset_12b_);
			size_t seekOffsetNrm = (size_t)vertexDataNorm + vxoffset * sizeof(VTX_norm_cmpr_offset_4b_);
			size_t seekOffsetTg = (size_t)vertexDataTg + vxoffset * sizeof(VTX_tg_cmpr_offset_4b_);
			size_t seekOffsetUV = (size_t)vertexDataUV + vxoffset * sizeof(VTX_uv_cmpr_offset_4b_);
			memcpy((void*)seekOffsetPos, data->submeshes[i]->vtxPositions, vertexCount * sizeof(VTX_pos_offset_12b_));
			memcpy((void*)seekOffsetNrm, data->submeshes[i]->vtxNormals, vertexCount * sizeof(VTX_norm_cmpr_offset_4b_));
			memcpy((void*)seekOffsetTg, data->submeshes[i]->vtxTangents, vertexCount * sizeof(VTX_tg_cmpr_offset_4b_));
			memcpy((void*)seekOffsetUV, data->submeshes[i]->vtxUVs[0], vertexCount * sizeof(VTX_uv_cmpr_offset_4b_));		 // only support 1 uv map atm
			
			vxoffset += vertexCount;

			SHARD3D_VERBOSE("Mesh {0} pushing back vertex buffer ({1}) with {2} vertices", (void*)this, i, vertexCount);
			if (i == 0) continue; // no need to set an offset for the first submesh
			uint32_t vertexOffset = submeshes[i - 1].vertexOffset + submeshes[i - 1].vertexCount;
			submeshes[i].vertexOffset = vertexOffset;
			SHARD3D_VERBOSE("Mesh {0} vertex buffer ({1}) vertex offset {2}", (void*)this, i, vertexOffset);
		}

		uint32_t fullIndexCount{};
		for (int i = 0; i < data->submeshCount; i++) {
			fullIndexCount += data->submeshes[i]->indCount;
		}
		idxCount = fullIndexCount;

		uint32_t* indexData = reinterpret_cast<uint32_t*>(malloc(fullIndexCount * sizeof(uint32_t)));

		
		uint32_t ixoffset = 0;
		for (int i = 0; i < data->submeshCount; i++) {
			uint32_t indexCount = data->submeshes[i]->indCount;
			submeshes[i].indexCount = indexCount;
			size_t seekOffset = (size_t)indexData + ixoffset * sizeof(uint32_t);
			memcpy((void*)seekOffset, data->submeshes[i]->vtxIndices, indexCount * sizeof(uint32_t));
			ixoffset += indexCount;
			// fast copy
		
			SHARD3D_VERBOSE("Mesh {0} pushing back index buffer ({1}) with {2} indices", (void*)this, i, indexCount);
			if (i == 0) continue; // no need to set an offset for the first submesh
			uint32_t indexOffset = submeshes[i - 1].indexOffset + submeshes[i - 1].indexCount;
			submeshes[i].indexOffset = indexOffset;
			SHARD3D_VERBOSE("Mesh {0} index buffer ({1}) index offset {2}", (void*)this, i, indexOffset);
		}

		hasIndexBuffer = fullIndexCount > 0;

		meshID = vertexAllocator->allocate(fullVertexCount, fullIndexCount);

		VertexDataInfo vtxData{};
		vtxData.position = vertexDataPos;
		vtxData.uv = vertexDataUV;
		vtxData.normal = vertexDataNorm;
		vtxData.tangent = vertexDataTg;

		vertexAllocator->write(meshID, vtxData, indexData);

		std::free(vertexDataPos);
		std::free(vertexDataNorm);
		std::free(vertexDataTg);
		std::free(vertexDataUV);
		std::free(indexData);
	}

	void Mesh3D::draw(VkCommandBuffer commandBuffer, uint32_t index, uint32_t instances, uint32_t instanceOffset) {
		auto offsets = vertexAllocator->getOffsetData(meshID);
		uint32_t firstVertex = submeshes[index].vertexOffset + offsets.vertexOffset;
		if (hasIndexBuffer) {
			uint32_t firstIndex = submeshes[index].indexOffset + offsets.indexOffset;
			vkCmdDrawIndexed(commandBuffer, submeshes[index].indexCount, instances, firstIndex, firstVertex, instanceOffset);
		} else {
			vkCmdDraw(commandBuffer, submeshes[index].vertexCount, instances, firstVertex, instanceOffset);
		}
	}

	void Mesh3D::drawAll(VkCommandBuffer commandBuffer, uint32_t instances, uint32_t instanceOffset) {
		auto offsets = vertexAllocator->getOffsetData(meshID);
		if (hasIndexBuffer) {
			vkCmdDrawIndexed(commandBuffer, idxCount, instances, offsets.indexOffset, offsets.vertexOffset, instanceOffset);
		} else {
			vkCmdDraw(commandBuffer, vtxCount, instances, offsets.vertexOffset, instanceOffset);
		}
	}
}