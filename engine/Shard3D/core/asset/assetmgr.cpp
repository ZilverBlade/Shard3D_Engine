#include "../../s3dpch.h" 

#include "assetmgr.h"
#include "../../core.h"
#include "../../utils/json_ext.h"

#include <fstream>
#include <filesystem>
#include <ktx.h>
#include "../vulkan_api/bindless.h"
#include <vulkan/vk_enum_string_helper.h>

namespace Shard3D {
	AssetID AssetManager::importTexture2D(const std::string& sourcepath, const std::string& destpath, ResourceSystem* rSys, TextureImportInfo importInfo, TextureLoadInfo loadInfo, PackageInvocationIndex index) {
		if (IOUtils::isSubPath(ProjectSystem::getAssetLocation() + destpath, ProjectSystem::getAssetLocation())) {
			SHARD3D_ERROR("Cannot emplace a texture outside of the assets folder!");
			return AssetID::null();
		}
		std::string reldp = std::filesystem::relative(destpath, std::filesystem::path(ProjectSystem::getAssetLocation())).string();
		AssetID id = AssetID(reldp + ENGINE_ASSET_SUFFIX);
		AssetID texture = AssetID::null();
		if (strUtils::hasEnding(reldp, ".s3dtex"))
			texture = AssetID(reldp);
		else
			texture = AssetID(reldp + ".s3dtex");

		PackageInvocationIndex newIndex = index;
		newIndex.resource = ResourceType::Texture2D;

		if (auto dir = texture.getAbsolute().substr(0, texture.getAbsolute().find_last_of("/"));  !std::filesystem::exists(dir)) {
			std::filesystem::create_directories(dir);
		}
		TextureImporter::importTextureFromFile(importInfo, rSys, { sourcepath }, texture);

		std::ofstream fout(id.getAbsolute());

		nlohmann::ordered_json out{};

		out["version"] = ENGINE_VERSION;
		out["assetType"] ="texture2d";
		out["fileExt"]= "s3dtex";
		out["resourcePII"] = static_cast<uint32_t>(newIndex.index);
		out["assetFile"] = texture.getAsset();
		out["assetOrig"] = sourcepath;
		
		
		out["properties"]["filter"]= string_TextureFilter(loadInfo.filter);
		out["properties"]["addressMode"]= string_TextureSamplerMode(loadInfo.addressMode);
		out["properties"]["format"] = string_TextureFormat(loadInfo.format);
		out["properties"]["compression"]= string_TextureCompression(loadInfo.compression);
		out["properties"]["colorSpace"] = string_TextureColorSpace(loadInfo.colorSpace);
		out["properties"]["channels"]= string_TextureChannels(loadInfo.channels);
		out["properties"]["enableMipMaps"] = loadInfo.enableMipMaps;
		out["properties"]["isNormalMap"] = loadInfo.isNormalMap;
		out["properties"]["trueResolutionWidth"] = loadInfo.trueResolutionWidth;
		out["properties"]["trueResolutionHeight"] = loadInfo.trueResolutionHeight;

		fout << out.dump(4);

		fout.flush();
		fout.close();
		return id;
	}

	AssetID AssetManager::importTexture2D(const void* data, size_t textureSize, glm::ivec2 resolution, const std::string& destpath, ResourceSystem* rSys, TextureImportInfo importInfo, TextureLoadInfo loadInfo, PackageInvocationIndex index) {
		if (IOUtils::isSubPath(ProjectSystem::getAssetLocation() + destpath, ProjectSystem::getAssetLocation())) {
			SHARD3D_ERROR("Cannot emplace a texture outside of the assets folder!");
			return AssetID::null();
		}
		std::string reldp = std::filesystem::relative(destpath, std::filesystem::path(ProjectSystem::getAssetLocation())).string();
		AssetID id = AssetID(reldp + ENGINE_ASSET_SUFFIX);
		AssetID texture = AssetID::null();
		if (strUtils::hasEnding(reldp, ".s3dtex"))
			texture = AssetID(reldp);
		else
			texture = AssetID(reldp + ".s3dtex");

		PackageInvocationIndex newIndex = index;
		newIndex.resource = ResourceType::Texture2D;

		if (auto dir = texture.getAbsolute().substr(0, texture.getAbsolute().find_last_of("/"));  !std::filesystem::exists(dir)) {
			std::filesystem::create_directories(dir);
		}
		TextureImporter::importTextureFromMemory(importInfo, rSys, data, getVkFormatTextureFormat(loadInfo.format, loadInfo.colorSpace, loadInfo.channels, loadInfo.compression), resolution.x, resolution.y, textureSize, texture);

		std::ofstream fout(id.getAbsolute());

		nlohmann::ordered_json out{};

		out["version"] = ENGINE_VERSION;
		out["assetType"] = "texture2d";
		out["fileExt"] = "s3dtex";
		out["resourcePII"] = static_cast<uint32_t>(newIndex.index);
		out["assetFile"] = texture.getAsset();
		out["assetOrig"] = "<void*>";


		out["properties"]["filter"] = string_TextureFilter(loadInfo.filter);
		out["properties"]["addressMode"] = string_TextureSamplerMode(loadInfo.addressMode);
		out["properties"]["format"] = string_TextureFormat(loadInfo.format);
		out["properties"]["compression"] = string_TextureCompression(loadInfo.compression);
		out["properties"]["colorSpace"] = string_TextureColorSpace(loadInfo.colorSpace);
		out["properties"]["channels"] = string_TextureChannels(loadInfo.channels);
		out["properties"]["enableMipMaps"] = loadInfo.enableMipMaps;
		out["properties"]["isNormalMap"] = loadInfo.isNormalMap;
		out["properties"]["trueResolutionWidth"] = loadInfo.trueResolutionWidth;
		out["properties"]["trueResolutionHeight"] = loadInfo.trueResolutionHeight;

		fout << out.dump(4);

		fout.flush();
		fout.close();
		return id;
	}

	AssetID AssetManager::modifyTexture2D(const AssetID& asset, TextureLoadInfo loadInfo) {
		if (!IOUtils::doesFileExist(asset.getAbsolute())) {
			SHARD3D_ERROR("Model '{0}' does not exist!", asset.getAsset());
			return AssetID::null();
		}

		auto rp = ResourceSystem::discoverResourceInformation(asset);
		std::ofstream fout(asset.getAbsolute());

		nlohmann::ordered_json out{};

		out["version"] = ENGINE_VERSION;
		out["assetType"] = "texture2d";
		out["fileExt"] = rp.fileExtension;
		out["resourcePII"] = static_cast<uint32_t>(rp.PII.index);
		out["assetFile"] = rp.resources[0];
		out["assetOrig"] = rp.origin;


		out["properties"]["filter"] = string_TextureFilter(loadInfo.filter);
		out["properties"]["addressMode"] = string_TextureSamplerMode(loadInfo.addressMode);
		out["properties"]["format"] = string_TextureFormat(loadInfo.format);
		out["properties"]["compression"] = string_TextureCompression(loadInfo.compression);
		out["properties"]["colorSpace"] = string_TextureColorSpace(loadInfo.colorSpace);
		out["properties"]["channels"] = string_TextureChannels(loadInfo.channels);
		out["properties"]["enableMipMaps"] = loadInfo.enableMipMaps;
		out["properties"]["isNormalMap"] = loadInfo.isNormalMap;
		out["properties"]["trueResolutionWidth"] = loadInfo.trueResolutionWidth;
		out["properties"]["trueResolutionHeight"] = loadInfo.trueResolutionHeight;

		fout << out.dump(4);

		fout.flush();
		fout.close();
		return asset;
	}

	AssetID AssetManager::importTextureCube(const std::vector<std::string>& sourcetextures, const std::string& destpath, ResourceSystem* rSys, TextureImportInfo importInfo, TextureLoadInfo loadInfo, PackageInvocationIndex index) {
		if (IOUtils::isSubPath(ProjectSystem::getAssetLocation() + destpath, ProjectSystem::getAssetLocation())) {
			SHARD3D_ERROR("Cannot emplace a texture outside of the assets folder!");
			return AssetID::null();
		}
		std::string reldp = std::filesystem::relative(destpath, std::filesystem::path(ProjectSystem::getAssetLocation())).string();
		AssetID id = AssetID(reldp + ENGINE_ASSET_SUFFIX);
		AssetID texture = AssetID::null();
		if (strUtils::hasEnding(reldp, ".s3dtex"))
			texture = AssetID(reldp);
		else
			texture = AssetID(reldp + ".s3dtex");

		PackageInvocationIndex newIndex = index;
		newIndex.resource = ResourceType::Texture2D;

		if (auto dir = texture.getAbsolute().substr(0, texture.getAbsolute().find_last_of("/"));  !std::filesystem::exists(dir)) {
			std::filesystem::create_directories(dir);
		}
		TextureImporter::importTextureFromFile(importInfo, rSys, sourcetextures, texture);

		std::ofstream fout(id.getAbsolute());
		nlohmann::ordered_json out{};

		out["version"] = ENGINE_VERSION;
		out["assetType"] = "texturecube";
		out["fileExt"] = "s3dtex";
		out["resourcePII"] = static_cast<uint32_t>(newIndex.index);
		out["assetFile"] = texture.getAsset();
		out["assetOrig"] = sourcetextures;


		out["properties"]["filter"] = string_TextureFilter(loadInfo.filter);
		out["properties"]["addressMode"] = string_TextureSamplerMode(loadInfo.addressMode);
		out["properties"]["format"] = string_TextureFormat(loadInfo.format);
		out["properties"]["compression"] = string_TextureCompression(loadInfo.compression);
		out["properties"]["colorSpace"] = string_TextureColorSpace(loadInfo.colorSpace);
		out["properties"]["channels"] = string_TextureChannels(loadInfo.channels);
		out["properties"]["trueResolutionWidth"] = loadInfo.trueResolutionWidth;
		out["properties"]["trueResolutionHeight"] = loadInfo.trueResolutionHeight;

		fout << out.dump(4);

		fout.flush();
		fout.close();
		return id;
	}

	AssetID AssetManager::importTextureCube(const void* data, size_t textureSize, glm::ivec2 resolution, const std::string& destpath, ResourceSystem* rSys, TextureImportInfo importInfo, TextureLoadInfo loadInfo, PackageInvocationIndex index) {
		if (IOUtils::isSubPath(ProjectSystem::getAssetLocation() + destpath, ProjectSystem::getAssetLocation())) {
			SHARD3D_ERROR("Cannot emplace a texture outside of the assets folder!");
			return AssetID::null();
		}
		std::string reldp = std::filesystem::relative(destpath, std::filesystem::path(ProjectSystem::getAssetLocation())).string();
		AssetID id = AssetID(reldp + ENGINE_ASSET_SUFFIX);
		AssetID texture = AssetID::null();
		if (strUtils::hasEnding(reldp, ".s3dtex"))
			texture = AssetID(reldp);
		else
			texture = AssetID(reldp + ".s3dtex");

		PackageInvocationIndex newIndex = index;
		newIndex.resource = ResourceType::Texture2D;

		if (auto dir = texture.getAbsolute().substr(0, texture.getAbsolute().find_last_of("/"));  !std::filesystem::exists(dir)) {
			std::filesystem::create_directories(dir);
		}
		TextureImporter::importTextureFromMemory(importInfo, rSys, data, getVkFormatTextureFormat(loadInfo.format, loadInfo.colorSpace, loadInfo.channels, loadInfo.compression), resolution.x, resolution.y, textureSize, texture);

		std::ofstream fout(id.getAbsolute());

		nlohmann::ordered_json out{};

		out["version"] = ENGINE_VERSION;
		out["assetType"] = "texturecube";
		out["fileExt"] = "s3dtex";
		out["resourcePII"] = static_cast<uint32_t>(newIndex.index);
		out["assetFile"] = texture.getAsset();
		out["assetOrig"] = "<void*>";

		out["properties"]["filter"] = string_TextureFilter(loadInfo.filter);
		out["properties"]["addressMode"] = string_TextureSamplerMode(loadInfo.addressMode);
		out["properties"]["format"] = string_TextureFormat(loadInfo.format);
		out["properties"]["compression"] = string_TextureCompression(loadInfo.compression);
		out["properties"]["colorSpace"] = string_TextureColorSpace(loadInfo.colorSpace);
		out["properties"]["channels"] = string_TextureChannels(loadInfo.channels);
		out["properties"]["trueResolutionWidth"] = loadInfo.trueResolutionWidth;
		out["properties"]["trueResolutionHeight"] = loadInfo.trueResolutionHeight;

		fout << out.dump(4);

		fout.flush();
		fout.close();
		return id;
	}

	std::vector<AssetID> AssetManager::importMeshes(const std::string& sourcepath, const std::string& destpath, ResourceSystem* rSys, Model3DImportInfo info, PackageInvocationIndex index) {
		if (IOUtils::isSubPath(ProjectSystem::getAssetLocation() + destpath, ProjectSystem::getAssetLocation())) {
			SHARD3D_ERROR("Cannot emplace a mesh outside of the assets folder!");
			return {};
		}
		std::string reldp = std::filesystem::relative(destpath, std::filesystem::path(ProjectSystem::getAssetLocation())).string();
		AssetID id = AssetID(reldp + ENGINE_ASSET_SUFFIX);
		AssetID model = AssetID(reldp);
		
		PackageInvocationIndex newIndex = index;
		newIndex.resource = ResourceType::Model3D;

		std::vector<ModelInfo> readInfos = ModelImporter::importModelsFromFile(info, rSys, sourcepath, model);
		std::vector<AssetID> assets;
		for (auto& readInfo : readInfos) {
			std::string base = readInfo.meshAsset.getAsset().substr(0, readInfo.meshAsset.getAsset().find_last_of("."));
			{
				assets.push_back(base + ".s3dasset");
				std::ofstream fout(assets.back().getAbsolute());

				nlohmann::ordered_json out{};

				out["version"] = ENGINE_VERSION;
				out["assetType"] = "mesh3d";
				out["fileExt"] = "s3dmesh";
				out["resourcePII"] = static_cast<uint32_t>(newIndex.index);
				out["assetFile"] = readInfo.meshAsset.getAsset();
				out["assetOrig"] = sourcepath;
				out["materials"] = readInfo.defaultMaterials;


				out["properties"]["castShadows"] = true;
				out["properties"]["enableDistanceFields"] = true;
				fout << out.dump(4);

				fout.flush();
				fout.close();
			}
			if (readInfo.skeletonAsset) {
				assets.push_back(base + "_skeleton.s3dasset");
				std::ofstream fout(assets.back().getAbsolute());

				nlohmann::ordered_json out{};

				out["version"] = ENGINE_VERSION;
				out["assetType"] = "skeleton";
				out["fileExt"] = "s3dskeleton";
				out["resourcePII"] = static_cast<uint32_t>(newIndex.index);
				out["assetFile"] = readInfo.skeletonAsset.getAsset();
				out["assetOrig"] = sourcepath;
				out["attachedMeshAsset"] = base + ".s3dasset";
				fout << out.dump(4);

				fout.flush();
				fout.close();
			}
			if (readInfo.animationAssets.size()) {
				for (auto& animation : readInfo.animationAssets) {
					std::string strAnim = animation.getAsset();
					assets.push_back(strAnim.substr(0, strAnim.find_last_of(".")) + ".s3dasset");
					std::ofstream fout(assets.back().getAbsolute());

					nlohmann::ordered_json out{};

					out["version"] = ENGINE_VERSION;
					out["assetType"] = "skeletal_animation";
					out["fileExt"] = "s3danimation";
					out["resourcePII"] = static_cast<uint32_t>(newIndex.index);
					out["assetFile"] = animation.getAsset();
					out["assetOrig"] = sourcepath;
					out["attachedSkeletonAsset"] = base + "_skeleton.s3dasset";
					fout << out.dump(4);

					fout.flush();
					fout.close();
				}
			}
		}
		return assets;
	}

	AssetID AssetManager::modifyMesh(const AssetID& asset, Resources::Mesh3D* model) {
		if (!IOUtils::doesFileExist(asset.getAbsolute())) {
			SHARD3D_ERROR("Model '{0}' does not exist!", asset.getAsset());
			return AssetID::null();
		}

		std::ofstream fout(asset.getAbsolute());

		auto rp = ResourceSystem::discoverResourceInformation(asset);

		nlohmann::ordered_json out{};

		out["version"] = ENGINE_VERSION;
		out["assetType"] = "mesh3d";
		out["fileExt"] = rp.fileExtension;
		out["resourcePII"] = static_cast<uint32_t>(rp.PII.index);
		out["assetFile"] = rp.resources[0];
		out["assetOrig"] = rp.origin;
		out["materials"] = model->materials;


		out["properties"]["castShadows"] = true;
		out["properties"]["enableDistanceFields"] = true;

		fout << out.dump(4);

		fout.flush();
		fout.close();
		return asset;
	}


	AssetID AssetManager::createMaterial(const std::string& destpath, SurfaceMaterial* material) {
		std::string dest = destpath;
		std::replace(dest.begin(), dest.end(), '\\', '/');
		if (!strUtils::hasEnding(dest, ".s3dasset")) dest = destpath + ".s3dasset";
		material->serialize(dest);

		std::string reldp = std::filesystem::relative(destpath, std::filesystem::path(ProjectSystem::getAssetLocation())).string();
		return AssetID(reldp);
	}

	AssetID AssetManager::createPhysicsAsset(const std::string& destpath, const PhysicsAsset& asset) {
		if (IOUtils::isSubPath(ProjectSystem::getAssetLocation() + destpath, ProjectSystem::getAssetLocation())) {
			SHARD3D_ERROR("Cannot emplace a mesh outside of the assets folder!");
			return AssetID::null();
		}
		std::string reldp = std::filesystem::relative(destpath, std::filesystem::path(ProjectSystem::getAssetLocation())).string();
		AssetID id = AssetID(reldp + ENGINE_ASSET_SUFFIX);
		AssetID model = AssetID(reldp);

		std::ofstream fout(destpath);

		nlohmann::ordered_json out{};

		out["version"] = ENGINE_VERSION;
		out["assetType"] = "physics_asset";
		switch (asset.colliderType) {
		case(PhysicsColliderType::Sphere):
			out["colliderType"] = "sphere";
			out["collider"]["radius"] = asset.sphereRadius;
			break;
		case(PhysicsColliderType::Box):
			out["colliderType"] = "box";
			out["collider"]["extent"] = asset.boxExtent;
			break;
		case(PhysicsColliderType::Plane):
			out["colliderType"] = "plane";
			break;
		case(PhysicsColliderType::Capsule):
			out["colliderType"] = "capsule";
			out["collider"]["radius"] = asset.cylindricalRadius;
			out["collider"]["length"] = asset.cylindricalLength;
			break;
		case(PhysicsColliderType::Cylinder):
			out["colliderType"] = "cylinder";
			out["collider"]["radius"] = asset.cylindricalRadius;
			out["collider"]["length"] = asset.cylindricalLength;
			break;
		case(PhysicsColliderType::ConvexHullAsset):
			out["colliderType"] = "convex_hull";
			out["collider"]["hullPoints"] = nlohmann::ordered_json::array();
			for (glm::vec3 point : asset.convexPoints) {
				out["collider"]["hullPoints"].push_back(point);
			}
			break;
		case(PhysicsColliderType::MeshAsset):
			out["colliderType"] = "trimesh";
			out["collider"]["trimeshAsset"] = asset.trimeshAsset.getAsset();
			break;
		}

		out["mass"] = asset.mass;
		out["customCenterOfMass"] = asset.enableCustomCOM;
		out["centerOfMass"] = asset.computedCOM;
		out["intertiaTensor"] = nlohmann::ordered_json::array();
		out["intertiaTensor"].push_back(asset.computedInteriaTensor[0]);
		out["intertiaTensor"].push_back(asset.computedInteriaTensor[1]);
		out["intertiaTensor"].push_back(asset.computedInteriaTensor[2]);

		out["positionOffset"] = asset.colliderPositionOffset;
		out["rotationOffset"] = asset.colliderRotationOffset;

		fout << out.dump(4);

		fout.flush();
		fout.close();

		return id;
	}

	PhysicsAsset AssetManager::loadPhysicsAsset(const AssetID& asset) {
		ResourceInfo info{};
		simdjson::dom::parser parser;
		if (!std::filesystem::exists(asset.getAbsolute())) return PhysicsAsset();
		simdjson::padded_string json = simdjson::padded_string::load(asset.getAbsolute());
		const auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			SHARD3D_WARN("Failed to load {0} (ID {1})", asset.getAsset(), asset.getID());
			return PhysicsAsset();
		}
		if (data["assetType"].get_string().value() != "physics_asset") {
			SHARD3D_WARN("Trying to load non mesh3d as a mesh3d asset!");
			return PhysicsAsset();
		}

		PhysicsAsset ph{};

		if (data["colliderType"].get_string().value() == "sphere") {
			ph.colliderType = PhysicsColliderType::Sphere;
			ph.sphereRadius = data["collider"]["radius"].get_double().value();
		}
		if (data["colliderType"].get_string().value() == "box") {
			ph.colliderType = PhysicsColliderType::Box;
			ph.boxExtent = SIMDJSON_READ_VEC3(data["collider"]["extent"]);
		}
		if (data["colliderType"].get_string().value() == "plane") {
			ph.colliderType = PhysicsColliderType::Plane;
		}
		if (data["colliderType"].get_string().value() == "capsule") {
			ph.colliderType = PhysicsColliderType::Capsule;
			ph.cylindricalRadius = data["collider"]["radius"].get_double().value();
			ph.cylindricalLength = data["collider"]["length"].get_double().value();
		}
		if (data["colliderType"].get_string().value() == "cylinder") {
			ph.colliderType = PhysicsColliderType::Cylinder;
			ph.cylindricalRadius = data["collider"]["radius"].get_double().value();
			ph.cylindricalLength = data["collider"]["length"].get_double().value();
		}
		if (data["colliderType"].get_string().value() == "convex_hull") {
			ph.colliderType = PhysicsColliderType::ConvexHullAsset;

			for (int i = 0; i < data["collider"]["hullPoints"].get_array().size(); i++) {
				ph.convexPoints.push_back(SIMDJSON_READ_VEC3(data["collider"]["hullPoints"].get_array().at(i)));
			}
		}
		if (data["colliderType"].get_string().value() == "trimesh") {
			ph.colliderType = PhysicsColliderType::MeshAsset;
			ph.trimeshAsset =
				std::string(data["collider"]["trimeshAsset"].get_string().value());
		}


		ph.mass = data["mass"].get_double().value();
		ph.computedCOM = SIMDJSON_READ_VEC3(data["centerOfMass"]);
		ph.computedInteriaTensor[0] = SIMDJSON_READ_VEC3(data["intertiaTensor"].get_array().at(0));
		ph.computedInteriaTensor[1] = SIMDJSON_READ_VEC3(data["intertiaTensor"].get_array().at(1));
		ph.computedInteriaTensor[2] = SIMDJSON_READ_VEC3(data["intertiaTensor"].get_array().at(2));

		ph.colliderPositionOffset = SIMDJSON_READ_VEC3(data["positionOffset"]);
		ph.colliderRotationOffset = SIMDJSON_READ_VEC3(data["rotationOffset"]);

		return ph;
	}

	sPtr<SkeletalAnimation> AssetManager::loadSkeletalAnimation(const AssetID& asset) {

		simdjson::dom::parser parser;
		if (!std::filesystem::exists(asset.getAbsolute())) return nullptr;
		simdjson::padded_string json = simdjson::padded_string::load(asset.getAbsolute());
		const auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			SHARD3D_WARN("Failed to load {0} (ID {1})", asset.getAsset(), asset.getID());
			return nullptr;
		}
		if (data["assetType"].get_string().value() != "skeletal_animation") {
			SHARD3D_WARN("Trying to load non skeletal_animation as a skeletal_animation asset!");
			return nullptr;
		}

		
		AssetID animFile = std::string(data["assetFile"].get_string().value());
		SkeletalAnimationRawData* animData = ModelImporter::loadAnimationData(animFile);
		if (!animData) return nullptr;
		sPtr<SkeletalAnimation> animation = make_sPtr<SkeletalAnimation>(animData);
		ModelImporter::freeAnimationData(animData);
		return animation;
	}

	AssetID AssetManager::exportBakedReflectionCube(ktxTexture2* textureData, const std::string& destpath, PackageInvocationIndex index) {
		if (IOUtils::isSubPath(ProjectSystem::getAssetLocation() + destpath, ProjectSystem::getAssetLocation())) {
			SHARD3D_ERROR("Cannot emplace a refection capture outside of the assets folder!");
			return AssetID::null();
		}
		std::string reldp = std::filesystem::relative(destpath, std::filesystem::path(ProjectSystem::getAssetLocation())).string();
		AssetID id = AssetID(reldp + ENGINE_ASSET_SUFFIX);

		std::ofstream tout(destpath);
		TextureImporter::exportCompressedTextureKTX(textureData, destpath.c_str());

		std::ofstream fout(destpath + ENGINE_ASSET_SUFFIX);

		nlohmann::ordered_json out{};

		out["version"] = ENGINE_VERSION;
		out["assetType"] = "baked_lighting";
		out["fileExt"] = "s3dbak";
		out["bakedType"] = "hdr_cube_reflection";
		out["resourcePII"] = static_cast<uint32_t>(index.index);
		out["assetFile"] = reldp;
		out["properties"]["vkFormat"] = string_VkFormat(static_cast<VkFormat>(textureData->vkFormat));

		fout << out.dump(4);

		fout.flush();
		fout.close();
		return id;
	}

	bool AssetManager::reimportAsset(const AssetID& asset) {
		if (!IOUtils::doesFileExist(asset.getAsset())) return false;
		ResourceInfo info = ResourceSystem::discoverResourceInformation(asset);
		switch (info.PII.resource) {
		case(ResourceType::Model3D): case(ResourceType::Texture2D):
			if (!IOUtils::doesFileExist(info.origin)) return false;
			if (!IOUtils::doesFileExist(ResourceSystem::getVirtualPath(asset))) { SHARD3D_FATAL("Virtual path does not exist for asset that is trying to reimport asset!"); return false; } // this should always exist
			std::filesystem::remove(info.resources[0]);
			if (bool copied = std::filesystem::copy_file(info.origin, info.resources[0])) {
				switch (info.PII.resource) {
				case(ResourceType::Model3D): {
					//if (ResourceSystem::getMeshAssets().contains(asset)) {
					//	//vkDeviceWaitIdle(engineDevice.device());
					//	//ResourceSystem::loadMesh(asset, true);
					//}
				} break;
				case(ResourceType::Texture2D): {
					//if (ResourceSystem::getTextureAssets().contains(asset)) {
					//	//vkDeviceWaitIdle(engineDevice.device());
					//	//ResourceSystem::loadTexture(asset, true);
					//}
				} break;
				}
				return copied;
			}
			break;
		case(ResourceType::TextureCube):
			// no check for if the texture cube origin exists, just hope and pray it works until i add it :skull:
			for (auto& resource : ResourceSystem::getVirtualPathArray(asset.getAsset()))
				if (!IOUtils::doesFileExist(resource)) { SHARD3D_FATAL("Virtual path does not exist for asset that is trying to reimport asset!"); return false; } // this should always exist
			
			break;
		default: return false;
		}
	}

	void AssetManager::renameVirtualResource(AssetType type, const AssetID& oldAsset, const AssetID& newAsset) {
		SHARD3D_ASSERT(type == AssetType::Model3D || type == AssetType::Texture2D || type == AssetType::TextureCube && "Can only rename resource files");
		std::ifstream f(oldAsset.getAbsolute());
		nlohmann::ordered_json data = nlohmann::ordered_json::parse(f);
		f.close();
		std::string ext = data["fileExt"].get<std::string>();

		if (type == AssetType::Texture2D || type == AssetType::TextureCube) {
			std::string oldmagic = std::string(data["assetFile"].get<std::string>());
			std::string magicpath = newAsset.getAsset().substr(0, newAsset.getAsset().find_last_of(".") + 1) + ext;// remove .s3dasset from asset, add back the extension of the resource 
			data["assetFile"] = magicpath;
			std::filesystem::rename(AssetID(oldmagic).getAbsolute(), AssetID(magicpath).getAbsolute());

			std::string oldCachektx = ResourceSystem::getTextureCacheLocation(oldAsset);
			if (std::filesystem::exists(oldCachektx)) {
				// fix the cache files if they exist
				std::string newCachektx = ResourceSystem::getTextureCacheLocation(newAsset);

				std::filesystem::rename(oldCachektx, newCachektx);
				std::filesystem::rename(oldCachektx + ".s3dcache", newCachektx + ".s3dcache");
			}

		} else if (type == AssetType::Model3D) {
			if (ResourceSystem::getAssetVersion(newAsset) < Version(0, 13, 0)) {
				for (int i = 0; i < data["assetFiles"].size(); i++) {
					auto& entry = data["assetFiles"].at(i);
					std::string oldmagic = std::string(entry.get<std::string>());
					std::string magicpath = newAsset.getAsset().substr(0, newAsset.getAsset().find_last_of(".") + 1) + ext;// remove .s3dasset from asset, add back the extension of the resource 
					entry = magicpath;
					std::filesystem::rename(AssetID(oldmagic).getAbsolute(), AssetID(magicpath).getAbsolute());
				}
			} else {
				std::string oldmagic = std::string(data["assetFile"].get<std::string>());
				std::string magicpath = newAsset.getAsset().substr(0, newAsset.getAsset().find_last_of(".") + 1) + ext;// remove .s3dasset from asset, add back the extension of the resource 
				data["assetFile"] = magicpath;
				std::filesystem::rename(AssetID(oldmagic).getAbsolute(), AssetID(magicpath).getAbsolute());
			}
			
		}
		std::ofstream fout(oldAsset.getAbsolute());
		fout << data.dump(4);
		fout.flush();
		fout.close();
	}

	void AssetManager::purgeAsset(const AssetID& asset) {
		AssetType type = ResourceSystem::discoverAssetType(asset.getAbsolute());
		if (type == AssetType::TextureCube || type == AssetType::Texture2D) {
			std::string vpath = ResourceSystem::getVirtualPath(asset);
			if (std::filesystem::remove(vpath))
				SHARD3D_LOG("file {0} deleted.", vpath);
			else
				SHARD3D_LOG("file {0} not found.", vpath);
			std::string vcachepath = ResourceSystem::getTextureCacheLocation(asset);
			if (std::filesystem::remove(vcachepath))
				SHARD3D_LOG("file {0} deleted.", vcachepath);
			else
				SHARD3D_LOG("file {0} not found.", vcachepath);
		}
		else if (type == AssetType::Model3D) {
			std::vector<std::string> vpaths;
			if (ResourceSystem::getAssetVersion(asset) < Version(0, 13, 0) ){
				vpaths	= ResourceSystem::getVirtualPathArray(asset);
			} else {
				vpaths.push_back(ResourceSystem::getVirtualPath(asset));
			}
			for (std::string& vpath : vpaths)
				if (std::filesystem::remove(vpath))
					SHARD3D_LOG("file {0} deleted.", vpath);
				else
					SHARD3D_LOG("file {0} not found.", vpath);
		}

		if (std::filesystem::remove(asset.getAbsolute()))
			std::cout << "file " << asset.getAsset() << " deleted.\n";
		else
			std::cout << "file " << asset.getAsset() << " not found.\n";
	}
}
