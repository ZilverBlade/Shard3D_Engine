#include "assetid.h"
#include <xhash>
#include <algorithm>
namespace Shard3D {
	static std::hash<std::string> hasher;
	static std::string convert(std::string orig) { 
		std::replace(orig.begin(), orig.end(), '\\', '/'); 
		return orig; 
	}
	static std::string forceToLower(std::string orig) {
		std::transform(orig.begin(), orig.end(), orig.begin(),
			[](unsigned char c) { return std::tolower(c); });
		return orig;
	}
	AssetID::AssetID(const std::string& asset) 
		: assetFile(asset.empty() ? "" : convert(asset)),
		assetID(asset.empty() ? 0 : hasher(forceToLower(assetFile))) {
#ifndef NDEBUG
		//if (ProjectSystem::initialized()) assert(!strUtils::hasStarting(assetFile, ProjectSystem::getAssetLocation()) && "Illegal asset naming!!!");
#endif
	}
}