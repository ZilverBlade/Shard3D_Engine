#pragma once
#include "../../core.h"
namespace Shard3D {
	class S3DBuffer;
	enum class SynchronizationAttachment {
		None, // Only use for ComputeToXXX syncs
		Depth,
		Color
	};

	enum class SynchronizationType {
		None,
		ComputeToCompute,
		ComputeToGraphics,
		GraphicsToCompute,
		Transfer,
		DebugBarrier
	};

	class S3DSynchronization {
	public:
		S3DSynchronization() = default;
		S3DSynchronization(SynchronizationType syncType_, VkPipelineStageFlags graphicsStage = 0);
		void addImageBarrier(SynchronizationAttachment syncAttachment_, VkImage toSyncImage, VkImageSubresourceRange subresourceRange, VkImageLayout oldLayout, VkImageLayout newLayout);
		void addMemoryBarrier(VkAccessFlags syncMemoryAccess_);
		void addBufferMemoryBarrier(VkAccessFlags syncMemoryAccessSrc, VkAccessFlags syncMemoryAccessDst, S3DBuffer* buffer);
		void syncBarrier(VkCommandBuffer commandBuffer);
		void clearBarriers() { memoryBarriers.clear(); imageMemoryBarriers.clear(); }
	protected:
		VkPipelineStageFlags srcStageMask{};
		VkPipelineStageFlags dstStageMask{};
		std::vector<VkMemoryBarrier> memoryBarriers;
		std::vector<VkBufferMemoryBarrier> bufferMemoryBarriers;
		std::vector<VkImageMemoryBarrier> imageMemoryBarriers;
		SynchronizationType syncType{};
	};
//  this is the sync'y
}