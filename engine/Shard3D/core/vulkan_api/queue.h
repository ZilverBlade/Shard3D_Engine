#pragma once
extern "C" {
	typedef struct VkQueue_T* VkQueue;
}
namespace Shard3D {
	enum class QueueType {
		Graphics,
		Compute,
		Transfer
	};
	class S3DQueue {
	public:
		VkQueue queue;
	private:
		bool occupied = false;
		QueueType type;
		friend class S3DDevice;
	};
}