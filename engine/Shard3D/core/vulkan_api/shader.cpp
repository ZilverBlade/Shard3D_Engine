#include "shader.h"
#include <filesystem>
#include "../../utils/logger.h"
#include "../../systems/handlers/project_system.h"
namespace Shard3D {
	void S3DShaderSpecialization::addConstant(size_t location, void* value, size_t size) {
		VkSpecializationMapEntry entry;
		entry.constantID = location;
		entry.size = size;
		entry.offset = entries.size() == 0 ? 0 : entries.back().offset + entries.back().size;
		entries.push_back(entry);
		data.resize(entry.size + entry.offset);
		memcpy(data.data() + entry.offset, value, size);
	}
	VkSpecializationInfo S3DShaderSpecialization::getSpecializationInfo()const {
		VkSpecializationInfo specialization;
		specialization.dataSize = data.size();
		specialization.pData = data.data();
		specialization.mapEntryCount = entries.size();
		specialization.pMapEntries = entries.data();
		return specialization;
	}

	S3DShader::S3DShader(
		ShaderType type_, 
		const std::string& rawShaderFile, 
		const std::string& subdirectory,
		const S3DShaderSpecialization& specializationInfo,
		const std::vector<std::string> definitions,
		const std::string& overrideFile
	) : type(type_), originFile(rawShaderFile), specializationInfo(specializationInfo) {
		std::string directory = ProjectSystem::getAssetLocation() + "shaderdata/" + subdirectory;
		if (!std::filesystem::exists(directory)) std::filesystem::create_directories(directory);
		compiledFile = overrideFile.empty() ? 
			ShaderSystem::getRuntimeShaderDirectory(rawShaderFile, subdirectory) : 
			directory + "/" + overrideFile + ".spv";
		if (!IOUtils::doesFileExist(compiledFile)) {
			compiledCode = ShaderSystem::compileOnTheFlyDirect(IOUtils::readText(rawShaderFile), rawShaderFile.c_str(), type, definitions);
			if (!compiledCode.empty()) {
				IOUtils::writeStackBinary(compiledCode.data(), compiledCode.size(), compiledFile);
			}
		}
		else {
			SHARD3D_LOG("Reading cached shader '{0}'", compiledFile); 
			compiledCode = IOUtils::readBinary(compiledFile);
		}
	}

	S3DShader::S3DShader(ShaderType type, const std::string& raw_code, const S3DShaderSpecialization& specializationInfo, const std::vector<std::string> definitions) : specializationInfo(specializationInfo) {
		compiledCode = ShaderSystem::compileOnTheFly(raw_code, type, definitions);
	}

	S3DShader::~S3DShader() {
	}

	VkShaderModule S3DShader::createShaderModule(S3DDevice& device) const {
		auto bytes = IOUtils::readBinary(compiledFile);
		VkShaderModule shaderModule{};
		VkShaderModuleCreateInfo createInfo{};
		createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		createInfo.codeSize = bytes.size();
		createInfo.pCode = reinterpret_cast<const uint32_t*>(bytes.data());
		
		VK_ASSERT(vkCreateShaderModule(device.device(), &createInfo, nullptr, &shaderModule), "failed to create shader module!");
		return shaderModule;
	}
}