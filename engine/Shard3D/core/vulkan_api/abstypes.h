#pragma once
#include "../../utils/logger.h"
namespace Shard3D {
	enum class TextureFilter {
		Nearest, Linear
	};
	static inline const char* string_TextureFilter(TextureFilter input) {
		switch (input) {
		case(TextureFilter::Nearest):
			return "nearest";
		case(TextureFilter::Linear):
			return "linear";
		}
	}
	static inline TextureFilter enum_TextureFilter(const std::string& input) {
		if (input == "nearest") return TextureFilter::Nearest;
		if (input == "linear") return TextureFilter::Linear;
	}

	enum class TextureSamplerMode {
		Repeat, RepeatMirrored,
		ClampToEdge, ClampToBorder, ClampToEdgeMirrored
	};
	static inline const char* string_TextureSamplerMode(TextureSamplerMode input) {
		switch (input) {
		case(TextureSamplerMode::Repeat):
			return "repeat";
		case(TextureSamplerMode::RepeatMirrored):
			return "repeatMirrored";
		case(TextureSamplerMode::ClampToEdge):
			return "clampToEdge";
		case(TextureSamplerMode::ClampToBorder):
			return "clampToBorder";
		case(TextureSamplerMode::ClampToEdgeMirrored):
			return "clampToEdgeMirrored";
		}
	}
	static inline TextureSamplerMode enum_TextureSamplerMode(const std::string& input) {
		if (input == "repeat") return TextureSamplerMode::Repeat;
		if (input == "repeatMirrored") return TextureSamplerMode::RepeatMirrored;
		if (input == "clampToEdge") return TextureSamplerMode::ClampToEdge;
		if (input == "clampToBorder") return TextureSamplerMode::ClampToBorder;
		if (input == "clampToEdgeMirrored") return TextureSamplerMode::ClampToEdgeMirrored;
	}
	enum class TextureFormat {
		Unorm8,
		Unorm16,
		Floating16,
		Floating32,
		FloatingEBGR
	};
	enum class TextureColorSpace {
		Linear,
		sRGB
	};
	enum class TextureChannels {
		R,
		RG,
		RGB,
		RGBA
	};
	static inline const char* string_TextureFormat(TextureFormat input) {
		switch (input) {
		case(TextureFormat::Unorm8):
			return "unorm8";
		case(TextureFormat::Unorm16):
			return "unorm16";
		case(TextureFormat::Floating16):
			return "floating16";
		case(TextureFormat::Floating32):
			return "floating32";
		case(TextureFormat::FloatingEBGR):
			return "floatingEBGR";
		}
	}
	static inline TextureFormat enum_TextureFormat(const std::string& input) {
			 if (input == "unorm8") return TextureFormat::Unorm8;
		else if (input == "unorm16") return TextureFormat::Unorm16;
		else if (input == "floating16") return TextureFormat::Floating16;
		else if (input == "floating32") return TextureFormat::Floating32;
		else if (input == "floatingEBGR") return TextureFormat::FloatingEBGR;
	}

	static inline const char* string_TextureColorSpace(TextureColorSpace input) {
		switch (input) {
		case(TextureColorSpace::Linear):
			return "linear";
		case(TextureColorSpace::sRGB):
			return "sRGB";
		}
	}
	static inline TextureColorSpace enum_TextureColorSpace(const std::string& input) {
			 if (input == "linear") return TextureColorSpace::Linear;
		else if (input == "sRGB") return TextureColorSpace::sRGB;
	}

	static inline const char* string_TextureChannels(TextureChannels input) {
		switch (input) {
		case(TextureChannels::R):
			return "r";
		case(TextureChannels::RG):
			return "rg";
		case(TextureChannels::RGB):
			return "rgb";
		case(TextureChannels::RGBA):
			return "rgba";
		}
	}
	static inline TextureChannels enum_TextureChannels(const std::string& input) {
		if (input == "rgba") return TextureChannels::RGBA;
		if (input == "rgb") return TextureChannels::RGB;
		if (input == "rg") return TextureChannels::RG;
		if (input == "r") return TextureChannels::R;
	}

	enum class TextureCompression { // https://learn.microsoft.com/en-us/windows/win32/direct3d11/texture-block-compression-in-direct3d-11
		Lossless, 
		BC1,    // Three color channels (5 bits:6 bits:5 bits), with 0 or 1 bit(s) of alpha
		BC2,    // Three color channels (5 bits:6 bits:5 bits), with 4 bits of alpha
		BC3,    // Three color channels (5 bits:6 bits:5 bits) with 8 bits of alpha
		BC4,    // One color channel (8 bits)
		BC5,    // Two color channels (8 bits:8 bits)
		BC6H,   // Three color channels (16 bits:16 bits:16 bits) in "half" floating point*
		BC7     // Three color channels (4 to 7 bits per channel) with 0 to 8 bits of alpha
	};
	static inline const char* string_TextureCompression(TextureCompression input) {
		switch (input) {
		case(TextureCompression::Lossless):
			return "lossless"; 
		case(TextureCompression::BC1):
			return "bc1";
		case(TextureCompression::BC2):
			return "bc";
		case(TextureCompression::BC3):
			return "bc3";
		case(TextureCompression::BC4):
			return "bc4";
		case(TextureCompression::BC5):
			return "bc5";
		case(TextureCompression::BC6H):
			return "bc6hdr";
		case(TextureCompression::BC7):
			return "bc7";
		}
	}
	static inline TextureCompression enum_TextureCompression(const std::string& input) {
		if (input == "lossless") return TextureCompression::Lossless;
		if (input == "bc1") return TextureCompression::BC1;
		if (input == "bc2") return TextureCompression::BC2;
		if (input == "bc3") return TextureCompression::BC3;
		if (input == "bc4") return TextureCompression::BC4;
		if (input == "bc5") return TextureCompression::BC5;
		if (input == "bc6hdr") return TextureCompression::BC6H;
		if (input == "bc7") return TextureCompression::BC7;
	}

	enum class TextureCubeFormat {
		Cube, Equi, Paraboloid
	};
	//static inline const char* string_TextureCubeFormat(TextureCubeFormat input) {
	//	switch (input) {
	//	case(TextureCubeFormat::Cube):
	//		return "cube";
	//	case(TextureCubeFormat::Equi):
	//		return "equi";
	//	case(TextureCubeFormat::Paraboloid):
	//		return "paraboloid";
	//	}
	//}
	//static inline TextureCubeFormat enum_TextureCubeFormat(const std::string& input) {
	//	if (input == "cube") return TextureCubeFormat::Cube;
	//	if (input == "equi") return TextureCubeFormat::Equi;
	//	if (input == "paraboloid") return TextureCubeFormat::Paraboloid;
	//}

	static inline VkFormat getVkFormatTextureFormat(TextureFormat format, TextureColorSpace cSpace, TextureChannels channels, TextureCompression compression) {
		if (compression == TextureCompression::Lossless) {
			if (format == TextureFormat::Unorm8 && cSpace == TextureColorSpace::Linear) {
				switch (channels) {
				case (TextureChannels::R): return VK_FORMAT_R8_UNORM;
				case (TextureChannels::RG): return VK_FORMAT_R8G8_UNORM;
				case (TextureChannels::RGB):
				case (TextureChannels::RGBA): return VK_FORMAT_R8G8B8A8_UNORM;
				}
			} else if (format == TextureFormat::Unorm8 && cSpace == TextureColorSpace::sRGB) {
				switch (channels) {
				case (TextureChannels::R): return VK_FORMAT_R8_SRGB;
				case (TextureChannels::RG): return VK_FORMAT_R8G8_SRGB;
				case (TextureChannels::RGB):
				case (TextureChannels::RGBA): return VK_FORMAT_R8G8B8A8_SRGB;
				}
			} else if (format == TextureFormat::Unorm16 && cSpace == TextureColorSpace::Linear) {
				switch (channels) {
				case (TextureChannels::R): return VK_FORMAT_R16_UNORM;
				case (TextureChannels::RG): return VK_FORMAT_R16G16_UNORM;
				case (TextureChannels::RGB):
				case (TextureChannels::RGBA): return VK_FORMAT_R16G16B16A16_UNORM;
				}
			} else if (format == TextureFormat::Floating16 && cSpace == TextureColorSpace::Linear) {
				switch (channels) {
				case (TextureChannels::R): return VK_FORMAT_R16_SFLOAT;
				case (TextureChannels::RG): return VK_FORMAT_R16G16_SFLOAT;
				case (TextureChannels::RGB):
				case (TextureChannels::RGBA): return VK_FORMAT_R16G16B16A16_SFLOAT;
				}
			} else if (format == TextureFormat::Floating32 && cSpace == TextureColorSpace::Linear) {
				switch (channels) {
				case (TextureChannels::R): return VK_FORMAT_R32_SFLOAT;
				case (TextureChannels::RG): return VK_FORMAT_R32G32_SFLOAT;
				case (TextureChannels::RGB):
				case (TextureChannels::RGBA): return VK_FORMAT_R32G32B32A32_SFLOAT;
				}
			} else if (format == TextureFormat::FloatingEBGR && cSpace == TextureColorSpace::Linear) {
				switch (channels) {
				case (TextureChannels::RGB): return VK_FORMAT_E5B9G9R9_UFLOAT_PACK32;
				}
			} else {
				SHARD3D_ERROR("Lossless format combination with colour space is not a valid format! Falling back to VK_FORMAT_R8G8B8A8_UNORM");
				return VK_FORMAT_R8G8B8A8_SRGB;
			}
		} else if (compression == TextureCompression::BC1 && format == TextureFormat::Unorm8) {
			if (cSpace == TextureColorSpace::Linear ) {
				switch (channels) {
				case (TextureChannels::RGB): return VK_FORMAT_BC1_RGB_UNORM_BLOCK;
				//case (TextureChannels::RGBA): return VK_FORMAT_BC1_RGBA_UNORM_BLOCK;
				default: SHARD3D_ERROR("BC1 only supports 3 channels!");
				}
			} else if (cSpace == TextureColorSpace::sRGB) {
				switch (channels) {
				case (TextureChannels::RGB): return VK_FORMAT_BC1_RGB_SRGB_BLOCK;
				//case (TextureChannels::RGBA): return VK_FORMAT_BC1_RGBA_SRGB_BLOCK;
				default: SHARD3D_ERROR("BC1 only supports 3 channels!");
				// KTX2 does not support a 1 bit alpha channel for BC1
				}
			}
		} else if (compression == TextureCompression::BC3 && format == TextureFormat::Unorm8) {
			if (cSpace == TextureColorSpace::Linear) {
				switch (channels) {
				case (TextureChannels::RGB):
				case (TextureChannels::RGBA): return VK_FORMAT_BC3_UNORM_BLOCK;
				default: SHARD3D_ERROR("BC3 only supports 3-4 channels!");
				}
			} else if (cSpace == TextureColorSpace::sRGB) {
				switch (channels) {
				case (TextureChannels::RGB):
				case (TextureChannels::RGBA): return VK_FORMAT_BC3_SRGB_BLOCK;
				default: SHARD3D_ERROR("BC3 only supports 3-4 channels!");
				}
			}
		} else if (compression == TextureCompression::BC4 && format == TextureFormat::Unorm8) {
			if (cSpace == TextureColorSpace::Linear) {
				switch (channels) {
				case (TextureChannels::R): return VK_FORMAT_BC4_UNORM_BLOCK;
				default: SHARD3D_ERROR("BC4 only supports 1 channel!");
				}
			} else if (cSpace == TextureColorSpace::sRGB) {
				SHARD3D_ERROR("BC4 only supports a linear colour space!");
				switch (channels) {
				case (TextureChannels::R): return VK_FORMAT_BC4_UNORM_BLOCK;
				default: SHARD3D_ERROR("BC4 only supports 3-4 channels!");
				}
			}
		} else if (compression == TextureCompression::BC5 && format == TextureFormat::Unorm8) {
			if (cSpace == TextureColorSpace::Linear) {
				switch (channels) {
				case (TextureChannels::RG): return VK_FORMAT_BC5_UNORM_BLOCK;
				default: SHARD3D_ERROR("BC5 only supports 2 channels!");
				}
			}
		} else if (compression == TextureCompression::BC7 && format == TextureFormat::Unorm8) {
			if (cSpace == TextureColorSpace::Linear) {
				switch (channels) {
				case (TextureChannels::RGB):
				case (TextureChannels::RGBA): return VK_FORMAT_BC7_UNORM_BLOCK;
				}
			} else if (cSpace == TextureColorSpace::sRGB) {
				switch (channels) {
				case (TextureChannels::RGB):
				case (TextureChannels::RGBA): return VK_FORMAT_BC7_SRGB_BLOCK;
				}
			}
		}

		// KTX2 does not support BC2 and BC6H, hence they are not included here

		SHARD3D_ERROR("Compressed format combination with colour space is not a valid format! Falling back to VK_FORMAT_R8G8B8A8_UNORM");
		return VK_FORMAT_R8G8B8A8_SRGB;
	}

	static inline VkFormat enum_VkFormat(const std::string& input_value) {
		if (input_value == "VK_FORMAT_A1R5G5B5_UNORM_PACK16")
		   return VK_FORMAT_A1R5G5B5_UNORM_PACK16;
		if (input_value == "VK_FORMAT_A2B10G10R10_SINT_PACK32")
		   return VK_FORMAT_A2B10G10R10_SINT_PACK32;
		if (input_value == "VK_FORMAT_A2B10G10R10_SNORM_PACK32")
		   return VK_FORMAT_A2B10G10R10_SNORM_PACK32;
		if (input_value == "VK_FORMAT_A2B10G10R10_SSCALED_PACK32")
		   return VK_FORMAT_A2B10G10R10_SSCALED_PACK32;
		if (input_value == "VK_FORMAT_A2B10G10R10_UINT_PACK32")
		   return VK_FORMAT_A2B10G10R10_UINT_PACK32;
		if (input_value == "VK_FORMAT_A2B10G10R10_UNORM_PACK32")
		   return VK_FORMAT_A2B10G10R10_UNORM_PACK32;
		if (input_value == "VK_FORMAT_A2B10G10R10_USCALED_PACK32")
		   return VK_FORMAT_A2B10G10R10_USCALED_PACK32;
		if (input_value == "VK_FORMAT_A2R10G10B10_SINT_PACK32")
		   return VK_FORMAT_A2R10G10B10_SINT_PACK32;
		if (input_value == "VK_FORMAT_A2R10G10B10_SNORM_PACK32")
		   return VK_FORMAT_A2R10G10B10_SNORM_PACK32;
		if (input_value == "VK_FORMAT_A2R10G10B10_SSCALED_PACK32")
		   return VK_FORMAT_A2R10G10B10_SSCALED_PACK32;
		if (input_value == "VK_FORMAT_A2R10G10B10_UINT_PACK32")
		   return VK_FORMAT_A2R10G10B10_UINT_PACK32;
		if (input_value == "VK_FORMAT_A2R10G10B10_UNORM_PACK32")
		   return VK_FORMAT_A2R10G10B10_UNORM_PACK32;
		if (input_value == "VK_FORMAT_A2R10G10B10_USCALED_PACK32")
		   return VK_FORMAT_A2R10G10B10_USCALED_PACK32;
		if (input_value == "VK_FORMAT_A4B4G4R4_UNORM_PACK16")
		   return VK_FORMAT_A4B4G4R4_UNORM_PACK16;
		if (input_value == "VK_FORMAT_A4R4G4B4_UNORM_PACK16")
		   return VK_FORMAT_A4R4G4B4_UNORM_PACK16;
		if (input_value == "VK_FORMAT_A8B8G8R8_SINT_PACK32")
		   return VK_FORMAT_A8B8G8R8_SINT_PACK32;
		if (input_value == "VK_FORMAT_A8B8G8R8_SNORM_PACK32")
		   return VK_FORMAT_A8B8G8R8_SNORM_PACK32;
		if (input_value == "VK_FORMAT_A8B8G8R8_SRGB_PACK32")
		   return VK_FORMAT_A8B8G8R8_SRGB_PACK32;
		if (input_value == "VK_FORMAT_A8B8G8R8_SSCALED_PACK32")
		   return VK_FORMAT_A8B8G8R8_SSCALED_PACK32;
		if (input_value == "VK_FORMAT_A8B8G8R8_UINT_PACK32")
		   return VK_FORMAT_A8B8G8R8_UINT_PACK32;
		if (input_value == "VK_FORMAT_A8B8G8R8_UNORM_PACK32")
		   return VK_FORMAT_A8B8G8R8_UNORM_PACK32;
		if (input_value == "VK_FORMAT_A8B8G8R8_USCALED_PACK32")
		   return VK_FORMAT_A8B8G8R8_USCALED_PACK32;
		if (input_value == "VK_FORMAT_ASTC_10x10_SFLOAT_BLOCK")
		   return VK_FORMAT_ASTC_10x10_SFLOAT_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_10x10_SRGB_BLOCK")
		   return VK_FORMAT_ASTC_10x10_SRGB_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_10x10_UNORM_BLOCK")
		   return VK_FORMAT_ASTC_10x10_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_10x5_SFLOAT_BLOCK")
		   return VK_FORMAT_ASTC_10x5_SFLOAT_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_10x5_SRGB_BLOCK")
		   return VK_FORMAT_ASTC_10x5_SRGB_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_10x5_UNORM_BLOCK")
		   return VK_FORMAT_ASTC_10x5_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_10x6_SFLOAT_BLOCK")
		   return VK_FORMAT_ASTC_10x6_SFLOAT_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_10x6_SRGB_BLOCK")
		   return VK_FORMAT_ASTC_10x6_SRGB_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_10x6_UNORM_BLOCK")
		   return VK_FORMAT_ASTC_10x6_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_10x8_SFLOAT_BLOCK")
		   return VK_FORMAT_ASTC_10x8_SFLOAT_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_10x8_SRGB_BLOCK")
		   return VK_FORMAT_ASTC_10x8_SRGB_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_10x8_UNORM_BLOCK")
		   return VK_FORMAT_ASTC_10x8_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_12x10_SFLOAT_BLOCK")
		   return VK_FORMAT_ASTC_12x10_SFLOAT_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_12x10_SRGB_BLOCK")
		   return VK_FORMAT_ASTC_12x10_SRGB_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_12x10_UNORM_BLOCK")
		   return VK_FORMAT_ASTC_12x10_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_12x12_SFLOAT_BLOCK")
		   return VK_FORMAT_ASTC_12x12_SFLOAT_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_12x12_SRGB_BLOCK")
		   return VK_FORMAT_ASTC_12x12_SRGB_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_12x12_UNORM_BLOCK")
		   return VK_FORMAT_ASTC_12x12_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_4x4_SFLOAT_BLOCK")
		   return VK_FORMAT_ASTC_4x4_SFLOAT_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_4x4_SRGB_BLOCK")
		   return VK_FORMAT_ASTC_4x4_SRGB_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_4x4_UNORM_BLOCK")
		   return VK_FORMAT_ASTC_4x4_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_5x4_SFLOAT_BLOCK")
		   return VK_FORMAT_ASTC_5x4_SFLOAT_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_5x4_SRGB_BLOCK")
		   return VK_FORMAT_ASTC_5x4_SRGB_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_5x4_UNORM_BLOCK")
		   return VK_FORMAT_ASTC_5x4_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_5x5_SFLOAT_BLOCK")
		   return VK_FORMAT_ASTC_5x5_SFLOAT_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_5x5_SRGB_BLOCK")
		   return VK_FORMAT_ASTC_5x5_SRGB_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_5x5_UNORM_BLOCK")
		   return VK_FORMAT_ASTC_5x5_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_6x5_SFLOAT_BLOCK")
		   return VK_FORMAT_ASTC_6x5_SFLOAT_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_6x5_SRGB_BLOCK")
		   return VK_FORMAT_ASTC_6x5_SRGB_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_6x5_UNORM_BLOCK")
		   return VK_FORMAT_ASTC_6x5_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_6x6_SFLOAT_BLOCK")
		   return VK_FORMAT_ASTC_6x6_SFLOAT_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_6x6_SRGB_BLOCK")
		   return VK_FORMAT_ASTC_6x6_SRGB_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_6x6_UNORM_BLOCK")
		   return VK_FORMAT_ASTC_6x6_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_8x5_SFLOAT_BLOCK")
		   return VK_FORMAT_ASTC_8x5_SFLOAT_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_8x5_SRGB_BLOCK")
		   return VK_FORMAT_ASTC_8x5_SRGB_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_8x5_UNORM_BLOCK")
		   return VK_FORMAT_ASTC_8x5_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_8x6_SFLOAT_BLOCK")
		   return VK_FORMAT_ASTC_8x6_SFLOAT_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_8x6_SRGB_BLOCK")
		   return VK_FORMAT_ASTC_8x6_SRGB_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_8x6_UNORM_BLOCK")
		   return VK_FORMAT_ASTC_8x6_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_8x8_SFLOAT_BLOCK")
		   return VK_FORMAT_ASTC_8x8_SFLOAT_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_8x8_SRGB_BLOCK")
		   return VK_FORMAT_ASTC_8x8_SRGB_BLOCK;
		if (input_value == "VK_FORMAT_ASTC_8x8_UNORM_BLOCK")
		   return VK_FORMAT_ASTC_8x8_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_B10G11R11_UFLOAT_PACK32")
		   return VK_FORMAT_B10G11R11_UFLOAT_PACK32;
		if (input_value == "VK_FORMAT_B10X6G10X6R10X6G10X6_422_UNORM_4PACK16")
		   return VK_FORMAT_B10X6G10X6R10X6G10X6_422_UNORM_4PACK16;
		if (input_value == "VK_FORMAT_B12X4G12X4R12X4G12X4_422_UNORM_4PACK16")
		   return VK_FORMAT_B12X4G12X4R12X4G12X4_422_UNORM_4PACK16;
		if (input_value == "VK_FORMAT_B16G16R16G16_422_UNORM")
		   return VK_FORMAT_B16G16R16G16_422_UNORM;
		if (input_value == "VK_FORMAT_B4G4R4A4_UNORM_PACK16")
		   return VK_FORMAT_B4G4R4A4_UNORM_PACK16;
		if (input_value == "VK_FORMAT_B5G5R5A1_UNORM_PACK16")
		   return VK_FORMAT_B5G5R5A1_UNORM_PACK16;
		if (input_value == "VK_FORMAT_B5G6R5_UNORM_PACK16")
		   return VK_FORMAT_B5G6R5_UNORM_PACK16;
		if (input_value == "VK_FORMAT_B8G8R8A8_SINT")
		   return VK_FORMAT_B8G8R8A8_SINT;
		if (input_value == "VK_FORMAT_B8G8R8A8_SNORM")
		   return VK_FORMAT_B8G8R8A8_SNORM;
		if (input_value == "VK_FORMAT_B8G8R8A8_SRGB")
		   return VK_FORMAT_B8G8R8A8_SRGB;
		if (input_value == "VK_FORMAT_B8G8R8A8_SSCALED")
		   return VK_FORMAT_B8G8R8A8_SSCALED;
		if (input_value == "VK_FORMAT_B8G8R8A8_UINT")
		   return VK_FORMAT_B8G8R8A8_UINT;
		if (input_value == "VK_FORMAT_B8G8R8A8_UNORM")
		   return VK_FORMAT_B8G8R8A8_UNORM;
		if (input_value == "VK_FORMAT_B8G8R8A8_USCALED")
		   return VK_FORMAT_B8G8R8A8_USCALED;
		if (input_value == "VK_FORMAT_B8G8R8G8_422_UNORM")
		   return VK_FORMAT_B8G8R8G8_422_UNORM;
		if (input_value == "VK_FORMAT_B8G8R8_SINT")
		   return VK_FORMAT_B8G8R8_SINT;
		if (input_value == "VK_FORMAT_B8G8R8_SNORM")
		   return VK_FORMAT_B8G8R8_SNORM;
		if (input_value == "VK_FORMAT_B8G8R8_SRGB")
		   return VK_FORMAT_B8G8R8_SRGB;
		if (input_value == "VK_FORMAT_B8G8R8_SSCALED")
		   return VK_FORMAT_B8G8R8_SSCALED;
		if (input_value == "VK_FORMAT_B8G8R8_UINT")
		   return VK_FORMAT_B8G8R8_UINT;
		if (input_value == "VK_FORMAT_B8G8R8_UNORM")
		   return VK_FORMAT_B8G8R8_UNORM;
		if (input_value == "VK_FORMAT_B8G8R8_USCALED")
		   return VK_FORMAT_B8G8R8_USCALED;
		if (input_value == "VK_FORMAT_BC1_RGBA_SRGB_BLOCK")
		   return VK_FORMAT_BC1_RGBA_SRGB_BLOCK;
		if (input_value == "VK_FORMAT_BC1_RGBA_UNORM_BLOCK")
		   return VK_FORMAT_BC1_RGBA_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_BC1_RGB_SRGB_BLOCK")
		   return VK_FORMAT_BC1_RGB_SRGB_BLOCK;
		if (input_value == "VK_FORMAT_BC1_RGB_UNORM_BLOCK")
		   return VK_FORMAT_BC1_RGB_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_BC2_SRGB_BLOCK")
		   return VK_FORMAT_BC2_SRGB_BLOCK;
		if (input_value == "VK_FORMAT_BC2_UNORM_BLOCK")
		   return VK_FORMAT_BC2_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_BC3_SRGB_BLOCK")
		   return VK_FORMAT_BC3_SRGB_BLOCK;
		if (input_value == "VK_FORMAT_BC3_UNORM_BLOCK")
		   return VK_FORMAT_BC3_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_BC4_SNORM_BLOCK")
		   return VK_FORMAT_BC4_SNORM_BLOCK;
		if (input_value == "VK_FORMAT_BC4_UNORM_BLOCK")
		   return VK_FORMAT_BC4_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_BC5_SNORM_BLOCK")
		   return VK_FORMAT_BC5_SNORM_BLOCK;
		if (input_value == "VK_FORMAT_BC5_UNORM_BLOCK")
		   return VK_FORMAT_BC5_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_BC6H_SFLOAT_BLOCK")
		   return VK_FORMAT_BC6H_SFLOAT_BLOCK;
		if (input_value == "VK_FORMAT_BC6H_UFLOAT_BLOCK")
		   return VK_FORMAT_BC6H_UFLOAT_BLOCK;
		if (input_value == "VK_FORMAT_BC7_SRGB_BLOCK")
		   return VK_FORMAT_BC7_SRGB_BLOCK;
		if (input_value == "VK_FORMAT_BC7_UNORM_BLOCK")
		   return VK_FORMAT_BC7_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_D16_UNORM")
		   return VK_FORMAT_D16_UNORM;
		if (input_value == "VK_FORMAT_D16_UNORM_S8_UINT")
		   return VK_FORMAT_D16_UNORM_S8_UINT;
		if (input_value == "VK_FORMAT_D24_UNORM_S8_UINT")
		   return VK_FORMAT_D24_UNORM_S8_UINT;
		if (input_value == "VK_FORMAT_D32_SFLOAT")
		   return VK_FORMAT_D32_SFLOAT;
		if (input_value == "VK_FORMAT_D32_SFLOAT_S8_UINT")
		   return VK_FORMAT_D32_SFLOAT_S8_UINT;
		if (input_value == "VK_FORMAT_E5B9G9R9_UFLOAT_PACK32")
		   return VK_FORMAT_E5B9G9R9_UFLOAT_PACK32;
		if (input_value == "VK_FORMAT_EAC_R11G11_SNORM_BLOCK")
		   return VK_FORMAT_EAC_R11G11_SNORM_BLOCK;
		if (input_value == "VK_FORMAT_EAC_R11G11_UNORM_BLOCK")
		   return VK_FORMAT_EAC_R11G11_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_EAC_R11_SNORM_BLOCK")
		   return VK_FORMAT_EAC_R11_SNORM_BLOCK;
		if (input_value == "VK_FORMAT_EAC_R11_UNORM_BLOCK")
		   return VK_FORMAT_EAC_R11_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_ETC2_R8G8B8A1_SRGB_BLOCK")
		   return VK_FORMAT_ETC2_R8G8B8A1_SRGB_BLOCK;
		if (input_value == "VK_FORMAT_ETC2_R8G8B8A1_UNORM_BLOCK")
		   return VK_FORMAT_ETC2_R8G8B8A1_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_ETC2_R8G8B8A8_SRGB_BLOCK")
		   return VK_FORMAT_ETC2_R8G8B8A8_SRGB_BLOCK;
		if (input_value == "VK_FORMAT_ETC2_R8G8B8A8_UNORM_BLOCK")
		   return VK_FORMAT_ETC2_R8G8B8A8_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_ETC2_R8G8B8_SRGB_BLOCK")
		   return VK_FORMAT_ETC2_R8G8B8_SRGB_BLOCK;
		if (input_value == "VK_FORMAT_ETC2_R8G8B8_UNORM_BLOCK")
		   return VK_FORMAT_ETC2_R8G8B8_UNORM_BLOCK;
		if (input_value == "VK_FORMAT_G10X6B10X6G10X6R10X6_422_UNORM_4PACK16")
		   return VK_FORMAT_G10X6B10X6G10X6R10X6_422_UNORM_4PACK16;
		if (input_value == "VK_FORMAT_G10X6_B10X6R10X6_2PLANE_420_UNORM_3PACK16")
		   return VK_FORMAT_G10X6_B10X6R10X6_2PLANE_420_UNORM_3PACK16;
		if (input_value == "VK_FORMAT_G10X6_B10X6R10X6_2PLANE_422_UNORM_3PACK16")
		   return VK_FORMAT_G10X6_B10X6R10X6_2PLANE_422_UNORM_3PACK16;
		if (input_value == "VK_FORMAT_G10X6_B10X6R10X6_2PLANE_444_UNORM_3PACK16")
		   return VK_FORMAT_G10X6_B10X6R10X6_2PLANE_444_UNORM_3PACK16;
		if (input_value == "VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_420_UNORM_3PACK16")
		   return VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_420_UNORM_3PACK16;
		if (input_value == "VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_422_UNORM_3PACK16")
		   return VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_422_UNORM_3PACK16;
		if (input_value == "VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_444_UNORM_3PACK16")
		   return VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_444_UNORM_3PACK16;
		if (input_value == "VK_FORMAT_G12X4B12X4G12X4R12X4_422_UNORM_4PACK16")
		   return VK_FORMAT_G12X4B12X4G12X4R12X4_422_UNORM_4PACK16;
		if (input_value == "VK_FORMAT_G12X4_B12X4R12X4_2PLANE_420_UNORM_3PACK16")
		   return VK_FORMAT_G12X4_B12X4R12X4_2PLANE_420_UNORM_3PACK16;
		if (input_value == "VK_FORMAT_G12X4_B12X4R12X4_2PLANE_422_UNORM_3PACK16")
		   return VK_FORMAT_G12X4_B12X4R12X4_2PLANE_422_UNORM_3PACK16;
		if (input_value == "VK_FORMAT_G12X4_B12X4R12X4_2PLANE_444_UNORM_3PACK16")
		   return VK_FORMAT_G12X4_B12X4R12X4_2PLANE_444_UNORM_3PACK16;
		if (input_value == "VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_420_UNORM_3PACK16")
		   return VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_420_UNORM_3PACK16;
		if (input_value == "VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_422_UNORM_3PACK16")
		   return VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_422_UNORM_3PACK16;
		if (input_value == "VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_444_UNORM_3PACK16")
		   return VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_444_UNORM_3PACK16;
		if (input_value == "VK_FORMAT_G16B16G16R16_422_UNORM")
		   return VK_FORMAT_G16B16G16R16_422_UNORM;
		if (input_value == "VK_FORMAT_G16_B16R16_2PLANE_420_UNORM")
		   return VK_FORMAT_G16_B16R16_2PLANE_420_UNORM;
		if (input_value == "VK_FORMAT_G16_B16R16_2PLANE_422_UNORM")
		   return VK_FORMAT_G16_B16R16_2PLANE_422_UNORM;
		if (input_value == "VK_FORMAT_G16_B16R16_2PLANE_444_UNORM")
		   return VK_FORMAT_G16_B16R16_2PLANE_444_UNORM;
		if (input_value == "VK_FORMAT_G16_B16_R16_3PLANE_420_UNORM")
		   return VK_FORMAT_G16_B16_R16_3PLANE_420_UNORM;
		if (input_value == "VK_FORMAT_G16_B16_R16_3PLANE_422_UNORM")
		   return VK_FORMAT_G16_B16_R16_3PLANE_422_UNORM;
		if (input_value == "VK_FORMAT_G16_B16_R16_3PLANE_444_UNORM")
		   return VK_FORMAT_G16_B16_R16_3PLANE_444_UNORM;
		if (input_value == "VK_FORMAT_G8B8G8R8_422_UNORM")
		   return VK_FORMAT_G8B8G8R8_422_UNORM;
		if (input_value == "VK_FORMAT_G8_B8R8_2PLANE_420_UNORM")
		   return VK_FORMAT_G8_B8R8_2PLANE_420_UNORM;
		if (input_value == "VK_FORMAT_G8_B8R8_2PLANE_422_UNORM")
		   return VK_FORMAT_G8_B8R8_2PLANE_422_UNORM;
		if (input_value == "VK_FORMAT_G8_B8R8_2PLANE_444_UNORM")
		   return VK_FORMAT_G8_B8R8_2PLANE_444_UNORM;
		if (input_value == "VK_FORMAT_G8_B8_R8_3PLANE_420_UNORM")
		   return VK_FORMAT_G8_B8_R8_3PLANE_420_UNORM;
		if (input_value == "VK_FORMAT_G8_B8_R8_3PLANE_422_UNORM")
		   return VK_FORMAT_G8_B8_R8_3PLANE_422_UNORM;
		if (input_value == "VK_FORMAT_G8_B8_R8_3PLANE_444_UNORM")
		   return VK_FORMAT_G8_B8_R8_3PLANE_444_UNORM;
		if (input_value == "VK_FORMAT_PVRTC1_2BPP_SRGB_BLOCK_IMG")
		   return VK_FORMAT_PVRTC1_2BPP_SRGB_BLOCK_IMG;
		if (input_value == "VK_FORMAT_PVRTC1_2BPP_UNORM_BLOCK_IMG")
		   return VK_FORMAT_PVRTC1_2BPP_UNORM_BLOCK_IMG;
		if (input_value == "VK_FORMAT_PVRTC1_4BPP_SRGB_BLOCK_IMG")
		   return VK_FORMAT_PVRTC1_4BPP_SRGB_BLOCK_IMG;
		if (input_value == "VK_FORMAT_PVRTC1_4BPP_UNORM_BLOCK_IMG")
		   return VK_FORMAT_PVRTC1_4BPP_UNORM_BLOCK_IMG;
		if (input_value == "VK_FORMAT_PVRTC2_2BPP_SRGB_BLOCK_IMG")
		   return VK_FORMAT_PVRTC2_2BPP_SRGB_BLOCK_IMG;
		if (input_value == "VK_FORMAT_PVRTC2_2BPP_UNORM_BLOCK_IMG")
		   return VK_FORMAT_PVRTC2_2BPP_UNORM_BLOCK_IMG;
		if (input_value == "VK_FORMAT_PVRTC2_4BPP_SRGB_BLOCK_IMG")
		   return VK_FORMAT_PVRTC2_4BPP_SRGB_BLOCK_IMG;
		if (input_value == "VK_FORMAT_PVRTC2_4BPP_UNORM_BLOCK_IMG")
		   return VK_FORMAT_PVRTC2_4BPP_UNORM_BLOCK_IMG;
		if (input_value == "VK_FORMAT_R10X6G10X6B10X6A10X6_UNORM_4PACK16")
		   return VK_FORMAT_R10X6G10X6B10X6A10X6_UNORM_4PACK16;
		if (input_value == "VK_FORMAT_R10X6G10X6_UNORM_2PACK16")
		   return VK_FORMAT_R10X6G10X6_UNORM_2PACK16;
		if (input_value == "VK_FORMAT_R10X6_UNORM_PACK16")
		   return VK_FORMAT_R10X6_UNORM_PACK16;
		if (input_value == "VK_FORMAT_R12X4G12X4B12X4A12X4_UNORM_4PACK16")
		   return VK_FORMAT_R12X4G12X4B12X4A12X4_UNORM_4PACK16;
		if (input_value == "VK_FORMAT_R12X4G12X4_UNORM_2PACK16")
		   return VK_FORMAT_R12X4G12X4_UNORM_2PACK16;
		if (input_value == "VK_FORMAT_R12X4_UNORM_PACK16")
		   return VK_FORMAT_R12X4_UNORM_PACK16;
		if (input_value == "VK_FORMAT_R16G16B16A16_SFLOAT")
		   return VK_FORMAT_R16G16B16A16_SFLOAT;
		if (input_value == "VK_FORMAT_R16G16B16A16_SINT")
		   return VK_FORMAT_R16G16B16A16_SINT;
		if (input_value == "VK_FORMAT_R16G16B16A16_SNORM")
		   return VK_FORMAT_R16G16B16A16_SNORM;
		if (input_value == "VK_FORMAT_R16G16B16A16_SSCALED")
		   return VK_FORMAT_R16G16B16A16_SSCALED;
		if (input_value == "VK_FORMAT_R16G16B16A16_UINT")
		   return VK_FORMAT_R16G16B16A16_UINT;
		if (input_value == "VK_FORMAT_R16G16B16A16_UNORM")
		   return VK_FORMAT_R16G16B16A16_UNORM;
		if (input_value == "VK_FORMAT_R16G16B16A16_USCALED")
		   return VK_FORMAT_R16G16B16A16_USCALED;
		if (input_value == "VK_FORMAT_R16G16B16_SFLOAT")
		   return VK_FORMAT_R16G16B16_SFLOAT;
		if (input_value == "VK_FORMAT_R16G16B16_SINT")
		   return VK_FORMAT_R16G16B16_SINT;
		if (input_value == "VK_FORMAT_R16G16B16_SNORM")
		   return VK_FORMAT_R16G16B16_SNORM;
		if (input_value == "VK_FORMAT_R16G16B16_SSCALED")
		   return VK_FORMAT_R16G16B16_SSCALED;
		if (input_value == "VK_FORMAT_R16G16B16_UINT")
		   return VK_FORMAT_R16G16B16_UINT;
		if (input_value == "VK_FORMAT_R16G16B16_UNORM")
		   return VK_FORMAT_R16G16B16_UNORM;
		if (input_value == "VK_FORMAT_R16G16B16_USCALED")
		   return VK_FORMAT_R16G16B16_USCALED;
		if (input_value == "VK_FORMAT_R16G16_SFLOAT")
		   return VK_FORMAT_R16G16_SFLOAT;
		if (input_value == "VK_FORMAT_R16G16_SINT")
		   return VK_FORMAT_R16G16_SINT;
		if (input_value == "VK_FORMAT_R16G16_SNORM")
		   return VK_FORMAT_R16G16_SNORM;
		if (input_value == "VK_FORMAT_R16G16_SSCALED")
		   return VK_FORMAT_R16G16_SSCALED;
		if (input_value == "VK_FORMAT_R16G16_UINT")
		   return VK_FORMAT_R16G16_UINT;
		if (input_value == "VK_FORMAT_R16G16_UNORM")
		   return VK_FORMAT_R16G16_UNORM;
		if (input_value == "VK_FORMAT_R16G16_USCALED")
		   return VK_FORMAT_R16G16_USCALED;
		if (input_value == "VK_FORMAT_R16_SFLOAT")
		   return VK_FORMAT_R16_SFLOAT;
		if (input_value == "VK_FORMAT_R16_SINT")
		   return VK_FORMAT_R16_SINT;
		if (input_value == "VK_FORMAT_R16_SNORM")
		   return VK_FORMAT_R16_SNORM;
		if (input_value == "VK_FORMAT_R16_SSCALED")
		   return VK_FORMAT_R16_SSCALED;
		if (input_value == "VK_FORMAT_R16_UINT")
		   return VK_FORMAT_R16_UINT;
		if (input_value == "VK_FORMAT_R16_UNORM")
		   return VK_FORMAT_R16_UNORM;
		if (input_value == "VK_FORMAT_R16_USCALED")
		   return VK_FORMAT_R16_USCALED;
		if (input_value == "VK_FORMAT_R32G32B32A32_SFLOAT")
		   return VK_FORMAT_R32G32B32A32_SFLOAT;
		if (input_value == "VK_FORMAT_R32G32B32A32_SINT")
		   return VK_FORMAT_R32G32B32A32_SINT;
		if (input_value == "VK_FORMAT_R32G32B32A32_UINT")
		   return VK_FORMAT_R32G32B32A32_UINT;
		if (input_value == "VK_FORMAT_R32G32B32_SFLOAT")
		   return VK_FORMAT_R32G32B32_SFLOAT;
		if (input_value == "VK_FORMAT_R32G32B32_SINT")
		   return VK_FORMAT_R32G32B32_SINT;
		if (input_value == "VK_FORMAT_R32G32B32_UINT")
		   return VK_FORMAT_R32G32B32_UINT;
		if (input_value == "VK_FORMAT_R32G32_SFLOAT")
		   return VK_FORMAT_R32G32_SFLOAT;
		if (input_value == "VK_FORMAT_R32G32_SINT")
		   return VK_FORMAT_R32G32_SINT;
		if (input_value == "VK_FORMAT_R32G32_UINT")
		   return VK_FORMAT_R32G32_UINT;
		if (input_value == "VK_FORMAT_R32_SFLOAT")
		   return VK_FORMAT_R32_SFLOAT;
		if (input_value == "VK_FORMAT_R32_SINT")
		   return VK_FORMAT_R32_SINT;
		if (input_value == "VK_FORMAT_R32_UINT")
		   return VK_FORMAT_R32_UINT;
		if (input_value == "VK_FORMAT_R4G4B4A4_UNORM_PACK16")
		   return VK_FORMAT_R4G4B4A4_UNORM_PACK16;
		if (input_value == "VK_FORMAT_R4G4_UNORM_PACK8")
		   return VK_FORMAT_R4G4_UNORM_PACK8;
		if (input_value == "VK_FORMAT_R5G5B5A1_UNORM_PACK16")
		   return VK_FORMAT_R5G5B5A1_UNORM_PACK16;
		if (input_value == "VK_FORMAT_R5G6B5_UNORM_PACK16")
		   return VK_FORMAT_R5G6B5_UNORM_PACK16;
		if (input_value == "VK_FORMAT_R64G64B64A64_SFLOAT")
		   return VK_FORMAT_R64G64B64A64_SFLOAT;
		if (input_value == "VK_FORMAT_R64G64B64A64_SINT")
		   return VK_FORMAT_R64G64B64A64_SINT;
		if (input_value == "VK_FORMAT_R64G64B64A64_UINT")
		   return VK_FORMAT_R64G64B64A64_UINT;
		if (input_value == "VK_FORMAT_R64G64B64_SFLOAT")
		   return VK_FORMAT_R64G64B64_SFLOAT;
		if (input_value == "VK_FORMAT_R64G64B64_SINT")
		   return VK_FORMAT_R64G64B64_SINT;
		if (input_value == "VK_FORMAT_R64G64B64_UINT")
		   return VK_FORMAT_R64G64B64_UINT;
		if (input_value == "VK_FORMAT_R64G64_SFLOAT")
		   return VK_FORMAT_R64G64_SFLOAT;
		if (input_value == "VK_FORMAT_R64G64_SINT")
		   return VK_FORMAT_R64G64_SINT;
		if (input_value == "VK_FORMAT_R64G64_UINT")
		   return VK_FORMAT_R64G64_UINT;
		if (input_value == "VK_FORMAT_R64_SFLOAT")
		   return VK_FORMAT_R64_SFLOAT;
		if (input_value == "VK_FORMAT_R64_SINT")
		   return VK_FORMAT_R64_SINT;
		if (input_value == "VK_FORMAT_R64_UINT")
		   return VK_FORMAT_R64_UINT;
		if (input_value == "VK_FORMAT_R8G8B8A8_SINT")
		   return VK_FORMAT_R8G8B8A8_SINT;
		if (input_value == "VK_FORMAT_R8G8B8A8_SNORM")
		   return VK_FORMAT_R8G8B8A8_SNORM;
		if (input_value == "VK_FORMAT_R8G8B8A8_SRGB")
		   return VK_FORMAT_R8G8B8A8_SRGB;
		if (input_value == "VK_FORMAT_R8G8B8A8_SSCALED")
		   return VK_FORMAT_R8G8B8A8_SSCALED;
		if (input_value == "VK_FORMAT_R8G8B8A8_UINT")
		   return VK_FORMAT_R8G8B8A8_UINT;
		if (input_value == "VK_FORMAT_R8G8B8A8_UNORM")
		   return VK_FORMAT_R8G8B8A8_UNORM;
		if (input_value == "VK_FORMAT_R8G8B8A8_USCALED")
		   return VK_FORMAT_R8G8B8A8_USCALED;
		if (input_value == "VK_FORMAT_R8G8B8_SINT")
		   return VK_FORMAT_R8G8B8_SINT;
		if (input_value == "VK_FORMAT_R8G8B8_SNORM")
		   return VK_FORMAT_R8G8B8_SNORM;
		if (input_value == "VK_FORMAT_R8G8B8_SRGB")
		   return VK_FORMAT_R8G8B8_SRGB;
		if (input_value == "VK_FORMAT_R8G8B8_SSCALED")
		   return VK_FORMAT_R8G8B8_SSCALED;
		if (input_value == "VK_FORMAT_R8G8B8_UINT")
		   return VK_FORMAT_R8G8B8_UINT;
		if (input_value == "VK_FORMAT_R8G8B8_UNORM")
		   return VK_FORMAT_R8G8B8_UNORM;
		if (input_value == "VK_FORMAT_R8G8B8_USCALED")
		   return VK_FORMAT_R8G8B8_USCALED;
		if (input_value == "VK_FORMAT_R8G8_SINT")
		   return VK_FORMAT_R8G8_SINT;
		if (input_value == "VK_FORMAT_R8G8_SNORM")
		   return VK_FORMAT_R8G8_SNORM;
		if (input_value == "VK_FORMAT_R8G8_SRGB")
		   return VK_FORMAT_R8G8_SRGB;
		if (input_value == "VK_FORMAT_R8G8_SSCALED")
		   return VK_FORMAT_R8G8_SSCALED;
		if (input_value == "VK_FORMAT_R8G8_UINT")
		   return VK_FORMAT_R8G8_UINT;
		if (input_value == "VK_FORMAT_R8G8_UNORM")
		   return VK_FORMAT_R8G8_UNORM;
		if (input_value == "VK_FORMAT_R8G8_USCALED")
		   return VK_FORMAT_R8G8_USCALED;
		if (input_value == "VK_FORMAT_R8_SINT")
		   return VK_FORMAT_R8_SINT;
		if (input_value == "VK_FORMAT_R8_SNORM")
		   return VK_FORMAT_R8_SNORM;
		if (input_value == "VK_FORMAT_R8_SRGB")
		   return VK_FORMAT_R8_SRGB;
		if (input_value == "VK_FORMAT_R8_SSCALED")
		   return VK_FORMAT_R8_SSCALED;
		if (input_value == "VK_FORMAT_R8_UINT")
		   return VK_FORMAT_R8_UINT;
		if (input_value == "VK_FORMAT_R8_UNORM")
		   return VK_FORMAT_R8_UNORM;
		if (input_value == "VK_FORMAT_R8_USCALED")
		   return VK_FORMAT_R8_USCALED;
		if (input_value == "VK_FORMAT_S8_UINT")
		   return VK_FORMAT_S8_UINT;
		if (input_value == "VK_FORMAT_UNDEFINED")
		   return VK_FORMAT_UNDEFINED;
		if (input_value == "VK_FORMAT_X8_D24_UNORM_PACK32")
		   return VK_FORMAT_X8_D24_UNORM_PACK32;
	}
}