#pragma once
#include "device.h"
#include "shader.h"

namespace Shard3D {
	class S3DComputePipeline {
	public:
		S3DComputePipeline(S3DDevice& device, VkPipelineLayout pipelineLayout, const S3DShader& shader);
		~S3DComputePipeline();

		S3DComputePipeline(const S3DComputePipeline&) = delete;
		S3DComputePipeline& operator=(const S3DComputePipeline&) = delete;
		S3DComputePipeline() = default;

		void bind(VkCommandBuffer commandBuffer);

		void destroyS3DComputePipeline();

	private:
		void createS3DComputePipeline(
			VkPipelineLayout& pipelineLayout,
			const S3DShader& shader
		);

		S3DDevice& engineDevice;
		VkPipeline computePipeline;
		std::vector<VkShaderModule> shaderModules;
	};
}