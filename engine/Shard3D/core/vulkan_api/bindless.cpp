#include "../../core.h"
#include "bindless.h"

namespace Shard3D {

    // *************** Descriptor Pool Builder *********************

    BindlessDescriptorSetResourcePool::Builder& BindlessDescriptorSetResourcePool::Builder::addDescriptorType(
        VkDescriptorType descriptorType, uint32_t binding) {
        SHARD3D_ASSERT(descriptorType != VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER && "VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER is not supported by bindless descriptors!!");
        poolTypes.push_back({ descriptorType, maxElements });
        poolBindings.push_back({ descriptorType, binding });
        return *this;
    }

    BindlessDescriptorSetResourcePool::Builder& BindlessDescriptorSetResourcePool::Builder::setPoolFlags(
        VkDescriptorPoolCreateFlags flags) {
        poolFlags = flags;
        return *this;
    }

    BindlessDescriptorSetResourcePool::Builder& BindlessDescriptorSetResourcePool::Builder::setShaderStages(VkShaderStageFlags allowedStages)  {
        stageFlags = allowedStages;
        return *this;
    }

    BindlessDescriptorSetResourcePool::Builder& BindlessDescriptorSetResourcePool::Builder::setMaximumSize(uint32_t size) {
        maxElements = size;
        return *this;
    }

    uPtr<BindlessDescriptorSetResourcePool> BindlessDescriptorSetResourcePool::Builder::build() const {
        return make_uPtr<BindlessDescriptorSetResourcePool>(engineDevice, poolFlags, poolTypes, poolBindings, maxElements, stageFlags);
    }

    // *************** Descriptor Pool *********************

    BindlessDescriptorSetResourcePool::BindlessDescriptorSetResourcePool(
        S3DDevice& engineDevice,
        VkDescriptorPoolCreateFlags poolFlags,
        const std::vector<VkDescriptorPoolSize>& poolSizes,
        const std::vector<DescriptorBindingInfo>& poolBindings,
        uint32_t maxElements,
        VkShaderStageFlags allowedStages
    ) 
        : engineDevice{ engineDevice }, maxSize(maxElements) {
        VkDescriptorPoolCreateInfo descriptorPoolInfo{};
        descriptorPoolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        descriptorPoolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
        descriptorPoolInfo.pPoolSizes = poolSizes.data();
        descriptorPoolInfo.maxSets = 1;
        descriptorPoolInfo.flags = poolFlags | VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT | VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT;

        VK_ASSERT(vkCreateDescriptorPool(engineDevice.device(), &descriptorPoolInfo, nullptr, &descriptorPool), "failed to create descriptor pool!");

        std::vector<VkDescriptorSetLayoutBinding> setLayoutBindings{};
        std::vector<VkDescriptorBindingFlags> setLayoutBindingFlags{};
        for (auto& binding : poolBindings) {
            VkDescriptorSetLayoutBinding layoutBinding{};
            layoutBinding.binding = binding.binding;
            layoutBinding.descriptorType = binding.type;
            layoutBinding.descriptorCount = maxSize;
            layoutBinding.stageFlags = allowedStages;
            setLayoutBindings.push_back(layoutBinding);
            SHARD3D_ASSERT(types.count(binding.binding) == 0 && "Binding is already in use!");
            types[binding.binding] = binding.type;
            setLayoutBindingFlags.emplace_back(
                VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT | VK_DESCRIPTOR_BINDING_UPDATE_AFTER_BIND_BIT // | VK_DESCRIPTOR_BINDING_UPDATE_UNUSED_WHILE_PENDING_BIT
            );
        }

        bindingFlags.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO;
        bindingFlags.bindingCount = poolBindings.size();
        bindingFlags.pBindingFlags = setLayoutBindingFlags.data();

        VkDescriptorSetLayoutCreateInfo descriptorSetLayoutInfo{};
        descriptorSetLayoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        descriptorSetLayoutInfo.bindingCount = static_cast<uint32_t>(setLayoutBindings.size());
        descriptorSetLayoutInfo.pBindings = setLayoutBindings.data();
        descriptorSetLayoutInfo.flags = VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT;
        descriptorSetLayoutInfo.pNext = &bindingFlags;

        VK_ASSERT(vkCreateDescriptorSetLayout(
            engineDevice.device(),
            &descriptorSetLayoutInfo,
            nullptr,
            &indexedSetLayout
        ), "failed to create descriptor set layout!");

        uint32_t counts[1]{ maxSize };

        setCounts.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_ALLOCATE_INFO;
        setCounts.descriptorSetCount = 1;
        setCounts.pDescriptorCounts = counts;

        VkDescriptorSetAllocateInfo allocInfo{};
        allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        allocInfo.descriptorPool = descriptorPool;
        allocInfo.pSetLayouts = &indexedSetLayout;
        allocInfo.descriptorSetCount = 1;
        allocInfo.pNext = &setCounts;

        VK_ASSERT(vkAllocateDescriptorSets(engineDevice.device(), &allocInfo, &indexedDescriptor), "failed to create bindless descriptor!");

        VkSamplerCreateInfo samplerCreate{};
        samplerCreate.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
        vkCreateSampler(engineDevice.device(), &samplerCreate, nullptr, &defaultSampler);
    }

    BindlessDescriptorSetResourcePool::~BindlessDescriptorSetResourcePool() {
        vkFreeDescriptorSets(
            engineDevice.device(),
            descriptorPool,
            1,
            &indexedDescriptor
        );
        vkDestroyDescriptorSetLayout(engineDevice.device(), indexedSetLayout, nullptr);
        vkDestroyDescriptorPool(engineDevice.device(), descriptorPool, nullptr);
    }

    void BindlessDescriptorSetResourcePool::resetPool() {
        vkResetDescriptorPool(engineDevice.device(), descriptorPool, 0);
    }

    ShaderResourceIndex BindlessDescriptorSetResourcePool::allocateIndex(uint32_t binding) {
        ShaderResourceIndex index = 0;
        while (true) {
            if (index >= allocatedIndices[binding].size() || allocatedIndices[binding][index] == false) {
                if (index > maxSize) SHARD3D_FATAL("No free descriptor set array indices left!");
                break;
            }
            index++;
        }
        SHARD3D_VERBOSE("Bindless descriptor {0} allocated index {1} in binding {2}", (void*)this, index, binding);
        allocatedIndices[binding][index] = true;
        return index; 
    }

    bool BindlessDescriptorSetResourcePool::freeIndex(uint32_t binding, ShaderResourceIndex index) {
        SHARD3D_VERBOSE("Bindless descriptor {0} deallocated index {1} in binding {2}", (void*)this, index, binding);
        BindlessDescriptorWriter(*this).writeEmpty(binding, index).build();
        allocatedIndices[binding][index] = false;
        return true;
    }

    bool BindlessDescriptorSetResourcePool::isAllocated(uint32_t binding, ShaderResourceIndex index) {
        if (allocatedIndices[binding].find(index) == allocatedIndices[binding].end()) return false;
        return allocatedIndices[binding][index];
    }

    BindlessDescriptorWriter::BindlessDescriptorWriter(BindlessDescriptorSetResourcePool& pool) : resourcePool{ pool } {}

    BindlessDescriptorWriter& BindlessDescriptorWriter::writeBuffer(uint32_t binding, ShaderResourceIndex index_element, VkDescriptorBufferInfo* bufferInfo) {
        SHARD3D_ASSERT(resourcePool.allocatedIndices.count(binding) != 0 && "Trying to write buffer to binding that is not defined!");
        SHARD3D_ASSERT(index_element < resourcePool.maxSize && "Trying to write buffer out of index bounds!");

        VkWriteDescriptorSet write{};
        write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        write.dstSet = resourcePool.indexedDescriptor;
        write.dstBinding = binding;    
        write.descriptorCount = 1;
        write.dstArrayElement = index_element; 
        write.descriptorType = resourcePool.types.at(binding);
        write.pBufferInfo = bufferInfo;
        writes.push_back(write);

        return *this;
    }
    BindlessDescriptorWriter& BindlessDescriptorWriter::writeImage(uint32_t binding, ShaderResourceIndex index_element, VkDescriptorImageInfo* imageInfo) {
        SHARD3D_ASSERT(resourcePool.allocatedIndices.count(binding) != 0 && "Trying to write image to binding that is not defined!");
        SHARD3D_ASSERT(index_element < resourcePool.maxSize && "Trying to write image out of index bounds!");

        VkWriteDescriptorSet write{};
        write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        write.dstSet = resourcePool.indexedDescriptor;
        write.dstBinding = binding;    
        write.descriptorCount = 1; 
        write.dstArrayElement = index_element;
        write.descriptorType = resourcePool.types.at(binding);
        write.pImageInfo = imageInfo;
        writes.push_back(write);

        return *this;
    }
    BindlessDescriptorWriter& BindlessDescriptorWriter::writeEmpty(uint32_t binding, ShaderResourceIndex index_element) {
        SHARD3D_ASSERT(resourcePool.allocatedIndices.count(binding) != 0 && "Trying to write image to binding that is not defined!");
        SHARD3D_ASSERT(index_element < resourcePool.maxSize && "Trying to write image out of index bounds!");

        VkWriteDescriptorSet write{};
        write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        write.dstSet = resourcePool.indexedDescriptor;
        write.dstBinding = binding;
        write.descriptorCount = 1;
        write.dstArrayElement = index_element;
        write.descriptorType = resourcePool.types.at(binding);

        static VkDescriptorImageInfo imageInfoNULL;
        static VkDescriptorBufferInfo bufferInfoNULL;

        imageInfoNULL = resourcePool.engineDevice.getDefaultImageDescriptorInfo();
        bufferInfoNULL = resourcePool.engineDevice.getDefaultBufferDescriptorInfo();

        write.pImageInfo = &imageInfoNULL;
        write.pBufferInfo = &bufferInfoNULL;
        writes.push_back(write);

        return *this;
    }
    void BindlessDescriptorWriter::build()  {
        vkUpdateDescriptorSets(
            resourcePool.engineDevice.device(),
            writes.size(),
            writes.data(),
            0, nullptr
        );
    }
}