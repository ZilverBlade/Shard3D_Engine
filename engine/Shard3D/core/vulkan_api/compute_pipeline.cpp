#include "compute_pipeline.h"
#include "../../core.h"
#include <format>
namespace Shard3D {
	S3DComputePipeline::S3DComputePipeline(
		S3DDevice& device,
		VkPipelineLayout pipelineLayout,
		const S3DShader& shader
	)
		: engineDevice{ device } {
		createS3DComputePipeline(pipelineLayout, shader);
	}
	S3DComputePipeline::~S3DComputePipeline() {
		for (auto& module : shaderModules) {
			vkDestroyShaderModule(engineDevice.device(), module, nullptr);
		}
		vkDestroyPipeline(engineDevice.device(), computePipeline, nullptr);
	}

	void S3DComputePipeline::createS3DComputePipeline(
		VkPipelineLayout& pipelineLayout,
		const S3DShader& shader
	) {
		VkPipelineShaderStageCreateInfo shaderStage{};
		VkShaderModule module = shader.createShaderModule(engineDevice);
		shaderModules.push_back(module);
		shaderStage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		shaderStage.stage = VK_SHADER_STAGE_COMPUTE_BIT;
		shaderStage.module = module;
		shaderStage.pName = "main";
		shaderStage.flags = 0;
		shaderStage.pNext = nullptr;
		VkSpecializationInfo specialization = shader.getSpecializationInfo();
		shaderStage.pSpecializationInfo = &specialization;

		VkComputePipelineCreateInfo computePipelineCreateInfo {};
		computePipelineCreateInfo.stage = shaderStage;
		computePipelineCreateInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
		computePipelineCreateInfo.layout = pipelineLayout;
		computePipelineCreateInfo.flags = 0;

		VK_ASSERT(vkCreateComputePipelines(engineDevice.device(), VK_NULL_HANDLE, 1, &computePipelineCreateInfo, nullptr, &computePipeline), "failed to create compute pipeline!");	
	}

	void S3DComputePipeline::destroyS3DComputePipeline() {
		vkDestroyPipeline(engineDevice.device(), computePipeline, nullptr);
	}
	void S3DComputePipeline::bind(VkCommandBuffer commandBuffer) {
		vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, computePipeline);
	}
}