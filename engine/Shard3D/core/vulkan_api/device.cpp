#pragma once
#include "../../s3dpch.h"
#include "device.h"
#include "descriptors.h"
#include "../../core.h"

#include "../misc/graphics_settings.h"
#include <set>

namespace Shard3D {
	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
	VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
	VkDebugUtilsMessageTypeFlagsEXT messageType,
	const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
	void* pUserData) {

		if (messageType == VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT) {
			if (messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
				SHARD3D_WARN("validation layer: {0}", pCallbackData->pMessage);
			else if (messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
				SHARD3D_ERROR("validation layer: {0}", pCallbackData->pMessage);
			else if (messageType == VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT)
				SHARD3D_INFO("validation layer: {0}", pCallbackData->pMessage);
		} else if (messageType == VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT) {
			if (messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
				SHARD3D_WARN("vulkan performance: {0}", pCallbackData->pMessage);
			else if (messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
				SHARD3D_ERROR("vulkan performance: {0}", pCallbackData->pMessage);
			else if (messageType == VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT)
				SHARD3D_INFO("vulkan performance: {0}", pCallbackData->pMessage);
		} else {
			if (messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
				SHARD3D_WARN("vulkan: {0}", pCallbackData->pMessage);
			else if (messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
				SHARD3D_ERROR("vulkan: {0}", pCallbackData->pMessage);
			else if (messageType == VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT)
				SHARD3D_INFO("vulkan: {0}", pCallbackData->pMessage);
		}

		return VK_FALSE;
	}

	static VkResult CreateDebugUtilsMessengerEXT (
	VkInstance instance,
	const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo,
	const VkAllocationCallbacks* pAllocator,
	VkDebugUtilsMessengerEXT* pDebugMessenger) {
		auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(
			instance,
			"vkCreateDebugUtilsMessengerEXT");
		if (func != nullptr) {
			return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
		} else {
			return VK_ERROR_EXTENSION_NOT_PRESENT;
		}
	}

	static void DestroyDebugUtilsMesenginengerEXT(
		VkInstance instance,
		VkDebugUtilsMessengerEXT debugMessenger,
		const VkAllocationCallbacks* pAllocator) {
		auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(
			instance,
			"vkDestroyDebugUtilsMessengerEXT");
		if (func != nullptr) {
			func(instance, debugMessenger, pAllocator);
		}
	}

	static VKAPI_ATTR void VKAPI_CALL deviceMemoryReportCallbackEXT(
		const VkDeviceMemoryReportCallbackDataEXT* pCallbackData,
		void* pUserData
	) {
		SHARD3D_ERROR("DEVICE MEMORY ERROR: {0}, (objectType: {1}, memory size {2} kB)", string_VkDeviceMemoryReportEventTypeEXT(pCallbackData->type), string_VkObjectType(pCallbackData->objectType), pCallbackData->size / 1000i64);
	}
	
	VkSampleCountFlagBits S3DDevice::getMaxUsableSampleCount() {
	    VkPhysicalDeviceProperties physicalDeviceProperties;
	    vkGetPhysicalDeviceProperties(physicalDevice_, &physicalDeviceProperties);
	
	    VkSampleCountFlags counts = std::min(physicalDeviceProperties.limits.framebufferColorSampleCounts, physicalDeviceProperties.limits.framebufferDepthSampleCounts);
	    if (counts & VK_SAMPLE_COUNT_64_BIT) { return VK_SAMPLE_COUNT_64_BIT; }
	    if (counts & VK_SAMPLE_COUNT_32_BIT) { return VK_SAMPLE_COUNT_32_BIT; }
	    if (counts & VK_SAMPLE_COUNT_16_BIT) { return VK_SAMPLE_COUNT_16_BIT; }
	    if (counts & VK_SAMPLE_COUNT_8_BIT) { return VK_SAMPLE_COUNT_8_BIT; }
	    if (counts & VK_SAMPLE_COUNT_4_BIT) { return VK_SAMPLE_COUNT_4_BIT; }
	    if (counts & VK_SAMPLE_COUNT_2_BIT) { return VK_SAMPLE_COUNT_2_BIT; }
	
	    return VK_SAMPLE_COUNT_1_BIT;
	}


	S3DDevice::S3DDevice() {
		createInstance();
		setupDebugMessenger();
		pickPhyisicalDevice();
		createLogicalDevice();
		createCommandPool();
		graphicsQueues = createQueues(QueueType::Graphics, 1, 0);
		createArenaBuffer(ProjectSystem::getEngineSettings().memory.memoryAllocatorSizeBytes); // 32 mb
		createDefaultResourceObjects();

		staticMaterialPool = S3DDescriptorPool::Builder(*this)
			.setMaxSets(2048)
			.addPoolSize(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1024)
			.addPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1024)
			.addPoolSize(VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1024)
			.setPoolFlags(VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT)
			.build_r();
	}

	S3DDevice::~S3DDevice() {
		delete staticMaterialPool;
		for (S3DCheckpointData* dat : checkpointDebugDataNVIDIA) {
			delete dat;
		}
		if (enableValidationLayers) {
			DestroyDebugUtilsMesenginengerEXT(instance, debugMessenger, nullptr);
		}

		vkFreeMemory(device_, bufferMemoryArena, nullptr);
		vkFreeMemory(device_, defaultBufferMemory, nullptr);
		vkFreeMemory(device_, defaultImageMemory, nullptr);
		vkDestroyBuffer(device_, defaultBuffer, nullptr);
		vkDestroySampler(device_, defaultSampler, nullptr);
		vkDestroyImage(device_, defaultImage, nullptr);
		vkDestroyImageView(device_, defaultImageView, nullptr);
		vkDestroyCommandPool(device_, commandPool, nullptr);
		vkDestroyDevice(device_, nullptr);
		vkDestroyInstance(instance, nullptr);
	}

	void S3DDevice::createInstance() {
		if (enableValidationLayers && !checkValidationLayerSupport()) {
			SHARD3D_FATAL("Validation errors not available!");
		}
		VkApplicationInfo appInfo = {};
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.pApplicationName = "Shard3D (Vulkan)";
		appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
		appInfo.pEngineName = "Shard3D Torque";
		appInfo.engineVersion = VK_MAKE_VERSION(ENGINE_VERSION.getMajor(), ENGINE_VERSION.getMinor(), ENGINE_VERSION.getPatch());
		appInfo.apiVersion = VK_API_VERSION_1_2; // do not use version 1.3

		VkInstanceCreateInfo createInfo{};
		createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		createInfo.pApplicationInfo = &appInfo;

		std::vector<const char*> extensions = getRequiredExtensions();
		createInfo.enabledExtensionCount = extensions.size();
		createInfo.ppEnabledExtensionNames = extensions.data();

		VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo;
		if (enableValidationLayers) {
			createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
			createInfo.ppEnabledLayerNames = validationLayers.data();

			populateDebugMessengerCreateInfo(debugCreateInfo);
			createInfo.pNext = (VkDebugUtilsMessengerCreateInfoEXT*)&debugCreateInfo;
		} else {
			createInfo.enabledLayerCount = 0;
			createInfo.pNext = nullptr;
		}

		VK_ASSERT(vkCreateInstance(&createInfo, nullptr, &instance), "Failed to create instance!");
		volkLoadInstance(instance);

		hasGlfwRequiredInstanceExtensions();
	}
	void S3DDevice::populateDebugMessengerCreateInfo(
	VkDebugUtilsMessengerCreateInfoEXT& createInfo) {
		createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
		createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
		createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
		createInfo.pfnUserCallback = debugCallback;
		createInfo.pUserData = nullptr;  // Optional
	}
	void S3DDevice::hasGlfwRequiredInstanceExtensions() {
		uint32_t extensionCount = 0;
		vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
		std::vector<VkExtensionProperties> extensions(extensionCount);
		vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, extensions.data());

		std::cout << "available extensions:" << std::endl;
		std::set<std::string> available;
		for (const auto& extension : extensions) {
			std::cout << "\t" << extension.extensionName << std::endl;
			available.insert(extension.extensionName);
		}

		std::cout << "required extensions:" << std::endl;
		auto requiredExtensions = getRequiredExtensions();
		for (const auto& required : requiredExtensions) {
			std::cout << "\t" << required << std::endl;
			if (available.find(required) == available.end()) {
				SHARD3D_FATAL("Missing required glfw extension");
			}
		}
	}
	void S3DDevice::createArenaBuffer(VkDeviceSize sizeBytes) {
		VkBufferCreateInfo bufferInfo{};
		bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		bufferInfo.size = sizeBytes;
		bufferInfo.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
		bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

		VkBuffer buffer;
		if (vkCreateBuffer(device_, &bufferInfo, nullptr, &buffer) != VK_SUCCESS) {
			throw std::runtime_error("failed to create buffer!");
		}
		VkMemoryRequirements memRequirements;
		vkGetBufferMemoryRequirements(device_, buffer, &memRequirements);
		vkDestroyBuffer(device_, buffer, nullptr);
		
		VkMemoryAllocateInfo allocate{};
		allocate.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		allocate.allocationSize = sizeBytes;
		allocate.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
		if (allocate.memoryTypeIndex == UINT32_MAX) {
			allocate.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
		}
		vkAllocateMemory(device_, &allocate, nullptr, &bufferMemoryArena);
		if (ProjectSystem::getEngineSettings().memory.memoryAllocatorBlockSizeBytes == 0) {
			SHARD3D_FATAL("0 is an invalid block size");
		}
		ProjectSystem::getEngineSettings().memory.memoryAllocatorBlockSizeBytes = std::ceil(ProjectSystem::getEngineSettings().memory.memoryAllocatorBlockSizeBytes / memRequirements.alignment) * memRequirements.alignment;
		
		bufferMemoryArenaBlocks.resize(sizeBytes / ProjectSystem::getEngineSettings().memory.memoryAllocatorBlockSizeBytes, { 0, 0, true });
		for (int i = 0; i < bufferMemoryArenaBlocks.size(); i++) {
			bufferMemoryArenaBlocks[i].size = bufferMemoryArenaBlocks.size() - i;
			bufferMemoryArenaBlocks[i].next = i + 1;
		}

		VK_ASSERT(vkMapMemory(device_, bufferMemoryArena, 0, VK_WHOLE_SIZE, 0, &bufferMemoryArenaMapped), "failed to create arena buffer");
	}
	void S3DDevice::setupDebugMessenger() {
		if (!enableValidationLayers) return;
		VkDebugUtilsMessengerCreateInfoEXT createInfo;
		populateDebugMessengerCreateInfo(createInfo);
		if (CreateDebugUtilsMessengerEXT(instance, &createInfo, nullptr, &debugMessenger) != VK_SUCCESS) {
			SHARD3D_FATAL("Failed to set up debug messenger!");
		}
	}
	void S3DDevice::createDefaultResourceObjects() {
		createBuffer(
			1,
			VK_BUFFER_USAGE_TRANSFER_SRC_BIT |
				VK_BUFFER_USAGE_TRANSFER_DST_BIT |
				VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT|
				VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT|
				VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT |
				VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
			defaultBuffer,
			defaultBufferMemory
		); // full image size (aka mip level 0)

		size_t dataSize = 1;
		char data[1]{0x00};
		void* dat;
		vkMapMemory(device_, defaultBufferMemory, 0, dataSize, 0, &dat);

		VkExtent3D mExtent = { 1U, 1U, 1U };

		VkImageCreateInfo imageInfo{};
		imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageInfo.imageType = VK_IMAGE_TYPE_2D;
		imageInfo.extent = mExtent;
		imageInfo.mipLevels = 1;
		imageInfo.arrayLayers = 1;
		imageInfo.format = VK_FORMAT_R8_UINT;
		imageInfo.flags = 0;
		imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
		imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		imageInfo.usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT |
			VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_STORAGE_BIT;
		imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
		imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

		createImageWithInfo(
			imageInfo,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
			defaultImage,
			defaultImageMemory
		);

		VkCommandBuffer commandBuffer = beginSingleTimeCommands();
		transitionImageLayout(
			commandBuffer,
			defaultImage,
			VK_FORMAT_R8_UINT,
			VK_IMAGE_LAYOUT_UNDEFINED,
			VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			0,
			1,
			0,
			1
		);
		copyBufferToImage(
			commandBuffer,
			defaultBuffer,
			defaultImage,
			1,
			1,
			1
		);
		transitionImageLayout(
			commandBuffer,
			defaultImage,
			VK_FORMAT_R8_UINT,
			VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
			0,
			1,
			0,
			1
		);
		endSingleTimeCommands(commandBuffer);
		VkImageViewCreateInfo viewInfo{};
		viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		viewInfo.image = defaultImage;
		viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		viewInfo.format = VK_FORMAT_R8_UINT;
		viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		viewInfo.subresourceRange.baseMipLevel = 0;
		viewInfo.subresourceRange.levelCount = 1;
		viewInfo.subresourceRange.baseArrayLayer = 0;
		viewInfo.subresourceRange.layerCount = 1;

		VK_ASSERT(vkCreateImageView(device_, &viewInfo, nullptr, &defaultImageView), "failed to create default image view!");

		VkSamplerCreateInfo samplerInfo{};
		samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		samplerInfo.magFilter = VK_FILTER_NEAREST;
		samplerInfo.minFilter = VK_FILTER_NEAREST;

		samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;

		samplerInfo.anisotropyEnable = VK_FALSE;
		samplerInfo.maxAnisotropy = 1.0f;
		samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
		samplerInfo.unnormalizedCoordinates = VK_FALSE;

		samplerInfo.compareEnable = VK_FALSE;
		samplerInfo.compareOp = VK_COMPARE_OP_NEVER;

		samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
		samplerInfo.mipLodBias = 0.0f;
		samplerInfo.minLod = 0.0f;
		samplerInfo.maxLod = 1.0f;

		VK_ASSERT(vkCreateSampler(device_, &samplerInfo, nullptr, &defaultSampler), "failed to create default sampler!");
	}
	bool S3DDevice::checkValidationLayerSupport() {
		uint32_t layerCount;
		vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

		std::vector<VkLayerProperties> availableLayers(layerCount);
		vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

		for (const char* layerName : validationLayers) {
			bool layerFound = false;

			for (const auto& layerProperties : availableLayers) {
				if (strcmp(layerName, layerProperties.layerName) == 0) {
					layerFound = true;
					break;
				}
			}

			if (!layerFound) {
				return false;
			}
		}

		return true;
	}
	void S3DDevice::pickPhyisicalDevice() {
		uint32_t deviceCount = 0;
		vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);
		if (deviceCount == 0) {
			throw std::runtime_error("failed to find GPUs with Vulkan support!");
		}
		std::vector<VkPhysicalDevice> devices(deviceCount);
		vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());
		SHARD3D_INFO("GPUs found: ");
		for (uint32_t i = 0; i < devices.size(); i++) {
			VkPhysicalDeviceProperties properties{};
			vkGetPhysicalDeviceProperties(devices[i], &properties);
			SHARD3D_INFO("---- [{0}] ({1}) {2} ", i, 24 + string_VkPhysicalDeviceType(properties.deviceType), properties.deviceName);
		}
		for (const auto& device : devices) {
			if (isSuitableDevice(device)) {
				physicalDevice_ = device;
				break;
			}
		}
		if (physicalDevice_ == VK_NULL_HANDLE) {
			SHARD3D_FATAL("failed to find a suitable GPU!");
		}
		vkGetPhysicalDeviceProperties(physicalDevice_, &deviceProperties);
		requestDeviceFeatures(deviceFeatures);
		vkGetPhysicalDeviceFeatures(physicalDevice_, &supportedFeatures);
		vkGetPhysicalDeviceProperties(physicalDevice_, &properties);
		VkPhysicalDeviceMemoryProperties memProperties;
		vkGetPhysicalDeviceMemoryProperties(physicalDevice_, &memProperties);

		gpuInfo.name = properties.deviceName;
		switch (properties.deviceType) {
		case(VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU):gpuInfo.type = "Discrete"; break; case(VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU):gpuInfo.type = "Integrated"; break; default:gpuInfo.type = "Unknown"; break;
		}
		gpuInfo.memory = memProperties.memoryHeaps[0].size;
		SHARD3D_INFO("Using physical device: {0}", properties.deviceName);
		if (properties.deviceType != VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
			SHARD3D_WARN("Graphics device is not discrete. Performance may not be optimal or device may not be supported. Ignore if you play on a system that only has an integrated gpu, otherwise, check if you are running this engine with the correct graphics card selected");
		if (properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_CPU)
			SHARD3D_WARN("Graphics are being rendered with CPU, this can cause a significant performance decrease/may not be supported. Continue at own risk.");
	}

	bool S3DDevice::checkDeviceFeatureSupport(VkPhysicalDevice device) {
		VkPhysicalDeviceFeatures features{};
		vkGetPhysicalDeviceFeatures(device, &features);
		return
			features.samplerAnisotropy &
			features.sampleRateShading &
			features.fragmentStoresAndAtomics &
			features.fillModeNonSolid &
			features.wideLines &
			features.independentBlend &
			features.geometryShader &
			features.shaderClipDistance &
			features.multiViewport &
			true;
	}
	void S3DDevice::requestDeviceFeatures(VkPhysicalDeviceFeatures& features) {
		features.samplerAnisotropy = VK_TRUE;

		// clip plane
		features.shaderClipDistance = VK_TRUE;

		// SSBO store
		features.fragmentStoresAndAtomics = VK_TRUE;

		// MSAA
		features.sampleRateShading = VK_TRUE;

		// line rasteriser 
		features.fillModeNonSolid = VK_TRUE;
		features.wideLines = VK_TRUE;

		// geometry shader
		features.geometryShader = VK_TRUE;

		// blending
		features.independentBlend = VK_TRUE;
		features.dualSrcBlend = VK_TRUE;

		// render to multiple layers
		features.multiViewport = VK_TRUE;

	}
	bool S3DDevice::checkDeviceExtensionSupport(VkPhysicalDevice device) {
		uint32_t extensionCount;
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);

		std::vector<VkExtensionProperties> availableExtensions(extensionCount);
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());

		std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end());

		for (const auto& extension : availableExtensions) {
			requiredExtensions.erase(extension.extensionName);
		}

		return requiredExtensions.empty();
	}

	bool S3DDevice::isSuitableDevice(VkPhysicalDevice device) {
		return checkDeviceExtensionSupport(device) && checkDeviceFeatureSupport(device);
	}
	bool S3DDevice::isPreferredDevice(VkPhysicalDevice device) {
		if (!isSuitableDevice(device)) {
			return false;
		}

		auto props = VkPhysicalDeviceProperties{};
		vkGetPhysicalDeviceProperties(device, &props);

		return props.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU;
	}
	QueueFamilyIndices S3DDevice::findQueueFamilies(VkPhysicalDevice device) {
		QueueFamilyIndices indices{};
		uint32_t queueFamilyCount = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);

		std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());
		int i = 0;
		for (const auto& queueFamily : queueFamilies) {
			if (queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT && indices.graphicsFamily == uint32_t(-1)) {
				indices.graphicsFamily = i;
			}
			if (queueFamily.queueFlags & VK_QUEUE_COMPUTE_BIT && indices.computeFamily == uint32_t(-1)) {
				indices.computeFamily = i;
			}
			i++;
		}
		return indices;
	}

	std::vector<const char*> S3DDevice::getRequiredExtensions() {
		uint32_t glfwExtensionCount = 0;
		const char** glfwExtensions;
		glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

		std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

		if (enableValidationLayers) {
			extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
		}

		extensions.push_back(VK_KHR_SURFACE_EXTENSION_NAME);
#ifdef _WIN32
		extensions.push_back("VK_KHR_win32_surface");
#endif
		return extensions;
	}

	std::vector<S3DQueue> S3DDevice::createQueues(QueueType queueType, uint32_t queueCount, uint32_t queueOffset) {
		QueueFamilyIndices indices = findQueueFamilies(physicalDevice_);
		uint32_t queueFamilyIndex = 0;
		uint32_t qiOffset = 0;
		switch (queueType) {
		case(QueueType::Graphics):
			queueFamilyIndex = indices.graphicsFamily;
			break;
		case(QueueType::Compute):
			queueFamilyIndex = indices.computeFamily;
			break;
		}

		std::vector<S3DQueue> queues{};
		queues.resize(queueCount);
		for (uint32_t i = 0; i < queueCount; i++) {
			queues[i].type = queueType;
			vkGetDeviceQueue(device_, queueFamilyIndex, i + queueOffset, &queues[i].queue);
		}
		return queues;
	}

	void S3DDevice::createLogicalDevice() {
		VkDeviceCreateInfo createInfo{};

		QueueFamilyIndices indices = findQueueFamilies(physicalDevice_);
		std::vector<float> queuePriorities{
			1.0f
		};
		VkDeviceQueueCreateInfo queueCreateInfo[1]{};
		queueCreateInfo[0].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo[0].queueFamilyIndex = 0;
		queueCreateInfo[0].queueCount = 1;
		queueCreateInfo[0].pQueuePriorities = queuePriorities.data();


		createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO; 
		createInfo.pQueueCreateInfos = queueCreateInfo;
		createInfo.queueCreateInfoCount = 1;

		createInfo.pEnabledFeatures = &deviceFeatures;

		uint32_t extensionCount;
		vkEnumerateDeviceExtensionProperties(physicalDevice_, nullptr, &extensionCount, nullptr);

		std::vector<VkExtensionProperties> availableExtensions(extensionCount);
		vkEnumerateDeviceExtensionProperties(physicalDevice_, nullptr, &extensionCount, availableExtensions.data());

		for (VkExtensionProperties& extension : availableExtensions) {
			if (std::string(extension.extensionName) == VK_KHR_FRAGMENT_SHADING_RATE_EXTENSION_NAME) {
				if (ProjectSystem::getEngineSettings().renderer.allowFragmentShadingRateEXT) {
					supportedExtensions.khr_fragment_shading_rate = true;
					deviceExtensions.push_back(VK_KHR_FRAGMENT_SHADING_RATE_EXTENSION_NAME);
				}
			}
			if (std::string(extension.extensionName) == VK_EXT_CONSERVATIVE_RASTERIZATION_EXTENSION_NAME) {
				supportedExtensions.ext_conservative_rasterization = true;
				deviceExtensions.push_back(VK_EXT_CONSERVATIVE_RASTERIZATION_EXTENSION_NAME);
				VkPhysicalDeviceProperties2 propertiesCR;
				ext_conservative_rasterization_properties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CONSERVATIVE_RASTERIZATION_PROPERTIES_EXT;
				propertiesCR.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
				propertiesCR.pNext = &ext_conservative_rasterization_properties;
				vkGetPhysicalDeviceProperties2(physicalDevice_, &propertiesCR);
			}

			if (ProjectSystem::getEngineSettings().renderer.allowRayTracingUsage) {
				if (std::string(extension.extensionName) == VK_KHR_RAY_QUERY_EXTENSION_NAME) {
					supportedExtensions.khr_ray_query = true;
					deviceExtensions.push_back(VK_KHR_RAY_QUERY_EXTENSION_NAME);
				}
				if (std::string(extension.extensionName) == VK_KHR_ACCELERATION_STRUCTURE_EXTENSION_NAME) {
					supportedExtensions.khr_acceleration_structure = true;
					deviceExtensions.push_back(VK_KHR_ACCELERATION_STRUCTURE_EXTENSION_NAME);
				}
				if (std::string(extension.extensionName) == VK_KHR_DEFERRED_HOST_OPERATIONS_EXTENSION_NAME) {
					supportedExtensions.khr_deferred_host_operations = true;
					deviceExtensions.push_back(VK_KHR_ACCELERATION_STRUCTURE_EXTENSION_NAME);
				}
			}

			if (std::string(extension.extensionName) == VK_NV_DEVICE_DIAGNOSTIC_CHECKPOINTS_EXTENSION_NAME) {
				supportedExtensions.nv_device_diagnostic_checkpoints = true;
				deviceExtensions.push_back(VK_NV_DEVICE_DIAGNOSTIC_CHECKPOINTS_EXTENSION_NAME);
				SHARD3D_INFO("'VK_NV_device_diagnostic_checkpoints' is available, crash log will be more detailed");
			}

			if (std::string(extension.extensionName) == VK_EXT_DESCRIPTOR_INDEXING_EXTENSION_NAME) {
				deviceExtensions.push_back(VK_EXT_DESCRIPTOR_INDEXING_EXTENSION_NAME);
			}
			if (std::string(extension.extensionName) == VK_KHR_IMAGELESS_FRAMEBUFFER_EXTENSION_NAME) {
				deviceExtensions.push_back(VK_KHR_IMAGELESS_FRAMEBUFFER_EXTENSION_NAME);
			}
			if (std::string(extension.extensionName) == VK_EXT_SHADER_VIEWPORT_INDEX_LAYER_EXTENSION_NAME) {
				supportedExtensions.ext_shader_viewport_index_layer = true;
				deviceExtensions.push_back(VK_EXT_SHADER_VIEWPORT_INDEX_LAYER_EXTENSION_NAME);
			}
			if (std::string(extension.extensionName) == VK_EXT_MEMORY_BUDGET_EXTENSION_NAME) {
				supportedExtensions.ext_memory_budget = true;
				deviceExtensions.push_back(VK_EXT_MEMORY_BUDGET_EXTENSION_NAME);
			}
		}
		if (std::find(deviceExtensions.begin(), deviceExtensions.end(), VK_EXT_DESCRIPTOR_INDEXING_EXTENSION_NAME) == deviceExtensions.end()) {
			SHARD3D_FATAL("Device does not support \"VK_EXT_descriptor_indexing\"!");
		}
		if (std::find(deviceExtensions.begin(), deviceExtensions.end(), VK_KHR_IMAGELESS_FRAMEBUFFER_EXTENSION_NAME) == deviceExtensions.end()) {
			SHARD3D_FATAL("Device does not support \"VK_KHR_imageless_framebuffer\"!");
		}
		createInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
		createInfo.ppEnabledExtensionNames = deviceExtensions.data();

		if (enableValidationLayers) {
			createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
			createInfo.ppEnabledLayerNames = validationLayers.data();
		} else {
			createInfo.enabledLayerCount = 0;
		}
		VkPhysicalDeviceImagelessFramebufferFeatures imagelessFramebuffer;
		createInfo.pNext = &imagelessFramebuffer;

		imagelessFramebuffer.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGELESS_FRAMEBUFFER_FEATURES;
		imagelessFramebuffer.imagelessFramebuffer = VK_TRUE;
		void** last_pNext = &imagelessFramebuffer.pNext;

		descriptor_indexing_features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES;
		// Enable non-uniform indexing
		descriptor_indexing_features.runtimeDescriptorArray = VK_TRUE;
		descriptor_indexing_features.shaderSampledImageArrayNonUniformIndexing = VK_TRUE;
		descriptor_indexing_features.shaderStorageBufferArrayNonUniformIndexing = VK_TRUE;
		descriptor_indexing_features.descriptorBindingVariableDescriptorCount = VK_FALSE; // we will use fixed size
		descriptor_indexing_features.descriptorBindingPartiallyBound = VK_TRUE;
		descriptor_indexing_features.descriptorBindingStorageBufferUpdateAfterBind = VK_TRUE;
		descriptor_indexing_features.descriptorBindingSampledImageUpdateAfterBind = VK_TRUE;

		*last_pNext = &descriptor_indexing_features;
		last_pNext = &descriptor_indexing_features.pNext;

		if (supportedExtensions.khr_fragment_shading_rate) {
			khr_fragment_shading_rate_features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_FEATURES_KHR;
			khr_fragment_shading_rate_features.primitiveFragmentShadingRate = VK_FALSE;
			khr_fragment_shading_rate_features.pipelineFragmentShadingRate = VK_TRUE;
			khr_fragment_shading_rate_features.attachmentFragmentShadingRate = VK_FALSE;
			*last_pNext = &khr_fragment_shading_rate_features;
			last_pNext = &khr_fragment_shading_rate_features.pNext;
		}
		if (supportedExtensions.khr_acceleration_structure){
			khr_acceleration_structure_features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR;
			khr_acceleration_structure_features.accelerationStructure = VK_TRUE;
			*last_pNext = &khr_acceleration_structure_features;
			last_pNext = &khr_acceleration_structure_features.pNext;
		}
		if (supportedExtensions.nv_device_diagnostic_checkpoints) {
			nv_device_diagnostic_checkpoints_features.sType = VK_STRUCTURE_TYPE_DEVICE_DIAGNOSTICS_CONFIG_CREATE_INFO_NV;
			nv_device_diagnostic_checkpoints_features.diagnosticsConfig = VK_TRUE;
			*last_pNext = &nv_device_diagnostic_checkpoints_features;
			last_pNext = &nv_device_diagnostic_checkpoints_features.pNext;
		}

		VK_ASSERT(vkCreateDevice(physicalDevice_, &createInfo, nullptr, &device_), "failed to create logical device!");
		SHARD3D_INFO("Created Logical Device");
	}

	void S3DDevice::createCommandPool() {
		QueueFamilyIndices queueFamilyIndices = findQueueFamilies(physicalDevice_);

		VkCommandPoolCreateInfo graphicsPoolInfo{};
		graphicsPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		graphicsPoolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
		graphicsPoolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily;
		VK_ASSERT(vkCreateCommandPool(device_, &graphicsPoolInfo, nullptr, &commandPool), "failed to create command pool");
	}

	uint32_t S3DDevice::findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties) {
		VkPhysicalDeviceMemoryProperties memProperties;
		vkGetPhysicalDeviceMemoryProperties(physicalDevice_, &memProperties);
		for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
			if ((typeFilter & (1 << i)) &&
				(memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
				return i;
			}
		}
		return UINT32_MAX;
	}

	SwapChainSupportDetails S3DDevice::getSwapChainSupport(VkSurfaceKHR surface) {
		SwapChainSupportDetails supportDetails{};
		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice_, surface, &supportDetails.capabilities);
		uint32_t formatCount;
		vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice_, surface, &formatCount, nullptr);

		if (formatCount != 0) {
			supportDetails.formats.resize(formatCount);
			vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice_, surface, &formatCount, supportDetails.formats.data());
		}

		uint32_t presentModeCount;
		vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice_, surface, &presentModeCount, nullptr);

		if (presentModeCount != 0) {
			supportDetails.presentModes.resize(presentModeCount);
			vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice_, surface, &presentModeCount, supportDetails.presentModes.data());
		}

		return supportDetails;
	}

	// TODO: fix this, not all devices have more than 2 queues so 
	S3DQueue* S3DDevice::getAvailableQueue(QueueType type) {
		switch (type) {
		case(QueueType::Graphics):
			for (S3DQueue& queue : graphicsQueues) {
				return &queue;
				//if (!queue.occupied) {
				//	queue.occupied = true;
				//	return &queue;
				//}
			}
		}
	}

	void S3DDevice::freeAvailableQueue(S3DQueue* queue) {
		queue->occupied = false;
	}

	VkCommandBuffer S3DDevice::beginSingleTimeCommands() {
		VkCommandBufferAllocateInfo allocInfo{};
		allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		allocInfo.commandPool = commandPool;
		allocInfo.commandBufferCount = 1;

		VkCommandBuffer commandBuffer;
		vkAllocateCommandBuffers(device_, &allocInfo, &commandBuffer);

		VkCommandBufferBeginInfo beginInfo{};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

		vkBeginCommandBuffer(commandBuffer, &beginInfo);
		return commandBuffer;
	}

	void S3DDevice::endSingleTimeCommands(VkCommandBuffer commandBuffer) {
		vkEndCommandBuffer(commandBuffer);

		VkSubmitInfo submitInfo{};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &commandBuffer;

		S3DQueue* queue = getAvailableQueue(QueueType::Graphics);

		VK_ASSERT(vkQueueSubmit(queue->queue, 1, &submitInfo, VK_NULL_HANDLE), "failed to submit single time commands!");
		vkQueueWaitIdle(queue->queue);

		freeAvailableQueue(queue);
		vkFreeCommandBuffers(device_, commandPool, 1, &commandBuffer);
	}


	S3DCheckpointData* S3DDevice::allocateCheckpointNVIDIA(S3DCheckpointType type, const char* debugname_MAX64CHAR) {
		S3DCheckpointData* check = new S3DCheckpointData(type, debugname_MAX64CHAR);
		checkpointDebugDataNVIDIA.push_back(check);
		return check;
	}

	VkDescriptorImageInfo S3DDevice::getDefaultImageDescriptorInfo() {
		return {
			defaultSampler,
			defaultImageView,
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
		};
	}

	VkDescriptorBufferInfo S3DDevice::getDefaultBufferDescriptorInfo() {
		return {
			defaultBuffer,
			0,
			VK_WHOLE_SIZE
		};
	}

	void S3DDevice::createBuffer(
		VkDeviceSize size,
		VkBufferUsageFlags usage,
		VkMemoryPropertyFlags properties,
		VkBuffer& buffer,
		VkDeviceMemory& bufferMemory
	) {
		VkBufferCreateInfo bufferInfo{};
		bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		bufferInfo.size = size;
		bufferInfo.usage = usage;
		bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

		if (vkCreateBuffer(device_, &bufferInfo, nullptr, &buffer) != VK_SUCCESS) {
			throw std::runtime_error("failed to create buffer!");
		}

		VkMemoryRequirements memRequirements;
		vkGetBufferMemoryRequirements(device_, buffer, &memRequirements);

		VkMemoryAllocateInfo allocInfo{};
		allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		allocInfo.allocationSize = memRequirements.size;
		allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, properties);

		if (vkAllocateMemory(device_, &allocInfo, nullptr, &bufferMemory) != VK_SUCCESS) {
			throw std::runtime_error("failed to allocate vertex buffer memory!");
		}

		vkBindBufferMemory(device_, buffer, bufferMemory, 0);
	}

	void S3DDevice::createBufferAV(VkDeviceSize size, VkBufferUsageFlags usage, VkBuffer& buffer, VkDeviceMemory& bufferMemory, VkDeviceSize& offset, void** mapped) {
		VkBufferCreateInfo bufferInfo{};
		bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		bufferInfo.size = size;
		bufferInfo.usage = usage;
		bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

		if (vkCreateBuffer(device_, &bufferInfo, nullptr, &buffer) != VK_SUCCESS) {
			throw std::runtime_error("failed to create buffer!");
		}

		uint32_t i = 0;
		bool valid = false;
		uint32_t blocks = static_cast<uint32_t>(std::ceil(float(size) / static_cast<float>(ProjectSystem::getEngineSettings().memory.memoryAllocatorBlockSizeBytes)));
		
		uint32_t fragmentCount = 0;
		while (!valid && i < bufferMemoryArenaBlocks.size()) {
			const BufferBlockData& data = bufferMemoryArenaBlocks[i];
			if (data.isEmptySpace) {
				if (blocks <= data.size) {
					valid = true;
				} else {
					i += data.size;
					fragmentCount++;
				}
			} else {
				i += data.size;
			}
		}
		if (!valid) {
			SHARD3D_FATAL("Out of buffer memory ("+std::to_string(fragmentCount)+" fragmented blocks)");
		}
		for (int j = i; j < i + blocks; j++) {
			assert(bufferMemoryArenaBlocks[j].isEmptySpace);
		}
		BufferBlockData& data = bufferMemoryArenaBlocks[i];
		data.size = blocks;
		data.next = i + blocks;
		data.isEmptySpace = false;

		offset = i * ProjectSystem::getEngineSettings().memory.memoryAllocatorBlockSizeBytes;
		bufferMemory = bufferMemoryArena;
		*mapped = bufferMemoryArenaMapped;
		vkBindBufferMemory(device_, buffer, bufferMemoryArena, offset);
	}

	void S3DDevice::freeBufferAV(VkDeviceSize size, VkDeviceSize offset) {
		// TODO: fix
		
		//uint32_t i = static_cast<uint32_t>(std::ceil(float(offset) / static_cast<float>(ProjectSystem::getEngineSettings().memory.memoryAllocatorBlockSizeBytes)));
		//uint32_t blocks = static_cast<uint32_t>(std::ceil(float(size) / static_cast<float>(ProjectSystem::getEngineSettings().memory.memoryAllocatorBlockSizeBytes)));
		//
		//BufferBlockData& data = bufferMemoryArenaBlocks[i];
		//data.size = bufferMemoryArenaBlocks[i + blocks].isEmptySpace ? bufferMemoryArenaBlocks[i + blocks].size + blocks : blocks;
		//data.next = i + data.size;
		//data.isEmptySpace = true;
	}

	void S3DDevice::createImageWithInfo(
		const VkImageCreateInfo& imageInfo,
		VkMemoryPropertyFlags properties,
		//VkSampleCountFlagBits numSamples,
		VkImage& image,
		VkDeviceMemory& imageMemory
	) {
		if (vkCreateImage(device_, &imageInfo, nullptr, &image) != VK_SUCCESS) {
			throw std::runtime_error("Failed to create image!");
		}

		//imageInfo.samples = numSamples;

		VkMemoryRequirements memRequirements;
		vkGetImageMemoryRequirements(device_, image, &memRequirements);

		VkMemoryAllocateInfo allocInfo{};
		allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		allocInfo.allocationSize = memRequirements.size;
		allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, properties);

		if (vkAllocateMemory(device_, &allocInfo, nullptr, &imageMemory) != VK_SUCCESS) {
			throw std::runtime_error("Failed to allocate image memory!");
		}

		if (vkBindImageMemory(device_, image, imageMemory, 0) != VK_SUCCESS) {
			throw std::runtime_error("Failed to bind image memory!");
		}
	}

	void S3DDevice::copyBuffer(VkCommandBuffer commandBuffer, VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size, VkDeviceSize srcOffset, VkDeviceSize dstOffset) {
		VkBufferCopy bufferCopy{};
		bufferCopy.size = size;
		bufferCopy.srcOffset = srcOffset;
		bufferCopy.dstOffset = dstOffset;

		vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &bufferCopy);
	}

	void S3DDevice::copyImage(VkCommandBuffer commandBuffer, VkExtent3D extent, VkImage srcImage, VkImageLayout srcLayout, VkImageSubresourceLayers srcSubresourceLayers, VkOffset3D srcOffset, VkImage dstImage, VkImageLayout dstLayout, VkImageSubresourceLayers dstSubresourceLayers, VkOffset3D dstOffset) {
		VkImageCopy imageCopy{};
		imageCopy.extent = extent;
		imageCopy.srcSubresource = srcSubresourceLayers;
		imageCopy.srcOffset = srcOffset;
		imageCopy.dstSubresource = dstSubresourceLayers;
		imageCopy.dstOffset = dstOffset;
		vkCmdCopyImage(commandBuffer, srcImage, srcLayout, dstImage, dstLayout, 1, &imageCopy);
	}

	void S3DDevice::copyBufferToImage(
		VkCommandBuffer commandBuffer, VkBuffer buffer, VkImage image, uint32_t width, uint32_t height, uint32_t layerCount) {

		VkBufferImageCopy region{};
		region.bufferOffset = 0;
		region.bufferRowLength = 0;
		region.bufferImageHeight = 0;

		region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		region.imageSubresource.mipLevel = 0;
		region.imageSubresource.baseArrayLayer = 0;
		region.imageSubresource.layerCount = layerCount;

		region.imageOffset = { 0, 0, 0 };
		region.imageExtent = { width, height, 1 };

		vkCmdCopyBufferToImage(
			commandBuffer,
			buffer,
			image,
			VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			1,
			&region);
	}

	void S3DDevice::copyImageToBuffer(
		VkCommandBuffer commandBuffer, VkImage image, VkImageLayout layout, VkBuffer buffer, uint32_t width, uint32_t height, uint32_t layerCount, uint32_t mipLevel, VkDeviceSize dstOffset) {
		
		VkBufferImageCopy region{};
		region.bufferOffset = dstOffset;
		region.bufferRowLength = 0;
		region.bufferImageHeight = 0;

		region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		region.imageSubresource.mipLevel = mipLevel;
		region.imageSubresource.baseArrayLayer = 0;
		region.imageSubresource.layerCount = layerCount;

		region.imageOffset = { 0, 0, 0 };
		region.imageExtent = { width, height, 1 };

		vkCmdCopyImageToBuffer(
			commandBuffer,
			image,
			layout,
			buffer,
			1,
			&region
		);
	}

	void S3DDevice::generateMipMaps(
		VkCommandBuffer commandBuffer, 
		VkImage image, 
		VkFormat format, 
		uint32_t width, 
		uint32_t height, 
		uint32_t depth,
		uint32_t mipLevels,
		uint32_t layerCount,
		VkImageLayout finalLayout
	) {
		VkFormatProperties formatProperties;
		vkGetPhysicalDeviceFormatProperties(physicalDevice_, format, &formatProperties);
		if (!(formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT)) {
			SHARD3D_ERROR("Texture does not support linear blitting! No mip maps are being generated...");
			return;
		}

		VkImageMemoryBarrier barrier{};
		barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;

		barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

		barrier.image = image;
		barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		barrier.subresourceRange.baseMipLevel = 0;
		barrier.subresourceRange.levelCount = 1;
		barrier.subresourceRange.baseArrayLayer = 0;
		barrier.subresourceRange.layerCount = layerCount;

		int mipWidth = width;
		int mipHeight = height;
		int mipDepth = depth;

		for (uint32_t i = 1; i < mipLevels; i++) {
			//VkImageMemoryBarrier barrier{ ibarrier };
			barrier.subresourceRange.baseMipLevel = i - 1;
			barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

			vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier);

			VkImageBlit blit{};

			blit.srcOffsets[0] = { 0, 0, 0 };
			blit.srcOffsets[1] = { mipWidth, mipHeight, mipDepth };
			blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			blit.srcSubresource.mipLevel = i - 1;
			blit.srcSubresource.baseArrayLayer = 0;
			blit.srcSubresource.layerCount = layerCount;
			
			blit.dstOffsets[0] = { 0, 0, 0 };
			blit.dstOffsets[1] = { mipWidth > 1 ? mipWidth / 2 : 1, mipHeight > 1 ? mipHeight / 2 : 1, mipDepth > 1 ? mipDepth / 2 : 1 };
			blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			blit.dstSubresource.mipLevel = i;
			blit.dstSubresource.baseArrayLayer = 0;
			blit.dstSubresource.layerCount = layerCount;

			vkCmdBlitImage(commandBuffer, image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &blit, VK_FILTER_LINEAR);

			if (finalLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
				barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
				barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
				barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
				barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
				vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier);
			}


			if (mipWidth > 1) mipWidth /= 2;
			if (mipHeight > 1) mipHeight /= 2;
			if (mipDepth > 1) mipDepth /= 2;
		}

		barrier.subresourceRange.baseMipLevel = mipLevels - 1;

		if (finalLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
			barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
			vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier);
		} else if (finalLayout == VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL) {
			barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
			vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier);
		} 
	}
	
void S3DDevice::resizeTexture(VkCommandBuffer commandBuffer, VkImage image, VkFormat format, uint32_t oldWidth, uint32_t oldHeight, uint32_t newWidth, uint32_t newHeight, VkImageLayout finalLayout) {
    VkFormatProperties formatProperties;
    vkGetPhysicalDeviceFormatProperties(physicalDevice_, format, &formatProperties);
    if (!(formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT)) {
        SHARD3D_ERROR("Texture does not support linear blitting! No mip maps are being generated...");
        return;
    }

    VkImageMemoryBarrier barrier{};
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;

    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

    barrier.image = image;
    barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    barrier.subresourceRange.baseMipLevel = 0;
    barrier.subresourceRange.levelCount = 1;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = 1;

    barrier.subresourceRange.baseMipLevel = 0;
    barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
    barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

    vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier);

    VkImageBlit blit{};

    blit.srcOffsets[0] = { 0, 0, 0 };
    blit.srcOffsets[1] = { (int)oldWidth, (int)oldHeight, 1 };
    blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    blit.srcSubresource.mipLevel = 0;
    blit.srcSubresource.baseArrayLayer = 0;
    blit.srcSubresource.layerCount = 1;

    blit.dstOffsets[0] = { 0, 0, 0 };
    blit.dstOffsets[1] = { (int)newWidth, (int)newHeight, 1 };
    blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    blit.dstSubresource.mipLevel = 0;
    blit.dstSubresource.baseArrayLayer = 0;
    blit.dstSubresource.layerCount = 1;

    vkCmdBlitImage(commandBuffer, image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &blit, VK_FILTER_LINEAR);

    if (finalLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
        barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
        vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier);
    } else if (finalLayout == VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL) {
        barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier);
    }
}
	void S3DDevice::transitionImageLayout(
		VkCommandBuffer commandBuffer,
		VkImage image,
		VkFormat format,
		VkImageLayout oldLayout,
		VkImageLayout newLayout,
		uint32_t baseMipLevel, 
		uint32_t mipLevels,
		uint32_t baseLayer, 
		uint32_t layerCount
	) {
		// uses an image memory barrier transition image layouts and transfer queue
		// family ownership when VK_SHARING_MODE_EXCLUSIVE is used. There is an
		// equivalent buffer memory barrier to do this for buffers

		VkImageMemoryBarrier barrier{};
		barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		barrier.oldLayout = oldLayout;
		barrier.newLayout = newLayout;

		barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

		barrier.image = image;
		barrier.subresourceRange.baseMipLevel = baseMipLevel;
		barrier.subresourceRange.levelCount = mipLevels;
		barrier.subresourceRange.baseArrayLayer = baseLayer;
		barrier.subresourceRange.layerCount = layerCount;

		if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL || format == VK_FORMAT_D32_SFLOAT_S8_UINT || 
			format == VK_FORMAT_D24_UNORM_S8_UINT || format == VK_FORMAT_D32_SFLOAT || format == VK_FORMAT_D16_UNORM || format == VK_FORMAT_D16_UNORM_S8_UINT || format == VK_FORMAT_X8_D24_UNORM_PACK32) {
			barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
			if (format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT || format == VK_FORMAT_D16_UNORM_S8_UINT) {
				barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
			}
		} else {
			barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		}

		VkPipelineStageFlags sourceStage;
		VkPipelineStageFlags destinationStage;

		if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
			barrier.srcAccessMask = 0;
			barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

			sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
			destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		} else if (
			oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_GENERAL) {
			barrier.srcAccessMask = 0;
			barrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT;

			sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
			destinationStage = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
		} else if (
			oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL) {
			barrier.srcAccessMask = 0;
			barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

			sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
			destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		} else if (
			oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL &&
			newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

			sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
			destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
		} else if (
			oldLayout == VK_IMAGE_LAYOUT_UNDEFINED &&
			newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
			barrier.srcAccessMask = 0;
			barrier.dstAccessMask =
				VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

			sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
			destinationStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
		} else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL) {
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

			sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
			destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		} else if (oldLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL) {
			barrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
			barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

			sourceStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
			destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		} else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT ;
			barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

			sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT ;
			destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
		} else if (
			oldLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
			barrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
			barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

			sourceStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT ;
			destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		} else if (
			oldLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_UNDEFINED) {
			barrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
			barrier.dstAccessMask = VK_ACCESS_NONE;

			sourceStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
			destinationStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		} else if (
			oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
			barrier.srcAccessMask = 0;
			barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

			sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
			destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
		} else if (
			oldLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_UNDEFINED) {
			barrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
			barrier.dstAccessMask = VK_ACCESS_NONE;

			sourceStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
			destinationStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		} else if (
			oldLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_GENERAL) {
			barrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
			barrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT;

			sourceStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
			destinationStage = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
		} else if (
			oldLayout == VK_IMAGE_LAYOUT_GENERAL  && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
			barrier.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT; 
			barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

			sourceStage = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT; 
			destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
		} else if (
			oldLayout == VK_IMAGE_LAYOUT_GENERAL && newLayout == VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL) {
			barrier.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT;
			barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

			sourceStage = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
			destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		} else if (
			oldLayout == VK_IMAGE_LAYOUT_GENERAL && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
			barrier.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT;
			barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

			sourceStage = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
			destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		} else if (
			oldLayout == VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_GENERAL) {
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
			barrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT;

			sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
			destinationStage = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
		} else if (
			oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_GENERAL) {
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			barrier.dstAccessMask = VK_ACCESS_MEMORY_WRITE_BIT | VK_ACCESS_MEMORY_WRITE_BIT;

			sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
			destinationStage = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
		} else {
			SHARD3D_FATAL("Unsupported layout transition!");
		}
		vkCmdPipelineBarrier(
			commandBuffer,
			sourceStage,
			destinationStage,
			0,
			0,
			nullptr,
			0,
			nullptr,
			1,
			&barrier
		);
	}
}