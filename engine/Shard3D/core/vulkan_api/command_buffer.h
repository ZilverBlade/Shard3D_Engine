#pragma once
#include "device.h"
namespace Shard3D {
	class S3DCommandBuffer {
	public:
		S3DCommandBuffer(S3DDevice& device);
		~S3DCommandBuffer();
		S3DCommandBuffer(const S3DCommandBuffer&) = delete;
		S3DCommandBuffer& operator=(const S3DCommandBuffer&) = delete;

		VkCommandBuffer getCommandBuffer() {
			return commandBuffer;
		}
		void begin(VkCommandBufferUsageFlags flags = VkFlags());
		void end();
		void reset(VkCommandBufferResetFlags flags = VkFlags());
	private:
		VkCommandBuffer commandBuffer{};
		VkCommandPool commandPool;
		S3DDevice& engineDevice;
	};

}