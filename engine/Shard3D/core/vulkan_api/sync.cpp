#include "sync.h"
#include "buffer.h"
namespace Shard3D {
	S3DSynchronization::S3DSynchronization(SynchronizationType syncType_, VkPipelineStageFlags graphicsStage) : syncType(syncType_) {
		switch (syncType_) {
		case(SynchronizationType::GraphicsToCompute):
			dstStageMask = VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;
			srcStageMask = graphicsStage;
			break;
		case(SynchronizationType::ComputeToCompute):
			srcStageMask = dstStageMask = VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;
			break;
		case(SynchronizationType::ComputeToGraphics):
			dstStageMask = graphicsStage;
			srcStageMask = VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;
			break;
		case(SynchronizationType::Transfer):
			dstStageMask = srcStageMask = VK_PIPELINE_STAGE_TRANSFER_BIT;
			break;
		case(SynchronizationType::DebugBarrier):
			dstStageMask = srcStageMask = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
		}
		if (syncType == SynchronizationType::DebugBarrier) {
			VkMemoryBarrier memoryBarrier{};
			memoryBarrier.sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER;
			memoryBarrier.srcAccessMask = VK_ACCESS_INDIRECT_COMMAND_READ_BIT |
				   VK_ACCESS_INDEX_READ_BIT |
				   VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT |
				   VK_ACCESS_UNIFORM_READ_BIT |
				   VK_ACCESS_INPUT_ATTACHMENT_READ_BIT |
				   VK_ACCESS_SHADER_READ_BIT |
				   VK_ACCESS_SHADER_WRITE_BIT |
				   VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |
				   VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT |
				   VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT |
				   VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT |
				   VK_ACCESS_TRANSFER_READ_BIT |
				   VK_ACCESS_TRANSFER_WRITE_BIT |
				   VK_ACCESS_HOST_READ_BIT |
				   VK_ACCESS_HOST_WRITE_BIT,
			memoryBarrier.dstAccessMask = VK_ACCESS_INDIRECT_COMMAND_READ_BIT |
				   VK_ACCESS_INDEX_READ_BIT |
				   VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT |
				   VK_ACCESS_UNIFORM_READ_BIT |
				   VK_ACCESS_INPUT_ATTACHMENT_READ_BIT |
				   VK_ACCESS_SHADER_READ_BIT |
				   VK_ACCESS_SHADER_WRITE_BIT |
				   VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |
				   VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT |
				   VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT |
				   VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT |
				   VK_ACCESS_TRANSFER_READ_BIT |
				   VK_ACCESS_TRANSFER_WRITE_BIT |
				   VK_ACCESS_HOST_READ_BIT |
				   VK_ACCESS_HOST_WRITE_BIT;
			memoryBarriers.push_back(memoryBarrier);
		}
	}
	void S3DSynchronization::addImageBarrier(
		SynchronizationAttachment syncAttachment_, VkImage toSyncImage, VkImageSubresourceRange subresourceRange, VkImageLayout oldLayout, VkImageLayout newLayout) {
		VkImageMemoryBarrier imageMemoryBarrier{};
		imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		imageMemoryBarrier.newLayout = newLayout;

		/* .image and .subresourceRange should identify image subresource accessed */
		imageMemoryBarrier.image = toSyncImage;
		imageMemoryBarrier.subresourceRange = subresourceRange;
		imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

		// check if the new layout will be written to
		if (newLayout == VK_IMAGE_LAYOUT_GENERAL)
			imageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_SHADER_WRITE_BIT;
		else
			imageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		
		if (syncType == SynchronizationType::GraphicsToCompute) {

			// for some reason VK_IMAGE_LAYOUT_UNDEFINED makes the image not visible in RenderDoc, however validation and synchronisation dont throw errors
			// VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL appears to work according to synchronisation layers, but is wrong according to validation layers
			// VK_IMAGE_LAYOUT_GENERAL is fine with the validation layers, however is wrong according to synchronisation, so I have no idea what to do
			imageMemoryBarrier.oldLayout = oldLayout; // VK_IMAGE_LAYOUT_UNDEFINED;

			switch (syncAttachment_) {
			case (SynchronizationAttachment::Color):
				imageMemoryBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
				SHARD3D_ASSERT(srcStageMask == VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT && "Invalid synchronization!");
				break;
			case (SynchronizationAttachment::Depth):
				imageMemoryBarrier.srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
				SHARD3D_ASSERT(srcStageMask == (VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT) && "Invalid synchronization!");
				break;
			}
		}
		else { // If origin is from compute the image must have been written to
			imageMemoryBarrier.oldLayout = oldLayout;// VK_IMAGE_LAYOUT_GENERAL;
			imageMemoryBarrier.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT;
		}
		imageMemoryBarriers.push_back(imageMemoryBarrier);
	}

	void S3DSynchronization::addMemoryBarrier(VkAccessFlags syncMemoryAccess_) {
		SHARD3D_ASSERT(syncType != SynchronizationType::GraphicsToCompute && "Cannot add a memory barrier coming from a graphics stage!");
		VkMemoryBarrier memoryBarrier{};
		memoryBarrier.sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER;
		memoryBarrier.dstAccessMask = syncMemoryAccess_;
		memoryBarrier.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT;

		memoryBarriers.push_back(memoryBarrier);
	}

	void S3DSynchronization::addBufferMemoryBarrier(VkAccessFlags syncMemoryAccessSrc, VkAccessFlags syncMemoryAccessDst, S3DBuffer* buffer) {
		VkBufferMemoryBarrier bufferMemoryBarrier{};
		bufferMemoryBarrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;

		bufferMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		bufferMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

		bufferMemoryBarrier.srcAccessMask = syncMemoryAccessSrc;
		bufferMemoryBarrier.dstAccessMask = syncMemoryAccessDst;

		bufferMemoryBarrier.buffer = buffer->getBuffer();
		bufferMemoryBarrier.size = buffer->getBufferSize();
		bufferMemoryBarrier.offset = 0;

		bufferMemoryBarriers.push_back(bufferMemoryBarrier);
	}

	void S3DSynchronization::syncBarrier(VkCommandBuffer commandBuffer) {
		SHARD3D_ASSERT(syncType != SynchronizationType::None);
		vkCmdPipelineBarrier(
			commandBuffer,
			srcStageMask,					// srcStageMask
			dstStageMask,					// dstStageMask
			0,								// dependencyFlags
			memoryBarriers.size(),			// memoryBarrierCount         
			memoryBarriers.data(),			// pMemoryBarriers
			bufferMemoryBarriers.size(),	// bufferMemoryBarrierCount
			bufferMemoryBarriers.data(),	// pBufferMemoryBarriers
			imageMemoryBarriers.size(),		// imageMemoryBarrierCount
			imageMemoryBarriers.data()		// pImageMemoryBarriers
		);
	}
}