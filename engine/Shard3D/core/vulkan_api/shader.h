#pragma once
#include "device.h"
#include "../../systems/computational/shader_system.h"
#include "../../utils/engine_utils.h"
#include "../../s3dstd.h" 
namespace Shard3D {

	class S3DShaderSpecialization {
	public:
		S3DShaderSpecialization() = default;
		S3DShaderSpecialization(const S3DShaderSpecialization&) = default;
		S3DShaderSpecialization& operator =(const S3DShaderSpecialization&) = default;

		void addConstant(size_t location, void* value, size_t size);
		template <typename T>
		void addConstant(size_t location, T value) {
			addConstant(location, &value, sizeof(T));
		}

		VkSpecializationInfo getSpecializationInfo() const;
	private:
		std::vector<VkSpecializationMapEntry> entries;
		std::vector<char> data;
	};

	class S3DShader {
	public:
		S3DShader(
			ShaderType type, 
			const std::string& rawShaderFile,
			const std::string& subdirectory,
			const S3DShaderSpecialization& specializationInfo = S3DShaderSpecialization(),
			const std::vector<std::string> definitions = std::vector<std::string>(),
			const std::string& overrideFile = std::string()
		);
		S3DShader(
			ShaderType type,
			const std::string& raw_code,
			const S3DShaderSpecialization& specializationInfo = S3DShaderSpecialization(),
			const std::vector<std::string> definitions = std::vector<std::string>()
		);
		
		~S3DShader();

		S3DShader() = default;
 		S3DShader(const S3DShader&) = default;
		S3DShader& operator =(const S3DShader&) = default;

		VkShaderModule createShaderModule(S3DDevice& device) const;
		VkShaderStageFlagBits getVkShaderStage() const { 
			switch (type) {
			case Shard3D::ShaderType::Vertex:
				return VK_SHADER_STAGE_VERTEX_BIT;
				
			case Shard3D::ShaderType::Fragment:
				return VK_SHADER_STAGE_FRAGMENT_BIT;
				
			case Shard3D::ShaderType::TesselationCtrl:
				return VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
				
			case Shard3D::ShaderType::TesselationEval:
				return VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
				
			case Shard3D::ShaderType::Geometry:
				return VK_SHADER_STAGE_GEOMETRY_BIT;
				
			case Shard3D::ShaderType::Compute:
				return VK_SHADER_STAGE_COMPUTE_BIT;
				
			}
		}
		const VkSpecializationInfo getSpecializationInfo() const {
			return specializationInfo.getSpecializationInfo();
		}
	private:
		S3DShaderSpecialization specializationInfo;
		ShaderType type;
		std::string originFile;
		std::string compiledFile;
		std::vector<char> compiledCode;
	};
}