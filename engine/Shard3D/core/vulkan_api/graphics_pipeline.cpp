#pragma once
#include "../../s3dpch.h"
#include "graphics_pipeline.h"
#include "../asset/primitives.h"

namespace Shard3D {

	S3DGraphicsPipelineConfigInfo::S3DGraphicsPipelineConfigInfo(uint32_t colorAttachmentCount) {
		colorBlendAttachments.resize(colorAttachmentCount);
		inputAssemblyInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
		inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		inputAssemblyInfo.primitiveRestartEnable = VK_FALSE;
		inputAssemblyInfo.flags = 0;

		viewportInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
		viewportInfo.viewportCount = 1;
		viewportInfo.pViewports = nullptr;
		viewportInfo.scissorCount = 1;
		viewportInfo.pScissors = nullptr;

		//RASTERIZATION
		rasterizationInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
		rasterizationInfo.depthClampEnable = VK_FALSE;
		rasterizationInfo.rasterizerDiscardEnable = VK_FALSE;
		rasterizationInfo.polygonMode = VK_POLYGON_MODE_FILL;
		rasterizationInfo.lineWidth = 1.0f;
		rasterizationInfo.cullMode = VK_CULL_MODE_NONE;
		rasterizationInfo.frontFace = VK_FRONT_FACE_CLOCKWISE;
		rasterizationInfo.depthBiasEnable = VK_FALSE;
		rasterizationInfo.depthBiasConstantFactor = 0.0f;  // Optional
		rasterizationInfo.depthBiasClamp = 0.0f;           // Optional
		rasterizationInfo.depthBiasSlopeFactor = 0.0f;     // Optional

		multisampleInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
		multisampleInfo.sampleShadingEnable = VK_FALSE;
		multisampleInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
		multisampleInfo.minSampleShading = 1.0f;           // Optional
		multisampleInfo.pSampleMask = nullptr;             // Optional
		multisampleInfo.alphaToCoverageEnable = VK_FALSE;  // Optional
		multisampleInfo.alphaToOneEnable = VK_FALSE;       // Optional

		for (auto& blendAttachment : colorBlendAttachments) {
			blendAttachment.colorWriteMask =
				VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT |
				VK_COLOR_COMPONENT_A_BIT;
			blendAttachment.blendEnable = VK_FALSE;
			blendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;   // Optional
			blendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;  // Optional
			blendAttachment.colorBlendOp = VK_BLEND_OP_ADD;              // Optional
			blendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;   // Optional
			blendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;  // Optional
			blendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;              // Optional
		}

		colorBlendInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
		colorBlendInfo.logicOpEnable = VK_FALSE;
		colorBlendInfo.logicOp = VK_LOGIC_OP_COPY;  // Optional
		colorBlendInfo.attachmentCount = colorAttachmentCount;
		colorBlendInfo.pAttachments = colorBlendAttachments.data();
		colorBlendInfo.blendConstants[0] = 0.0f;  // Optional
		colorBlendInfo.blendConstants[1] = 0.0f;  // Optional
		colorBlendInfo.blendConstants[2] = 0.0f;  // Optional
		colorBlendInfo.blendConstants[3] = 0.0f;  // Optional

		depthStencilInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
		depthStencilInfo.depthTestEnable = VK_TRUE;
		depthStencilInfo.depthWriteEnable = VK_TRUE;
		depthStencilInfo.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
		depthStencilInfo.depthBoundsTestEnable = VK_FALSE;
		depthStencilInfo.minDepthBounds = 0.0f;  // Optional
		depthStencilInfo.maxDepthBounds = 1.0f;  // Optional
		depthStencilInfo.stencilTestEnable = VK_FALSE;
		depthStencilInfo.front = {};  // Optional
		depthStencilInfo.back = {};   // Optional

		dynamicStateEnables = { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR };
	}

	void S3DGraphicsPipelineConfigInfo::enableVertexDescriptions(int descriptions) {
		bitswitch(descriptions) {
			bitcase(VertexDescriptionOptions_Position) {
				VectorUtils::merge(bindingDescriptions, Vertex::getPositionBindingDescriptions());
				VectorUtils::merge(attributeDescriptions, Vertex::getPositionAttributeDescriptions());
			}
			bitcase(VertexDescriptionOptions_UV) {
				VectorUtils::merge(bindingDescriptions, Vertex::getUVBindingDescriptions());
				VectorUtils::merge(attributeDescriptions, Vertex::getUVAttributeDescriptions());
			}
			bitcase(VertexDescriptionOptions_Normal) {
				VectorUtils::merge(bindingDescriptions, Vertex::getNormalBindingDescriptions());
				VectorUtils::merge(attributeDescriptions, Vertex::getNormalAttributeDescriptions());
			}
			bitcase(VertexDescriptionOptions_Tangent) {
				VectorUtils::merge(bindingDescriptions, Vertex::getTangentBindingDescriptions());
				VectorUtils::merge(attributeDescriptions, Vertex::getTangentAttributeDescriptions());
			}
		}
	}


	void S3DGraphicsPipelineConfigInfo::enableMultiplicativeBlending(uint32_t attachment) {
		colorBlendAttachments[attachment].blendEnable = VK_TRUE;
		colorBlendAttachments[attachment].colorWriteMask =
			VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT |
			VK_COLOR_COMPONENT_A_BIT;
		colorBlendAttachments[attachment].srcColorBlendFactor = VK_BLEND_FACTOR_ZERO;
		colorBlendAttachments[attachment].dstColorBlendFactor = VK_BLEND_FACTOR_SRC_COLOR;
		colorBlendAttachments[attachment].colorBlendOp = VK_BLEND_OP_ADD;
		colorBlendAttachments[attachment].srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
		colorBlendAttachments[attachment].dstAlphaBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
		colorBlendAttachments[attachment].alphaBlendOp = VK_BLEND_OP_ADD;

	}
	void S3DGraphicsPipelineConfigInfo::enableMultiplicativeAlphaBlending(uint32_t attachment) {
		colorBlendAttachments[attachment].blendEnable = VK_TRUE;
		colorBlendAttachments[attachment].colorWriteMask =
			VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT |
			VK_COLOR_COMPONENT_A_BIT;
		colorBlendAttachments[attachment].srcColorBlendFactor = VK_BLEND_FACTOR_DST_COLOR;
		colorBlendAttachments[attachment].dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
		colorBlendAttachments[attachment].colorBlendOp = VK_BLEND_OP_ADD;
		colorBlendAttachments[attachment].srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
		colorBlendAttachments[attachment].dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
		colorBlendAttachments[attachment].alphaBlendOp = VK_BLEND_OP_ADD;
	}
	void S3DGraphicsPipelineConfigInfo::enableAdditiveBlending(uint32_t attachment) {
		colorBlendAttachments[attachment].blendEnable = VK_TRUE;
		colorBlendAttachments[attachment].colorWriteMask =
			VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT |
			VK_COLOR_COMPONENT_A_BIT;
		colorBlendAttachments[attachment].srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
		colorBlendAttachments[attachment].dstColorBlendFactor = VK_BLEND_FACTOR_DST_ALPHA;
		colorBlendAttachments[attachment].colorBlendOp = VK_BLEND_OP_ADD;
		colorBlendAttachments[attachment].srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
		colorBlendAttachments[attachment].dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
		colorBlendAttachments[attachment].alphaBlendOp = VK_BLEND_OP_ADD;
	}
	void Shard3D::S3DGraphicsPipelineConfigInfo::enableAdditiveAlphaBlending(uint32_t attachment) {
		colorBlendAttachments[attachment].blendEnable = VK_TRUE;
		colorBlendAttachments[attachment].colorWriteMask =
			VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT |
			VK_COLOR_COMPONENT_A_BIT;
		colorBlendAttachments[attachment].srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
		colorBlendAttachments[attachment].dstColorBlendFactor = VK_BLEND_FACTOR_ONE;
		colorBlendAttachments[attachment].colorBlendOp = VK_BLEND_OP_ADD;
		colorBlendAttachments[attachment].srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
		colorBlendAttachments[attachment].dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
		colorBlendAttachments[attachment].alphaBlendOp = VK_BLEND_OP_ADD;
	}
	void S3DGraphicsPipelineConfigInfo::enableErasureBlending(uint32_t attachment) {
		colorBlendAttachments[attachment].blendEnable = VK_TRUE;
		colorBlendAttachments[attachment].colorWriteMask = VK_COLOR_COMPONENT_A_BIT;
		colorBlendAttachments[attachment].srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
		colorBlendAttachments[attachment].dstAlphaBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
		colorBlendAttachments[attachment].alphaBlendOp = VK_BLEND_OP_REVERSE_SUBTRACT;
	}
	void S3DGraphicsPipelineConfigInfo::enableAlphaBlending(uint32_t attachment) {
		colorBlendAttachments[attachment].blendEnable = VK_TRUE;
		colorBlendAttachments[attachment].colorWriteMask =
			VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT |
			VK_COLOR_COMPONENT_A_BIT;
		colorBlendAttachments[attachment].srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
		colorBlendAttachments[attachment].dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
		colorBlendAttachments[attachment].colorBlendOp = VK_BLEND_OP_ADD;
		colorBlendAttachments[attachment].srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
		colorBlendAttachments[attachment].dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
		colorBlendAttachments[attachment].alphaBlendOp = VK_BLEND_OP_ADD;
	}
	void S3DGraphicsPipelineConfigInfo::enableWeightedBlending() {
		SHARD3D_ASSERT(colorBlendAttachments.size() >= 2 && "Weighted blending requires at least 2 attachments! (RGBA16F & R8)");
		enableAlphaBlending(0);
		enableAlphaBlending(1);
		disableDepthWrite();
		colorBlendAttachments[0].srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
		colorBlendAttachments[0].dstColorBlendFactor = VK_BLEND_FACTOR_ONE;
		colorBlendAttachments[0].srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
		colorBlendAttachments[0].dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE;

		colorBlendAttachments[1].colorWriteMask = VK_COLOR_COMPONENT_R_BIT;
		colorBlendAttachments[1].srcColorBlendFactor = VK_BLEND_FACTOR_ZERO;
		colorBlendAttachments[1].dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR;
	}

	void S3DGraphicsPipelineConfigInfo::lineRasterizer(float thickness) {
		rasterizationInfo.polygonMode = VK_POLYGON_MODE_LINE;
		rasterizationInfo.lineWidth = thickness;
	}
	void S3DGraphicsPipelineConfigInfo::pointRasterizer() {
		rasterizationInfo.polygonMode = VK_POLYGON_MODE_POINT;
	}
	void S3DGraphicsPipelineConfigInfo::setCullMode(VkCullModeFlags cullMode) {
		rasterizationInfo.cullMode = cullMode;
	}

	void S3DGraphicsPipelineConfigInfo::setSampleCount(VkSampleCountFlagBits samples) {
		multisampleInfo.rasterizationSamples = samples;
		multisampleInfo.sampleShadingEnable = static_cast<VkBool32>(samples > VK_SAMPLE_COUNT_1_BIT);
	}

	void S3DGraphicsPipelineConfigInfo::reverseDepth() {
		depthStencilInfo.depthCompareOp = VK_COMPARE_OP_GREATER_OR_EQUAL;
	}

	void S3DGraphicsPipelineConfigInfo::disableDepthTest() {
		depthStencilInfo.depthTestEnable = VK_FALSE;
		depthStencilInfo.depthWriteEnable = VK_FALSE;
	}

	void S3DGraphicsPipelineConfigInfo::disableDepthWrite() {
		depthStencilInfo.depthWriteEnable = VK_FALSE;
	}

	S3DGraphicsPipeline::S3DGraphicsPipeline(S3DDevice& device, const std::vector<S3DShader>& shaders, const S3DGraphicsPipelineConfigInfo& configInfo) : engineDevice(device) {
		assert(configInfo.pipelineLayout != nullptr &&
				"Cannot create graphics pipeline:: no pipelineLayout provided in configInfo");
		assert(configInfo.renderPass != nullptr &&
			"Cannot create graphics pipeline:: no renderPass provided in configInfo");

		std::vector<VkPipelineShaderStageCreateInfo> shaderStages{};
		std::vector<VkShaderModule> shaderModules{};
		shaderStages.reserve(shaders.size());
		shaderModules.reserve(shaders.size());
		for (auto& shader : shaders) {
			VkPipelineShaderStageCreateInfo shaderStage{};
			VkShaderModule module = shader.createShaderModule(engineDevice);
			shaderModules.push_back(module);
			shaderStage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
			shaderStage.stage = shader.getVkShaderStage();
			shaderStage.module = module;
			shaderStage.pName = "main";
			shaderStage.flags = 0;
			shaderStage.pNext = nullptr;
			shaderStage.pSpecializationInfo = shader.getSpecializationInfo().pData ? &shader.getSpecializationInfo() : nullptr;
			shaderStages.push_back(shaderStage);
		}

		auto& bindingDescriptions = configInfo.bindingDescriptions;
		auto& attributeDescriptions = configInfo.attributeDescriptions;
		VkPipelineVertexInputStateCreateInfo vertexInputInfo{};
		vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
		vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
		vertexInputInfo.vertexBindingDescriptionCount = static_cast<uint32_t>(bindingDescriptions.size());
		vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();
		vertexInputInfo.pVertexBindingDescriptions = bindingDescriptions.data();

		VkPipelineDynamicStateCreateInfo dynamicStateInfo{};
		dynamicStateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
		dynamicStateInfo.pDynamicStates = configInfo.dynamicStateEnables.data();
		dynamicStateInfo.dynamicStateCount = static_cast<uint32_t>(configInfo.dynamicStateEnables.size());
		dynamicStateInfo.flags = 0;
		dynamicStateInfo.pNext = nullptr;

		VkGraphicsPipelineCreateInfo pipelineInfo{};
		pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
		if (configInfo.enablePipelineDerivatives) {
			if (configInfo.basePipeline == VK_NULL_HANDLE) {
				pipelineInfo.flags = VK_PIPELINE_CREATE_ALLOW_DERIVATIVES_BIT;
			} else {
				pipelineInfo.flags = VK_PIPELINE_CREATE_DERIVATIVE_BIT;
			}
		}
		pipelineInfo.stageCount = shaderStages.size();
		pipelineInfo.pStages = shaderStages.data();
		pipelineInfo.pVertexInputState = &vertexInputInfo;
		pipelineInfo.pDynamicState = &dynamicStateInfo;
		pipelineInfo.pInputAssemblyState = &configInfo.inputAssemblyInfo;
		pipelineInfo.pViewportState = &configInfo.viewportInfo;
		pipelineInfo.pRasterizationState = &configInfo.rasterizationInfo;
		pipelineInfo.pMultisampleState = &configInfo.multisampleInfo;
		pipelineInfo.pColorBlendState = &configInfo.colorBlendInfo;
		pipelineInfo.pDepthStencilState = &configInfo.depthStencilInfo;
		
		pipelineInfo.layout = configInfo.pipelineLayout;
		pipelineInfo.renderPass = configInfo.renderPass;
		pipelineInfo.subpass = 0;

		pipelineInfo.basePipelineIndex = -1;
		pipelineInfo.basePipelineHandle = configInfo.basePipeline;

		if (vkCreateGraphicsPipelines(engineDevice.device(), nullptr, 1, &pipelineInfo, nullptr, &pipeline) != VK_SUCCESS) {
			throw std::runtime_error("failed to create graphics pipeline!");
		}

		for (VkShaderModule sm : shaderModules) {
			vkDestroyShaderModule(engineDevice.device(), sm, nullptr);
		}
	}

	S3DGraphicsPipeline::~S3DGraphicsPipeline() {
		vkDestroyPipeline(engineDevice.device(), pipeline, nullptr);
	}

	void S3DGraphicsPipeline::bind(VkCommandBuffer commandBuffer) {
		vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
	}

}