#pragma once

#define VK_NO_PROTOTYPES
#include <volk.h>
#include "queue.h"
#include "checkpoints.h"
#include "../../s3dstd.h"

#define MACRO_CHECKPOINT_NVIDIA(device, ctype, cname, commandBuffer) \
	if (device.supportedExtensions.nv_device_diagnostic_checkpoints) { \
		static S3DCheckpointData* M_checkpdata___nested0000 = device.allocateCheckpointNVIDIA(ctype, cname); \
		M_checkpdata___nested0000->mark(commandBuffer); \
	}

namespace Shard3D {
	class S3DDescriptorPool;
	struct QueueFamilyIndices {
		uint32_t presentFamily = 0;
		uint32_t presentQueueCount = 0;

		uint32_t graphicsFamily = -1;
		uint32_t graphicsQueueCount = 0;

		uint32_t computeFamily = -1;
		uint32_t computeQueueCount = 0;
	};
	struct SupportedDeviceExtensions {
		bool khr_fragment_shading_rate = false;
		bool ext_conservative_rasterization = false;
		bool ext_shader_viewport_index_layer = false;
		bool ext_memory_budget = false;
		bool khr_ray_query = false; 
		bool khr_acceleration_structure = false;
		bool khr_deferred_host_operations = false;
		bool nv_device_diagnostic_checkpoints = false;
	};
	struct SwapChainSupportDetails {
		VkSurfaceCapabilitiesKHR capabilities;
		std::vector<VkSurfaceFormatKHR> formats;
		std::vector<VkPresentModeKHR> presentModes;
	};
	struct GraphicsCardInfo {
		const char* name;
		const char* type;
		VkDeviceSize memory;
	};
	class S3DDevice {
	public:
#ifndef NDEBUG
		const bool enableValidationLayers = true;
#else
		const bool enableValidationLayers = false;
#endif
		// Not copyable or movable
		S3DDevice(const S3DDevice&) = delete;
		S3DDevice& operator=(const S3DDevice&) = delete;
		S3DDevice(S3DDevice&&) = delete;
		S3DDevice& operator=(S3DDevice&&) = delete;

		S3DDevice();
		~S3DDevice();

		VkCommandPool getCommandPool() {
			return commandPool;
		}
		VkDevice device() {
			return device_;
		}
		VkPhysicalDevice physicalDevice() {
			return physicalDevice_;
		}
		VkInstance getInstance() {
			return instance;
		}
		GraphicsCardInfo getGPUInformation() {
			return gpuInfo;
		}
		VkSampleCountFlagBits getMaxUsableSampleCount();
		uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);

		SwapChainSupportDetails getSwapChainSupport(VkSurfaceKHR surface);

		S3DQueue* getAvailableQueue(QueueType type);
		void freeAvailableQueue(S3DQueue* queue);

		const VkPhysicalDeviceProperties& getDeviceProperties() { return deviceProperties; }
		const VkPhysicalDeviceFeatures& getDeviceFeatures() { return deviceFeatures; }
		QueueFamilyIndices findPhysicalQueueFamilies() {
			return findQueueFamilies(physicalDevice_);
		}

		S3DCheckpointData* allocateCheckpointNVIDIA(S3DCheckpointType type, const char* debugname_MAX64CHAR);

		VkDescriptorImageInfo getDefaultImageDescriptorInfo();
		VkDescriptorBufferInfo getDefaultBufferDescriptorInfo();

		void createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& bufferMemory);
		void createBufferAV(VkDeviceSize size, VkBufferUsageFlags usage, VkBuffer& buffer, VkDeviceMemory& bufferMemory, VkDeviceSize& offset, void** mapped);
		void freeBufferAV(VkDeviceSize size, VkDeviceSize offset);
		void createImageWithInfo(const VkImageCreateInfo& imageInfo, VkMemoryPropertyFlags properties, VkImage& image, VkDeviceMemory& imageMemory);

		VkCommandBuffer beginSingleTimeCommands();
		void endSingleTimeCommands(VkCommandBuffer commandBuffer);
		void copyBuffer(VkCommandBuffer commandBuffer, VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size, VkDeviceSize srcOffset = 0, VkDeviceSize dstOffset = 0);
		void copyImage(VkCommandBuffer commandBuffer, VkExtent3D extent, VkImage srcImage, VkImageLayout srcLayout, VkImageSubresourceLayers srcSubresourceLayers, VkOffset3D srcOffset, VkImage dstImage, VkImageLayout dstLayout, VkImageSubresourceLayers dstSubresourceLayers, VkOffset3D dstOffset);
		void copyBufferToImage(VkCommandBuffer commandBuffer, VkBuffer buffer, VkImage image, uint32_t width, uint32_t height, uint32_t layerCount);
		void copyImageToBuffer(VkCommandBuffer commandBuffer, VkImage image, VkImageLayout layout, VkBuffer buffer, uint32_t width, uint32_t height, uint32_t layerCount, uint32_t mipLevel, VkDeviceSize dstOffset = 0);
		void transitionImageLayout(VkCommandBuffer commandBuffer, VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout, uint32_t baseMipLevel, uint32_t mipLevels, uint32_t baseLayer, uint32_t layerCount);
		void generateMipMaps(VkCommandBuffer commandBuffer, VkImage image, VkFormat format, uint32_t width, uint32_t height, uint32_t depth, uint32_t mipLevels, uint32_t layerCount, VkImageLayout finalLayout);
		void resizeTexture(VkCommandBuffer commandBuffer, VkImage image, VkFormat format, uint32_t oldWidth, uint32_t oldHeight, uint32_t newWidth, uint32_t newHeight, VkImageLayout finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

		VkPhysicalDeviceProperties properties;
		VkPhysicalDeviceFeatures supportedFeatures;
		SupportedDeviceExtensions supportedExtensions;
		VkPhysicalDeviceConservativeRasterizationPropertiesEXT ext_conservative_rasterization_properties{};
		VkPhysicalDeviceAccelerationStructurePropertiesKHR khr_acceleration_structure_properties{};

		S3DDescriptorPool* staticMaterialPool;
	private:

		void createDefaultResourceObjects();

		VkPhysicalDeviceProperties deviceProperties{};
		VkPhysicalDeviceFeatures deviceFeatures{};
		VkPhysicalDeviceDescriptorIndexingFeatures descriptor_indexing_features{};
		VkPhysicalDeviceFragmentShadingRateFeaturesKHR khr_fragment_shading_rate_features{};
		VkPhysicalDeviceAccelerationStructureFeaturesKHR khr_acceleration_structure_features{};
		VkPhysicalDeviceDiagnosticsConfigFeaturesNV nv_device_diagnostic_checkpoints_features{};

		bool checkValidationLayerSupport();

		void createInstance();
		void setupDebugMessenger();
		void pickPhyisicalDevice();
		bool checkDeviceFeatureSupport(VkPhysicalDevice device);
		void createLogicalDevice();
		void createCommandPool();

		void requestDeviceFeatures(VkPhysicalDeviceFeatures& features);
		bool checkDeviceExtensionSupport(VkPhysicalDevice device);

		bool isSuitableDevice(VkPhysicalDevice device);
		bool isPreferredDevice(VkPhysicalDevice device);
		std::vector<const char*> getRequiredExtensions();
		QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device);
		void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo);
		void hasGlfwRequiredInstanceExtensions();

		void createArenaBuffer(VkDeviceSize sizeBytes);

		std::vector<S3DQueue> createQueues(QueueType queueType, uint32_t queueCount, uint32_t queueOffset);

		std::vector<S3DQueue> graphicsQueues{};
		std::vector<S3DQueue> computeQueues{};

		std::vector<S3DCheckpointData*> checkpointDebugDataNVIDIA{};

		VkCommandPool commandPool;
		VkInstance instance{};

		VkPhysicalDevice physicalDevice_ = nullptr;
		VkDevice device_;
		GraphicsCardInfo gpuInfo;

		VkDeviceMemory bufferMemoryArena;
		void* bufferMemoryArenaMapped;


		struct BufferBlockData {
			size_t size;
			size_t next;
			bool isEmptySpace;
		};
		std::vector<BufferBlockData> bufferMemoryArenaBlocks;

		VkDebugUtilsMessengerEXT debugMessenger{};
		std::vector<const char*> validationLayers = { "VK_LAYER_KHRONOS_validation" };
		std::vector<const char*> deviceExtensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };

		VkSampler defaultSampler;
		VkImage defaultImage;
		VkImageView defaultImageView;
		VkBuffer defaultBuffer;
		VkDeviceMemory defaultBufferMemory;
		VkDeviceMemory defaultImageMemory;

		bool deviceCreated = false;
		friend class S3DWindow;
	};
}