#pragma once
#include "device.h"
#include "shader.h"
namespace Shard3D {
	enum VertexDescriptionsFlags {
		VertexDescriptionOptions_Position = BIT(0),
		VertexDescriptionOptions_UV = BIT(1),
		VertexDescriptionOptions_Normal = BIT(2),
		VertexDescriptionOptions_Tangent = BIT(3)
	};
	struct S3DGraphicsPipelineConfigInfo {
		S3DGraphicsPipelineConfigInfo(uint32_t colorAttachmentCount = 1);
		S3DGraphicsPipelineConfigInfo(const S3DGraphicsPipelineConfigInfo&) = delete;
		S3DGraphicsPipelineConfigInfo& operator=(const S3DGraphicsPipelineConfigInfo&) = delete;

		std::vector<VkVertexInputBindingDescription> bindingDescriptions{};
		std::vector<VkVertexInputAttributeDescription> attributeDescriptions{};
		VkPipelineViewportStateCreateInfo viewportInfo{};
		VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo{};
		VkPipelineRasterizationStateCreateInfo rasterizationInfo{};
		VkPipelineMultisampleStateCreateInfo multisampleInfo{};
		std::vector<VkPipelineColorBlendAttachmentState> colorBlendAttachments{};
		VkPipelineColorBlendStateCreateInfo colorBlendInfo{};
		VkPipelineDepthStencilStateCreateInfo depthStencilInfo{};
		std::vector<VkDynamicState> dynamicStateEnables{};
		VkPipelineLayout pipelineLayout = nullptr;
		VkRenderPass renderPass = nullptr;

		bool enablePipelineDerivatives = false;
		VkPipeline basePipeline = VK_NULL_HANDLE;

		void enableVertexDescriptions(int descriptions);
		void enableMultiplicativeBlending(uint32_t attachment = 0);
		// REQUIRES IN SHADER SRC_COLOR = SRC_COLOR * SRC_ALPHA!!!!
		void enableMultiplicativeAlphaBlending(uint32_t attachment = 0);
		void enableAdditiveBlending(uint32_t attachment = 0);
		void enableAdditiveAlphaBlending(uint32_t attachment = 0);
		void enableErasureBlending(uint32_t attachment = 0);
		void enableAlphaBlending(uint32_t attachment = 0);
		void enableWeightedBlending();
		void lineRasterizer(float thickness = 1.0f);
		void pointRasterizer();
		void setCullMode(VkCullModeFlags cullMode);
		void setSampleCount(VkSampleCountFlagBits samples = VK_SAMPLE_COUNT_1_BIT);
		void reverseDepth();
		void disableDepthTest();
		void disableDepthWrite();
	};

	class  S3DGraphicsPipeline {
	public:
		S3DGraphicsPipeline(
			S3DDevice& device,
			const std::vector<S3DShader>& shaders,
			const S3DGraphicsPipelineConfigInfo& configInfo
		);
		~S3DGraphicsPipeline();
		
		S3DGraphicsPipeline(const S3DGraphicsPipeline&) = delete;
		S3DGraphicsPipeline& operator=(const S3DGraphicsPipeline&) = delete;

		void bind(VkCommandBuffer commandBuffer);
		VkPipeline getPipeline() {
			return pipeline;
		}
	private:
		VkPipeline pipeline;
		S3DDevice& engineDevice;
	};

}