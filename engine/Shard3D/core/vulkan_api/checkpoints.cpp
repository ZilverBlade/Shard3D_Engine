#include "checkpoints.h"
#include <vulkan/vk_enum_string_helper.h>
#include <magic_enum.hpp>
#include "../../utils/logger.h"
namespace Shard3D {
	S3DCheckpointData::S3DCheckpointData(S3DCheckpointType type, const char* name) : type__debug(type) {
		static_assert(magic_enum::is_magic_enum_supported && "magic enum is not supported");
		memset(name__debug, 0x00, MAX_CHARACTER_COUNT_CHECKPOINT_DEBUG_NAME);
		strncpy(name__debug, name, std::min(strlen(name), MAX_CHARACTER_COUNT_CHECKPOINT_DEBUG_NAME));
	}
	void S3DCheckpointData::mark(VkCommandBuffer commandBuffer) {
		vkCmdSetCheckpointNV(commandBuffer, this);
	}
	void S3DCheckpointData::print(VkPipelineStageFlagBits stage) {
		SHARD3D_ERROR("check point type {0}: ----> {1} (LAST PIPELINE STAGE {2})", magic_enum::enum_name(type__debug), name__debug, string_VkPipelineStageFlagBits(stage));
	}
}