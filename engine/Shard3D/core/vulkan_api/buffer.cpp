/*
 * Encapsulates a vulkan buffer
 *
 * Initially based off VulkanBuffer by Sascha Willems -
 * https://github.com/SaschaWillems/Vulkan/blob/master/base/VulkanBuffer.h
 */
#include "../../s3dpch.h" 
#include "buffer.h"

namespace Shard3D {

    /**
     * Returns the minimum instance size required to be compatible with devices minOffsetAlignment
     *
     * @param instanceSize The size of an instance
     * @param minOffsetAlignment The minimum required alignment, in bytes, for the offset member (eg
     * minUniformBufferOffsetAlignment)
     *
     * @return VkResult of the buffer mapping call
     */
    VkDeviceSize S3DBuffer::getAlignment(VkDeviceSize instanceSize, VkDeviceSize minOffsetAlignment) {
        if (minOffsetAlignment > 0) {
            return (instanceSize + minOffsetAlignment - 1) & ~(minOffsetAlignment - 1);
        }
        return instanceSize;
    }

    S3DBuffer::S3DBuffer(
        S3DDevice& device,
        VkDeviceSize instanceSize,
        uint32_t instanceCount,
        VkBufferUsageFlags usageFlags,
        VkMemoryPropertyFlags memoryPropertyFlags,
        bool useDedicatedDeviceMemory,
        VkDeviceSize minOffsetAlignment)
        : engineDevice{ device },
        instanceSize{ instanceSize },
        instanceCount{ instanceCount },
        usageFlags{ usageFlags },
        memoryPropertyFlags{ memoryPropertyFlags },
        usingDedicatedDeviceMemory{useDedicatedDeviceMemory}{

        alignmentSize = getAlignment(instanceSize, minOffsetAlignment);
        bufferSize = alignmentSize * instanceCount;
        if (useDedicatedDeviceMemory) {
            device.createBuffer(bufferSize, usageFlags, memoryPropertyFlags, buffer, memory);
        } else {
            device.createBufferAV(bufferSize, usageFlags, buffer, memory, mappedOffset, &mapped);
        }
    }

    S3DBuffer::~S3DBuffer() {
        unmap();
        vkDestroyBuffer(engineDevice.device(), buffer, nullptr);
        if (usingDedicatedDeviceMemory) {
            vkFreeMemory(engineDevice.device(), memory, nullptr);
        } else {
            engineDevice.freeBufferAV(bufferSize, mappedOffset);
        }
    }

    /**
     * Map a memory range of this buffer. If successful, mapped points to the specified buffer range.
     *
     * @param size (Optional) Size of the memory range to map. Pass VK_WHOLE_SIZE to map the complete
     * buffer range.
     * @param offset (Optional) Byte offset from beginning
     *
     * @return VkResult of the buffer mapping call
     */
    VkResult S3DBuffer::map(VkDeviceSize size, VkDeviceSize offset) {
        if (usingDedicatedDeviceMemory) {
            SHARD3D_ASSERT(buffer && memory && "Called map on buffer before create");
            return vkMapMemory(engineDevice.device(), memory, offset, size, 0, &mapped);
        } else {
            return VK_SUCCESS;
        }
    }

    /**
     * Unmap a mapped memory range
     *
     * @note Does not return a result as vkUnmapMemory can't fail
     */
    void S3DBuffer::unmap() {
        if (mapped && usingDedicatedDeviceMemory) {
            vkUnmapMemory(engineDevice.device(), memory);
            mapped = nullptr;
        }
    }

    /**
     * Copies the specified data to the mapped buffer. Default value writes whole buffer range
     *
     * @param data Pointer to the data to copy
     *
     */
    void S3DBuffer::writeToBuffer(const void* data) {
        SHARD3D_ASSERT(mapped && "Cannot copy to unmapped buffer");

        char* memOffset = (char*)mapped;
        memOffset += mappedOffset;
        memcpy(memOffset, data, bufferSize);
    }

    /**
     * Copies the specified data to the mapped buffer. Default value writes whole buffer range
     *
     * @param data Pointer to the data to copy
     * @param size Size of the data to copy. Pass VK_WHOLE_SIZE to flush the complete buffer
     * range.
     * @param offset Byte offset from beginning of mapped region
     *
     */
    void S3DBuffer::writeToBuffer(const void* data, VkDeviceSize offset, VkDeviceSize size) {
        SHARD3D_ASSERT(mapped && "Cannot copy to unmapped buffer");

        char* memOffset = (char*)mapped;
        memOffset += mappedOffset + offset;
        memcpy(memOffset, data, size);
    }

    /**
     * Flush a memory range of the buffer to make it visible to the device
     *
     * @note Only required for non-coherent memory
     *
     * @param size (Optional) Size of the memory range to flush. Pass VK_WHOLE_SIZE to flush the
     * complete buffer range.
     * @param offset (Optional) Byte offset from beginning
     *
     * @return VkResult of the flush call
     */
    VkResult S3DBuffer::flush(VkDeviceSize size, VkDeviceSize offset) {
        VkMappedMemoryRange mappedRange = {};
        mappedRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
        mappedRange.memory = memory;
        mappedRange.offset = mappedOffset + offset;
        mappedRange.size = size;
        return vkFlushMappedMemoryRanges(engineDevice.device(), 1, &mappedRange);
    }

    /**
     * Invalidate a memory range of the buffer to make it visible to the host
     *
     * @note Only required for non-coherent memory
     *
     * @param size (Optional) Size of the memory range to invalidate. Pass VK_WHOLE_SIZE to invalidate
     * the complete buffer range.
     * @param offset (Optional) Byte offset from beginning
     *
     * @return VkResult of the invalidate call
     */
    VkResult S3DBuffer::invalidate(VkDeviceSize size, VkDeviceSize offset) {
        VkMappedMemoryRange mappedRange = {};
        mappedRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
        mappedRange.memory = memory;
        mappedRange.offset = mappedOffset + offset;
        mappedRange.size = size;
        return vkInvalidateMappedMemoryRanges(engineDevice.device(), 1, &mappedRange);
    }

    /**
     * Create a buffer info descriptor
     *
     * @param size (Optional) Size of the memory range of the descriptor
     * @param offset (Optional) Byte offset from beginning
     *
     * @return VkDescriptorBufferInfo of specified offset and range
     */
    VkDescriptorBufferInfo S3DBuffer::descriptorInfo(VkDeviceSize size, VkDeviceSize offset) {
        return VkDescriptorBufferInfo{
            buffer,
            offset,
            size,
        };
    }

    /**
     * Copies "instanceSize" bytes of data to the mapped buffer at an offset of index * alignmentSize
     *
     * @param data Pointer to the data to copy
     * @param index Used in offset calculation
     *
     */
    void S3DBuffer::writeToIndex(const void* data, int index) {
        writeToBuffer(data, instanceSize, index * alignmentSize);
    }

    /**
     *  Flush the memory range at index * alignmentSize of the buffer to make it visible to the device
     *
     * @param index Used in offset calculation
     *
     */
    VkResult S3DBuffer::flushIndex(int index) { return flush(alignmentSize, index * alignmentSize); }

    /**
     * Create a buffer info descriptor
     *
     * @param index Specifies the region given by index * alignmentSize
     *
     * @return VkDescriptorBufferInfo for instance at index
     */
    VkDescriptorBufferInfo S3DBuffer::descriptorInfoForIndex(int index) {
        return descriptorInfo(alignmentSize, index * alignmentSize);
    }

    /**
     * Invalidate a memory range of the buffer to make it visible to the host
     *
     * @note Only required for non-coherent memory
     *
     * @param index Specifies the region to invalidate: index * alignmentSize
     *
     * @return VkResult of the invalidate call
     */
    VkResult S3DBuffer::invalidateIndex(int index) {
        return invalidate(alignmentSize, index * alignmentSize);
    }

} 
