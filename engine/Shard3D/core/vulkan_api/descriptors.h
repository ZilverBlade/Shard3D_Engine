#pragma once
#include "device.h"
#include "../rendering/swap_chain.h"
#include "../../systems/handlers/project_system.h"

namespace Shard3D {
	class S3DDescriptorSetLayout {
	public:
		class Builder {
		public:
			Builder(S3DDevice& engineDevice) : engineDevice{ engineDevice } {}

			Builder& addBinding(
				uint32_t binding,
				VkDescriptorType descriptorType,
				VkShaderStageFlags stageFlags,
				uint32_t count = 1);
			uPtr<S3DDescriptorSetLayout> build() const;
			S3DDescriptorSetLayout* build_r() const;
		private:
			S3DDevice& engineDevice;
			std::unordered_map<uint32_t, VkDescriptorSetLayoutBinding> bindings{};
		};

		S3DDescriptorSetLayout(
			S3DDevice& engineDevice, std::unordered_map<uint32_t, VkDescriptorSetLayoutBinding> bindings);
		~S3DDescriptorSetLayout();
		DELETE_COPY(S3DDescriptorSetLayout)

		VkDescriptorSetLayout getDescriptorSetLayout() const { return descriptorSetLayout; }

	private:
		S3DDevice& engineDevice;
		VkDescriptorSetLayout descriptorSetLayout;
		std::unordered_map<uint32_t, VkDescriptorSetLayoutBinding> bindings;

		friend class S3DDescriptorWriter;
	};

	class S3DDescriptorPool {
	public:
		class Builder {
		public:
			Builder(S3DDevice& engineDevice) : engineDevice{ engineDevice } {}

			Builder& addPoolSize(VkDescriptorType descriptorType, uint32_t count);
			Builder& setPoolFlags(VkDescriptorPoolCreateFlags flags);
			Builder& setMaxSets(uint32_t count);
			uPtr<S3DDescriptorPool> build() const;
			S3DDescriptorPool* build_r() const;
		private:
			S3DDevice& engineDevice;
			std::vector<VkDescriptorPoolSize> poolSizes{};
			uint32_t maxSets = 1000;
			VkDescriptorPoolCreateFlags poolFlags = 0;
		};

		S3DDescriptorPool(
			S3DDevice& engineDevice,
			uint32_t maxSets,
			VkDescriptorPoolCreateFlags poolFlags,
			const std::vector<VkDescriptorPoolSize>& poolSizes);
		~S3DDescriptorPool();
		DELETE_COPY(S3DDescriptorPool)

		bool allocateDescriptor(
			const VkDescriptorSetLayout descriptorSetLayout, VkDescriptorSet& descriptor) const;

		void freeDescriptors(const std::vector<VkDescriptorSet>& descriptors) const;

		void resetPool();

	private:
		S3DDevice& engineDevice;
		VkDescriptorPool descriptorPool;

		friend class S3DDescriptorWriter;
	};

	class S3DDescriptorWriter {
	public:
		S3DDescriptorWriter(S3DDescriptorSetLayout& setLayout, S3DDescriptorPool& pool);

		S3DDescriptorWriter& writeBuffer(uint32_t binding, VkDescriptorBufferInfo* bufferInfo);
		S3DDescriptorWriter& writeImage(uint32_t binding, VkDescriptorImageInfo* imageInfo);
		S3DDescriptorWriter& write(uint32_t binding, void* pNext);

		bool build(VkDescriptorSet& set);
		void overwrite(VkDescriptorSet& set);

	private:
		S3DDescriptorSetLayout& setLayout;
		S3DDescriptorPool& pool;
		std::vector<VkWriteDescriptorSet> writes;
	};
} 