#pragma once
#include "../../s3dstd.h"
#include "../../core.h"

namespace Shard3D {
	class S3DPipelineLayout {
	public:
		S3DPipelineLayout(S3DDevice& device, const std::vector<VkPushConstantRange>& pushConstantRanges = std::vector<VkPushConstantRange>(), const std::vector<VkDescriptorSetLayout>& descriptorSetLayouts = std::vector<VkDescriptorSetLayout>());
		~S3DPipelineLayout();
		DELETE_COPY(S3DPipelineLayout);

		inline VkPipelineLayout getPipelineLayout() { return pipelineLayout; }
	private:
		VkPipelineLayout pipelineLayout{};
		S3DDevice& engineDevice;
	};
}