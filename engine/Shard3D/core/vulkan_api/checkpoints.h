#pragma once
#include <volk.h>

#define MAX_CHARACTER_COUNT_CHECKPOINT_DEBUG_NAME 64Ui64
namespace Shard3D {
	enum class S3DCheckpointType {
		GENERIC,
		BEGIN_RENDER_PASS,
		END_RENDER_PASS,
		DRAW,
		SYNC_BARRIER,
		COMPUTE_DISPATCH,
		IMAGE_COPY,
		BUFFER_COPY
	};

	// debug with checkpoints (only available on nvidia devices)
	class S3DCheckpointData {
	public:
		S3DCheckpointData(S3DCheckpointType type, const char* name);
		S3DCheckpointData(const S3DCheckpointData&) = default;

		void mark(VkCommandBuffer commandBuffer);

		void print(VkPipelineStageFlagBits stage);
	private:
		S3DCheckpointType type__debug;
		char name__debug[MAX_CHARACTER_COUNT_CHECKPOINT_DEBUG_NAME];
	};
}