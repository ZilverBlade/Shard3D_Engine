#pragma once

#include "device.h"
#include <map>
#define BINDLESS_SSBO_BINDING 0
#define BINDLESS_SAMPLER_BINDING 1
#define BINDLESS_UNALLOCATED_INDEX 0xffffffffu

namespace Shard3D {
	struct DescriptorBindingInfo {
		VkDescriptorType type;
		uint32_t binding;
	};
typedef uint32_t ShaderResourceIndex;
typedef uint64_t ShaderBufferID;
	class BindlessDescriptorSetResourcePool {
	public:
		class Builder {
		public:
			Builder(S3DDevice& engineDevice) : engineDevice{ engineDevice } {}

			Builder& addDescriptorType(VkDescriptorType descriptorType, uint32_t binding);
			Builder& setPoolFlags(VkDescriptorPoolCreateFlags flags);
			Builder& setShaderStages(VkShaderStageFlags allowedStages);
			Builder& setMaximumSize(uint32_t size);
			uPtr<BindlessDescriptorSetResourcePool> build() const;

		private:
			S3DDevice& engineDevice;
			std::vector<VkDescriptorPoolSize> poolTypes{};
			std::vector<DescriptorBindingInfo> poolBindings{};
			VkDescriptorPoolCreateFlags poolFlags = 0;
			VkShaderStageFlags stageFlags = 0;
			uint32_t maxElements = 32;
		};

		BindlessDescriptorSetResourcePool(
			S3DDevice& engineDevice,
			VkDescriptorPoolCreateFlags poolFlags,
			const std::vector<VkDescriptorPoolSize>& poolSizes,
			const std::vector<DescriptorBindingInfo>& poolBindings,
			uint32_t maxElements,
			VkShaderStageFlags allowedStages
		);
		~BindlessDescriptorSetResourcePool();

		DELETE_COPY(BindlessDescriptorSetResourcePool);

		void resetPool();

		inline VkDescriptorSet getDescriptor() const { return indexedDescriptor; }
		inline VkDescriptorSetLayout getLayout() const { return indexedSetLayout; }
		ShaderResourceIndex allocateIndex(uint32_t binding);
		bool freeIndex(uint32_t binding, ShaderResourceIndex index);

		bool isAllocated(uint32_t binding, ShaderResourceIndex index);
		auto& getAllocated() { return allocatedIndices; }
	private:
		VkSampler defaultSampler;
		S3DDevice& engineDevice;
		VkDescriptorPool descriptorPool{};
		VkDescriptorSet indexedDescriptor{};
		VkDescriptorSetLayout indexedSetLayout{};
		std::map<uint32_t, VkDescriptorType> types{};
		VkDescriptorSetLayoutBindingFlagsCreateInfo bindingFlags{};
		VkDescriptorSetVariableDescriptorCountAllocateInfo setCounts{};
		std::map<uint32_t, std::map<ShaderResourceIndex, bool>> allocatedIndices;
		uint32_t maxSize;
		friend class BindlessDescriptorWriter;
	};
	class BindlessDescriptorWriter {
	public:
		BindlessDescriptorWriter(BindlessDescriptorSetResourcePool& resourcePool);
		
		BindlessDescriptorWriter& writeBuffer(uint32_t binding, ShaderResourceIndex index_element, VkDescriptorBufferInfo* bufferInfo);
		BindlessDescriptorWriter& writeImage(uint32_t binding, ShaderResourceIndex index_element, VkDescriptorImageInfo* imageInfo);
		BindlessDescriptorWriter& writeEmpty(uint32_t binding, ShaderResourceIndex index_element);

		void build();
	private:
		BindlessDescriptorSetResourcePool& resourcePool;
		std::vector<VkWriteDescriptorSet> writes;
	};

using SmartDescriptorSet = BindlessDescriptorSetResourcePool;
using SmartDescriptorWriter = BindlessDescriptorWriter;
}