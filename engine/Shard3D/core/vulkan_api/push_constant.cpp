#include "push_constant.h"

namespace Shard3D {
	S3DPushConstant::S3DPushConstant(size_t size, VkShaderStageFlags stages, uint32_t offset) {
		SHARD3D_ASSERT(size <= 128, "Push Constant has a maximum capacity of 128 bytes");
		pushConstantRange.stageFlags = stages;
		pushConstantRange.offset = offset;
		pushConstantRange.size = static_cast<uint32_t>(size);
	}
	S3DPushConstant::~S3DPushConstant()
	{
	}
	void S3DPushConstant::push(VkCommandBuffer commandBuffer, VkPipelineLayout pipelineLayout, const VoidRef<128>& data) {
		vkCmdPushConstants(
			commandBuffer,
			pipelineLayout,
			pushConstantRange.stageFlags,
			pushConstantRange.offset,
			pushConstantRange.size,
			data.getDataPointer()
		);
	}
}