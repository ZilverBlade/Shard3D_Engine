#include "../../s3dpch.h"
#include "command_buffer.h"

namespace Shard3D {
	S3DCommandBuffer::S3DCommandBuffer(S3DDevice& device) : commandPool(device.getCommandPool()), engineDevice(device) {
		VkCommandBufferAllocateInfo allocInfo{};
		allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocInfo.commandPool = commandPool;
		allocInfo.commandBufferCount = 1;
		allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;

		VK_ASSERT(vkAllocateCommandBuffers(engineDevice.device(), &allocInfo, &commandBuffer), "failed to allocate command buffers!");
	}
	S3DCommandBuffer::~S3DCommandBuffer() {
		vkFreeCommandBuffers(
			engineDevice.device(),
			commandPool,
			1,
			&commandBuffer
		);
	}
	void S3DCommandBuffer::begin(VkCommandBufferUsageFlags flags) {
		VkCommandBufferBeginInfo beginInfo{};
		//beginInfo.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT; // uengineful for renderpass only commands
		beginInfo.flags = flags;
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		
		VK_ASSERT(vkBeginCommandBuffer(commandBuffer, &beginInfo), "failed to begin recording command buffer!");
	}
	void S3DCommandBuffer::end() {
		VK_ASSERT(vkEndCommandBuffer(commandBuffer),"failed to record command buffer!");
	}
	void S3DCommandBuffer::reset(VkCommandBufferResetFlags flags) {
		VK_ASSERT(vkResetCommandBuffer(commandBuffer, flags),"failed to reset command buffer!");
	}
}