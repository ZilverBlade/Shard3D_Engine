#pragma once
#include "../../core.h"
namespace Shard3D {
	class S3DDestructionObject {
	public:
		S3DDestructionObject(S3DDevice& device, VkFence fence) : device(&device), fence(fence) {
		}
		S3DDestructionObject(const S3DDestructionObject&) = default;

		const VkFence getFence() const {
			return fence;
		}
		bool isFree() const {
			if (!fence) {
				vkDeviceWaitIdle(device->device());
				return true; // no fence = no sync?
			}
			VkResult status = vkGetFenceStatus(device->device(), fence);
			if (status == VK_SUCCESS) {
				return true;
			}
			return false;
		}
	private:
		VkFence fence = nullptr;
		S3DDevice* device = nullptr; // store device as pointer to allow copy constructors
	};
}