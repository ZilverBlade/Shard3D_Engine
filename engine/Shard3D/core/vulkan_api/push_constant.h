#pragma once

#include "../../s3dstd.h"
#include "../../core.h"

namespace Shard3D {
	class S3DPushConstant {
	public:
		S3DPushConstant() = default;
		S3DPushConstant(size_t size, VkShaderStageFlags stages, uint32_t offset = 0);
		~S3DPushConstant();
		void push(VkCommandBuffer commandBuffer, VkPipelineLayout pipelineLayout, const VoidRef<128>& data);
		inline VkPushConstantRange getRange() { return pushConstantRange; }
	private:
		VkPushConstantRange pushConstantRange{};
	};
}