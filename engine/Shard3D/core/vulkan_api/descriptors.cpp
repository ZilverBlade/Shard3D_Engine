#include "../../s3dpch.h"
#include "descriptors.h"

namespace Shard3D {

    // *************** Descriptor Set Layout Builder *********************

    S3DDescriptorSetLayout::Builder& S3DDescriptorSetLayout::Builder::addBinding(
        uint32_t binding,
        VkDescriptorType descriptorType,
        VkShaderStageFlags stageFlags,
        uint32_t count) {
        SHARD3D_ASSERT(bindings.count(binding) == 0 && "Binding already in use");
        VkDescriptorSetLayoutBinding layoutBinding{};
        layoutBinding.binding = binding;
        layoutBinding.descriptorType = descriptorType;
        layoutBinding.descriptorCount = count;
        layoutBinding.stageFlags = stageFlags;
        bindings[binding] = layoutBinding;
        return *this;
    }

    uPtr<S3DDescriptorSetLayout> S3DDescriptorSetLayout::Builder::build() const {
        return make_uPtr<S3DDescriptorSetLayout>(engineDevice, bindings);
    }
    S3DDescriptorSetLayout* S3DDescriptorSetLayout::Builder::build_r() const {
        return new S3DDescriptorSetLayout(engineDevice, bindings);
    }

    // *************** Descriptor Set Layout *********************

    S3DDescriptorSetLayout::S3DDescriptorSetLayout(
        S3DDevice& engineDevice, std::unordered_map<uint32_t, VkDescriptorSetLayoutBinding> bindings)
        : engineDevice{ engineDevice }, bindings{ bindings } {
        std::vector<VkDescriptorSetLayoutBinding> setLayoutBindings{};
        for (auto kv : bindings) {
            setLayoutBindings.push_back(kv.second);
        }

        VkDescriptorSetLayoutCreateInfo descriptorSetLayoutInfo{};
        descriptorSetLayoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        descriptorSetLayoutInfo.bindingCount = static_cast<uint32_t>(setLayoutBindings.size());
        descriptorSetLayoutInfo.pBindings = setLayoutBindings.data();
        
        VK_ASSERT(vkCreateDescriptorSetLayout(
            engineDevice.device(),
            &descriptorSetLayoutInfo,
            nullptr,
            &descriptorSetLayout), "failed to create descriptor set layout!");
    }

    S3DDescriptorSetLayout::~S3DDescriptorSetLayout() {
        vkDestroyDescriptorSetLayout(engineDevice.device(), descriptorSetLayout, nullptr);
    }

    // *************** Descriptor Pool Builder *********************

    S3DDescriptorPool::Builder& S3DDescriptorPool::Builder::addPoolSize(
        VkDescriptorType descriptorType, uint32_t count) {
        poolSizes.push_back({ descriptorType, count });
        return *this;
    }

    S3DDescriptorPool::Builder& S3DDescriptorPool::Builder::setPoolFlags(
        VkDescriptorPoolCreateFlags flags) {
        poolFlags = flags;
        return *this;
    }
    S3DDescriptorPool::Builder& S3DDescriptorPool::Builder::setMaxSets(uint32_t count) {
        maxSets = count;
        return *this;
    }

    uPtr<S3DDescriptorPool> S3DDescriptorPool::Builder::build() const {
        return make_uPtr<S3DDescriptorPool>(engineDevice, maxSets, poolFlags, poolSizes);
    }

    S3DDescriptorPool* S3DDescriptorPool::Builder::build_r() const {
        return new S3DDescriptorPool(engineDevice, maxSets, poolFlags, poolSizes);
    }

    // *************** Descriptor Pool *********************

    S3DDescriptorPool::S3DDescriptorPool(
        S3DDevice& engineDevice,
        uint32_t maxSets,
        VkDescriptorPoolCreateFlags poolFlags,
        const std::vector<VkDescriptorPoolSize>& poolSizes)
        : engineDevice{ engineDevice } {
        VkDescriptorPoolCreateInfo descriptorPoolInfo{};
        descriptorPoolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        descriptorPoolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
        descriptorPoolInfo.pPoolSizes = poolSizes.data();
        descriptorPoolInfo.maxSets = maxSets;
        descriptorPoolInfo.flags = poolFlags;
        
        VK_ASSERT(vkCreateDescriptorPool(engineDevice.device(), &descriptorPoolInfo, nullptr, &descriptorPool), "failed to create descriptor pool!");
    }

    S3DDescriptorPool::~S3DDescriptorPool() {
        vkDestroyDescriptorPool(engineDevice.device(), descriptorPool, nullptr);
    }

    bool S3DDescriptorPool::allocateDescriptor(
        const VkDescriptorSetLayout descriptorSetLayout, VkDescriptorSet& descriptor) const {
        VkDescriptorSetAllocateInfo allocInfo{};
        allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        allocInfo.descriptorPool = descriptorPool;
        allocInfo.pSetLayouts = &descriptorSetLayout;
        allocInfo.descriptorSetCount = 1;

        // Might want to create a "DescriptorPoolManager" class that handles this case, and builds
        // a new pool whenever an old pool fills up. But this is beyond our current scope
        if (vkAllocateDescriptorSets(engineDevice.device(), &allocInfo, &descriptor) != VK_SUCCESS) {
            SHARD3D_ERROR("Descriptor pool overfilled! Allocation failed!");
            return false;
        }
        return true;
    }

    void S3DDescriptorPool::freeDescriptors(const std::vector<VkDescriptorSet>& descriptors) const {
        vkFreeDescriptorSets(
            engineDevice.device(),
            descriptorPool,
            static_cast<uint32_t>(descriptors.size()),
            descriptors.data());
    }

    void S3DDescriptorPool::resetPool() {
        vkResetDescriptorPool(engineDevice.device(), descriptorPool, 0);
    }

    // *************** Descriptor Writer *********************

    S3DDescriptorWriter::S3DDescriptorWriter(S3DDescriptorSetLayout& setLayout, S3DDescriptorPool& pool)
        : setLayout{ setLayout }, pool{ pool } {}

    S3DDescriptorWriter& S3DDescriptorWriter::writeBuffer(
        uint32_t binding, VkDescriptorBufferInfo* bufferInfo) {
        SHARD3D_ASSERT(setLayout.bindings.count(binding) == 1 && "Layout does not contain specified binding");

        auto& bindingDescription = setLayout.bindings[binding];

        VkWriteDescriptorSet write{};
        write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        write.descriptorType = bindingDescription.descriptorType;
        write.dstBinding = binding;
        write.pBufferInfo = bufferInfo;
        write.descriptorCount = bindingDescription.descriptorCount;
        
        writes.push_back(write);
        return *this;
    }

    S3DDescriptorWriter& S3DDescriptorWriter::writeImage(
        uint32_t binding, VkDescriptorImageInfo* imageInfo) {
        SHARD3D_ASSERT(setLayout.bindings.count(binding) == 1 && "Layout does not contain specified binding");

        auto& bindingDescription = setLayout.bindings[binding];

        VkWriteDescriptorSet write{};
        write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        write.descriptorType = bindingDescription.descriptorType;
        write.dstBinding = binding;
        write.pImageInfo = imageInfo;
        write.descriptorCount = bindingDescription.descriptorCount;

        writes.push_back(write);
        return *this;
    }

    S3DDescriptorWriter& S3DDescriptorWriter::write(
        uint32_t binding, void* pNext) {
        SHARD3D_ASSERT(setLayout.bindings.count(binding) == 1 && "Layout does not contain specified binding");

        auto& bindingDescription = setLayout.bindings[binding];

        VkWriteDescriptorSet write{};
        write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        write.descriptorType = bindingDescription.descriptorType;
        write.dstBinding = binding;
        write.pNext = pNext;
        write.descriptorCount = bindingDescription.descriptorCount;

        writes.push_back(write);
        return *this;
    }

    bool S3DDescriptorWriter::build(VkDescriptorSet& set) {
        if (!set) {
            bool success = pool.allocateDescriptor(setLayout.getDescriptorSetLayout(), set);
            if (!success) {
                return false;
            }
        }
        overwrite(set);
        return true;
    }

    void S3DDescriptorWriter::overwrite(VkDescriptorSet& set) {
        for (auto& write : writes) {
            write.dstSet = set;
        }
        vkUpdateDescriptorSets(pool.engineDevice.device(), writes.size(), writes.data(), 0, nullptr);
    }

}  // namespace Shard3D
