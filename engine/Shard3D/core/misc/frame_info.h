#pragma once
#include "../rendering/camera.h"

#include "../../core.h"
#include "../vulkan_api/descriptors.h"
#include "../vulkan_api/bindless.h"
#include "../asset/texture.h"
#include "../../utils/telemetry.h"
#include "../ecs/level.h"

namespace Shard3D {
	class ResourceSystem;
	class TerrainList;
	class RenderList;
	class DecalBatchList;
	class HUDRenderSystem;
	struct HDRPPFX {
		float hdrExposure = 1.0f;
		float hdrContrast = 1.0f;
		float hdrSaturation = 1.0f;
		float hdrGain = 0.0f;
	};
	struct BloomPPFX {
		float bloomThreshold = 1.0f;
		float bloomIntensity = 1.0f;
	};
	struct SSAOPPFX {
		float ssaoRadius = 0.3f;
		int ssaoSamples = 32;
		float ssaoContrast = 1.7f;
		float ssaoPower = 1.0f;
		float ssaoBias = 0.005f;
	};
	struct FXAAPPFX {
		float fxaaMaxSpan = 4.0f;
		float fxaaMinReduct = 1.f / 128.f;
	};
	struct GlobalUbo {
		//alignment rules https://cdn.discordapp.com/attachments/763044823342776340/974725640274210926/unknown.png

		glm::mat4 projection{ 1.f };
		glm::mat4 inverseProjection{ 1.f };
		glm::mat4 view{ 1.f };
		glm::mat4 inverseView{ 1.f };
		glm::mat4 projectionView{ 1.f };
		glm::mat4 projectionPrevFrame{ 1.f };
		glm::mat4 viewPrevFrame{ 1.f };
		glm::mat4 projectionViewPrevFrame{ 1.f };

		std::array<glm::vec4, 6> frustumPlanes;
		glm::vec4 globalClipPlane = { 0.0f, 0.0f, 0.0f, 0.0f }; // must be (0.0, 0.0, 0.0, 0.0) to disable
		glm::vec2 screenSize = { 1280, 720 };
		float nearPlane = 0.0;
		float farPlane = 0.0;
		int velocityBufferTileExtent;
	};
	struct alignas(16)Skylight {
		glm::vec4 reflectionTint; // skybox may provide custom tint
		int dynamicSky;
		uint32_t reflection_id;
		uint32_t irradiance_id;
	};
	struct alignas(16)Skybox {
		glm::vec4 tint;
		ShaderResourceIndex skybox_id;
	};
	struct alignas(16)ShadowMapInfo {
		glm::mat4 lightSpaceMatrix;
		int shadowMapConvention; // 1 = conventional, 2 = parab, 3 = dual parab, 4 = cube, 5 = pssm2, 6 = pssm3, 7 = pssm4
		ShaderResourceIndex shadowMap_id;
		ShaderResourceIndex shadowMapTranslucent_ID;
		ShaderResourceIndex shadowMapTranslucentColor_ID;
		ShaderResourceIndex shadowMapTranslucentReveal_ID;
		float bias; // either variance probability bias or PCF shadow bias multiplier
		float sharpness; // 0 - 0.99 value for shadow sharpness filter
		// PCF exclusive stuff
		float poissonRadius;
		uint32_t poissonSamples;
	};
	struct alignas(16)PointLight {
		glm::vec3 position;
		ShaderResourceIndex shadowMapInfoIndex;
		glm::vec3 color;
		float radius;
	};
	struct alignas(16)PointLightComplex {
		glm::vec4 positionPipeEnd0;
		glm::vec4 positionPipeEnd1;
		glm::vec4 color;
		float radius;
		float sourceRadius;
		VkBool32 isPipe;
		ShaderResourceIndex shadowMapInfoIndex;
	};
	struct alignas(16)SpotLight {
		glm::vec4 position;
		glm::vec4 color;
		glm::vec3 direction;
		ShaderResourceIndex shadowMapInfoIndex;
		glm::vec2 angle; //outer, inner
		ShaderResourceIndex cookie_ID;
		float radius;
	};
	struct alignas(16)SpotLightComplex {
		glm::vec4 position;
		glm::vec4 color;
		glm::vec3 direction;
		ShaderResourceIndex shadowMapInfoIndex;
		glm::vec2 angle; //outer, inner
		uint32_t cookie_ID;
		float sourceRadius;
		float radius;
	};
	struct alignas(16)DirectionalLight {
		glm::vec4 position{};
		glm::vec4 color{};
		glm::vec3 direction{};
		ShaderResourceIndex shadowMapInfoIndex;
	};

	struct alignas(16)BoxReflectionCube {
		glm::mat4 invtransform;
		glm::vec4 color;
		alignas(16)glm::vec3 boundSize;
		float intersectionDist;
		ShaderResourceIndex reflection_id;
		ShaderResourceIndex irradiance_id;
	}; 
	struct alignas(16)PlanarReflection {
		glm::vec4 color;
		glm::vec4 cartesianPlane;
		float maxHeight;
		float intersectionDist;
		float distortDuDvFactor;
		float distortionDeltaLimit;
		ShaderResourceIndex reflection_id;
		ShaderResourceIndex irradiance_id;
	};
	struct alignas(16)BoxAmbientOcclusionVolume {
		glm::mat4 invtransform;
		glm::vec3 tint; float maxOcclusion;
		alignas(16)glm::vec3 boundSize;
		float intersectionDist;
	};

	struct LightPropagationVolume {
		glm::vec4 color; // w intensity
		alignas(16)glm::vec3 center;
		alignas(16)glm::vec3 boundsScale;
		float fadeRatio;
		ShaderResourceIndex redvoxel_id;
		ShaderResourceIndex greenvoxel_id;
		ShaderResourceIndex bluevoxel_id;
	};

	struct alignas(16)ExponentialFog {
		float density;
		float falloff;
		float strength;
		float sunScattPow;
		glm::vec4 color;
		int updownbias;
	};

	struct OmniLightUBO { // point and spot lights
		PointLight pointLights[384];
		alignas(16)int numPointLights;
		SpotLight spotLights[384];
		alignas(16)int numSpotLights;
	};
	struct OmniLightComplexUBO { // complex point and spot lights
		PointLightComplex pointLights[256];
		alignas(16)int numPointLights;
		SpotLightComplex spotLights[256];
		alignas(16)int numSpotLights;
	};
	struct ShadowMapInfoUBO {
		ShadowMapInfo shadowMaps[256];
	};

	struct EnvironmentUBO { // atmospheric influence
		DirectionalLight directionalLights[ENGINE_MAX_DIRECTIONAL_LIGHTS];
		alignas(16)int numDirectionalLights;

		Skylight skylights[ENGINE_MAX_DIRECTIONAL_LIGHTS];
		alignas(16)int numSkylights;

		ExponentialFog exponentialFogs[ENGINE_MAX_DIRECTIONAL_LIGHTS];
		alignas(16)int numExponentialFogs;

		Skybox activeSkybox; // you can only have 1 skybox at a time
	};

	struct SotAFXUBO { // state of the art effects
		BoxReflectionCube reflectionCubes[ENGINE_MAX_REFLECTION_CUBES];
		alignas(16)int numReflectionCubes;

		PlanarReflection reflectionPlanes[ENGINE_MAX_REFLECTION_PLANES];
		alignas(16)int numReflectionPlanes;

		BoxAmbientOcclusionVolume aoVolumes[ENGINE_MAX_LIGHTING_VOLUMES];
		alignas(16)int numAOVolumes;
		LightPropagationVolume lightPropagationVolumeCascades[3];
		alignas(16)int numLightPropagationVolumeCascades;
	};

	struct SceneBuffers {
		OmniLightUBO sceneOmniLights;
		OmniLightComplexUBO sceneOmniComplexLights;
		ShadowMapInfoUBO sceneShadowMapInfos;
		EnvironmentUBO sceneEnvironment;
		SotAFXUBO sceneSotAFX;
	};

	struct RenderObjects {
		RenderList* renderList;
		TerrainList* terrainList;
		DecalBatchList* decalList;
	};

	// rendering frame info
	struct FrameInfo { 
		int frameIndex;
		VkCommandBuffer commandBuffer;
		S3DCamera camera;
		Telemetry* telemetry;
		VkDescriptorSet globalDescriptorSet;
		VkDescriptorSet sceneDescriptorSet;
		sPtr<ECS::Level>& level;
		RenderObjects renderObjects;
		ResourceSystem* resourceSystem;
	};

	// scene update frame info
	struct FrameInfo2 {
		int frameIndex;
		float frameTime;
		S3DCamera& camera;
		sPtr<ECS::Level>& level;
		HUDRenderSystem* hudRenderSystem;
		ResourceSystem* resourceSystem;
	};
}