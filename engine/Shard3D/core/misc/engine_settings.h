#pragma once

#include <vector>
#include <string>

namespace Shard3D {
	struct ESET {
		enum class EarlyDepthMode {
			/*
				++ Less CPU load
				-- GPU usage may increase significantly
			*/
			Off = 0,
			/*
				++ Deferred pass will only execute shaders for visible fragments
				-- Higher CPU load (render scene twice)
			*/
			OpaqueOnly = 1,
			/*
				++ Deferred pass will only execute shaders for visible fragments
				++ Discard fragments behind materials with discard masks
				-- Higher CPU load (render scene twice)
				-- Potentially higher GPU load when discard mask usage
			*/
			OpaqueAndDiscardMasks = 2
		};
		enum class DepthBufferFormat {
			/*
				++ Fast
				-- Worse precision
			*/
			StdF32Bit = 0,
			/*
				++ Lower fragment shader usage for reading more depth information
				++ Improved accuracy
				-- Slightly higher vertex shader usage for log operations
			*/
			Log16Bit = 1,
			/*
				++ Reduced vertex shader costs
				-- Increased fragment shader usage for reading more depth information (4 bytes/pixel)
			*/
			ReverseF32Bit = 2,
		};
		enum class TransparentShadows {
			/*
				++ Less render intensive
				-- Less pretty
				(Recommended)
			*/
			DontRender = 0,
			/*
				++ Cheap
				-- Slightly more draw calls
				-- Innacurate shadows for very transparent objects
			*/
			RenderOpaque = 1,
			/*
				++ Significantly Prettier
				-- Slightly more draw calls
				-- More render intensive
				-- Increased pixel (fragment) shader usage
			*/
			RenderTransparentGrayScale = 2,
			/*
				++ Significantly Prettier
				-- Significantly more render intensive
				-- Significantly increased pixel (fragment) shader usage
				-- Significantly higher memory usage
			*/
			RenderTransparentColored = 3
		};
		enum class MaskedShadows {
			/*
				++ Cheap
				-- Innacurate shadows
			*/
			RenderOpaque = 0,
			/*
				++ Significantly Prettier
				-- More render intensive
				-- Increased pixel (fragment) shader usage
				(Recommended)
			*/
			RenderMasked = 1
		};
		enum class ShadowMap {
			/*
				++ Fast to generate
				++ Low VRAM usage
				-- Very slow on the pixel (fragment) shader
				-- Might suffer from shadow acne and peter panning
			*/
			PCF = 0,
			/*
				++ Light on the pixel (fragment) shader
				++ Can easily be baked
				-- Slower to generate
				-- Heavier VRAM usage (especially with higher quality settings)
				-- Might suffer from light leaking/bleeding
				(Recommended)
			*/
			Variance = 1
		};
		enum class SpecularModel {
			/*
				++ Fast
				-- Not supported at the moment
			*/
			Phong = 0,
			/*
				++ Faster
				-- Poor representation of shiny surfaces
			*/
			BlinnPhong = 1,
			/*
				++ Great representation of shiny surfaces
				-- Slower
			*/
			Gaussian = 2
		};
		enum class RendererMode {
			/*
				++ Fast
				++ Lower memory usage
				-- Poor light culling and poor thread efficiency
			*/
			DeferredShading = 0,
			/*
				++ Better light culling (potentially better performance) 
				++ Better thread efficiency
				-- Higher memory usage and bandwidth (additional 8 bytes per pixel for the diffuse and specular buffers)

				Note: avoid high counts of overlapping lights due to lack of matrix multiplication reusage per pixel
			*/
			FullyDeferredLighting = 1,
		};
	};
	
	// Engine Settings
	struct EngineSettings {
		// WINDOW
		struct window_ {
			bool resizable = true;
			int defaultWidth = 1280;
			int defaultHeight = 720;
		} window;

		
		// CPU
		struct cpu_ {
			uint32_t maximumAllowedFrameTimeMilliseconds = 100u;
		} cpu;

		// PHYSICS
		struct physics_ {
			uint32_t updateRate = 120u;
			bool useQuickStep = true;
		} physics;

		// RENDERER
		struct renderer_ {
			bool enableDoubleSidedMaterials = true;
			bool enableDeferredDecals = true;
			bool enableAlphaTest = true;
			bool allowFragmentShadingRateEXT = false;
			bool allowRayTracingUsage = false;
			ESET::EarlyDepthMode earlyZMode = ESET::EarlyDepthMode::Off;
			ESET::DepthBufferFormat depthBufferFormat = ESET::DepthBufferFormat::StdF32Bit;
			ESET::RendererMode rendererMode = ESET::RendererMode::DeferredShading;
			bool infiniteFarZ = false;

		} renderer;

		// SHADING/RENDERING
		struct shading_ {
			bool enableMetallicParam = false;
			bool enableFresnel = true;
			bool diffuseAffectsPartialReflections = true;
			bool specularAffectsReflectionIntensity = false;

			ESET::SpecularModel specularModel = ESET::SpecularModel::BlinnPhong;

			ESET::TransparentShadows renderTransparentShadows = ESET::TransparentShadows::DontRender;
			ESET::MaskedShadows renderMaskedShadows = ESET::MaskedShadows::RenderMasked;
			ESET::ShadowMap directionalShadowMapType = ESET::ShadowMap::PCF;
			ESET::ShadowMap spotShadowMapType = ESET::ShadowMap::Variance;
			ESET::ShadowMap pointShadowMapType = ESET::ShadowMap::Variance;
		} shading; // shading and rendering

		// MEMORY
		struct memory_ {
			bool enableVertexTangents = true;
			bool use16bitReflections = false; // TODO: see if viable
			uint32_t arenaAllocatorDefaultVertexSize = 32767;
			float arenaAllocatorCapacityGrowMultiplier = 1.5f;
			float arenaAllocatorCapacityShrinkThreshold = 0.5f;
			uint32_t memoryAllocatorSizeBytes = 33554432; // 32mb default
			uint32_t memoryAllocatorBlockSizeBytes = 256; // 256 bytes default
		} memory;
		// POSTFX
		struct postfx_ {
			bool enableBloom = true;
			bool fastBloom = false;
			bool enableSSAO = true;
			bool enableMotionBlur = true;
		} postfx;
		std::vector<uint32_t> allowedMaterialPermutations;
	};

	// Script Settings
	struct ScriptConfig {
		
	};

	struct ShadingSettings {
		bool enableSpecularMapping = true;
		bool enableNormalMapping = true;
		bool enableShadows = true;
		bool enableTransparency = true;
		std::vector<std::string> localShaderDefines;
	};

}