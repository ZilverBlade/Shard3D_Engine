#pragma once

#include "../../utils/simple_ini.h"
#include "../rendering/window.h"
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
namespace Shard3D {
	enum GraphicsEnum {
		Null = 0,
		Cinematic = 1,
		Ultra = 2,
		High = 3,
		Medium = 4,
		Low = 5,
		Poor = 6
	};

	enum WindowType {
		Windowed = 0,
		Borderless = 1,
		Fullscreen = 2
	};

	struct RuntimeInfo {
		RuntimeInfo() = default;
		float aspectRatio = 1.f;
		glm::vec4 ambientLightColor = { 1.0f, 1.0f, 1.0f, 0.41f };;
		glm::vec4 localScreen{1280,720, 0, 0}; // posX, posY, width, height, 

		glm::uvec3 PostProcessingInvocationIDCounts{ 1280 / 16, 720 / 16, 1};
	};

	struct Settings {
		Settings() = default;
		//	[WINDOW]
//Window width of the main window
			//@return (live usage)
		int WindowWidth = 1920;
		//	[WINDOW]
//Window height of the main window
			//@return (live usage)
		int	WindowHeight = 1000;
		//	[WINDOW]
//Window type of the main window. 
//@param Can be: Windowed, Borderless, Fullscreen.
//@return (live usage)
		WindowType WindowType = Borderless;

		//	[DISPLAY]
//Whether the FIFO present mode is used or not. When off, the swapchain will figure out to use Mailbox or Immediate
			//@return (requires swapchain recreation)
		bool VSync = true;

		//	[TEXTURES]
//Level of anisotropic filtering used by the texture system.
			//@return (requires restart)
		int maxAnisotropy = 16;

		//	[GRAPHICS]
// Amount of frames per second to limit the renderer.
// Requires V-Sync to be turned off
// 			   @param 0: uncapped
// 			   @param any other positive integer: max framerate
			//@return (live usage)
		int FramerateCap = 0;

		//	[GRAPHICS]
//Time to wait between frames, used by the renderer to reduce computation
// 			   @param 0: uncapped
// 			   @param any other positive float: time between frames
			//@return (live usage)
		const float FramerateCapInterval = 0.f;
		//	[GRAPHICS]
//Samples used by the renderer. Keep at 1, as this is deprecated and should not be used. 
			//@return (requires restart)
		VkSampleCountFlagBits MSAASamples = VK_SAMPLE_COUNT_1_BIT;
		//	[GRAPHICS]
/*LOD Coef. Quality will range from graphics settings :
			@param Poor: Always lowest LODs
			@param Low: Medium LODs load in at (placeholder) distance.
			@param Medium: Medium LODs load in at (placeholder) distance, High LODs load in at (placeholder) distance.
			@param High: Medium LODs load in at (placeholder) distance, High LODs load in at (placeholder) distance.
			@param Cinematic Always highest LODs
			@return (live usage)
						*/
		GraphicsEnum LODCoef = Medium;
		//	[GRAPHICS]
/*Shadowmap quality. Quality will range from graphics settings :
			@param Poor: No shadows
			@param  Low: (placeholder)
			@param  Medium: (placeholder)
			@param  High: (placeholder)
			@param  Ultra: (placeholder) + (placeholder)
			@param  Cinematic: (placeholder) + (placeholder) (should not be used for real-time rendering)
			@return (live usage)
			*/
		GraphicsEnum ShadowQuality = Medium;
		//	[GRAPHICS]
//placeholder
			//@param Can be: Low, Medium, High
			//@return (live usage)
		GraphicsEnum ReflectionQuality = Medium;
	};

	struct EditorPreviewSettings {
		EditorPreviewSettings() = default;

		bool V_EDITOR_BILLBOARDS = true;
		bool V_HIDDEN_MESHES = true;
		bool V_HIDDEN_BILLBOARDS = true;
		bool V_GRID = true;
		bool V_GUI = true;
		bool V_CULLVOLUMES = false;
		bool V_PHYSICS = false;
		bool ONLY_GAME;
	};

	class GraphicsSettings2 {
	protected:
		friend class S3DRenderer;
	private:
		static inline Settings graphics{};
		static inline RuntimeInfo r_info{};
		static inline CSimpleIniA ini;
		static inline S3DWindow* engineWindow;
	public:
		static inline EditorPreviewSettings editorPreview{};

		// you may initialise this twice, as the engine window is required later on. First initialisation can be with nullptr as argument.
		//static void init(S3DWindow* window);
		//static void read();
		static RuntimeInfo& getRuntimeInfo() { return r_info; }
	// as this getter passes the value by reference, you may modify its contents
		//static Settings& get();
		
		//static void set(Settings sets);
		 
		static void setWindowMode(WindowType winType);
		static void toggleFullscreen();
	};

	struct GraphicsSettings {

	// Textures
		struct Texture {
			bool mipMapping = true;
			float anisotropy = 8.0f; // max 16
		} texture;

	// Reflections
		struct Reflections {
			float renderQuality = 1.0f;
		} reflections;

	// Shadows
		struct Shadows {
			bool directional = true;
			bool spot = true;
			bool point = true;
			float directionalResolutionMultiplier = 1.0f;
			float spotResolutionMultiplier = 1.0f;
			float pointResolutionMultiplier = 1.0f;

			int varianceDirectionalBlurQuality = 2;
			int varianceSpotBlurQuality = 1;
			int variancePointBlurQuality = 0;
			int varianceDirectionalMultiSamples = 8;
			int varianceSpotMultiSamples = 4;
			int variancePointMultiSamples = 1;
			
			struct Volumetric { // TODO: implement
				bool castSpotShadow = true;
				bool castPointShadow = true;

				uint32_t rayStepsDirectional = 8;
				uint32_t rayStepsSpot =  8;
				uint32_t rayStepsPoint = 8;
			} volumetric;
		} shadows;

	// Display
		struct Display {
			float gamma = 2.2f;
		} display;

	// Renderer
		struct Renderer {
			bool verticalSync = false;
			int frameRateLimit = 240;
			uint32_t framesInFlight = 3;
			bool msaa = false;
			uint32_t msaaSamples = 2; // 2x or 4x recommended with FXAA, 8x is extremely expensive
			bool parallaxMapping = false; // enables parallax mapping on terrain
		} renderer;

	// Post Processing
		struct PostProcessing {
			struct AntiAliasing {
				bool fxaa = true;
				float fxaaSoftness = 8.0f;
			} aa;
			struct ScreenSpaceAmbientOcclusion {
				float quality = 0.50f; // resolution multiplier
				int kernelQuality = 2; // 0, 1, 2
			} ssao;
			struct Bloom {
				int bloomQuality = 1; // 0, 1, 2, 3
			} bloom;
			struct MotionBlur {
				int samples = 15;
			} motionBlur;
		} postfx;
	};

}