#include "../../s3dpch.h" 
#include "graphics_settings.h"
#include "../../core.h"
#include <fstream>
namespace Shard3D {

	void GraphicsSettings2::setWindowMode(WindowType winType) {
		engineWindow->setWindowMode((S3DWindow::WindowType)(int)winType);
	}
	void GraphicsSettings2::toggleFullscreen()
	{/*
		if (borderlessFullscreen == false && windowType == 2) {
			setWindowMode(Windowed);
		}
		else {
			CSimpleIniA ini;
			ini.SetUnicode();
			ini.LoadFile(GAME_SETTINGS_PATH);

			// backup window position and window size
			glfwGetWindowPos(window, &windowPosX, &windowPosY);
			glfwGetWindowSize(window, &windowWidth, &windowHeight);
			ini.SetLongValue("WINDOW", "WIDTH", (long)windowWidth);
			ini.SetLongValue("WINDOW", "HEIGHT", (long)windowHeight);
			ini.SaveFile(GAME_SETTINGS_PATH);

			setWindowMode(Borderless);
		}
		*/
		setWindowMode(Borderless);
	}
}