#pragma once

#include <glm/glm.hpp>
namespace Shard3D {
	struct SphereBounds {
		SphereBounds() = default;
		SphereBounds(const SphereBounds&) = default;
		SphereBounds(float radius) : radius(radius) {
		}
		void setAndCheck(glm::vec3 point) {
			if (float len2 = glm::dot(point, point); len2 > radius * radius) {
				radius = sqrt(len2);
			}
		}
		float radius{};
	};
	struct VolumeBounds {
		VolumeBounds() = default;
		VolumeBounds(const VolumeBounds&) = default;
		VolumeBounds(glm::vec3 position, glm::vec3 extent) : maxXYZ(position + extent), minXYZ(position - extent)
		{
			assert(glm::all(glm::lessThan(minXYZ, maxXYZ)));
		}
		void setAndCheck(glm::vec3 point) {
			if (point.x > maxXYZ.x) maxXYZ.x = point.x;
			if (point.y > maxXYZ.y) maxXYZ.y = point.y;
			if (point.z > maxXYZ.z) maxXYZ.z = point.z;

			if (point.x < minXYZ.x) minXYZ.x = point.x;
			if (point.y < minXYZ.y) minXYZ.y = point.y;
			if (point.z < minXYZ.z) minXYZ.z = point.z;
		}
		bool isInVolume(glm::vec3 point) {
			return glm::all(glm::lessThan(point, maxXYZ)) && glm::all(glm::greaterThan(point, minXYZ));
		}
		void transform(const glm::mat4& matrix) {
			glm::vec3 extent = (maxXYZ - minXYZ) * 0.5f;
			glm::vec3 center = (maxXYZ + minXYZ) * 0.5f;
			
			glm::vec3 right = glm::abs(glm::vec3(matrix[0])) * extent.x;
			glm::vec3 up = glm::abs(glm::vec3(matrix[1])) * extent.y;
			glm::vec3 front = glm::abs(glm::vec3(matrix[2])) * extent.z;

			const float i = right.x + up.x + front.x;
			const float j = right.y + up.y + front.y;
			const float k = right.z + up.z + front.z;

			*this = VolumeBounds(matrix * glm::vec4(center, 1.0f), { i,j,k });
		}
		glm::vec3 maxXYZ{};
		glm::vec3 minXYZ{};
	};
	struct VolumePoints {
		VolumePoints() = default;
		VolumePoints(const VolumeBounds& bounds) {
			points[0] = { bounds.minXYZ.x, bounds.minXYZ.y, bounds.minXYZ.z, 1.0f };
			points[1] = { bounds.minXYZ.x, bounds.minXYZ.y, bounds.maxXYZ.z, 1.0f };
			points[2] = { bounds.minXYZ.x, bounds.maxXYZ.y, bounds.minXYZ.z, 1.0f };
			points[3] = { bounds.minXYZ.x, bounds.maxXYZ.y, bounds.maxXYZ.z, 1.0f };
			points[4] = { bounds.maxXYZ.x, bounds.minXYZ.y, bounds.minXYZ.z, 1.0f };
			points[5] = { bounds.maxXYZ.x, bounds.minXYZ.y, bounds.maxXYZ.z, 1.0f };
			points[6] = { bounds.maxXYZ.x, bounds.maxXYZ.y, bounds.minXYZ.z, 1.0f };
			points[7] = { bounds.maxXYZ.x, bounds.maxXYZ.y, bounds.maxXYZ.z, 1.0f };
		}
		void transform(const glm::mat4& matrix) {
			for (int i = 0; i < 8; i++) {
				points[i] = matrix * points[i];
			}
		}
		glm::vec4 points[8]{};
	};
}