#include "../../s3dpch.h"  
#include "renderer.h"

#include "../../core.h"
#include "../misc/graphics_settings.h"

namespace Shard3D {
	S3DRenderer::S3DRenderer(S3DDevice& device) : engineDevice(device), commandPool(device.getCommandPool()) {
		engineQueue = engineDevice.getAvailableQueue(QueueType::Graphics);
	}
	S3DRenderer::~S3DRenderer() {
		engineDevice.freeAvailableQueue(engineQueue);
	}
	void S3DRenderer::submitCommandBuffer(S3DCommandBuffer* commandBuffer) {
		toSubmitCommandBuffers.push_back(commandBuffer->getCommandBuffer());
	}
	void S3DRenderer::submitQueue(VkSubmitInfo submitInfo, VkFence fence) {
		submitInfo.commandBufferCount = toSubmitCommandBuffers.size();
		submitInfo.pCommandBuffers = toSubmitCommandBuffers.data();

		if (VkResult result = vkQueueSubmit(engineQueue->queue, 1, &submitInfo, fence); result != VK_SUCCESS) {
			if (result == VK_ERROR_DEVICE_LOST) {
				if (engineDevice.supportedExtensions.nv_device_diagnostic_checkpoints) {
					uint32_t numCheckpoints;
					std::vector<VkCheckpointDataNV> checkpoints;
					vkGetQueueCheckpointDataNV(engineQueue->queue, &numCheckpoints, nullptr);
					checkpoints.resize(numCheckpoints, { VK_STRUCTURE_TYPE_CHECKPOINT_DATA_NV });
					vkGetQueueCheckpointDataNV(engineQueue->queue, &numCheckpoints, checkpoints.data());
					for (auto& chckp : checkpoints) {
						reinterpret_cast<S3DCheckpointData*>(chckp.pCheckpointMarker)->print(chckp.stage);
					}
				} else {
					SHARD3D_WARN("Device does not support VK_NV_device_diagnostic_checkpoints! No additional crash info is provided");
				}
				SHARD3D_FATAL("Failed to submit draw command buffer! VK_ERROR_DEVICE_LOST (-4)");
			} else {
				SHARD3D_ERROR("Error submitting queue: {0}", string_VkResult(result));
			}
		}
		toSubmitCommandBuffers.clear();
	}
}