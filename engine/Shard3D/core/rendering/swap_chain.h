#pragma once
#include "../vulkan_api/device.h"
#include <chrono>

namespace Shard3D {
	class  S3DSwapChain {
	public:
        S3DSwapChain(S3DDevice& device, S3DWindow& window, VkExtent2D windowExtent, S3DSwapChain* oldSwapChain = nullptr);
        ~S3DSwapChain();

        S3DSwapChain(const S3DSwapChain&) = delete;
        S3DSwapChain& operator=(const S3DSwapChain&) = delete;
        S3DSwapChain() = default;

        VkFramebuffer getFramebuffer(int index) {
            return swapChainFramebuffers[index];
        }
        VkRenderPass getRenderPass() {
            return swapChainRenderPass;
        }
        VkImageView getImageView(int index) {
            return swapChainImageViews[index];
        }
        size_t getImageCount() {
            return swapChainImageViews.size();
        }
        VkFormat getSwapChainImageFormat() {
            return swapChainImageFormat;
        }
        VkExtent2D getSwapChainExtent() {
            return swapChainExtent;
        }
        uint32_t width() {
            return swapChainExtent.width;
        }
        uint32_t height() {
            return swapChainExtent.height;
        }
        uint32_t getImageIndex() {
            return currentFrame;
        }

        float extentAspectRatio() {
            return static_cast<float>(swapChainExtent.width) / static_cast<float>(swapChainExtent.height);
        }

        VkResult acquireNextImage(uint32_t* imageIndex);
        VkSubmitInfo getSubmitInfo(uint32_t* imageIndex);
        VkResult present(VkQueue queue, uint32_t* imageIndex);

        VkFence getFence(uint32_t imageIndex) {
            return inFlightFences[imageIndex];
        }
        VkFence getFenceCurrentFrame() {
            return inFlightFences[currentFrame];
        }

        bool compareSwapFormats(const S3DSwapChain& swapChain) const {
            return swapChain.swapChainImageFormat == swapChainImageFormat;
        }

        void beginRenderPass(VkCommandBuffer commandBuffer) {
            VkRenderPassBeginInfo renderPassBeginInfo{};
            renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            renderPassBeginInfo.renderPass = swapChainRenderPass;
            renderPassBeginInfo.framebuffer = swapChainFramebuffers[currentFrame];
            renderPassBeginInfo.renderArea.extent = swapChainExtent;
            renderPassBeginInfo.clearValueCount = 1;
            VkClearValue clearColor{};
            clearColor.color = { 0.0f, 0.0f, 0.0f, 1.0 };
            renderPassBeginInfo.pClearValues = &clearColor;

            vkCmdBeginRenderPass(commandBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

            VkViewport viewport = {};
            viewport.x = 0.0f;
            viewport.y = 0.0f;
            viewport.width = static_cast<float>(swapChainExtent.width);
            viewport.height = static_cast<float>(swapChainExtent.height);
            viewport.minDepth = 0.0f;
            viewport.maxDepth = 1.0f;
            vkCmdSetViewport(commandBuffer, 0, 1, &viewport);

            VkRect2D scissor = {};
            scissor.extent = swapChainExtent;
            scissor.offset.x = 0;
            scissor.offset.y = 0;
            vkCmdSetScissor(commandBuffer, 0, 1, &scissor);
        }
        void endRenderPass(VkCommandBuffer commandBuffer) {
            vkCmdEndRenderPass(commandBuffer);
        }
    private:
        void init();
        void createSwapChain(const VkSurfaceCapabilitiesKHR& capabilities, VkExtent2D extent, VkSurfaceFormatKHR surfaceFormat, VkPresentModeKHR presentMode);
        void createImageViews();
        void createRenderPass();
        void createFramebuffers();
        void createSyncObjects();
        void setImageCount(const VkSurfaceCapabilitiesKHR& capabilities);

        // Helper functions
        VkSurfaceFormatKHR chooseSwapSurfaceFormat(
            const std::vector<VkSurfaceFormatKHR>&availableFormats);


        VkPresentModeKHR chooseSwapPresentMode(
            const std::vector<VkPresentModeKHR>&availablePresentModes);
        VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR & capabilities);

        VkFormat swapChainImageFormat{};
        VkExtent2D swapChainExtent{};

        std::vector<VkFramebuffer> swapChainFramebuffers{};
        std::vector<VkImageView> swapChainImageViews{};
        VkRenderPass swapChainRenderPass{};

        S3DDevice& engineDevice;
        S3DWindow& engineWindow;

        VkSwapchainKHR swapChain{};
        S3DSwapChain* oldSwapChain = nullptr;

        std::vector<VkSemaphore> imageAvailableSemaphores{};
        std::vector<VkSemaphore> renderFinishedSemaphores{};
        std::vector<VkFence> inFlightFences{};
        std::vector<VkFence> imagesInFlight{};
        uint32_t currentFrame = 0;
        uint32_t imageCount = 0;
	};
}