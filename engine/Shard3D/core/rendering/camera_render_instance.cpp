#include "camera_render_instance.h"

namespace Shard3D {
	CameraRenderInstance::CameraRenderInstance(S3DDevice& device, ResourceSystem* resourceSystem, RendererFeatures rendererFeatures, glm::vec2 resolution,
		uPtr<S3DDescriptorSetLayout>& globalSetLayout, VkDescriptorSetLayout sceneSetLayout, VkDescriptorSetLayout skeletonSetLayout, 
		VkDescriptorSetLayout terrainSetLayout, uint32_t framesInFlight)
		: resolution(resolution), engineDevice(device), resourceSystem(resourceSystem) {
		SHARD3D_ASSERT(rendererFeatures.deferredRendererFeatures.enableNormalDecals ? rendererFeatures.deferredRendererFeatures.enableNormalDecals == rendererFeatures.enableDecals : true);

		globalPool = S3DDescriptorPool::Builder(engineDevice)
			.setMaxSets(framesInFlight)
			.addPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, framesInFlight)
			.build();
		uboBuffers.resize(framesInFlight);
		for (int i = 0; i < uboBuffers.size(); i++) {
			uboBuffers[i] = make_uPtr<S3DBuffer>(
				engineDevice,
				sizeof(GlobalUbo),
				1,
				VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
			);
			uboBuffers[i]->map();
		}
		globalDescriptorSets.resize(framesInFlight);
		for (int i = 0; i < globalDescriptorSets.size(); i++) {
			auto bufferInfo = uboBuffers[i]->descriptorInfo();
			S3DDescriptorWriter(*globalSetLayout, *globalPool)
				.writeBuffer(0, &bufferInfo)
				.build(globalDescriptorSets[i]);
		}

		if (ProjectSystem::getEngineSettings().postfx.enableMotionBlur) {
			rendererFeatures.deferredRendererFeatures.enableVelocityBuffer = true;
		}
		deferredRenderSystem = new DeferredRenderSystem(engineDevice, resourceSystem, rendererFeatures.deferredRendererFeatures, globalSetLayout->getDescriptorSetLayout(), sceneSetLayout, skeletonSetLayout, terrainSetLayout, resolution);
		SceneBufferInputData gbuffer = deferredRenderSystem->getGBufferAttachments();
		if (rendererFeatures.enableMotionBlur) {
			velocityBuffer = new VelocityBufferSystem(engineDevice, resolution, gbuffer.depthSceneInfo, gbuffer.gbuffer4, deferredRenderSystem->getVelocityBuffer(), globalSetLayout->getDescriptorSetLayout());
			mb = new PostFX::MotionBlur(engineDevice, resolution, gbuffer.colorAttachment, gbuffer.depthSceneInfo, velocityBuffer->getAttachment(), floor(resolution.y * (20.0 / 720.0)));
			motionBlurFX = make_uPtr<PostProcessingEffect>(
				engineDevice,
				resolution,
				S3DShader(ShaderType::Fragment, "resources/shaders/postfx/motion_blur.frag", "postfx"),
				std::vector<VkDescriptorImageInfo>{ gbuffer.colorAttachment->getDescriptor(), gbuffer.depthSceneInfo->getDescriptor(), velocityBuffer->getAttachment()->getDescriptor(), mb->getNext()->getDescriptor() },
				VK_FORMAT_R16G16B16A16_SFLOAT
			);
			gbuffer.colorAttachment = motionBlurFX->getAttachment();
		}
		if (rendererFeatures.enablePostFX) {
			postProcessingSystem = new PostProcessingSystem(engineDevice, resolution, gbuffer);
		}
		if (rendererFeatures.enableDecals) {
			decalRenderSystem = new DecalRenderSystem(engineDevice, resourceSystem, globalSetLayout->getDescriptorSetLayout(), gbuffer);
			deferredRenderSystem->writeDescriptors(decalRenderSystem->getDBufferAttachments().dbuffer2->getDescriptor());
		} else {
			deferredRenderSystem->writeDescriptors();
		}

		volumetricLighting = new VolumetricLightingSystem(
			engineDevice, resourceSystem, resolution / 2.f, gbuffer, deferredRenderSystem->getCompositionRenderPass()->getRenderPass(),
			globalSetLayout->getDescriptorSetLayout(), sceneSetLayout);
		
		skyAtmosphere = new SkyAtmosphereSystem(engineDevice, resourceSystem, globalSetLayout->getDescriptorSetLayout(), deferredRenderSystem->getCompositionRenderPass()->getRenderPass());

		telemetry = new Telemetry(engineDevice);
		earlyZPassCPU = make_uPtr<TelemetryCPUMetric>(telemetry, "Camera Renderer", "EarlyZ Pass (CPU)");
		earlyZPassGPU = make_uPtr<TelemetryGPUMetric>(telemetry, "Camera Renderer", "EarlyZ Pass (GPU)");

		gbufferPassCPU = make_uPtr<TelemetryCPUMetric>(telemetry, "Camera Renderer", "GBuffer Pass (CPU)");
		gbufferPassGPU = make_uPtr<TelemetryGPUMetric>(telemetry, "Camera Renderer", "GBuffer Pass (GPU)");

		decalPassCPU = make_uPtr<TelemetryCPUMetric>(telemetry, "Camera Renderer", "Decal Pass (CPU)");
		decalPassGPU = make_uPtr<TelemetryGPUMetric>(telemetry, "Camera Renderer", "Decal Pass (GPU)");

		lightingPassGPU = make_uPtr<TelemetryGPUMetric>(telemetry, "Camera Renderer", "Deferred Shading Pass (GPU)");

		volumetricPassGPU = make_uPtr<TelemetryGPUMetric>(telemetry, "Camera Renderer", "Volumetric Lighting (GPU)");

		translucencyPassCPU = make_uPtr<TelemetryCPUMetric>(telemetry, "Camera Renderer", "Translucency (CPU)");
		translucencyPassGPU = make_uPtr<TelemetryGPUMetric>(telemetry, "Camera Renderer", "Translucency (GPU)");

		compositionPassGPU = make_uPtr<TelemetryGPUMetric>(telemetry, "Camera Renderer", "Composition Pass (GPU)");

		velocityBufferGPU = make_uPtr<TelemetryGPUMetric>(telemetry, "Camera Renderer", "Velocity Buffer (GPU)");
		motionBlurGPU = make_uPtr<TelemetryGPUMetric>(telemetry, "Camera Renderer", "Motion Blur (GPU)");
		postFXGPU = make_uPtr<TelemetryGPUMetric>(telemetry, "Camera Renderer", "Post FX (GPU)");
		ssaoGPU = make_uPtr<TelemetryGPUMetric>(telemetry, "Camera Renderer", "SSAO (GPU)");

		telemetry->registerTelemetryMetric(earlyZPassCPU.get());
		telemetry->registerTelemetryMetric(earlyZPassGPU.get());
		telemetry->registerTelemetryMetric(gbufferPassCPU.get());
		telemetry->registerTelemetryMetric(gbufferPassGPU.get());
		telemetry->registerTelemetryMetric(decalPassCPU.get());
		telemetry->registerTelemetryMetric(decalPassGPU.get());
		telemetry->registerTelemetryMetric(lightingPassGPU.get());
		telemetry->registerTelemetryMetric(volumetricPassGPU.get());
		telemetry->registerTelemetryMetric(translucencyPassCPU.get());
		telemetry->registerTelemetryMetric(translucencyPassGPU.get());
		telemetry->registerTelemetryMetric(compositionPassGPU.get());
		telemetry->registerTelemetryMetric(motionBlurGPU.get());
		telemetry->registerTelemetryMetric(velocityBufferGPU.get());
		telemetry->registerTelemetryMetric(postFXGPU.get());
		telemetry->registerTelemetryMetric(ssaoGPU.get());
	}
	CameraRenderInstance::~CameraRenderInstance() {
		delete deferredRenderSystem;
		delete volumetricLighting;
		if (postProcessingSystem) {
			delete postProcessingSystem;
		}
		if (velocityBuffer) {
			delete velocityBuffer;
			delete mb;
		}
		if (decalRenderSystem) {
			delete decalRenderSystem;
		}
		delete skyAtmosphere;
		delete telemetry;
	}
	void CameraRenderInstance::updateCamera(sPtr<Level>& level, Actor cameraActor, float aspectRatio) {
		auto& cameraComponent = cameraActor.getComponent<Components::CameraComponent>();
		cameraComponent.ar = aspectRatio;
		cameraComponent.setProjection(ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit, ProjectSystem::getEngineSettings().renderer.infiniteFarZ);
		cameraComponent.camera.setViewYXZ(cameraActor.getTransform().transformMatrix);
		postProcessingApplier.update(level, cameraComponent.camera.postfx, cameraActor);
	}
	void CameraRenderInstance::renderEarlyZ(VkCommandBuffer commandBuffer, VkDescriptorSet sceneSSBO, VkFence fence, uint32_t frameIndex, 
		float frameTime, Actor cameraActor, sPtr<Level>& level, RenderObjects renderObjects) {
		auto& cameraComponent = cameraActor.getComponent<Components::CameraComponent>();
		FrameInfo frameInfo{
			frameIndex,
			commandBuffer,
			cameraComponent.camera,
			telemetry,
			globalDescriptorSets[frameIndex],
			sceneSSBO,
			level,
			renderObjects,
			resourceSystem
		};

		GlobalUbo ubo{};
		ubo.projection = frameInfo.camera.getProjection();
		ubo.inverseProjection = frameInfo.camera.getInverseProjection();
		ubo.view = frameInfo.camera.getView();
		ubo.inverseView = frameInfo.camera.getInverseView();
		ubo.projectionView = frameInfo.camera.getProjection() * frameInfo.camera.getView();
		ubo.projectionPrevFrame = projectionPrevFrame;
		ubo.viewPrevFrame = viewPrevFrame;
		ubo.projectionViewPrevFrame = projectionViewPrevFrame;
		ubo.frustumPlanes = frameInfo.camera.getFrustumPlanes();
		ubo.nearPlane = cameraComponent.nearClip;
		ubo.farPlane = cameraComponent.farClip;
		ubo.globalClipPlane = { 0.0f, 0.0f, 0.0f, 0.0f };
		ubo.screenSize = resolution;

		// velocity buffer stuff
		ubo.velocityBufferTileExtent = floor(resolution.y * (20.f / 720.f)) / 2;
		projectionPrevFrame = ubo.projectionPrevFrame;
		viewPrevFrame = ubo.viewPrevFrame;
		projectionViewPrevFrame = ubo.projectionView;

		uboBuffers[frameIndex]->writeToBuffer(&ubo);
		uboBuffers[frameIndex]->flush();

		earlyZPassCPU->record();
		earlyZPassGPU->record(commandBuffer);

		if (frameInfo.renderObjects.terrainList->getTerrain()) {
			frameInfo.renderObjects.terrainList->getTerrain()->updateVisibilityCalcs(commandBuffer, frameInfo.camera, 32.0f);
		}
		deferredRenderSystem->beginEarlyZPass(frameInfo);
		deferredRenderSystem->renderEarlyDepthTerrain(frameInfo);
		deferredRenderSystem->renderEarlyDepthMesh3D(frameInfo, MeshMobilityOption_All);
		for (auto& [renderCall, userData] : earlyZRenderCalls) {
			renderCall(frameInfo, userData);
		}
		deferredRenderSystem->endEarlyZPass(frameInfo);
		earlyZPassCPU->end();
		earlyZPassGPU->end(commandBuffer);
	}
	void CameraRenderInstance::render(VkCommandBuffer commandBuffer, VkDescriptorSet sceneSSBO, VkFence fence, uint32_t frameIndex, 
		float frameTime, Actor cameraActor, sPtr<Level>& level, RenderObjects renderObjects, SceneBuffers& sceneBuffers) {
		auto& cameraComponent = cameraActor.getComponent<Components::CameraComponent>();
		FrameInfo frameInfo{
			frameIndex,
			commandBuffer,
			cameraComponent.camera,
			telemetry,
			globalDescriptorSets[frameIndex],
			sceneSSBO,
			level,
			renderObjects,
			resourceSystem
		};

		skyAtmosphere->runGarbageCollector(frameIndex, fence);

		gbufferPassCPU->record();
		gbufferPassGPU->record(commandBuffer);

		if (frameInfo.renderObjects.terrainList->getTerrain()) {
			frameInfo.renderObjects.terrainList->getTerrain()->updateVisibilityCalcs(commandBuffer, frameInfo.camera, 32.0f);
		}

		deferredRenderSystem->beginGBufferPass(frameInfo);
		deferredRenderSystem->renderGBufferTerrain(frameInfo);
		deferredRenderSystem->renderGBufferMesh3D(frameInfo, MeshMobilityOption_All);
		for (auto& [renderCall, userData]: gbufferRenderCalls) {
			renderCall(frameInfo, userData);
		}
		deferredRenderSystem->endGBufferPass(frameInfo);
		gbufferPassCPU->end();
		gbufferPassGPU->end(commandBuffer);

		decalPassCPU->record();
		decalPassGPU->record(commandBuffer);
		if (decalRenderSystem) {
			decalRenderSystem->renderDecals(frameInfo);
			decalRenderSystem->applyDBuffer(frameInfo);
		}
		decalPassCPU->end();
		decalPassGPU->end(commandBuffer);

		ssaoGPU->record(commandBuffer);
		deferredRenderSystem->renderSSAO(frameInfo);
		ssaoGPU->end(commandBuffer);

		skyAtmosphere->renderSkyLightCubemap(frameInfo);
		lightingPassGPU->record(commandBuffer);
		deferredRenderSystem->renderDeferredLighting(frameInfo, sceneBuffers);
		deferredRenderSystem->composeLighting(frameInfo);


		deferredRenderSystem->beginCompositionPass(frameInfo);
		deferredRenderSystem->renderSkybox(frameInfo);
		skyAtmosphere->render(frameInfo);
		deferredRenderSystem->endComposition(frameInfo);

		deferredRenderSystem->composeLightingMS(frameInfo);

		lightingPassGPU->end(commandBuffer);

		translucencyPassCPU->record();
		translucencyPassGPU->record(commandBuffer);
		deferredRenderSystem->beginBlendingPass(frameInfo);
		deferredRenderSystem->renderForwardMesh3D(frameInfo, MeshMobilityOption_All);
		for (auto& [renderCall, userData] : forwardRenderCalls) {
			renderCall(frameInfo, userData);
		}
		deferredRenderSystem->endBlendingPass(frameInfo);
		translucencyPassCPU->end();
		translucencyPassGPU->end(commandBuffer);

		volumetricPassGPU->record(commandBuffer);
		volumetricLighting->render(frameInfo);
		volumetricLighting->blur(frameInfo);
		volumetricPassGPU->end(commandBuffer);

		compositionPassGPU->record(commandBuffer);
		deferredRenderSystem->beginCompositionPass(frameInfo);
		for (auto& [renderCall, userData] : postCompositionRenderCalls) {
			renderCall(frameInfo, userData);
		}
		deferredRenderSystem->composeAlpha(frameInfo);
		volumetricLighting->composite(frameInfo);
		deferredRenderSystem->endComposition(frameInfo);
		compositionPassGPU->end(commandBuffer);

		float velocityBufferTileExtent = floor(resolution.y * (20.f / 720.f)) / 2;
		if (velocityBuffer) {
			velocityBufferGPU->record(commandBuffer);
			velocityBuffer->renderAndUpdate(frameInfo);
			mb->render(commandBuffer, frameInfo.camera.postfx);
			velocityBufferGPU->end(commandBuffer);

			struct MotionBlurInfo {
				float targetFrameRate;
				float currentFrameRate;
				int samples;
				float cameraNearZ;
				float cameraFarZ;
				float k;
				float kEx;
			};
			MotionBlurInfo motionBlur{};
			motionBlur.targetFrameRate = frameInfo.camera.postfx.motionBlur.targetFramerate;
			motionBlur.currentFrameRate = 1.0f / frameTime;
			motionBlur.samples = ProjectSystem::getGraphicsSettings().postfx.motionBlur.samples;//frameInfo.camera.postfx.motionBlur.quality;
			motionBlur.cameraNearZ = cameraComponent.nearClip;
			motionBlur.cameraFarZ = cameraComponent.farClip;
			motionBlur.k = velocityBufferTileExtent * 2;
			motionBlur.kEx = velocityBufferTileExtent;
			
			motionBlurGPU->record(commandBuffer);
			motionBlurFX->render(commandBuffer, motionBlur);
			motionBlurGPU->end(commandBuffer);
		}
		if (postProcessingSystem) {
			postFXGPU->record(commandBuffer);
			postProcessingSystem->renderCore(frameInfo);
			postFXGPU->end(commandBuffer);
		}
		for (auto& [renderCall, userData] : arbitraryRenderCalls) {
			renderCall(frameInfo, userData);
		}
	}
	void CameraRenderInstance::resize(glm::vec2 newResolution) {
		resolution = newResolution;
		deferredRenderSystem->resize({ newResolution, 1 });
		SceneBufferInputData gbuffer = deferredRenderSystem->getGBufferAttachments();
		if (velocityBuffer) {
			velocityBuffer->resize(newResolution, getGBufferImages().depthSceneInfo, getGBufferImages().gbuffer4);
			mb->resize(newResolution, { getGBufferImages().colorAttachment->getDescriptor(), getGBufferImages().depthSceneInfo->getDescriptor(), velocityBuffer->getAttachment()->getDescriptor() });
			motionBlurFX->resize(newResolution, { getGBufferImages().colorAttachment->getDescriptor(), getGBufferImages().depthSceneInfo->getDescriptor(), velocityBuffer->getAttachment()->getDescriptor(), mb->getNext()->getDescriptor()});
			gbuffer.colorAttachment = motionBlurFX->getAttachment();
		}
		volumetricLighting->resize(newResolution/2.f, gbuffer);
		if (postProcessingSystem) {
			postProcessingSystem->resize(newResolution, gbuffer);
		}
		if (decalRenderSystem) {
			decalRenderSystem->resize(deferredRenderSystem->getGBufferAttachments());
			deferredRenderSystem->writeDescriptors(decalRenderSystem->getDBufferAttachments().dbuffer2->getDescriptor());
		} else {
			deferredRenderSystem->writeDescriptors();
		}

	}
}