#pragma once

#include <thread>
#include <future>
#include <fstream>
#include <vector>
#include "../../systems/systems.h"
#include "../asset/assetmgr.h"

namespace Shard3D {
	class LevelRenderInstance {
	public:
		LevelRenderInstance(S3DDevice& device, ResourceSystem* resourceSystem, VkDescriptorSetLayout globalSetLayout, uPtr<S3DDescriptorSetLayout>& sceneSetLayout, uPtr<S3DDescriptorSetLayout>& skeletonDescriptorSetLayout, uPtr<S3DDescriptorSetLayout>& terrainDescriptorSetLayout, uint32_t framesInFlight);
		~LevelRenderInstance();
		DELETE_COPY(LevelRenderInstance)
		DELETE_MOVE(LevelRenderInstance)
		void runGarbageCollector(uint32_t frameIndex, VkFence fence, sPtr<Level>& level);
		RenderObjects getRenderObjects();
		void render(VkCommandBuffer commandBuffer, uint32_t frameIndex, sPtr<Level>& level);

		VkDescriptorSet getDescriptorSet(uint32_t frameIndex) {
			return sceneDescriptorSets[frameIndex];
		}
		ShadowMappingSystem* getShadowMapSystem() {
			return shadowMappingSystem;
		}
		PlanarReflectionSystem* getPlanarReflectionSystem() {
			return planarReflectionMappingSystem;
		}
		uPtr<SceneBuffers>& getBuffer(uint32_t frameIndex) {
			return sceneBuffersStruct[frameIndex];
		}
		void resizePlanarReflections(glm::vec2 newResolution, VkDescriptorImageInfo depthSceneInfo);

	private:
		S3DDevice& engineDevice;
		ResourceSystem* resourceSystem;
		RenderList* renderList{};
		TerrainList* terrainList{};
		DecalBatchList* decalList{};

		uPtr<S3DDescriptorSetLayout>& sceneSetLayout;
		uPtr<S3DDescriptorSetLayout>& skeletonDescriptorSetLayout;
		uPtr<S3DDescriptorSetLayout>& terrainDescriptorSetLayout;

		ShadowMappingSystem* shadowMappingSystem{};
		LightPropagationVolumeSystem* lightPropagationVolumeSystem{};
		PlanarReflectionSystem* planarReflectionMappingSystem{};
		ExponentialFogSystem exponentialFogSystem{};
		LightSystem lightSystem{};
		ReflectionSystem reflectionSystem{};
		SkySystem skySystem{};
		LightingVolumeSystem volumeSystem{};

		struct BufferPointers {
			uPtr<S3DBuffer> omni;
			uPtr<S3DBuffer> omniC;
			uPtr<S3DBuffer> shadows;
			uPtr<S3DBuffer> env;
			uPtr<S3DBuffer> sotafx;
		};
		std::vector<BufferPointers> sceneUBOs{};
		std::vector<VkDescriptorSet> sceneDescriptorSets{};
		std::vector<uPtr<SceneBuffers>> sceneBuffersStruct{};

		friend class CameraRenderInstance;
	};
}