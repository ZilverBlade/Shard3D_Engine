#include "../../s3dpch.h" 
#include "framebuffer.h"
#include "render_pass.h"

namespace Shard3D{

	S3DFramebuffer::S3DFramebuffer(S3DDevice& device, S3DRenderPass* renderPass, const std::vector<S3DRenderTarget*>& attachments, uint32_t layer, uint32_t mipLevel) 
		: engineDevice(device), useLayer(layer), useMipLevel(mipLevel) 
	{
		create(device, renderPass->getRenderPass(), attachments);
		this->attachments.reserve(attachments.size());
		for (S3DRenderTarget* attachment : attachments) {
			this->attachments.push_back(attachment);
		}
	}

	S3DFramebuffer::S3DFramebuffer(S3DDevice& device, S3DRenderPass* renderPass, bool imageless, glm::ivec3 imagelessDimensions)
		: engineDevice(device), useLayer(0), useMipLevel(0)
	{	
		width = imagelessDimensions.x;
		height = imagelessDimensions.y;
		depth = imagelessDimensions.z;
		createImageless(device, renderPass->getRenderPass());
	}

	S3DFramebuffer::~S3DFramebuffer() {
		destroy();
	}

	void S3DFramebuffer::create(S3DDevice& device, VkRenderPass renderPass, const std::vector<S3DRenderTarget*>& newAttachments) {
		std::vector<VkImageView> imageViews;
		int lowestLayerCountOverride = -1;
		for (auto& attachment : newAttachments) {
			if (attachment->getAttachmentDescription().overrideDirectLayerWrite) {
				lowestLayerCountOverride == -1 ? lowestLayerCountOverride = (int)attachment->getAttachmentDescription().layerCount :
				lowestLayerCountOverride = std::min(lowestLayerCountOverride, (int)attachment->getAttachmentDescription().layerCount);
			}
			imageViews.push_back(attachment->getRenderImageView(useLayer, useMipLevel));
		}
		if (lowestLayerCountOverride == -1) lowestLayerCountOverride = 1;

		width = static_cast<uint32_t>(newAttachments[0]->getDimensions().x) / std::pow(2, useMipLevel);
		height = static_cast<uint32_t>(newAttachments[0]->getDimensions().y) / std::pow(2, useMipLevel);
		depth = static_cast<uint32_t>(newAttachments[0]->getDimensions().z);

		VkFramebufferCreateInfo fbufferCreateInfo{};
		fbufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		fbufferCreateInfo.pNext = nullptr;
		fbufferCreateInfo.flags = 0;
		fbufferCreateInfo.renderPass = renderPass;
		fbufferCreateInfo.attachmentCount = static_cast<uint32_t>(imageViews.size());
		fbufferCreateInfo.pAttachments = imageViews.data();
		fbufferCreateInfo.width = width;
		fbufferCreateInfo.height = height;
		fbufferCreateInfo.layers = lowestLayerCountOverride;
		VK_ASSERT(vkCreateFramebuffer(device.device(), &fbufferCreateInfo, nullptr, &framebuffer) ,"Failed to create framebuffer!");
	}

	void S3DFramebuffer::createImageless(S3DDevice& device, VkRenderPass renderPass) {
		VkFramebufferAttachmentsCreateInfo createInfo{};
		createInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENTS_CREATE_INFO;
		createInfo.attachmentImageInfoCount = 0;
		createInfo.pAttachmentImageInfos = nullptr;

		VkFramebufferCreateInfo fbufferCreateInfo{};
		fbufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		fbufferCreateInfo.pNext = &createInfo;
		fbufferCreateInfo.flags = VK_FRAMEBUFFER_CREATE_IMAGELESS_BIT;
		fbufferCreateInfo.renderPass = renderPass;
		fbufferCreateInfo.attachmentCount = 0;
		fbufferCreateInfo.pAttachments = nullptr;
		fbufferCreateInfo.width = width;
		fbufferCreateInfo.height = height;
		fbufferCreateInfo.layers = 1;
		VK_ASSERT(vkCreateFramebuffer(device.device(), &fbufferCreateInfo, nullptr, &framebuffer), "Failed to create framebuffer!");
	}

	void S3DFramebuffer::destroy() {
		vkDestroyFramebuffer(engineDevice.device(), framebuffer, nullptr);
	}

	void S3DFramebuffer::resize(glm::ivec3 newDimensions, S3DRenderPass* renderPass) {
		for (S3DRenderTarget* attachment : attachments) {
			attachment->resize(newDimensions);
		}
		destroy();
		create(engineDevice, renderPass->getRenderPass(), attachments);
	}

}