#pragma once

#include "../../systems/systems.h"
#include "../ecs/actor.h"
#include "level_render_instance.h"
#include "../../systems/post_fx/fx/ppfx_motion_blur.h"
#include "../../systems/rendering/sky_atmosphere_system.h"

namespace Shard3D {
	class CameraRenderInstance {
	public:
		struct RendererFeatures {
			DeferredRenderSystem::RendererFeatures deferredRendererFeatures;
			bool enableMotionBlur;
			bool enablePostFX;
			bool enableDecals;
		};
		CameraRenderInstance(S3DDevice& device, ResourceSystem* resourceSystem, RendererFeatures rendererFeatures, glm::vec2 resolution, uPtr<S3DDescriptorSetLayout>& globalSetLayout, VkDescriptorSetLayout sceneSetLayout, VkDescriptorSetLayout skeletonSetLayout, VkDescriptorSetLayout terrainSetLayout, uint32_t framesInFlight);
		~CameraRenderInstance();

		DELETE_COPY(CameraRenderInstance);
		DELETE_MOVE(CameraRenderInstance);

		void addArbitraryRenderCall(const std::function<void(FrameInfo& frameInfo, void* userData)>& renderCall, void* userData) {
			arbitraryRenderCalls.push_back({ renderCall, userData });
		}
		void addEarlyZRenderCall(const std::function<void(FrameInfo& frameInfo, void* userData)>& renderCall, void* userData) {
			earlyZRenderCalls.push_back({ renderCall, userData });
		}
		void addGBufferRenderCall(const std::function<void(FrameInfo& frameInfo, void* userData)>& renderCall, void* userData) {
			gbufferRenderCalls.push_back({ renderCall, userData });
		}
		void addForwardRenderCall(const std::function<void(FrameInfo& frameInfo, void* userData)>& renderCall, void* userData) {
			forwardRenderCalls.push_back({ renderCall, userData });
		}
		void addPostCompRenderCall(const std::function<void(FrameInfo& frameInfo, void* userData)>& renderCall, void* userData) {
			postCompositionRenderCalls.push_back({ renderCall, userData });
		}

		void updateCamera(sPtr<Level>& level, Actor cameraActor, float aspectRatio);
		void renderEarlyZ(VkCommandBuffer commandBuffer, VkDescriptorSet sceneSSBO, VkFence fence, uint32_t frameIndex, float frameTime, Actor cameraActor,
			sPtr<Level>& level, RenderObjects renderObjects);
		void render(VkCommandBuffer commandBuffer, VkDescriptorSet sceneSSBO, VkFence fence, uint32_t frameIndex, float frameTime, Actor cameraActor,
			sPtr<Level>& level, RenderObjects renderObjects, SceneBuffers& sceneBuffers);
		void resize(glm::vec2 newResolution);
		DeferredRenderSystem* getRenderer() {
			return deferredRenderSystem;
		}
		S3DRenderTarget* getFinalImage() {
			if (postProcessingSystem) {
				return postProcessingSystem->getFinalImage();
			} else {
				if (motionBlurFX) {
					return motionBlurFX->getAttachment();
				} else {
					return deferredRenderSystem->getGBufferAttachments().colorAttachment;
				}
			}
		}
		SceneBufferInputData getGBufferImages() {
			return deferredRenderSystem->getGBufferAttachments();
		}
		DecalBufferInputData getDBufferImages() {
			if (decalRenderSystem) {
				return decalRenderSystem->getDBufferAttachments();
			} else {
				return {};
			}
		}
		LightingBufferInputData getLBufferImages() {
			if (deferredRenderSystem->getRendererFeatures().enableDeferredLighting) {
				return deferredRenderSystem->getLBufferAttachments();
			} else {
				return {};
			}
		}
		S3DRenderTarget* getVelocityBuffer() {
			if (velocityBuffer) {
				return velocityBuffer->getAttachment();
			} else {
				return nullptr;
			}
		}

		Telemetry* getTelemetry() const {
			return telemetry;
		}

		VkDescriptorSet getGlobalDescriptor(uint32_t frameIndex) {
			return globalDescriptorSets[frameIndex];
		}
	private:
		std::vector<std::pair<std::function<void(FrameInfo& frameInfo, void* userData)>, void*>> arbitraryRenderCalls;
		std::vector<std::pair<std::function<void(FrameInfo& frameInfo, void* userData)>, void*>> earlyZRenderCalls;
		std::vector<std::pair<std::function<void(FrameInfo& frameInfo, void* userData)>, void*>> gbufferRenderCalls;
		std::vector<std::pair<std::function<void(FrameInfo& frameInfo, void* userData)>, void*>> forwardRenderCalls;
		std::vector<std::pair<std::function<void(FrameInfo& frameInfo, void* userData)>, void*>> postCompositionRenderCalls;

		Telemetry* telemetry;

		uPtr<TelemetryCPUMetric> earlyZPassCPU;
		uPtr<TelemetryGPUMetric> earlyZPassGPU;
		uPtr<TelemetryCPUMetric> gbufferPassCPU;
		uPtr<TelemetryGPUMetric> gbufferPassGPU;
		uPtr<TelemetryCPUMetric> decalPassCPU;
		uPtr<TelemetryGPUMetric> decalPassGPU;
		uPtr<TelemetryGPUMetric> lightingPassGPU;
		uPtr<TelemetryGPUMetric> volumetricPassGPU;
		uPtr<TelemetryCPUMetric> translucencyPassCPU;
		uPtr<TelemetryGPUMetric> translucencyPassGPU;
		uPtr<TelemetryGPUMetric> compositionPassGPU;

		uPtr<TelemetryGPUMetric> ssaoGPU;
		uPtr<TelemetryGPUMetric> motionBlurGPU;
		uPtr<TelemetryGPUMetric> velocityBufferGPU;
		uPtr<TelemetryGPUMetric> postFXGPU;

		S3DDevice& engineDevice;
		ResourceSystem* resourceSystem;

		uPtr<PostProcessingEffect> motionBlurFX;

		uPtr<S3DDescriptorPool> globalPool{};
		std::vector<uPtr<S3DBuffer>> uboBuffers{};
		std::vector<VkDescriptorSet> globalDescriptorSets{};

		DeferredRenderSystem* deferredRenderSystem{};
		DecalRenderSystem* decalRenderSystem{};
		VelocityBufferSystem* velocityBuffer{};
		PostProcessingSystem* postProcessingSystem{};
		PostProcessingApplySystem postProcessingApplier{};
		SkyAtmosphereSystem* skyAtmosphere{};
		VolumetricLightingSystem* volumetricLighting{};

		PostFX::MotionBlur* mb;
		glm::mat4 projectionPrevFrame;
		glm::mat4 viewPrevFrame;
		glm::mat4 projectionViewPrevFrame;

		std::vector<int> visibleTerrainTiles;
		glm::vec2 resolution;
	};
}