#pragma once
#include "../vulkan_api/device.h"
#include <glm/glm.hpp>

namespace Shard3D {
	enum class S3DRenderTargetType {
		Depth,
		DepthStencil,
		Color,
		Resolve
	};

	struct S3DRenderTargetCreateInfo {
		VkFormat format;
		VkImageAspectFlags imageAspect;
		VkImageViewType viewType;
		glm::ivec3 dimensions;
		VkImageUsageFlags usage;
		VkImageLayout layout;
		S3DRenderTargetType renderTargetType;
		bool linearFiltering = true;
		VkSampleCountFlagBits sampleCount = VK_SAMPLE_COUNT_1_BIT;
		bool enableSamplerPCF = false;
		uint32_t layerCount = 1;
		uint32_t mipLevels = 1;
		bool overrideDirectLayerWrite = false;
		VkImage inheritVkImage = VK_NULL_HANDLE;
	};

	class S3DRenderTarget {
	public:
		S3DRenderTarget(S3DDevice& device, const S3DRenderTargetCreateInfo& attachmentCreateInfo);
		~S3DRenderTarget();

		S3DRenderTarget(const S3DRenderTarget&) = delete;
		S3DRenderTarget& operator=(const S3DRenderTarget&) = delete;
		S3DRenderTarget(S3DRenderTarget&&) = delete;
		S3DRenderTarget& operator=(S3DRenderTarget&&) = delete;

		VkImage getImage() { return image; }
		VkImageSubresourceRange getImageSubresourceRange() { return subresourceRange; }
		VkImageView getImageView() { return imageView; }
		VkImageView getImageViewStencilBuffer() { return imageViewStencilBuffer; }
		VkImageView getRenderImageView(uint32_t layer, uint32_t mipLevel) {
			return renderImageViews[layer][mipLevel];
		}
		VkSampler getSampler() { return sampler; }
		VkImageLayout getImageLayout() { return attachmentDescription.layout; }
		VkDeviceMemory getDeviceMemory() { return imageMemory; }
		glm::ivec3 getDimensions() { return dimensions; }
		size_t getResourceSize() {
			return memSize;
		}
		VkDescriptorImageInfo getDescriptor() {
			return {
				sampler,
				imageView,
				attachmentDescription.layout
			};
		}
		VkDescriptorImageInfo getDescriptorStencilBuffer() {
			return {
				sampler,
				imageViewStencilBuffer,
				attachmentDescription.layout
			};
		}
		S3DRenderTargetType getAttachmentType() { return attachmentDescription.renderTargetType; }
		const S3DRenderTargetCreateInfo& getAttachmentDescription() { return attachmentDescription; }

		void resize(glm::ivec3 newDimensions);
	private:
		void destroy();
		void create(S3DDevice& device, const S3DRenderTargetCreateInfo& attachmentCreateInfo);

		VkImage image{};
		VkDeviceMemory imageMemory{};
		VkSampler sampler{};
		VkImageSubresourceRange subresourceRange{};
		VkImageView imageView{};
		VkImageView imageViewStencilBuffer{};

		size_t memSize = 0;
		std::vector<std::vector<VkImageView>> renderImageViews{};
		std::vector<std::vector<VkImageSubresourceRange>> renderSubresourceRanges{};

		S3DRenderTargetCreateInfo attachmentDescription;

		S3DDevice& engineDevice;

		bool inheritsVkImage = false;

		glm::ivec3 dimensions{};
	};
}