#include "../../s3dpch.h"
#include "camera.h"

namespace Shard3D {

	void S3DCamera::setOrthographicProjection(
		float left, float right, float top, float bottom, float zNear /*Near clipping plane*/, float zFar /*Far clipping plane*/, bool reverseDepth) {
		projectionMatrix = glm::mat4{ 1.0f };
		projectionMatrix[0][0] = 2.f / (right - left);
		projectionMatrix[1][1] = 2.f / (bottom - top);
		projectionMatrix[2][2] = 1.f / (zFar - zNear);
		projectionMatrix[3][0] = -(right + left) / (right - left);
		projectionMatrix[3][1] = -(bottom + top) / (bottom - top);

		if (reverseDepth) {
			projectionMatrix[3][2] = zFar / (zFar - zNear);
		} else {
			projectionMatrix[3][2] = -zNear / (zFar - zNear);
		}

		nearZ = zNear;
		farZ = zFar;
		inverseProjectionMatrix = glm::inverse(projectionMatrix);
		projectionViewMatrix = projectionMatrix * viewMatrix;
		recalculateFrustumPlanes();
	}

	void S3DCamera::setPerspectiveProjection(float fovy, float aspect, float zNear /*Near clipping plane*/, float zFar /*Far clipping plane*/, bool reverseDepth, bool infiniteFarZ) {
		SHARD3D_ASSERT(glm::abs(aspect - std::numeric_limits<float>::epsilon()) > 0.0f);
		infiniteFarZ *= reverseDepth;
		const float tanHalfFovy = tan(fovy / 2.f);
		projectionMatrix = glm::mat4{ 0.0f };
		projectionMatrix[0][0] = 1.f / (aspect * tanHalfFovy);
		projectionMatrix[1][1] = 1.f / (tanHalfFovy);
		projectionMatrix[2][3] = 1.f;

		if (infiniteFarZ) {
			projectionMatrix[3][2] = zNear;
		} else if (reverseDepth) {
			projectionMatrix[2][2] = -zNear / (zFar - zNear);
			projectionMatrix[3][2] = (zFar * zNear) / (zFar - zNear);
		} else {
			projectionMatrix[2][2] = zFar / (zFar - zNear);
			projectionMatrix[3][2] = -(zFar * zNear) / (zFar - zNear);
		}

		nearZ = zNear;
		farZ = zFar;
		inverseProjectionMatrix = glm::inverse(projectionMatrix);
		projectionViewMatrix = projectionMatrix * viewMatrix;
		recalculateFrustumPlanes();
	}
	/* *
* Set camera view based on the camera's position and direction
*/
	void S3DCamera::setViewDirection(glm::vec3 position, glm::vec3 direction, glm::vec3 up) {
		const glm::vec3 w{ glm::normalize(direction) };
		const glm::vec3 u{ glm::normalize(glm::cross(w, up)) };
		const glm::vec3 v{ glm::cross(w, u) };

		viewMatrix = glm::mat4{ 1.f };
		viewMatrix[0][0] = u.x;
		viewMatrix[1][0] = u.y;
		viewMatrix[2][0] = u.z;
		viewMatrix[0][1] = v.x;
		viewMatrix[1][1] = v.y;
		viewMatrix[2][1] = v.z;
		viewMatrix[0][2] = w.x;
		viewMatrix[1][2] = w.y;
		viewMatrix[2][2] = w.z;
		viewMatrix[3][0] = -glm::dot(u, position);
		viewMatrix[3][1] = -glm::dot(v, position);
		viewMatrix[3][2] = -glm::dot(w, position);

		inverseViewMatrix = glm::mat4{ 1.f };
		inverseViewMatrix[0][0] = u.x;
		inverseViewMatrix[0][1] = u.y;
		inverseViewMatrix[0][2] = u.z;
		inverseViewMatrix[1][0] = v.x;
		inverseViewMatrix[1][1] = v.y;
		inverseViewMatrix[1][2] = v.z;
		inverseViewMatrix[2][0] = w.x;
		inverseViewMatrix[2][1] = w.y;
		inverseViewMatrix[2][2] = w.z;
		inverseViewMatrix[3][0] = position.x;
		inverseViewMatrix[3][1] = position.y;
		inverseViewMatrix[3][2] = position.z;

		projectionViewMatrix = projectionMatrix * viewMatrix;
		recalculateFrustumPlanes();
	}
	/* *
* Set camera view based on the camera's position and target
*/
	void S3DCamera::setViewTarget(glm::vec3 position, glm::vec3 target, glm::vec3 up) {
		SHARD3D_ASSERT((target - position) != glm::vec3(0) && "direction cannot be 0");
		setViewDirection(position, target - position, up);
	}
	/* *
* Set camera view based on the camera's position and rotation
*/
	void S3DCamera::setViewYXZ(glm::vec3 position, glm::vec3 rotation) {		
		const float c3 = glm::cos(rotation.z);
		const float s3 = glm::sin(rotation.z);
		const float c2 = glm::cos(rotation.x);
		const float s2 = glm::sin(rotation.x);
		const float c1 = glm::cos(rotation.y);
		const float s1 = glm::sin(rotation.y);
		const glm::vec3 u{ (c1 * c3 + s1 * s2 * s3), (c2 * s3), (c1 * s2 * s3 - c3 * s1) };
		const glm::vec3 v{ (c3 * s1 * s2 - c1 * s3), (c2 * c3), (c1 * c3 * s2 + s1 * s3) };
		const glm::vec3 w{ (c2 * s1), (-s2), (c1 * c2) };
		viewMatrix = glm::mat4{ 1.f };
		viewMatrix[0][0] = u.x;
		viewMatrix[1][0] = u.y;
		viewMatrix[2][0] = u.z;
		viewMatrix[0][1] = v.x;
		viewMatrix[1][1] = v.y;
		viewMatrix[2][1] = v.z;
		viewMatrix[0][2] = w.x;
		viewMatrix[1][2] = w.y;
		viewMatrix[2][2] = w.z;
		viewMatrix[3][0] = -glm::dot(u, position);
		viewMatrix[3][1] = -glm::dot(v, position);
		viewMatrix[3][2] = -glm::dot(w, position);

		inverseViewMatrix = glm::mat4{ 1.f };
		inverseViewMatrix[0][0] = u.x;
		inverseViewMatrix[0][1] = u.y;
		inverseViewMatrix[0][2] = u.z;
		inverseViewMatrix[1][0] = v.x;
		inverseViewMatrix[1][1] = v.y;
		inverseViewMatrix[1][2] = v.z;
		inverseViewMatrix[2][0] = w.x;
		inverseViewMatrix[2][1] = w.y;
		inverseViewMatrix[2][2] = w.z;
		inverseViewMatrix[3][0] = position.x;
		inverseViewMatrix[3][1] = position.y;
		inverseViewMatrix[3][2] = position.z;

		projectionViewMatrix = projectionMatrix * viewMatrix;
		recalculateFrustumPlanes();
	}
	void S3DCamera::setViewYXZ(glm::mat4 transformMatrix) {	
		transformMatrix[0] /= glm::length(transformMatrix[0]);
		transformMatrix[1] /= glm::length(transformMatrix[1]);
		transformMatrix[2] /= glm::length(transformMatrix[2]);

		inverseViewMatrix = transformMatrix;

		viewMatrix = glm::mat4{ 1.f };

		const glm::vec3 u =  { 
			inverseViewMatrix[0][0],
			inverseViewMatrix[0][1],
			inverseViewMatrix[0][2]
		};
		const glm::vec3 v = { 
			inverseViewMatrix[1][0],
			inverseViewMatrix[1][1],
			inverseViewMatrix[1][2] 
		};
		const glm::vec3 w = { 
			inverseViewMatrix[2][0],
			inverseViewMatrix[2][1],
			inverseViewMatrix[2][2] 
		};

		viewMatrix[0][0] = inverseViewMatrix[0][0];
		viewMatrix[1][0] = inverseViewMatrix[0][1];
		viewMatrix[2][0] = inverseViewMatrix[0][2];
		viewMatrix[0][1] = inverseViewMatrix[1][0];
		viewMatrix[1][1] = inverseViewMatrix[1][1];
		viewMatrix[2][1] = inverseViewMatrix[1][2];
		viewMatrix[0][2] = inverseViewMatrix[2][0];
		viewMatrix[1][2] = inverseViewMatrix[2][1];
		viewMatrix[2][2] = inverseViewMatrix[2][2];
		viewMatrix[3][0] = -glm::dot(u, { inverseViewMatrix[3].x, inverseViewMatrix[3].y, inverseViewMatrix[3].z });
		viewMatrix[3][1] = -glm::dot(v, { inverseViewMatrix[3].x, inverseViewMatrix[3].y, inverseViewMatrix[3].z });
		viewMatrix[3][2] = -glm::dot(w, { inverseViewMatrix[3].x, inverseViewMatrix[3].y, inverseViewMatrix[3].z });

		projectionViewMatrix = projectionMatrix * viewMatrix;
		recalculateFrustumPlanes();
	}

	void S3DCamera::setViewMatrix(glm::mat4 vm) {
		viewMatrix = vm;
		projectionViewMatrix = projectionMatrix * viewMatrix;
		recalculateFrustumPlanes();
	}


	void S3DCamera::recalculateFrustumPlanes() {
		frustumPlanes[0].x = projectionViewMatrix[0].w + projectionViewMatrix[0].x;
		frustumPlanes[0].y = projectionViewMatrix[1].w + projectionViewMatrix[1].x;
		frustumPlanes[0].z = projectionViewMatrix[2].w + projectionViewMatrix[2].x;
		frustumPlanes[0].w = projectionViewMatrix[3].w + projectionViewMatrix[3].x;

		frustumPlanes[1].x = projectionViewMatrix[0].w - projectionViewMatrix[0].x;
		frustumPlanes[1].y = projectionViewMatrix[1].w - projectionViewMatrix[1].x;
		frustumPlanes[1].z = projectionViewMatrix[2].w - projectionViewMatrix[2].x;
		frustumPlanes[1].w = projectionViewMatrix[3].w - projectionViewMatrix[3].x;

		frustumPlanes[2].x = projectionViewMatrix[0].w - projectionViewMatrix[0].y;
		frustumPlanes[2].y = projectionViewMatrix[1].w - projectionViewMatrix[1].y;
		frustumPlanes[2].z = projectionViewMatrix[2].w - projectionViewMatrix[2].y;
		frustumPlanes[2].w = projectionViewMatrix[3].w - projectionViewMatrix[3].y;

		frustumPlanes[3].x = projectionViewMatrix[0].w + projectionViewMatrix[0].y;
		frustumPlanes[3].y = projectionViewMatrix[1].w + projectionViewMatrix[1].y;
		frustumPlanes[3].z = projectionViewMatrix[2].w + projectionViewMatrix[2].y;
		frustumPlanes[3].w = projectionViewMatrix[3].w + projectionViewMatrix[3].y;

		frustumPlanes[4].x = projectionViewMatrix[0].w + projectionViewMatrix[0].z;
		frustumPlanes[4].y = projectionViewMatrix[1].w + projectionViewMatrix[1].z;
		frustumPlanes[4].z = projectionViewMatrix[2].w + projectionViewMatrix[2].z;
		frustumPlanes[4].w = projectionViewMatrix[3].w + projectionViewMatrix[3].z;

		frustumPlanes[5].x = projectionViewMatrix[0].w - projectionViewMatrix[0].z;
		frustumPlanes[5].y = projectionViewMatrix[1].w - projectionViewMatrix[1].z;
		frustumPlanes[5].z = projectionViewMatrix[2].w - projectionViewMatrix[2].z;
		frustumPlanes[5].w = projectionViewMatrix[3].w - projectionViewMatrix[3].z;

		// normalize
		for (size_t i = 0; i < 6; i++) {
			frustumPlanes[i] /= glm::length(glm::vec3{ frustumPlanes[i] });
		}
	}

	void S3DCamera::flipProjectionUpsideDown() {
		glm::mat4 flipMatrix = {
			{ 1.0f, 0.0f, 0.0f, 0.0f },
			{ 0.0f, -1.0f, 0.0f, 0.0f },
			{ 0.0f, 0.0f, 1.0f, 0.0f },
			{ 0.0f, 0.0f, 0.0f, 1.0f }
		};

		projectionMatrix = flipMatrix * projectionMatrix;

		inverseProjectionMatrix = glm::inverse(projectionMatrix);
		projectionViewMatrix = projectionMatrix * viewMatrix;
		recalculateFrustumPlanes();
	}

	bool S3DCamera::isInFrustum(const SphereBounds& sphere, glm::vec4 position) const {
		float dist = -sphere.radius;
		for (int i = 0; i < 6; i++) {
			if (glm::dot(frustumPlanes[i], position) < dist) {
				return false;
			}
		}
		return true;
	}

	bool S3DCamera::isInFrustum(const VolumePoints& points) const {
		for (int i = 0; i < 6; i++) {
			bool inside = false;

			for (int j = 0; j < 8; j++) {
				if (glm::dot(frustumPlanes[i], points.points[j]) > 0.0f) {
					inside = true;
					break;
				}
			}

			if (!inside) {
				return false;
			}
		}

		return true;
	}

	bool S3DCamera::isInFrustum(const VolumeBounds& aabb) const {
		for (int i = 0; i < 6; i++) {
			// Check each axis (x,y,z) to get the AABB vertex furthest away from the direction the plane is facing (plane normal)
			glm::vec4 axisVert;
			
			axisVert.x = frustumPlanes[i].x < 0.0f ? aabb.minXYZ.x : aabb.maxXYZ.x;
			axisVert.y = frustumPlanes[i].y < 0.0f ? aabb.minXYZ.y : aabb.maxXYZ.y;
			axisVert.z = frustumPlanes[i].z < 0.0f ? aabb.minXYZ.z : aabb.maxXYZ.z;
			axisVert.w = 1.0f;
			
			// Now we get the signed distance from the AABB vertex that's furthest down the frustum planes normal,
			// and if the signed distance is negative, then the entire bounding box is behind the frustum plane, which means
			// that it should be culled
			if (glm::dot(frustumPlanes[i], axisVert) < 0.0f) {
				return false; // not in frustum
			}
			//glm::vec3 extents = (aabb.maxXYZ - aabb.minXYZ) * 0.5f;
			//glm::vec3 center = (aabb.maxXYZ + aabb.minXYZ) * 0.5f;
			//const float r = extents.x * std::abs(frustumPlanes[i].x) +
			//	extents.y * std::abs(frustumPlanes[i].y) + extents.z * std::abs(frustumPlanes[i].z);
			//
			//if (-r > glm::dot(glm::vec4(center, 1.0f), frustumPlanes[i])) {
			//	return false;
			//}
		}
		return true;
	}
}