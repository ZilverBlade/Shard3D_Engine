#include "level_render_instance.h"
#include <glm/packing.hpp>
namespace Shard3D {
	LevelRenderInstance::LevelRenderInstance(S3DDevice& device, ResourceSystem* resourceSystem, VkDescriptorSetLayout globalSetLayout,
		uPtr<S3DDescriptorSetLayout>& sceneSetLayout, uPtr<S3DDescriptorSetLayout>& skeletonDescriptorSetLayout, 
		uPtr<S3DDescriptorSetLayout>& terrainDescriptorSetLayout, uint32_t framesInFlight)
		: engineDevice(device), resourceSystem(resourceSystem), sceneSetLayout(sceneSetLayout), skeletonDescriptorSetLayout(skeletonDescriptorSetLayout), terrainDescriptorSetLayout(terrainDescriptorSetLayout) {
		static_assert(sizeof(SceneBuffers::sceneOmniLights) <= 65535 && "Buffer too large to be uniform!");
		static_assert(sizeof(SceneBuffers::sceneOmniComplexLights) <= 65535 && "Buffer too large to be uniform!");
		static_assert(sizeof(SceneBuffers::sceneShadowMapInfos) <= 65535 && "Buffer too large to be uniform!");
		static_assert(sizeof(SceneBuffers::sceneEnvironment) <= 65535 && "Buffer too large to be uniform!");
		static_assert(sizeof(SceneBuffers::sceneSotAFX) <= 65535 && "Buffer too large to be uniform!");

		renderList = new RenderList(ProjectSystem::getEngineSettings().allowedMaterialPermutations, skeletonDescriptorSetLayout, resourceSystem);
		terrainList = new TerrainList(engineDevice, terrainDescriptorSetLayout, globalSetLayout);
		if (ProjectSystem::getEngineSettings().renderer.enableDeferredDecals) {
			decalList = new DecalBatchList(engineDevice, resourceSystem);
		}

		sceneUBOs.resize(framesInFlight);
		for (int i = 0; i < sceneUBOs.size(); i++) {
			sceneUBOs[i].omni = make_uPtr<S3DBuffer>(
				engineDevice,
				sizeof(SceneBuffers::sceneOmniLights),
				1,
				VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
				);
			sceneUBOs[i].omni->map();
			sceneUBOs[i].omniC = make_uPtr<S3DBuffer>(
				engineDevice,
				sizeof(SceneBuffers::sceneOmniComplexLights),
				1,
				VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
				);
			sceneUBOs[i].omniC->map();
			sceneUBOs[i].shadows = make_uPtr<S3DBuffer>(
				engineDevice,
				sizeof(SceneBuffers::sceneShadowMapInfos),
				1,
				VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
				);
			sceneUBOs[i].shadows->map();
			sceneUBOs[i].env = make_uPtr<S3DBuffer>(
				engineDevice,
				sizeof(SceneBuffers::sceneEnvironment),
				1,
				VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
				);
			sceneUBOs[i].env->map();
			sceneUBOs[i].sotafx = make_uPtr<S3DBuffer>(
				engineDevice,
				sizeof(SceneBuffers::sceneSotAFX),
				1,
				VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
				);
			sceneUBOs[i].sotafx->map();
		}
		sceneDescriptorSets.resize(framesInFlight);
		sceneBuffersStruct.resize(framesInFlight);
		for (int i = 0; i < sceneDescriptorSets.size(); i++) {
			auto omnibufferInfo = sceneUBOs[i].omni->descriptorInfo();
			auto omnicomplexbufferInfo = sceneUBOs[i].omniC->descriptorInfo();
			auto shadowbufferInfo = sceneUBOs[i].shadows->descriptorInfo();
			auto envbufferInfo = sceneUBOs[i].env->descriptorInfo();
			auto sotafxbufferInfo = sceneUBOs[i].sotafx->descriptorInfo();
			S3DDescriptorWriter(*sceneSetLayout, *device.staticMaterialPool)
				.writeBuffer(0, &omnibufferInfo)
				.writeBuffer(1, &omnicomplexbufferInfo)
				.writeBuffer(2, &shadowbufferInfo)
				.writeBuffer(3, &envbufferInfo)
				.writeBuffer(4, &sotafxbufferInfo)
				.build(sceneDescriptorSets[i]);
			sceneBuffersStruct[i] = make_uPtr<SceneBuffers>();
		}
		shadowMappingSystem = new ShadowMappingSystem(engineDevice, resourceSystem, skeletonDescriptorSetLayout->getDescriptorSetLayout(), terrainDescriptorSetLayout->getDescriptorSetLayout(), framesInFlight);
		lightPropagationVolumeSystem = new LightPropagationVolumeSystem(engineDevice, resourceSystem, shadowMappingSystem);
	}
	LevelRenderInstance::~LevelRenderInstance() {
		delete renderList;
		delete terrainList;
		if (decalList) {
			delete decalList;
		}
		if (shadowMappingSystem)
			delete shadowMappingSystem;
		if (planarReflectionMappingSystem)
			delete planarReflectionMappingSystem;
		if (lightPropagationVolumeSystem)
			delete lightPropagationVolumeSystem;

		engineDevice.staticMaterialPool->freeDescriptors(sceneDescriptorSets);
	}
	void LevelRenderInstance::runGarbageCollector(uint32_t frameIndex, VkFence fence, sPtr<Level>& level) {
		renderList->runGarbageCollector(frameIndex, fence);
		if (decalList) {
			decalList->runGarbageCollector(frameIndex, fence);
		}
		if (shadowMappingSystem)
			shadowMappingSystem->runGarbageCollector(frameIndex, fence);
		if (lightPropagationVolumeSystem)
			lightPropagationVolumeSystem->runGarbageCollector(frameIndex, fence);
		if (planarReflectionMappingSystem)
			planarReflectionMappingSystem->runGarbageCollector(frameIndex, fence);
		terrainList->update(level, resourceSystem);
	}
	RenderObjects LevelRenderInstance::getRenderObjects() {
		return { renderList, terrainList, decalList };
	}
	void LevelRenderInstance::render(VkCommandBuffer commandBuffer, uint32_t frameIndex, sPtr<Level>& level) {
		renderList->updateRenderList(level);
		FrameInfo frameInfo{
			frameIndex,
			commandBuffer,
			level->getPossessedCamera(),
			nullptr, // telemetry
			nullptr, // ubo
			sceneDescriptorSets[frameIndex],
			level,
			getRenderObjects(),
			resourceSystem
		};
		if (decalList) {
			decalList->updateBatches(frameInfo);
		}
		if (shadowMappingSystem)
			shadowMappingSystem->render(frameInfo);
		if (lightPropagationVolumeSystem) {
			lightPropagationVolumeSystem->update(frameInfo, *sceneBuffersStruct[frameIndex]);
			lightPropagationVolumeSystem->render(frameInfo);
		}
		if (planarReflectionMappingSystem)
			planarReflectionMappingSystem->render(frameInfo);
		lightSystem.update(frameInfo, *sceneBuffersStruct[frameIndex]);
		reflectionSystem.update(frameInfo, *sceneBuffersStruct[frameIndex]);
		exponentialFogSystem.update(frameInfo, *sceneBuffersStruct[frameIndex]);
		volumeSystem.update(frameInfo, *sceneBuffersStruct[frameIndex]);
		skySystem.update(frameInfo, *sceneBuffersStruct[frameIndex]);
		exponentialFogSystem.update(frameInfo, *sceneBuffersStruct[frameIndex]);
		sceneUBOs[frameIndex].omni->writeToBuffer(&sceneBuffersStruct[frameIndex]->sceneOmniLights);
		sceneUBOs[frameIndex].omni->flush();
		sceneUBOs[frameIndex].omniC->writeToBuffer(&sceneBuffersStruct[frameIndex]->sceneOmniComplexLights);
		sceneUBOs[frameIndex].omniC->flush();
		sceneUBOs[frameIndex].shadows->writeToBuffer(&sceneBuffersStruct[frameIndex]->sceneShadowMapInfos);
		sceneUBOs[frameIndex].shadows->flush();
		sceneUBOs[frameIndex].env->writeToBuffer(&sceneBuffersStruct[frameIndex]->sceneEnvironment);
		sceneUBOs[frameIndex].env->flush();
		sceneUBOs[frameIndex].sotafx->writeToBuffer(&sceneBuffersStruct[frameIndex]->sceneSotAFX);
		sceneUBOs[frameIndex].sotafx->flush();
	}
	void LevelRenderInstance::resizePlanarReflections(glm::vec2 newResolution, VkDescriptorImageInfo depthSceneInfo) {
		if (!planarReflectionMappingSystem) {
			planarReflectionMappingSystem = new PlanarReflectionSystem(engineDevice, resourceSystem, depthSceneInfo, sceneSetLayout->getDescriptorSetLayout(), skeletonDescriptorSetLayout->getDescriptorSetLayout(), terrainDescriptorSetLayout->getDescriptorSetLayout(), sceneUBOs.size());
		}
		planarReflectionMappingSystem->resize(newResolution, depthSceneInfo);
	}
}