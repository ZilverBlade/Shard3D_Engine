#pragma once
#include "window.h"
#include "../vulkan_api/command_buffer.h"

namespace Shard3D {
	class S3DRenderer {
	public:
		S3DRenderer(S3DDevice& device);
		~S3DRenderer();

		S3DRenderer(const S3DRenderer&) = delete;
		S3DRenderer& operator=(const S3DRenderer&) = delete;
		S3DRenderer(S3DRenderer&&) = delete;
		S3DRenderer& operator=(S3DRenderer&&) = delete;

		void submitCommandBuffer(S3DCommandBuffer* commandBuffer);
		void submitQueue(VkSubmitInfo submitInfo, VkFence fence);
		VkQueue getQueue() {
			return engineQueue->queue;
		}
	private:
		std::vector<VkCommandBuffer> toSubmitCommandBuffers{};
		VkCommandPool commandPool;
		S3DDevice& engineDevice;
		S3DQueue* engineQueue;
	};
}