#include "window.h"
#include "../../utils/simple_ini.h"
#include "../misc/graphics_settings.h"
#include "../misc/cheat_codes.h"
#include "../asset/image_tools.h"
#include "../../core.h"

namespace Shard3D {
	S3DWindow::S3DWindow(S3DDevice& dvc, int w, int h, std::string name) : device(dvc), width{ w }, height{ h }, windowName{ name } {
		initWindow();
		createWindowSurface(device.instance, &surface);
	}

	S3DWindow::~S3DWindow() {
		glfwDestroyWindow(window);
		glfwTerminate();
		vkDestroySurfaceKHR(device.instance, surface, nullptr);
	}

	void S3DWindow::initWindow() {
		glfwInit();
		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

		if (ProjectSystem::getEngineSettings().window.resizable) {
			glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
		}
		else {
			glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
		}

		monitor = glfwGetPrimaryMonitor();
		window = glfwCreateWindow(
			ProjectSystem::getEngineSettings().window.defaultWidth,
			ProjectSystem::getEngineSettings().window.defaultHeight,
			(ProjectSystem::getGameName()
#ifndef NDEBUG
				+ " !! DEBUG BUILD !!"
#endif
				).c_str(),
			nullptr, 
			nullptr
		);

		glfwSetWindowUserPointer(window, this);
		glfwSetWindowFocusCallback(window, windowFocusCallback);
		glfwSetFramebufferSizeCallback(window, framebufferResizeCallback);
		glfwSetKeyCallback(window, keyCallback);
		glfwSetCharCallback(window, charCallback);
		glfwSetMouseButtonCallback(window, mouseBtnCallback);
		glfwSetCursorPosCallback(window, mouseMotionCallback);
		glfwSetScrollCallback(window, mouseScrollCallback);

		Shard3D::CheatCodes::init(window);

		GLFWimage images[1];
		images[0].pixels = ImageResource::getSTBImage(ENSET_WINDOW_ICON_PATH, &images[0].width, &images[0].height, 0, 4); //rgba channels 
		glfwSetWindowIcon(window, 1, images);
		ImageResource::freeSTBImage(images[0].pixels);

		//ini.LoadFile(GAME_SETTINGS_PATH);
		//
		//if ((std::string)ini.GetValue("WINDOW", "WINDOW_TYPE") == "Borderless") {
		//	setWindowMode(Borderless);
		//} else if ((std::string)ini.GetValue("WINDOW", "WINDOW_TYPE") == "Fullscreen") {
		//	setWindowMode(Fullscreen);
		//} else { //for windowed mode, used as a fallback if invalid options chosen lol
		//	//setWindowMode(Windowed);
		//}
	}

	void S3DWindow::createWindowSurface(VkInstance instance, VkSurfaceKHR* surface) {
		VK_ASSERT(glfwCreateWindowSurface(instance, window, nullptr, surface), "Failed to create window surface!");
	}

	void S3DWindow::setWindowMode(WindowType winType) {
		//CSimpleIniA ini;
		//ini.LoadFile(GAME_SETTINGS_PATH);
		//
		//if (winType == Windowed) {
		//	glfwDestroyWindow(window);
		//	glfwWindowHint(GLFW_DECORATED, true);
		//	window = glfwCreateWindow(
		//		ini.GetLongValue("WINDOW", "DEFAULT_WIDTH"),
		//		ini.GetLongValue("WINDOW", "DEFAULT_HEIGHT"),
		//		ini.GetValue("WINDOW", "WindowName"),
		//		nullptr,
		//		nullptr
		//	);
		//	SHARD3D_INFO("Set Windowed");
		//}
		//else if (winType == Borderless) {
		//	glfwDestroyWindow(window);
		//	glfwWindowHint(GLFW_DECORATED, false);
		//	const GLFWvidmode* mode = glfwGetVideoMode(monitor);
		//	window = glfwCreateWindow(
		//		mode->width,
		//		mode->height + 1,
		//		ini.GetValue("WINDOW", "WindowName"),
		//		nullptr,
		//		nullptr
		//	);
		//	glfwSetWindowPos(window, 0, 0);
		//	SHARD3D_INFO("Set Borderless Fullscreen");
		//}
		//else if (winType == Fullscreen) {
		//	// get resolution of monitor
		//	const GLFWvidmode* videoMode = glfwGetVideoMode(monitor);
		//	// switch to full screen
		//	glfwSetWindowMonitor(window, monitor, 0, 0, videoMode->width, videoMode->height, videoMode->refreshRate);
		//	SHARD3D_INFO("Set Fullscreen");
		//}
	}

	void S3DWindow::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
		S3DWindow* wndPtr = reinterpret_cast<S3DWindow*>(glfwGetWindowUserPointer(window));
		if (action == GLFW_PRESS) {
			Events::KeyDownEvent _event(wndPtr, key, 0);
			wndPtr->eventCallback(_event);
		} else if (action == GLFW_RELEASE){
			Events::KeyReleaseEvent _event(wndPtr, key);
			wndPtr->eventCallback(_event);
		} else if (action == GLFW_REPEAT) {
			Events::KeyDownEvent _event(wndPtr, key, true);
			wndPtr->eventCallback(_event);
		}
	}
	void S3DWindow::charCallback(GLFWwindow* window, unsigned int c) {
		S3DWindow* wndPtr = reinterpret_cast<S3DWindow*>(glfwGetWindowUserPointer(window));
		Events::KeyPressEvent _event(wndPtr, c);
		wndPtr->eventCallback(_event);
	}
	void S3DWindow::mouseBtnCallback(GLFWwindow* window, int button, int action, int mods) {
		S3DWindow* wndPtr = reinterpret_cast<S3DWindow*>(glfwGetWindowUserPointer(window));
		if (action == GLFW_PRESS) {
			Events::MouseButtonDownEvent _event(wndPtr, button);
			wndPtr->eventCallback(_event);
		}
		else if (action == GLFW_RELEASE) {
			Events::MouseButtonReleaseEvent _event(wndPtr, button);
			wndPtr->eventCallback(_event);
		}
	}

	void S3DWindow::mouseMotionCallback(GLFWwindow* window, double xpos, double ypos) {
		S3DWindow* wndPtr = reinterpret_cast<S3DWindow*>(glfwGetWindowUserPointer(window));
		Events::MouseHoverEvent _event(wndPtr, static_cast<float>(xpos), static_cast<float>(ypos));
		wndPtr->eventCallback(_event);
	}

	void S3DWindow::mouseScrollCallback(GLFWwindow* window, double xoffset, double yoffset) {
		S3DWindow* wndPtr = reinterpret_cast<S3DWindow*>(glfwGetWindowUserPointer(window));
		Events::MouseScrollEvent _event(wndPtr, static_cast<float>(xoffset), static_cast<float>(yoffset));
		wndPtr->eventCallback(_event);
	}

	void S3DWindow::framebufferResizeCallback(GLFWwindow* window, int width, int height) {
		auto engineWindow = reinterpret_cast<S3DWindow*>(glfwGetWindowUserPointer(window));
		engineWindow->framebufferResized = true;
		engineWindow->width = width;
		engineWindow->height = height;

		uint32_t icountX = width / 16;
		uint32_t icountY = height / 16;
		// round by excess due to post processing dispatch ID's needing to cover the whole screen
		if (icountX * 16 < width)
			icountX += 1;
		if (icountY * 16 < height)
			icountY += 1;		

		GraphicsSettings2::getRuntimeInfo().PostProcessingInvocationIDCounts = glm::ivec3(icountX, icountY, 1);

		Events::WindowResizeEvent _event(engineWindow, static_cast<uint32_t>(width), static_cast<uint32_t>(height));
		engineWindow->eventCallback(_event);
	}
	void S3DWindow::windowFocusCallback(GLFWwindow* window, int focussed) {
		auto engineWindow = reinterpret_cast<S3DWindow*>(glfwGetWindowUserPointer(window));
		engineWindow->focussed = focussed == GLFW_TRUE;
	}
}