#include "../../s3dpch.h"
#include "render_target.h"

namespace Shard3D {
	S3DRenderTarget::S3DRenderTarget(S3DDevice& device, const S3DRenderTargetCreateInfo& attachmentCreateInfo) 
		: engineDevice(device), attachmentDescription(attachmentCreateInfo), inheritsVkImage(attachmentCreateInfo.inheritVkImage) {
		create(device, attachmentCreateInfo);
	}
	S3DRenderTarget::~S3DRenderTarget() {
		destroy();
	}

	void S3DRenderTarget::create(S3DDevice& device, const S3DRenderTargetCreateInfo& attachmentCreateInfo) {
		if (!inheritsVkImage) {
			VkImageCreateInfo imageCreateInfo{};
			imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
			imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
			imageCreateInfo.format = attachmentCreateInfo.format;
			imageCreateInfo.extent.width = attachmentCreateInfo.dimensions.x;
			imageCreateInfo.extent.height = attachmentCreateInfo.dimensions.y;
			imageCreateInfo.extent.depth = attachmentCreateInfo.dimensions.z;
			imageCreateInfo.mipLevels = attachmentCreateInfo.mipLevels;
			imageCreateInfo.arrayLayers = attachmentCreateInfo.layerCount;
			imageCreateInfo.samples = attachmentCreateInfo.sampleCount;
			imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
			imageCreateInfo.usage = attachmentCreateInfo.usage;
			imageCreateInfo.flags |= attachmentCreateInfo.viewType == VK_IMAGE_VIEW_TYPE_CUBE ? VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT : 0;
			VkMemoryAllocateInfo memAlloc = {};
			memAlloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
			VkMemoryRequirements memReqs;

			VK_ASSERT(vkCreateImage(device.device(), &imageCreateInfo, nullptr, &image), "Failed to create image!");
			vkGetImageMemoryRequirements(device.device(), image, &memReqs);
			memSize = memAlloc.allocationSize = memReqs.size;
			memAlloc.memoryTypeIndex = device.findMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

			if (vkAllocateMemory(device.device(), &memAlloc, nullptr, &imageMemory) != VK_SUCCESS) {
				SHARD3D_ERROR("Ran out of memory!");
				return;
			}

			VK_ASSERT(vkBindImageMemory(device.device(), image, imageMemory, 0), "Failed to bind memory!");
			VkImageViewCreateInfo imageViewCreateInfo = {};
			imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
			imageViewCreateInfo.viewType = attachmentCreateInfo.viewType;
			imageViewCreateInfo.format = attachmentCreateInfo.format;
			subresourceRange.aspectMask = attachmentCreateInfo.imageAspect;
			subresourceRange.baseMipLevel = 0;
			subresourceRange.levelCount = attachmentCreateInfo.mipLevels;
			subresourceRange.baseArrayLayer = 0;
			subresourceRange.layerCount = attachmentCreateInfo.layerCount;
			imageViewCreateInfo.subresourceRange = subresourceRange;
			imageViewCreateInfo.image = image;
			switch (attachmentCreateInfo.format) {
			case(VK_FORMAT_R8_UNORM):
			case(VK_FORMAT_R16_UNORM):
			case(VK_FORMAT_R16_SFLOAT):
			case(VK_FORMAT_D16_UNORM):
			case(VK_FORMAT_D16_UNORM_S8_UINT):
			case(VK_FORMAT_D32_SFLOAT):
			case(VK_FORMAT_D32_SFLOAT_S8_UINT):
				imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_R;
				imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_R;
				imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_R;
			}

			VK_ASSERT(vkCreateImageView(device.device(), &imageViewCreateInfo, nullptr, &imageView), "Failed to create image view!");

		} else {
			image = attachmentCreateInfo.inheritVkImage;
		}
		switch (attachmentCreateInfo.format) {
		case(VK_FORMAT_D16_UNORM_S8_UINT):
		case(VK_FORMAT_D32_SFLOAT_S8_UINT):
			VkImageViewCreateInfo imageViewStencilCreateInfo = {};
			imageViewStencilCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
			imageViewStencilCreateInfo.viewType = attachmentCreateInfo.viewType;
			imageViewStencilCreateInfo.format = attachmentCreateInfo.format;
			subresourceRange.aspectMask = VK_IMAGE_ASPECT_STENCIL_BIT;
			subresourceRange.baseMipLevel = 0;
			subresourceRange.levelCount = attachmentCreateInfo.mipLevels;
			subresourceRange.baseArrayLayer = 0;
			subresourceRange.layerCount = attachmentCreateInfo.layerCount;
			imageViewStencilCreateInfo.subresourceRange = subresourceRange;
			imageViewStencilCreateInfo.image = image;
			VK_ASSERT(vkCreateImageView(device.device(), &imageViewStencilCreateInfo, nullptr, &imageViewStencilBuffer), "Failed to create image view!");

			break;
		}
		
		renderImageViews.resize(attachmentCreateInfo.overrideDirectLayerWrite ? 1 : attachmentCreateInfo.layerCount);
		renderSubresourceRanges.resize(attachmentCreateInfo.overrideDirectLayerWrite ? 1 : attachmentCreateInfo.layerCount);
		for (int i = 0; i < renderImageViews.size(); i++) {
			renderImageViews[i].resize(attachmentCreateInfo.mipLevels);
			renderSubresourceRanges[i].resize(attachmentCreateInfo.mipLevels);
			for (int j = 0; j < renderImageViews[i].size(); j++) {
				VkImageViewCreateInfo subImageViewCreateInfo = {};
				subImageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
				subImageViewCreateInfo.viewType = attachmentCreateInfo.overrideDirectLayerWrite ? VK_IMAGE_VIEW_TYPE_2D_ARRAY : VK_IMAGE_VIEW_TYPE_2D;
				subImageViewCreateInfo.format = attachmentCreateInfo.format;
				renderSubresourceRanges[i][j].aspectMask = attachmentCreateInfo.imageAspect;
				renderSubresourceRanges[i][j].baseMipLevel = j;
				renderSubresourceRanges[i][j].levelCount = 1;
				renderSubresourceRanges[i][j].baseArrayLayer = i;
				renderSubresourceRanges[i][j].layerCount = attachmentCreateInfo.overrideDirectLayerWrite ? attachmentCreateInfo.layerCount : 1;
				subImageViewCreateInfo.subresourceRange = renderSubresourceRanges[i][j];
				subImageViewCreateInfo.image = image;

				VK_ASSERT(vkCreateImageView(device.device(), &subImageViewCreateInfo, nullptr, &renderImageViews[i][j]), "Failed to create render image view!");
			}
		}
	

		if (!inheritsVkImage) {
			if (attachmentCreateInfo.usage & VK_IMAGE_USAGE_SAMPLED_BIT) {
				// Create sampler to sample from the attachment in the fragment shader
				VkSamplerCreateInfo samplerInfo = {};
				samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
				samplerInfo.magFilter = attachmentCreateInfo.linearFiltering ? VK_FILTER_LINEAR : VK_FILTER_NEAREST;
				samplerInfo.minFilter = attachmentCreateInfo.linearFiltering ? VK_FILTER_LINEAR : VK_FILTER_NEAREST;
				samplerInfo.mipmapMode = attachmentCreateInfo.linearFiltering ? VK_SAMPLER_MIPMAP_MODE_LINEAR : VK_SAMPLER_MIPMAP_MODE_NEAREST;
				samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
				samplerInfo.mipLodBias = 0.0f;
				samplerInfo.maxAnisotropy = 1.0f;
				samplerInfo.minLod = 0.0f;
				samplerInfo.maxLod = static_cast<float>(attachmentCreateInfo.mipLevels - 1);
				samplerInfo.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
				samplerInfo.addressModeV = samplerInfo.addressModeU;
				samplerInfo.addressModeW = samplerInfo.addressModeU;
				samplerInfo.compareEnable = attachmentCreateInfo.enableSamplerPCF;
				samplerInfo.compareOp = VK_COMPARE_OP_LESS;

				VK_ASSERT(vkCreateSampler(device.device(), &samplerInfo, nullptr, &sampler), "Failed to create sampler");
			}
		}
		dimensions = { attachmentCreateInfo.dimensions.x, attachmentCreateInfo.dimensions.y, attachmentCreateInfo.dimensions.z };
	}
	void S3DRenderTarget::destroy() {
 		for (int i = 0; i < renderImageViews.size(); i++) {
			for (int j = 0; j < renderImageViews[i].size(); j++) {
				vkDestroyImageView(engineDevice.device(), renderImageViews[i][j], nullptr);
			}
		}
		if (!inheritsVkImage) {
			if (sampler) {
				vkDestroySampler(engineDevice.device(), sampler, nullptr);
			}
			vkDestroyImageView(engineDevice.device(), imageView, nullptr);
			vkDestroyImage(engineDevice.device(), image, nullptr);
			vkFreeMemory(engineDevice.device(), imageMemory, nullptr);
		}
	}

	void S3DRenderTarget::resize(glm::ivec3 newDimensions) {
		if (newDimensions == attachmentDescription.dimensions) return; // in case already been resized
		destroy();
		attachmentDescription.dimensions = newDimensions;
		create(engineDevice, attachmentDescription);
	}	
}
