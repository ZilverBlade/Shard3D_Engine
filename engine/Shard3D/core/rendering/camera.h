#pragma once
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE

#include <glm/glm.hpp>
#include "postfx.h"
#include "../asset/primitives.h"

namespace Shard3D {
	class S3DCamera {
	public:
		void setOrthographicProjection(float left, float right, float top, float bottom, float zNear, float zFar, bool reverseDepth = false);
		void setPerspectiveProjection(float fovy, float aspect, float zNear, float zFar, bool reverseDepth = false, bool infiniteFarZ = false);

		void setViewDirection(glm::vec3 position, glm::vec3 direction, glm::vec3 up = glm::vec3(0.f, 1.f, 0.f));
		void setViewTarget(glm::vec3 position, glm::vec3 target, glm::vec3 up = glm::vec3(0.f, 1.f, 0.f));
		void setViewYXZ(glm::vec3 position, glm::vec3 rotation);
		void setViewYXZ(glm::mat4 transformMatrix);
		void setViewMatrix(glm::mat4 viewMatrix);

		void recalculateFrustumPlanes();

		void flipProjectionUpsideDown();

		bool isInFrustum(const SphereBounds& sphere, glm::vec4 position) const;
		bool isInFrustum(const VolumePoints& points) const;
		bool isInFrustum(const VolumeBounds& aabb) const;

		const glm::mat4& getProjection() const { return projectionMatrix; }
		const glm::mat4& getInverseProjection() const { return inverseProjectionMatrix; }
		const glm::mat4& getView() const { return viewMatrix; }
		const glm::mat4& getInverseView() const { return inverseViewMatrix; }
		const glm::mat4& getProjectionView() const { return projectionViewMatrix; }
		const glm::vec3 getPosition() const { return glm::vec3(inverseViewMatrix[3]); }

		const std::array<glm::vec4, 6>& getFrustumPlanes() {
			return frustumPlanes;
		}

		PostFXData postfx{};

		const float getNear() const {
			return nearZ;
		}
		const float getFar() const {
			return farZ;
		}
	private:
		glm::mat4 projectionMatrix{ 1.f };
		glm::mat4 inverseProjectionMatrix{ 1.f };
		glm::mat4 viewMatrix{ 1.f };
		glm::mat4 inverseViewMatrix{ 1.f };

		glm::mat4 projectionViewMatrix{ 1.f };

		std::array<glm::vec4, 6> frustumPlanes{};

		float nearZ, farZ;
	};
}