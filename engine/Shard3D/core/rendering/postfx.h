#pragma once
#include "../../s3dstd.h"
namespace Shard3D {
	struct PostFXData {
		struct ColorGrading {
			float contrast = 1.0f;
			float saturation = 1.0f;
			float gain = 0.0f;
			float temperature = 5200.f; // Kelvin
			float hueShift = 0.f;
			glm::vec3 shadows = glm::vec3(0.5, 0.5, 0.5);
			glm::vec3 midtones = glm::vec3(0.5, 0.5, 0.5);
			glm::vec3 highlights = glm::vec3(0.5, 0.5, 0.5);
		//	AssetID lut = AssetID::null();
		} grading;
		struct HDR {
			float exposure = 1.0f;
			float lim = 0.5f;
			enum class ToneMappingAlgorithm : int {
				NONE = 0,
				REINHARD_TONE_MAPPING = 1,
				REINHARD_EXTENDED_TONE_MAPPING = 2,
				EXPONENTIAL_TONE_MAPPING = 3,
				REINHARD_LUMA_TONE_MAPPING = 4,
				HABLE_TONE_MAPPING = 5,
				ACES_TONE_MAPPING = 6
			} toneMappingAlgorithm = ToneMappingAlgorithm::REINHARD_LUMA_TONE_MAPPING;
			bool exposureInfluence = true;
			bool limInfluence = true;
			bool toneMappingAlgorithmInfluence = true;
		} hdr;
		struct Bloom {
			float threshold = 1.0f;
			float strength = 1.0f;
			float knee = 0.55f; // 0-1
			bool enable = true;
			bool threshholdInfluence = true;
			bool strengthInfluence = true;
			bool kneeInfluence = true;
			bool enableInfluence = false;
		} bloom;
		struct MotionBlur {
			float targetFramerate = 60.0f; // effective shutter speed
			int maxLength = 20;
			bool enable = true;
			bool targetFramerateInfluence = true;
			bool maxLengthInfluence = true;
			bool enableInfluence = false;
		} motionBlur;

		// RENDERER FEATURES

		struct SSAO {
			float bias = 0.01f; // 0.000 - 0.100
			float radius = 0.3f; // 0 - 1
			float intensity = 1.0f;
			float power = 2.0f;
			bool enable = true;
			bool influence = false;
		} ssao;
		struct LightPropagationVolume {
			glm::ivec3 resolution = { 32, 32, 32 };
			glm::vec3 extent = { 64, 64, 64 };
			uint32_t numCascades = 1; // max 3
			float cascadeLogBase = 2.0f;
			float fadeRatio = 4.0f;
			uint32_t numPropagations = 1;
			glm::vec4 boost = glm::vec4(1.f, 1.f, 1.f, 100.f);
			float temporalBlend = 8.0f;
			bool fixed = false;
			glm::vec3 fixedPosition = { 0.0f, 0.0f, 0.0f };
			bool enable = true;
			bool influence = false;
		} lightPropagationVolume;
		struct Mist {
			float mieScattering = 0.924f;
			float constantScattering = 0.120f;
			glm::vec3 tint{1.f, 1.f, 1.f};
			bool enable = false;
			bool influence = false;
		} mist;
	};
}