#include "../../s3dpch.h" 
#include "swap_chain.h"
#include "../vulkan_api/device.h"
#include "../misc/graphics_settings.h"
#include <vulkan/vk_enum_string_helper.h>

namespace Shard3D {
	S3DSwapChain::S3DSwapChain(S3DDevice& device, S3DWindow& window, VkExtent2D windowExtent, S3DSwapChain* oldSwapChain) : engineDevice(device), engineWindow(window), swapChainExtent(windowExtent), oldSwapChain(oldSwapChain){
		init();
	}
	S3DSwapChain::~S3DSwapChain() {
		for (size_t i = 0; i < imageCount; i++) {
			vkDestroySemaphore(engineDevice.device(), renderFinishedSemaphores[i], nullptr);
			vkDestroySemaphore(engineDevice.device(), imageAvailableSemaphores[i], nullptr);
			vkDestroyFence(engineDevice.device(), inFlightFences[i], nullptr);
		}
		for (VkFramebuffer framebuffer : swapChainFramebuffers) {
			vkDestroyFramebuffer(engineDevice.device(), framebuffer, nullptr);
		}
		for (VkImageView imageView : swapChainImageViews) {
			vkDestroyImageView(engineDevice.device(), imageView, nullptr);
		}
		vkDestroyRenderPass(engineDevice.device(), swapChainRenderPass, nullptr);
		vkDestroySwapchainKHR(engineDevice.device(), swapChain, nullptr);
	}
	void S3DSwapChain::init() {
		SwapChainSupportDetails details = engineDevice.getSwapChainSupport(engineWindow.getSurface());
		setImageCount(details.capabilities);
		VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(details.formats);
		swapChainImageFormat = surfaceFormat.format;
		createSwapChain(details.capabilities, getSwapChainExtent(), surfaceFormat, chooseSwapPresentMode(details.presentModes));

		createImageViews();
		createRenderPass();
		createFramebuffers();
		createSyncObjects();
	}
	VkResult S3DSwapChain::acquireNextImage(uint32_t* imageIndex) {
	//	SHARD3D_INFO("waiting for fence {0}", (void*)inFlightFences[currentFrame]);
		vkWaitForFences(
			engineDevice.device(),
			1,
			&inFlightFences[currentFrame],
			VK_TRUE,
			UINT64_MAX
		);
		VkResult result = vkAcquireNextImageKHR(
			engineDevice.device(),
			swapChain,
			UINT64_MAX,
			imageAvailableSemaphores[currentFrame],  // must be a not signaled enginemaphore
			VK_NULL_HANDLE,
			imageIndex
		);
		return result;
	}
	VkSubmitInfo S3DSwapChain::getSubmitInfo(uint32_t* imageIndex) {
		if (imagesInFlight[*imageIndex] != VK_NULL_HANDLE) {
			vkWaitForFences(engineDevice.device(), 1, &imagesInFlight[*imageIndex], VK_TRUE, UINT64_MAX);
		}
		imagesInFlight[*imageIndex] = inFlightFences[currentFrame];
		VkSubmitInfo submitInfo = {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

		static VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
		submitInfo.waitSemaphoreCount = 1;
		submitInfo.pWaitSemaphores = &imageAvailableSemaphores[currentFrame];
		submitInfo.pWaitDstStageMask = waitStages;

		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = &renderFinishedSemaphores[currentFrame];

		vkResetFences(engineDevice.device(), 1, &inFlightFences[currentFrame]);
		return submitInfo;
	}
	VkResult S3DSwapChain::present(VkQueue queue, uint32_t* imageIndex) {
		VkPresentInfoKHR presentInfo = {};
		presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		   
		presentInfo.waitSemaphoreCount = 1;
		presentInfo.pWaitSemaphores = &renderFinishedSemaphores[currentFrame];
		   
		presentInfo.swapchainCount = 1;
		presentInfo.pSwapchains = &swapChain;

		presentInfo.pImageIndices = imageIndex;

		VkResult result = vkQueuePresentKHR(queue, &presentInfo);
		currentFrame = (currentFrame + 1) % ProjectSystem::getGraphicsSettings().renderer.framesInFlight;
		return result;
	}
	void S3DSwapChain::createSwapChain(const VkSurfaceCapabilitiesKHR& capabilities, VkExtent2D extent, VkSurfaceFormatKHR surfaceFormat, VkPresentModeKHR presentMode) {
		VkSwapchainCreateInfoKHR createInfo{};
		createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
		createInfo.surface = engineWindow.getSurface();
		createInfo.minImageCount = capabilities.maxImageCount > 0 &&
			imageCount > capabilities.maxImageCount ? capabilities.maxImageCount : imageCount;
		createInfo.imageFormat = surfaceFormat.format;
		createInfo.imageColorSpace = surfaceFormat.colorSpace;
		createInfo.imageExtent = extent;
		createInfo.imageArrayLayers = 1;
		createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
		createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
		createInfo.preTransform = capabilities.currentTransform;
		createInfo.presentMode = presentMode;
		createInfo.clipped = VK_TRUE;
		if (oldSwapChain) {
			createInfo.oldSwapchain = oldSwapChain->swapChain;
		} else {
			createInfo.oldSwapchain = nullptr;
		}
		QueueFamilyIndices indices = engineDevice.findPhysicalQueueFamilies();
		uint32_t queueFamilyIndices[] = { indices.graphicsFamily, indices.presentFamily };

		if (indices.graphicsFamily != indices.presentFamily) {
			createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
			createInfo.queueFamilyIndexCount = 2;
			createInfo.pQueueFamilyIndices = queueFamilyIndices;
		} else {
			createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
			createInfo.queueFamilyIndexCount = 0;      // Optional
			createInfo.pQueueFamilyIndices = nullptr;  // Optional
		}
		VK_ASSERT(vkCreateSwapchainKHR(engineDevice.device(), &createInfo, nullptr, &swapChain), "failed to create swap chain!");

		swapChainImageFormat = surfaceFormat.format;
		swapChainExtent = extent;
	}
	void S3DSwapChain::createImageViews() {
		std::vector<VkImage> swapChainImages{};
		swapChainImages.resize(imageCount);
		swapChainImageViews.resize(imageCount);
		vkGetSwapchainImagesKHR(engineDevice.device(), swapChain, &imageCount, swapChainImages.data());

		for (int i = 0; i < imageCount; i++) {
			VkImageViewCreateInfo imageViewCreateInfo = {};
			imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
			imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
			imageViewCreateInfo.format = swapChainImageFormat;
			VkImageSubresourceRange subresourceRange{};
			subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			subresourceRange.baseMipLevel = 0;
			subresourceRange.levelCount = 1;
			subresourceRange.baseArrayLayer = 0;
			subresourceRange.layerCount = 1;
			imageViewCreateInfo.subresourceRange = subresourceRange;
			imageViewCreateInfo.image = swapChainImages[i];
			VK_ASSERT(vkCreateImageView(engineDevice.device(), &imageViewCreateInfo, nullptr, &swapChainImageViews[i]), "failed to create texture image view!");
		}
	}
	void S3DSwapChain::createRenderPass() {
		VkAttachmentDescription colorAttachment{};
	
		colorAttachment.format = swapChainImageFormat;
		colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
		colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

		VkAttachmentReference colorAttachmentRef = { 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };

		VkSubpassDescription subpassDescription{};
		subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subpassDescription.pColorAttachments = &colorAttachmentRef;
		subpassDescription.colorAttachmentCount = 1;
		subpassDescription.pDepthStencilAttachment = nullptr;
		subpassDescription.pResolveAttachments = nullptr;

		// Uengine subpass dependencies for layout transitions

		VkSubpassDependency dependency = {}; 
		dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
		dependency.srcAccessMask = 0;
		dependency.srcStageMask =
			VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
		dependency.dstSubpass = 0;
		dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
		dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

		// Create the actual renderpass
		VkRenderPassCreateInfo renderPassInfo{};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		renderPassInfo.attachmentCount = 1;
		renderPassInfo.pAttachments = &colorAttachment;
		renderPassInfo.subpassCount = 1;
		renderPassInfo.pSubpasses = &subpassDescription;
		renderPassInfo.dependencyCount = 1;
		renderPassInfo.pDependencies = &dependency;

		VK_ASSERT(vkCreateRenderPass(engineDevice.device(), &renderPassInfo, nullptr, &swapChainRenderPass), "failed to create render pass!");
	}
	void S3DSwapChain::createFramebuffers() {
		swapChainFramebuffers.resize(imageCount);
		for (int i = 0; i < imageCount; i++) {
			VkFramebufferCreateInfo framebufferInfo{};
			framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			framebufferInfo.pNext = nullptr;
			framebufferInfo.flags = 0;
			framebufferInfo.renderPass = swapChainRenderPass;
			framebufferInfo.attachmentCount = 1;
			framebufferInfo.pAttachments = &swapChainImageViews[i];
			framebufferInfo.width = swapChainExtent.width;
			framebufferInfo.height = swapChainExtent.height;
			framebufferInfo.layers = 1;
			VK_ASSERT(vkCreateFramebuffer(
				engineDevice.device(),
				&framebufferInfo,
				nullptr,
				&swapChainFramebuffers[i]),
				"failed to create framebuffer!");
		}
	}
	void S3DSwapChain::createSyncObjects() {
		imageAvailableSemaphores.resize(imageCount);
		renderFinishedSemaphores.resize(imageCount);
		inFlightFences.resize(imageCount);
		imagesInFlight.resize(imageCount, nullptr);

		VkSemaphoreCreateInfo semaphoreInfo = {};
		semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

		VkFenceCreateInfo fenceInfo = {};
		fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
		fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

		for (size_t i = 0; i < imageCount; i++) {
			if (vkCreateSemaphore(engineDevice.device(), &semaphoreInfo, nullptr, &imageAvailableSemaphores[i]) != VK_SUCCESS ||
				vkCreateSemaphore(engineDevice.device(), &semaphoreInfo, nullptr, &renderFinishedSemaphores[i]) != VK_SUCCESS ||
				vkCreateFence(engineDevice.device(), &fenceInfo, nullptr, &inFlightFences[i]) != VK_SUCCESS) {
				SHARD3D_FATAL("failed to create synchronization objects for a frame!");
			}
		}
	
	}

	void S3DSwapChain::setImageCount(const VkSurfaceCapabilitiesKHR& capabilities) {
		imageCount = std::clamp(ProjectSystem::getGraphicsSettings().renderer.framesInFlight, capabilities.minImageCount, capabilities.maxImageCount);
	}


	VkSurfaceFormatKHR S3DSwapChain::chooseSwapSurfaceFormat(
		const std::vector<VkSurfaceFormatKHR>& availableFormats) {

		VkSurfaceFormatKHR finalFormat{};
		for (const auto& availableFormat : availableFormats) {
			if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
				finalFormat = availableFormat;
			}
		}

		return finalFormat;

		SHARD3D_WARN("VK_FORMAT_B8G8R8A8_UNORM not available!");
		return availableFormats[0];
	}

	VkPresentModeKHR S3DSwapChain::chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes) {
		if (ProjectSystem::getGraphicsSettings().renderer.verticalSync == false) {
			for (const auto& availablePresentMode : availablePresentModes) {
				if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
					SHARD3D_INFO("Present mode: Mailbox");
					return VK_PRESENT_MODE_MAILBOX_KHR;
				} else if (availablePresentMode == VK_PRESENT_MODE_IMMEDIATE_KHR) {
					SHARD3D_INFO("Present mode: Immediate");
					return VK_PRESENT_MODE_IMMEDIATE_KHR;
				}
			}
		} else {
			SHARD3D_INFO("Present mode: V-Sync");
			return VK_PRESENT_MODE_FIFO_KHR;
		}
		SHARD3D_WARN("No present mode chosen!");
	}

	VkExtent2D S3DSwapChain::chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities) {
		if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
			return capabilities.currentExtent;
		} else {
			VkExtent2D actualExtent = swapChainExtent;
			actualExtent.width = std::max(
				capabilities.minImageExtent.width,
				std::min(capabilities.maxImageExtent.width, actualExtent.width));
			actualExtent.height = std::max(
				capabilities.minImageExtent.height,
				std::min(capabilities.maxImageExtent.height, actualExtent.height));

			return actualExtent;
		}
	}
}