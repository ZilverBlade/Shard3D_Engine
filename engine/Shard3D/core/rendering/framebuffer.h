#pragma once
#include "render_target.h"

namespace Shard3D {
	class S3DRenderPass;
	class S3DFramebuffer {
	public:
		S3DFramebuffer(S3DDevice& device, S3DRenderPass* renderPass, const std::vector<S3DRenderTarget*>& attachments, uint32_t layer = 0, uint32_t mipLevel = 0);
		S3DFramebuffer(S3DDevice& device, S3DRenderPass* renderPass, bool imageless, glm::ivec3 imagelessDimensions);
		~S3DFramebuffer();

		S3DFramebuffer(const S3DFramebuffer&) = delete;
		S3DFramebuffer& operator=(const S3DFramebuffer&) = delete;
		S3DFramebuffer(S3DFramebuffer&&) = delete;
		S3DFramebuffer& operator=(S3DFramebuffer&&) = delete;

		glm::ivec3 getDimensions() { return { width, height, depth }; }
		VkFramebuffer getFramebuffer() { return framebuffer; }
		void resize(glm::ivec3 newDimensions, S3DRenderPass* renderPass);
	private:
		void create(S3DDevice& device, VkRenderPass renderPass, const std::vector<S3DRenderTarget*>& attachments);
		void createImageless(S3DDevice& device, VkRenderPass renderPass);
		void destroy();

		uint32_t width{};
		uint32_t height{};
		uint32_t depth{};

		VkFramebuffer framebuffer{};
		uint32_t useMipLevel = 0;
		uint32_t useLayer = 0;

		std::vector<S3DRenderTarget*> attachments;
		S3DDevice& engineDevice;
	};
}