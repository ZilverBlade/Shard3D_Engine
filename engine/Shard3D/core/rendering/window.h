#pragma once

#define VK_NO_PROTOTYPES
#include <volk.h>
//#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include "../../events/event.h"

#include "../../core/vulkan_api/device.h"

namespace Shard3D {
	class S3DWindow {
	public:
		enum WindowType {
			Windowed = 0,
			Borderless = 1,
			Fullscreen = 2
		};
	private:
		using EventCallbackFunc = std::function<void(Events::Event&)>;
		friend class GraphicsSettings;
	public:
		S3DWindow(S3DDevice& device, int w, int h, std::string name);
		~S3DWindow();
		
		DELETE_COPY(S3DWindow);

		void setEventCallback(const EventCallbackFunc& cbf) { eventCallback = cbf; }
		
		bool shouldClose() { return glfwWindowShouldClose(window); }
		VkExtent2D getExtent() { return { static_cast<uint32_t>(width), static_cast<uint32_t>(height) }; }
		bool wasWindowResized() { return framebufferResized; }
		void resetWindowResizedFlag() { framebufferResized = false; }
		bool isWindowFocussed() { return focussed; }
		GLFWwindow* getGLFWwindow() { return window; }

		void createWindowSurface(VkInstance instance, VkSurfaceKHR* surface);
		void setWindowMode(WindowType winType);

		bool isKeyDown(int keyCode) { return glfwGetKey(window, keyCode) == GLFW_PRESS; }
		bool isMouseButtonDown(int button) { return glfwGetMouseButton(window, button) == GLFW_PRESS; }

		inline int getWindowWidth() { return width; }
		inline int getWindowHeight() { return height; }

		inline VkSurfaceKHR getSurface() { return surface; }
	private:
		void initWindow();

		int width;
		int height;

		bool focussed = true;
		bool framebufferResized = false;

		std::string windowName;
		GLFWwindow* window;
		VkSurfaceKHR surface;
		GLFWmonitor* monitor = nullptr;
		S3DDevice& device;
		EventCallbackFunc eventCallback;

		// CALLBACKS
		static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void charCallback(GLFWwindow* window, unsigned int c);
		static void mouseBtnCallback(GLFWwindow* window, int button, int action, int mods);
		static void mouseMotionCallback(GLFWwindow* window, double xpos, double ypos);
		static void mouseScrollCallback(GLFWwindow* window, double xoffset, double yoffset);
		static void framebufferResizeCallback(GLFWwindow* window, int width, int height);
		static void windowFocusCallback(GLFWwindow* window, int focussed);
	};
}