#pragma once
#include "framebuffer.h"
#include "render_target.h"

namespace Shard3D {
	struct AttachmentClearColor {
		VkClearColorValue color{{0.f, 0.f, 0.f, 1.f}};
		VkClearDepthStencilValue depth{ 1.f, 0 };
	};
	struct AttachmentInfo {
		S3DRenderTarget* framebufferAttachment;
		VkAttachmentLoadOp loadOp;
		VkAttachmentStoreOp storeOp;
		AttachmentClearColor clear = AttachmentClearColor{};
		VkAttachmentLoadOp stencilLoadOp;
		VkAttachmentStoreOp stencilStoreOp;
	};

	class  S3DRenderPass {
	public:
		S3DRenderPass(S3DDevice& device, const std::vector<AttachmentInfo>& attachments);
		~S3DRenderPass();

		S3DRenderPass(const S3DRenderPass&) = delete;
		S3DRenderPass& operator=(const S3DRenderPass&) = delete;
		S3DRenderPass(S3DRenderPass&&) = delete;
		S3DRenderPass& operator=(S3DRenderPass&&) = delete;

		void beginRenderPass(VkCommandBuffer commandbuffer, S3DFramebuffer* framebuffer, uint32_t viewports = 1);
		void endRenderPass(VkCommandBuffer commandbuffer);

		void setViewportSize(uint32_t w, uint32_t h) { width = w; height = h; }

		VkRenderPass getRenderPass() { return renderpass; }
	private:
		uint32_t width{};
		uint32_t height{};

		S3DDevice& engineDevice;

		std::vector<VkClearValue> clearValues{};
		VkRenderPass renderpass{};
	};
}