#pragma once
#define GLM_FORCE_QUAT_DATA_WXYZ
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <algorithm>
namespace Shard3D {

	struct Quaternion {
		Quaternion() = default;
		Quaternion(const Quaternion&) = default;
		Quaternion& operator=(const Quaternion&) = default;
		Quaternion(float vw, float vx, float vy, float vz) : w(vw), x(vx), y(vy), z(vz) {}

		const Quaternion& operator/(float rhs) const {
			float inv = 1.0f / rhs;
			return Quaternion(w * inv, x * inv, y * inv, z * inv);
		}
		const Quaternion& operator*(float v) const {
			return Quaternion(v* w, v * x, v * y, v * z);
		}
		const Quaternion& operator+(const Quaternion& v) const {
			return Quaternion(v.w + w, v.w + x, v.w + y, v.w + z);
		}
		static Quaternion slerp(const Quaternion& lhs, const Quaternion& rhs, float alpha) {
			Quaternion slerped = rhs;
			float cosTheta = lhs.w * rhs.w + lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;

			if (cosTheta < 0.0f) {
				slerped = Quaternion(-rhs.w, -rhs.x, -rhs.y, -rhs.z);
				cosTheta = -cosTheta;
			}
			if (cosTheta > 1.0f - std::numeric_limits<float>::epsilon()) {
				return Quaternion(
					glm::mix(lhs.w, rhs.w, alpha),
					glm::mix(lhs.x, rhs.x, alpha),
					glm::mix(lhs.y, rhs.y, alpha),
					glm::mix(lhs.z, rhs.z, alpha)
				);
			} else {
				float angle = acos(cosTheta);
				return (lhs * sin((1.0f - alpha) * angle) + slerped * sin(alpha * angle)) / sin(angle);
			}
		}
		inline glm::mat3 rotationMatrix() {
			glm::mat3 result;
			float qxx = x * x;
			float qyy = y * y;
			float qzz = z * z;
			float qxz = x * z;
			float qxy = x * y;
			float qyz = y * z;
			float qwx = w * x;
			float qwy = w * y;
			float qwz = w * z;

			result[0][0] = 1.0 - 2.0 * (qyy + qzz);
			result[0][1] = 2.0 * (qxy + qwz);
			result[0][2] = 2.0 * (qxz - qwy);

			result[1][0] = 2.0 * (qxy - qwz);
			result[1][1] = 1.0 - 2.0 * (qxx + qzz);
			result[1][2] = 2.0 * (qyz + qwx);

			result[2][0] = 2.0 * (qxz + qwy);
			result[2][1] = 2.0 * (qyz - qwx);
			result[2][2] = 1.0 - 2.0 * (qxx + qyy);

			return result;
		}
		float w = 1.0f, x = 0.0f, y = 0.0f, z = 0.0f;
	};

	struct QuatTransform {
		void setTranslation(glm::vec3 _t) {
			translation = glm::vec3(_t.x, _t.y, _t.z);
		}
		void setRotationQuat(Quaternion _e) {
			rotationQuat = _e;
		}
		void setScale(glm::vec3 _s) {
			scale = glm::vec3(_s.x, _s.y, _s.z);
		}

		glm::vec3 getTranslation() {
			return glm::vec3(translation.x, translation.y, translation.z);
		}
		Quaternion getRotation() {
			return rotationQuat;
		}
		glm::vec3 getScale() {
			return glm::vec3(scale.x, scale.y, scale.z);
		}

		inline glm::mat4 transformMatrix() {
			glm::mat4 transform = glm::toMat4((glm::quat&)rotationQuat);// rotationQuat.rotationMatrix();
			transform[3] = glm::vec4(translation, 1.f);
			transform[0] *= scale.x;
			transform[1] *= scale.y;
			transform[2] *= scale.z;
			return transform;
		}
	private:
		glm::vec3 translation { 0.f, 0.f, 0.f };
		Quaternion rotationQuat{ 1.f, 0.f, 0.f, 0.f };
		glm::vec3 scale { 1.f, 1.f, 1.f };
		glm::mat4 transform{1.f};
	};
	struct TransformMath {
		// https://en.wikipedia.org/wiki/Euler_angles#Rotation_matrix

			// Matrix corresponds to translate * Rz * Rx * Ry * scale transformation
	// Rotation convention = ZXY tait-bryan angles
		static inline glm::mat4 ZXY(glm::vec3 translation, glm::vec3 rotation, glm::vec3 scale) {
			const float c3 = glm::cos(rotation.z);
			const float s3 = glm::sin(rotation.z);
			const float c2 = glm::cos(rotation.x);
			const float s2 = glm::sin(rotation.x);
			const float c1 = glm::cos(rotation.y);
			const float s1 = glm::sin(rotation.y);

			return glm::mat4(
				{
					scale.x * (-c2 * s1),
					scale.x * (c1 * c2),
					scale.x * (s2),
					0.0f,
				},
				{
					scale.y * (c1 * s3 + c3 * s1 * s2),
					scale.y * (s1 * s3 - c1 * c3 * s2),
					scale.y * (c2 * c3),
					0.0f,
				},
				{
					scale.z * (c1 * c3 - s1 * s2 * s3),
					scale.z * (c3 * s1 + c1 * s2 * s3),
					scale.z * (-c2 * s3),
					0.0f,
				},
				{ translation.x, translation.y, translation.z, 1.0f }
			);
		}
		// Matrix corresponds to translate * Ry * Rx * Rz * scale transformation
	// Rotation convention = YXZ tait-bryan angles
		static inline glm::mat4 YXZ(glm::vec3 translation, glm::vec3 rotation, glm::vec3 scale) {
			const float c3 = glm::cos(rotation.z);
			const float s3 = glm::sin(rotation.z);
			const float c2 = glm::cos(rotation.x);
			const float s2 = glm::sin(rotation.x);
			const float c1 = glm::cos(rotation.y);
			const float s1 = glm::sin(rotation.y);
			return glm::mat4(
				{
					scale.x * (c1 * c3 + s1 * s2 * s3),
					scale.x * (c2 * s3),
					scale.x * (c1 * s2 * s3 - c3 * s1),
					0.f
				},	
				{	
					scale.y * (c3 * s1 * s2 - c1 * s3),
					scale.y * (c2 * c3),
					scale.y * (c1 * c3 * s2 + s1 * s3),
					0.f
				},	
				{	
					scale.z * (c2 * s1),
					scale.z * (-s2),
					scale.z * (c1 * c2),
					0.f
				},
				{ translation.x, translation.y, translation.z, 1.0f }
			);

		}
		static inline glm::mat3 YXZ(glm::vec3 rotation) {
			const float c3 = glm::cos(rotation.z);
			const float s3 = glm::sin(rotation.z);
			const float c2 = glm::cos(rotation.x);
			const float s2 = glm::sin(rotation.x);
			const float c1 = glm::cos(rotation.y);
			const float s1 = glm::sin(rotation.y);
			return glm::mat3(
				{
					(c1 * c3 + s1 * s2 * s3),
					(c2 * s3),
					(c1 * s2 * s3 - c3 * s1)
				},
				{
					(c3 * s1 * s2 - c1 * s3),
					(c2 * c3),
					(c1 * c3 * s2 + s1 * s3)
				},
				{
					(c2 * s1),
					(-s2),
					(c1 * c2)
				}
			);

		}

		static inline glm::vec3 decomposeEulerYXZ(const glm::mat3& rotation) {
			const float Y = atan2(rotation[2][0], rotation[2][2]);
			const float X = atan2(-rotation[2][1], sqrt(std::max(1.0f - rotation[2][1] * rotation[2][1], 0.0f)));
			const float Z = atan2(rotation[0][1], rotation[1][1]);
			return { X, Y, Z };
		}

		static inline void TransformMath::decompose(const glm::mat4& transform, glm::vec3* outTranslation, glm::vec3* outRotationEulerYXZ, glm::vec3* outScale) {
			// From glm::decompose in matrix_decompose.inl
			using namespace glm;
			using T = float;

			mat4 LocalMatrix(transform);

			if (epsilonEqual(LocalMatrix[3][3], static_cast<float>(0), epsilon<T>()))
				return;

			// First, isolate perspective.  This is the messiest.
			if (
				epsilonNotEqual(LocalMatrix[0][3], static_cast<T>(0), epsilon<T>()) ||
				epsilonNotEqual(LocalMatrix[1][3], static_cast<T>(0), epsilon<T>()) ||
				epsilonNotEqual(LocalMatrix[2][3], static_cast<T>(0), epsilon<T>())) {
				// Clear the perspective partition
				LocalMatrix[0][3] = LocalMatrix[1][3] = LocalMatrix[2][3] = static_cast<T>(0);
				LocalMatrix[3][3] = static_cast<T>(1);
			}

			// Next take care of translation (easy).
			*outTranslation = vec3(LocalMatrix[3]);
			LocalMatrix[3] = vec4(0, 0, 0, LocalMatrix[3].w);

			vec3 Row[3], Pdum3;

			// Now get scale and shear.
			for (length_t i = 0; i < 3; ++i)
				for (length_t j = 0; j < 3; ++j)
					Row[i][j] = LocalMatrix[i][j];

			// Compute X scale factor and normalize first row.
			outScale->x = length(Row[0]);
			Row[0] = detail::scale(Row[0], static_cast<T>(1));
			outScale->y = length(Row[1]);
			Row[1] = detail::scale(Row[1], static_cast<T>(1));
			outScale->z = length(Row[2]);
			Row[2] = detail::scale(Row[2], static_cast<T>(1));

			// At this point, the matrix (in rows[]) is orthonormal.
			// Check for a coordinate system flip.  If the determinant
			// is -1, then negate the matrix and the scaling factors.
#if 0
			Pdum3 = cross(Row[1], Row[2]); // v3Cross(row[1], row[2], Pdum3);
			if (dot(Row[0], Pdum3) < 0) {
				for (length_t i = 0; i < 3; i++) {
					scale[i] *= static_cast<T>(-1);
					Row[i] *= static_cast<T>(-1);
				}
		}
#endif
			mat3 tRow{ 1.f };
			tRow[0] = Row[0]; tRow[1] = Row[1]; tRow[2] = Row[2];

			*outRotationEulerYXZ = decomposeEulerYXZ(tRow);
	}

	};


}