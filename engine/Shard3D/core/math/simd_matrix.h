#pragma once
#include <glm/ext/matrix_float4x4.hpp>
#include <immintrin.h>

namespace Shard3D {
	namespace SIMD {

		constexpr static inline glm::mat4 mat4Mult(const glm::mat4& lhs, const glm::mat4& rhs) {
			float lhs_R01_dat[8] = { lhs[0][0], lhs[1][0], lhs[2][0], lhs[3][0], lhs[0][1], lhs[1][1], lhs[2][1], lhs[3][1] };
			float lhs_R23_dat[8] = { lhs[0][2], lhs[1][2], lhs[2][2], lhs[3][2], lhs[0][3], lhs[1][3], lhs[2][3], lhs[3][3] };
			float rhs_C01_dat[8] = { lhs[0][0], lhs[0][1], lhs[0][2], lhs[0][3], lhs[1][0], lhs[1][1], lhs[1][2], lhs[1][3] };
			float rhs_C23_dat[8] = { lhs[2][0], lhs[2][1], lhs[2][2], lhs[2][3], lhs[3][0], lhs[3][1], lhs[3][2], lhs[3][3] };

			__m256 lhs_R01 = _mm256_load_ps(lhs_R01_dat);
			__m256 lhs_R23 = _mm256_load_ps(lhs_R23_dat);
			__m256 rhs_C01 = _mm256_load_ps(rhs_C01_dat);
			__m256 rhs_C23 = _mm256_load_ps(rhs_C23_dat);

			__m256 mul_lhs_R01_rhs_C01 = _mm256_mul_ps(lhs_R01, rhs_C01);
			__m256 mul_lhs_R23_rhs_C23 = _mm256_mul_ps(lhs_R23, rhs_C23);

			float mul_lhs_R01_rhs_C01_data[8]{};
			float mul_lhs_R23_rhs_C23_data[8]{};
			_mm256_storeu_ps(mul_lhs_R01_rhs_C01_data, mul_lhs_R01_rhs_C01);
			_mm256_storeu_ps(mul_lhs_R23_rhs_C23_data, mul_lhs_R23_rhs_C23);

			//float result_preprod_C01[8] = { mul_lhs_R01_rhs_C01_data[0], mul_lhs_R01_rhs_C01_data[1], mul_lhs_R01_rhs_C01_data[3], };
			//float result_preprod_C23[8] = { lhs[0][2], lhs[1][2], lhs[2][2], lhs[3][2], lhs[0][3], lhs[1][3], lhs[2][3], lhs[3][3] };
			//
			//__m256 mul_lhs_R01_rhs_C01 = _mm256_add_ps(, );
			//__m256 mul_lhs_R23_rhs_C23 = _mm256_add_ps(, );
		}
	}
}