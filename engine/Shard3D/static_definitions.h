#pragma once
#include "utils/versioning.h"

namespace Shard3D {

//=====================================================================================================================
//=====================================================================================================================

// Definitions are engine only, requires recompiling static library to take effect

const static Version ENGINE_VERSION = Version{ VersionState::Alpha, 0, 13, 1 };	// Shard3D 

#define EDITOR_SETTINGS_PATH						"pref/editor_settings.ini"
#define ENGINE_SCRIPT_LIBRARY_PATH					"resources/scriptdata"
#define ENGINE_ASSET_SUFFIX							".s3dasset"
#define ENGINE_SHADER_FILES_PATH					"resources/shaderdata/"							//Shader files (SPIR-V)
#define ENGINE_FONT_PATH							"resources/fonts/"		//Fonts
#define ENGINE_DEFAULT_ENGINE_FONT					"resources/fonts/fs-tahoma-8px.ttf"		//Default engine font
#define ENGINE_FONT_SIZE							16		//size in px
#define ENGINE_SHARD3D_LEVELFILE_OPTIONS			"Shard3D Level (*.s3dlevel)\0*.s3dlevel\0All files (*.*)\0*.*\0"
#define ENGINE_SHARD3D_ASSETFILE_OPTIONS			"Shard3D Asset (*.s3dasset)\0*.s3dasset\0All files (*.*)\0*.*\0"
#define ENGINE_SHARD3D_BLUEPRINT_OPTIONS			"Shard3D Prefab (*.s3dprefab)\0*.s3dprefab\0All files (*.*)\0*.*\0"
#define ENGINE_SHARD3D_HUDFILE_OPTIONS				"Shard3D HUD (*.s3dhud)\0*.s3dhud\0All files (*.*)\0*.*\0"
#define ENGINE_MAX_LIGHTS							128
#define ENGINE_MAX_DIRECTIONAL_LIGHTS				16
#define ENGINE_MAX_REFLECTION_CUBES					128
#define ENGINE_MAX_REFLECTION_PLANES				8
#define ENGINE_MAX_LIGHTING_VOLUMES					64

//=====================================================================================================================
//=====================================================================================================================

// tools
#define ENSET_BETA_DEBUG_TOOLS		

#ifdef NDEBUG
#define SHARD3D_NO_ASSERT	// Disable assertions for slight performance boost
#endif

}