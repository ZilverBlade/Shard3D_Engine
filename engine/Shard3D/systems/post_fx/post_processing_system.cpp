#include "../../s3dpch.h" 

#include "post_processing_system.h"
#include "../computational/shader_system.h"
#include "../../core/rendering/render_pass.h"
#include "../../vulkan_abstr.h"
#include "../../ecs.h"
#include "../../core/misc/graphics_settings.h"
#include "../handlers/material_system.h"
#include "../rendering/gbuffer_struct.h"
#include "../../core/misc/engine_settings.h"

namespace Shard3D::Systems {
	PostProcessingSystem::PostProcessingSystem(
		S3DDevice& device, 
		glm::vec2 resolution,
		SceneBufferInputData imageInput
	) : engineDevice(device) {


		if (ProjectSystem::getEngineSettings().postfx.enableBloom)
			fxBloom = make_uPtr<PostFX::Bloom>(device,
				resolution,
				imageInput.colorAttachment
			);

		fxHDR = make_uPtr<PostFX::HDR>(device,
			resolution,		
			ProjectSystem::getEngineSettings().postfx.enableBloom ? fxBloom->getNext() : imageInput.colorAttachment
		);

		if (ProjectSystem::getGraphicsSettings().postfx.aa.fxaa) {
			fxAA = make_uPtr<PostFX::FXAA311>(device,
				resolution,
				fxHDR->getNext()
			);
		}

		fxFix = make_uPtr<PostFX::FlipDeband>(device,
			resolution,
			ProjectSystem::getGraphicsSettings().postfx.aa.fxaa ? fxAA->getNext() : fxHDR->getNext()
		);
	}

	PostProcessingSystem::~PostProcessingSystem() {
		
	}

	void PostProcessingSystem::resize(glm::vec2 newSize, const SceneBufferInputData& imageInput) {
		if (ProjectSystem::getEngineSettings().postfx.enableBloom)
			fxBloom->resize(newSize, { imageInput.colorAttachment->getDescriptor() });
		fxHDR->resize(newSize, { ProjectSystem::getEngineSettings().postfx.enableBloom ? fxBloom->getNext()->getDescriptor() : imageInput.colorAttachment->getDescriptor() });
		if (ProjectSystem::getGraphicsSettings().postfx.aa.fxaa) {
			fxAA->resize(newSize, { fxHDR->getNext()->getDescriptor() });
		}
		fxFix->resize(newSize, { ProjectSystem::getGraphicsSettings().postfx.aa.fxaa ? fxAA->getNext()->getDescriptor() : fxHDR->getNext()->getDescriptor() });


	}

	void PostProcessingSystem::renderCore(FrameInfo& frameInfo) {
		if (ProjectSystem::getEngineSettings().postfx.enableBloom)
			fxBloom->render(frameInfo.commandBuffer, frameInfo.camera.postfx);
		fxHDR->render(frameInfo.commandBuffer, frameInfo.camera.postfx);
		if (ProjectSystem::getGraphicsSettings().postfx.aa.fxaa) {
			fxAA->render(frameInfo.commandBuffer, frameInfo.camera.postfx);
		}
		fxFix->render(frameInfo.commandBuffer, frameInfo.camera.postfx);
	}
}