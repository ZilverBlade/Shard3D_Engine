#pragma once
#include "ppfx_base.h"

namespace Shard3D {
	namespace PostFX {
		class MotionBlur : public PostFX::Effect {
		public:
			MotionBlur(
				S3DDevice& device,
				glm::vec2 resolution,
				S3DRenderTarget* previousTarget,
				S3DRenderTarget* depthBuffer,
				S3DRenderTarget* velocityBuffer,
				int k = 20
			);
			virtual ~MotionBlur();
			virtual void render(VkCommandBuffer commandBuffer, const PostFXData& postFXdata) override;
			virtual void resize(glm::vec2 size, const std::vector<VkDescriptorImageInfo> previousTargets) override;
			S3DRenderTarget* getNext() {
				return tileNeighborhood->getAttachment();
			}
		private:
			void createPasses(glm::vec2 resolution, const std::vector<VkDescriptorImageInfo> previousTargets);

			uPtr<PostProcessingEffect> tileMax;
			uPtr<PostProcessingEffect> tileNeighborhood;
			uPtr<PostProcessingEffect> blurOut;

			int k;

			struct motion_blur_settings_ {
				float targetFrameRate;
				float currentFrameRate;
				int samples;
				float cameraNearZ;
				float cameraFarZ;
			} motionBlurData;
		};
	}
}