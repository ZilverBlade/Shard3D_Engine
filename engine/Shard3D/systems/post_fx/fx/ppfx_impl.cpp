#include "ppfx_bloom.h"
#include "ppfx_hdr.h"
#include "ppfx_flip_deband.h"
#include "ppfx_fxaa311.h"
#include "ppfx_gamma_correct.h"
#include "ppfx_motion_blur.h"

#include "../../../core/misc/engine_settings.h"
#include "../../../core/misc/graphics_settings.h"

// post fx implementation
namespace Shard3D::PostFX {
	
	MotionBlur::MotionBlur(
		S3DDevice& device, 
		glm::vec2 resolution,
		S3DRenderTarget* previousTarget,
		S3DRenderTarget* depthBuffer,
		S3DRenderTarget* velocityBuffer,
		int k
	) : PostFX::Effect(device), k(k) {
		createPasses(resolution, { previousTarget->getDescriptor(), depthBuffer->getDescriptor(), velocityBuffer->getDescriptor() });
	}

	MotionBlur::~MotionBlur() {}

	void MotionBlur::render(VkCommandBuffer commandBuffer, const PostFXData& postFXdata) {
		tileMax->render(commandBuffer, k);
		tileNeighborhood->render(commandBuffer);
	}

	void MotionBlur::resize(glm::vec2 size, const std::vector<VkDescriptorImageInfo> previousTargets) {
		k = floor(size.y * (20.0f / 720.0f));
		tileMax->resize(ceil(size / static_cast<float>(k)), { previousTargets[2] });
		tileNeighborhood->resize(ceil(glm::vec3(tileMax->getAttachment()->getDimensions()) / glm::vec3(3)), { tileMax->getAttachment()->getDescriptor() });
	}

	void MotionBlur::createPasses(glm::vec2 resolution, const std::vector<VkDescriptorImageInfo> previousTargets) {
		VkFormat velocityTextureFormat = VK_FORMAT_R8G8_SNORM;

		tileMax = make_uPtr<PostProcessingEffect>(
			engineDevice, ceil(resolution / static_cast<float>(k)),
			S3DShader{ ShaderType::Fragment, "resources/shaders/postfx/velocity_tile_max.frag", "postfx" },
			std::vector<VkDescriptorImageInfo>{ previousTargets[2] },
			velocityTextureFormat,
			VK_IMAGE_VIEW_TYPE_2D,
			1,
			1,
			false
		);
		tileNeighborhood = make_uPtr<PostProcessingEffect>(
			engineDevice, ceil(glm::vec3(tileMax->getAttachment()->getDimensions()) / glm::vec3(3)),
			S3DShader{ ShaderType::Fragment, "resources/shaders/postfx/velocity_tile_neighborhood.frag", "postfx" },
			std::vector<VkDescriptorImageInfo>{ tileMax->getAttachment()->getDescriptor() },
			velocityTextureFormat,
			VK_IMAGE_VIEW_TYPE_2D,
			1,
			1,
			false
		);
	}

	GammaCorrect::GammaCorrect(
	S3DDevice& device,
	glm::vec2 resolution,
	S3DRenderTarget* previousTarget,
	VkRenderPass renderPass
	) : PostFX::Effect(device), inputAttachments({ previousTarget->getDescriptor() }) {
		auto builder = S3DDescriptorSetLayout::Builder(engineDevice);
		builder.addBinding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT);

		ppfxSceneDescriptorLayout = builder.build();

		createSceneDescriptors();
		createPipelineLayout();
		createPipeline(S3DShader{ ShaderType::Fragment, "resources/shaders/postfx/gamma_correct.frag", "postfx" }, renderPass);
	}

	GammaCorrect::~GammaCorrect() {
		vkDestroyPipelineLayout(engineDevice.device(), ppfxPipelineLayout, nullptr);
	}
	void GammaCorrect::render(VkCommandBuffer commandBuffer, const PostFXData& postFXdata) {
		ppfxPipeline->bind(commandBuffer);
		//VkDescriptorSet descriptors[3]{
		//	frameInfo.globalDescriptorSet,
		//	frameInfo.sceneDescriptorSet,
		//	ppfxSceneDescriptorSet
		//};
		vkCmdBindDescriptorSets(commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			ppfxPipelineLayout,
			0,
			1,
			&ppfxSceneDescriptorSet,
			0,
			nullptr
		);
		float invGamma = 1.0 / 1.0; // gamma no longer corrected in swapchain pass
		vkCmdPushConstants(commandBuffer, ppfxPipelineLayout, VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(invGamma), &invGamma);
		vkCmdDraw(commandBuffer, 6, 1 ,0,0);
	}

	void GammaCorrect::resize(glm::vec2 size, const std::vector<VkDescriptorImageInfo> previousTargets) {
		inputAttachments = previousTargets;
		createSceneDescriptors();
	}

	void GammaCorrect::createPipelineLayout() {
		std::vector<VkDescriptorSetLayout> descriptorSetLayouts{
			ppfxSceneDescriptorLayout->getDescriptorSetLayout()
		};

		VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
		pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		pipelineLayoutInfo.setLayoutCount = (uint32_t)descriptorSetLayouts.size();
		pipelineLayoutInfo.pSetLayouts = descriptorSetLayouts.data();
		pipelineLayoutInfo.pushConstantRangeCount = 1;

		VkPushConstantRange pushConstantRange{};
		pushConstantRange.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
		pushConstantRange.offset = 0;
		pushConstantRange.size = 128;

		pipelineLayoutInfo.pPushConstantRanges = &pushConstantRange;
		VK_VALIDATE(vkCreatePipelineLayout(engineDevice.device(), &pipelineLayoutInfo, nullptr, &ppfxPipelineLayout), "failed to create pipeline layout!");
	}

	void GammaCorrect::createPipeline(const S3DShader& fragmentShader, VkRenderPass renderPass) {
		S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo();
		pipelineConfig.setCullMode(VK_CULL_MODE_BACK_BIT);
		pipelineConfig.disableDepthTest();

		pipelineConfig.pipelineLayout = ppfxPipelineLayout;
		pipelineConfig.renderPass = renderPass;
		ppfxPipeline = make_uPtr<S3DGraphicsPipeline>(
			engineDevice, std::vector<S3DShader>{
			S3DShader{ ShaderType::Vertex, "resources/shaders/fullscreen_quad.vert", "misc" },
				fragmentShader
		},
			pipelineConfig
				);
	}

	void GammaCorrect::createSceneDescriptors() {
		auto writer = S3DDescriptorWriter(*ppfxSceneDescriptorLayout, *engineDevice.staticMaterialPool);
		writer.writeImage(0, &inputAttachments[0]);
		writer.build(ppfxSceneDescriptorSet);
	}

	FlipDeband::FlipDeband(
		S3DDevice& device,
		glm::vec2 resolution,
		S3DRenderTarget* previousTarget
	) : PostFX::Effect(device) {
		createPasses(resolution, {previousTarget->getDescriptor() });
	}

	FlipDeband::~FlipDeband() {
	}

	void FlipDeband::render(VkCommandBuffer commandBuffer, const PostFXData& postFXdata) {
		struct Push {
			glm::vec2 res;
			float invGamma ;
		}push;
		push.res = static_cast<glm::vec2>(flipDeband->getAttachment()->getDimensions());
		push.invGamma = ProjectSystem::isEditorMode() ? 1.0 : 1.0 / ProjectSystem::getGraphicsSettings().display.gamma;
		flipDeband->render(commandBuffer, push);
	}

	void FlipDeband::resize(glm::vec2 size, const std::vector<VkDescriptorImageInfo> previousTargets) {
		flipDeband->resize(size, previousTargets);
	}

	void FlipDeband::createPasses(glm::vec2 resolution, const std::vector<VkDescriptorImageInfo> previousTargets) {
		flipDeband = make_uPtr<PostProcessingEffect>(
			engineDevice, resolution,
			S3DShader{ ShaderType::Fragment,
			"resources/shaders/postfx/debander_8bit.frag", 
			"postfx" },
			previousTargets,
			VK_FORMAT_R8G8B8A8_UNORM
		);
	}

	HDR::HDR(
		S3DDevice& device,
		glm::vec2 resolution,
		S3DRenderTarget* previousTarget
	) : PostFX::Effect(device) {
		createPasses(resolution, {previousTarget->getDescriptor() });
	}

	HDR::~HDR() {

	}

	void HDR::render(VkCommandBuffer commandBuffer, const PostFXData& postFXdata) {
		hdrData.contrast = postFXdata.grading.contrast;
		hdrData.saturation = postFXdata.grading.saturation;
		hdrData.gain = postFXdata.grading.gain;
		hdrData.temperature = postFXdata.grading.temperature;
		hdrData.hueShift = postFXdata.grading.hueShift / 360.0f;
		hdrData.lim = postFXdata.hdr.lim;
		hdrData.toneMappingMethod = static_cast<int>(postFXdata.hdr.toneMappingAlgorithm);
		hdrData.shadows = postFXdata.grading.shadows;
		hdrData.midtones = postFXdata.grading.midtones;
		hdrData.highlights = postFXdata.grading.highlights;
		colorCorrection->render(commandBuffer, VoidData<128>(hdrData));
	}

	void HDR::resize(glm::vec2 size, const std::vector<VkDescriptorImageInfo> previousTargets) {
		colorCorrection->resize(size, previousTargets);
	}

	void HDR::createPasses(glm::vec2 resolution, const std::vector<VkDescriptorImageInfo> previousTargets) {
		colorCorrection = make_uPtr<PostProcessingEffect>(
			engineDevice, resolution,
			S3DShader{ ShaderType::Fragment, "resources/shaders/postfx/hdr.frag", "postfx" },
			previousTargets,
			VK_FORMAT_R16G16B16A16_SFLOAT
		);
	}

	Bloom::Bloom(
		S3DDevice& device,
		glm::vec2 resolution,
		S3DRenderTarget* previousTarget
	) : PostFX::Effect(device) {
		createPasses(resolution, {previousTarget->getDescriptor()});
	}
	Bloom::~Bloom() {

	}
	struct bloompassextrinfo {
		float threshold = 1.0f;
		float strength = 1.0f;
		float exposure = 1.0f;
		alignas(16)glm::vec2 screensize;
	};

	struct bloomcombinfo {
		float sizeCoef = 0.55f;
		int quality = 1;
		float exposure;
		alignas(16)glm::vec2 screensize;
	};
	struct lensFlareInfo {
		float ghostDistribution;
		int ghostSamples;
		uint32_t lensColorTexture;
		uint32_t lensFlareTexture;
		glm::vec3 channelDistort;
	};
	void Bloom::render(VkCommandBuffer commandBuffer, const PostFXData& postFXdata) {
		bloompassextrinfo ext{};
		ext.screensize = static_cast<glm::vec2>(bloomCrossExtract->getAttachment()->getDimensions());
		ext.strength = postFXdata.bloom.strength;
		ext.exposure = postFXdata.hdr.exposure;
		ext.threshold = postFXdata.bloom.threshold;
		bloomCrossExtract->render(commandBuffer, ext);
		gaussianPass0->render(commandBuffer, static_cast<glm::vec2>(gaussianPass0->getAttachment()->getDimensions()));
		if (!ProjectSystem::getEngineSettings().postfx.fastBloom) {
			gaussianPass1->render(commandBuffer, static_cast<glm::vec2>(gaussianPass1->getAttachment()->getDimensions()));
			gaussianPassLarge0->render(commandBuffer, static_cast<glm::vec2>(gaussianPassLarge0->getAttachment()->getDimensions()));
			gaussianPassLarge1->render(commandBuffer, static_cast<glm::vec2>(gaussianPassLarge1->getAttachment()->getDimensions()));
		}
		//lensFlareInfo lfin{};
		//lfin.ghostDistribution = 0.7f;
		//lfin.ghostSamples = 7;
		//lfin.channelDistort = { 0.0003, 0.001, 0.0018 };
		//lensFlare->render(commandBuffer, lfin);
		bloomcombinfo comb{};
		comb.screensize = static_cast<glm::vec2>(bloomCombine->getAttachment()->getDimensions());
		comb.quality = ProjectSystem::getGraphicsSettings().postfx.bloom.bloomQuality;
		comb.sizeCoef = postFXdata.bloom.knee;
		comb.exposure = postFXdata.hdr.exposure;
		bloomCombine->render(commandBuffer, comb);
	}

	void Bloom::resize(glm::vec2 size, const std::vector<VkDescriptorImageInfo> previousTargets) {
		bloomCrossExtract->resize(size / 2.f, previousTargets);
		gaussianPass0->resize(size / 8.f, { bloomCrossExtract->getAttachment()->getDescriptor() });
		if (!ProjectSystem::getEngineSettings().postfx.fastBloom) {
			gaussianPass1->resize(size / 8.f, { gaussianPass0->getAttachment()->getDescriptor() });
			gaussianPassLarge0->resize(size / 16.f, { gaussianPass1->getAttachment()->getDescriptor() });
			gaussianPassLarge1->resize(size / 16.f, { gaussianPassLarge0->getAttachment()->getDescriptor() });
		}
		//lensFlare->resize(size / 8.0f, std::vector<VkDescriptorImageInfo>{gaussianPass1->getAttachment()->getDescriptor() });
		bloomCombine->resize(size, ProjectSystem::getEngineSettings().postfx.fastBloom ? std::vector<VkDescriptorImageInfo>{previousTargets[0], gaussianPass0->getAttachment()->getDescriptor() } : std::vector<VkDescriptorImageInfo>{ previousTargets[0], gaussianPass1->getAttachment()->getDescriptor(), gaussianPassLarge1->getAttachment()->getDescriptor()/*, lensFlare->getAttachment()->getDescriptor()*/});
	}

	void Bloom::createPasses(glm::vec2 resolution, const std::vector<VkDescriptorImageInfo> previousTargets) {

		VkFormat bloomTextureFormat = VK_FORMAT_B10G11R11_UFLOAT_PACK32;

		bloomCrossExtract = make_uPtr<PostProcessingEffect>(
			engineDevice, resolution / 2.f,
			S3DShader{ ShaderType::Fragment, "resources/shaders/postfx/bloom_pass_extract_cross.frag", "postfx" },
			previousTargets,
			bloomTextureFormat
			);
		S3DShader bloomXsh = S3DShader{ ShaderType::Fragment, "resources/shaders/postfx/bloom_cross_blur_pass.frag", "postfx" };
		S3DShader bloomHsh = S3DShader{ ShaderType::Fragment, "resources/shaders/postfx/gaussian_pass.frag", "postfx", {}, { "GAUSSIAN_KERNEL_SIZE_11", "GAUSSIAN_PASS_HORIZONTAL", "TEX_FORMAT_RGB" }, "bloom_blur_pass_h.frag"};
		S3DShader bloomVsh = S3DShader{ ShaderType::Fragment, "resources/shaders/postfx/gaussian_pass.frag", "postfx", {}, { "GAUSSIAN_KERNEL_SIZE_11", "GAUSSIAN_PASS_VERTICAL", "TEX_FORMAT_RGB" }, "bloom_blur_pass_v.frag" };
		gaussianPass0 = make_uPtr<PostProcessingEffect>(
			engineDevice, resolution / 8.f,
			ProjectSystem::getEngineSettings().postfx.fastBloom ? bloomVsh : bloomHsh,
			std::vector<VkDescriptorImageInfo>{ bloomCrossExtract->getAttachment()->getDescriptor() },
			bloomTextureFormat
			);
		if (!ProjectSystem::getEngineSettings().postfx.fastBloom) {
			gaussianPass1 = make_uPtr<PostProcessingEffect>(
				engineDevice, resolution / 8.f,
				bloomVsh,
				std::vector<VkDescriptorImageInfo>{gaussianPass0->getAttachment()->getDescriptor() },
				bloomTextureFormat
				);
			gaussianPassLarge0 = make_uPtr<PostProcessingEffect>(
				engineDevice, resolution / 16.f,
				bloomHsh,
				std::vector<VkDescriptorImageInfo>{ gaussianPass1->getAttachment()->getDescriptor() },
				bloomTextureFormat
				);
			gaussianPassLarge1 = make_uPtr<PostProcessingEffect>(
				engineDevice, resolution / 16.f,
				bloomVsh, 
				std::vector<VkDescriptorImageInfo>{ gaussianPassLarge0->getAttachment()->getDescriptor() },
				bloomTextureFormat
				);
		}
		//lensFlare = make_uPtr<PostProcessingEffect>(
		//	engineDevice, resolution / 8.f,
		//	S3DShader{ ShaderType::Fragment, "resources/shaders/postfx/lens_flare.frag", "postfx" },
		//	std::vector<VkDescriptorImageInfo>{ gaussianPass1->getAttachment()->getDescriptor() },
		//	VK_FORMAT_B10G11R11_UFLOAT_PACK32
		//);
		bloomCombine = make_uPtr<PostProcessingEffect>(
			engineDevice, resolution,
			S3DShader{ ShaderType::Fragment, "resources/shaders/postfx/bloom_pass_combine.frag", "postfx", {},
{
ProjectSystem::getEngineSettings().postfx.fastBloom ? "EnableFastBloom" : "MultiBloomKnl"
} },
			ProjectSystem::getEngineSettings().postfx.fastBloom
			? std::vector<VkDescriptorImageInfo>{ previousTargets[0], gaussianPass0->getAttachment()->getDescriptor() }
			: std::vector<VkDescriptorImageInfo>{ previousTargets[0], gaussianPass1->getAttachment()->getDescriptor(), gaussianPassLarge1->getAttachment()->getDescriptor()/*, lensFlare->getAttachment()->getDescriptor()*/},
			VK_FORMAT_R16G16B16A16_SFLOAT
		);


	}

	FXAA311::FXAA311(
		S3DDevice& device,
		glm::vec2 resolution,
		S3DRenderTarget* previousTarget
	) : PostFX::Effect(device) {
		createPasses(resolution, { previousTarget->getDescriptor() });
	}
	FXAA311::~FXAA311() {
	}
	void FXAA311::render(VkCommandBuffer commandBuffer, const PostFXData& postFXdata) {
		fxaapush.res = static_cast<glm::vec2>(fxaaPass->getAttachment()->getDimensions());
		fxaapush.gamma = ProjectSystem::getGraphicsSettings().display.gamma;
		fxaapush.invGamma = 1.0f / fxaapush.gamma;
		fxaapush.maxSpan = ProjectSystem::getGraphicsSettings().postfx.aa.fxaaSoftness;
		fxaaPass->render(commandBuffer, fxaapush);
	}
	void FXAA311::resize(glm::vec2 size, const std::vector<VkDescriptorImageInfo> previousTargets) {
		fxaaPass->resize(size, previousTargets);
	}
	void FXAA311::createPasses(glm::vec2 resolution, const std::vector<VkDescriptorImageInfo> previousTargets) {
		fxaaPass = make_uPtr<PostProcessingEffect>(
			engineDevice, resolution,
			S3DShader{ ShaderType::Fragment, "resources/shaders/postfx/fxaa.frag", "postfx" },
			previousTargets,
			VK_FORMAT_R16G16B16A16_SFLOAT
		);
	}
}