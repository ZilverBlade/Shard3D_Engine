#pragma once
#include "ppfx_base.h"

namespace Shard3D {
	namespace PostFX {
		// nvidia fxaa 3.11
		class FXAA311 : public PostFX::Effect {
		public:
			FXAA311(
				S3DDevice& device,
				glm::vec2 resolution,
				S3DRenderTarget* previousTargets
			);
			virtual ~FXAA311();
			virtual void render(VkCommandBuffer commandBuffer, const PostFXData& postFXdata) override;
			virtual void resize(glm::vec2 size, const std::vector<VkDescriptorImageInfo> previousTargets) override;
			S3DRenderTarget* getNext() {
				return fxaaPass->getAttachment();
			}
		private:
			void createPasses(glm::vec2 resolution, const std::vector<VkDescriptorImageInfo> previousTargets);

			uPtr<PostProcessingEffect> fxaaPass;

			struct fxaapush_ {
				glm::vec2 res;
				float invGamma;
				float gamma;
				float maxSpan;
			}fxaapush;
		};
	}
}