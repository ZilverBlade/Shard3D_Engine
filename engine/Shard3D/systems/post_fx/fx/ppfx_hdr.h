#pragma once
#include "ppfx_base.h"

namespace Shard3D {
	namespace PostFX {
		class HDR : public PostFX::Effect {
		public:
			HDR(
				S3DDevice& device,
				glm::vec2 resolution,
				S3DRenderTarget* previousTarget
			);
			virtual ~HDR();
			virtual void render(VkCommandBuffer commandBuffer, const PostFXData& postFXdata) override;
			virtual void resize(glm::vec2 size, const std::vector<VkDescriptorImageInfo>  previousTargets) override;
			S3DRenderTarget* getNext() {
				return colorCorrection->getAttachment();
			}
		private:
			void createPasses(glm::vec2 resolution, const std::vector<VkDescriptorImageInfo> previousTargets);

			uPtr<PostProcessingEffect> colorCorrection;

			struct _hdr_settings {
				int toneMappingMethod;
				float lim;
				float gain;
				float contrast;
				float saturation;
				float temperature;
				float hueShift;
				float n;
				alignas(16)glm::vec3 shadows;
				alignas(16)glm::vec3 midtones;
				alignas(16)glm::vec3 highlights;
			} hdrData;
		};
	}
}