#pragma once
#include "ppfx_base.h"

namespace Shard3D {
	namespace PostFX {
		class GammaCorrect : public PostFX::Effect {
		public:
			GammaCorrect(
				S3DDevice& device,
				glm::vec2 resolution,
				S3DRenderTarget* previousTarget,
				VkRenderPass renderPass
			);
			virtual ~GammaCorrect();
			virtual void render(VkCommandBuffer commandBuffer, const PostFXData& postFXdata) override;
			virtual void resize(glm::vec2 size, const std::vector<VkDescriptorImageInfo> previousTargets) override;
		private:
			void createPipelineLayout();
			void createPipeline(const S3DShader& fragmentShader, VkRenderPass renderPass);
			void createSceneDescriptors();

			std::vector<VkDescriptorImageInfo> inputAttachments;

			uPtr<S3DGraphicsPipeline> ppfxPipeline{};

			uPtr<S3DDescriptorSetLayout> ppfxSceneDescriptorLayout{};
			VkDescriptorImageInfo ppfxDescriptor_BaseRenderedScene{};
			VkDescriptorSet ppfxSceneDescriptorSet{};

			VkPipelineLayout ppfxPipelineLayout{};
		};
	}
}