#pragma once
#include "ppfx_base.h"

namespace Shard3D {
	namespace PostFX {
		class Bloom : public PostFX::Effect {
		public:
			Bloom(
				S3DDevice& device,
				glm::vec2 resolution,
				S3DRenderTarget* previousTarget
			);
			virtual ~Bloom();
			virtual void render(VkCommandBuffer commandBuffer, const PostFXData& postFXdata) override;
			virtual void resize(glm::vec2 size, const std::vector<VkDescriptorImageInfo> previousTargets) override;
			S3DRenderTarget* getNext() {
				return bloomCombine->getAttachment();
			}
		private:
			void createPasses(glm::vec2 resolution, const std::vector<VkDescriptorImageInfo> previousTargets);

			uPtr<PostProcessingEffect> bloomCrossExtract;
			uPtr<PostProcessingEffect> gaussianPass0;
			uPtr<PostProcessingEffect> gaussianPass1;
			uPtr<PostProcessingEffect> gaussianPassLarge0;
			uPtr<PostProcessingEffect> gaussianPassLarge1;
			//uPtr<PostProcessingEffect> lensFlare;
			uPtr<PostProcessingEffect> bloomCombine;

			struct _bloom_settings {
				glm::vec2 screenSize;
			} bloomData;
		};
	}
}