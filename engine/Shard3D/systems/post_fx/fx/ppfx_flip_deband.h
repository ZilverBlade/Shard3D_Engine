#pragma once
#include "ppfx_base.h"

namespace Shard3D {
	namespace PostFX {
		class FlipDeband : public PostFX::Effect {
		public:
			FlipDeband(
				S3DDevice& device,
				glm::vec2 resolution,
				S3DRenderTarget* previousTarget
			);
			virtual ~FlipDeband();
			virtual void render(VkCommandBuffer commandBuffer, const PostFXData& postFXdata) override;
			virtual void resize(glm::vec2 size, const std::vector<VkDescriptorImageInfo> previousTargets) override;
			S3DRenderTarget* getNext() {
				return flipDeband->getAttachment();
			}
		private:
			void createPasses(glm::vec2 resolution, const std::vector<VkDescriptorImageInfo> previousTargets);

			uPtr<PostProcessingEffect> flipDeband;
		};
	}
}