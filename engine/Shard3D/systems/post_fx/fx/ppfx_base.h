#pragma once
#include "../post_processing_fx.h"

namespace Shard3D {
	namespace PostFX {
		class Effect {
		public:
			Effect(
				S3DDevice& device
			) : engineDevice(device) {}
			virtual ~Effect() {}
			virtual void render(VkCommandBuffer commandBuffer, const PostFXData& postFXdata) {}
			virtual void resize(glm::vec2 size, const std::vector<VkDescriptorImageInfo> previousTargets) {}
			// derived classes must implement this //virtual SceneBufferInputData getNext();
		protected:
			S3DDevice& engineDevice;
		};
	}
}