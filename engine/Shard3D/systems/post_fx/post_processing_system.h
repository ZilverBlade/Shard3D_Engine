#pragma once
#include "../../s3dstd.h"
#include "../../vulkan_abstr.h"
#include "../../core/misc/frame_info.h"	  

#include "fx/ppfx_bloom.h"
#include "fx/ppfx_hdr.h"
#include "fx/ppfx_flip_deband.h"
#include "fx/ppfx_fxaa311.h"
#include "fx/ppfx_gamma_correct.h"

namespace Shard3D {
	inline namespace Systems {
		struct SceneBufferInputData;
		class PostProcessingSystem {
		public:
			PostProcessingSystem(
				S3DDevice& device,
				glm::vec2 resolution,
				SceneBufferInputData imageInput
			);
			~PostProcessingSystem();

			PostProcessingSystem(const PostProcessingSystem&) = delete;
			PostProcessingSystem& operator=(const PostProcessingSystem&) = delete;

			void renderCore(FrameInfo& frameInfo);

			void resize(glm::vec2 newSize, const SceneBufferInputData& imageInput);

			inline S3DDevice& getDevice() { return engineDevice; }
			inline S3DRenderTarget* getFinalImage() { return fxFix->getNext(); }
		private:
			uPtr<PostFX::Bloom> fxBloom;
			uPtr<PostFX::HDR> fxHDR;
			uPtr<PostFX::FlipDeband> fxFix;
			uPtr<PostFX::FXAA311> fxAA;

			S3DDevice& engineDevice;
		};
	}
}