#pragma once
#include "../../s3dstd.h"
#include "../../vulkan_abstr.h"
#include "../../core/misc/frame_info.h"	  
#include "../../core/vulkan_api/graphics_pipeline.h"
#include "../rendering/gbuffer_struct.h"
#include "../../core/rendering/render_pass.h"

namespace Shard3D {
	inline namespace Systems {
		class PostProcessingEffect {
		public:
			PostProcessingEffect(
				S3DDevice& device,
				glm::vec2 resolution,
				const S3DShader& fragmentShader,
				const std::vector<VkDescriptorImageInfo>& inputAttachments,
				VkFormat framebufferFormat,
				VkImageViewType viewType = VK_IMAGE_VIEW_TYPE_2D,
				uint32_t layers = 1,
				uint32_t mipLevels = 1,
				bool linearSampling = true
			);
			~PostProcessingEffect();

			PostProcessingEffect(const PostProcessingEffect&) = delete;
			PostProcessingEffect& operator= (const PostProcessingEffect&) = delete;

			void render(VkCommandBuffer commandBuffer, VoidData<128> pushData = VoidData<128>(), uint32_t layer = 0, uint32_t mipLevel = 0);
			void resize(glm::vec2 newResolution, const std::vector<VkDescriptorImageInfo>& inputAttachments);

			S3DRenderTarget* getAttachment() {
				return ppfxRenderTarget;
			}
		private:
			void createPipelineLayout();
			void createPipeline(const S3DShader& fragmentShader);
			void createSceneDescriptors();
			void createRenderPass(glm::vec2 resolution);

			S3DDevice& engineDevice;

			uint32_t mipLevels = 1;
			uint32_t layerCount = 1;
			bool linearSampling = true;

			std::vector<std::vector<S3DFramebuffer*>> ppfxSubFramebuffers{};
			S3DRenderPass* ppfxRenderPass{};
			S3DRenderTarget* ppfxRenderTarget{};
			VkFormat ppfxFramebufferFormat;
			VkImageViewType ppfxFramebufferViewType;

			std::vector<VkDescriptorImageInfo> inputAttachments;

			uPtr<S3DGraphicsPipeline> ppfxPipeline{};
			uPtr<S3DPipelineLayout> ppfxPipelineLayout{};
			S3DPushConstant ppfxPush{};

			uPtr<S3DDescriptorSetLayout> ppfxSceneDescriptorLayout{};
			VkDescriptorImageInfo ppfxDescriptorRenderTarget{};
			VkDescriptorSet ppfxSceneDescriptorSet{};
		};
	}
}