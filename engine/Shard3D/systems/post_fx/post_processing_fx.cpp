#include "post_processing_fx.h"
#include "../../core/asset/assetmgr.h"

namespace Shard3D {
	PostProcessingEffect::PostProcessingEffect(
		S3DDevice& device,
		glm::vec2 resolution,
		const S3DShader& fragmentShader,
		const std::vector<VkDescriptorImageInfo>& inputAttachments,
		VkFormat framebufferFormat,
		VkImageViewType viewType,
		uint32_t layers,
		uint32_t mipLevels,
		bool linearSampling
	) : engineDevice(device), inputAttachments(inputAttachments), ppfxFramebufferFormat(framebufferFormat), ppfxFramebufferViewType(viewType), layerCount(layers), mipLevels(mipLevels), linearSampling(linearSampling){
		if (inputAttachments.size() != 0) {
			S3DDescriptorSetLayout::Builder builder = S3DDescriptorSetLayout::Builder(engineDevice);
			for (int i = 0; i < inputAttachments.size(); i++) {
				builder.addBinding(i, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT);
			}
			ppfxSceneDescriptorLayout = builder.build();
		}
		createSceneDescriptors();
		createRenderPass(resolution);
		createPipelineLayout();
		createPipeline(fragmentShader);
	}

	PostProcessingEffect::~PostProcessingEffect() {
		delete ppfxRenderTarget;
		delete ppfxRenderPass;
		for (auto& sub : ppfxSubFramebuffers) {
			for (auto* ppfxFramebuffer : sub) {
				delete ppfxFramebuffer;
			}
		}
		engineDevice.staticMaterialPool->freeDescriptors({ ppfxSceneDescriptorSet });
	}

	void PostProcessingEffect::render(VkCommandBuffer commandBuffer, VoidData<128> pushData, uint32_t layer, uint32_t mipLevel) {
		ppfxRenderPass->beginRenderPass(commandBuffer, ppfxSubFramebuffers[layer][mipLevel]);
		ppfxPipeline->bind(commandBuffer);
		vkCmdBindDescriptorSets(commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			ppfxPipelineLayout->getPipelineLayout(),
			0,
			1,
			&ppfxSceneDescriptorSet,
			0,
			nullptr
		);
		ppfxPush.push(commandBuffer, ppfxPipelineLayout->getPipelineLayout(), pushData);
		vkCmdDraw(commandBuffer, ppfxFramebufferViewType == VK_IMAGE_VIEW_TYPE_CUBE ? 36 : 6, 1, 0, 0);
		ppfxRenderPass->endRenderPass(commandBuffer);
	}

	void PostProcessingEffect::resize(glm::vec2 newResolution, const std::vector<VkDescriptorImageInfo>& newInputAttachments) {
		inputAttachments = newInputAttachments;
		createRenderPass(newResolution);
		createSceneDescriptors();
	}

	void PostProcessingEffect::createPipelineLayout() {
		std::vector<VkDescriptorSetLayout> descriptorSetLayouts{};
		if (inputAttachments.size() != 0) {
			descriptorSetLayouts.push_back(ppfxSceneDescriptorLayout->getDescriptorSetLayout());
		}
		ppfxPush = S3DPushConstant(128, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT);
		std::vector<VkPushConstantRange> pushConstantRanges{
			ppfxPush.getRange()
		};
		ppfxPipelineLayout = make_uPtr<S3DPipelineLayout>(engineDevice, pushConstantRanges, descriptorSetLayouts);
	}

	void PostProcessingEffect::createPipeline(const S3DShader& fragmentShader) {
		S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo();
		if (!ppfxFramebufferViewType != VK_IMAGE_VIEW_TYPE_CUBE) {
			pipelineConfig.setCullMode(VK_CULL_MODE_BACK_BIT);
		}
		pipelineConfig.disableDepthTest();
		pipelineConfig.pipelineLayout = ppfxPipelineLayout->getPipelineLayout();
		pipelineConfig.renderPass = ppfxRenderPass->getRenderPass();
		ppfxPipeline = make_uPtr<S3DGraphicsPipeline>(
			engineDevice, std::vector<S3DShader>{
			S3DShader{ ShaderType::Vertex, ppfxFramebufferViewType == VK_IMAGE_VIEW_TYPE_CUBE ? "resources/shaders/full_screen_cube.vert" : "resources/shaders/fullscreen_quad.vert", "misc"},
				fragmentShader
			},
			pipelineConfig
		);
	}

	void PostProcessingEffect::createSceneDescriptors() {
		auto writer = S3DDescriptorWriter(*ppfxSceneDescriptorLayout, *engineDevice.staticMaterialPool);
		for (int i = 0; i < inputAttachments.size(); i++) {
			writer.writeImage(i, &inputAttachments[i]);
		}
		writer.build(ppfxSceneDescriptorSet);
		
	}
	void PostProcessingEffect::createRenderPass(glm::vec2 resolution) {
		PTR_DELETE(ppfxRenderTarget);
		PTR_DELETE(ppfxRenderPass);
		for (auto& sub : ppfxSubFramebuffers) {
			for (auto* ppfxFramebuffer : sub) {
				PTR_DELETE(ppfxFramebuffer);
			}
		}
		ppfxSubFramebuffers.clear();

		S3DRenderTargetCreateInfo createInfo{};
		createInfo.format = ppfxFramebufferFormat;
		createInfo.imageAspect = VK_IMAGE_ASPECT_COLOR_BIT;
		createInfo.viewType = ppfxFramebufferViewType;
		createInfo.dimensions = { resolution, 1 };
		createInfo.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
		createInfo.layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		createInfo.renderTargetType = S3DRenderTargetType::Color;
		createInfo.linearFiltering = linearSampling;
		createInfo.layerCount = layerCount;
		createInfo.mipLevels = mipLevels;

		ppfxRenderTarget = new S3DRenderTarget(engineDevice, createInfo);

		ppfxRenderPass = new S3DRenderPass(engineDevice, { AttachmentInfo{ppfxRenderTarget, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE }});
		ppfxSubFramebuffers.resize(layerCount);
		for (int i = 0; i < layerCount; i++) {
			ppfxSubFramebuffers[i].resize(mipLevels);
			for (int j = 0; j < mipLevels; j++) {
				ppfxSubFramebuffers[i][j] = new S3DFramebuffer(engineDevice, ppfxRenderPass, { ppfxRenderTarget }, i, j);
			}
		}
	}
}


