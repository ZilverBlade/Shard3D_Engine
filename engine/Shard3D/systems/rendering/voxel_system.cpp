#include "voxel_system.h"
#include "../handlers/resource_system.h"
#include "../handlers/render_list.h"
#include "light_propagation_volume_system.h"

namespace Shard3D {
	struct PushConstantVXGEN {
		glm::mat4 model;
		glm::mat4 normal; // + material id
	};

	struct PushConstantVXGI {
	alignas(16)glm::vec3 resolution;
	alignas(16)glm::vec3 extent; // l, w, h
	alignas(16)glm::vec3 centerToWorld;
	alignas(16)glm::vec3 energyConservationFactor;
		float rayMarchThresholdAlpha;
		float rayMarchThresholdDistance;
		uint32_t maxBounces;
		uint32_t dispatchSampleCount;
		uint32_t dispatchSampleOffset;
		float blend;
	};

	struct PrivateUBO {
		glm::mat4 viewProjX;
		glm::mat4 viewProjY;
		glm::mat4 viewProjZ;
	};

	const VkFormat VoxelFormat = VK_FORMAT_R16G16B16A16_SFLOAT;//VK_FORMAT_B10G11R11_UFLOAT_PACK32

	VoxelizerSystem::VoxelizerSystem(S3DDevice& engineDevice, ResourceSystem* resourceSystem, VkDescriptorSetLayout sceneBufferDescriptorLayout, glm::ivec3 voxelResolution, glm::mat4 transform, glm::vec3 extent, bool useRayTracing)
		: engineDevice(engineDevice), voxelResolution(voxelResolution) , resourceSystem(resourceSystem), transform(transform), extent(extent), useRayTracing(useRayTracing) {
		createVoxelImage(voxelResolution, VoxelFormat, voxelGIImage, voxelGIImageMemory, voxelGIImageView, nullptr, voxelGISampler, VK_FILTER_LINEAR);
		
		if (!useRayTracing) {
			createVoxelImage(voxelResolution, VK_FORMAT_R8G8B8A8_UNORM, voxelDiffuseDataImage, voxelDiffuseDataImageMemory, voxelDiffuseDataStorageImageView, &voxelDiffuseDataSamplerImageView, voxelDiffuseDataSampler, VK_FILTER_NEAREST);
			createVoxelImage(voxelResolution, VK_FORMAT_R16G16B16A16_SNORM, voxelNormalDataImage, voxelNormalDataImageMemory, voxelNormalDataStorageImageView, &voxelNormalDataSamplerImageView, voxelNormalDataSampler, VK_FILTER_NEAREST);
		}
		VkCommandBuffer commandBuffer = engineDevice.beginSingleTimeCommands();
		if (!useRayTracing) {
			int mipLevels = (int)std::floor(std::log2(std::max(voxelResolution.x, std::max(voxelResolution.y, voxelResolution.z)))) + 1;
			engineDevice.transitionImageLayout(commandBuffer, voxelDiffuseDataImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, 0, mipLevels, 0, 1);
			engineDevice.transitionImageLayout(commandBuffer, voxelNormalDataImage, VK_FORMAT_R16G16B16A16_SNORM, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, 0, mipLevels, 0, 1);
			engineDevice.transitionImageLayout(commandBuffer, voxelDiffuseDataImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 0, mipLevels, 0, 1);
			engineDevice.transitionImageLayout(commandBuffer, voxelNormalDataImage, VK_FORMAT_R16G16B16A16_SNORM, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 0, mipLevels, 0, 1);
		}
		engineDevice.transitionImageLayout(commandBuffer, voxelGIImage, VoxelFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, 0, 1, 0, 1);
		engineDevice.endSingleTimeCommands(commandBuffer);

		createPasses();
		createBuffers();
		createDescriptors();
		createPipelines(sceneBufferDescriptorLayout);

	}

	void VoxelizerSystem::generateSceneData(VkCommandBuffer commandBuffer, sPtr<Level>& level, RenderList* renderList) {
		if (!useRayTracing) {
			int mipLevels = (int)std::floor(std::log2(std::max(voxelResolution.x, std::max(voxelResolution.y, voxelResolution.z)))) + 1;
			engineDevice.transitionImageLayout(commandBuffer, voxelDiffuseDataImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_GENERAL, 0, mipLevels, 0, 1);
			engineDevice.transitionImageLayout(commandBuffer, voxelNormalDataImage, VK_FORMAT_R16G16B16A16_SNORM, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_GENERAL, 0, mipLevels, 0, 1);

			PrivateUBO ubo{};

			S3DCamera camera;
			camera.setOrthographicProjection(-extent.z, extent.z, extent.x, -extent.x, -extent.y, extent.y, false);
			camera.setViewDirection(transform[3], { 1.0, 0.0, 0.0 });

			ubo.viewProjX = camera.getProjectionView();

			camera.setViewDirection(transform[3], { 0.0, 1.0, 0.0 }, { 0.0, 0.0, -1.0 });
			ubo.viewProjY = camera.getProjectionView();

			camera.setViewDirection(transform[3], { 0.0, 0.0, 1.0 });
			ubo.viewProjZ = camera.getProjectionView();

			globalUBO->writeToBuffer(&ubo);
			globalUBO->flush();


			vxGenRenderPass->beginRenderPass(commandBuffer, vxGenFramebuffer);

			vxClearPipeline->bind(commandBuffer);


			vkCmdBindDescriptorSets(
				commandBuffer,
				VK_PIPELINE_BIND_POINT_GRAPHICS,
				vxClearPipelineLayout->getPipelineLayout(),
				0,
				1,
				&vxGenDescriptor,
				0,
				nullptr
			);

			vkCmdDraw(commandBuffer, 6, 1, 0, 0);

			vxGenPipeline->bind(commandBuffer);

			VkDescriptorSet sets[3]{
				globalUBODescriptor,
				vxGenDescriptor,
				resourceSystem->getDescriptor()->getDescriptor()
			};

			vkCmdBindDescriptorSets(
				commandBuffer,
				VK_PIPELINE_BIND_POINT_GRAPHICS,
				vxGenPipelineLayout->getPipelineLayout(),
				0,
				3,
				sets,
				0,
				nullptr
			);

			for (auto& entry : renderList->getIterator({ SurfaceMaterialPipelineClassPermutationOptions_Opaque, SurfaceMaterialPipelineClassPermutationOptions_Opaque | SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling }, MeshMobilityOption_Static)) {
				for (auto& [a, sf] : entry.renderList) {
					Mesh3D* model = resourceSystem->retrieveMesh(sf.mesh->mesh);
					for (uint32_t idx : sf.material_indices) {
						Material* material = resourceSystem->retrieveMaterial(sf.mesh->materials[idx]);
						uint32_t materialID = material->getShaderMaterialID();

						PushConstantVXGEN push{};
						push.model = sf.mesh->transform;
						push.normal = sf.mesh->normal;
						push.normal[3].x = *reinterpret_cast<float*>(&materialID);
						pushVXGEN.push(
							commandBuffer,
							vxGenPipelineLayout->getPipelineLayout(),
							push
						);

						model->draw(commandBuffer, idx);
					}
				}
			}

			vxGenRenderPass->endRenderPass(commandBuffer);

			engineDevice.transitionImageLayout(commandBuffer, voxelDiffuseDataImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 0, mipLevels, 0, 1);
			engineDevice.transitionImageLayout(commandBuffer, voxelNormalDataImage, VK_FORMAT_R16G16B16A16_SNORM, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 0, mipLevels, 0, 1);
			engineDevice.generateMipMaps(commandBuffer, voxelDiffuseDataImage, VK_FORMAT_R8G8B8A8_UNORM, voxelResolution.x, voxelResolution.y, voxelResolution.z, mipLevels, 1, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
			engineDevice.generateMipMaps(commandBuffer, voxelNormalDataImage, VK_FORMAT_R16G16B16A16_SNORM, voxelResolution.x, voxelResolution.y, voxelResolution.z, mipLevels, 1, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}

	}

	void VoxelizerSystem::generateGlobalIllumination(VkCommandBuffer commandBuffer, sPtr<Level>& level, RenderList* renderList, VkDescriptorSet sceneBuffer, uint32_t sampleCount, const VXPathTracedGIConfig& config) {
		vxGIShaderPipeline->bind(commandBuffer);

		VkDescriptorSet sets[2]{
			vxIlluminateDescriptor,
			sceneBuffer
		};

		vkCmdBindDescriptorSets(
			commandBuffer,
			VK_PIPELINE_BIND_POINT_COMPUTE,
			vxGIShaderPipelineLayout->getPipelineLayout(),
			0,
			2,
			sets,
			0,
			nullptr
		);

		const uint32_t MAX_SAMPLES_PER_DISPATCH = 8U;
		const uint32_t dispatchCount = sampleCount / MAX_SAMPLES_PER_DISPATCH;

		PushConstantVXGI push{};
		push.resolution = voxelResolution;
		push.extent = { extent .x, extent .z, extent .y};
		push.centerToWorld = transform[3];
		push.energyConservationFactor = config.energyConservationFactor; // light conserved per bounce
		push.rayMarchThresholdAlpha = 0.5f; // pointless since we are doing nearest filtering but idk
		push.rayMarchThresholdDistance = config.rayMarchThresholdDistance; // increase so that the acne is reduced, however dont increase too much or else there will be light leaking
		push.dispatchSampleCount = MAX_SAMPLES_PER_DISPATCH; // limit to prevent long shader execution times, resulting in VK_DEVICE_LOST. The slower the hardware, the lower this value should be
		push.maxBounces = config.maxBounces;
		for (int d = 0; d < dispatchCount; d++) {
			push.dispatchSampleOffset = d * MAX_SAMPLES_PER_DISPATCH;
			push.blend = 1.0f / float(d + 1);
			SHARD3D_INFO("dispatching VXGI bake no {0}", d);
			vkCmdPushConstants(
				commandBuffer,
				vxGIShaderPipelineLayout->getPipelineLayout(),
				VK_SHADER_STAGE_COMPUTE_BIT,
				0,
				sizeof(PushConstantVXGI),
				&push
			);

			vkCmdDispatch(commandBuffer, voxelResolution.x / 8, voxelResolution.y / 8, voxelResolution.z / 8);
		}
	}

	void VoxelizerSystem::clearGlobalIllumination(VkCommandBuffer commandBuffer) {
		iterations = 0;

	}

	void VoxelizerSystem::generateGlobalIlluminationProgressive(VkCommandBuffer commandBuffer, sPtr<Level>& level, RenderList* renderList, VkDescriptorSet sceneBuffer, uint32_t samplesPerFrame, const VXPathTracedGIConfig& config) {
		vxGIShaderPipeline->bind(commandBuffer);

		VkDescriptorSet sets[3]{
			vxIlluminateDescriptor,
			sceneBuffer
		};

		vkCmdBindDescriptorSets(
			commandBuffer,
			VK_PIPELINE_BIND_POINT_COMPUTE,
			vxGIShaderPipelineLayout->getPipelineLayout(),
			0,
			2,
			sets,
			0,
			nullptr
		);

		PushConstantVXGI push{};
		push.resolution = voxelResolution;
		push.extent = { extent.x, extent.z, extent.y };
		push.centerToWorld = transform[3];
		push.energyConservationFactor = config.energyConservationFactor; // light conserved per bounce
		push.rayMarchThresholdAlpha = 0.5f; // pointless since we are doing nearest filtering but idk
		push.rayMarchThresholdDistance = config.rayMarchThresholdDistance; // increase so that the acne is reduced, however dont increase too much or else there will be light leaking
		push.dispatchSampleCount = samplesPerFrame; // limit to prevent long shader execution times, resulting in VK_DEVICE_LOST. The slower the hardware, the lower this value should be
		push.maxBounces = config.maxBounces;

		push.dispatchSampleOffset = iterations * samplesPerFrame;
		push.blend = 1.0f / float(iterations + 1);
		vkCmdPushConstants(
			commandBuffer,
			vxGIShaderPipelineLayout->getPipelineLayout(),
			VK_SHADER_STAGE_COMPUTE_BIT,
			0,
			sizeof(PushConstantVXGI),
			&push
		);

		vkCmdDispatch(commandBuffer, voxelResolution.x / 8, voxelResolution.y / 8, voxelResolution.z / 8);
		iterations++;
	}

	void VoxelizerSystem::createVoxelImage(glm::ivec3 resolution, VkFormat format, VkImage& image, VkDeviceMemory& memory, VkImageView& imageViewStorage, VkImageView* imageViewSampler, VkSampler& sampler, VkFilter filter) {
		int mipLevels = (int)std::floor(std::log2(std::max(voxelResolution.x, std::max(voxelResolution.y, voxelResolution.z)))) + 1;
		VkImageCreateInfo imageCreateInfo{};
		imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageCreateInfo.imageType = VK_IMAGE_TYPE_3D;
		imageCreateInfo.format = format;
		imageCreateInfo.extent.width = resolution.x;
		imageCreateInfo.extent.height = resolution.y;
		imageCreateInfo.extent.depth = resolution.z;
		imageCreateInfo.mipLevels = imageViewSampler ? mipLevels : 1;
		imageCreateInfo.arrayLayers = 1;
		imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
		imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
		imageCreateInfo.usage = VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
		imageCreateInfo.flags = 0;
		VkMemoryAllocateInfo memAlloc = {};
		memAlloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		VkMemoryRequirements memReqs;

		VK_ASSERT(vkCreateImage(engineDevice.device(), &imageCreateInfo, nullptr, &image), "Failed to create image!");
		vkGetImageMemoryRequirements(engineDevice.device(), image, &memReqs);
		memAlloc.memoryTypeIndex = engineDevice.findMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		memAlloc.allocationSize = memReqs.size;

		if (vkAllocateMemory(engineDevice.device(), &memAlloc, nullptr, &memory) != VK_SUCCESS) {
			SHARD3D_ERROR("Ran out of memory!");
			return;
		}

		VK_ASSERT(vkBindImageMemory(engineDevice.device(), image, memory, 0), "Failed to bind memory!");

		VkImageViewCreateInfo imageViewCreateInfo = {};
		imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_3D;
		imageViewCreateInfo.format = format;
		imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
		imageViewCreateInfo.subresourceRange.levelCount = 1;
		imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
		imageViewCreateInfo.subresourceRange.layerCount = 1;
		imageViewCreateInfo.image = image;

		VK_ASSERT(vkCreateImageView(engineDevice.device(), &imageViewCreateInfo, nullptr, &imageViewStorage), "Failed to create image view!");

		if (imageViewSampler) {
			VkImageViewCreateInfo imageViewCreateInfoS = {};
			imageViewCreateInfoS.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
			imageViewCreateInfoS.viewType = VK_IMAGE_VIEW_TYPE_3D;
			imageViewCreateInfoS.format = format;
			imageViewCreateInfoS.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			imageViewCreateInfoS.subresourceRange.baseMipLevel = 0;
			imageViewCreateInfoS.subresourceRange.levelCount = mipLevels;
			imageViewCreateInfoS.subresourceRange.baseArrayLayer = 0;
			imageViewCreateInfoS.subresourceRange.layerCount = 1;
			imageViewCreateInfoS.image = image;

			VK_ASSERT(vkCreateImageView(engineDevice.device(), &imageViewCreateInfoS, nullptr, imageViewSampler), "Failed to create image view!");
		}

		VkSamplerCreateInfo samplerInfo = {};
		samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		samplerInfo.magFilter = filter;
		samplerInfo.minFilter = filter;
		samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
		samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
		samplerInfo.addressModeV = samplerInfo.addressModeU;
		samplerInfo.addressModeW = samplerInfo.addressModeU;
		samplerInfo.borderColor = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK;
		samplerInfo.mipLodBias = 0.0f;
		samplerInfo.maxAnisotropy = 1.0f;
		samplerInfo.minLod = 0.0f;
		samplerInfo.maxLod = float(mipLevels - 1);

		VK_ASSERT(vkCreateSampler(engineDevice.device(), &samplerInfo, nullptr, &sampler), "Failed to create sampler");
	}

	void VoxelizerSystem::createAccelerationStructure() {
		VkDeviceSize shouldbeenough = 64000000ui64; // ~64mb
		VkAccelerationStructureCreateInfoKHR createInfoBLAS{};
		createInfoBLAS.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR;
		createInfoBLAS.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
		//createInfoBLAS.buffer
		vkCreateAccelerationStructureKHR(engineDevice.device(), &createInfoBLAS, nullptr, &BLAS_rt);
	}



	void VoxelizerSystem::createPasses() {
		S3DRenderTargetCreateInfo createInfo{};
		createInfo.dimensions = { voxelResolution.x, voxelResolution.y, 1 };
		createInfo.renderTargetType = S3DRenderTargetType::Depth;
		createInfo.format = VK_FORMAT_D16_UNORM;
		createInfo.imageAspect = VK_IMAGE_ASPECT_DEPTH_BIT;
		createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		createInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
		createInfo.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		vxGenFramebufferAttachment = new S3DRenderTarget(engineDevice, createInfo);

		AttachmentInfo info{};
		info.clear.depth = { 1.0f, 1 };
		info.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		info.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		info.framebufferAttachment = vxGenFramebufferAttachment;
		vxGenRenderPass = new S3DRenderPass(engineDevice, { info });

		vxGenFramebuffer = new S3DFramebuffer(engineDevice, vxGenRenderPass, { vxGenFramebufferAttachment });
	}
	void VoxelizerSystem::createBuffers() {
		globalUBO = make_uPtr<S3DBuffer>(
				engineDevice,
				sizeof(PrivateUBO),
				1,
				VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
				);
		globalUBO->map();
	}
	void VoxelizerSystem::createDescriptors() {
		localPool = S3DDescriptorPool::Builder(engineDevice)
			.setMaxSets(3)
			.addPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1)
			.addPoolSize(VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 3)
			.addPoolSize(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 2)
			.build();

		if (!useRayTracing) {
			globalUBODescriptorLayout = S3DDescriptorSetLayout::Builder(engineDevice)
				.addBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_GEOMETRY_BIT)
				.build();

			auto bufferInfo = globalUBO->descriptorInfo();
			S3DDescriptorWriter(*globalUBODescriptorLayout, *localPool)
				.writeBuffer(0, &bufferInfo)
				.build(globalUBODescriptor);


			vxGenDescriptorLayout = S3DDescriptorSetLayout::Builder(engineDevice)
				.addBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_FRAGMENT_BIT)
				.addBinding(1, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_FRAGMENT_BIT)
				.build();

			VkDescriptorImageInfo imageInfoN = {
				VK_NULL_HANDLE,
				voxelNormalDataStorageImageView,
				VK_IMAGE_LAYOUT_GENERAL
			};
			VkDescriptorImageInfo imageInfoD = {
				VK_NULL_HANDLE,
				voxelDiffuseDataStorageImageView,
				VK_IMAGE_LAYOUT_GENERAL
			};
			S3DDescriptorWriter(*vxGenDescriptorLayout, *localPool)
				.writeImage(0, &imageInfoN)
				.writeImage(1, &imageInfoD)
				.build(vxGenDescriptor);

			vxIlluminateDescriptorLayout = S3DDescriptorSetLayout::Builder(engineDevice)
				.addBinding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_COMPUTE_BIT)
				.addBinding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_COMPUTE_BIT)
				.addBinding(2, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_COMPUTE_BIT)
				.build();

			VkDescriptorImageInfo imageInfoSamplerN = {
				voxelNormalDataSampler,
				voxelNormalDataSamplerImageView,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
			};
			VkDescriptorImageInfo imageInfoSamplerD = {
				voxelDiffuseDataSampler,
				voxelDiffuseDataSamplerImageView,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
			};
			VkDescriptorImageInfo imageInfoG = {
				VK_NULL_HANDLE,
				voxelGIImageView,
				VK_IMAGE_LAYOUT_GENERAL
			};
			S3DDescriptorWriter(*vxIlluminateDescriptorLayout, *localPool)
				.writeImage(0, &imageInfoSamplerN)
				.writeImage(1, &imageInfoSamplerD)
				.writeImage(2, &imageInfoG)
				.build(vxIlluminateDescriptor);
		} else {
			vxIlluminateDescriptorLayout = S3DDescriptorSetLayout::Builder(engineDevice)
				.addBinding(0, VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR, VK_SHADER_STAGE_COMPUTE_BIT)
				.addBinding(1, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_COMPUTE_BIT)
				.build();

			VkWriteDescriptorSetAccelerationStructureKHR accelerationWrite = {};
			accelerationWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_KHR;
			accelerationWrite.pAccelerationStructures = &TLAS_rt;
			accelerationWrite.accelerationStructureCount = 1;

			VkDescriptorImageInfo imageInfoG = {
				VK_NULL_HANDLE,
				voxelGIImageView,
				VK_IMAGE_LAYOUT_GENERAL
			};
			S3DDescriptorWriter(*vxIlluminateDescriptorLayout, *localPool)
				.write(0, &accelerationWrite)
				.writeImage(1, &imageInfoG)
				.build(vxIlluminateDescriptor);
		}
		
		
	}
	void VoxelizerSystem::createPipelines(VkDescriptorSetLayout sceneSetLayout) {
		pushVXGEN = S3DPushConstant(sizeof(PushConstantVXGEN), VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_GEOMETRY_BIT);
		pushVXGI = S3DPushConstant(sizeof(PushConstantVXGI), VK_SHADER_STAGE_COMPUTE_BIT);

		std::vector<VkDescriptorSetLayout> descriptorSetLayoutsVXGEN{
			globalUBODescriptorLayout->getDescriptorSetLayout(),
			vxGenDescriptorLayout->getDescriptorSetLayout(),
			resourceSystem->getDescriptor()->getLayout()
		};

		vxGenPipelineLayout = make_uPtr<S3DPipelineLayout>(engineDevice, std::vector<VkPushConstantRange>{pushVXGEN.getRange()}, descriptorSetLayoutsVXGEN);

		S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo();
		pipelineConfig.enableVertexDescriptions(VertexDescriptionOptions_Position| VertexDescriptionOptions_Normal | VertexDescriptionOptions_UV);
		pipelineConfig.disableDepthTest();
		pipelineConfig.disableDepthWrite();

		VkPipelineRasterizationConservativeStateCreateInfoEXT conservativeRaster{};
		conservativeRaster.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_CONSERVATIVE_STATE_CREATE_INFO_EXT;
		conservativeRaster.conservativeRasterizationMode = VK_CONSERVATIVE_RASTERIZATION_MODE_OVERESTIMATE_EXT;
		conservativeRaster.flags = 0;
		pipelineConfig.rasterizationInfo.pNext = &conservativeRaster;

		pipelineConfig.renderPass = vxGenRenderPass->getRenderPass();
		pipelineConfig.pipelineLayout = vxGenPipelineLayout->getPipelineLayout();
		vxGenPipeline = make_uPtr<S3DGraphicsPipeline>(
			engineDevice, std::vector<S3DShader>{
			S3DShader{ ShaderType::Vertex, "resources/shaders/editor/bake_vxgen.vert", "editor" },
			S3DShader{ ShaderType::Geometry, "resources/shaders/editor/bake_vxgen.geom", "editor" },
			S3DShader{ ShaderType::Fragment, "resources/shaders/editor/bake_vxgen.frag", "editor" },
		},
		pipelineConfig
		);

		std::vector<VkDescriptorSetLayout> descriptorSetLayoutsVXGI{
			vxIlluminateDescriptorLayout->getDescriptorSetLayout(),
			sceneSetLayout
		};

		vxGIShaderPipelineLayout = make_uPtr<S3DPipelineLayout>(engineDevice, std::vector<VkPushConstantRange>{pushVXGI.getRange()}, descriptorSetLayoutsVXGI);
		vxGIShaderPipeline = make_uPtr<S3DComputePipeline>(engineDevice, vxGIShaderPipelineLayout->getPipelineLayout(), S3DShader{ ShaderType::Compute, "resources/shaders/editor/bake_gi_voxel.comp", "editor" });

		vxClearPipelineLayout = make_uPtr<S3DPipelineLayout>(engineDevice, std::vector<VkPushConstantRange>{}, std::vector<VkDescriptorSetLayout>{ vxGenDescriptorLayout->getDescriptorSetLayout() });

		S3DGraphicsPipelineConfigInfo pipelineConfigClear = S3DGraphicsPipelineConfigInfo();
		pipelineConfigClear.disableDepthTest();
		pipelineConfigClear.disableDepthWrite();

		pipelineConfigClear.renderPass = vxGenRenderPass->getRenderPass();
		pipelineConfigClear.pipelineLayout = vxClearPipelineLayout->getPipelineLayout();
		vxClearPipeline = make_uPtr<S3DGraphicsPipeline>(
			engineDevice, std::vector<S3DShader>{
			S3DShader{ ShaderType::Vertex, "resources/shaders/fullscreen_quad.vert", "misc" },
			S3DShader{ ShaderType::Fragment, "resources/shaders/editor/bake_vxclear.frag", "editor" },
		},
		pipelineConfigClear
		);

	}

}