#include "volumetric_lighting_system.h"

namespace Shard3D::Systems {
	struct VolumetricLightingPush {
		uint32_t lightIndex;
		float gScattering;
		float cScattering;
		float padd;
		glm::vec3 tint;
	};
	struct VolumetricSpecializationData {
		uint32_t lightType;
		uint32_t maxSteps;
	};
	struct BlurPush {
		glm::vec2 resolution;
		glm::vec2 direction;
	};
	
	const static VkFormat VOLUMETRIC_LIGHTING_FORMAT = VK_FORMAT_B10G11R11_UFLOAT_PACK32;

	VolumetricLightingSystem::VolumetricLightingSystem(
		S3DDevice& device, ResourceSystem* resourceSystem, glm::vec2 resolution, const SceneBufferInputData& gbuffer,
		VkRenderPass compositionRenderPass, VkDescriptorSetLayout globalSetLayout, VkDescriptorSetLayout sceneSetLayout
	) : engineDevice(device), resourceSystem(resourceSystem) {
		resourceSystem->loadMesh(AssetID("engine/meshes/spotlightcone.fbx.s3dasset"));
		resourceSystem->loadMesh(AssetID("engine/meshes/pointlightsphere.fbx.s3dasset"));
		createAttachments(resolution);
		createRenderPass();
		createFramebuffer();
		createBlurPasses();
		createDescriptorSetLayout();
		createPipelines(globalSetLayout, sceneSetLayout, compositionRenderPass);
		writeDescriptors(gbuffer.depthSceneInfo->getDescriptor());
	}

	VolumetricLightingSystem::~VolumetricLightingSystem() {
		engineDevice.staticMaterialPool->freeDescriptors({ compositingInputDescriptorSet, volumetricLightingInputDescriptorSet });

		delete volumetricLightingRenderPass;
		delete volumetricLightAttachment;
		delete volumetricLightFramebuffer;
	}


	void VolumetricLightingSystem::drawLight(FrameInfo& frameInfo, uint32_t lightType, uint32_t lightIndex) {
		VolumetricLightingPush push;
		push.lightIndex = lightIndex;
		push.cScattering = frameInfo.camera.postfx.mist.constantScattering;
		push.gScattering = frameInfo.camera.postfx.mist.mieScattering;
		push.tint = frameInfo.camera.postfx.mist.tint;
		volumetricLightingPush.push(frameInfo.commandBuffer, volumetricLightingPipelineLayout->getPipelineLayout(), push);
		switch (lightType) {
		case(0):
			vkCmdDraw(frameInfo.commandBuffer, 6, 1, 0, 0);
			break;
		case(1):
			resourceSystem->retrieveMesh(AssetID("engine/meshes/spotlightcone.fbx.s3dasset"))->drawAll(frameInfo.commandBuffer);
			break;
		case(2):
			resourceSystem->retrieveMesh(AssetID("engine/meshes/pointlightsphere.fbx.s3dasset"))->drawAll(frameInfo.commandBuffer);
			break;
		}
	}


	void VolumetricLightingSystem::render(FrameInfo& frameInfo) {
		if (!frameInfo.camera.postfx.mist.enable) return;
		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::BEGIN_RENDER_PASS, "VOLUMETRIC_LIGHTS", frameInfo.commandBuffer);
		volumetricLightingRenderPass->beginRenderPass(frameInfo.commandBuffer, volumetricLightFramebuffer);
		VkDescriptorSet sets[4]{
			frameInfo.globalDescriptorSet,
			frameInfo.sceneDescriptorSet,
			resourceSystem->getDescriptor()->getDescriptor(),
			volumetricLightingInputDescriptorSet
		};
		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			volumetricLightingPipelineLayout->getPipelineLayout(),
			0,
			4,
			sets,
			0,
			nullptr
		);

		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::GENERIC, "VOLUMETRIC_POINTLIGHT", frameInfo.commandBuffer);
		{
			volumetricLightingPointLightPipeline->bind(frameInfo.commandBuffer);
			int lightIndex = 0;
			frameInfo.level->registry.view<Components::PointLightComponent, RENDER_ITER>().each([&](Components::PointLightComponent light, Components::TransformComponent transform, auto& visibility) {
				if (light.volumetric) drawLight(frameInfo, 2, lightIndex);
				lightIndex += 1;
			});
		}
		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::GENERIC, "VOLUMETRIC_SPOTLIGHT", frameInfo.commandBuffer);
		{
			volumetricLightingSpotLightPipeline->bind(frameInfo.commandBuffer);
			int lightIndex = 0;
			frameInfo.level->registry.view<Components::SpotLightComponent, RENDER_ITER>().each([&](Components::SpotLightComponent light, Components::TransformComponent transform, auto& visibility) {
				if (light.volumetric) drawLight(frameInfo, 1, lightIndex);
				lightIndex += 1;
			});
		}
		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::GENERIC, "VOLUMETRIC_DIRLIGHT", frameInfo.commandBuffer);
		{
			volumetricLightingDirectionalLightPipeline->bind(frameInfo.commandBuffer);
			int lightIndex = 0;
			frameInfo.level->registry.view<Components::DirectionalLightComponent, RENDER_ITER>().each([&](Components::DirectionalLightComponent light, Components::TransformComponent transform, auto& visibility) {
				if (light.volumetric) drawLight(frameInfo, 0, lightIndex);
				lightIndex += 1;
			});
		}
		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::END_RENDER_PASS, "VOLUMETRIC_LIGHTS", frameInfo.commandBuffer);
		volumetricLightingRenderPass->endRenderPass(frameInfo.commandBuffer);
	}

	void VolumetricLightingSystem::blur(FrameInfo& frameInfo) {
		if (!frameInfo.camera.postfx.mist.enable) return;
		BlurPush push;
		push.resolution = blurPassH->getAttachment()->getDimensions();
		push.direction = { 1.0, 0.0 };
		blurPassH->render(frameInfo.commandBuffer, push);
		push.direction = { 0.0, 1.0 };
		blurPassV->render(frameInfo.commandBuffer, push);
	}

	void VolumetricLightingSystem::composite(FrameInfo& frameInfo) {
		if (!frameInfo.camera.postfx.mist.enable) return;
		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			compositingPipelineLayout->getPipelineLayout(),
			0,
			1,
			&compositingInputDescriptorSet,
			0,
			nullptr
		);
		compositingPipeline->bind(frameInfo.commandBuffer);
		vkCmdDraw(frameInfo.commandBuffer, 6, 1, 0, 0);
	}

	void VolumetricLightingSystem::resize(glm::vec2 newSize, const SceneBufferInputData& gbuffer) {
		volumetricLightFramebuffer->resize({ newSize, 1 }, volumetricLightingRenderPass);
		blurPassH->resize(newSize, { volumetricLightAttachment->getDescriptor() });
		blurPassV->resize(newSize, { blurPassH->getAttachment()->getDescriptor() });
		writeDescriptors(gbuffer.depthSceneInfo->getDescriptor());
	}

	void VolumetricLightingSystem::createAttachments(glm::vec2 resolution) {
		S3DRenderTargetCreateInfo createInfo{};
		createInfo.dimensions = { resolution, 1 };
		createInfo.format = VK_FORMAT_B10G11R11_UFLOAT_PACK32;
		createInfo.renderTargetType = S3DRenderTargetType::Color;
		createInfo.imageAspect = VK_IMAGE_ASPECT_COLOR_BIT;
		createInfo.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
		createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		createInfo.linearFiltering = true; 
		createInfo.layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

		volumetricLightAttachment = new S3DRenderTarget(
			engineDevice,
			createInfo
		);
	}

	void VolumetricLightingSystem::createRenderPass() {
		AttachmentInfo info{};
		info.framebufferAttachment = volumetricLightAttachment;
		info.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		info.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		info.clear.color = { 0.0f, 0.0f, 0.0f };
		volumetricLightingRenderPass = new S3DRenderPass(engineDevice, { info });
	}

	void VolumetricLightingSystem::createFramebuffer() {
		volumetricLightFramebuffer = new S3DFramebuffer(engineDevice, volumetricLightingRenderPass, { volumetricLightAttachment });
	}

	void VolumetricLightingSystem::createDescriptorSetLayout() {
		volumetricLightingInputDescriptorSetLayout = S3DDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT)
			.build();
		compositingInputDescriptorSetLayout = S3DDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT)
			.build();
	}

	void VolumetricLightingSystem::createPipelines(VkDescriptorSetLayout globalSetLayout, VkDescriptorSetLayout sceneSetLayout, VkRenderPass renderPass) {
		{
			volumetricLightingPush = S3DPushConstant(sizeof(VolumetricLightingPush), VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT);
			volumetricLightingPipelineLayout = make_uPtr<S3DPipelineLayout>(
				engineDevice, std::vector<VkPushConstantRange>{volumetricLightingPush.getRange()},
				std::vector<VkDescriptorSetLayout>{globalSetLayout, sceneSetLayout,
				resourceSystem->getDescriptor()->getLayout(), volumetricLightingInputDescriptorSetLayout->getDescriptorSetLayout()}
			);

			S3DGraphicsPipelineConfigInfo configInfo{};
			configInfo.disableDepthTest();
			configInfo.setCullMode(VK_CULL_MODE_BACK_BIT);
			configInfo.renderPass = volumetricLightingRenderPass->getRenderPass();
			configInfo.pipelineLayout = volumetricLightingPipelineLayout->getPipelineLayout();
			configInfo.colorBlendAttachments[0].blendEnable = VK_TRUE;
			configInfo.colorBlendAttachments[0].alphaBlendOp = VK_BLEND_OP_ADD;
			configInfo.colorBlendAttachments[0].colorBlendOp = VK_BLEND_OP_ADD;
			configInfo.colorBlendAttachments[0].srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
			configInfo.colorBlendAttachments[0].dstColorBlendFactor = VK_BLEND_FACTOR_ONE;
			configInfo.colorBlendAttachments[0].srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
			configInfo.colorBlendAttachments[0].dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;

			VolumetricSpecializationData specializationData;
			specializationData.lightType = 0;
			specializationData.maxSteps = ProjectSystem::getGraphicsSettings().shadows.volumetric.rayStepsDirectional;

			S3DShaderSpecialization specializationInfoVolumetrics;
			specializationInfoVolumetrics.addConstant(0, &specializationData.lightType, sizeof(uint32_t));
			specializationInfoVolumetrics.addConstant(1, &specializationData.maxSteps, sizeof(uint32_t));

			volumetricLightingDirectionalLightPipeline = make_uPtr<S3DGraphicsPipeline>(
				engineDevice, std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/rendering/dir_light_quad.vert", "rendering", {}, {"VOLUMETRICS_SAMPLING_POSITION"}, "volumetric_lighting_dir.vert" },
				S3DShader{ ShaderType::Fragment, "resources/shaders/rendering/volumetric_lighting.frag", "rendering", specializationInfoVolumetrics }
			}, configInfo);

			configInfo.setCullMode(VK_CULL_MODE_FRONT_BIT);
			configInfo.enableVertexDescriptions(VertexDescriptionOptions_Position);
			specializationData.lightType = 1;
			specializationData.maxSteps = ProjectSystem::getGraphicsSettings().shadows.volumetric.rayStepsSpot;

			specializationInfoVolumetrics = {};
			specializationInfoVolumetrics.addConstant(0, &specializationData.lightType, sizeof(uint32_t));
			specializationInfoVolumetrics.addConstant(1, &specializationData.maxSteps, sizeof(uint32_t));

			volumetricLightingSpotLightPipeline = make_uPtr<S3DGraphicsPipeline>(
				engineDevice, std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/rendering/spot_light_cone.vert", "rendering", {}, {"VOLUMETRICS_SAMPLING_POSITION"}, "volumetric_lighting_spot.vert"},
					S3DShader{ ShaderType::Fragment, "resources/shaders/rendering/volumetric_lighting.frag", "rendering", specializationInfoVolumetrics }
			}, configInfo); 

			specializationData.lightType = 2;
			specializationData.maxSteps = ProjectSystem::getGraphicsSettings().shadows.volumetric.rayStepsPoint;

			specializationInfoVolumetrics = {};
			specializationInfoVolumetrics.addConstant(0, &specializationData.lightType, sizeof(uint32_t));
			specializationInfoVolumetrics.addConstant(1, &specializationData.maxSteps, sizeof(uint32_t));

			volumetricLightingPointLightPipeline = make_uPtr<S3DGraphicsPipeline>(
				engineDevice, std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/rendering/point_light_sphere.vert", "rendering", {}, {"VOLUMETRICS_SAMPLING_POSITION"}, "volumetric_lighting_point.vert" },
					S3DShader{ ShaderType::Fragment, "resources/shaders/rendering/volumetric_lighting.frag", "rendering", specializationInfoVolumetrics }
			}, configInfo);
		}
		{
			compositingPipelineLayout = make_uPtr<S3DPipelineLayout>(
				engineDevice, std::vector<VkPushConstantRange>{},
				std::vector<VkDescriptorSetLayout>{compositingInputDescriptorSetLayout->getDescriptorSetLayout()}
			);

			S3DGraphicsPipelineConfigInfo configInfo{};
			configInfo.disableDepthTest();
			configInfo.setCullMode(VK_CULL_MODE_BACK_BIT);
			configInfo.renderPass = renderPass;
			configInfo.pipelineLayout = compositingPipelineLayout->getPipelineLayout();
			configInfo.colorBlendAttachments[0].blendEnable = VK_TRUE;
			configInfo.colorBlendAttachments[0].alphaBlendOp = VK_BLEND_OP_ADD;
			configInfo.colorBlendAttachments[0].colorBlendOp = VK_BLEND_OP_ADD;
			configInfo.colorBlendAttachments[0].srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
			configInfo.colorBlendAttachments[0].dstColorBlendFactor = VK_BLEND_FACTOR_ONE;
			configInfo.colorBlendAttachments[0].srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
			configInfo.colorBlendAttachments[0].dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;

			compositingPipeline = make_uPtr<S3DGraphicsPipeline>(
				engineDevice, std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/fullscreen_quad.vert", "misc" },
				S3DShader{ ShaderType::Fragment, "resources/shaders/postfx/volumetric_lighting_composite.frag", "rendering" }
			}, configInfo);
		}
	}

	void VolumetricLightingSystem::createBlurPasses() {
		blurPassH = make_uPtr<PostProcessingEffect>(
			engineDevice,
			volumetricLightFramebuffer->getDimensions(),
			S3DShader{ ShaderType::Fragment,  "resources/shaders/postfx/fast_gaussian_9rgb.frag", "postfx" },
			std::vector<VkDescriptorImageInfo>{ volumetricLightAttachment->getDescriptor() },
			VOLUMETRIC_LIGHTING_FORMAT
		);
		blurPassV = make_uPtr<PostProcessingEffect>(
			engineDevice,
			volumetricLightFramebuffer->getDimensions(),
			S3DShader{ ShaderType::Fragment,  "resources/shaders/postfx/fast_gaussian_9rgb.frag", "postfx" },
			std::vector<VkDescriptorImageInfo>{ blurPassH->getAttachment()->getDescriptor() },
			VOLUMETRIC_LIGHTING_FORMAT
		);
	}

	void VolumetricLightingSystem::writeDescriptors(VkDescriptorImageInfo depthImageInfo) {
		S3DDescriptorWriter(*volumetricLightingInputDescriptorSetLayout, *engineDevice.staticMaterialPool)
			.writeImage(0, &depthImageInfo)
			.build(volumetricLightingInputDescriptorSet);
		S3DDescriptorWriter(*compositingInputDescriptorSetLayout, *engineDevice.staticMaterialPool)
			.writeImage(0, &blurPassV->getAttachment()->getDescriptor())
			.build(compositingInputDescriptorSet);
	}

}