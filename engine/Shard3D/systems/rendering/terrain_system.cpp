#include "terrain_system.h"
#include "../../core/ecs/actor.h"
#include "../handlers/resource_system.h"
#include "../handlers/vertex_arena_allocator.h"
#include <glm/gtx/hash.hpp>
#include <ktx.h>
#include <ktxvulkan.h>
#include <zstd.h>
#include "../post_fx/post_processing_fx.h"

namespace Shard3D {
	const float TERRAIN_QUALITY = 32.0f;
	struct TerrainUpdatePushData {
		std::array<glm::vec4, 6> frustumPlanes;
		glm::vec3 cameraPosition;
		float cameraFocalZoom;
		float terrainQuality;
		float lodBias;
	};


	TerrainTexture::TerrainTexture(S3DDevice& device)
		: device(device) {
	}

	TerrainTexture::~TerrainTexture() {
		vkDestroyImage(device.device(), tileImage, nullptr);
		vkFreeMemory(device.device(), tileImageMemory, nullptr);
		vkDestroyImageView(device.device(), tileImageView, nullptr);
		vkDestroySampler(device.device(), tileSampler, nullptr);
	}

	void TerrainTexture::createTexture(glm::ivec2 resolution, int levels, VkFormat editorFormat, VkFormat gameFormat) {
		this->editorFormat = editorFormat;
		this->gameFormat = gameFormat;
		this->resolution = resolution;
		this->mipLevels = levels;
		VkImageCreateInfo imageCreateInfo{};
		imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
		imageCreateInfo.format = editorFormat;
		imageCreateInfo.extent.width = resolution.x;
		imageCreateInfo.extent.height = resolution.y;
		imageCreateInfo.extent.depth = 1;
		imageCreateInfo.mipLevels = mipLevels;
		imageCreateInfo.arrayLayers = 1;
		imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
		imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
		imageCreateInfo.usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
		imageCreateInfo.usage |= VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
		imageCreateInfo.flags = 0;
		VkMemoryAllocateInfo memAlloc = {};
		memAlloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		VkMemoryRequirements memReqs;

		VK_ASSERT(vkCreateImage(device.device(), &imageCreateInfo, nullptr, &tileImage), "Failed to create image!");
		vkGetImageMemoryRequirements(device.device(), tileImage, &memReqs);
		memAlloc.memoryTypeIndex = device.findMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		memAlloc.allocationSize = memReqs.size;
		sizeBytes = memReqs.size;

		if (vkAllocateMemory(device.device(), &memAlloc, nullptr, &tileImageMemory) != VK_SUCCESS) {
			SHARD3D_ERROR("Ran out of memory!");
			return;
		}

		VK_ASSERT(vkBindImageMemory(device.device(), tileImage, tileImageMemory, 0), "Failed to bind memory!");
		createImageView();
		recreateSampler();
		VkCommandBuffer commandBuffer = device.beginSingleTimeCommands();
		device.transitionImageLayout(commandBuffer, tileImage, editorFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, 0, mipLevels, 0, 1);
		device.endSingleTimeCommands(commandBuffer);
	}

	void TerrainTexture::loadTexture(ktxTexture2* texture, VkFormat editorFormat, glm::uvec2 editorResolution) {
		S3DQueue* queue = device.getAvailableQueue(QueueType::Graphics);
		ktxVulkanDeviceInfo* dvcInfo = ktxVulkanDeviceInfo_Create(device.physicalDevice(), device.device(), queue->queue, device.getCommandPool(), nullptr);
		ktxVulkanTexture ktxVkTex{};
		ktxVkTex.depth = 1;
		ktxVkTex.width = texture->baseWidth;
		ktxVkTex.height = texture->baseHeight;
		this->editorFormat = editorFormat;
		gameFormat = ktxVkTex.imageFormat = (VkFormat)texture->vkFormat;
		ktxVkTex.viewType = VK_IMAGE_VIEW_TYPE_2D;
		ktxVkTex.levelCount = texture->numLevels;

		auto erc = ktxTexture2_VkUploadEx(texture, dvcInfo, &ktxVkTex,
			VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		SHARD3D_ASSERT(erc == KTX_SUCCESS);
		ktxVulkanDeviceInfo_Destroy(dvcInfo);
		device.freeAvailableQueue(queue);
		if (!ProjectSystem::isEditorMode()) {
			recreateSampler();
			tileImage = ktxVkTex.image;
			tileImageMemory = ktxVkTex.deviceMemory;
			imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			sizeBytes = texture->dataSize;
			resolution = { texture->baseWidth, texture->baseHeight };
		} else {
			createTexture(editorResolution, editorFormat == VK_FORMAT_R32_SFLOAT ? texture->numLevels : 3, editorFormat, gameFormat);

			VkImageViewCreateInfo imageViewCreateInfo = {};
			imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
			imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
			imageViewCreateInfo.format = gameFormat;
			imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
			imageViewCreateInfo.subresourceRange.levelCount = 1;
			imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
			imageViewCreateInfo.subresourceRange.layerCount = 1;
			imageViewCreateInfo.image = ktxVkTex.image;
			VkImageView ktxView;
			VK_ASSERT(vkCreateImageView(device.device(), &imageViewCreateInfo, nullptr, &ktxView), "Failed to create image view!");

			PostProcessingEffect transcodeToEditor = PostProcessingEffect(
				device, editorResolution,
				S3DShader(ShaderType::Fragment,
					editorFormat == VK_FORMAT_R32_SFLOAT ? "resources/shaders/editor/terrain_transcode_height_map_r16f-r32f.frag" :
					"resources/shaders/editor/terrain_transcode_material_map_rgba8u-rgba16u.frag", "editor", {}),
				{ VkDescriptorImageInfo{tileSampler, ktxView,VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL } }, editorFormat
			);
			VkCommandBuffer commandBuffer = device.beginSingleTimeCommands();
			transcodeToEditor.render(commandBuffer);
			device.transitionImageLayout(commandBuffer, transcodeToEditor.getAttachment()->getImage(),
				gameFormat, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, 0, 1, 0, 1);
			VkImageSubresourceLayers subres;
			subres.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			subres.baseArrayLayer = 0;
			subres.layerCount = 1;
			subres.mipLevel = 0;
			device.copyImage(commandBuffer, { editorResolution.x, editorResolution.y, 1 }, transcodeToEditor.getAttachment()->getImage(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
				subres, {}, tileImage, VK_IMAGE_LAYOUT_GENERAL, subres, {});
			device.endSingleTimeCommands(commandBuffer);
			vkDestroyImageView(device.device(), ktxView, nullptr);
			vkDestroyImage(device.device(), ktxVkTex.image, nullptr);
			vkFreeMemory(device.device(), ktxVkTex.deviceMemory, nullptr);
		}

		createImageView();
	}

	ktxTexture2* TerrainTexture::getKTXTexture(float resolutionFactor) {
		ktxTexture2* ktxTex{};
		ktxTextureCreateInfo txCreateInfo{};
		memset(&txCreateInfo, 0, sizeof(ktxTextureCreateInfo));
		txCreateInfo.baseWidth = resolution.x * resolutionFactor;
		txCreateInfo.baseHeight = resolution.y * resolutionFactor;
		txCreateInfo.baseDepth = 1;
		txCreateInfo.generateMipmaps = KTX_FALSE;
		txCreateInfo.vkFormat = gameFormat;
		txCreateInfo.isArray = KTX_FALSE;
		txCreateInfo.numLevels = editorFormat == VK_FORMAT_R32_SFLOAT ? mipLevels : 1;
		txCreateInfo.numLayers = 1;
		txCreateInfo.numFaces = 1;
		txCreateInfo.numDimensions = 2;

		ktxResult cErr = ktxTexture2_Create(&txCreateInfo, KTX_TEXTURE_CREATE_ALLOC_STORAGE, &ktxTex);
		if (cErr != KTX_SUCCESS) {
			SHARD3D_ERROR("Failed to allocate ktx texture!");
			return nullptr;
		}

		VkBuffer copyBuffer;
		VkDeviceMemory copyBufferMemory;

		device.createBuffer(
			sizeBytes,
			VK_BUFFER_USAGE_TRANSFER_DST_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
			copyBuffer,
			copyBufferMemory
		);
		void* pixels{};
		vkMapMemory(device.device(), copyBufferMemory, 0, sizeBytes, 0, &pixels);
		VkCommandBuffer commandBuffer = device.beginSingleTimeCommands();
		for (int i = 0; i < (editorFormat == VK_FORMAT_R32_SFLOAT ? mipLevels : 1); i++) {
			glm::ivec2 res = glm::round(glm::vec2(std::pow(2.0, std::log2(double(txCreateInfo.baseWidth)) - i), std::pow(2.0, std::log2(double(txCreateInfo.baseHeight)) - i)));
			PostProcessingEffect transcodeToGame = PostProcessingEffect(
			device, (glm::vec2)res,
			S3DShader(ShaderType::Fragment,
				editorFormat == VK_FORMAT_R32_SFLOAT ? "resources/shaders/editor/terrain_transcode_height_map_r32f-r16f.frag" :
				"resources/shaders/editor/terrain_transcode_material_map_rgba16u-rgba8u.frag", "editor", {}),
			{ VkDescriptorImageInfo{tileSampler, tileImageView, imageLayout } }, gameFormat
			);

			VkCommandBuffer commandBuffer = device.beginSingleTimeCommands();
			float lod = i;
			transcodeToGame.render(commandBuffer, &lod);
			device.transitionImageLayout(commandBuffer, transcodeToGame.getAttachment()->getImage(), 
				gameFormat, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, 0, 1, 0, 1);
			device.copyImageToBuffer(commandBuffer, transcodeToGame.getAttachment()->getImage(), 
				VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, copyBuffer, res.x, res.y, 1, 0);
			device.endSingleTimeCommands(commandBuffer);
			ktxResult sifmErr = ktxTexture_SetImageFromMemory(ktxTexture(ktxTex), i, 0, 0, reinterpret_cast<ktx_uint8_t*>(pixels), transcodeToGame.getAttachment()->getResourceSize());
			if (sifmErr != KTX_SUCCESS) SHARD3D_ERROR("Failed to modify texture! code: {0}", sifmErr);
		}

		vkUnmapMemory(device.device(), copyBufferMemory);
		vkDestroyBuffer(device.device(), copyBuffer, nullptr);
		vkFreeMemory(device.device(), copyBufferMemory, nullptr);
		return ktxTex;
	}

	void TerrainTexture::recreateSampler() {
		if (tileSampler != VK_NULL_HANDLE) {
			vkDestroySampler(device.device(), tileSampler, nullptr);
		}
		VkSamplerCreateInfo samplerInfo = {};
		samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		samplerInfo.magFilter = VK_FILTER_LINEAR;
		samplerInfo.minFilter = VK_FILTER_LINEAR;
		samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
		samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		samplerInfo.addressModeV = samplerInfo.addressModeU;
		samplerInfo.addressModeW = samplerInfo.addressModeU;
		samplerInfo.borderColor = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK;
		samplerInfo.mipLodBias = 0.0f;
		samplerInfo.anisotropyEnable = VK_FALSE;
		samplerInfo.maxAnisotropy = 1.0f;
		samplerInfo.minLod = 0.0f;
		samplerInfo.maxLod = static_cast<float>(mipLevels - 1);
		VK_ASSERT(vkCreateSampler(device.device(), &samplerInfo, nullptr, &tileSampler), "Failed to create sampler");
	}

	void TerrainTexture::createImageView() {
		VkImageViewCreateInfo imageViewCreateInfo = {};
		imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		imageViewCreateInfo.format = ProjectSystem::isEditorMode() ? editorFormat : gameFormat;
		imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
		imageViewCreateInfo.subresourceRange.levelCount = 1;
		imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
		imageViewCreateInfo.subresourceRange.layerCount = 1;
		imageViewCreateInfo.image = tileImage;

		VK_ASSERT(vkCreateImageView(device.device(), &imageViewCreateInfo, nullptr, &tileImageView), "Failed to create image view!");

		imageViewCreateInfo.subresourceRange.levelCount = 1;

		VK_ASSERT(vkCreateImageView(device.device(), &imageViewCreateInfo, nullptr, &tileImageViewMips), "Failed to create image view!");
	}

	TerrainActor::TerrainActor(S3DDevice& device) : device(device) {
		
	}
	TerrainActor::~TerrainActor() {
		if (terrainCullDescriptorSet) {
			device.staticMaterialPool->freeDescriptors({ terrainCullDescriptorSet });
		}
	}

	void TerrainActor::render(FrameInfo& frameInfo, uint32_t lod) {
		vkCmdDrawIndexedIndirect(frameInfo.commandBuffer, terrainDrawIndirectBuffer->getBuffer(), lod * 20ui64, 1, 0);
	}
	void TerrainActor::loadFromAsset(Actor actor, AssetID asset) {
		simdjson::dom::parser parser;
		simdjson::padded_string json = simdjson::padded_string::load(asset.getAbsolute());
		auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			return;
		}

		AssetID heightMapAsset = std::string(data["assetFiles"]["heightMap"].get_string().value());
		AssetID materialMap0Asset = std::string(data["assetFiles"]["materialMap0"].get_string().value());
		AssetID physicsTileHeightField = std::string(data["assetFiles"]["physicsTileHeightField"].get_string().value());
		AssetID physicsTileMaterialMap = std::string(data["assetFiles"]["physicsTileMaterialMap"].get_string().value());
		AssetID physDataAsset = std::string(data["assetFiles"]["physicsData"].get_string().value());
		AssetID aabbDataAsset = std::string(data["assetFiles"]["aabbData"].get_string().value());

		std::vector<char> aabbDat = IOUtils::readBinary(aabbDataAsset.getAbsolute());
		memcpy(terrainAABBs.data(), aabbDat.data(), aabbDat.size());
		if (physicsTileHeightField && physicsTileMaterialMap && physDataAsset) {
			std::vector<char> physData = IOUtils::readBinary(physDataAsset.getAbsolute());
			std::vector<char> physHeightData = IOUtils::readBinary(physicsTileHeightField.getAbsolute());
			std::vector<char> physMaterialData = IOUtils::readBinary(physicsTileMaterialMap.getAbsolute());
			ZSTD_DCtx* decompressionContext = ZSTD_createDCtx();

			auto& trc = actor.getComponent<Components::TerrainComponent>();
			trc.physData = new TerrainPhysicsData();
			trc.physData->position = {};
			trc.physData->tiles.resize(*reinterpret_cast<int*>(physData.data()) * *reinterpret_cast<int*>(physData.data() + 4));

			for (int i = 0; i < trc.physData->tiles.size(); i++) {
				auto& tile = trc.physData->tiles[i];
				tile.offsetX			= *reinterpret_cast<float*>(physData.data() + 8 + i * 32 + 0);
				tile.offsetY			= *reinterpret_cast<float*>(physData.data() + 8 + i * 32 + 4);
				tile.verticesX			= *reinterpret_cast<int*>(physData.data() + 8 + i * 32 + 8);
				tile.verticesY			= *reinterpret_cast<int*>(physData.data() + 8 + i * 32 + 12);
				tile.widthX				= *reinterpret_cast<float*>(physData.data() + 8 + i * 32 + 16);
				tile.widthY				= *reinterpret_cast<float*>(physData.data() + 8 + i * 32 + 20);
				tile.materialDataResX	= *reinterpret_cast<int*>(physData.data() + 8 + i * 32 + 24);
				tile.materialDataResY	= *reinterpret_cast<int*>(physData.data() + 8 + i * 32 + 28);
				tile.aabb = terrainAABBs[i];
			}
			
			{
				size_t deflatedSizeHeight = ZSTD_getFrameContentSize(physHeightData.data(), physHeightData.size());
				uint8_t* deflatedBytesHeight = new uint8_t[deflatedSizeHeight];
				size_t readSizeHeight = ZSTD_decompressDCtx(decompressionContext, deflatedBytesHeight, deflatedSizeHeight, physHeightData.data(), physHeightData.size());
				size_t deflatedSizeMaterial = ZSTD_getFrameContentSize(physMaterialData.data(), physMaterialData.size());
				uint8_t* deflatedBytesMaterial = new uint8_t[deflatedSizeMaterial];
				size_t readSizeMaterial = ZSTD_decompressDCtx(decompressionContext, deflatedBytesMaterial, deflatedSizeMaterial, physMaterialData.data(), physMaterialData.size());

				for (int i = 0; i < trc.physData->tiles.size(); i++) {
					auto& tile = trc.physData->tiles[i];

					tile.heights.resize(tile.verticesX * tile.verticesY);
					memcpy(tile.heights.data(), reinterpret_cast<float*>(deflatedBytesHeight + tile.heights.size() * i * sizeof(float)), tile.heights.size() * sizeof(float));
					tile.materialData.resize(tile.materialDataResX * tile.materialDataResY);
					memcpy(tile.materialData.data(), reinterpret_cast<uint8_t*>(deflatedBytesMaterial + tile.materialData.size() * i * sizeof(uint8_t)), tile.materialData.size() * sizeof(uint8_t));
				}
				delete[] deflatedBytesHeight;
				delete[] deflatedBytesMaterial;
			}
			ZSTD_freeDCtx(decompressionContext);
		}

		VkFormat editorFormatHeight = enum_VkFormat(std::string(data["config"]["editorFormatHeightMap"].get_string().value()));
		VkFormat editorFormatMaterial = enum_VkFormat(std::string(data["config"]["editorFormatMaterialMap0"].get_string().value()));
		glm::ivec2 editorResHeight = SIMDJSON_READ_IVEC2(data["config"]["editorResolutionHeightMap"]);
		glm::ivec2 editorResMaterial = SIMDJSON_READ_IVEC2(data["config"]["editorResolutionMaterialMap0"]);
		
		heightMap->loadTexture(TextureImporter::loadCompressedCachedTextureKTX(heightMapAsset.getAbsolute().c_str()), editorFormatHeight, editorResHeight);
		materialMap->loadTexture(TextureImporter::loadCompressedCachedTextureKTX(materialMap0Asset.getAbsolute().c_str()), editorFormatMaterial, editorResMaterial);

	}

	void TerrainActor::populateMaterialBuffer(ResourceSystem* resourceSystem, const std::vector<TerrainMaterial*>& terrainMaterials) {
		SHARD3D_ASSERT(terrainMaterials.size() <= 8 && "Max material count is 8!!");
		reinterpret_cast<TerrainMaterialBlendBufferData*>(terrainMaterialBuffer->getMappedMemory())->maxMaterials = terrainMaterials.size();
		for (int i = 0; i < terrainMaterials.size(); i++) {
			TerrainMaterial* material = terrainMaterials[i];
			if (!material) {
				reinterpret_cast<TerrainMaterialBlendBufferData*>(terrainMaterialBuffer->getMappedMemory())->materials[i] = {};
				continue;
			}
			reinterpret_cast<TerrainMaterialBlendBufferData*>(terrainMaterialBuffer->getMappedMemory())->materials[i] = material->getTerrainMaterialBufferData();
		}
		terrainMaterialBuffer->flush();
	}

	void TerrainActor::createBufferData() {
		terrainMaterialBuffer = make_uPtr<S3DBuffer>(
			device,
			sizeof(TerrainMaterialBlendBufferData),
			1,
			VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
		);
		terrainMaterialBuffer->map();

		terrainVisibilityBuffer = make_uPtr<S3DBuffer>(
			device,
			8,
			heightMap->mipLevels * numTilesX * numTilesY,
			VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
			true
		);

	}
	void TerrainActor::updateVisibilityCalcs(VkCommandBuffer commandBuffer, S3DCamera& camera, float quality, float lodBias) {
		terrainUpdatePipeline->bind(commandBuffer);
		vkCmdBindDescriptorSets(
			commandBuffer,
			VK_PIPELINE_BIND_POINT_COMPUTE,
			terrainUpdatePipelineLayout->getPipelineLayout(),
			0,
			1,
			&terrainCullDescriptorSet,
			0,
			nullptr
		);
		TerrainUpdatePushData push;
		push.frustumPlanes = camera.getFrustumPlanes();
		push.cameraPosition = camera.getPosition();
		push.cameraFocalZoom = camera.getProjection()[1][1];
		push.terrainQuality = quality;
		push.lodBias = lodBias;

		terrainUpdatePush.push(commandBuffer, terrainUpdatePipelineLayout->getPipelineLayout(), push);
		vkCmdDispatch(commandBuffer, std::ceil(numTilesX / 8.f), std::ceil(numTilesY / 8.f), 1);

		S3DSynchronization sync;
		sync = S3DSynchronization(SynchronizationType::ComputeToGraphics, VK_PIPELINE_STAGE_DRAW_INDIRECT_BIT);
		sync.addBufferMemoryBarrier(VK_ACCESS_SHADER_WRITE_BIT, VK_ACCESS_INDIRECT_COMMAND_READ_BIT, terrainDrawIndirectBuffer.get());
		sync.syncBarrier(commandBuffer);
		sync = S3DSynchronization(SynchronizationType::ComputeToGraphics, VK_PIPELINE_STAGE_VERTEX_SHADER_BIT);
		sync.addBufferMemoryBarrier(VK_ACCESS_SHADER_WRITE_BIT, VK_ACCESS_MEMORY_READ_BIT, terrainVisibilityBuffer.get());
		sync.syncBarrier(commandBuffer);
	}

	void TerrainActor::createDescriptorData(uPtr<S3DDescriptorSetLayout>& descriptorSetLayout) {
		VkDescriptorBufferInfo visBufferInfo = terrainVisibilityBuffer->descriptorInfo();
		VkDescriptorBufferInfo matBufferInfo = terrainMaterialBuffer->descriptorInfo();
		VkDescriptorImageInfo terrainHeightMapImageInfo{ heightMap->tileSampler, heightMap->tileImageView, heightMap->imageLayout };
		VkDescriptorImageInfo terrainMaterialMapImageInfo{ materialMap->tileSampler, materialMap->tileImageView, heightMap->imageLayout };
		S3DDescriptorWriter(*descriptorSetLayout, *device.staticMaterialPool)
			.writeBuffer(0, &visBufferInfo)
			.writeBuffer(1, &matBufferInfo)
			.writeImage(2, &terrainHeightMapImageInfo)
			.writeImage(3, &terrainMaterialMapImageInfo)
			.build(terrainDescriptor);
	}

	void TerrainActor::createTerrainUpdateObjects() {
		terrainAABBBuffer = make_uPtr<S3DBuffer>(
			device,
			32,
			numTilesX * numTilesY,
			VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
		);
		terrainDrawIndirectBuffer = make_uPtr<S3DBuffer>(
			device,
			sizeof(VkDrawIndexedIndirectCommand),
			heightMap->mipLevels,
			VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
			true
		);
		terrainMeshDataBuffer = make_uPtr<S3DBuffer>(
			device,
			16,
			heightMap->mipLevels,
			VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
		); 
		terrainAABBBuffer->map();
		for (int i = 0; i < terrainAABBs.size(); i++) {
			*reinterpret_cast<glm::vec3*>(reinterpret_cast<char*>(terrainAABBBuffer->getMappedMemory()) + i * 32) = terrainAABBs[i].minXYZ;
			*reinterpret_cast<glm::vec3*>(reinterpret_cast<char*>(terrainAABBBuffer->getMappedMemory()) + i * 32 + 16) = terrainAABBs[i].maxXYZ;
		}
		terrainAABBBuffer->flush();
		//terrainAABBBuffer->unmap();

		terrainCullDescriptorSetLayout = S3DDescriptorSetLayout::Builder(device)
			.addBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, VK_SHADER_STAGE_COMPUTE_BIT)
			.addBinding(1, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, VK_SHADER_STAGE_COMPUTE_BIT)
			.addBinding(2, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, VK_SHADER_STAGE_COMPUTE_BIT)
			.addBinding(3, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_COMPUTE_BIT)
			.build();

		S3DDescriptorWriter(*terrainCullDescriptorSetLayout, *device.staticMaterialPool)
			.writeBuffer(0, &terrainVisibilityBuffer->descriptorInfo())
			.writeBuffer(1, &terrainAABBBuffer->descriptorInfo())
			.writeBuffer(2, &terrainDrawIndirectBuffer->descriptorInfo())
			.writeBuffer(3, &terrainMeshDataBuffer->descriptorInfo())
			.build(terrainCullDescriptorSet);

		terrainUpdatePush = S3DPushConstant(sizeof(TerrainUpdatePushData), VK_SHADER_STAGE_COMPUTE_BIT);

		terrainUpdatePipelineLayout = make_uPtr<S3DPipelineLayout>(device, std::vector<VkPushConstantRange>{terrainUpdatePush.getRange()},
			std::vector<VkDescriptorSetLayout>{terrainCullDescriptorSetLayout->getDescriptorSetLayout()});

		S3DShaderSpecialization specialization;
		specialization.addConstant(0, heightMap->mipLevels);
		specialization.addConstant(1, numTilesX);
		specialization.addConstant(2, numTilesY);
		terrainUpdatePipeline = make_uPtr<S3DComputePipeline>(device, terrainUpdatePipelineLayout->getPipelineLayout(),
			S3DShader(ShaderType::Compute, "resources/shaders/rendering/terrain_update.comp", "rendering", specialization));
	}


	void TerrainActor::setMeshData(ResourceSystem* resourceSystem) {
		terrainMeshDataBuffer->map();
		for (int lod = 0; lod < heightMap->mipLevels; lod++) {
			Mesh3D* plane{};
			switch (divisions) {
			case(TerrainTileDivisions::D16x16):
				plane = resourceSystem->retrieveMesh(AssetID("engine/meshes/terrain/planes_subdivide16_lod0.s3dasset"));
				break;
			case(TerrainTileDivisions::D32x32):
				switch (lod) {
				case(0):
					plane = resourceSystem->retrieveMesh(AssetID("engine/meshes/terrain/planes_subdivide32_lod0.s3dasset"));
					break;
				case(1):
					plane = resourceSystem->retrieveMesh(AssetID("engine/meshes/terrain/planes_subdivide32_lod1.s3dasset"));
					break;
				}
				break;
			case(TerrainTileDivisions::D64x64):
				switch (lod) {
				case(0):
					plane = resourceSystem->retrieveMesh(AssetID("engine/meshes/terrain/planes_subdivide64_lod0.s3dasset"));
					break;
				case(1):
					plane = resourceSystem->retrieveMesh(AssetID("engine/meshes/terrain/planes_subdivide64_lod1.s3dasset"));
					break;
				case(2):
					plane = resourceSystem->retrieveMesh(AssetID("engine/meshes/terrain/planes_subdivide64_lod2.s3dasset"));
					break;
				}
				break;
			case(TerrainTileDivisions::D128x128):
				switch (lod) {
				case(0):
					plane = resourceSystem->retrieveMesh(AssetID("engine/meshes/terrain/planes_subdivide128_lod0.s3dasset"));
					break;
				case(1):
					plane = resourceSystem->retrieveMesh(AssetID("engine/meshes/terrain/planes_subdivide128_lod1.s3dasset"));
					break;
				case(2):
					plane = resourceSystem->retrieveMesh(AssetID("engine/meshes/terrain/planes_subdivide128_lod2.s3dasset"));
					break;
				case(3):
					plane = resourceSystem->retrieveMesh(AssetID("engine/meshes/terrain/planes_subdivide128_lod3.s3dasset"));
					break;
				}
				break;
			case(TerrainTileDivisions::D256x256):
				switch (lod) {
				case(0):
					plane = resourceSystem->retrieveMesh(AssetID("engine/meshes/terrain/planes_subdivide256_lod0.s3dasset"));
					break;
				case(1):
					plane = resourceSystem->retrieveMesh(AssetID("engine/meshes/terrain/planes_subdivide256_lod1.s3dasset"));
					break;
				case(2):
					plane = resourceSystem->retrieveMesh(AssetID("engine/meshes/terrain/planes_subdivide256_lod2.s3dasset"));
					break;
				case(3):
					plane = resourceSystem->retrieveMesh(AssetID("engine/meshes/terrain/planes_subdivide256_lod3.s3dasset"));
					break;
				case(4):
					plane = resourceSystem->retrieveMesh(AssetID("engine/meshes/terrain/planes_subdivide256_lod4.s3dasset"));
					break;
				}
				break;
			}
			SHARD3D_ASSERT(plane);

			struct TerrainMeshData {
				uint32_t indexCount;
				uint32_t firstIndex;
				int vertexOffset;
				// 4 bytes of padding
			};
			auto offsets = resourceSystem->getVertexAllocator()->getOffsetData(plane->meshID);
			static_cast<TerrainMeshData&>(reinterpret_cast<AlignedType<TerrainMeshData, 16>*>(terrainMeshDataBuffer->getMappedMemory())[lod]).firstIndex = offsets.indexOffset;
			static_cast<TerrainMeshData&>(reinterpret_cast<AlignedType<TerrainMeshData, 16>*>(terrainMeshDataBuffer->getMappedMemory())[lod]).indexCount = offsets.indexCount;
			static_cast<TerrainMeshData&>(reinterpret_cast<AlignedType<TerrainMeshData, 16>*>(terrainMeshDataBuffer->getMappedMemory())[lod]).vertexOffset = offsets.vertexOffset;
		}
		terrainMeshDataBuffer->flush();
		terrainMeshDataBuffer->unmap();
	}


	TerrainList::TerrainList(S3DDevice& device, uPtr<S3DDescriptorSetLayout>& terrainDescriptorSetLayout, VkDescriptorSetLayout globalSetLayout)
		: device(device), terrainDescriptorSetLayout(terrainDescriptorSetLayout), globalSetLayout(globalSetLayout) {

	}
	void TerrainList::update(sPtr<Level>& level, ResourceSystem* resourceSystem) {
		bool terrainExists = false;
		for (auto& actor_ : level->registry.view<Components::TerrainComponent>()) {
			actor = { actor_ , level.get() };
			auto& trc = actor.getComponent<Components::TerrainComponent>();
			terrainExists = true;
			if (trc.loadAsset) {
				vkDeviceWaitIdle(device.device()); // wait for device to finish using terrain resources
				terrain = make_sPtr<TerrainActor>(device);
				terrain->numTilesX = trc.tiles.x;
				terrain->numTilesY = trc.tiles.y;
				terrain->tileExtent = trc.tileExtent;

				terrain->heightMap = make_uPtr<TerrainTexture>(device);
				terrain->materialMap = make_uPtr<TerrainTexture>(device);
				terrain->terrainAABBs.resize(trc.tiles.x * trc.tiles.y);
				terrain->divisions = (TerrainTileDivisions)trc.tileSubdivisions;
				terrain->loadFromAsset(actor, trc.terrainAsset);
				terrain->createBufferData();
				terrain->createTerrainUpdateObjects();
				terrain->setMeshData(resourceSystem);

				terrain->createDescriptorData(terrainDescriptorSetLayout);
				std::vector<TerrainMaterial*> materials;
				for (int i = 0; i < trc.materials.size(); i++) {
					bool invalidMaterial = false;
					if (trc.materials[i]) {
						resourceSystem->loadMaterialRecursive(trc.materials[i], false);
						materials.push_back(dynamic_cast<TerrainMaterial*>(resourceSystem->retrieveMaterial(trc.materials[i])));
					}
				}
				terrain->populateMaterialBuffer(resourceSystem, materials);

				trc.loadAsset = false;
				break;
			}
		}
		if (!terrainExists) {
			vkDeviceWaitIdle(device.device()); // wait for device to finish using terrain resources
			terrain = {};
			actor = {};
		}
	}

}
