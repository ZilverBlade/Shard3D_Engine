#include "../handlers/resource_system.h"
#include "../handlers/render_list.h"
#include "light_propagation_volume_system.h"
#include "shadow_mapping_system.h"

namespace Shard3D::Systems {
	struct LPVInjectionPush {
		alignas(16)glm::vec3 lpvExtent;
		alignas(16)glm::vec3 lpvCenter;
		int res;
	};
	struct LPVPropagationPush {
		int pingPongIndex;
	};
	struct LPVBlendPush {
		glm::ivec4 blendOffset;
		int cascade;
		int finalPingPongOutput;
		float temporalBlend;
	};

	struct VirtualPointLight {
		glm::vec4 position;
		glm::vec4 color;
	};

	struct VirtualPointLightUBO {
		VirtualPointLight virtualPointLight[2048];
	};

	const VkFormat VoxelFormat = VK_FORMAT_R16G16B16A16_SFLOAT;

	LPVTexture3D::LPVTexture3D(S3DDevice& device, glm::ivec3 resolution, VkImageLayout imageLayout, VkFormat format, VkFilter filter, bool rendertarget) : device(device), voxelImageLayout(imageLayout) {
		VkImageCreateInfo imageCreateInfo{};
		imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageCreateInfo.imageType = VK_IMAGE_TYPE_3D;
		imageCreateInfo.format = format;
		imageCreateInfo.extent.width = resolution.x;
		imageCreateInfo.extent.height = resolution.y;
		imageCreateInfo.extent.depth = resolution.z;
		imageCreateInfo.mipLevels = 1;
		imageCreateInfo.arrayLayers = 1;
		imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
		imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
		
		imageCreateInfo.usage = VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;

		imageCreateInfo.flags = VK_IMAGE_CREATE_2D_ARRAY_COMPATIBLE_BIT;
		VkMemoryAllocateInfo memAlloc = {};
		memAlloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		VkMemoryRequirements memReqs;

		VK_ASSERT(vkCreateImage(device.device(), &imageCreateInfo, nullptr, &voxelDataImage), "Failed to create image!");
		vkGetImageMemoryRequirements(device.device(), voxelDataImage, &memReqs);
		memAlloc.memoryTypeIndex = device.findMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		memAlloc.allocationSize = memReqs.size;

		if (vkAllocateMemory(device.device(), &memAlloc, nullptr, &voxelDataImageMemory) != VK_SUCCESS) {
			SHARD3D_ERROR("Ran out of memory!");
			return;
		}

		VK_ASSERT(vkBindImageMemory(device.device(), voxelDataImage, voxelDataImageMemory, 0), "Failed to bind memory!");

		VkImageViewCreateInfo imageViewCreateInfo = {};
		imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_3D;
		imageViewCreateInfo.format = format;
		imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
		imageViewCreateInfo.subresourceRange.levelCount = 1;
		imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
		imageViewCreateInfo.subresourceRange.layerCount = 1;
		imageViewCreateInfo.image = voxelDataImage;

		VK_ASSERT(vkCreateImageView(device.device(), &imageViewCreateInfo, nullptr, &voxelDataImageView), "Failed to create image view!");

		if (filter != VK_FILTER_MAX_ENUM) {
			VkSamplerCreateInfo samplerInfo = {};
			samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
			samplerInfo.magFilter = filter;
			samplerInfo.minFilter = filter;
			samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
			samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
			samplerInfo.addressModeV = samplerInfo.addressModeU;
			samplerInfo.addressModeW = samplerInfo.addressModeU;
			samplerInfo.borderColor = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK;
			samplerInfo.mipLodBias = 0.0f;
			samplerInfo.maxAnisotropy = 1.0f;
			samplerInfo.minLod = 0.0f;
			samplerInfo.maxLod = 0.0f;

			VK_ASSERT(vkCreateSampler(device.device(), &samplerInfo, nullptr, &voxelDataSampler), "Failed to create sampler");
		}
	}

	LPVTexture3D::~LPVTexture3D() {
		vkDestroyImage(device.device(), voxelDataImage, nullptr);
		vkFreeMemory(device.device(), voxelDataImageMemory, nullptr);
		vkDestroyImageView(device.device(), voxelDataImageView, nullptr);
		vkDestroySampler(device.device(), voxelDataSampler, nullptr);
	}

	FastComputeShader::FastComputeShader(S3DDevice& device, glm::vec2 extent, S3DShader fragmentShader, uint32_t pushSize, std::vector<VkDescriptorSetLayout> descriptorSetLayouts) {
		pushC = S3DPushConstant(pushSize, VK_SHADER_STAGE_FRAGMENT_BIT);
		pipelineLayout = make_uPtr<S3DPipelineLayout>(device, pushSize == 0 ? std::vector<VkPushConstantRange>{} : std::vector<VkPushConstantRange>{pushC.getRange()}, descriptorSetLayouts);
		renderPass = new S3DRenderPass(device, {});
		framebuffer = new S3DFramebuffer(device, renderPass, true, {extent, 1});
		S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo(0);
		pipelineConfig.setCullMode(VK_CULL_MODE_BACK_BIT);
		pipelineConfig.disableDepthTest();
		pipelineConfig.renderPass = renderPass->getRenderPass();
		pipelineConfig.pipelineLayout = pipelineLayout->getPipelineLayout();

		pipeline = make_uPtr<S3DGraphicsPipeline>(
			device, std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/fullscreen_quad.vert", "misc" },
				fragmentShader
			}, pipelineConfig
		);

	}

	FastComputeShader::~FastComputeShader() {
		delete renderPass;
		delete framebuffer;
	}

	void FastComputeShader::push(VkCommandBuffer commandBuffer, const VoidRef<128>& data) {
		pushC.push(commandBuffer, pipelineLayout->getPipelineLayout(), data);
	}

	void FastComputeShader::dispatch(VkCommandBuffer commandBuffer) {
		renderPass->beginRenderPass(commandBuffer, framebuffer);
		pipeline->bind(commandBuffer);
		vkCmdDraw(commandBuffer, 6, 1, 0, 0);
		renderPass->endRenderPass(commandBuffer);
	}

	LightPropagationVolumeSystem::LightPropagationVolumeSystem(S3DDevice& device, ResourceSystem* resourceSystem, ShadowMappingSystem* shadowMappingSystem) 
		: engineDevice(device), resourceSystem(resourceSystem), shadowMappingSystem(shadowMappingSystem)
	{
		rsmInputDescriptorSetLayout = S3DDescriptorSetLayout::Builder(device)
			.addBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT)
			.addBinding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_VERTEX_BIT)
			.addBinding(2, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_VERTEX_BIT)
			.addBinding(3, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_VERTEX_BIT)
			.build();
		voxelDescriptorSetLayout = S3DDescriptorSetLayout::Builder(device)
			.addBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_FRAGMENT_BIT)
			.addBinding(1, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_FRAGMENT_BIT)
			.addBinding(2, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_FRAGMENT_BIT)
			.addBinding(3, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_FRAGMENT_BIT)
			.addBinding(4, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_FRAGMENT_BIT)
			.addBinding(5, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_FRAGMENT_BIT)
			.addBinding(6, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_FRAGMENT_BIT, 9)
			.build();
		shSumDescriptorSetLayout = S3DDescriptorSetLayout::Builder(device)
			.addBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_FRAGMENT_BIT)
			.build();

		vplDescriptorSetLayout = S3DDescriptorSetLayout::Builder(device)
			.addBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT)
			.build();


		voxelResolution = { 32, 32, 32 };
		createVoxels(voxelResolution);
		createDescriptors();
		createPasses();
	}

	LightPropagationVolumeSystem::~LightPropagationVolumeSystem() {
		destroyVoxels();
		for (auto& [actor, rsmdata] : reflectiveShadowMaps) {
			engineDevice.staticMaterialPool->freeDescriptors(rsmdata->descriptorSet);
			delete rsmdata;
		}
		engineDevice.staticMaterialPool->freeDescriptors({ voxelDescriptorSet ,shSumDescriptorSet });
		delete clearPass;
		delete propagationPass;
		delete averagingPass;
		delete blendPass;

		delete injectionRenderPass;
		delete injectionFramebuffer;
		delete injectionRenderTarget_SH_Red;
		delete injectionRenderTarget_SH_Green;
		delete injectionRenderTarget_SH_Blue;
		delete injectionRenderTarget_SH_Sum;
		delete injectionRSMPipelineLayout;
		delete injectionVPLPipelineLayout;
		delete injectionRSMPipeline;
		delete injectionVPLPipeline;
	}



	void LightPropagationVolumeSystem::destroyVoxels() {
		for (int i = 0; i < 3; i++) {
			resourceSystem->getDescriptor()->freeIndex(BINDLESS_SAMPLER_BINDING, lpvSH_Red[i]->optionalBindless);
			resourceSystem->getDescriptor()->freeIndex(BINDLESS_SAMPLER_BINDING, lpvSH_Green[i]->optionalBindless);
			resourceSystem->getDescriptor()->freeIndex(BINDLESS_SAMPLER_BINDING, lpvSH_Blue[i]->optionalBindless);
			delete lpvSH_Red[i];
			delete lpvSH_Green[i];
			delete lpvSH_Blue[i];
		}
		delete lpv_shSum;
		delete lpv_InoutInjProp_Red0;
		delete lpv_InoutInjProp_Red1;
		delete lpv_InoutInjProp_Green0;
		delete lpv_InoutInjProp_Green1;
		delete lpv_InoutInjProp_Blue0;
		delete lpv_InoutInjProp_Blue1;
	}

	void LightPropagationVolumeSystem::createVoxels(glm::ivec3 resolution) { 
		VkCommandBuffer commandBuffer = engineDevice.beginSingleTimeCommands();
		for (int i = 0; i < 3; i++) {
			lpvSH_Red[i] = new LPVTexture3D(engineDevice, resolution, VK_IMAGE_LAYOUT_GENERAL, VoxelFormat, VK_FILTER_LINEAR);
			lpvSH_Green[i] = new LPVTexture3D(engineDevice, resolution, VK_IMAGE_LAYOUT_GENERAL, VoxelFormat, VK_FILTER_LINEAR);
			lpvSH_Blue[i] = new LPVTexture3D(engineDevice, resolution, VK_IMAGE_LAYOUT_GENERAL, VoxelFormat, VK_FILTER_LINEAR);
			engineDevice.transitionImageLayout(commandBuffer, lpvSH_Red[i]->voxelDataImage, VoxelFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, 0, 1, 0, 1);
			engineDevice.transitionImageLayout(commandBuffer, lpvSH_Green[i]->voxelDataImage, VoxelFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, 0, 1, 0, 1);
			engineDevice.transitionImageLayout(commandBuffer, lpvSH_Blue[i]->voxelDataImage, VoxelFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, 0, 1, 0, 1);
		}
		lpv_shSum = new LPVTexture3D(engineDevice, resolution, VK_IMAGE_LAYOUT_GENERAL, VK_FORMAT_R16_SFLOAT, VK_FILTER_MAX_ENUM, true);
		lpv_InoutInjProp_Red0 = new LPVTexture3D(engineDevice, resolution, VK_IMAGE_LAYOUT_GENERAL, VoxelFormat, VK_FILTER_MAX_ENUM, true);
		lpv_InoutInjProp_Red1 = new LPVTexture3D(engineDevice, resolution, VK_IMAGE_LAYOUT_GENERAL, VoxelFormat, VK_FILTER_MAX_ENUM);
		lpv_InoutInjProp_Green0 = new LPVTexture3D(engineDevice, resolution, VK_IMAGE_LAYOUT_GENERAL, VoxelFormat, VK_FILTER_MAX_ENUM, true);
		lpv_InoutInjProp_Green1 = new LPVTexture3D(engineDevice, resolution, VK_IMAGE_LAYOUT_GENERAL, VoxelFormat, VK_FILTER_MAX_ENUM);
		lpv_InoutInjProp_Blue0 = new LPVTexture3D(engineDevice, resolution, VK_IMAGE_LAYOUT_GENERAL, VoxelFormat, VK_FILTER_MAX_ENUM, true);
		lpv_InoutInjProp_Blue1 = new LPVTexture3D(engineDevice, resolution, VK_IMAGE_LAYOUT_GENERAL, VoxelFormat, VK_FILTER_MAX_ENUM);
		engineDevice.transitionImageLayout(commandBuffer, lpv_shSum->voxelDataImage, VK_FORMAT_R16_SFLOAT, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, 0, 1, 0, 1);
		engineDevice.transitionImageLayout(commandBuffer, lpv_InoutInjProp_Red0->voxelDataImage, VoxelFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, 0, 1, 0, 1);
		engineDevice.transitionImageLayout(commandBuffer, lpv_InoutInjProp_Red1->voxelDataImage, VoxelFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, 0, 1, 0, 1);
		engineDevice.transitionImageLayout(commandBuffer, lpv_InoutInjProp_Green0->voxelDataImage, VoxelFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, 0, 1, 0, 1);
		engineDevice.transitionImageLayout(commandBuffer, lpv_InoutInjProp_Green1->voxelDataImage, VoxelFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, 0, 1, 0, 1);
		engineDevice.transitionImageLayout(commandBuffer, lpv_InoutInjProp_Blue0->voxelDataImage, VoxelFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, 0, 1, 0, 1);
		engineDevice.transitionImageLayout(commandBuffer, lpv_InoutInjProp_Blue1->voxelDataImage, VoxelFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, 0, 1, 0, 1);
		engineDevice.endSingleTimeCommands(commandBuffer);
	}

	void LightPropagationVolumeSystem::createDescriptors() {
		VkDescriptorImageInfo lpv_InoutInjProp_R0ImageInfo = lpv_InoutInjProp_Red0->getDescriptor();
		VkDescriptorImageInfo lpv_InoutInjProp_R1ImageInfo = lpv_InoutInjProp_Red1->getDescriptor();
		VkDescriptorImageInfo lpv_InoutInjProp_G0ImageInfo = lpv_InoutInjProp_Green0->getDescriptor();
		VkDescriptorImageInfo lpv_InoutInjProp_G1ImageInfo = lpv_InoutInjProp_Green1->getDescriptor();
		VkDescriptorImageInfo lpv_InoutInjProp_B0ImageInfo = lpv_InoutInjProp_Blue0->getDescriptor();
		VkDescriptorImageInfo lpv_InoutInjProp_B1ImageInfo = lpv_InoutInjProp_Blue1->getDescriptor();

		std::array<VkDescriptorImageInfo, 9> lpvSH_RGBCascades{
			lpvSH_Red[0]->getDescriptor(),
			lpvSH_Green[0]->getDescriptor(),
			lpvSH_Blue[0]->getDescriptor(),
			lpvSH_Red[1]->getDescriptor(),
			lpvSH_Green[1]->getDescriptor(),
			lpvSH_Blue[1]->getDescriptor(),
			lpvSH_Red[2]->getDescriptor(),
			lpvSH_Green[2]->getDescriptor(),
			lpvSH_Blue[2]->getDescriptor()
		};
		int ci = 0;
		int rgbi = 0;
		for (auto& imageInfo : lpvSH_RGBCascades) {
			LPVTexture3D* lpv = rgbi == 0 ? lpvSH_Red[ci] : rgbi == 1 ? lpvSH_Green[ci] : lpvSH_Blue[ci];
			lpv->optionalBindless = resourceSystem->getDescriptor()->allocateIndex(BINDLESS_SAMPLER_BINDING);
			BindlessDescriptorWriter(*resourceSystem->getDescriptor())
				.writeImage(BINDLESS_SAMPLER_BINDING, lpv->optionalBindless, &imageInfo)
				.build();

			imageInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
			imageInfo.sampler = VK_NULL_HANDLE;

			if (rgbi == 2) {
				rgbi = 0;
				ci++;
			} else {
				rgbi++;
			}
		}

		S3DDescriptorWriter(*voxelDescriptorSetLayout, *engineDevice.staticMaterialPool)
			.writeImage(0, &lpv_InoutInjProp_R0ImageInfo)
			.writeImage(1, &lpv_InoutInjProp_G0ImageInfo)
			.writeImage(2, &lpv_InoutInjProp_B0ImageInfo)
			.writeImage(3, &lpv_InoutInjProp_R1ImageInfo)
			.writeImage(4, &lpv_InoutInjProp_G1ImageInfo)
			.writeImage(5, &lpv_InoutInjProp_B1ImageInfo)
			.writeImage(6, lpvSH_RGBCascades.data())
			.build(voxelDescriptorSet);

		VkDescriptorImageInfo lpv_ShSumImageInfo = lpv_shSum->getDescriptor();
		S3DDescriptorWriter(*shSumDescriptorSetLayout, *engineDevice.staticMaterialPool)
			.writeImage(0, &lpv_ShSumImageInfo)
			.build(shSumDescriptorSet);

		vplBuffers.resize(ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		vplDescriptors.resize(ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		for (int i = 0; i < vplDescriptors.size(); i++) {
			vplBuffers[i] = make_uPtr<S3DBuffer>(
				engineDevice,
				sizeof(VirtualPointLightUBO),
				1,
				VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
			);
			vplBuffers[i]->map();
			VkDescriptorBufferInfo vplBufferInfo = vplBuffers[i]->descriptorInfo();
			S3DDescriptorWriter(*vplDescriptorSetLayout, *engineDevice.staticMaterialPool)
				.writeBuffer(0, &vplBufferInfo)
				.build(vplDescriptors[i]);
		}
	}

	void LightPropagationVolumeSystem::createPasses() {
		clearPass = new FastComputeShader(engineDevice, voxelResolution, S3DShader(ShaderType::Fragment, "resources/shaders/worldfx/lpv_clear.frag", "worldfx"), 0, {voxelDescriptorSetLayout->getDescriptorSetLayout(), shSumDescriptorSetLayout->getDescriptorSetLayout() });
		averagingPass = new FastComputeShader(engineDevice, voxelResolution, S3DShader(ShaderType::Fragment, "resources/shaders/worldfx/lpv_average.frag", "worldfx"), 0, { voxelDescriptorSetLayout->getDescriptorSetLayout(), shSumDescriptorSetLayout->getDescriptorSetLayout()});
		propagationPass = new FastComputeShader(engineDevice, voxelResolution, S3DShader(ShaderType::Fragment, "resources/shaders/worldfx/lpv_propagation.frag", "worldfx"), sizeof(LPVPropagationPush), { voxelDescriptorSetLayout->getDescriptorSetLayout() });
		blendPass = new FastComputeShader(engineDevice, voxelResolution, S3DShader(ShaderType::Fragment, "resources/shaders/worldfx/lpv_temporal_blend.frag", "worldfx"), sizeof(LPVBlendPush), { voxelDescriptorSetLayout->getDescriptorSetLayout() });
	

		S3DRenderTargetCreateInfo createInfo{};
		createInfo.format = VK_FORMAT_R16G16B16A16_SFLOAT;
		createInfo.imageAspect = VK_IMAGE_ASPECT_COLOR_BIT;
		createInfo.viewType = VK_IMAGE_VIEW_TYPE_3D;
		createInfo.dimensions = { 32,32,32 };
		createInfo.layout = VK_IMAGE_LAYOUT_GENERAL;
		createInfo.renderTargetType = S3DRenderTargetType::Color;
		createInfo.linearFiltering = true;
		createInfo.sampleCount = VK_SAMPLE_COUNT_1_BIT;
		createInfo.enableSamplerPCF = false;
		createInfo.layerCount = 32;
		createInfo.mipLevels = 1;
		createInfo.overrideDirectLayerWrite = true;

		createInfo.inheritVkImage = lpv_InoutInjProp_Red0->voxelDataImage;
		injectionRenderTarget_SH_Red = new S3DRenderTarget(engineDevice, createInfo);
		createInfo.inheritVkImage = lpv_InoutInjProp_Green0->voxelDataImage;
		injectionRenderTarget_SH_Green = new S3DRenderTarget(engineDevice, createInfo);
		createInfo.inheritVkImage = lpv_InoutInjProp_Blue0->voxelDataImage;
		injectionRenderTarget_SH_Blue = new S3DRenderTarget(engineDevice, createInfo);
		createInfo.inheritVkImage = lpv_shSum->voxelDataImage;
		createInfo.format = VK_FORMAT_R16_SFLOAT;
		injectionRenderTarget_SH_Sum = new S3DRenderTarget(engineDevice, createInfo);

		AttachmentInfo shRedInfo;
		shRedInfo.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
		shRedInfo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		shRedInfo.framebufferAttachment = injectionRenderTarget_SH_Red;
		AttachmentInfo shGreenInfo = shRedInfo;
		shGreenInfo.framebufferAttachment = injectionRenderTarget_SH_Green;
		AttachmentInfo shBlueInfo = shRedInfo;
		shBlueInfo.framebufferAttachment = injectionRenderTarget_SH_Blue;
		AttachmentInfo shSumInfo = shRedInfo;
		shSumInfo.framebufferAttachment = injectionRenderTarget_SH_Sum;
		injectionRenderPass = new S3DRenderPass(engineDevice,
			{ shRedInfo , shGreenInfo , shBlueInfo , shSumInfo });

		injectionFramebuffer = new S3DFramebuffer(engineDevice, injectionRenderPass,
			{ injectionRenderTarget_SH_Red , injectionRenderTarget_SH_Green , injectionRenderTarget_SH_Blue, injectionRenderTarget_SH_Sum });

		VkPushConstantRange injectionPushRange;
		injectionPushRange.offset = 0;
		injectionPushRange.size = sizeof(LPVInjectionPush);
		injectionPushRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		injectionRSMPipelineLayout = new S3DPipelineLayout(engineDevice, { injectionPushRange },
			{ voxelDescriptorSetLayout->getDescriptorSetLayout(), rsmInputDescriptorSetLayout->getDescriptorSetLayout() });

		injectionVPLPipelineLayout = new S3DPipelineLayout(engineDevice, { injectionPushRange },
			{ vplDescriptorSetLayout->getDescriptorSetLayout() });

		S3DGraphicsPipelineConfigInfo injectionConfigInfo(4);
		injectionConfigInfo.disableDepthTest();
		injectionConfigInfo.setCullMode(VK_CULL_MODE_BACK_BIT);
		injectionConfigInfo.pointRasterizer();
		injectionConfigInfo.inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
		injectionConfigInfo.enableAdditiveAlphaBlending(0);
		injectionConfigInfo.enableAdditiveAlphaBlending(1);
		injectionConfigInfo.enableAdditiveAlphaBlending(2);
		injectionConfigInfo.enableAdditiveAlphaBlending(3);
		injectionConfigInfo.renderPass = injectionRenderPass->getRenderPass();
		injectionConfigInfo.pipelineLayout = injectionRSMPipelineLayout->getPipelineLayout();

		injectionRSMPipeline = new S3DGraphicsPipeline(engineDevice,
			{ S3DShader(ShaderType::Vertex, "resources/shaders/worldfx/lpv_injection_rsm.vert", "worldfx"),
			S3DShader(ShaderType::Geometry, "resources/shaders/worldfx/lpv_injection.geom", "worldfx"),
			S3DShader(ShaderType::Fragment, "resources/shaders/worldfx/lpv_injection.frag", "worldfx")
			}, injectionConfigInfo);

		injectionConfigInfo.pipelineLayout = injectionVPLPipelineLayout->getPipelineLayout();
		injectionVPLPipeline = new S3DGraphicsPipeline(engineDevice,
			{ S3DShader(ShaderType::Vertex, "resources/shaders/worldfx/lpv_injection_vpl.vert", "worldfx"),
			S3DShader(ShaderType::Geometry, "resources/shaders/worldfx/lpv_injection.geom", "worldfx"),
			S3DShader(ShaderType::Fragment, "resources/shaders/worldfx/lpv_injection.frag", "worldfx")
			}, injectionConfigInfo);
	}

	void LightPropagationVolumeSystem::render(FrameInfo& frameInfo) {
		//
		//for (int i = 0; i < frameInfo.camera.postfx.lightPropagationVolume.numCascades; i++) {
		//	engineDevice.transitionImageLayout(frameInfo.commandBuffer, lpvSH_Red[i]->voxelDataImage, VoxelFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, 0, 1, 0, 1);
		//	engineDevice.transitionImageLayout(frameInfo.commandBuffer, lpvSH_Green[i]->voxelDataImage, VoxelFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, 0, 1, 0, 1);
		//	engineDevice.transitionImageLayout(frameInfo.commandBuffer, lpvSH_Blue[i]->voxelDataImage, VoxelFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, 0, 1, 0, 1);
		//}
		for (int i = 0; i < frameInfo.camera.postfx.lightPropagationVolume.numCascades; i++) {
			clearVolumes(frameInfo);
			injectionRenderPass->beginRenderPass(frameInfo.commandBuffer, injectionFramebuffer, 1);
			injectRSMData(frameInfo, i);
			injectVPLData(frameInfo, i);
			injectionRenderPass->endRenderPass(frameInfo.commandBuffer);
			averageVolume(frameInfo, i);
			propagateVolume(frameInfo, i);
			blendVolumes(frameInfo, i);
		}
		lpvCenterVxpf = lpvCenterVx;
	}

	void LightPropagationVolumeSystem::update(FrameInfo& frameInfo, SceneBuffers& scene) {
		vplCount = 0;
		frameInfo.level->registry.view<Components::TransformComponent, Components::VirtualPointLightComponent, Components::IsVisibileComponent>()
			.each([&](Components::TransformComponent transform, Components::VirtualPointLightComponent vpl, Components::IsVisibileComponent) {
			reinterpret_cast<VirtualPointLightUBO*>(vplBuffers[frameInfo.frameIndex]->getMappedMemory())->virtualPointLight[vplCount].color = glm::vec4(vpl.color, vpl.lightIntensity);
			reinterpret_cast<VirtualPointLightUBO*>(vplBuffers[frameInfo.frameIndex]->getMappedMemory())->virtualPointLight[vplCount].position = transform.transformMatrix[3];
			vplCount++;
		});
		vplBuffers[frameInfo.frameIndex]->flush();

		scene.sceneSotAFX.numLightPropagationVolumeCascades = frameInfo.camera.postfx.lightPropagationVolume.numCascades;
		for (int i = 0; i < frameInfo.camera.postfx.lightPropagationVolume.numCascades; i++) {
			// avoid jaggedness on static objects when moving the camera by snapping the position to the nearest original pixel
			glm::vec3 halfRes = glm::vec3(voxelResolution) / 2.f;

			glm::vec3 fixedPos = frameInfo.camera.postfx.lightPropagationVolume.fixedPosition;
			glm::vec3 position = frameInfo.camera.postfx.lightPropagationVolume.fixed ? fixedPos : frameInfo.camera.getPosition();
			glm::vec3 extent = frameInfo.camera.postfx.lightPropagationVolume.extent / pow(frameInfo.camera.postfx.lightPropagationVolume.cascadeLogBase, static_cast<float>(i));
			glm::vec3 cc = position / extent;
			glm::ivec3 full = round(cc * halfRes);
			lpvCenterVx[i] = full;
			lpvCenter[i] = ((glm::vec3)full / halfRes) * extent;

			//SHARD3D_LOG("{0},{1},{2}", full.x, full.y, full.z);
			//
			//SHARD3D_INFO("{0},{1},{2}", lpvCenter[i].x, lpvCenter[i].y, lpvCenter[i].z);
			scene.sceneSotAFX.lightPropagationVolumeCascades[i].center = lpvCenter[i];
			scene.sceneSotAFX.lightPropagationVolumeCascades[i].fadeRatio = frameInfo.camera.postfx.lightPropagationVolume.fadeRatio;
			
			scene.sceneSotAFX.lightPropagationVolumeCascades[i].boundsScale = frameInfo.camera.postfx.lightPropagationVolume.extent / pow(frameInfo.camera.postfx.lightPropagationVolume.cascadeLogBase, static_cast<float>(i));
			scene.sceneSotAFX.lightPropagationVolumeCascades[i].color = frameInfo.camera.postfx.lightPropagationVolume.boost;
			scene.sceneSotAFX.lightPropagationVolumeCascades[i].redvoxel_id = lpvSH_Red[i]->optionalBindless;
			scene.sceneSotAFX.lightPropagationVolumeCascades[i].greenvoxel_id = lpvSH_Green[i]->optionalBindless;
			scene.sceneSotAFX.lightPropagationVolumeCascades[i].bluevoxel_id = lpvSH_Blue[i]->optionalBindless;
		}
	}

	void LightPropagationVolumeSystem::runGarbageCollector(uint32_t frameIndex, VkFence fence) {
		currentFence = fence;
		for (int i = 0; i < rsmDestructionQueue.size(); i++) {
			RSMInjectionData* rsmData = rsmDestructionQueue[i].first;
			S3DDestructionObject destruct = rsmDestructionQueue[i].second;
			if (destruct.isFree() && destruct.getFence() != fence) {
				engineDevice.staticMaterialPool->freeDescriptors(rsmData->descriptorSet);
				delete rsmData;
				rsmDestructionQueue.erase(rsmDestructionQueue.begin() + i);
				i--;
			}
		}
	}

	std::array<ShaderResourceIndex, 3> LightPropagationVolumeSystem::getLPVDescriptorIndices(uint32_t cascade) {
		return {
			lpvSH_Red[cascade]->optionalBindless,
			lpvSH_Green[cascade]->optionalBindless,
			lpvSH_Blue[cascade]->optionalBindless
		};
	}

	void LightPropagationVolumeSystem::clearVolumes(FrameInfo& frameInfo) {
		VkDescriptorSet sets[2]{
			voxelDescriptorSet,
			shSumDescriptorSet
		};
		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			averagingPass->getPipelineLayout(),
			0,
			2,
			sets,
			0,
			nullptr
		);
		clearPass->dispatch(frameInfo.commandBuffer);
	}

	void LightPropagationVolumeSystem::injectRSMData(FrameInfo& frameInfo, uint32_t cascade) {
		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			injectionRSMPipelineLayout->getPipelineLayout(),
			0,
			1,
			&voxelDescriptorSet,
			0,
			nullptr
		);

		std::unordered_set<entt::entity> removedRSMs;
		for (auto& [actor, rsm] : reflectiveShadowMaps) {
			removedRSMs.insert(actor);
		}
		for (auto& [actor, shadowMap] : shadowMappingSystem->shadowMaps) {
			if (!shadowMap->reflectiveShadowMap) continue;

			if (reflectiveShadowMaps.find(actor) == reflectiveShadowMaps.end()) {
				auto depthMapImageInfo = shadowMap->getDepthAttachment()->getDescriptor();
				auto fluxMapImageInfo = shadowMap->reflectiveShadowMap->fluxMap->getDescriptor();
				auto normalMapImageInfo = shadowMap->reflectiveShadowMap->normalMap->getDescriptor();
				depthMapImageInfo.sampler = fluxMapImageInfo.sampler; // for some retarded reason, the shadow sampler compare op is used WHEN THE SAMPLER IS NOT EVEN A SAMPER2DSHADOW IN SHADER
				reflectiveShadowMaps[actor] = new RSMInjectionData();
				reflectiveShadowMaps[actor]->descriptorSet.resize(ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
				for (int i = 0; i < ProjectSystem::getGraphicsSettings().renderer.framesInFlight; i++){
					auto bufferInfo = shadowMap->shadowMapBufferInfo[i]->descriptorInfo();
					S3DDescriptorWriter(*rsmInputDescriptorSetLayout, *engineDevice.staticMaterialPool)
						.writeBuffer(0, &bufferInfo)
						.writeImage(1, &depthMapImageInfo)
						.writeImage(2, &fluxMapImageInfo)
						.writeImage(3, &normalMapImageInfo)
						.build(reflectiveShadowMaps[actor]->descriptorSet[i]);
				}
			}
			removedRSMs.erase(actor);

			vkCmdBindDescriptorSets(
				frameInfo.commandBuffer,
				VK_PIPELINE_BIND_POINT_GRAPHICS,
				injectionRSMPipelineLayout->getPipelineLayout(),
				1,
				1,
				&reflectiveShadowMaps[actor]->descriptorSet[frameInfo.frameIndex],
				0,
				nullptr
			);
			LPVInjectionPush push;
			push.lpvCenter = lpvCenter[cascade];
			push.lpvExtent = frameInfo.camera.postfx.lightPropagationVolume.extent / pow(frameInfo.camera.postfx.lightPropagationVolume.cascadeLogBase, static_cast<float>(cascade));
			push.res = 256;
			injectionRSMPipeline->bind(frameInfo.commandBuffer);
			vkCmdPushConstants(frameInfo.commandBuffer, injectionRSMPipelineLayout->getPipelineLayout(), VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(push), &push);
			vkCmdDraw(frameInfo.commandBuffer, 256 * 256, 1, 0, 0);
		}

		// cleanup
		for (auto& actor : removedRSMs) {
			if (reflectiveShadowMaps.find(actor) != reflectiveShadowMaps.end()) {
				rsmDestructionQueue.push_back({ reflectiveShadowMaps[actor], S3DDestructionObject(engineDevice, currentFence) });
				reflectiveShadowMaps.erase(actor);
			}
		}
	}

	void LightPropagationVolumeSystem::injectVPLData(FrameInfo& frameInfo, uint32_t cascade) {
		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			injectionVPLPipelineLayout->getPipelineLayout(),
			0,
			1,
			&vplDescriptors[frameInfo.frameIndex],
			0,
			nullptr
		);
		LPVInjectionPush push;
		push.lpvCenter = lpvCenter[cascade];
		push.lpvExtent = frameInfo.camera.postfx.lightPropagationVolume.extent / pow(frameInfo.camera.postfx.lightPropagationVolume.cascadeLogBase, static_cast<float>(cascade));

		vkCmdPushConstants(frameInfo.commandBuffer, injectionVPLPipelineLayout->getPipelineLayout(), VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(push), &push);
		injectionVPLPipeline->bind(frameInfo.commandBuffer);
		vkCmdDraw(frameInfo.commandBuffer, vplCount, 1, 0, 0);
	}

	void LightPropagationVolumeSystem::averageVolume(FrameInfo& frameInfo, uint32_t cascade) {
		VkDescriptorSet sets[2]{
			voxelDescriptorSet,
			shSumDescriptorSet
		};
		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			averagingPass->getPipelineLayout(),
			0,
			2,
			sets,
			0,
			nullptr
		);
		averagingPass->dispatch(frameInfo.commandBuffer);
	}

	void LightPropagationVolumeSystem::propagateVolume(FrameInfo& frameInfo, uint32_t cascade) {
		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			propagationPass->getPipelineLayout(),
			0,
			1,
			&voxelDescriptorSet,
			0,
			nullptr
		);
		LPVPropagationPush push;
		for (int p = 0; p < frameInfo.camera.postfx.lightPropagationVolume.numPropagations; p++) {
			push.pingPongIndex = p % 2;
			propagationPass->push(frameInfo.commandBuffer, push);
			propagationPass->dispatch(frameInfo.commandBuffer);
		}
	}

	void LightPropagationVolumeSystem::blendVolumes(FrameInfo& frameInfo, uint32_t cascade) {
		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			blendPass->getPipelineLayout(),
			0,
			1,
			&voxelDescriptorSet,
			0,
			nullptr
		);
		LPVBlendPush push;
		push.finalPingPongOutput = frameInfo.camera.postfx.lightPropagationVolume.numPropagations % 2;
		push.temporalBlend = 1.0 / frameInfo.camera.postfx.lightPropagationVolume.temporalBlend;
		//push.temporalBlend = 1.0;
		push.blendOffset = glm::ivec4(lpvCenterVx[cascade] - lpvCenterVxpf[cascade],0 );
		//push.blendOffset.z = -push.blendOffset.z;


		push.cascade = cascade;
		blendPass->push(frameInfo.commandBuffer, push);
		blendPass->dispatch(frameInfo.commandBuffer);
		//engineDevice.transitionImageLayout(frameInfo.commandBuffer, lpvSH_Red[cascade]->voxelDataImage, VoxelFormat, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 0, 1, 0, 1);
		//engineDevice.transitionImageLayout(frameInfo.commandBuffer, lpvSH_Green[cascade]->voxelDataImage, VoxelFormat, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 0, 1, 0, 1);
		//engineDevice.transitionImageLayout(frameInfo.commandBuffer, lpvSH_Blue[cascade]->voxelDataImage, VoxelFormat, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 0, 1, 0, 1);
	}

}