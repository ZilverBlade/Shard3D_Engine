#include "deferred_render_system.h"

#include "../../s3dpch.h" 

#include <glm/gtc/constants.hpp>

#include "../handlers/material_system.h"
#include "../../core/asset/assetmgr.h"
#include "../../core/asset/texture_cube.h"
#include "../../ecs.h"
#include "../../core/misc/engine_settings.h"
#include "../../core/rendering/framebuffer.h"
#include "../../core/rendering/render_pass.h"
#include "../../core/vulkan_api/bindless.h"
#include "../handlers/project_system.h"
#include "terrain_system.h"

using namespace Shard3D::ECS;

namespace Shard3D::Systems {

	struct StaticMaterialPushConstantData {
		glm::mat4 modelMatrix;
		glm::mat3x4 normalMatrix;
		uint32_t materialID;
		VkBool32 receiveDecals;
	};
	struct StaticEarlyZPushConstantData {
		glm::mat4 modelMatrix;
		uint32_t materialID;
	};
	struct DynamicGBufferPushConstantData {
		uint32_t actorBuffer;
		uint32_t materialID;
		VkBool32 receiveDecals;
	};
	struct DynamicPushConstantData {
		uint32_t actorBuffer;
		uint32_t materialID;
	};
	struct SSAOPushStruct {
		glm::vec2 screenSize;
		glm::vec2 tiles;
		int tileIndex;
		int interleaveX;
		int interleaveY;
		float radius;
		float bias;
		float intensity;
		float power;
		int kernelQuality; 
		float nearZ, farZ;
	};
	
	struct TerrainPush {
		glm::vec4 translation;
		glm::vec2 tiles;
		float tileExtent;
		float heightMul;
		uint32_t lod;
	};

	struct DeferredLightingPush {
		uint32_t lightIndex;
		float attenuationEpsilon;
	};

	static const glm::vec4 ssaoKernelSamplesLOD0[8] =
	{ // LOW (8 samples)
	glm::vec4(0.01305719,0.5872321,0.219337, 0.0f),
	glm::vec4(0.3230782,0.02207272,0.4188725, 0.0f),
	glm::vec4(-0.310725,-0.191367,0.75613686, 0.0f),
	glm::vec4(-0.4796457,0.09398766,0.5802653, 0.0f),
	glm::vec4(0.1399992,-0.3357702,0.5596789, 0.0f),
	glm::vec4(-0.2484578,0.2555322,0.3489439, 0.0f),
	glm::vec4(0.1871898,-0.702764,0.8317479, 0.0f),
	glm::vec4(0.8849149,0.2842076,0.368524, 0.0f)
	};

	static const glm::vec4 ssaoKernelSamplesLOD1[14] =  // MEDIUM (14 samples)
	{
	glm::vec4(0.4010039,0.8899381,0.11751772, 0.0f),
	glm::vec4(0.1617837,0.1338552,0.3530486, 0.0f),
	glm::vec4(-0.2305296,-0.1900085,0.5025396, 0.0f),
	glm::vec4(-0.6256684,0.1241661,0.1163932, 0.0f),
	glm::vec4(0.3820786,-0.3241398,0.4112825, 0.0f),
	glm::vec4(-0.08829653,0.1649759,0.1395879, 0.0f),
	glm::vec4(0.1891677,-0.1283755,0.09873557, 0.0f),
	glm::vec4(0.1986142,0.1767239,0.4380491, 0.0f),
	glm::vec4(-0.3294966,0.02684341,0.4021836, 0.0f),
	glm::vec4(-0.01956503,-0.3108062,0.410663, 0.0f),
	glm::vec4(-0.3215499,0.6832048,0.3433446, 0.0f),
	glm::vec4(0.7026125,0.1648249,0.02250625, 0.0f),
	glm::vec4(0.03704464,-0.939131,0.1358765, 0.0f),
	glm::vec4(-0.6984446,-0.6003422,0.84016943, 0.0f)
	};
	static const glm::vec4 ssaoKernelSamplesLOD2[26] =
	{ // HIGH (26 samples)
	glm::vec4(0.2196607,0.9032637,0.2254677, 0.0f),
	glm::vec4(0.05916681,0.2201506,0.2430302, 0.0f),
	glm::vec4(-0.4152246,0.1320857,0.7036734, 0.0f),
	glm::vec4(-0.3790807,0.1454145,0.200605, 0.0f),
	glm::vec4(0.3149606,-0.1294581,0.7044517, 0.0f),
	glm::vec4(-0.1108412,0.2162839,0.1336278, 0.0f),
	glm::vec4(0.658012,-0.4395972,0.2919373, 0.0f),
	glm::vec4(0.5377914,0.3112189,0.426864, 0.0f),
	glm::vec4(-0.2752537,0.07625949,0.2273409, 0.0f),
	glm::vec4(-0.1915639,-0.4973421,0.3129629, 0.0f),
	glm::vec4(-0.2634767,0.5277923,0.1107446, 0.0f),
	glm::vec4(0.8242752,0.02434147,0.76049098, 0.0f),
	glm::vec4(0.06262707,-0.2128643,0.23671562, 0.0f),
	glm::vec4(-0.1795662,-0.3543862,0.87924347, 0.0f),
	glm::vec4(0.06039629,0.24629,0.4501176, 0.0f),
	glm::vec4(-0.7786345,-0.3814852,0.2391262, 0.0f),
	glm::vec4(0.2792919,0.2487278,0.25185341, 0.0f),
	glm::vec4(0.1841383,0.1696993,0.8936281, 0.0f),
	glm::vec4(-0.3479781,0.4725766,0.719685, 0.0f),
	glm::vec4(-0.1365018,-0.2513416,0.470937, 0.0f),
	glm::vec4(0.1280388,-0.563242,0.3419276, 0.0f),
	glm::vec4(-0.4800232,-0.1899473,0.2398808, 0.0f),
	glm::vec4(0.6389147,0.1191014,0.5271206, 0.0f),
	glm::vec4(0.1932822,-0.3692099,0.6060588, 0.0f),
	glm::vec4(-0.3465451,-0.1654651,0.6746758, 0.0f),
	glm::vec4(0.2448421,-0.1610962,0.1289366, 0.0f)
	};
	
	DeferredRenderSystem::DeferredRenderSystem(S3DDevice& device, ResourceSystem* resourceSystem, DeferredRenderSystem::RendererFeatures rendererFeatures, VkDescriptorSetLayout globalSetLayout, VkDescriptorSetLayout sceneSetLayout, VkDescriptorSetLayout riggedSkeletonSetLayout, VkDescriptorSetLayout terrainSetLayout, glm::vec2 resolution)
		: engineDevice(device), resourceSystem(resourceSystem), rendererFeatures(rendererFeatures) {
		if (ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit) {
			useReverseDepth = true;
		}
		if (rendererFeatures.enableDeferredLighting) {
			if (rendererFeatures.enableMSAA) {
				SHARD3D_FATAL("Fully deferred lighting and MSAA not currently supported!");
			}
		}

		createRenderPasses(resolution);
		createFramebuffers();

		createSkyboxPipeline(globalSetLayout, sceneSetLayout);
		createTerrainPipelines(globalSetLayout, terrainSetLayout);

		auto lilayoutBuilder = S3DDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT) // depth
			.addBinding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT) // gbuffer0
			.addBinding(2, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT) // gbuffer1
			.addBinding(3, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT) // gbuffer2
			.addBinding(4, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT) // gbuffer3
			.addBinding(5, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT); // gbuffer4


		if (rendererFeatures.enableDeferredLighting) {
			lilayoutBuilder.addBinding(8, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT); // lbuffer0
			lilayoutBuilder.addBinding(9, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT); // lbuffer1
		} else if (rendererFeatures.enableMSAA) {
			lilayoutBuilder.addBinding(8, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT); // gbuffer0	ms
			lilayoutBuilder.addBinding(9, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT); // gbuffer1	ms
			lilayoutBuilder.addBinding(10, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT); // gbuffer2	ms
			lilayoutBuilder.addBinding(11, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT); // sample weights
		}
		if (rendererFeatures.enableSSAO) {
			lilayoutBuilder.addBinding(6, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT); // ssao
			ssaoInputLayout = S3DDescriptorSetLayout::Builder(engineDevice)
				.addBinding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT) // depth
				.addBinding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT) // gbuffer3
				.addBinding(2, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT) // noise texture
				.addBinding(3, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT) // samples
				.build();
		}
		if (rendererFeatures.enableNormalDecals) {
			lilayoutBuilder.addBinding(7, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT); // ssao
		}
		lightingInputLayout = lilayoutBuilder.build();
		alphaCompositeInputLayout = S3DDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT) // col accum
			.addBinding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT) // col reveal
			.build();
		deferredLitLayout = S3DDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT) // deferred lit
			.build();

		if (rendererFeatures.enableMSAA) {
			auto dfResolveLayoutBuilder = S3DDescriptorSetLayout::Builder(engineDevice)
				.addBinding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT) // depth
				.addBinding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT) // gbuffer0
				.addBinding(2, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT) // gbuffer1
				.addBinding(3, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT) // gbuffer2
				.addBinding(4, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT) // gbuffer3
				.addBinding(5, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT); // gbuffer4
			if (rendererFeatures.enableVelocityBuffer) {
				dfResolveLayoutBuilder.addBinding(6, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT); // velocity
			}
			deferredResolveInputLayout = dfResolveLayoutBuilder.build();
		}

		if (rendererFeatures.enableSSAO) {
			createSSAOSampleBuffer();
			createSSAOPipeline(globalSetLayout);
		}
		if (rendererFeatures.enableMSAA) {
			createGBufferResolvePipeline(globalSetLayout);
		}
		if (rendererFeatures.enableDeferredLighting) {
			resourceSystem->loadMesh(AssetID("engine/meshes/spotlightcone.fbx.s3dasset"));
			resourceSystem->loadMesh(AssetID("engine/meshes/pointlightsphere.fbx.s3dasset"));
		}
		createDeferredPipelines(globalSetLayout, sceneSetLayout);
		
		materialSystem = new MaterialSystem(this, resourceSystem, globalSetLayout, sceneSetLayout, riggedSkeletonSetLayout);
		materialSystem->setAllAvailableMaterialShaderPermutations(ProjectSystem::getEngineSettings().allowedMaterialPermutations);
		these.insert(this);
	}
	DeferredRenderSystem::~DeferredRenderSystem() {
		delete framebufferGBuffer;
		delete framebufferWeightedBlending;
		delete framebufferDeferredShading;
		delete framebufferComposition;
		delete renderPassGBuffer;
		delete renderPassWeightedBlending;
		delete renderPassDeferredShading;
		delete renderPassComposition;
		if (rendererFeatures.enableDeferredLighting) {
			delete framebufferDeferredLighting;
			delete renderPassDeferredLighting;
			delete lbuffer0;
			delete lbuffer1;
			delete deferredLightingPipelineLayout;
		}
		if (rendererFeatures.enableEarlyZ) {
			delete framebufferEarlyDepth;
			delete renderPassEarlyDepth;
		}
		if (rendererFeatures.enableSSAO) {
			delete framebufferSSAOInterleave[0][0];
			delete framebufferSSAOInterleave[0][1];
			delete framebufferSSAOInterleave[1][0];
			delete framebufferSSAOInterleave[1][1];
			delete ssaoAttachmentInterleave[0][0];
			delete ssaoAttachmentInterleave[0][1];
			delete ssaoAttachmentInterleave[1][0];
			delete ssaoAttachmentInterleave[1][1];
			delete renderPassSSAO;
			engineDevice.staticMaterialPool->freeDescriptors({ ssaoInputDescriptorSet, ssaoSamplesInputDescriptorSet });
		}

		delete finalRenderTarget;
		delete depthRenderTarget;
		delete gbuffer0;
		delete gbuffer1;
		delete gbuffer2;
		delete gbuffer3;
		delete gbuffer4;
		if (rendererFeatures.enableVelocityBuffer) {
			delete velocityBuffer;
		}
		delete colorAccumulationAttachment;
		delete colorRevealAttachment;

		delete deferredShadingPipelineLayout;
		delete deferredCompositionPipelineLayout;
		if (rendererFeatures.enableMSAA) {
			delete depthRenderTargetMS;
			delete gbuffer0MS;
			delete gbuffer1MS;
			delete gbuffer2MS;
			delete gbuffer3MS;
			delete gbuffer4MS;
			delete msaaSampleWeightBuffer;
			delete msaaSampleMaskStencilBuffer;
			if (rendererFeatures.enableVelocityBuffer) {
				delete velocityBufferMS;
			}
			delete colorAccumulationAttachmentMS;
			delete colorRevealAttachmentMS;

			delete framebufferGBufferMSResolve;
			delete renderPassGBufferMSResolve;
			delete framebufferGBufferMSResolveStencilWrite;
			delete renderPassGBufferMSResolveStencilWrite;
			delete renderPassDeferredShadingMS;

			delete deferredResolveMSStencilWritePipelineLayout;
			delete deferredResolvePipelineLayout;
			engineDevice.staticMaterialPool->freeDescriptors({  deferredResolveInputDescriptorSet });
		}

		engineDevice.staticMaterialPool->freeDescriptors({ alphaCompositeInputDescriptorSet, deferredLitDescriptorSet,
			lightingInputDescriptorSet });
		delete skyboxPipelineLayout;
		delete ssaoPipelineLayout;
		delete materialSystem;

		these.erase(this);
	}


	void DeferredRenderSystem::createRenderPasses(glm::vec2 resolution) {
		VkFormat depthFormat;
		switch (ProjectSystem::getEngineSettings().renderer.depthBufferFormat) {
		case (ESET::DepthBufferFormat::Log16Bit):
			if (rendererFeatures.enableStencilBuffer) {
				depthFormat = VK_FORMAT_D16_UNORM_S8_UINT;
			} else {
				depthFormat = VK_FORMAT_D16_UNORM;
			}
			break;
		case (ESET::DepthBufferFormat::StdF32Bit):
		case (ESET::DepthBufferFormat::ReverseF32Bit):
			if (rendererFeatures.enableStencilBuffer) {
				depthFormat = VK_FORMAT_D32_SFLOAT_S8_UINT;
			} else {
				depthFormat = VK_FORMAT_D32_SFLOAT;
			}
			break;
		}

		glm::ivec3 viewportSize = { resolution, 1 };
		glm::ivec3 ssaoSize = glm::ivec3(resolution * ProjectSystem::getGraphicsSettings().postfx.ssao.quality, 1);
		finalRenderTarget = new S3DRenderTarget(engineDevice, {
			rendererFeatures.finalFrameBufferFormat,
			VK_IMAGE_ASPECT_COLOR_BIT,
			VK_IMAGE_VIEW_TYPE_2D,
			viewportSize,
			VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
			S3DRenderTargetType::Color,
			true,
			VK_SAMPLE_COUNT_1_BIT
			}
		);

		depthRenderTarget = new S3DRenderTarget(engineDevice, {
			depthFormat,
			VK_IMAGE_ASPECT_DEPTH_BIT,
			VK_IMAGE_VIEW_TYPE_2D,
			viewportSize,
			VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT,
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
			rendererFeatures.enableStencilBuffer ? S3DRenderTargetType::DepthStencil : S3DRenderTargetType::Depth,
			false,
			VK_SAMPLE_COUNT_1_BIT
			}
		);
		gbuffer0 = new S3DRenderTarget(engineDevice, {
			VK_FORMAT_R8G8B8A8_UNORM,
			VK_IMAGE_ASPECT_COLOR_BIT,
			VK_IMAGE_VIEW_TYPE_2D,
			viewportSize,
			VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
			S3DRenderTargetType::Color,
			false,
			VK_SAMPLE_COUNT_1_BIT
			}
		);
		gbuffer1 = new S3DRenderTarget(engineDevice, {
			VK_FORMAT_R8G8_UNORM,
			VK_IMAGE_ASPECT_COLOR_BIT,
			VK_IMAGE_VIEW_TYPE_2D,
			viewportSize,
			VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
			S3DRenderTargetType::Color,
			false,
			VK_SAMPLE_COUNT_1_BIT
			}
		);
		gbuffer2 = new S3DRenderTarget(engineDevice, {
			VK_FORMAT_B10G11R11_UFLOAT_PACK32,
			VK_IMAGE_ASPECT_COLOR_BIT,
			VK_IMAGE_VIEW_TYPE_2D,
			viewportSize,
			VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
			S3DRenderTargetType::Color,
			false,
			VK_SAMPLE_COUNT_1_BIT
			}
		);
		gbuffer3 = new S3DRenderTarget(engineDevice, {
			VK_FORMAT_R32_UINT,
			VK_IMAGE_ASPECT_COLOR_BIT,
			VK_IMAGE_VIEW_TYPE_2D,
			viewportSize,
			VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
			S3DRenderTargetType::Color,
			false,
			VK_SAMPLE_COUNT_1_BIT
			}
		);
		gbuffer4 = new S3DRenderTarget(engineDevice, {
			VK_FORMAT_R8_UINT,
			VK_IMAGE_ASPECT_COLOR_BIT,
			VK_IMAGE_VIEW_TYPE_2D,
			viewportSize,
			VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
			S3DRenderTargetType::Color,
			false,
			VK_SAMPLE_COUNT_1_BIT
			}
		);
		colorAccumulationAttachment = new S3DRenderTarget(engineDevice, {
			VK_FORMAT_R16G16B16A16_SFLOAT,
			VK_IMAGE_ASPECT_COLOR_BIT,
			VK_IMAGE_VIEW_TYPE_2D,
			viewportSize,
			VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
			rendererFeatures.enableMSAA ? S3DRenderTargetType::Resolve : S3DRenderTargetType::Color,
			false,
			VK_SAMPLE_COUNT_1_BIT
			}
		);
		colorRevealAttachment = new S3DRenderTarget(engineDevice, {
			VK_FORMAT_R8_UNORM,
			VK_IMAGE_ASPECT_COLOR_BIT,
			VK_IMAGE_VIEW_TYPE_2D,
			viewportSize,
			VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
			rendererFeatures.enableMSAA ? S3DRenderTargetType::Resolve : S3DRenderTargetType::Color,
			false,
			VK_SAMPLE_COUNT_1_BIT
			}
		);

		if (rendererFeatures.enableVelocityBuffer) {
			velocityBuffer = new S3DRenderTarget(engineDevice, {
					rendererFeatures.velocityBufferFormat,
					VK_IMAGE_ASPECT_COLOR_BIT,
					VK_IMAGE_VIEW_TYPE_2D,
					viewportSize,
					VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
					VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
					S3DRenderTargetType::Color,
					false,
					VK_SAMPLE_COUNT_1_BIT
				}
			);
		}
		if (rendererFeatures.enableDeferredLighting) {
			lbuffer0 = new S3DRenderTarget(engineDevice, {
				VK_FORMAT_B10G11R11_UFLOAT_PACK32,
				VK_IMAGE_ASPECT_COLOR_BIT,
				VK_IMAGE_VIEW_TYPE_2D,
				viewportSize,
				VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				S3DRenderTargetType::Color,
				false,
				VK_SAMPLE_COUNT_1_BIT
				}
			);
			lbuffer1 = new S3DRenderTarget(engineDevice, {
				VK_FORMAT_B10G11R11_UFLOAT_PACK32,
				VK_IMAGE_ASPECT_COLOR_BIT,
				VK_IMAGE_VIEW_TYPE_2D,
				viewportSize,
				VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				S3DRenderTargetType::Color,
				false,
				VK_SAMPLE_COUNT_1_BIT
				}
			);
		}
		if (rendererFeatures.enableMSAA) {
			depthRenderTargetMS = new S3DRenderTarget(engineDevice, {
				depthFormat,
				VK_IMAGE_ASPECT_DEPTH_BIT,
				VK_IMAGE_VIEW_TYPE_2D,
				viewportSize,
				VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				rendererFeatures.enableStencilBuffer ? S3DRenderTargetType::DepthStencil : S3DRenderTargetType::Depth,
				false,
				rendererFeatures.msaaSamples
				}
			);
			gbuffer0MS = new S3DRenderTarget(engineDevice, {
				VK_FORMAT_R8G8B8A8_UNORM,
				VK_IMAGE_ASPECT_COLOR_BIT,
				VK_IMAGE_VIEW_TYPE_2D,
				viewportSize,
				VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				S3DRenderTargetType::Color,
				false,
				rendererFeatures.msaaSamples
				}
			);
			gbuffer1MS = new S3DRenderTarget(engineDevice, {
				VK_FORMAT_R8G8_UNORM,
				VK_IMAGE_ASPECT_COLOR_BIT,
				VK_IMAGE_VIEW_TYPE_2D,
				viewportSize,
				VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				S3DRenderTargetType::Color,
				false,
				rendererFeatures.msaaSamples
				}
			);
			gbuffer2MS = new S3DRenderTarget(engineDevice, {
				VK_FORMAT_B10G11R11_UFLOAT_PACK32,
				VK_IMAGE_ASPECT_COLOR_BIT,
				VK_IMAGE_VIEW_TYPE_2D,
				viewportSize,
				VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				S3DRenderTargetType::Color,
				false,
				rendererFeatures.msaaSamples
				}
			);
			gbuffer3MS = new S3DRenderTarget(engineDevice, {
				VK_FORMAT_R32_UINT,
				VK_IMAGE_ASPECT_COLOR_BIT,
				VK_IMAGE_VIEW_TYPE_2D,
				viewportSize,
				VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				S3DRenderTargetType::Color,
				false,
				rendererFeatures.msaaSamples
				}
			);
			gbuffer4MS = new S3DRenderTarget(engineDevice, {
				VK_FORMAT_R8_UINT,
				VK_IMAGE_ASPECT_COLOR_BIT,
				VK_IMAGE_VIEW_TYPE_2D,
				viewportSize,
				VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				S3DRenderTargetType::Color,
				false,
				rendererFeatures.msaaSamples
				}
			);
			colorAccumulationAttachmentMS = new S3DRenderTarget(engineDevice, {
				VK_FORMAT_R16G16B16A16_SFLOAT,
				VK_IMAGE_ASPECT_COLOR_BIT,
				VK_IMAGE_VIEW_TYPE_2D,
				viewportSize,
				VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				S3DRenderTargetType::Color,
				false,
				rendererFeatures.msaaSamples
				}
			);
			colorRevealAttachmentMS = new S3DRenderTarget(engineDevice, {
				VK_FORMAT_R8_UNORM,
				VK_IMAGE_ASPECT_COLOR_BIT,
				VK_IMAGE_VIEW_TYPE_2D,
				viewportSize,
				VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				S3DRenderTargetType::Color,
				false,
				rendererFeatures.msaaSamples
				}
			);

			VkFormat sampleWeightBufferFormat;
			switch (rendererFeatures.msaaSamples) {
			case(VK_SAMPLE_COUNT_2_BIT):
				sampleWeightBufferFormat = VK_FORMAT_R8_UINT;
				break;
			case(VK_SAMPLE_COUNT_4_BIT):
				sampleWeightBufferFormat = VK_FORMAT_R16_UINT;
				break;
			case(VK_SAMPLE_COUNT_8_BIT):
				sampleWeightBufferFormat = VK_FORMAT_R32_UINT;
				break;
			}

			msaaSampleWeightBuffer = new S3DRenderTarget(engineDevice, {
				sampleWeightBufferFormat,
				VK_IMAGE_ASPECT_COLOR_BIT,
				VK_IMAGE_VIEW_TYPE_2D,
				viewportSize,
				VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				S3DRenderTargetType::Color,
				false,
				VK_SAMPLE_COUNT_1_BIT
				}
			);

			msaaSampleMaskStencilBuffer = new S3DRenderTarget(engineDevice, {
				VK_FORMAT_S8_UINT,
				VK_IMAGE_ASPECT_STENCIL_BIT,
				VK_IMAGE_VIEW_TYPE_2D,
				viewportSize,
				VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
				VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
				S3DRenderTargetType::DepthStencil,
				false,
				VK_SAMPLE_COUNT_1_BIT
				}
			);

			if (rendererFeatures.enableVelocityBuffer) {
				velocityBufferMS = new S3DRenderTarget(engineDevice, {
					rendererFeatures.velocityBufferFormat,
					VK_IMAGE_ASPECT_COLOR_BIT,
					VK_IMAGE_VIEW_TYPE_2D,
					viewportSize,
					VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
					VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
					S3DRenderTargetType::Color,
					false,
					rendererFeatures.msaaSamples
					}
				);
			}
			
		}

		AttachmentClearColor gbufferClear{ };
		gbufferClear.color = { 0.f, 0.f, 0.f, 0.f };
		AttachmentClearColor gbufferNormalClear{ };
		gbufferNormalClear.color = { 0u };
		AttachmentClearColor gbufferShadingModelClear{ };
		gbufferShadingModelClear.color = { 0xFFu };
		AttachmentClearColor colorRevealClear{ };
		colorRevealClear.color = { 1.0f };
		AttachmentClearColor colorAccumClear{ };
		colorAccumClear.color = { 0.0f, 0.0f, 0.0f, 0.0f };
		AttachmentClearColor ssaoClear{ };
		ssaoClear.color = { 1.0f };
		AttachmentClearColor depthClear{ };
		depthClear.depth = { useReverseDepth ? 0.0f : 1.0f, 0x00u };
		AttachmentInfo depthClearAttachment = { rendererFeatures.enableMSAA ? depthRenderTargetMS : depthRenderTarget, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, depthClear };
		AttachmentInfo depthLoadAttachment = { rendererFeatures.enableMSAA ? depthRenderTargetMS : depthRenderTarget, VK_ATTACHMENT_LOAD_OP_LOAD, VK_ATTACHMENT_STORE_OP_STORE};
		if (rendererFeatures.enableStencilBuffer) {
			depthClearAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			depthClearAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_STORE;
			depthLoadAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
			depthLoadAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_STORE;
		}
		
		if (rendererFeatures.enableEarlyZ) {
			renderPassEarlyDepth = new S3DRenderPass(
				engineDevice, {
				depthClearAttachment,
			});
		}
		std::vector<AttachmentInfo> gbufferInfos = {
			rendererFeatures.enableEarlyZ ? depthLoadAttachment : depthClearAttachment,
			AttachmentInfo{ rendererFeatures.enableMSAA ? gbuffer0MS : gbuffer0, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, gbufferClear },
			AttachmentInfo{ rendererFeatures.enableMSAA ? gbuffer1MS : gbuffer1, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, gbufferClear },
			AttachmentInfo{ rendererFeatures.enableMSAA ? gbuffer2MS : gbuffer2, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, gbufferClear },
			AttachmentInfo{ rendererFeatures.enableMSAA ? gbuffer3MS : gbuffer3, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, gbufferNormalClear },
			AttachmentInfo{ rendererFeatures.enableMSAA ? gbuffer4MS : gbuffer4, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, gbufferShadingModelClear }
		};
		if (rendererFeatures.enableVelocityBuffer) {
			gbufferInfos.push_back(AttachmentInfo{ rendererFeatures.enableMSAA ? velocityBufferMS : velocityBuffer, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, gbufferClear });
		}
 		renderPassGBuffer = new S3DRenderPass(engineDevice, gbufferInfos);
		if (rendererFeatures.enableMSAA) {
			renderPassWeightedBlending = new S3DRenderPass(
				engineDevice, {
				depthLoadAttachment, // depth comparison
				AttachmentInfo{colorAccumulationAttachmentMS, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, colorAccumClear },
				AttachmentInfo{colorRevealAttachmentMS, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, colorRevealClear },
				AttachmentInfo{colorAccumulationAttachment, VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_STORE },
				AttachmentInfo{colorRevealAttachment, VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_STORE },
			});
		} else {
			renderPassWeightedBlending = new S3DRenderPass(
				engineDevice, {
				depthLoadAttachment, // depth comparison
				AttachmentInfo{colorAccumulationAttachment, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, colorAccumClear },
				AttachmentInfo{colorRevealAttachment, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, colorRevealClear },
			});
		}

		std::vector<AttachmentInfo> lightingAttachmentInfos{
			AttachmentInfo{finalRenderTarget, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE }
		};
		if (rendererFeatures.enableMSAA) {
			lightingAttachmentInfos.push_back(AttachmentInfo{ msaaSampleMaskStencilBuffer,
				VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE, {}, VK_ATTACHMENT_LOAD_OP_LOAD, VK_ATTACHMENT_STORE_OP_STORE });
			renderPassDeferredShadingMS = new S3DRenderPass(
				engineDevice, {AttachmentInfo{finalRenderTarget, VK_ATTACHMENT_LOAD_OP_LOAD, VK_ATTACHMENT_STORE_OP_STORE },
				lightingAttachmentInfos.back() });
		}
		renderPassDeferredShading = new S3DRenderPass(
			engineDevice, {
			lightingAttachmentInfos
		});

		if (rendererFeatures.enableDeferredLighting) {
			renderPassDeferredLighting = new S3DRenderPass(
				engineDevice, {
					depthLoadAttachment,
					AttachmentInfo{lbuffer0, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, gbufferClear },
					AttachmentInfo{lbuffer1, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, gbufferClear }
			});
		}
		AttachmentInfo depthResolved = { depthRenderTarget, VK_ATTACHMENT_LOAD_OP_LOAD, VK_ATTACHMENT_STORE_OP_STORE , {}, VK_ATTACHMENT_LOAD_OP_LOAD, VK_ATTACHMENT_STORE_OP_STORE };
		renderPassComposition = new S3DRenderPass(
			engineDevice, {
			depthResolved, // depth comparison
			AttachmentInfo{finalRenderTarget, VK_ATTACHMENT_LOAD_OP_LOAD, VK_ATTACHMENT_STORE_OP_STORE }, // load previously composed image
		});

		if (rendererFeatures.enableMSAA) {
			std::vector<AttachmentInfo> gbufferResolveInfos = {
				AttachmentInfo{ depthRenderTarget, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, depthClear },
				AttachmentInfo{ gbuffer0, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, gbufferClear },
				AttachmentInfo{ gbuffer1, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, gbufferClear },
				AttachmentInfo{ gbuffer2, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, gbufferClear },
				AttachmentInfo{ gbuffer3, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, gbufferNormalClear },
				AttachmentInfo{ gbuffer4, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, gbufferShadingModelClear },
				AttachmentInfo{ msaaSampleWeightBuffer, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, gbufferClear }
			};
			if (rendererFeatures.enableVelocityBuffer) {
				gbufferResolveInfos.push_back(AttachmentInfo{ velocityBuffer, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, gbufferClear });
			}
			
			renderPassGBufferMSResolve = new S3DRenderPass(
				engineDevice, gbufferResolveInfos);
			renderPassGBufferMSResolveStencilWrite = new S3DRenderPass(
				engineDevice, { { msaaSampleMaskStencilBuffer, VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE, depthClear, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE }});
		}

		if (rendererFeatures.enableSSAO) {
			glm::ivec3 ssaoInterleaveSize = glm::ivec3(glm::ceil(glm::vec2(ssaoSize) / 2.0f), 1);
			for (int x = 0; x < 2; x++) {
				for (int y = 0; y < 2; y++) {
					ssaoAttachmentInterleave[x][y] = new S3DRenderTarget(engineDevice, {
						VK_FORMAT_R8_UNORM,
						VK_IMAGE_ASPECT_COLOR_BIT,
						VK_IMAGE_VIEW_TYPE_2D,
						ssaoInterleaveSize,
						VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
						VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
						S3DRenderTargetType::Color,
						true,
						VK_SAMPLE_COUNT_1_BIT
						}
					);
				}
			}
			renderPassSSAO = new S3DRenderPass(
				engineDevice, {
				AttachmentInfo{ssaoAttachmentInterleave[0][0], VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE, ssaoClear },
			});
			ssaoInterleaveCombine = make_uPtr<PostProcessingEffect>(engineDevice, ssaoSize,
				S3DShader{ ShaderType::Fragment, "resources/shaders/postfx/ssao_interleave_combine.frag", "postfx" },
				std::vector<VkDescriptorImageInfo>{
				ssaoAttachmentInterleave[0][0]->getDescriptor(), ssaoAttachmentInterleave[0][1]->getDescriptor(), 
					ssaoAttachmentInterleave[1][0]->getDescriptor(), ssaoAttachmentInterleave[1][1]->getDescriptor(), },
				VK_FORMAT_R8_UNORM
			);
			ssaoBlurH0 = make_uPtr<PostProcessingEffect>(engineDevice, ssaoSize,
				S3DShader{ ShaderType::Fragment, "resources/shaders/postfx/ssao_bilateral_blur.frag", "postfx" },
				std::vector<VkDescriptorImageInfo>{ssaoInterleaveCombine->getAttachment()->getDescriptor(), depthRenderTarget->getDescriptor()},
				VK_FORMAT_R8_UNORM
			);
			ssaoBlurV0 = make_uPtr<PostProcessingEffect>(engineDevice, ssaoSize,
				S3DShader{ ShaderType::Fragment, "resources/shaders/postfx/ssao_bilateral_blur.frag", "postfx" },
				std::vector<VkDescriptorImageInfo>{ssaoBlurH0->getAttachment()->getDescriptor(), depthRenderTarget->getDescriptor() },
				VK_FORMAT_R8_UNORM
			);
			ssaoProcess = make_uPtr<PostProcessingEffect>(engineDevice, resolution,
				S3DShader{ ShaderType::Fragment, "resources/shaders/postfx/ssao_process.frag", "postfx" },
				std::vector<VkDescriptorImageInfo>{ ssaoBlurV0->getAttachment()->getDescriptor(), depthRenderTarget->getDescriptor()  },
				VK_FORMAT_R8_UNORM
			);
		}
	}

	void DeferredRenderSystem::createFramebuffers() {
		if (rendererFeatures.enableEarlyZ) {
			framebufferEarlyDepth = new S3DFramebuffer(
				engineDevice, renderPassEarlyDepth, {
					rendererFeatures.enableMSAA ? depthRenderTargetMS : depthRenderTarget
			});
		}
		std::vector<S3DRenderTarget*> gbufferAttachments = {
			rendererFeatures.enableMSAA ? depthRenderTargetMS : depthRenderTarget,
			rendererFeatures.enableMSAA ? gbuffer0MS : gbuffer0,
			rendererFeatures.enableMSAA ? gbuffer1MS : gbuffer1,
			rendererFeatures.enableMSAA ? gbuffer2MS : gbuffer2,
			rendererFeatures.enableMSAA ? gbuffer3MS : gbuffer3,
			rendererFeatures.enableMSAA ? gbuffer4MS : gbuffer4
		};
		if (rendererFeatures.enableVelocityBuffer) {
			gbufferAttachments.push_back(rendererFeatures.enableMSAA ? velocityBufferMS : velocityBuffer);
		}
		framebufferGBuffer = new S3DFramebuffer(engineDevice, renderPassGBuffer, gbufferAttachments);
		
		if (rendererFeatures.enableSSAO) {
			framebufferSSAOInterleave[0][0] = new S3DFramebuffer(
				engineDevice, renderPassSSAO, {
					ssaoAttachmentInterleave[0][0]
				});
			framebufferSSAOInterleave[0][1] = new S3DFramebuffer(
				engineDevice, renderPassSSAO, {
					ssaoAttachmentInterleave[0][1]
				});
			framebufferSSAOInterleave[1][0] = new S3DFramebuffer(
				engineDevice, renderPassSSAO, {
					ssaoAttachmentInterleave[1][0]
				});
			framebufferSSAOInterleave[1][1] = new S3DFramebuffer(
				engineDevice, renderPassSSAO, {
					ssaoAttachmentInterleave[1][1]
				});
		}
		if (rendererFeatures.enableMSAA) {
			framebufferWeightedBlending = new S3DFramebuffer(
				engineDevice, renderPassWeightedBlending, {
					depthRenderTargetMS,
					colorAccumulationAttachmentMS,
					colorRevealAttachmentMS,
					colorAccumulationAttachment,
					colorRevealAttachment
				});
		} else {
			framebufferWeightedBlending = new S3DFramebuffer(
				engineDevice, renderPassWeightedBlending, {
					depthRenderTarget,
					colorAccumulationAttachment,
					colorRevealAttachment
				});

		}
		std::vector<S3DRenderTarget*> renderTargetsLighting{
			finalRenderTarget
		};
		if (rendererFeatures.enableMSAA) {
			renderTargetsLighting.push_back(msaaSampleMaskStencilBuffer);
		}
		if (rendererFeatures.enableDeferredLighting) {
			framebufferDeferredLighting = new S3DFramebuffer(
				engineDevice, renderPassDeferredLighting, {
					depthRenderTarget, lbuffer0, lbuffer1
			});
		}
		framebufferDeferredShading = new S3DFramebuffer(
			engineDevice, renderPassDeferredShading, renderTargetsLighting);
		framebufferComposition = new S3DFramebuffer(
			engineDevice, renderPassComposition, {
				depthRenderTarget,
				finalRenderTarget
		});

		if (rendererFeatures.enableMSAA) {
			std::vector<S3DRenderTarget*> gbufferResolveAttachments = {
				depthRenderTarget,
				gbuffer0,
				gbuffer1,
				gbuffer2,
				gbuffer3,
				gbuffer4,
				msaaSampleWeightBuffer
			};
			if (rendererFeatures.enableVelocityBuffer) {
				gbufferResolveAttachments.push_back(velocityBuffer);
			}
			framebufferGBufferMSResolve = new S3DFramebuffer(engineDevice, renderPassGBufferMSResolve, gbufferResolveAttachments);
			framebufferGBufferMSResolveStencilWrite = new S3DFramebuffer(engineDevice, renderPassGBufferMSResolveStencilWrite, { msaaSampleMaskStencilBuffer });
		}
	}

	void DeferredRenderSystem::writeDescriptors(VkDescriptorImageInfo normalDBuffer) {
		VkDescriptorImageInfo depthimageInfo = depthRenderTarget->getDescriptor();
		auto lightingInputWriter = S3DDescriptorWriter(*lightingInputLayout, *engineDevice.staticMaterialPool)
			.writeImage(0, rendererFeatures.enableMSAA ? &depthRenderTargetMS->getDescriptor() : &depthimageInfo) // TODO image layout error
			.writeImage(1, &gbuffer0->getDescriptor())
			.writeImage(2, &gbuffer1->getDescriptor())
			.writeImage(3, &gbuffer2->getDescriptor())
			.writeImage(4, rendererFeatures.enableMSAA ? &gbuffer3MS->getDescriptor() : &gbuffer3->getDescriptor())
			.writeImage(5, rendererFeatures.enableMSAA ? &gbuffer4MS->getDescriptor() : &gbuffer4->getDescriptor());

		VkDescriptorImageInfo lbuffer0Info;
		VkDescriptorImageInfo lbuffer1Info;
		VkDescriptorImageInfo gbuffer0MSInfo;
		VkDescriptorImageInfo gbuffer1MSInfo;
		VkDescriptorImageInfo gbuffer2MSInfo;
		VkDescriptorImageInfo msaaSampleWeightInfo;

		if (rendererFeatures.enableDeferredLighting) {
			lbuffer0Info = lbuffer0->getDescriptor();
			lbuffer1Info = lbuffer1->getDescriptor();

			lightingInputWriter.writeImage(8, &lbuffer0Info);
			lightingInputWriter.writeImage(9, &lbuffer1Info);
		} else if (rendererFeatures.enableMSAA) {
			gbuffer0MSInfo = gbuffer0MS->getDescriptor();
			gbuffer1MSInfo = gbuffer1MS->getDescriptor();
			gbuffer2MSInfo = gbuffer2MS->getDescriptor();
			msaaSampleWeightInfo = msaaSampleWeightBuffer->getDescriptor();
			lightingInputWriter.writeImage(8, &gbuffer0MSInfo);
			lightingInputWriter.writeImage(9, &gbuffer1MSInfo);
			lightingInputWriter.writeImage(10, &gbuffer2MSInfo);
			lightingInputWriter.writeImage(11, &msaaSampleWeightInfo);
		}
		if (rendererFeatures.enableSSAO) {
			auto blueNoiseTexture = resourceSystem->retrieveTexture(AssetID("engine/textures/internal/blue_noise_128x128.s3dasset"))->getImageInfo();
			auto ssaoSampleBufferInfo = ssaoSampleBuffer->descriptorInfo();
			S3DDescriptorWriter(*ssaoInputLayout, *engineDevice.staticMaterialPool)
				.writeImage(0, &depthimageInfo)
				.writeImage(1, &gbuffer3->getDescriptor())
				.writeImage(2, &blueNoiseTexture)
				.writeBuffer(3, &ssaoSampleBufferInfo)
				.build(ssaoInputDescriptorSet);
			VkDescriptorImageInfo ssaoimageInfo = ssaoProcess->getAttachment()->getDescriptor();
			lightingInputWriter.writeImage(6, &ssaoimageInfo);
		}
		if (rendererFeatures.enableNormalDecals) {
			lightingInputWriter.writeImage(7, &normalDBuffer);
		}
		lightingInputWriter.build(lightingInputDescriptorSet);

		VkDescriptorImageInfo alphaColAccum = colorAccumulationAttachment->getDescriptor();
		VkDescriptorImageInfo alphaColReveal = colorRevealAttachment->getDescriptor();;
		S3DDescriptorWriter(*alphaCompositeInputLayout, *engineDevice.staticMaterialPool)
			.writeImage(0, &alphaColAccum)
			.writeImage(1, &alphaColReveal)
			.build(alphaCompositeInputDescriptorSet);

		S3DDescriptorWriter(*deferredLitLayout, *engineDevice.staticMaterialPool)
			.writeImage(0, &finalRenderTarget->getDescriptor())
			.build(deferredLitDescriptorSet);

		if (rendererFeatures.enableMSAA) {
			auto gbufferResolveInputWriter = S3DDescriptorWriter(*deferredResolveInputLayout, *engineDevice.staticMaterialPool)
				.writeImage(0, &depthRenderTargetMS->getDescriptor())
				.writeImage(1, &gbuffer0MS->getDescriptor())
				.writeImage(2, &gbuffer1MS->getDescriptor())
				.writeImage(3, &gbuffer2MS->getDescriptor())
				.writeImage(4, &gbuffer3MS->getDescriptor())
				.writeImage(5, &gbuffer4MS->getDescriptor());
			if (rendererFeatures.enableVelocityBuffer) {
				gbufferResolveInputWriter.writeImage(6, &velocityBufferMS->getDescriptor());
			}
			gbufferResolveInputWriter.build(deferredResolveInputDescriptorSet);
		}
	}

	void DeferredRenderSystem::createSSAOSampleBuffer() {
		int sampleCount = 0;
		int qual = ProjectSystem::getGraphicsSettings().postfx.ssao.kernelQuality;
		int bSize = 0;
		glm::vec4* useSamples;
		switch (qual) {
		case(2):
			sampleCount = 18;
			bSize = sampleCount * sizeof(glm::vec4);
			useSamples = (glm::vec4*)malloc(bSize);
			memcpy(useSamples, ssaoKernelSamplesLOD2, bSize);
			break;
		case(1):
			sampleCount = 10;
			bSize = sampleCount * sizeof(glm::vec4);
			useSamples = (glm::vec4*)malloc(bSize);
			memcpy(useSamples, ssaoKernelSamplesLOD1, bSize);
			break;
		case(0):
		default:
			sampleCount = 5;
			bSize = sampleCount * sizeof(glm::vec4);
			useSamples = (glm::vec4*)malloc(bSize);
			memcpy(useSamples, ssaoKernelSamplesLOD0, bSize);
			break;
		}

		for (int i = 0; i < sampleCount; i++) {
			useSamples[i] = glm::normalize(useSamples[i]);
		}

		ssaoSampleBuffer = make_uPtr<S3DBuffer>(
				engineDevice,
				sizeof(int) * 4 + bSize,
				1,
				VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
			);
		
		ssaoSampleBuffer->writeToBuffer(&sampleCount, 0, 4);
		ssaoSampleBuffer->writeToBuffer(useSamples, 16, bSize);
		ssaoSampleBuffer->flush();

		free(useSamples);
	}

	void DeferredRenderSystem::createSkyboxPipeline(VkDescriptorSetLayout globalSetLayout, VkDescriptorSetLayout sceneSetLayout) {
		std::vector<VkDescriptorSetLayout> descriptorSetLayouts{
			globalSetLayout,
			sceneSetLayout,
			resourceSystem->getDescriptor()->getLayout()
		};

		skyboxPipelineLayout = new S3DPipelineLayout(engineDevice, {}, descriptorSetLayouts);

		S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo();
		pipelineConfig.setCullMode(VK_CULL_MODE_FRONT_BIT); // we only want to see the "inside"
		pipelineConfig.enableVertexDescriptions(VertexDescriptionOptions_Position);
		pipelineConfig.disableDepthWrite();
		if (useReverseDepth) {
			pipelineConfig.reverseDepth();
		}

		pipelineConfig.renderPass = renderPassComposition->getRenderPass();
		pipelineConfig.pipelineLayout = skyboxPipelineLayout->getPipelineLayout();
		skyboxPipeline = make_uPtr<S3DGraphicsPipeline>(
			engineDevice, std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/rendering/skybox.vert", "rendering" },
				S3DShader{ ShaderType::Fragment, "resources/shaders/rendering/skybox.frag", "rendering" },
			},
			pipelineConfig
		);
	}

	void DeferredRenderSystem::createSSAOPipeline(VkDescriptorSetLayout globalSetLayout) {
		std::vector<VkDescriptorSetLayout> descriptorSetLayouts{
			   globalSetLayout,
			   ssaoInputLayout->getDescriptorSetLayout()
		};
		VkPushConstantRange pushRange{};
		pushRange.offset = 0;
		pushRange.size = sizeof(SSAOPushStruct);
		pushRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT |
			VK_SHADER_STAGE_FRAGMENT_BIT;
		ssaoPipelineLayout = new S3DPipelineLayout(engineDevice, { pushRange }, descriptorSetLayouts);
		S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo();
		//pipelineConfig.setCullMode(VK_CULL_MODE_BACK_BIT);
		pipelineConfig.disableDepthTest();

		pipelineConfig.renderPass = renderPassSSAO->getRenderPass();
		pipelineConfig.pipelineLayout = ssaoPipelineLayout->getPipelineLayout();
		ssaoPipeline = make_uPtr<S3DGraphicsPipeline>(
			engineDevice, std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/fullscreen_quad_tileable.vert", "misc" },
				S3DShader{ ShaderType::Fragment, "resources/shaders/rendering/ssao.frag", "rendering" },
			},
			pipelineConfig
		);
	}

	void DeferredRenderSystem::createTerrainPipelines(VkDescriptorSetLayout globalSetLayout, VkDescriptorSetLayout terrainSetLayout) {
		terrainPush = S3DPushConstant(sizeof(TerrainPush), VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT);
		terrainPipelineLayout = make_uPtr<S3DPipelineLayout>(
			engineDevice, std::vector<VkPushConstantRange> { terrainPush.getRange() },
			std::vector<VkDescriptorSetLayout>{ globalSetLayout, resourceSystem->getDescriptor()->getLayout(), terrainSetLayout });
		

		S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo(rendererFeatures.enableVelocityBuffer ? 6 : 5);
		pipelineConfig.colorBlendAttachments[2].colorWriteMask = 0; // no emission write
		if (rendererFeatures.enableVelocityBuffer) {
			pipelineConfig.colorBlendAttachments[5].colorWriteMask = 0; // no velocity write
		}
		pipelineConfig.enableVertexDescriptions(VertexDescriptionOptions_Position | VertexDescriptionOptions_UV);
		pipelineConfig.setCullMode(VK_CULL_MODE_BACK_BIT);
		if (rendererFeatures.enableMSAA) {
			pipelineConfig.setSampleCount(rendererFeatures.msaaSamples);
		}
		if (ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit) {
			pipelineConfig.reverseDepth();
		}

		S3DShaderSpecialization specializationVert;
		specializationVert.addConstant(0, static_cast<VkBool32>(rendererFeatures.enableClipPlane));
		specializationVert.addConstant(1, static_cast<uint32_t>(4));
		specializationVert.addConstant(2, static_cast<VkBool32>(ProjectSystem::getGraphicsSettings().renderer.parallaxMapping));

		pipelineConfig.pipelineLayout = terrainPipelineLayout->getPipelineLayout();
		pipelineConfig.renderPass = renderPassGBuffer->getRenderPass();

		terrainGBufferPipeline = make_uPtr<S3DGraphicsPipeline>(
			engineDevice, std::vector<S3DShader>{
			S3DShader{ ShaderType::Vertex, "resources/shaders/rendering/terrain.vert", "rendering", specializationVert },
			S3DShader{ ShaderType::Fragment, "resources/shaders/rendering/terrain_deferred.frag", "rendering" },
		}, pipelineConfig
		);

		pipelineConfig.renderPass = renderPassEarlyDepth->getRenderPass();
		terrainEarlyZPipeline = make_uPtr<S3DGraphicsPipeline>(
			engineDevice, std::vector<S3DShader>{
			S3DShader{ ShaderType::Vertex, "resources/shaders/rendering/terrain.vert", "rendering", specializationVert, { "EarlyZ" },  "terrain_earlyz.vert" }
		}, pipelineConfig
		);
	}

	void DeferredRenderSystem::createGBufferResolvePipeline(VkDescriptorSetLayout globalSetLayout) {
		std::vector<VkDescriptorSetLayout> descriptorSetLayouts{
			globalSetLayout,
			deferredResolveInputLayout->getDescriptorSetLayout()
		};
		deferredResolvePush = S3DPushConstant(sizeof(float), VK_SHADER_STAGE_FRAGMENT_BIT);
		deferredResolvePipelineLayout = new S3DPipelineLayout(engineDevice, { deferredResolvePush.getRange() }, descriptorSetLayouts);
		deferredResolveMSStencilWritePipelineLayout = new S3DPipelineLayout(engineDevice, {}, {lightingInputLayout->getDescriptorSetLayout()});

		S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo(rendererFeatures.enableVelocityBuffer ? 7 : 6);
		pipelineConfig.setCullMode(VK_CULL_MODE_BACK_BIT);

		if (useReverseDepth) {
			pipelineConfig.reverseDepth();
		}

		S3DShaderSpecialization specialization;
		specialization.addConstant(0, &rendererFeatures.msaaSamples, sizeof(uint32_t));

		pipelineConfig.renderPass = renderPassGBufferMSResolve->getRenderPass();
		pipelineConfig.pipelineLayout = deferredResolvePipelineLayout->getPipelineLayout();
		deferredResolvePipeline = make_uPtr<S3DGraphicsPipeline>(
			engineDevice, std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/fullscreen_quad.vert", "misc" },
				S3DShader{ ShaderType::Fragment, "resources/shaders/rendering/gbuffer_resolve.frag", "rendering", specialization },
			},
			pipelineConfig
		);



		S3DGraphicsPipelineConfigInfo pipelineConfigMSStencilWrite = S3DGraphicsPipelineConfigInfo(0);
		pipelineConfigMSStencilWrite.setCullMode(VK_CULL_MODE_BACK_BIT);

		if (useReverseDepth) {
			pipelineConfigMSStencilWrite.reverseDepth();
		}
		
		pipelineConfigMSStencilWrite.depthStencilInfo.stencilTestEnable = true;
		pipelineConfigMSStencilWrite.depthStencilInfo.front.compareOp = VK_COMPARE_OP_ALWAYS;
		pipelineConfigMSStencilWrite.depthStencilInfo.front.depthFailOp = VK_STENCIL_OP_REPLACE;
		pipelineConfigMSStencilWrite.depthStencilInfo.front.passOp = VK_STENCIL_OP_REPLACE;
		pipelineConfigMSStencilWrite.depthStencilInfo.front.failOp = VK_STENCIL_OP_REPLACE;
		pipelineConfigMSStencilWrite.depthStencilInfo.front.compareMask = 0xFF;
		pipelineConfigMSStencilWrite.depthStencilInfo.front.writeMask = 0xFF;
		pipelineConfigMSStencilWrite.depthStencilInfo.front.reference = 0xFF;

		pipelineConfigMSStencilWrite.renderPass = renderPassGBufferMSResolveStencilWrite->getRenderPass();
		pipelineConfigMSStencilWrite.pipelineLayout = deferredResolveMSStencilWritePipelineLayout->getPipelineLayout();
		deferredResolveMSStencilWritePipeline = make_uPtr<S3DGraphicsPipeline>(
			engineDevice, std::vector<S3DShader>{
			S3DShader{ ShaderType::Vertex, "resources/shaders/fullscreen_quad.vert", "misc" },
				S3DShader{ ShaderType::Fragment, "resources/shaders/rendering/gbuffer_resolve_stencil_write.frag", "rendering", specialization },
		},
			pipelineConfigMSStencilWrite
		);


	}

	void DeferredRenderSystem::createDeferredPipelines(VkDescriptorSetLayout globalSetLayout, VkDescriptorSetLayout sceneSetLayout) {

		std::vector<VkDescriptorSetLayout> descriptorSetLayoutsShading{ 
			globalSetLayout, sceneSetLayout,
			resourceSystem->getDescriptor()->getLayout(), lightingInputLayout->getDescriptorSetLayout() };
		std::vector<VkDescriptorSetLayout> descriptorSetLayoutsComposition{
			getAlphaCompositInputLayout()->getDescriptorSetLayout() 
		};
		if (rendererFeatures.enableDeferredLighting) {
			deferredLightingPush = S3DPushConstant(sizeof(DeferredLightingPush), VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT);
			deferredLightingPipelineLayout = new S3DPipelineLayout(engineDevice, { deferredLightingPush.getRange() }, descriptorSetLayoutsShading);
		}
		deferredShadingPipelineLayout = new S3DPipelineLayout(engineDevice, {}, descriptorSetLayoutsShading);
		deferredCompositionPipelineLayout = new S3DPipelineLayout(engineDevice, {}, descriptorSetLayoutsComposition);

		S3DGraphicsPipelineConfigInfo pipelineCompositionConfigInfo = S3DGraphicsPipelineConfigInfo(1);
		pipelineCompositionConfigInfo.setCullMode(VK_CULL_MODE_BACK_BIT);
		pipelineCompositionConfigInfo.enableAlphaBlending(0); // needed for alpha compositing
		pipelineCompositionConfigInfo.disableDepthTest();
		pipelineCompositionConfigInfo.pipelineLayout = deferredCompositionPipelineLayout->getPipelineLayout();
		pipelineCompositionConfigInfo.renderPass = renderPassComposition->getRenderPass();
		deferredCompositionPipeline = make_uPtr<S3DGraphicsPipeline>(
		   engineDevice, std::vector<S3DShader>{
			S3DShader{ ShaderType::Vertex, "resources/shaders/fullscreen_quad.vert", "misc" },
				S3DShader{ ShaderType::Fragment, "resources/shaders/rendering/composition_weighted_blending.frag", "rendering" },
		}, pipelineCompositionConfigInfo);


		std::string outputnameDeferredShading = rendererFeatures.enableDeferredLighting ? "composition_shading" : "composition_lighting";
		std::string outputnameDeferredLighting = "deferred_lighting";
		std::vector<std::string> defines{};
		if (!rendererFeatures.enableNormalDecals) {
			defines.push_back("NO_DECAL_NORMAL");
			outputnameDeferredShading += "_nodecnor";
			outputnameDeferredLighting += "_nodecnor";
		}
		if (!rendererFeatures.enableSSAO) {
			defines.push_back("NO_SSAO");
			outputnameDeferredShading += "_nossao";
		}
		if (rendererFeatures.enableMSAA) {
			defines.push_back("MULTI_SAMPLED");
			outputnameDeferredShading += "_msaa";
		}
		S3DShaderSpecialization deferredShadingSpecialization{};
		deferredShadingSpecialization.addConstant(0, rendererFeatures.msaaSamples);
	
		S3DGraphicsPipelineConfigInfo pipelineShadingConfigInfo = S3DGraphicsPipelineConfigInfo(1);
		pipelineShadingConfigInfo.setCullMode(VK_CULL_MODE_BACK_BIT);
		pipelineShadingConfigInfo.disableDepthTest();
		pipelineShadingConfigInfo.pipelineLayout = deferredShadingPipelineLayout->getPipelineLayout();
		pipelineShadingConfigInfo.renderPass = renderPassDeferredShading->getRenderPass();
		if (rendererFeatures.enableMSAA) {
			pipelineShadingConfigInfo.depthStencilInfo.stencilTestEnable = true;
			pipelineShadingConfigInfo.depthStencilInfo.front.compareOp = VK_COMPARE_OP_EQUAL;
			pipelineShadingConfigInfo.depthStencilInfo.front.depthFailOp = VK_STENCIL_OP_KEEP;
			pipelineShadingConfigInfo.depthStencilInfo.front.passOp = VK_STENCIL_OP_KEEP;
			pipelineShadingConfigInfo.depthStencilInfo.front.failOp = VK_STENCIL_OP_KEEP;
			pipelineShadingConfigInfo.depthStencilInfo.front.compareMask = 0xFF;
			pipelineShadingConfigInfo.depthStencilInfo.front.reference = 0x00;
		} 

		deferredShadingPipeline = make_uPtr<S3DGraphicsPipeline>(
		engineDevice, std::vector<S3DShader>{
			S3DShader{ ShaderType::Vertex, "resources/shaders/fullscreen_quad.vert", "misc" },
				S3DShader{ ShaderType::Fragment, rendererFeatures.enableDeferredLighting ? "resources/shaders/rendering/composition_shading.frag" : "resources/shaders/rendering/composition_lighting.frag", "rendering",
		deferredShadingSpecialization, defines, outputnameDeferredShading + (rendererFeatures.enableMSAA ? "_resolve.frag" : ".frag") },
		}, pipelineShadingConfigInfo);

		if (rendererFeatures.enableDeferredLighting) {
			S3DGraphicsPipelineConfigInfo pipelineDeferredLightingConfigInfo = S3DGraphicsPipelineConfigInfo(2);
			pipelineDeferredLightingConfigInfo.setCullMode(VK_CULL_MODE_BACK_BIT);
			pipelineDeferredLightingConfigInfo.enableAdditiveBlending(0);
			pipelineDeferredLightingConfigInfo.enableAdditiveBlending(1);
			pipelineDeferredLightingConfigInfo.disableDepthWrite();
			pipelineDeferredLightingConfigInfo.pipelineLayout = deferredLightingPipelineLayout->getPipelineLayout();
			pipelineDeferredLightingConfigInfo.renderPass = renderPassDeferredLighting->getRenderPass();

			// DIR
			S3DShaderSpecialization deferredLightingSpecialization;
			uint32_t lightType = 0;
			deferredLightingSpecialization = {};
			deferredLightingSpecialization.addConstant(0, lightType);
			
			deferredLightingDirectionalPipeline = make_uPtr<S3DGraphicsPipeline>(
			engineDevice, std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/rendering/dir_light_quad.vert", "rendering" },
					S3DShader{ ShaderType::Fragment, "resources/shaders/rendering/deferred_lighting.frag", "rendering",
			deferredLightingSpecialization },
			}, pipelineDeferredLightingConfigInfo);

			// omni lights use meshes to cull lighting
			pipelineDeferredLightingConfigInfo.setCullMode(VK_CULL_MODE_FRONT_BIT);
			pipelineDeferredLightingConfigInfo.enableVertexDescriptions(VertexDescriptionOptions_Position);
			// 2.5D culling against depth buffer
			if (useReverseDepth) {
				pipelineDeferredLightingConfigInfo.depthStencilInfo.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
			} else {
				pipelineDeferredLightingConfigInfo.depthStencilInfo.depthCompareOp = VK_COMPARE_OP_GREATER_OR_EQUAL;
			}
			// SPOT
			lightType = 1;
			deferredLightingSpecialization = {};
			deferredLightingSpecialization.addConstant(0, lightType);

			deferredLightingSpotPipeline = make_uPtr<S3DGraphicsPipeline>(
			engineDevice, std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/rendering/spot_light_cone.vert", "rendering" },
					S3DShader{ ShaderType::Fragment, "resources/shaders/rendering/deferred_lighting.frag", "rendering",
			deferredLightingSpecialization },
			}, pipelineDeferredLightingConfigInfo);

			// POINT
			lightType = 2;
			deferredLightingSpecialization = {};
			deferredLightingSpecialization.addConstant(0, lightType);

			deferredLightingPointPipeline = make_uPtr<S3DGraphicsPipeline>(
			engineDevice, std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/rendering/point_light_sphere.vert", "rendering" },
					S3DShader{ ShaderType::Fragment, "resources/shaders/rendering/deferred_lighting.frag", "rendering",
			deferredLightingSpecialization },
			}, pipelineDeferredLightingConfigInfo);
		}
		if (rendererFeatures.enableMSAA) {
			S3DGraphicsPipelineConfigInfo pipelineShadingMSConfigInfo = S3DGraphicsPipelineConfigInfo(1);
			pipelineShadingMSConfigInfo.setCullMode(VK_CULL_MODE_BACK_BIT);
			pipelineShadingMSConfigInfo.enableAlphaBlending(0); // blend with background if required, e.g. skybox
			pipelineShadingMSConfigInfo.disableDepthTest();
			pipelineShadingMSConfigInfo.pipelineLayout = deferredShadingPipelineLayout->getPipelineLayout();
			pipelineShadingMSConfigInfo.renderPass = renderPassDeferredShadingMS->getRenderPass();
			pipelineShadingMSConfigInfo.depthStencilInfo.stencilTestEnable = true;
			pipelineShadingMSConfigInfo.depthStencilInfo.front.compareOp = VK_COMPARE_OP_EQUAL;
			pipelineShadingMSConfigInfo.depthStencilInfo.front.depthFailOp = VK_STENCIL_OP_KEEP;
			pipelineShadingMSConfigInfo.depthStencilInfo.front.passOp = VK_STENCIL_OP_KEEP;
			pipelineShadingMSConfigInfo.depthStencilInfo.front.failOp = VK_STENCIL_OP_KEEP;
			pipelineShadingMSConfigInfo.depthStencilInfo.front.compareMask = 0xFF;
			pipelineShadingMSConfigInfo.depthStencilInfo.front.reference = 0xFF;

			deferredShadingPipelineMS = make_uPtr<S3DGraphicsPipeline>(
		    engineDevice, std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/fullscreen_quad.vert", "misc" },
					S3DShader{ ShaderType::Fragment, "resources/shaders/rendering/composition_lighting.frag", "rendering",
			deferredShadingSpecialization, defines, outputnameDeferredShading + ".frag" },
			}, pipelineShadingMSConfigInfo);
		}

	}

	void DeferredRenderSystem::resize(glm::ivec3 newSize) {
		if (rendererFeatures.enableEarlyZ) {
			framebufferEarlyDepth->resize(newSize, renderPassEarlyDepth);
		}
		framebufferGBuffer->resize(newSize, renderPassGBuffer);
		if (rendererFeatures.enableMSAA) {
			framebufferGBufferMSResolve->resize(newSize, renderPassGBufferMSResolve);
			framebufferGBufferMSResolveStencilWrite->resize(newSize, renderPassGBufferMSResolveStencilWrite);
		}
		if (rendererFeatures.enableSSAO) {
			glm::ivec3 ssaoNewSize = glm::ivec3(glm::vec2(newSize) * ProjectSystem::getGraphicsSettings().postfx.ssao.quality, 1);
			glm::ivec3 ssaoInterleaveSize = glm::ivec3(glm::ceil(glm::vec2(ssaoNewSize) / 2.0f), 1);
			framebufferSSAOInterleave[0][0]->resize(ssaoInterleaveSize, renderPassSSAO);
			framebufferSSAOInterleave[0][1]->resize(ssaoInterleaveSize, renderPassSSAO);
			framebufferSSAOInterleave[1][0]->resize(ssaoInterleaveSize, renderPassSSAO);
			framebufferSSAOInterleave[1][1]->resize(ssaoInterleaveSize, renderPassSSAO);
			ssaoInterleaveCombine->resize(ssaoNewSize, 
				{ ssaoAttachmentInterleave[0][0]->getDescriptor(), ssaoAttachmentInterleave[0][1]->getDescriptor(),
				ssaoAttachmentInterleave[1][0]->getDescriptor(),ssaoAttachmentInterleave[1][1]->getDescriptor(), });
			ssaoBlurH0->resize(ssaoNewSize, { ssaoInterleaveCombine->getAttachment()->getDescriptor(), depthRenderTarget->getDescriptor() });
			ssaoBlurV0->resize(ssaoNewSize, { ssaoBlurH0->getAttachment()->getDescriptor(), depthRenderTarget->getDescriptor() });
			ssaoProcess->resize(newSize, { ssaoBlurV0->getAttachment()->getDescriptor(), depthRenderTarget->getDescriptor() });
		}
		framebufferWeightedBlending->resize(newSize, renderPassWeightedBlending);
		framebufferDeferredShading->resize(newSize, renderPassDeferredShading);
		framebufferComposition->resize(newSize, renderPassComposition);
		if (rendererFeatures.enableDeferredLighting) {
			framebufferDeferredLighting->resize(newSize, renderPassDeferredLighting);
		}
	}
	
	void DeferredRenderSystem::renderEarlyDepthMesh3D(FrameInfo& frameInfo, MeshMobilityOptionsFlags mobility) {
		if (rendererFeatures.enableEarlyZ) {
			// opaque/masked
			SurfaceMaterialPipelineClassPermutationFlags lastBoundClass = -1;
			for (auto& iter : frameInfo.renderObjects.renderList->getIterator(materialSystem->getDeferrableClasses(), mobility)) {
				if (iter.renderList.size() == 0) continue;

				bool useDynamic = iter.meshMoblity == MeshMobilityOption_Dynamic;
				SurfaceMaterialPipelineClassPermutationFlags currentClass = iter.materialClass;
				currentClass |= useDynamic ? SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates : 0;
				currentClass |= iter.rigged ? SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms : 0;
				if (currentClass != lastBoundClass) {
					materialSystem->bindMaterialClassEarlyDepth(currentClass, frameInfo.commandBuffer);
					lastBoundClass = currentClass;

					VkDescriptorSet totalSets[2] {
						frameInfo.globalDescriptorSet,
						frameInfo.resourceSystem->getDescriptor()->getDescriptor()
					};
					vkCmdBindDescriptorSets(
						frameInfo.commandBuffer,
						VK_PIPELINE_BIND_POINT_GRAPHICS,
						materialSystem->getMaterialClass(currentClass)->getEarlyZPipelineLayout(),
						0,
						2,
						totalSets,
						0,
						nullptr
					);
				}
				for (auto& [id, actor] : iter.renderList) {
					SHARD3D_ASSERT(actor.mesh && "Cannot render a non existant mesh!");
					if (!frameInfo.camera.isInFrustum(actor.mesh->cullInfo.sphereBounds, actor.mesh->cullInfo.worldPosition)) {
						continue;
					}
					Mesh3D* model = frameInfo.resourceSystem->retrieveMesh(actor.mesh->mesh);
					SHARD3D_SILENT_ASSERT(model->materialSlots.size() == actor.mesh->materials.size() && "Mesh Component and 3D model do not match!");
					SHARD3D_ASSERT(model->materialSlots.size() <= actor.mesh->materials.size() && "Mesh Component has less slots than 3D model!");
					if (iter.rigged) {
						VkDescriptorSet descriptor = frameInfo.renderObjects.renderList->getSkeletonDescriptor(id, frameInfo.frameIndex);
						vkCmdBindDescriptorSets(
							frameInfo.commandBuffer,
							VK_PIPELINE_BIND_POINT_GRAPHICS,
							materialSystem->getMaterialClass(currentClass)->getEarlyZPipelineLayout(),
							2,
							1,
							&descriptor,
							0,
							nullptr
						);
					}

					for (uint32_t index : actor.material_indices) {
						if (!frameInfo.camera.isInFrustum(actor.mesh->cullInfo.transformedMaterialAABBs[index])) {
							continue;
						}
						if (useDynamic) {
							DynamicPushConstantData push{};
							push.actorBuffer = actor.mesh->actorBufferIndices[frameInfo.frameIndex];
							push.materialID = frameInfo.resourceSystem->retrieveMaterial(actor.mesh->materials[index])->getShaderMaterialID();
							assert(resourceSystem->getDescriptor()->isAllocated(BINDLESS_SSBO_BINDING, push.actorBuffer));
							vkCmdPushConstants(
								frameInfo.commandBuffer,
								materialSystem->getMaterialClass(currentClass)->getEarlyZPipelineLayout(),
								VK_SHADER_STAGE_VERTEX_BIT,
								0,
								sizeof(DynamicPushConstantData),
								&push
							);
						} else {
							StaticEarlyZPushConstantData push{};
							push.modelMatrix = actor.mesh->transform;
							push.materialID = frameInfo.resourceSystem->retrieveMaterial(actor.mesh->materials[index])->getShaderMaterialID();
							vkCmdPushConstants(
								frameInfo.commandBuffer,
								materialSystem->getMaterialClass(currentClass)->getEarlyZPipelineLayout(),
								VK_SHADER_STAGE_VERTEX_BIT,
								0,
								sizeof(StaticEarlyZPushConstantData),
								&push
							);
						}
						model->draw(frameInfo.commandBuffer, index);
					}
				}
			}
		}
	}
	void DeferredRenderSystem::renderEarlyDepthTerrain(FrameInfo& frameInfo) {
		if (rendererFeatures.enableEarlyZ) {
			if (frameInfo.renderObjects.terrainList->getTerrain()) {
				terrainEarlyZPipeline->bind(frameInfo.commandBuffer);
				VkDescriptorSet sets[3]{
					frameInfo.globalDescriptorSet,
					frameInfo.resourceSystem->getDescriptor()->getDescriptor(),
					frameInfo.renderObjects.terrainList->getTerrain()->terrainDescriptor
				};
				vkCmdBindDescriptorSets(
					frameInfo.commandBuffer,
					VK_PIPELINE_BIND_POINT_GRAPHICS,
					terrainPipelineLayout->getPipelineLayout(),
					0,
					3,
					sets,
					0,
					nullptr
				);
				TerrainPush push;
				push.heightMul = 1.0f;
				push.tileExtent = frameInfo.renderObjects.terrainList->getTerrain()->tileExtent;
				push.tiles = { frameInfo.renderObjects.terrainList->getTerrain()->numTilesX, frameInfo.renderObjects.terrainList->getTerrain()->numTilesY };
				push.translation = glm::vec4(0.f);

				for (int i = 0; i < frameInfo.renderObjects.terrainList->getTerrain()->getLODCount(); i++) {
					push.lod = i;
					terrainPush.push(frameInfo.commandBuffer, terrainPipelineLayout->getPipelineLayout(), push);
					frameInfo.renderObjects.terrainList->getTerrain()->render(frameInfo, i);
				}
			}
		}
	}
	void DeferredRenderSystem::renderGBufferMesh3D(FrameInfo& frameInfo, MeshMobilityOptionsFlags mobility) {
		// opaque/masked
		SurfaceMaterialPipelineClassPermutationFlags lastBoundClass = -1;
		MeshMobilityOptionsFlags lastMobility = 0;
		for (auto& iter : frameInfo.renderObjects.renderList->getIterator(materialSystem->getDeferrableClasses(), mobility)) {
			if (iter.renderList.size() == 0) continue;
			bool dynTrans = iter.meshMoblity == MeshMobilityOption_Dynamic;
			SurfaceMaterialPipelineClassPermutationFlags currentClass = iter.materialClass;
			currentClass |= dynTrans ? SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates : 0;
			currentClass |= iter.rigged ? SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms : 0;
			if (currentClass != lastBoundClass) {
				materialSystem->bindMaterialClassDeferred(currentClass, frameInfo.commandBuffer);
				lastBoundClass = currentClass;
				bindClass(frameInfo, currentClass);
			}
			renderClass(frameInfo, &iter, currentClass);
		}
	}
	void DeferredRenderSystem::renderGBufferTerrain(FrameInfo& frameInfo) {
		if (frameInfo.renderObjects.terrainList->getTerrain()) {
			terrainGBufferPipeline->bind(frameInfo.commandBuffer);
			VkDescriptorSet sets[3]{
				frameInfo.globalDescriptorSet,
				frameInfo.resourceSystem->getDescriptor()->getDescriptor(),
				frameInfo.renderObjects.terrainList->getTerrain()->terrainDescriptor
			};
			vkCmdBindDescriptorSets(
				frameInfo.commandBuffer,
				VK_PIPELINE_BIND_POINT_GRAPHICS,
				terrainPipelineLayout->getPipelineLayout(),
				0,
				3,
				sets,
				0,
				nullptr
			);

			TerrainPush push;
			push.heightMul = 1.0f;
			push.tileExtent = frameInfo.renderObjects.terrainList->getTerrain()->tileExtent;
			push.tiles = { frameInfo.renderObjects.terrainList->getTerrain()->numTilesX, frameInfo.renderObjects.terrainList->getTerrain()->numTilesY };
			push.translation = glm::vec4(0.f);

			for (int i = 0; i < frameInfo.renderObjects.terrainList->getTerrain()->getLODCount(); i++) {
				push.lod = i;
				terrainPush.push(frameInfo.commandBuffer, terrainPipelineLayout->getPipelineLayout(), push);
				frameInfo.renderObjects.terrainList->getTerrain()->render(frameInfo, i);
			}
		}
	}
	void DeferredRenderSystem::renderForwardMesh3D(FrameInfo& frameInfo, MeshMobilityOptionsFlags mobility) {
		SurfaceMaterialPipelineClassPermutationFlags lastBoundClass = -1;
		for (auto& iter : frameInfo.renderObjects.renderList->getIterator(materialSystem->getForwardOnlyClasses(), mobility)) {
			if (iter.renderList.size() == 0) continue;
			bool dynTrans = iter.meshMoblity == MeshMobilityOption_Dynamic;
			SurfaceMaterialPipelineClassPermutationFlags currentClass = iter.materialClass;
			currentClass |= dynTrans ? SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates : 0;
			currentClass |= iter.rigged ? SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms : 0;
			if (currentClass != lastBoundClass) {
				materialSystem->bindMaterialClassForward(currentClass, frameInfo.commandBuffer);
				lastBoundClass = currentClass;
				bindClass(frameInfo, currentClass);
			}
			renderClass(frameInfo, &iter, currentClass);
		}

		// on AMD gpus, the dynamic state persists throughout the entirety of the command buffer for whatever reason 
		// (even on pipelines without shading rate dynamic state), so we need to reset it, absolutely retarded
		if (engineDevice.supportedExtensions.khr_fragment_shading_rate) {
			VkExtent2D fragmentSize = { 1, 1 };
			VkFragmentShadingRateCombinerOpKHR combineOps[2]{ VK_FRAGMENT_SHADING_RATE_COMBINER_OP_KEEP_KHR , VK_FRAGMENT_SHADING_RATE_COMBINER_OP_KEEP_KHR };
			vkCmdSetFragmentShadingRateKHR(frameInfo.commandBuffer, &fragmentSize, combineOps);
		}
	}

	void DeferredRenderSystem::renderSkybox(FrameInfo& frameInfo) {
		skyboxPipeline->bind(frameInfo.commandBuffer);
		VkDescriptorSet sets[3]{
			frameInfo.globalDescriptorSet,
			frameInfo.sceneDescriptorSet,
			frameInfo.resourceSystem->getDescriptor()->getDescriptor()
		};

		vkCmdBindDescriptorSets(frameInfo.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
			skyboxPipelineLayout->getPipelineLayout(),
			0,
			3,
			sets,
			0,
			nullptr
		);
		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::DRAW, "DFRS_SKYBOX", frameInfo.commandBuffer);
		frameInfo.resourceSystem->retrieveMesh(frameInfo.resourceSystem->coreAssets.m_defaultModel)->draw(frameInfo.commandBuffer, 0);
	}
	void DeferredRenderSystem::beginEarlyZPass(FrameInfo& frameInfo) {
		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::BEGIN_RENDER_PASS, "DFRS_EARLYZ", frameInfo.commandBuffer);
		renderPassEarlyDepth->beginRenderPass(frameInfo.commandBuffer, framebufferEarlyDepth);
	}
	void DeferredRenderSystem::endEarlyZPass(FrameInfo& frameInfo) {
		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::END_RENDER_PASS, "DFRS_EARLYZ", frameInfo.commandBuffer);
		renderPassEarlyDepth->endRenderPass(frameInfo.commandBuffer);
	}
	void DeferredRenderSystem::beginGBufferPass(FrameInfo& frameInfo) {
		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::BEGIN_RENDER_PASS, "DFRS_GBUFFER", frameInfo.commandBuffer);
		renderPassGBuffer->beginRenderPass(frameInfo.commandBuffer, framebufferGBuffer);
	}

	void DeferredRenderSystem::endGBufferPass(FrameInfo& frameInfo) {
		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::END_RENDER_PASS, "DFRS_GBUFFER", frameInfo.commandBuffer);
		renderPassGBuffer->endRenderPass(frameInfo.commandBuffer);
		if (rendererFeatures.enableMSAA) {
			resolveGBuffer(frameInfo);
		}
	}

	void DeferredRenderSystem::renderDeferredLighting(FrameInfo& frameInfo, SceneBuffers& ssbo) {
		if (!rendererFeatures.enableDeferredLighting) return;
		SHARD3D_ASSERT(deferredLightingPipelineLayout && "Cannot bind material if shader pipeline was never created!");

		VkDescriptorSet sets[4]{
			frameInfo.globalDescriptorSet,
			frameInfo.sceneDescriptorSet,
			resourceSystem->getDescriptor()->getDescriptor(),
			getGBufferAttachmentInputDescriptorSet()
		};


		renderPassDeferredLighting->beginRenderPass(frameInfo.commandBuffer, framebufferDeferredLighting);

		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			deferredLightingPipelineLayout->getPipelineLayout(),
			0,
			4,
			sets,
			0,
			nullptr
		);

		DeferredLightingPush push;
		push.attenuationEpsilon = 0.001f; // TODO: extrapolate epsilon from post processing steps
		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::DRAW, "DFRS_DEFERRED_LIGHTING_DIRECTIONAL_LIGHTS", frameInfo.commandBuffer);
		if (ssbo.sceneEnvironment.numDirectionalLights) {
			deferredLightingDirectionalPipeline->bind(frameInfo.commandBuffer);
			for (int i = 0; i < ssbo.sceneEnvironment.numDirectionalLights; i++) {
				push.lightIndex = i;
				deferredLightingPush.push(frameInfo.commandBuffer, deferredLightingPipelineLayout->getPipelineLayout(), push);
				vkCmdDraw(frameInfo.commandBuffer, 6, 1, 0, 0);
			}
		}
		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::DRAW, "DFRS_DEFERRED_LIGHTING_SPOT_LIGHTS", frameInfo.commandBuffer);
		if (ssbo.sceneOmniLights.numSpotLights) {
			deferredLightingSpotPipeline->bind(frameInfo.commandBuffer);
			for (int i = 0; i < ssbo.sceneOmniLights.numSpotLights; i++) {
				push.lightIndex = i;
				deferredLightingPush.push(frameInfo.commandBuffer, deferredLightingPipelineLayout->getPipelineLayout(), push);
				resourceSystem->retrieveMesh(AssetID("engine/meshes/spotlightcone.fbx.s3dasset"))->drawAll(frameInfo.commandBuffer);
			}
		}
		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::DRAW, "DFRS_DEFERRED_LIGHTING_POINT_LIGHTS", frameInfo.commandBuffer);
		if (ssbo.sceneOmniLights.numPointLights) {
			deferredLightingPointPipeline->bind(frameInfo.commandBuffer);
			for (int i = 0; i < ssbo.sceneOmniLights.numPointLights; i++) {
				push.lightIndex = i;
				deferredLightingPush.push(frameInfo.commandBuffer, deferredLightingPipelineLayout->getPipelineLayout(), push);
				resourceSystem->retrieveMesh(AssetID("engine/meshes/pointlightsphere.fbx.s3dasset"))->drawAll(frameInfo.commandBuffer);
			}
		}
		renderPassDeferredLighting->endRenderPass(frameInfo.commandBuffer);
	}

	void DeferredRenderSystem::renderSSAO(FrameInfo& frameInfo) {
		if (rendererFeatures.enableSSAO) {
			ssaoPipeline->bind(frameInfo.commandBuffer);
			VkDescriptorSet sets[2]{
				frameInfo.globalDescriptorSet,
				ssaoInputDescriptorSet
			};
			vkCmdBindDescriptorSets(frameInfo.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
				ssaoPipelineLayout->getPipelineLayout(),
				0,
				2,
				sets,
				0,
				nullptr
			);
			SSAOPushStruct ssao{};  // control tile size? 256x256 seems to work best overall
			ssao.screenSize = ssaoAttachmentInterleave[0][0]->getDimensions();
			ssao.tiles = glm::ceil(glm::vec2(ssaoAttachmentInterleave[0][0]->getDimensions()) / glm::vec2(256.0f));
			ssao.bias = frameInfo.camera.postfx.ssao.bias;
			ssao.radius = frameInfo.camera.postfx.ssao.radius;
			ssao.intensity = frameInfo.camera.postfx.ssao.intensity;
			ssao.kernelQuality = ProjectSystem::getGraphicsSettings().postfx.ssao.kernelQuality;
			ssao.power = frameInfo.camera.postfx.ssao.power;
			ssao.nearZ = frameInfo.camera.getNear();
			ssao.farZ = frameInfo.camera.getFar();

			MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::GENERIC, "RENDER_SSAO", frameInfo.commandBuffer);
			for (int px = 0; px < 2; px++) {
				ssao.interleaveX = px;
				for (int py = 0; py < 2; py++) {
					ssao.interleaveY = py;
					renderPassSSAO->beginRenderPass(frameInfo.commandBuffer, framebufferSSAOInterleave[px][py]);
					for (int i = 0; i < ssao.tiles.x * ssao.tiles.y; i++) {
						ssao.tileIndex = i;
						vkCmdPushConstants(frameInfo.commandBuffer,
							ssaoPipelineLayout->getPipelineLayout(),
							VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(SSAOPushStruct), &ssao);
						vkCmdDraw(frameInfo.commandBuffer, 6, 1, 0, 0);
					}
					renderPassSSAO->endRenderPass(frameInfo.commandBuffer);
				}
			}
			ssaoInterleaveCombine->render(frameInfo.commandBuffer);
			struct SSAOBlurPush {
				glm::vec2 size;
				glm::vec2 dir;
				int type; // bilateral blur method: 0 = color, 1 = depth (more accurate, slower)
				float n;
				float f;
			};
			SSAOBlurPush pushH0{ glm::vec2(ssaoBlurH0->getAttachment()->getDimensions()), {1.0, 0.0}, 1, frameInfo.camera.getNear(), frameInfo.camera.getFar() };
			SSAOBlurPush pushV0{ glm::vec2(ssaoBlurV0->getAttachment()->getDimensions()), {0.0, 1.0}, 1, frameInfo.camera.getNear(), frameInfo.camera.getFar() };


			MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::GENERIC, "RENDER_SSAO_BILATERAL_BLUR_HORIZONAL", frameInfo.commandBuffer);
			ssaoBlurH0->render(frameInfo.commandBuffer, pushH0);

			MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::GENERIC, "RENDER_SSAO_BILATERAL_BLUR_VERTICAL", frameInfo.commandBuffer);
			ssaoBlurV0->render(frameInfo.commandBuffer, pushV0);

			MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::GENERIC, "RENDER_SSAO_PROCESS_IMAGE", frameInfo.commandBuffer);
			ssaoProcess->render(frameInfo.commandBuffer, ssao);
		}
	}

	void DeferredRenderSystem::beginBlendingPass(FrameInfo& frameInfo) {
		renderPassWeightedBlending->beginRenderPass(frameInfo.commandBuffer, framebufferWeightedBlending);
	}

	void DeferredRenderSystem::endBlendingPass(FrameInfo& frameInfo) {
		renderPassWeightedBlending->endRenderPass(frameInfo.commandBuffer);
	}

	void DeferredRenderSystem::composeLighting(FrameInfo& frameInfo) {
		renderPassDeferredShading->beginRenderPass(frameInfo.commandBuffer, framebufferDeferredShading);
		// lighting here
		bindDeferredShading(frameInfo.commandBuffer, frameInfo.globalDescriptorSet, frameInfo.sceneDescriptorSet, false);
		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::DRAW, "DFRS_DEFERRED_SHADING", frameInfo.commandBuffer);
		vkCmdDraw(frameInfo.commandBuffer, 6, 1, 0, 0);
		renderPassDeferredShading->endRenderPass(frameInfo.commandBuffer);
	}
	void DeferredRenderSystem::composeLightingMS(FrameInfo& frameInfo) {
		if (rendererFeatures.enableMSAA) {
			renderPassDeferredShadingMS->beginRenderPass(frameInfo.commandBuffer, framebufferDeferredShading);
			// lighting here
			bindDeferredShading(frameInfo.commandBuffer, frameInfo.globalDescriptorSet, frameInfo.sceneDescriptorSet, true);
			MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::DRAW, "DFRS_DEFERRED_SHADING_MS", frameInfo.commandBuffer);
			vkCmdDraw(frameInfo.commandBuffer, 6, 1, 0, 0);
			renderPassDeferredShadingMS->endRenderPass(frameInfo.commandBuffer);
		}
	}
	void DeferredRenderSystem::beginCompositionPass(FrameInfo& frameInfo) {
		renderPassComposition->beginRenderPass(frameInfo.commandBuffer, framebufferComposition);
	}

	void DeferredRenderSystem::composeAlpha(FrameInfo& frameInfo) {
		// add transparency
		bindDeferredComposition(frameInfo.commandBuffer);
		vkCmdDraw(frameInfo.commandBuffer, 6, 1, 0, 0);
	}

	void DeferredRenderSystem::endComposition(FrameInfo& frameInfo) {
		renderPassComposition->endRenderPass(frameInfo.commandBuffer);
	}

	void DeferredRenderSystem::renderClass(FrameInfo& frameInfo, SurfaceMaterialMeshIteratorObjectEntity* iter, SurfaceMaterialPipelineClassPermutationFlags flags) {
		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::DRAW, std::string("DFRS_RENDER_CLASS::class=" + std::to_string(iter->materialClass)+",mobilityList="+ std::to_string(iter->materialClass)).c_str(), frameInfo.commandBuffer);
		uint32_t draws{};
		bool translucent = flags & SurfaceMaterialPipelineClassPermutationOptions_Translucent;
		bool useDynamicBuffer = iter->meshMoblity == MeshMobilityOption_Dynamic;
		for (auto& [id, actor] : iter->renderList) {
			SHARD3D_ASSERT(actor.mesh && "Cannot render a non existant mesh!");
			if (!frameInfo.camera.isInFrustum(actor.mesh->cullInfo.sphereBounds, actor.mesh->cullInfo.worldPosition)) {
				continue;
			}
			Mesh3D* model = frameInfo.resourceSystem->retrieveMesh(actor.mesh->mesh);
			SHARD3D_SILENT_ASSERT(model->materialSlots.size() == actor.mesh->materials.size() && "Mesh Component and 3D model do not match!");
			SHARD3D_ASSERT(model->materialSlots.size() <= actor.mesh->materials.size() && "Mesh Component has less slots than 3D model!");
			if (iter->rigged) {
				VkDescriptorSet descriptor = frameInfo.renderObjects.renderList->getSkeletonDescriptor(id, frameInfo.frameIndex);
				vkCmdBindDescriptorSets(
					frameInfo.commandBuffer,
					VK_PIPELINE_BIND_POINT_GRAPHICS,
					materialSystem->getMaterialClass(flags)->getPipelineLayout(),
					translucent ? 4 : 3,
					1,
					&descriptor,
					0,
					nullptr
				);
			}

			for (uint32_t index : actor.material_indices) {
				if (!frameInfo.camera.isInFrustum(actor.mesh->cullInfo.transformedMaterialAABBs[index])) {
					continue;
				}
				Material* material = frameInfo.resourceSystem->retrieveMaterial(actor.mesh->materials[index]);
				uint32_t materialID = material->getShaderMaterialID();
				assert(resourceSystem->getDescriptor()->isAllocated(BINDLESS_SSBO_BINDING, materialID));
				if (useDynamicBuffer) {
					DynamicGBufferPushConstantData push{};
					push.actorBuffer = actor.mesh->actorBufferIndices[frameInfo.frameIndex];
					push.receiveDecals = actor.mesh->receiveDecals;
					push.materialID = frameInfo.resourceSystem->retrieveMaterial(actor.mesh->materials[index])->getShaderMaterialID();
					assert(resourceSystem->getDescriptor()->isAllocated(BINDLESS_SSBO_BINDING, push.actorBuffer));
					vkCmdPushConstants(
						frameInfo.commandBuffer,
						materialSystem->getMaterialClass(flags)->getPipelineLayout(),
						VK_SHADER_STAGE_VERTEX_BIT,
						0,
						translucent ? sizeof(DynamicPushConstantData) : sizeof(DynamicGBufferPushConstantData),
						&push
					);
				} else {
					StaticMaterialPushConstantData push{};
					push.modelMatrix = actor.mesh->transform;
					push.normalMatrix = actor.mesh->normal;
					push.materialID = materialID;
					push.receiveDecals = actor.mesh->receiveDecals;
					vkCmdPushConstants(
						frameInfo.commandBuffer,
						materialSystem->getMaterialClass(flags)->getPipelineLayout(),
						VK_SHADER_STAGE_VERTEX_BIT,
						0,
						translucent ? sizeof(StaticMaterialPushConstantData) - 4 : sizeof(StaticMaterialPushConstantData),
						&push
					);
				}
				model->draw(frameInfo.commandBuffer, index);
				draws++;
			}
		}
		SHARD3D_VERBOSE_F("Class {0} executed {1} drawcalls", flags, draws);
	}
	void DeferredRenderSystem::bindClass(FrameInfo& frameInfo, SurfaceMaterialPipelineClassPermutationFlags flags) {
		if (flags & SurfaceMaterialPipelineClassPermutationOptions_Translucent) {
			VkDescriptorSet totalSets[4]{
				frameInfo.globalDescriptorSet,
				frameInfo.sceneDescriptorSet,
				frameInfo.resourceSystem->getDescriptor()->getDescriptor(),
				deferredLitDescriptorSet
			};
			vkCmdBindDescriptorSets(
				frameInfo.commandBuffer,
				VK_PIPELINE_BIND_POINT_GRAPHICS,
				materialSystem->getMaterialClass(flags)->getPipelineLayout(),
				0,
				4,
				totalSets,
				0,
				nullptr
			);
		} else {
			VkDescriptorSet totalSets[3]{
				frameInfo.globalDescriptorSet,
				frameInfo.sceneDescriptorSet,
				frameInfo.resourceSystem->getDescriptor()->getDescriptor()
			};
			vkCmdBindDescriptorSets(
				frameInfo.commandBuffer,
				VK_PIPELINE_BIND_POINT_GRAPHICS,
				materialSystem->getMaterialClass(flags)->getPipelineLayout(),
				0,
				3,
				totalSets,
				0,
				nullptr
			);
		}
		
	}
	void DeferredRenderSystem::resolveGBuffer(FrameInfo& frameInfo) {
		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::GENERIC, "DFRS_RESOLVE_MSAA", frameInfo.commandBuffer);
		
		renderPassGBufferMSResolve->beginRenderPass(frameInfo.commandBuffer, framebufferGBufferMSResolve);
		deferredResolvePipeline->bind(frameInfo.commandBuffer);
		VkDescriptorSet totalSets[2]{
			frameInfo.globalDescriptorSet,
			deferredResolveInputDescriptorSet
		};
		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			deferredResolvePipelineLayout->getPipelineLayout(),
			0,
			2,
			totalSets,
			0,
			nullptr
		);
		// make configurable (higher = worse AA, potentially cheaper; lower = better AA, potentially more expensive)
		const float depthRangeCheck = 0.0075f; // range in meters to mark as a complex edge
		deferredResolvePush.push(frameInfo.commandBuffer, deferredResolvePipelineLayout->getPipelineLayout(), depthRangeCheck);

		vkCmdDraw(frameInfo.commandBuffer, 6, 1, 0, 0);
		renderPassGBufferMSResolve->endRenderPass(frameInfo.commandBuffer);

		// write stencil information
		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::GENERIC, "DFRS_RESOLVE_MSAA_STENCIL_WRITE", frameInfo.commandBuffer);

		renderPassGBufferMSResolveStencilWrite->beginRenderPass(frameInfo.commandBuffer, framebufferGBufferMSResolveStencilWrite);
		deferredResolveMSStencilWritePipeline->bind(frameInfo.commandBuffer);

		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			deferredResolveMSStencilWritePipelineLayout->getPipelineLayout(),
			0,
			1,
			&lightingInputDescriptorSet,
			0,
			nullptr
		);

		vkCmdDraw(frameInfo.commandBuffer, 6, 1, 0, 0);
		renderPassGBufferMSResolveStencilWrite->endRenderPass(frameInfo.commandBuffer);
	}


	void DeferredRenderSystem::bindDeferredShading(VkCommandBuffer commandBuffer, VkDescriptorSet globalSet, VkDescriptorSet sceneSSBO, bool msPass) {
		SHARD3D_ASSERT(deferredShadingPipelineLayout && "Cannot bind material if shader pipeline was never created!");

		VkDescriptorSet sets[4]{
			globalSet,
			sceneSSBO,
			resourceSystem->getDescriptor()->getDescriptor(),
			getGBufferAttachmentInputDescriptorSet()
		};

		vkCmdBindDescriptorSets(
			commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			deferredShadingPipelineLayout->getPipelineLayout(),
			0,
			4,
			sets,
			0,
			nullptr
		);

		if (msPass) {
			deferredShadingPipelineMS->bind(commandBuffer);
		} else {
			deferredShadingPipeline->bind(commandBuffer);
		}
	}
	void DeferredRenderSystem::bindDeferredComposition(VkCommandBuffer commandBuffer) {
		SHARD3D_ASSERT(deferredCompositionPipelineLayout && "Cannot bind material if shader pipeline was never created!");

		VkDescriptorSet sets[1]{
			getAlphaCompositeInputDescriptorSet()
		};

		vkCmdBindDescriptorSets(
			commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			deferredCompositionPipelineLayout->getPipelineLayout(),
			0,
			1,
			sets,
			0,
			nullptr
		);

		deferredCompositionPipeline->bind(commandBuffer);
	}
}