#pragma once

#include "../../s3dstd.h"
#include "../../vulkan_abstr.h"
#include "../../core/misc/frame_info.h"	  
#include "../../core/ecs/level.h"
#include "../../core/ecs/components.h"
#include "../handlers/render_list.h"
#include "gbuffer_struct.h"
#include "../post_fx/post_processing_fx.h"
#include "../../core/ecs/actor.h" 

namespace Shard3D {

	inline namespace Systems {

	
		class SkyAtmosphereSystem {
	  	public:
			SkyAtmosphereSystem(S3DDevice& device, ResourceSystem* resourceSystem, VkDescriptorSetLayout globalSetLayout, VkRenderPass renderPass, bool renderSun = true);
			~SkyAtmosphereSystem();

			DELETE_COPY(SkyAtmosphereSystem)

			void render(FrameInfo& frameInfo);
			void renderSkyLightCubemap(FrameInfo& frameInfo);
			void runGarbageCollector(uint32_t frameIndex, VkFence fence);
		private:
			struct SkyAtmosphereCubemap {
				S3DRenderTarget* atmosphericScatteringCubeRenderTarget;
				S3DFramebuffer* atmosphericScatteringCubeFramebuffers[6];
				S3DRenderPass* atmosphericScatteringCubeRenderPass;
				PostProcessingEffect* envIrradianceGen;

				uPtr<S3DGraphicsPipeline> atmosphericScatteringPipelineCubemap;

				ShaderResourceIndex reflection_id;
				ShaderResourceIndex irradiance_id;
			};
			struct SkyAtmosphereComponentInfo_ {
				Actor actor;
				uint32_t resolution;
				uint32_t updateRate;
				uint32_t* reflectionTexture;
				uint32_t* irradianceTexture;
			};
			bool skyAtmosphereDrawCall(FrameInfo&, Components::SkyAtmosphereComponent& atc, glm::mat4 projectionViewNoTranslate);

			void createPipeline(VkDescriptorSetLayout globalSetLayout, VkRenderPass renderPass, bool renderSun);

			uPtr<S3DGraphicsPipeline> atmosphericScatteringPipeline;
			uPtr<S3DPipelineLayout> atmosphericScatteringPipelineLayout;
			S3DPushConstant atmosphericScatteringPush;

			Resources::Mesh3D* sphereDome;

			std::unordered_set<entt::entity> removedReflections;
			std::unordered_map<entt::entity, SkyAtmosphereCubemap*> skyAtmosphereReflections;
			std::vector<std::pair<SkyAtmosphereComponentInfo_, bool>> rebuildReflections{};
			std::vector<std::pair<SkyAtmosphereCubemap*, S3DDestructionObject>> destroyReflectionQueue{};

			uint32_t currentFrameIndex;

			ResourceSystem* resourceSystem;
			S3DDevice& engineDevice;
		};
	}
}