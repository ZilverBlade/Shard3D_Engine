#pragma once
#include "../../s3dstd.h"
#include "../../vulkan_abstr.h"
#include "../../core/misc/frame_info.h"	  
#include "../../core/vulkan_api/graphics_pipeline.h"
#include "../rendering/gbuffer_struct.h"
#include "../../core/rendering/render_pass.h"

namespace Shard3D {
	inline namespace Systems {
		class VelocityBufferSystem {
		public:
			VelocityBufferSystem(
				S3DDevice& device,
				glm::vec2 resolution,
				S3DRenderTarget* depthStencilImage,
				S3DRenderTarget* gbuffer4Image,
				S3DRenderTarget* velocityImage,
				VkDescriptorSetLayout uboSetLayout
			);
			~VelocityBufferSystem();

			VelocityBufferSystem(const VelocityBufferSystem&) = delete;
			VelocityBufferSystem& operator= (const VelocityBufferSystem&) = delete;

			void renderAndUpdate(FrameInfo& frameInfo);
			void resize(glm::vec2 newResolution, S3DRenderTarget* newDepthImage, S3DRenderTarget* newGBuffer4Image);

			S3DRenderTarget* getAttachment() {
				return vBufferRenderTarget;
			}
		private:
			void createPipelineLayout(VkDescriptorSetLayout uboSetLayout);
			void createPipeline();
			void createPasses(glm::vec2 resolution);
			void createDescriptors(VkDescriptorImageInfo depthImageInfo, VkDescriptorImageInfo gbuffer4ImageInfo);

			S3DDevice& engineDevice;

			struct VelocityBufferPush {
				glm::mat4 invView{0.f};
				glm::mat4 invProj{0.f};
			} oldCamera;

			bool depthIsStored = false;

			S3DRenderPass* vBufferRenderPass{};
			S3DFramebuffer* vBufferFramebuffer{};
			S3DRenderTarget* vBufferRenderTarget{};

			S3DRenderTarget* depthInFlight{};
			S3DRenderTarget* currentDepth{};

			VkFormat vBufferFormat;

			uPtr<S3DGraphicsPipeline> vBufferPipeline{};
			uPtr<S3DPipelineLayout> vBufferPipelineLayout{};
			S3DPushConstant vBufferPush{};

			uPtr<S3DDescriptorSetLayout> depthStencilInputDescriptorSetLayout{};
			VkDescriptorSet depthStencilInputDescriptor{};
		};
	}
}