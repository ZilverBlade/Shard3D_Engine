#include "decal_render_system.h"

#include "../../s3dpch.h" 

#include <glm/gtc/constants.hpp>

#include "../handlers/material_system.h"
#include "../../core/asset/assetmgr.h"
#include "../../core/asset/texture_cube.h"
#include "../../ecs.h"
#include "../../core/misc/engine_settings.h"
#include "../../core/rendering/framebuffer.h"
#include "../../core/rendering/render_pass.h"
#include "../../core/vulkan_api/bindless.h"
#include "../handlers/project_system.h"

namespace Shard3D {


	static std::vector<DeferredDecalModel> decalModels{
		DeferredDecalModel::DBufferDiffuse,
		DeferredDecalModel::DBufferSpecularGlossinessReflectivity ,
		DeferredDecalModel::DBufferNormal,
		DeferredDecalModel::DBufferDiffuseSpecularGlossinessReflectivity,
		DeferredDecalModel::DBufferDiffuseSpecularGlossinessReflectivityNormal,
		DeferredDecalModel::DBufferDiffuseNormal,
		DeferredDecalModel::DBufferSpecularGlossinessReflectivityNormal,
		DeferredDecalModel::GBufferMultiplyDiffuse,
		DeferredDecalModel::GBufferMultiplySpecularGlossinessReflectivity,
		DeferredDecalModel::GBufferMultiplyDiffuseSpecularGlossinessReflectivity,
		DeferredDecalModel::GBufferAddEmission
	};
	static std::vector<DBufferBlendMode> dbufferBlendModes{
		DBufferBlendMode::Normal, DBufferBlendMode::Multiplicative, DBufferBlendMode::Additive, DBufferBlendMode::Erase
	};

	struct DecalPush {
		uint32_t material;
	};

	DecalBatchList::DecalBatchList(S3DDevice& device, ResourceSystem* resourceSystem)
		: engineDevice(device), resourceSystem(resourceSystem) {
		decalDescriptorSetLayout = S3DDescriptorSetLayout::Builder(device)
			.addBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT)
			.build();
	}
	DecalBatchList::~DecalBatchList() {
		vkDeviceWaitIdle(engineDevice.device());
		for (int i = 0; i < destroyBufferQueue.size(); i++) {
			auto& buffer = destroyBufferQueue[i].first;
			auto& destroy = destroyBufferQueue[i].second;
			delete buffer;
		}
		for (int i = 0; i < destroyDescriptorQueue.size(); i++) {
			auto& descriptor = destroyDescriptorQueue[i].first;
			auto& destroy = destroyDescriptorQueue[i].second;
			engineDevice.staticMaterialPool->freeDescriptors({ descriptor });
		}
	}

	void DecalBatchList::updateBatches(FrameInfo& frameInfo) {
		if (!activeLevel) {
			activeLevel = frameInfo.level.get();
		}
		if (activeLevel != frameInfo.level.get()) {
			activeLevel = frameInfo.level.get();
			vkDeviceWaitIdle(engineDevice.device());
			batches.clear();
			models.clear();
			actorData.clear();
		}
		for (auto actor_ : frameInfo.level->getActorRemovedComponents<Components::DeferredDecalComponent>()) {
			Actor actor = { actor_, frameInfo.level.get() };
			auto& ddc = actor.getComponent<Components::DeferredDecalComponent>();
			auto& tuple = actorData.at(actor);
			AssetID material = std::get<2>(tuple);
			DecalBatchData& batch = batches.at(std::get<0>(tuple)).at(std::get<1>(tuple)).at(material);
			uint32_t index = std::find(batch.instances.begin(), batch.instances.end(), actor) - batch.instances.begin();
			removeFromBatchBuffer(material, index, frameInfo.frameIndex, actor, std::get<0>(tuple), std::get<1>(tuple));
			actorData.erase(actor);
		}
		for (auto actor_ : frameInfo.level->getActorAddedComponents<Components::DeferredDecalComponent>()) {
			Actor actor = { actor_, frameInfo.level.get() };
			auto& ddc = actor.getComponent<Components::DeferredDecalComponent>();

			addToBatchBuffer(ddc.material, ddc.cachedIndex, frameInfo.frameIndex, actor);
			actorData[actor] = std::make_tuple(actor.getMobility() != Mobility::Static, ddc.priority, ddc.material);
		}

		if (ProjectSystem::isEditorMode()) {
			auto viewUpdate = frameInfo.level->registry.view<Components::TransformComponent, Components::DeferredDecalComponent,
				Components::BoxVolumeComponent>();
			for (auto actor_ : viewUpdate) {
				Actor actor = { actor_, frameInfo.level.get() };
				auto& ddc = actor.getComponent<Components::DeferredDecalComponent>();
				if (ddc.cachedIndex == 0xffffffff) continue;
				bool dynamic = actor.getMobility() != Mobility::Static;
				auto tuple = actorData[actor];
				auto iter = batches.at(dynamic)[ddc.priority].find(ddc.material);
				if (ddc.priority != std::get<1>(tuple)) {
					DecalBatchData& oldbatch = batches.at(std::get<0>(tuple)).at(std::get<1>(tuple)).at(std::get<2>(tuple));
					uint32_t oldindex = std::find(oldbatch.instances.begin(), oldbatch.instances.end(), actor) - oldbatch.instances.begin();
					removeFromBatchBuffer(std::get<2>(tuple), oldindex, frameInfo.frameIndex, actor, std::get<0>(tuple), std::get<1>(tuple));
					actorData.erase(actor);
					ddc.cachedIndex = 0xffffffff;
					
					addToBatchBuffer(ddc.material, ddc.cachedIndex, frameInfo.frameIndex, actor);
					actorData[actor] = { actor.getMobility() != Mobility::Static, ddc.priority, ddc.material };
					iter = batches.at(dynamic)[ddc.priority].find(ddc.material);
				} else if (iter == batches.at(dynamic)[ddc.priority].end()) {
					dynamic = !dynamic;
					iter = batches.at(dynamic)[ddc.priority].find(ddc.material);
				}
				DecalBatchData& batchData = iter->second;
				DecalInstanceData instanceData;

				auto& bvc = actor.getComponent<Components::BoxVolumeComponent>();
				instanceData.transform = actor.getTransform().transformMatrix;
				instanceData.transform[0] *= bvc.bounds.x;
				instanceData.transform[1] *= bvc.bounds.y;
				instanceData.transform[2] *= bvc.bounds.z;
				instanceData.invTransform = actor.getTransform().getInverseTransform();
				instanceData.invTransform[0][3] = ddc.opacity;
				(dynamic ? batchData.cpuAllocatedInstanceData[ddc.cachedIndex] : reinterpret_cast<DecalInstanceData*>(batchData.deviceBuffer->getMappedMemory())[ddc.cachedIndex]) = instanceData;
			}
		} else {
			auto viewUpdate = frameInfo.level->registry.view<Components::TransformComponent, Components::DeferredDecalComponent,
				Components::BoxVolumeComponent, Components::DynamicActorComponent>();
			for (auto actor_ : viewUpdate) {
				Actor actor = { actor_, frameInfo.level.get() };
				auto& ddc = actor.getComponent<Components::DeferredDecalComponent>();

				DecalBatchData& batchData = batches.at(true)[ddc.priority].at(ddc.material);
				DecalInstanceData instanceData;

				if (actor.isVisible()) {
					auto& bvc = actor.getComponent<Components::BoxVolumeComponent>();
					instanceData.transform = actor.getTransform().transformMatrix;
					instanceData.transform[0] *= bvc.bounds.x;
					instanceData.transform[1] *= bvc.bounds.z;
					instanceData.transform[2] *= bvc.bounds.y;
					instanceData.invTransform = actor.getTransform().getInverseTransform();
					instanceData.invTransform[0][3] = ddc.opacity;
					batchData.cpuAllocatedInstanceData[ddc.cachedIndex] = instanceData;
				} else {
					batchData.cpuAllocatedInstanceData[ddc.cachedIndex] = {};
				}
			}
		}
		cleanupBatchBuffers(frameInfo);

		if (ProjectSystem::isEditorMode()) {
			for (auto& [priority, batchesP] : batches[false]) {
				for (auto& [asset, batch] : batchesP) {
					batch.deviceBuffer->flush();
				}
			}
		}
		if (batches[true].size() > 0) {
			S3DSynchronization sync = S3DSynchronization(SynchronizationType::Transfer, VK_PIPELINE_STAGE_VERTEX_SHADER_BIT | VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT);
			for (auto& [priority, batchesP] : batches[true]) {
				for (auto& [asset, batch] : batchesP) {
					engineDevice.copyBuffer(
						frameInfo.commandBuffer,
						batch.hostBuffers[frameInfo.frameIndex]->getBuffer(),
						batch.deviceBuffer->getBuffer(),
						batch.instances.size() * sizeof(DecalInstanceData)
					);
					sync.addBufferMemoryBarrier(VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_TRANSFER_READ_BIT, batch.deviceBuffer);
				}
			}
			sync.syncBarrier(frameInfo.commandBuffer);
		}
	}

	void DecalBatchList::runGarbageCollector(uint32_t frameIndex, VkFence fence) {
		currentFence = fence;
		
		for (int i = 0; i < destroyBufferQueue.size(); i++) {
			auto& buffer = destroyBufferQueue[i].first;
			auto& destroy = destroyBufferQueue[i].second;
			if (destroy.getFence() == fence) continue; // from same frame
			if (destroy.isFree()) {
				delete buffer;
				destroyBufferQueue.erase(destroyBufferQueue.begin() + i);
				i--;
			}
		}
		for (int i = 0; i < destroyDescriptorQueue.size(); i++) {
			VkDescriptorSet descriptor = destroyDescriptorQueue[i].first;
			auto& destroy = destroyDescriptorQueue[i].second;
			if (destroy.getFence() == fence) continue; // from same frame
			if (destroy.isFree()) {
				engineDevice.staticMaterialPool->freeDescriptors({ descriptor });
				destroyDescriptorQueue.erase(destroyDescriptorQueue.begin() + i);
				i--;
			}
		}
	}

	void DecalBatchList::forceUpdateDecalsEDITORONLY(sPtr<Level>& level) {
		for (auto actor_ : level->registry.view<Components::TransformComponent, Components::DeferredDecalComponent,
				Components::BoxVolumeComponent, Components::StaticActorComponent>()) {
			Actor actor = { actor_, level.get() };
			auto& ddc = actor.getComponent<Components::DeferredDecalComponent>();

			addToBatchBuffer(ddc.material, ddc.cachedIndex, 0, actor);
			actorData[actor] = std::make_tuple(false, ddc.priority, ddc.material);
		}
	}

	void DecalBatchList::setAllocatedMaterialInstances(AssetID material, uint32_t instanceCount, uint32_t priority) {
		for (int i = 0; i < 2; i++) { // i == 0 -> false, i == 1 -> true
			if (batches[i][priority].find(material) != batches[i][priority].end()) {
				resizeBatchBuffer(batches[i][priority][material], instanceCount, i, priority);
			}
		}
	}

	uint32_t DecalBatchList::getRequiredInstanceCount(uint32_t bufferInstanceCount, uint32_t requiredInstances) {
		size_t newInstanceCount = requiredInstances + 1;
		uint32_t minNewSize = bufferInstanceCount * ProjectSystem::getEngineSettings().memory.arenaAllocatorCapacityGrowMultiplier;
		uint32_t instanceCount = newInstanceCount > minNewSize ? newInstanceCount : minNewSize;
		instanceCount += bufferInstanceCount;
		return instanceCount;
	}

	void DecalBatchList::resizeBatchBuffer(DecalBatchData& batchData, uint32_t newSize, bool dynamic, uint32_t priority) {
		destroyDescriptorQueue.push_back({ batchData.descriptor, S3DDestructionObject(engineDevice, currentFence) });
		destroyBufferQueue.push_back({ batchData.deviceBuffer, S3DDestructionObject(engineDevice, currentFence) });
		batchData.descriptor = {};
		if (dynamic) {
			uint32_t highestInstanceCount = 0;
			for (int i = 0; i < batchData.hostBuffers.size(); i++) {
				uint32_t instanceCount = newSize;
				S3DBuffer* newBuffer = new S3DBuffer(
					engineDevice,
					sizeof(DecalInstanceData),
					instanceCount,
					VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
					VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_CACHED_BIT
				);
				batchData.cpuAllocatedInstanceData.reserve(instanceCount);
				memcpy(newBuffer->getMappedMemory(), batchData.hostBuffers[i]->getMappedMemory(), batchData.hostBuffers[i]->getBufferSize());
				highestInstanceCount = std::max(instanceCount, highestInstanceCount);
				destroyBufferQueue.push_back({ batchData.hostBuffers[i], S3DDestructionObject(engineDevice, currentFence) });
				batchData.hostBuffers[i] = newBuffer;
			}
			batchData.deviceBuffer = new S3DBuffer(
				engineDevice,
				sizeof(DecalInstanceData),
				highestInstanceCount,
				VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
				VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
				true
			);
		} else {
			uint32_t instanceCount = getRequiredInstanceCount(batchData.deviceBuffer->getInstanceCount(), batchData.instances.size());
			batchData.deviceBuffer = new S3DBuffer(
				engineDevice,
				sizeof(DecalInstanceData),
				instanceCount,
				VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
				VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
				false
			);
		}
		writeBatchDescriptor(batchData);
	}

	void DecalBatchList::writeBatchDescriptor(DecalBatchData& batch) {
		S3DDescriptorWriter(*decalDescriptorSetLayout, *engineDevice.staticMaterialPool)
			.writeBuffer(0, &batch.deviceBuffer->descriptorInfo())
			.build(batch.descriptor);
	}

	void DecalBatchList::createBatchBuffer(AssetID asset, bool dynamic, uint32_t priority) {
		DecalBatchData& batchData = batches[dynamic][priority][asset];
		batchData.hostBuffers.resize(ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		if (dynamic) {
			for (int i = 0; i < batchData.hostBuffers.size(); i++) {
				S3DBuffer*& buffer = batchData.hostBuffers[i];
				buffer = new S3DBuffer(
					engineDevice,
					sizeof(DecalInstanceData),
					8,
					VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
					VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_CACHED_BIT,
					true
				);
				buffer->map();
			}
			batchData.deviceBuffer = new S3DBuffer(
				engineDevice,
				sizeof(DecalInstanceData),
				8,
				VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
				VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
				true
			);
		} else {
			batchData.deviceBuffer = new S3DBuffer(
				engineDevice,
				sizeof(DecalInstanceData),
				8,
				VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
				VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
			);
		}
		writeBatchDescriptor(batchData);
		Material* material = resourceSystem->retrieveMaterial(asset);
		batchData.materialID = material->getShaderMaterialID();
		DecalMaterial* decalMaterial = reinterpret_cast<DecalMaterial*>(material);
		DeferredDecalModel model = decalMaterial->model;
		DBufferBlendMode blendMode = decalMaterial->dbufferBlendMode;
		batchData.modelFlags = (DBufferFlags)model | (DBufferFlags)blendMode * (!decalMaterial->directGBuffer) | (dynamic ? 2048 : 0);
		models[batchData.modelFlags][priority].push_back(asset);
	}

	void DecalBatchList::addToBatchBuffer(AssetID material, uint32_t& cachedIndex, uint32_t frameIndex, Actor actor) {
		bool dynamic = actor.getMobility() != Mobility::Static;
		auto& ddc = actor.getComponent<Components::DeferredDecalComponent>();
		uint32_t priority = ddc.priority;
		auto iterBatch = batches[dynamic][priority].find(material);
		if (iterBatch == batches[dynamic][priority].end()) {
			createBatchBuffer(material, dynamic, priority);
			iterBatch = batches[dynamic][priority].find(material);
		}

		DecalBatchData& batchData = iterBatch->second;
		uint32_t index;
		index = batchData.instances.size();
		batchData.instances.push_back(actor);
		batchData.cpuAllocatedInstanceData.push_back({});
		cachedIndex = index;

		bool requireResize = batchData.deviceBuffer->getInstanceCount() < batchData.instances.size();
		if (requireResize) {
			uint32_t resizeCount = getRequiredInstanceCount(batchData.deviceBuffer->getInstanceCount(), batchData.instances.size());
			resizeBatchBuffer(batchData, resizeCount, dynamic, priority);
		}
		if (!dynamic) {
			DecalInstanceData instanceData;
			auto& bvc = actor.getComponent<Components::BoxVolumeComponent>();
			instanceData.transform = actor.getTransform().transformMatrix;
			instanceData.transform[0] *= bvc.bounds.x;
			instanceData.transform[1] *= bvc.bounds.y;
			instanceData.transform[2] *= bvc.bounds.z;
			instanceData.invTransform = actor.getTransform().getInverseTransform();
			instanceData.invTransform[0][3] = ddc.opacity;
			reinterpret_cast<DecalInstanceData*>(batchData.deviceBuffer->getMappedMemory())[index] = instanceData;
			batchData.deviceBuffer->flush();
		}
	}

	void DecalBatchList::removeFromBatchBuffer(AssetID material, uint32_t& cachedIndex,
		uint32_t frameIndex, Actor actor, bool oldIsDynamic, uint32_t oldPriority) {
		auto& batch = batches.at(oldIsDynamic).at(oldPriority).at(material);
		batch.eraseInstances.push_back(cachedIndex);
		cachedIndex = 0xffffffff;
	}

	void DecalBatchList::cleanupBatchBuffers(FrameInfo& frameInfo) {
		std::vector<std::tuple<bool, uint32_t, AssetID>> eraseBatches{};
		for (auto& [isDynamic, batchesM] : batches) {
			for (auto& [priority, batchesP] : batchesM) {
				for (auto& [asset, batch] : batchesP) {
					if (batch.eraseInstances.size() > 0) {
						void* temp = malloc(batch.instances.size() * sizeof(DecalInstanceData));
						int erasedCount = 0;
						int validInstanceCount = batch.instances.size();

						DecalInstanceData* writeBuffer = isDynamic ? batch.cpuAllocatedInstanceData.data()
							: static_cast<DecalInstanceData*>(batch.deviceBuffer->getMappedMemory());

						for (int index : batch.eraseInstances) {
							memcpy((char*)temp + index - erasedCount, (char*)writeBuffer + index + 1, validInstanceCount - index);
							VectorUtils::eraseItemAtIndex(batch.instances, index);
							erasedCount++;
						}
						memcpy(writeBuffer, temp, validInstanceCount - erasedCount);
						free(temp);
						for (int i = batch.eraseInstances[0]; i < batch.instances.size(); i++) {
							Actor(batch.instances[i], frameInfo.level.get()).getComponent<Components::DeferredDecalComponent>().cachedIndex = i;
						}
						batch.eraseInstances.clear();
						if (batch.instances.size() == 0) {
							eraseBatches.push_back({ isDynamic, priority, asset });
							VectorUtils::eraseItem(models[batch.modelFlags][priority], asset);
							if (isDynamic) {
								for (S3DBuffer* buffer : batch.hostBuffers) {
									destroyBufferQueue.push_back({ buffer, S3DDestructionObject(engineDevice, currentFence) });
								}
							}
							destroyBufferQueue.push_back({ batch.deviceBuffer, S3DDestructionObject(engineDevice, currentFence) });
							destroyDescriptorQueue.push_back({ batch.descriptor, S3DDestructionObject(engineDevice, currentFence) });
						}
					}
					if (isDynamic) {
						S3DBuffer* decalBuffer = isDynamic ? batch.hostBuffers[frameInfo.frameIndex] : batch.deviceBuffer;
						memcpy(decalBuffer->getMappedMemory(), batch.cpuAllocatedInstanceData.data(), batch.cpuAllocatedInstanceData.size() * sizeof(DecalInstanceData));
						decalBuffer->flush();
					}
				}
			}
		}

		for (auto& [dynamic, priority, asset]: eraseBatches) {
			batches[dynamic][priority].erase(asset);
		}
	}

	namespace Systems {
		DecalRenderSystem::DecalRenderSystem(S3DDevice& device, ResourceSystem* resourceSystem, VkDescriptorSetLayout globalSetLayout, SceneBufferInputData gbuffer)
			: engineDevice(device), resourceSystem(resourceSystem) {
			//TEMPORARY
			decalDescriptorSetLayout = S3DDescriptorSetLayout::Builder(device)
				.addBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT)
				.build();
			createRenderPasses(gbuffer);
			createFramebuffers(gbuffer);
			createPipelineLayouts(globalSetLayout, decalDescriptorSetLayout->getDescriptorSetLayout());
			createPipelines();
			createDBufferPipelines();
			writeDescriptors(gbuffer.depthSceneInfo->getDescriptor(), gbuffer.gbuffer4->getDescriptor());
		}
		DecalRenderSystem::~DecalRenderSystem() {
			engineDevice.staticMaterialPool->freeDescriptors({ depthDataDescriptor, dbufferApplyDescriptor });

			delete dbuffer0;
			delete dbuffer1;
			delete dbuffer2;

			delete renderPassDBuffer;
			delete framebufferDBuffer;
			delete renderPassDBufferApplyDR;
			delete renderPassDBufferApplySG;
			delete framebufferDBufferApplyDR;
			delete framebufferDBufferApplySG;
		}

		void DecalRenderSystem::createRenderPasses(SceneBufferInputData gbuffer) {
			dbuffer0 = new S3DRenderTarget(engineDevice, {
				VK_FORMAT_R8G8B8A8_UNORM,
				VK_IMAGE_ASPECT_COLOR_BIT,
				VK_IMAGE_VIEW_TYPE_2D,
				gbuffer.colorAttachment->getDimensions(),
				VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				S3DRenderTargetType::Color,
				false,
				VK_SAMPLE_COUNT_1_BIT
				}
			);
			dbuffer1 = new S3DRenderTarget(engineDevice, {
				VK_FORMAT_R8G8B8A8_UNORM,
				VK_IMAGE_ASPECT_COLOR_BIT,
				VK_IMAGE_VIEW_TYPE_2D,
				gbuffer.colorAttachment->getDimensions(),
				VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				S3DRenderTargetType::Color,
				false,
				VK_SAMPLE_COUNT_1_BIT
				}
			);
			dbuffer2 = new S3DRenderTarget(engineDevice, {
				VK_FORMAT_R8G8B8A8_UNORM,
				VK_IMAGE_ASPECT_COLOR_BIT,
				VK_IMAGE_VIEW_TYPE_2D,
				gbuffer.colorAttachment->getDimensions(),
				VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				S3DRenderTargetType::Color,
				false,
				VK_SAMPLE_COUNT_1_BIT
				}
			);
			AttachmentInfo dbuffer0Attachment{};
			dbuffer0Attachment.framebufferAttachment = dbuffer0;
			dbuffer0Attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			dbuffer0Attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			dbuffer0Attachment.clear.color = { 1.0, 1.0, 1.0, 0.0 };

			AttachmentInfo dbuffer1Attachment{};
			dbuffer1Attachment.framebufferAttachment = dbuffer1;
			dbuffer1Attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			dbuffer1Attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			dbuffer1Attachment.clear.color = { 1.0, 1.0, 1.0, 0.0 };

			AttachmentInfo dbuffer2Attachment{};
			dbuffer2Attachment.framebufferAttachment = dbuffer2;
			dbuffer2Attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			dbuffer2Attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			dbuffer2Attachment.clear.color = { 0.5, 0.5, 1.0, 0.0 };

			renderPassDBuffer = new S3DRenderPass(engineDevice, { dbuffer0Attachment, dbuffer1Attachment, dbuffer2Attachment });

			AttachmentInfo gbuffer0Attachment{};
			gbuffer0Attachment.framebufferAttachment = gbuffer.gbuffer0;
			gbuffer0Attachment.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
			gbuffer0Attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

			AttachmentInfo gbuffer1Attachment{};
			gbuffer1Attachment.framebufferAttachment = gbuffer.gbuffer1;
			gbuffer1Attachment.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
			gbuffer1Attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

			renderPassDBufferApplyDR = new S3DRenderPass(engineDevice, { gbuffer0Attachment });
			renderPassDBufferApplySG = new S3DRenderPass(engineDevice, { gbuffer1Attachment });
		}

		void DecalRenderSystem::createFramebuffers(SceneBufferInputData gbuffer) {
			framebufferDBuffer = new S3DFramebuffer(
				engineDevice,
				renderPassDBuffer,
				{ dbuffer0, dbuffer1, dbuffer2 }
			);
			framebufferDBufferApplyDR = new S3DFramebuffer(
				engineDevice,
				renderPassDBufferApplyDR,
				{ gbuffer.gbuffer0 }
			);
			framebufferDBufferApplySG = new S3DFramebuffer(
				engineDevice,
				renderPassDBufferApplySG,
				{ gbuffer.gbuffer1 }
			);
		}

		void DecalRenderSystem::writeDescriptors(VkDescriptorImageInfo depthImageInfo, VkDescriptorImageInfo gbuffer4ImageInfo) {
			S3DDescriptorWriter(*depthDataDescriptorSetLayout, *engineDevice.staticMaterialPool)
				.writeImage(0, &depthImageInfo)
				.writeImage(1, &gbuffer4ImageInfo)
				.build(depthDataDescriptor);
			S3DDescriptorWriter(*dbufferApplyDescriptorSetLayout, *engineDevice.staticMaterialPool)
				.writeImage(0, &dbuffer0->getDescriptor())
				.writeImage(1, &dbuffer1->getDescriptor())
				.build(dbufferApplyDescriptor);
		}

		void DecalRenderSystem::createPipelineLayouts(VkDescriptorSetLayout globalSetLayout, VkDescriptorSetLayout decalSetLayout) {
			dbufferApplyDescriptorSetLayout = S3DDescriptorSetLayout::Builder(engineDevice)
				.addBinding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT)
				.addBinding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT)
				.build();
			depthDataDescriptorSetLayout = S3DDescriptorSetLayout::Builder(engineDevice)
				.addBinding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT)
				.addBinding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT)
				.build();
			
			VkPushConstantRange pushRange;
			pushRange.offset = 0;
			pushRange.size = sizeof(DecalPush);
			pushRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT;

			decalPipelineLayout = make_uPtr<S3DPipelineLayout>(engineDevice,
				std::vector<VkPushConstantRange>{ pushRange },
				std::vector<VkDescriptorSetLayout>{ globalSetLayout, resourceSystem->getDescriptor()->getLayout(), depthDataDescriptorSetLayout->getDescriptorSetLayout(), decalSetLayout }
			);
			dbufferApplyPipelineLayout = make_uPtr<S3DPipelineLayout>(engineDevice,
				std::vector<VkPushConstantRange>{ },
				std::vector<VkDescriptorSetLayout>{ dbufferApplyDescriptorSetLayout->getDescriptorSetLayout() }
			);
		}

		void DecalRenderSystem::createPipelines() {
			for (DeferredDecalModel model : decalModels) {
				if (model == DeferredDecalModel::GBufferMultiplyDiffuse ||
					model == DeferredDecalModel::GBufferMultiplySpecularGlossinessReflectivity ||
					model == DeferredDecalModel::GBufferMultiplyDiffuseSpecularGlossinessReflectivity ||
					model == DeferredDecalModel::GBufferAddEmission) {
					// todo
				} else {
					for (DBufferBlendMode blendMode : dbufferBlendModes) {
						S3DGraphicsPipelineConfigInfo configInfo = S3DGraphicsPipelineConfigInfo(3);
						bool diffuse = model == DeferredDecalModel::DBufferDiffuse ||
							model == DeferredDecalModel::DBufferDiffuseNormal ||
							model == DeferredDecalModel::DBufferDiffuseSpecularGlossinessReflectivity || 
							model == DeferredDecalModel::DBufferDiffuseSpecularGlossinessReflectivityNormal;
						bool ssc = model == DeferredDecalModel::DBufferDiffuseSpecularGlossinessReflectivity ||
							model == DeferredDecalModel::DBufferSpecularGlossinessReflectivity || 
							model == DeferredDecalModel::DBufferSpecularGlossinessReflectivityNormal ||
							model == DeferredDecalModel::DBufferDiffuseSpecularGlossinessReflectivityNormal;
						bool normal = model == DeferredDecalModel::DBufferNormal || 
							model == DeferredDecalModel::DBufferDiffuseNormal ||
							model == DeferredDecalModel::DBufferDiffuseSpecularGlossinessReflectivityNormal;

						configInfo.enableVertexDescriptions(VertexDescriptionOptions_Position);
						configInfo.disableDepthWrite();
						configInfo.depthStencilInfo.depthCompareOp =
							ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit ?
							VK_COMPARE_OP_LESS_OR_EQUAL : VK_COMPARE_OP_GREATER_OR_EQUAL;

						configInfo.setCullMode(VK_CULL_MODE_FRONT_BIT);
						if (diffuse) {
							if (blendMode == DBufferBlendMode::Multiplicative) {
								configInfo.enableMultiplicativeAlphaBlending(0);
							} else if (blendMode == DBufferBlendMode::Additive) {
								configInfo.enableAdditiveBlending(0);
							} else if (blendMode == DBufferBlendMode::Erase) {
								configInfo.enableErasureBlending(0);
							} else {
								configInfo.enableAlphaBlending(0);
								configInfo.colorBlendAttachments[0].srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA;
								configInfo.colorBlendAttachments[0].dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
							}
						}
						if (ssc) {
							if (blendMode == DBufferBlendMode::Multiplicative) {
								configInfo.enableMultiplicativeAlphaBlending(1);
							} else if (blendMode == DBufferBlendMode::Additive) {
								configInfo.enableAdditiveBlending(1);
							} else if (blendMode == DBufferBlendMode::Erase) {
								configInfo.enableErasureBlending(1);
							} else {
								configInfo.enableAlphaBlending(1);
								configInfo.colorBlendAttachments[1].srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA;
								configInfo.colorBlendAttachments[1].dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
							}
						}
						if (normal) {
							if (blendMode == DBufferBlendMode::Erase) {
								configInfo.enableErasureBlending(2);
							} else {
								configInfo.enableAlphaBlending(2);
								configInfo.colorBlendAttachments[2].srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA;
								configInfo.colorBlendAttachments[2].dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
							}
						}

						VkColorComponentFlags allColors = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
						configInfo.colorBlendAttachments[0].colorWriteMask = diffuse ? allColors : 0;
						configInfo.colorBlendAttachments[1].colorWriteMask = ssc ? allColors : 0;
						configInfo.colorBlendAttachments[2].colorWriteMask = normal ? allColors : 0;

						configInfo.renderPass = renderPassDBuffer->getRenderPass();
						configInfo.pipelineLayout = decalPipelineLayout->getPipelineLayout();
						S3DShaderSpecialization specialization;
						specialization.addConstant(0, (VkBool32)diffuse);
						specialization.addConstant(1, (VkBool32)ssc);
						specialization.addConstant(2, (VkBool32)normal);
						specialization.addConstant(3, blendMode);
						S3DShader fragmentShader = S3DShader(ShaderType::Fragment, "resources/shaders/materials/deferred_decal.frag", "materials", specialization, {"DBUFFER"}, "deferred_decal_dbuffer.frag");

						decalPipelines[(DBufferFlags)model | (DBufferFlags)blendMode] = make_uPtr<S3DGraphicsPipeline>(engineDevice, std::vector<S3DShader>{
							S3DShader(ShaderType::Vertex, "resources/shaders/rendering/decal.vert", "rendering"), fragmentShader
						}, configInfo);
					}
				}
			}
		}

		void DecalRenderSystem::createDBufferPipelines() {
			S3DGraphicsPipelineConfigInfo configInfo = S3DGraphicsPipelineConfigInfo(1);

			configInfo.disableDepthTest();
			configInfo.enableVertexDescriptions(VertexDescriptionOptions_Position);
			configInfo.renderPass = renderPassDBufferApplyDR->getRenderPass();
			configInfo.pipelineLayout = dbufferApplyPipelineLayout->getPipelineLayout();

			configInfo.colorBlendAttachments[0].blendEnable = VK_TRUE;
			configInfo.colorBlendAttachments[0].srcColorBlendFactor = VK_BLEND_FACTOR_SRC1_COLOR;
			configInfo.colorBlendAttachments[0].dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR;
			configInfo.colorBlendAttachments[0].srcAlphaBlendFactor = VK_BLEND_FACTOR_SRC1_ALPHA;
			configInfo.colorBlendAttachments[0].dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA;

			configInfo.colorBlendAttachments[0].colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
			dbufferApplyPipelineDR = make_uPtr<S3DGraphicsPipeline>(engineDevice, std::vector<S3DShader>{
				S3DShader(ShaderType::Vertex, "resources/shaders/fullscreen_quad.vert", "rendering"), S3DShader(ShaderType::Fragment, "resources/shaders/rendering/dbuffer_apply_dc.frag", "rendering")
			}, configInfo);

			configInfo.renderPass = renderPassDBufferApplySG->getRenderPass();
			configInfo.colorBlendAttachments[0].srcColorBlendFactor = VK_BLEND_FACTOR_SRC1_COLOR;
			configInfo.colorBlendAttachments[0].dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR;
			configInfo.colorBlendAttachments[0].colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT;
			dbufferApplyPipelineSG = make_uPtr<S3DGraphicsPipeline>(engineDevice, std::vector<S3DShader>{
				S3DShader(ShaderType::Vertex, "resources/shaders/fullscreen_quad.vert", "rendering"), S3DShader(ShaderType::Fragment, "resources/shaders/rendering/dbuffer_apply_ss.frag", "rendering")
			}, configInfo);
		}

		void DecalRenderSystem::renderDecals(FrameInfo& frameInfo) {
			VkDescriptorSet sets[3]{
				frameInfo.globalDescriptorSet,
				frameInfo.resourceSystem->getDescriptor()->getDescriptor(),
				depthDataDescriptor
			};
			vkCmdBindDescriptorSets(
				frameInfo.commandBuffer,
				VK_PIPELINE_BIND_POINT_GRAPHICS,
				decalPipelineLayout->getPipelineLayout(),
				0,
				3,
				sets,
				0,
				nullptr
			);

			renderPassDBuffer->beginRenderPass(frameInfo.commandBuffer, framebufferDBuffer);
			for (auto& [pipelineFlags, pipeline] : decalPipelines) {
				for (uint32_t dyn = 0; dyn <= 2048; dyn += 2048) {
					auto& ppp = frameInfo.renderObjects.decalList->models[pipelineFlags | dyn];
					if (ppp.size()) {
						pipeline->bind(frameInfo.commandBuffer);
					}
					for (auto& [priority, materials] : ppp) {
						for (AssetID& asset : materials) {
							DecalBatchData& batchData = frameInfo.renderObjects.decalList->batches.at(dyn == 2048).at(priority).at(asset);
							DecalPush push;
							push.material = batchData.materialID;
							vkCmdPushConstants(
								frameInfo.commandBuffer,
								decalPipelineLayout->getPipelineLayout(),
								VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
								0,
								sizeof(DecalPush),
								&push
							);
							vkCmdBindDescriptorSets(
								frameInfo.commandBuffer,
								VK_PIPELINE_BIND_POINT_GRAPHICS,
								decalPipelineLayout->getPipelineLayout(),
								3,
								1,
								&batchData.descriptor,
								0,
								nullptr
							);
							frameInfo.resourceSystem->retrieveMesh(ResourceSystem::coreAssets.m_defaultModel)
								->drawAll(frameInfo.commandBuffer, batchData.instances.size());
						}
					}
				}
			}
			renderPassDBuffer->endRenderPass(frameInfo.commandBuffer);
		}
		void DecalRenderSystem::applyDBuffer(FrameInfo& frameInfo) {
			vkCmdBindDescriptorSets(
				frameInfo.commandBuffer,
				VK_PIPELINE_BIND_POINT_GRAPHICS,
				dbufferApplyPipelineLayout->getPipelineLayout(),
				0,
				1,
				&dbufferApplyDescriptor,
				0,
				nullptr
			);
			renderPassDBufferApplyDR->beginRenderPass(frameInfo.commandBuffer, framebufferDBufferApplyDR);
			dbufferApplyPipelineDR->bind(frameInfo.commandBuffer);
			vkCmdDraw(frameInfo.commandBuffer, 6, 1, 0, 0);
			renderPassDBufferApplyDR->endRenderPass(frameInfo.commandBuffer);
			renderPassDBufferApplySG->beginRenderPass(frameInfo.commandBuffer, framebufferDBufferApplySG);
			dbufferApplyPipelineSG->bind(frameInfo.commandBuffer);
			vkCmdDraw(frameInfo.commandBuffer, 6, 1, 0, 0);
			renderPassDBufferApplySG->endRenderPass(frameInfo.commandBuffer);
		}

		void DecalRenderSystem::resize(SceneBufferInputData gbuffer) {
			framebufferDBuffer->resize(gbuffer.colorAttachment->getDimensions(), renderPassDBuffer);
			framebufferDBufferApplyDR->resize(gbuffer.colorAttachment->getDimensions(), renderPassDBufferApplyDR);
			framebufferDBufferApplySG->resize(gbuffer.colorAttachment->getDimensions(), renderPassDBufferApplySG);
			writeDescriptors(gbuffer.depthSceneInfo->getDescriptor(), gbuffer.gbuffer4->getDescriptor());
		}
	}
}