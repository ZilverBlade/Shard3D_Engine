#pragma once

#define SHUDVK_NO_VULKAN
#include <shudcpp/shud.h>
#include <shudvk/vk_context.h>
#include "../../s3dstd.h"
#include "../../vulkan_abstr.h"
#include "../../core/misc/frame_info.h"	  
#include "../../core/vulkan_api/graphics_pipeline.h"
#include "../../core/rendering/render_pass.h"
#include "../../core/asset/texture2d.h"
#include "../../utils/json_ext.h"

namespace Shard3D {
	struct HUDElementData {
		std::string scriptClass{};
		SHUD::Element::Base* genericBase{};
		bool visible = true;
		std::unordered_map<std::string, HUDElementData*> children{};
		AssetID textureAtlasAsset = AssetID("engine/textures/null_tex.s3dasset");		
		SHUD::PressState state{};
		bool localizable = false;
		bool formattable = false;
		std::unordered_map<std::string, std::string> textFormattingData{};

		void createHUDElementAsset(AssetID asset, const std::string& name);
		std::string loadHUDElementAsset(const AssetID& asset, ResourceSystem* resourceSystem);

		void serializeHUDElement(nlohmann::ordered_json& elementOut, const std::string& elementName);
		std::string deserializeHUDElement(const simdjson::dom::element& data, ResourceSystem* resourceSystem);
	};
	class HUDRenderSystem {
	public:
		HUDRenderSystem(S3DDevice& device, S3DWindow& window, ResourceSystem* resourceSystem, S3DRenderTarget* sceneToOverlay, glm::vec2 resolution);
		~HUDRenderSystem();

		void resize(S3DRenderTarget* newScene, glm::vec2 newSize);

		void newFrame();
		const std::vector<HUDElementData*>& getElementActionList();
		void setCursorPosition(glm::vec2 extentOffset, glm::vec2 trueExtent, glm::vec2 position);
		void renderHUDLayer(SHUD::DrawList* drawList, const std::string& hudLayer);
		void render(VkCommandBuffer commandBuffer, uint32_t frameIndex);

		SHUD::ResourceObject allocateTexture(AssetID asset);
		SHUD::DrawList* getDrawList() {
			return context->GetDrawList();
		}
		SHUD::TextureObject getDefaultButtonAtlas();

		auto& getHUDLayer(const std::string& layer) {
			return elements[layer];
		}
		void eraseHUDLayer(const std::string& layer) {
			eraseHUDLayerData(elements.at(layer));
			elements.erase(layer);
		}
		static void eraseHUDLayerData(std::unordered_map<std::string, HUDElementData*>& list) {
			for (auto& [string, data] : list) {
				if (data->children.size()) {
					eraseHUDLayerData(data->children);
				}
				//delete data->genericBase; // TODO: memory leak, SHUD add virtual destructors
				delete data; 
			}
		}

		static void eraseHUDElement(const std::string& element, std::unordered_map<std::string, HUDElementData*>& list) {
			HUDElementData* data = nullptr;
			if (auto iter = list.find(element); iter == list.end()) {
				for (auto& [str, subData] : list) {
					eraseHUDElement(element, subData->children);
				}
			} else {
				data = iter->second;
				//delete data->genericBase;
				delete data; 
				list.erase(element);
			}
		}
		std::vector<std::string> getAllLayers() {
			std::vector<std::string> layers;
			layers.reserve(elements.size());
			for (auto& [string, data] : elements) {
				layers.push_back(string);
			}
			return layers;
		}

		static void createHUDLayerAsset(const std::string& path, const std::unordered_map<std::string, HUDElementData*>& list);
		static void loadHUDLayerAsset(const std::string& path, std::unordered_map<std::string, HUDElementData*>& list, ResourceSystem* resourceSystem);
	private:
		void renderElementData(SHUD::DrawList* drawList, HUDElementData* elementData);

		void createRenderPass(S3DRenderTarget* scene);
		void createFramebuffer(S3DRenderTarget* scene);
		
		S3DRenderPass* renderPass{};
		S3DFramebuffer* framebuffer{};

		std::unordered_map<AssetID, SHUD::ResourceObject> allocatedTextures;
		std::unordered_map<std::string /*HUD*/, std::unordered_map<std::string, HUDElementData*>> elements;

		std::vector<HUDElementData*> elementActions;

		SHUD::ResourceObject defaultbtn;
		ResourceSystem* resourceSystem;

		SHUD::VulkanContext* context{};
		glm::vec2 resolution;
		S3DDevice& device;
		S3DWindow& window;
	};
}