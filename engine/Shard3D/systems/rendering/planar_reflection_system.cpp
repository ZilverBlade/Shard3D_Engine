#include "planar_reflection_system.h"
#include "terrain_system.h"

namespace Shard3D {
	PlanarReflectionInstance::PlanarReflectionInstance(S3DDevice& device, ResourceSystem* resourceSystem, bool irradiance, DeferredRenderSystem::RendererFeatures rendererFeatures, glm::vec2 resolution, VkDescriptorSetLayout sceneSetLayout, VkDescriptorSetLayout skeletonSetLayout, VkDescriptorSetLayout terrainSetLayout, uint32_t framesInFlight)
		: resolution(resolution), irradianceGen(irradiance), engineDevice(device), resourceSystem(resourceSystem) {
		globalPool = S3DDescriptorPool::Builder(engineDevice)
			.setMaxSets(framesInFlight)
			.addPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, framesInFlight)
			.build();
		uboBuffers.resize(framesInFlight);
		for (int i = 0; i < uboBuffers.size(); i++) {
			uboBuffers[i] = make_uPtr<S3DBuffer>(
				engineDevice,
				sizeof(GlobalUbo),
				1,
				VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
				);
			uboBuffers[i]->map();
		}
		globalSetLayout = S3DDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT)
			.build();
		globalDescriptorSets.resize(framesInFlight);
		for (int i = 0; i < globalDescriptorSets.size(); i++) {
			auto bufferInfo = uboBuffers[i]->descriptorInfo();
			S3DDescriptorWriter(*globalSetLayout, *globalPool)
				.writeBuffer(0, &bufferInfo)
				.build(globalDescriptorSets[i]);
		}
		rendererFeatures.enableSSAO = false;
		rendererFeatures.enableClipPlane = true;
		rendererFeatures.enableNormalDecals = false;
		deferredRenderSystem = new DeferredRenderSystem(engineDevice, resourceSystem, rendererFeatures, globalSetLayout->getDescriptorSetLayout(), sceneSetLayout, skeletonSetLayout, terrainSetLayout, resolution);
		SceneBufferInputData gbuffer = deferredRenderSystem->getGBufferAttachments();

		if (ProjectSystem::getEngineSettings().renderer.enableDeferredDecals) {
			decalRenderSystem = new DecalRenderSystem(device, resourceSystem,
				globalSetLayout->getDescriptorSetLayout(), deferredRenderSystem->getGBufferAttachments());
			deferredRenderSystem->writeDescriptors(decalRenderSystem->getDBufferAttachments().dbuffer2->getDescriptor());
		} else {
			deferredRenderSystem->writeDescriptors();
		}
		if (irradiance) {
			createIrradianceGenPasses();
		}

		skyAtmosphere = new SkyAtmosphereSystem(engineDevice, resourceSystem, globalSetLayout->getDescriptorSetLayout(), deferredRenderSystem->getCompositionRenderPass()->getRenderPass());
		telemetry = new Telemetry(engineDevice);

		renderCPU = make_uPtr<TelemetryCPUMetric>(telemetry, "Reflection Renderer", "RenderScene (CPU)");
		renderGPU = make_uPtr<TelemetryGPUMetric>(telemetry, "Reflection Renderer", "RenderScene (GPU)");
		irradianceGPU = make_uPtr<TelemetryGPUMetric>(telemetry, "Reflection Renderer", "GenerateIrradiance (GPU)");

		telemetry->registerTelemetryMetric(renderCPU.get());
		telemetry->registerTelemetryMetric(renderGPU.get());
		reflectionDescriptorID = resourceSystem->getDescriptor()->allocateIndex(BINDLESS_SAMPLER_BINDING);
		if (irradiance) {
			irradianceDescriptorID = resourceSystem->getDescriptor()->allocateIndex(BINDLESS_SAMPLER_BINDING);
		}
		createDescriptor();
		updateResourceSize();
	}
	PlanarReflectionInstance::~PlanarReflectionInstance() {
		resourceSystem->getDescriptor()->freeIndex(BINDLESS_SAMPLER_BINDING, reflectionDescriptorID);
		if (envIrradianceGen) {
			resourceSystem->getDescriptor()->freeIndex(BINDLESS_SAMPLER_BINDING, irradianceDescriptorID);
		}
		delete deferredRenderSystem;
		if (decalRenderSystem) delete decalRenderSystem;
		delete skyAtmosphere;
		delete telemetry;
		if (envIrradianceGen) {
			delete envIrradianceGen;
			delete envIrradianceBlurH;
			delete envIrradianceBlurV;
		}
	}
	void PlanarReflectionInstance::updateResourceSize() {
		resourceSize = 0;
		auto gbuffer = getGBufferImages();
		resourceSize += gbuffer.colorAttachment->getResourceSize();
		resourceSize += gbuffer.depthSceneInfo->getResourceSize();
		resourceSize += gbuffer.gbuffer0->getResourceSize();
		resourceSize += gbuffer.gbuffer1->getResourceSize();
		resourceSize += gbuffer.gbuffer2->getResourceSize();
		resourceSize += gbuffer.gbuffer3->getResourceSize();
		resourceSize += gbuffer.gbuffer4->getResourceSize();
		if (ProjectSystem::getEngineSettings().renderer.enableDeferredDecals) {
			auto dbuffer = decalRenderSystem->getDBufferAttachments();
			resourceSize += dbuffer.dbuffer0->getResourceSize();
			resourceSize += dbuffer.dbuffer1->getResourceSize();
			resourceSize += dbuffer.dbuffer2->getResourceSize();
		}
		if (envIrradianceGen) {
			resourceSize += envIrradianceGen->getAttachment()->getResourceSize();
			resourceSize += envIrradianceBlurH->getAttachment()->getResourceSize();
			resourceSize += envIrradianceBlurV->getAttachment()->getResourceSize();
		}
		resourceSize += deferredRenderSystem->getWBColAccumAttachment()->getResourceSize();
		resourceSize += deferredRenderSystem->getWBColRevealAttachment()->getResourceSize();
	}
	void PlanarReflectionInstance::render(VkCommandBuffer commandBuffer, VkDescriptorSet sceneSSBO, uint32_t frameIndex, float aspectRatio, Actor mainCameraActor, glm::vec4 clipPlane, float clipOffset, glm::mat3 orientation, float irradianceSampleRadius, sPtr<Level>& level, RenderObjects renderObjects) {
		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::GENERIC, "PLANAR_REFLECTION_RENDER_INSTANCE", commandBuffer);

		auto cameraComponent = mainCameraActor.getComponent<Components::CameraComponent>(); // must copy not ref
		cameraComponent.ar = aspectRatio;
		cameraComponent.setProjection(ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit, ProjectSystem::getEngineSettings().renderer.infiniteFarZ);

		FrameInfo frameInfo{
			frameIndex,
			commandBuffer,
			cameraComponent.camera,
			telemetry,
			globalDescriptorSets[frameIndex],
			sceneSSBO,
			level,
			renderObjects,
			resourceSystem
		};
		glm::vec4 clipPlaneEquation = { clipPlane.x, clipPlane.z, clipPlane.y, clipPlane.w };
		glm::mat4 reflectedCameraTransform = mainCameraActor.getTransform().transformMatrix;
		glm::mat4 reflectionMatrix = {
			{ 1.0f - 2.0f * clipPlaneEquation.x * clipPlaneEquation.x, -2.0f * clipPlaneEquation.x * clipPlaneEquation.y, -2.0f * clipPlaneEquation.x * clipPlaneEquation.z, 0.0f },
			{ -2.0f * clipPlaneEquation.y * clipPlaneEquation.x, 1.0f - 2.0f * clipPlaneEquation.y * clipPlaneEquation.y, -2.0f * clipPlaneEquation.y * clipPlaneEquation.z, 0.0f },
			{ -2.0f * clipPlaneEquation.z * clipPlaneEquation.x, -2.0f * clipPlaneEquation.z * clipPlaneEquation.y, 1.0f - 2.0f * clipPlaneEquation.z * clipPlaneEquation.z, 0.0f },
			{ -2.0f * clipPlaneEquation.x * clipPlaneEquation.w , -2.0f * clipPlaneEquation.y * clipPlaneEquation.w, -2.0f * clipPlaneEquation.z * clipPlaneEquation.w, 1.0f },
		};
		frameInfo.camera.setViewYXZ(reflectionMatrix * reflectedCameraTransform);
		frameInfo.camera.flipProjectionUpsideDown();

		GlobalUbo ubo{};
		ubo.projection = frameInfo.camera.getProjection();
		ubo.inverseProjection = frameInfo.camera.getInverseProjection();
		ubo.view = frameInfo.camera.getView();
		ubo.inverseView = frameInfo.camera.getInverseView();
		ubo.projectionView = frameInfo.camera.getProjectionView();
		ubo.globalClipPlane = clipPlaneEquation;
		ubo.globalClipPlane.w += clipOffset;
		ubo.frustumPlanes = frameInfo.camera.getFrustumPlanes();
		ubo.screenSize = resolution;
		ubo.nearPlane = frameInfo.camera.getNear();
		ubo.farPlane = frameInfo.camera.getFar();

		uboBuffers[frameIndex]->writeToBuffer(&ubo);
		uboBuffers[frameIndex]->flush();

		renderCPU->record();
		renderGPU->record(commandBuffer);

		if (frameInfo.renderObjects.terrainList->getTerrain()) {
			frameInfo.renderObjects.terrainList->getTerrain()->updateVisibilityCalcs(commandBuffer, frameInfo.camera, 32.0f);
		}
		skyAtmosphere->runGarbageCollector(frameInfo.frameIndex, nullptr);
		deferredRenderSystem->beginEarlyZPass(frameInfo);
		deferredRenderSystem->renderEarlyDepthMesh3D(frameInfo, MeshMobilityOption_All);
		deferredRenderSystem->renderEarlyDepthTerrain(frameInfo);
		deferredRenderSystem->endEarlyZPass(frameInfo);

		deferredRenderSystem->beginGBufferPass(frameInfo);
		deferredRenderSystem->renderGBufferTerrain(frameInfo);
		deferredRenderSystem->renderGBufferMesh3D(frameInfo, MeshMobilityOption_All);
		deferredRenderSystem->endGBufferPass(frameInfo);

		if (decalRenderSystem) {
			decalRenderSystem->renderDecals(frameInfo);
			decalRenderSystem->applyDBuffer(frameInfo);
		}

		skyAtmosphere->renderSkyLightCubemap(frameInfo);
		deferredRenderSystem->composeLighting(frameInfo);

		deferredRenderSystem->beginCompositionPass(frameInfo);
		deferredRenderSystem->renderSkybox(frameInfo);
		skyAtmosphere->render(frameInfo);
		deferredRenderSystem->endComposition(frameInfo);

		deferredRenderSystem->beginBlendingPass(frameInfo);
		deferredRenderSystem->renderForwardMesh3D(frameInfo, MeshMobilityOption_All);
		deferredRenderSystem->endBlendingPass(frameInfo);

		deferredRenderSystem->beginCompositionPass(frameInfo);
		deferredRenderSystem->composeAlpha(frameInfo);
		deferredRenderSystem->endComposition(frameInfo);
		renderCPU->end();
		renderGPU->end(commandBuffer);
		if (envIrradianceGen) {
			irradianceGPU->record(commandBuffer);

			struct Push {
				glm::mat4 proj;
				glm::mat3x4 orientation;
				float irradianceSampleRadius;
			}push;
			push.proj = ubo.projection;
			push.orientation = orientation;
			push.irradianceSampleRadius = irradianceSampleRadius;
			envIrradianceGen->render(frameInfo.commandBuffer, push);
			glm::vec2 bd = { 1.0, 0.0 };
			envIrradianceBlurH->render(frameInfo.commandBuffer, bd);
			bd = { 0.0, 1.0 };
			envIrradianceBlurV->render(frameInfo.commandBuffer, bd);
			irradianceGPU->end(commandBuffer);
		}
	}
	void PlanarReflectionInstance::resize(glm::vec2 newResolution, VkDescriptorImageInfo depthSceneInfo) {
		deferredRenderSystem->resize({ newResolution, 1 });
		SceneBufferInputData gbuffer = deferredRenderSystem->getGBufferAttachments();
		if (decalRenderSystem) {
			decalRenderSystem->resize(deferredRenderSystem->getGBufferAttachments());
			deferredRenderSystem->writeDescriptors(decalRenderSystem->getDBufferAttachments().dbuffer2->getDescriptor());
		} else {
			deferredRenderSystem->writeDescriptors();
		}
		if (envIrradianceGen) {
			delete envIrradianceGen;
			createIrradianceGenPasses();
		}

		createDescriptor();
		updateResourceSize();
	}

	void PlanarReflectionInstance::createIrradianceGenPasses() {
		S3DShaderSpecialization specialization;
		const float sampleDelta = 3.1415926f / 16.f;
		specialization.addConstant(0, sampleDelta);
		glm::vec2 irradianceRes = glm::max(resolution / 8.0f, glm::vec2(64, 32));

		envIrradianceGen = new PostProcessingEffect(
			engineDevice,
			irradianceRes,
			S3DShader(ShaderType::Fragment, "resources/shaders/processing/envirrgen_planar.frag", "misc", specialization),
			{ deferredRenderSystem->getGBufferAttachments().colorAttachment->getDescriptor(),
			deferredRenderSystem->getGBufferAttachments().depthSceneInfo->getDescriptor() },
			VK_FORMAT_B10G11R11_UFLOAT_PACK32,
			VK_IMAGE_VIEW_TYPE_2D
		);
		envIrradianceBlurH = new PostProcessingEffect(
			engineDevice, irradianceRes,
			S3DShader{ ShaderType::Fragment,  "resources/shaders/postfx/fast_gaussian_9rgb.frag", "postfx" },
			std::vector<VkDescriptorImageInfo>{ envIrradianceGen->getAttachment()->getDescriptor() },
			VK_FORMAT_B10G11R11_UFLOAT_PACK32
		);
		envIrradianceBlurV = new PostProcessingEffect(
			engineDevice, irradianceRes,
			S3DShader{ ShaderType::Fragment,  "resources/shaders/postfx/fast_gaussian_9rgb.frag", "postfx" },
			std::vector<VkDescriptorImageInfo>{ envIrradianceBlurH->getAttachment()->getDescriptor() },
			VK_FORMAT_B10G11R11_UFLOAT_PACK32
		);
	}

	void PlanarReflectionInstance::createDescriptor() {
		VkDescriptorImageInfo reflectionInfo = getFinalImage()->getDescriptor();
		BindlessDescriptorWriter(*resourceSystem->getDescriptor()).writeImage(BINDLESS_SAMPLER_BINDING, reflectionDescriptorID, &reflectionInfo).build();
		if (envIrradianceBlurV) {
			VkDescriptorImageInfo irradianceInfo = envIrradianceBlurV->getAttachment()->getDescriptor();
			BindlessDescriptorWriter(*resourceSystem->getDescriptor()).writeImage(BINDLESS_SAMPLER_BINDING, irradianceDescriptorID, &irradianceInfo).build();
		}
	}

	PlanarReflectionSystem::PlanarReflectionSystem(S3DDevice& device, ResourceSystem* resourceSystem, VkDescriptorImageInfo depthSceneInfo, VkDescriptorSetLayout sceneSetLayout, VkDescriptorSetLayout skeletonSetLayout, VkDescriptorSetLayout terrainSetLayout, uint32_t framesInFlight)
	: sceneSetLayout(sceneSetLayout), depthSceneInfo(depthSceneInfo), skeletonSetLayout(skeletonSetLayout), terrainDescriptorSetLayout(terrainSetLayout), resourceSystem(resourceSystem), maxFramesInFlight(framesInFlight), engineDevice(device) {
		
	}
	PlanarReflectionSystem::~PlanarReflectionSystem() {
		vkDeviceWaitIdle(engineDevice.device());
		for (auto& [refl, destruct] : destroyReflectionQueue) {
			delete refl;
		}
		for (auto& [actor, refl] : reflectionMaps) {
			delete refl;
		}
	}

	void PlanarReflectionSystem::runGarbageCollector(uint32_t frameIndex, VkFence fence) {
		currentFrameIndex = frameIndex;
		uint32_t lastOccupiedFrameIndex = (currentFrameIndex + 1) % maxFramesInFlight;
		for (auto& [info, recreate] : rebuildReflections) {
			destroyReflectionQueue.push_back({ reflectionMaps[info.actor], {engineDevice, fence} });
			reflectionMaps.erase(info.actor);
			glm::vec2 res = info.resolutionMultiplier * currentResolution;
			if (recreate) {
				DeferredRenderSystem::RendererFeatures renderFeatures{};
				renderFeatures.finalFrameBufferFormat = info.finalImageFormat;
				reflectionMaps [info.actor] = std::move(new PlanarReflectionInstance(engineDevice, resourceSystem, info.generateIrradiance, renderFeatures, res, sceneSetLayout, skeletonSetLayout, terrainDescriptorSetLayout, maxFramesInFlight));
				*info.reflectionTexture = reflectionMaps[info.actor]->getReflectionDescriptor();
				*info.irradianceTexture = reflectionMaps[info.actor]->getIrradianceDescriptor();
			}
		}
		rebuildReflections.clear();
		for (int i = 0; i < destroyReflectionQueue.size(); i++) {
			auto& pair = destroyReflectionQueue[i];
			if (pair.second.getFence() == fence) continue; // from same frame
			if (pair.second.isFree()) {
				delete pair.first;
				destroyReflectionQueue.erase(destroyReflectionQueue.begin() + i);
				i--;
			}
		}
	}
	void PlanarReflectionSystem::render(FrameInfo& frameInfo) {
		for (auto& [actor, shadow] : reflectionMaps) {
			removedReflectionMaps.insert(actor);
		}
		auto viewCam = frameInfo.level->registry.view<Components::PlanarReflectionComponent, RENDER_ITER>();
		for (auto obj : viewCam) {
			ECS::Actor actor = { obj, frameInfo.level.get() };
			auto& r_transform = actor.getTransform();
			auto& r_component = actor.getComponent<Components::PlanarReflectionComponent>();
			bool actorReflectionMapPresent = reflectionMaps.find(actor) != reflectionMaps.end();
			bool recreateReflectionMap = false;
			if (actorReflectionMapPresent) {
				PlanarReflectionInstance* reflectionMap = reflectionMaps[actor];
				if (r_component.resolutionMultiplier * currentResolution != reflectionMap->getResolution() 
					|| r_component.reflectionFormat != reflectionMap->getFinalImage()->getAttachmentDescription().format) {
					recreateReflectionMap = true;
				} else {
					glm::mat4 realTransform = actor.getTransform().transformMatrix;
					realTransform = glm::rotate(realTransform, -glm::half_pi<float>(), glm::vec3(1.f, 0.f, 0.f));
					const glm::vec3 planeNormal = glm::normalize(realTransform[2]); // convert rotation to direction
					const float d = -glm::dot(planeNormal, glm::vec3(actor.getTransform().transformMatrix[3]));
					glm::vec4 clip = { planeNormal.x, planeNormal.z, planeNormal.y, d };

					reflectionMap->render(frameInfo.commandBuffer, frameInfo.sceneDescriptorSet, frameInfo.frameIndex,
						frameInfo.level->getPossessedCameraActor().getComponent<Components::CameraComponent>().ar, 
						frameInfo.level->getPossessedCameraActor(), clip, r_component.clipOffset, glm::mat3(realTransform),
						r_component.irradianceSampleRadius,
						frameInfo.level, frameInfo.renderObjects
					);
				}
			} else {
				recreateReflectionMap = true;
			}
			
			if (recreateReflectionMap) {
				SHARD3D_LOG("recreateReflectionMap rebuild flag for planar reflection actor {0}", actor.getTag());
				actorReflectionMapPresent = false;
				rebuildReflections.emplace_back(ReflectionComponentInfo_{ actor, r_component.irradiance, r_component.resolutionMultiplier, r_component.reflectionFormat, &r_component.reflectionShaderTexture_id, &r_component.irradianceShaderTexture_id }, true);
			}
			if (actorReflectionMapPresent) removedReflectionMaps.erase(actor);
		}
		for (auto& map : removedReflectionMaps) {
			Actor actor = { map, frameInfo.level.get() };
			if (!actor.isInvalid()) {
				if (actor.hasComponent<Components::PlanarReflectionComponent>()) {
					actor.getComponent<Components::PlanarReflectionComponent>().reflectionShaderTexture_id = 0;
					actor.getComponent<Components::PlanarReflectionComponent>().irradianceShaderTexture_id = 0;
				}
			}
			rebuildReflections.emplace_back(ReflectionComponentInfo_{ actor, false, 0, VK_FORMAT_UNDEFINED, nullptr, nullptr }, false);
		}
		removedReflectionMaps.clear();
	}

	void PlanarReflectionSystem::resize(glm::vec2 newScreenSize, VkDescriptorImageInfo depthSceneInfo) {
		this->depthSceneInfo = depthSceneInfo;
		currentResolution = newScreenSize;
		for (auto& [actor, reflection] : reflectionMaps) {
			vkDeviceWaitIdle(engineDevice.device());
			reflection->resize(newScreenSize * reflection->multiplier, depthSceneInfo);
		}
	}
}