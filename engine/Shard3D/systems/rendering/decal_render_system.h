#pragma once

#include "../../s3dstd.h"
#include "../../vulkan_abstr.h"
#include "../../core/misc/frame_info.h"	  
#include "../../core/ecs/level.h"
#include "../../core/ecs/actor.h"
#include "gbuffer_struct.h"
#include <unordered_map>
#include "../handlers/resource_system.h"

namespace Shard3D {
	class S3DRenderPass;
	class S3DRenderTarget;
	class S3DFramebuffer;

	struct DecalBufferInputData {
		S3DRenderTarget* dbuffer0; // diffuse.rgb (RGBA8)
		S3DRenderTarget* dbuffer1; // specular + glossiness + chromines (RGBA8)
		S3DRenderTarget* dbuffer2; // normal map tg space RGBA8
	};
	struct DecalInstanceData {
		glm::mat4 transform;
		glm::mat4 invTransform;
	};
	struct DecalBatchData {
		std::vector<S3DBuffer*> hostBuffers;
		std::vector<DecalInstanceData> cpuAllocatedInstanceData;
		S3DBuffer* deviceBuffer;
		VkDescriptorSet descriptor{};
		std::vector<entt::entity> instances;
		std::vector<uint32_t> eraseInstances;
		uint32_t materialID;
		uint32_t modelFlags;
	};
	class DecalBatchList {
	public:
		DecalBatchList(S3DDevice& device, ResourceSystem* resourceSystem);
		~DecalBatchList();

		DELETE_COPY(DecalBatchList)

		void updateBatches(FrameInfo& frameInfo);
		void runGarbageCollector(uint32_t frameIndex, VkFence fence);
		
		void forceUpdateDecalsEDITORONLY(sPtr<Level>& level);

		void setAllocatedMaterialInstances(AssetID material, uint32_t instanceCount, uint32_t priority);

		std::unordered_map<bool, std::map<uint32_t, std::unordered_map<AssetID, DecalBatchData>>> batches; // dynamic, priority, batches
		std::unordered_map<DBufferFlags, std::map<uint32_t, std::vector<AssetID>>> models; // include priority count
		uPtr<S3DDescriptorSetLayout>& getDescriptorSetLayout() {
			return decalDescriptorSetLayout;
		}
	private:
		std::vector<std::pair<S3DBuffer*, S3DDestructionObject>> destroyBufferQueue;
		std::vector<std::pair<VkDescriptorSet, S3DDestructionObject>> destroyDescriptorQueue;

		uint32_t getRequiredInstanceCount(uint32_t bufferInstanceCount, uint32_t requiredInstances);

		void resizeBatchBuffer(DecalBatchData& batch, uint32_t newSize, bool dynamic, uint32_t priority);
		void writeBatchDescriptor(DecalBatchData& batch);
		void createBatchBuffer(AssetID material, bool dynamic, uint32_t priority);
		void addToBatchBuffer(AssetID material, uint32_t& cachedIndex, uint32_t frameIndex, Actor actor);
		void removeFromBatchBuffer(AssetID material, uint32_t& cachedIndex, uint32_t frameIndex, Actor actor, bool oldIsDynamic, uint32_t oldPriority);
		void cleanupBatchBuffers(FrameInfo& frameInfo);

		Level* activeLevel{};

		VkFence currentFence{};
		std::unordered_map<entt::entity, std::tuple<bool, uint32_t, AssetID>> actorData;
		S3DDevice& engineDevice;
		ResourceSystem* resourceSystem;
		uPtr<S3DDescriptorSetLayout> decalDescriptorSetLayout;
	};

	inline namespace Systems {
		class DecalRenderSystem {
		public:
			DecalRenderSystem(S3DDevice& device, ResourceSystem* resourceSystem, VkDescriptorSetLayout globalSetLayout, SceneBufferInputData gbuffer);
			~DecalRenderSystem();

			DELETE_COPY(DecalRenderSystem)

			void renderDecals(FrameInfo& frameInfo);
			void applyDBuffer(FrameInfo& frameInfo);

			void resize(SceneBufferInputData gbuffer);

			DecalBufferInputData getDBufferAttachments() {
				return {
					dbuffer0,
					dbuffer1,
					dbuffer2
				};
			}
		private:

			void createRenderPasses(SceneBufferInputData gbuffer);
			void createFramebuffers(SceneBufferInputData gbuffer);

			void writeDescriptors(VkDescriptorImageInfo depthImageInfo, VkDescriptorImageInfo gbuffer4ImageInfo);

			void createPipelineLayouts(VkDescriptorSetLayout globalSetLayout, VkDescriptorSetLayout decalSetLayout);
			void createPipelines();
			void createDBufferPipelines();

			S3DRenderTarget* dbuffer0; // diffuse
			S3DRenderTarget* dbuffer1; // specular, glossiness, reflectivity
			S3DRenderTarget* dbuffer2; // normal

			S3DFramebuffer* framebufferDBuffer;
			S3DRenderPass* renderPassDBuffer;

			S3DFramebuffer* framebufferDBufferApplyDR;
			S3DFramebuffer* framebufferDBufferApplySG;
			S3DRenderPass* renderPassDBufferApplyDR;
			S3DRenderPass* renderPassDBufferApplySG;

			uPtr<S3DPipelineLayout> dbufferApplyPipelineLayout{};
			uPtr<S3DGraphicsPipeline> dbufferApplyPipelineDR{};
			uPtr<S3DGraphicsPipeline> dbufferApplyPipelineSG{};
			uPtr<S3DDescriptorSetLayout> dbufferApplyDescriptorSetLayout{};
			VkDescriptorSet dbufferApplyDescriptor{};


			//TEMPORARY
			uPtr<S3DDescriptorSetLayout> decalDescriptorSetLayout;

			uPtr<S3DDescriptorSetLayout> depthDataDescriptorSetLayout{};
			VkDescriptorSet depthDataDescriptor{};

			uPtr<S3DPipelineLayout> decalPipelineLayout{};
			// flag 2048 marks dynamic
			std::map<DBufferFlags, uPtr<S3DGraphicsPipeline>> decalPipelines{};

			S3DDevice& engineDevice;
			ResourceSystem* resourceSystem;
		};
	}
}