#pragma once
#include "../../s3dstd.h"
#include "../../vulkan_abstr.h"
#include "../../core/misc/frame_info.h"	  
#include "../../core/ecs/level.h"
#include "../../core/ecs/components.h"
#include "../../core/rendering/framebuffer.h"
#include "../../core/rendering/render_target.h"
#include "../../core/rendering/render_pass.h"

namespace Shard3D {
	inline namespace Systems {
		class ShadowMappingSystem;
		struct LPVTexture3D {
			LPVTexture3D(S3DDevice& device, glm::ivec3 resolution, VkImageLayout imageLayout, VkFormat format, VkFilter filter, bool rendertarget = true);
			~LPVTexture3D();

			S3DDevice& device;

			VkDescriptorImageInfo getDescriptor() {
				return {
					voxelDataSampler,
					voxelDataImageView,
					voxelImageLayout
				};
			}

			ShaderResourceIndex optionalBindless;
			VkImage voxelDataImage{};
			VkDeviceMemory voxelDataImageMemory{};
			VkImageView voxelDataImageView{};
			VkSampler voxelDataSampler{};
			VkImageLayout voxelImageLayout{};
		};

		class FastComputeShader {
		public:
			FastComputeShader(S3DDevice& device, glm::vec2 extent, S3DShader fragmentShader, uint32_t pushSize, std::vector<VkDescriptorSetLayout> descriptorSetLayouts);
			~FastComputeShader();
			void push(VkCommandBuffer commandBuffer, const VoidRef<128>& data);
			void dispatch(VkCommandBuffer commandBuffer);
			VkPipelineLayout getPipelineLayout() {
				return pipelineLayout->getPipelineLayout();
			}
		private:
			S3DPushConstant pushC;
			S3DRenderPass* renderPass;
			S3DFramebuffer* framebuffer;
			uPtr<S3DGraphicsPipeline> pipeline;
			uPtr<S3DPipelineLayout> pipelineLayout;
		};
		struct RSMInjectionData {
			std::vector<VkDescriptorSet> descriptorSet;
		};

		class LightPropagationVolumeSystem {
		public:
			LightPropagationVolumeSystem(S3DDevice& device, ResourceSystem* resourceSystem, ShadowMappingSystem* shadowMappingSystem);
			~LightPropagationVolumeSystem();
			void render(FrameInfo& frameInfo);
			void update(FrameInfo& frameInfo, SceneBuffers& scene);
			void runGarbageCollector(uint32_t frameIndex, VkFence fence);

			std::array<ShaderResourceIndex, 3> getLPVDescriptorIndices(uint32_t cascade);
		private:
			void clearVolumes(FrameInfo& frameInfo);
			void injectRSMData(FrameInfo& frameInfo, uint32_t cascade);
			void injectVPLData(FrameInfo& frameInfo, uint32_t cascade);
			void averageVolume(FrameInfo& frameInfo, uint32_t cascade);
			void propagateVolume(FrameInfo& frameInfo, uint32_t cascade);
			void blendVolumes(FrameInfo& frameInf, uint32_t cascade);

			void destroyVoxels();
			void createVoxels(glm::ivec3 resolution);
			void createDescriptors();
			void createPasses();

			std::array<LPVTexture3D*, 3> lpvSH_Red;
			std::array<LPVTexture3D*, 3> lpvSH_Green;
			std::array<LPVTexture3D*, 3> lpvSH_Blue;
			LPVTexture3D* lpv_InoutInjProp_Red0;
			LPVTexture3D* lpv_InoutInjProp_Red1;
			LPVTexture3D* lpv_InoutInjProp_Green0;
			LPVTexture3D* lpv_InoutInjProp_Green1;
			LPVTexture3D* lpv_InoutInjProp_Blue0;
			LPVTexture3D* lpv_InoutInjProp_Blue1;

			LPVTexture3D* lpv_shSum;

			VkDescriptorSet voxelDescriptorSet{};
			VkDescriptorSet shSumDescriptorSet{};
			uPtr<S3DDescriptorSetLayout> rsmInputDescriptorSetLayout;
			uPtr<S3DDescriptorSetLayout> voxelDescriptorSetLayout;
			uPtr<S3DDescriptorSetLayout> shSumDescriptorSetLayout;
			uPtr<S3DDescriptorSetLayout> vplDescriptorSetLayout;

			std::unordered_map<entt::entity, RSMInjectionData*> reflectiveShadowMaps;
			std::vector<std::pair<RSMInjectionData*, S3DDestructionObject>> rsmDestructionQueue{};
			std::vector<uPtr<S3DBuffer>> vplBuffers;
			std::vector<VkDescriptorSet> vplDescriptors;

			S3DRenderPass* injectionRenderPass;
			S3DFramebuffer* injectionFramebuffer;
			S3DRenderTarget* injectionRenderTarget_SH_Red;
			S3DRenderTarget* injectionRenderTarget_SH_Green;
			S3DRenderTarget* injectionRenderTarget_SH_Blue;
			S3DRenderTarget* injectionRenderTarget_SH_Sum;
			S3DPipelineLayout* injectionRSMPipelineLayout;
			S3DPipelineLayout* injectionVPLPipelineLayout;
			S3DGraphicsPipeline* injectionRSMPipeline;
			S3DGraphicsPipeline* injectionVPLPipeline;

			FastComputeShader* clearPass;
			FastComputeShader* averagingPass;
			FastComputeShader* propagationPass;
			FastComputeShader* blendPass;

			S3DDevice& engineDevice;
			ResourceSystem* resourceSystem;

			uint32_t vplCount = 0;

			ShadowMappingSystem* shadowMappingSystem;

			VkFence currentFence;

			glm::ivec3 voxelResolution;
			std::array<glm::vec3, 3> lpvCenter{};
			std::array<glm::ivec3, 3> lpvCenterVx{};
			std::array<glm::ivec3, 3> lpvCenterVxpf{};
		};
	}
}