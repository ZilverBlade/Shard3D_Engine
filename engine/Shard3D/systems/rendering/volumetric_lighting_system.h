#pragma once

#include "../../s3dstd.h"
#include "../../vulkan_abstr.h"
#include "../../core/misc/frame_info.h"	  
#include "../../core/ecs/level.h"
#include "../../core/ecs/components.h"
#include "../handlers/render_list.h"
#include "gbuffer_struct.h"
#include "../post_fx/post_processing_fx.h"
#include "../../core/ecs/actor.h" 

namespace Shard3D {
	inline namespace Systems {
		class VolumetricLightingSystem {
	  	public:
			VolumetricLightingSystem(S3DDevice& device, ResourceSystem* resourceSystem, glm::vec2 resolution, const SceneBufferInputData& gbuffer, VkRenderPass compositionRenderPass, VkDescriptorSetLayout globalSetLayout, VkDescriptorSetLayout sceneSetLayout);
			~VolumetricLightingSystem();

			DELETE_COPY(VolumetricLightingSystem)

			void render(FrameInfo& frameInfo);
			void blur(FrameInfo& frameInfo);
			void composite(FrameInfo& frameInfo);
			void resize(glm::vec2 newSize, const SceneBufferInputData& gbuffer);
		private:
			void drawLight(FrameInfo& frameInfo, uint32_t lightType, uint32_t lightIndex);

			void createAttachments(glm::vec2 resolution);
			void createRenderPass();
			void createFramebuffer();
			void createDescriptorSetLayout();
			void createPipelines(VkDescriptorSetLayout globalSetLayout, VkDescriptorSetLayout sceneSetLayout, VkRenderPass renderPass);
			void createBlurPasses();

			void writeDescriptors(VkDescriptorImageInfo depthImageInfo);

			uPtr<S3DGraphicsPipeline> volumetricLightingPointLightPipeline;
			uPtr<S3DGraphicsPipeline> volumetricLightingSpotLightPipeline;
			uPtr<S3DGraphicsPipeline> volumetricLightingDirectionalLightPipeline;
			uPtr<S3DPipelineLayout> volumetricLightingPipelineLayout;
			S3DPushConstant volumetricLightingPush;

			uPtr<S3DGraphicsPipeline> compositingPipeline;
			uPtr<S3DPipelineLayout> compositingPipelineLayout;

			VkDescriptorSet compositingInputDescriptorSet{};
			uPtr<S3DDescriptorSetLayout> compositingInputDescriptorSetLayout;
			VkDescriptorSet volumetricLightingInputDescriptorSet{};
			uPtr<S3DDescriptorSetLayout>  volumetricLightingInputDescriptorSetLayout;

			S3DRenderPass* volumetricLightingRenderPass;
			S3DRenderTarget* volumetricLightAttachment;
			S3DFramebuffer* volumetricLightFramebuffer;

			uPtr<PostProcessingEffect> blurPassH;
			uPtr<PostProcessingEffect> blurPassV;

			ResourceSystem* resourceSystem;
			S3DDevice& engineDevice;
		};
	}
}