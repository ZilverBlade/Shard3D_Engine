#pragma once
#include "../../s3dstd.h"
#include "../../vulkan_abstr.h"
#include "../../core/misc/frame_info.h"	  
#include "../../core/ecs/level.h"
#include "../../core/asset/importer_model.h"
#include "../../core/asset/mesh3d.h"

namespace Shard3D {
	inline namespace Systems {
		struct BrushableMesh {
			std::vector<glm::vec2> points; // on the XZ plane
			float uvScalability;
		};
		struct BrushCompositeRoad {
			Resources::Mesh3D* straight;
			Resources::Mesh3D* curveR;
			Resources::Mesh3D* branchT;
			Resources::Mesh3D* cross;
			float size = 2.0f;
		};
		class BrushData {
		public:
			BrushData(const BrushCompositeRoad& composite);
			void addNode(glm::vec3 node, uint32_t index = -1);
			void rmvNode(uint32_t index);
			void build(S3DDevice& device);

			const std::vector<Resources::Mesh3D*>& getMeshes() {
				return brushedMeshes;
			}
			const std::vector<glm::mat4>& getTransforms() {
				return transforms;
			}
			const BrushCompositeRoad& getComposite() {
				return composite;
			}
		private:
			BrushCompositeRoad composite;
			std::vector<Resources::Mesh3D*> brushedMeshes;
			std::vector<glm::mat4> transforms;
			std::vector<glm::vec3> pathNodes;
		};

		class BrushSystem {
		public:
			BrushSystem(S3DDevice& device, VkRenderPass renderPass, ResourceSystem* resourceSystem, VkDescriptorSetLayout globalSetLayout);
			~BrushSystem();

			BrushSystem(const BrushSystem&) = delete;
			BrushSystem& operator=(const BrushSystem&) = delete;

			void render(FrameInfo& frameInfo);
			void renderPreviewTiles(FrameInfo& frameInfo);

			sPtr<BrushData>& getBrush() {
				return brushDatas.at(0);
			}
		private:
			void createPipelineLayout(VkDescriptorSetLayout globalSetLayout);
			void createPipeline(VkRenderPass renderPass);

			S3DDevice& engineDevice;

			std::vector<sPtr<BrushData>> brushDatas;

			uPtr<S3DGraphicsPipeline> graphicsPipeline;
			S3DPushConstant push;
			uPtr<S3DPipelineLayout> pipelineLayout;
		};
	}
}