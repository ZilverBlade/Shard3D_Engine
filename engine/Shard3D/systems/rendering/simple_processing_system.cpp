#include "simple_processing_system.h"

namespace Shard3D::Systems {
	SimpleProcessingSystem::SimpleProcessingSystem(
		S3DDevice& device, 
		S3DShader shdr, 
		VkDescriptorImageInfo* imageInfo,
		glm::vec2 res,
		VkFormat format,
		void* buffer, 
		size_t bufferSize
	) : engineDevice(device), shader(shdr), resolution(res), format(format) {
		S3DDescriptorPool::Builder pbuilder = S3DDescriptorPool::Builder(device)
			.setMaxSets(2)
			.addPoolSize(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1)
			.addPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1);
		S3DDescriptorSetLayout::Builder dbuilder = S3DDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT);
		if (buffer) {
			dbuilder.addBinding(1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT);
		}
		descriptorLayout = dbuilder.build();
		localPool = pbuilder.setPoolFlags(VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT).build();

		S3DDescriptorWriter writer = S3DDescriptorWriter(*descriptorLayout, *localPool)
			.writeImage(0, imageInfo);
		if (buffer) {
			shaderBuffer = make_uPtr<S3DBuffer>(
				device,
				bufferSize,
				1,
				VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
				);
			shaderBuffer->map();
			shaderBuffer->writeToBuffer(buffer);
			shaderBuffer->flush();
			auto bufferInfo = shaderBuffer->descriptorInfo();
			writer.writeBuffer(1, &bufferInfo);
		}

		writer.build(descriptorSet);

		createRenderPass();
		createPipelineLayout();
		createPipelines(renderpass->getRenderPass());
	}

	SimpleProcessingSystem::~SimpleProcessingSystem() {
		delete framebuffer;
		delete renderpass; 
		vkDestroyPipelineLayout(engineDevice.device(), pipelineLayout, nullptr);
	}

	void SimpleProcessingSystem::process(VkCommandBuffer commandBuffer) {
		renderpass->beginRenderPass(commandBuffer, framebuffer);
		shaderPipeline->bind(commandBuffer);
		vkCmdBindDescriptorSets(
			commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			pipelineLayout,
			0,
			1,
			&descriptorSet,
			0, 
			nullptr
		);
		vkCmdDraw(commandBuffer, 6, 1, 0, 0);
		renderpass->endRenderPass(commandBuffer);
	}

	void SimpleProcessingSystem::createRenderPass() {
		glm::ivec3 size = { resolution, 1 };

		std::vector<AttachmentInfo> attachmentInfos{};
		std::vector<S3DRenderTarget*> attachments{};
		
		render_attachment = make_uPtr<S3DRenderTarget>(engineDevice, S3DRenderTargetCreateInfo{
			format,
			VK_IMAGE_ASPECT_COLOR_BIT,
			VK_IMAGE_VIEW_TYPE_2D,
			size,
			VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT,
			VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
			S3DRenderTargetType::Color,
			false,
			VK_SAMPLE_COUNT_1_BIT
			}
		);
		attachmentInfos.push_back({ render_attachment.get(), VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE });
		attachments.push_back(render_attachment.get());

		renderpass = new S3DRenderPass(engineDevice, attachmentInfos );
		framebuffer = new S3DFramebuffer(engineDevice, renderpass, attachments );
	}

	void SimpleProcessingSystem::createPipelineLayout() {
		std::vector<VkDescriptorSetLayout> descriptorSetLayouts{
			descriptorLayout->getDescriptorSetLayout()
		};

		VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
		pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		pipelineLayoutInfo.setLayoutCount = (uint32_t)descriptorSetLayouts.size();
		pipelineLayoutInfo.pSetLayouts = descriptorSetLayouts.data();
		pipelineLayoutInfo.pushConstantRangeCount = 0;
		pipelineLayoutInfo.pPushConstantRanges = nullptr;
		VK_VALIDATE(vkCreatePipelineLayout(engineDevice.device(), &pipelineLayoutInfo, nullptr, &pipelineLayout), "failed to create pipeline layout!");
	}

	void SimpleProcessingSystem::createPipelines(VkRenderPass renderPass) {
		SHARD3D_ASSERT(pipelineLayout != nullptr && "Cannot create pipeline before pipeline layout");	

		S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo();
		pipelineConfig.setCullMode(VK_CULL_MODE_BACK_BIT);
		pipelineConfig.disableDepthTest();

		pipelineConfig.pipelineLayout = pipelineLayout;
		pipelineConfig.renderPass = renderPass;

		shaderPipeline = make_uPtr<S3DGraphicsPipeline>(
			engineDevice, std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/fullscreen_quad_flipped.vert", "materials" },
				shader,
			},
			pipelineConfig
		);
	}
}