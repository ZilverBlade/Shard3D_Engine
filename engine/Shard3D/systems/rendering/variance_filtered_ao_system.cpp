/*
#include "../../s3dpch.h" 

#include "variance_filtered_ao_system.h"
#include "../../core/vulkan_api/graphics_pipeline.h"
#include "../../core/vulkan_api/descriptors.h"
#include "../../ecs.h"
#include <glm/ext/matrix_clip_space.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtx/projection.hpp>
#include "../../core/misc/engine_settings.h"
#include "../handlers/project_system.h"

namespace Shard3D::Systems {
	struct AOPushConstants {
		glm::mat4 projection;
		glm::mat4 model;
	};

	VarianceFilteredAmbientOcclusionSystem::VarianceFilteredAmbientOcclusionSystem(S3DDevice& device) : engineDevice(device) {
		for (auto& class_ : ProjectSystem::getEngineSettings().allowedMaterialPermutations) {
			if (!(class_ & SurfaceMaterialPipelineClassPermutationOptions_Translucent))
				opaqueMaterials.push_back(class_);
			else
				translucentMaterials.push_back(class_);
		}

		createPipelineLayout();
	}
	VarianceFilteredAmbientOcclusionSystem::~VarianceFilteredAmbientOcclusionSystem() {
		for (auto& [id, map] : shadowMaps) {
			delete map; // free the shadow maps
		}

		delete opaque_var_pipelineLayout;
		delete masked_var_pipelineLayout;
		delete opaque_den_pipelineLayout;
		delete masked_den_pipelineLayout;


	}

	AmbientOcclusionMap* VarianceFilteredAmbientOcclusionSystem::createSkyLightAmbientOcclusionMap(int resolution, ShaderResourceIndex* outID) {
		AmbientOcclusionMap* aoMap = new AmbientOcclusionMap(engineDevice.resourceSystem);

		*outID = aoMap->shadowMap_id;
		aoMap->depthAttachment = new S3DRenderTarget(engineDevice, {
			VK_FORMAT_D16_UNORM,
			VK_IMAGE_ASPECT_DEPTH_BIT,
			glm::ivec3(resolution, resolution, 1),
			VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
			VK_IMAGE_LAYOUT_GENERAL, //VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_OPTIMAL,
			VK_SAMPLE_COUNT_1_BIT
			}, S3DRenderTargetOptions_Depth
		);

		AttachmentInfo depthAttachmentInfo{};
		depthAttachmentInfo.framebufferAttachment = aoMap->depthAttachment;
		depthAttachmentInfo.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		depthAttachmentInfo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		depthAttachmentInfo.clear.color = { 1.f };

		aoMap->depthRenderPass = new S3DRenderPass(
			engineDevice, {
			depthAttachmentInfo
		});

		aoMap->depthFramebuffer = new S3DFramebuffer(engineDevice, aoMap->depthRenderPass->getRenderPass(), { aoMap->depthAttachment });


		aoMap->densityAttachment = new S3DRenderTarget(engineDevice, {
			VK_FORMAT_R32_SFLOAT,
			VK_IMAGE_ASPECT_COLOR_BIT,
			glm::ivec3(resolution, resolution, 1),
			VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
			VK_IMAGE_LAYOUT_GENERAL, //VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
			VK_SAMPLE_COUNT_1_BIT
			}, S3DRenderTargetOptions_Color
		);

		AttachmentInfo densityAttachmentInfo{};
		densityAttachmentInfo.framebufferAttachment = aoMap->densityAttachment;
		densityAttachmentInfo.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		densityAttachmentInfo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		densityAttachmentInfo.clear.color = { 0.f };

		aoMap->densityRenderPass = new S3DRenderPass(
			engineDevice, {
			densityAttachmentInfo
			});

		aoMap->densityFramebuffer = new S3DFramebuffer(engineDevice, aoMap->densityRenderPass->getRenderPass(), { aoMap->densityAttachment });

		aoMap->vfaoFilter = make_uPtr<PostProcessingEffect>(
			engineDevice, glm::vec2(resolution),
			S3DShader{ ShaderType::Fragment,  "resources/shaders/worldfx/vfao_filter.frag", "worldfx" },
			std::vector<S3DRenderTarget*>{ aoMap->depthAttachment, aoMap->densityAttachment },
			VK_FORMAT_R16G16B16A16_UNORM
		);

		SmartDescriptorWriter(*engineDevice.resourceSystem->getDescriptor())
			.writeImage(BINDLESS_SAMPLER_BINDING, aoMap->shadowMap_id,
				&aoMap->vfaoFilter->getAttachment()->getDescriptor()
			).build();

		SHARD3D_ASSERT(opaque_den_pipelineLayout != nullptr && "Cannot create pipeline before pipeline layout");
		SHARD3D_ASSERT(masked_den_pipelineLayout != nullptr && "Cannot create pipeline before pipeline layout");
		SHARD3D_ASSERT(opaque_var_pipelineLayout != nullptr && "Cannot create pipeline before pipeline layout");
		SHARD3D_ASSERT(masked_var_pipelineLayout != nullptr && "Cannot create pipeline before pipeline layout");

		S3DGraphicsPipelineConfigInfo opaque_v_pipelineConfig{};
		S3DGraphicsPipeline::pipelineConfig(opaque_v_pipelineConfig)
			.defaultS3DGraphicsPipelineConfigInfo()
			.enableVertexDescriptions(VertexDescriptionOptions_Position);
		S3DGraphicsPipelineConfigInfo opaque_d_pipelineConfig{};
		S3DGraphicsPipeline::pipelineConfig(opaque_d_pipelineConfig)
			.defaultS3DGraphicsPipelineConfigInfo()
			.enableVertexDescriptions(VertexDescriptionOptions_Position)
			.enableAlphaBlending(VK_BLEND_OP_ADD)
			.disableDepthTest();
		opaque_d_pipelineConfig.colorBlendAttachments[0].srcColorBlendFactor = VK_BLEND_FACTOR_SRC_COLOR;
		opaque_d_pipelineConfig.colorBlendAttachments[0].dstColorBlendFactor = VK_BLEND_FACTOR_SRC_COLOR;
		opaque_v_pipelineConfig.renderPass = aoMap->depthRenderPass->getRenderPass();
		opaque_d_pipelineConfig.renderPass = aoMap->densityRenderPass->getRenderPass();
		opaque_v_pipelineConfig.pipelineLayout = opaque_var_pipelineLayout->getPipelineLayout();
		opaque_d_pipelineConfig.pipelineLayout = opaque_den_pipelineLayout->getPipelineLayout();
		aoMap->opaque_var_pipeline = new S3DGraphicsPipeline(
			engineDevice, {
				S3DShader{ ShaderType::Vertex, "resources/shaders/worldfx/vfao_opaque.vert", "worldfx" }
			},
			opaque_v_pipelineConfig
		);
		aoMap->masked_var_pipeline = new S3DGraphicsPipeline(
			engineDevice, {
				S3DShader{ ShaderType::Vertex, "resources/shaders/worldfx/vfao_opaque.vert", "worldfx" } // opaque for now
			},
			opaque_v_pipelineConfig
		);
		aoMap->opaque_den_pipeline = new S3DGraphicsPipeline(
			engineDevice, {
				S3DShader{ ShaderType::Vertex, "resources/shaders/worldfx/vfao_opaque.vert", "worldfx" },
				S3DShader{ ShaderType::Pixel, "resources/shaders/worldfx/vfao_opaque_density.frag", "worldfx" }
			},
			opaque_d_pipelineConfig
		);
		aoMap->masked_den_pipeline = new S3DGraphicsPipeline(
			engineDevice, {
				S3DShader{ ShaderType::Vertex, "resources/shaders/worldfx/vfao_opaque.vert", "worldfx" }, // opaque for now
				S3DShader{ ShaderType::Pixel, "resources/shaders/worldfx/vfao_opaque_density.frag", "worldfx" }
			},
			opaque_d_pipelineConfig
		);
		return aoMap;
	}
	void VarianceFilteredAmbientOcclusionSystem::createPipelineLayout() {
		VkPushConstantRange range{};
		range.size = sizeof(AOPushConstants);
		range.stageFlags = VK_SHADER_STAGE_ALL_GRAPHICS;
		opaque_var_pipelineLayout = new S3DPipelineLayout(engineDevice, { range }, {});
		masked_var_pipelineLayout = new S3DPipelineLayout(engineDevice, { range }, {}); // missing descriptor
		opaque_den_pipelineLayout = new S3DPipelineLayout(engineDevice, { range }, {});
		masked_den_pipelineLayout = new S3DPipelineLayout(engineDevice, { range }, {}); // missing descriptor
	}

	void VarianceFilteredAmbientOcclusionSystem::runGarbageCollector(sPtr<Level>& level) {

	}	

	void VarianceFilteredAmbientOcclusionSystem::render(FrameInfo& frameInfo) {
		//frameInfo.level->registry.view<Components::SkyLightComponent, Components::IsVisibileComponent>().each([&](Components::SkyLightComponent env, auto& visibility) {
		//	if (!madeShadowMap) {
		//		shadowMaps[0] = createSkyLightAmbientOcclusionMap(2048, &test);
		//		madeShadowMap = true;
		//	}
		//	
		//	float clipBounds = 50.f;
		//	shadowMaps[0]->camera = S3DCamera{};
		//	shadowMaps[0]->camera.setViewYXZ(frameInfo.camera.getPosition(), { glm::half_pi<float>(), 0.f, 0.f });
		//	shadowMaps[0]->camera.setOrthographicProjection(
		//		clipBounds,
		//		-clipBounds,
		//		-clipBounds,
		//		clipBounds,
		//		-220.000f, 80.000f
		//	);
		//
		//	glm::mat4 lightSpaceMatrix = shadowMaps[0]->camera.getProjection() * shadowMaps[0]->camera.getView();
		//
		//	shadowMaps[0]->depthRenderPass->beginRenderPass(frameInfo.commandBuffer, shadowMaps[0]->depthFramebuffer);
		//	shadowMaps[0]->opaque_var_pipeline->bind(frameInfo.commandBuffer);
		//	AOPushConstants push{};
		//	push.projection = lightSpaceMatrix;
		//	for (SurfaceMaterialClassFlags& material : opaqueMaterials) {
		//		for (auto& [uuid, mesh] : frameInfo.level->getRenderList()->getSurfaceMaterialRenderingList()[material]) {
		//			auto& mesh_r = frameInfo.resourceSystem->retrieveMesh(mesh.mesh->mesh);
		//			mesh_r->bind(frameInfo.commandBuffer, VertexBindings::Position);
		//			push.model = mesh.mesh->transform;
		//			vkCmdPushConstants(
		//				frameInfo.commandBuffer,
		//				opaque_var_pipelineLayout->getPipelineLayout(),
		//				VK_SHADER_STAGE_ALL_GRAPHICS,
		//				0,
		//				sizeof(AOPushConstants),
		//				&push
		//			);
		//			for (int index : mesh.material_indices) {
		//				mesh_r->draw(frameInfo.commandBuffer, index);
		//			}
		//		}
		//	}	
		//	shadowMaps[0]->depthRenderPass->endRenderPass(frameInfo.commandBuffer);
		//	shadowMaps[0]->densityRenderPass->beginRenderPass(frameInfo.commandBuffer, shadowMaps[0]->densityFramebuffer);
		//	shadowMaps[0]->opaque_den_pipeline->bind(frameInfo.commandBuffer);
		//	for (SurfaceMaterialClassFlags& material : opaqueMaterials) {
		//		for (auto& [uuid, mesh] : frameInfo.level->getRenderList()->getSurfaceMaterialRenderingList()[material]) {
		//			auto& mesh_r = frameInfo.resourceSystem->retrieveMesh(mesh.mesh->mesh);
		//			mesh_r->bind(frameInfo.commandBuffer, VertexBindings::Position);
		//			push.model = mesh.mesh->transform;
		//			vkCmdPushConstants(
		//				frameInfo.commandBuffer,
		//				opaque_den_pipelineLayout->getPipelineLayout(),
		//				VK_SHADER_STAGE_ALL_GRAPHICS,
		//				0,
		//				sizeof(AOPushConstants),
		//				&push
		//			);
		//			for (int index : mesh.material_indices) {
		//				mesh_r->draw(frameInfo.commandBuffer, index);
		//			}
		//		}
		//	}
		//	shadowMaps[0]->densityRenderPass->endRenderPass(frameInfo.commandBuffer);
		//	shadowMaps[0]->vfaoFilter->render(frameInfo, static_cast<glm::vec2>(shadowMaps[0]->vfaoFilter->getAttachment().colorAttachment->getDimensions()));
		//});
	}
}
*/