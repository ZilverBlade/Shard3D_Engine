#pragma once
#include "../../s3dstd.h"
#include "../../vulkan_abstr.h"
#include "../../core/misc/frame_info.h"	  
#include "../../core/ecs/level.h"
#include "../../core/ecs/components.h"
#include "../../core/rendering/framebuffer.h"
#include "../../core/rendering/render_target.h"
#include "../../core/rendering/render_pass.h"

namespace Shard3D {
	



	struct VXPathTracedGIConfig {
		glm::vec3 energyConservationFactor = glm::vec3(1.0f);
		float rayMarchThresholdDistance = 0.4f;
		uint32_t maxBounces = 1;
	};
	class VoxelizerSystem {
	public:
		VoxelizerSystem(S3DDevice& engineDevice, ResourceSystem* resourceSystem, VkDescriptorSetLayout sceneBufferDescriptorLayout, glm::ivec3 voxelResolution, glm::mat4 transform, glm::vec3 extent, bool useRayTracing);

		void generateSceneData(VkCommandBuffer commandBuffer,sPtr<Level>& level, RenderList* renderList);
		void generateGlobalIllumination(VkCommandBuffer commandBuffer,sPtr<Level>& level, RenderList* renderList, VkDescriptorSet sceneBuffer, uint32_t sampleCount, const VXPathTracedGIConfig& config);

		void clearGlobalIllumination(VkCommandBuffer commandBuffer);
		void generateGlobalIlluminationProgressive(VkCommandBuffer commandBuffer, sPtr<Level>& level, RenderList* renderList, VkDescriptorSet sceneBuffer, uint32_t samplesPerFrame, const VXPathTracedGIConfig& config);

		VkDescriptorImageInfo getVoxelData() {
			return VkDescriptorImageInfo{
				voxelGISampler,
				voxelGIImageView,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
			};
		}

		void recreatePipelines(VkDescriptorSetLayout sceneBufferDescriptorLayout) {
			createPipelines(sceneBufferDescriptorLayout);
		}
	private:
		void createVoxelImage(glm::ivec3 resolution, VkFormat format, VkImage& image, VkDeviceMemory& memory, VkImageView& imageViewStorage, VkImageView* imageViewSampler, VkSampler& sampler, VkFilter filter);
		void createAccelerationStructure();

		void createPasses();
		void createBuffers();
		void createDescriptors();

		void createPipelines(VkDescriptorSetLayout sceneBufferDescriptorLayout);

		VkDescriptorSet vxIlluminateDescriptor{};
		VkDescriptorSet vxGenDescriptor{};
		uPtr<S3DDescriptorSetLayout> vxIlluminateDescriptorLayout;
		uPtr<S3DDescriptorSetLayout> vxGenDescriptorLayout;

		VkDescriptorSet globalUBODescriptor{};
		uPtr<S3DDescriptorSetLayout> globalUBODescriptorLayout;
		uPtr<S3DBuffer> globalUBO;

		uPtr<S3DDescriptorPool> localPool;

		S3DRenderPass* vxGenRenderPass;
		S3DFramebuffer* vxGenFramebuffer;
		S3DRenderTarget* vxGenFramebufferAttachment;

		uPtr<S3DGraphicsPipeline> vxGenPipeline;
		uPtr<S3DPipelineLayout> vxGenPipelineLayout;
		uPtr<S3DGraphicsPipeline> vxClearPipeline;
		uPtr<S3DPipelineLayout> vxClearPipelineLayout;
		uPtr<S3DComputePipeline> vxGIShaderPipeline;
		uPtr<S3DPipelineLayout> vxGIShaderPipelineLayout;

		S3DPushConstant pushVXGI;
		S3DPushConstant pushVXGEN;

		VkImage voxelDiffuseDataImage;
		VkDeviceMemory voxelDiffuseDataImageMemory;
		VkImageView voxelDiffuseDataStorageImageView;
		VkImageView voxelDiffuseDataSamplerImageView;
		VkSampler voxelDiffuseDataSampler;

		VkImage voxelNormalDataImage;
		VkDeviceMemory voxelNormalDataImageMemory;
		VkImageView voxelNormalDataStorageImageView;
		VkImageView voxelNormalDataSamplerImageView;
		VkSampler voxelNormalDataSampler;

		VkImage voxelGIImage;
		VkDeviceMemory voxelGIImageMemory;
		VkImageView voxelGIImageView;
		VkSampler voxelGISampler;

		S3DDevice& engineDevice;
		ResourceSystem* resourceSystem;

		glm::ivec3 voxelResolution;
		glm::mat4 transform;
		glm::vec3 extent;

		bool useRayTracing;
		VkAccelerationStructureKHR BLAS_rt;
		VkAccelerationStructureKHR TLAS_rt;

		uint32_t iterations = 0;
	};
}