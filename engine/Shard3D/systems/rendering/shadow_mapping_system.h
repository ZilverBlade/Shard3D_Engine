#pragma once
#include "../../s3dstd.h"
#include "../../vulkan_abstr.h"
#include "../../core/misc/frame_info.h"	  
#include "../../core/ecs/actor.h"	  
#include "../../core/rendering/render_pass.h"
#include "../../core/asset/assetmgr.h"
#include "../post_fx/post_processing_fx.h"
#include "../../core/vulkan_api/destruction.h"
#include "../handlers/resource_system.h"
#include "../handlers/render_list.h"

namespace Shard3D {
	inline namespace Systems {
		class LightPropagationVolumeSystem;
		struct LightSpaceMatricesStruct {
			glm::mat4 matrices[6]{};
		};
		enum class ShadowMapLight {
			None, Directional, Spot, Point
		};
		enum class ShadowMapType {
			PCF, Variance
		};
		enum class ShadowMapConvention {
			None, Conventional, Paraboloid, DualParaboloid, Cube, PSSM2, PSSM3, PSSM4 // only conventional and cube supported atm 
		};

		struct ReflectiveShadowMap {
			S3DRenderTarget* fluxMap;
			S3DRenderTarget* normalMap;
			void downSampleMaps(VkCommandBuffer commandBuffer, S3DDevice& device);
		};

		struct ShadowMapPipelinePermutations {
			DELETE_COPY(ShadowMapPipelinePermutations)
			ShadowMapPipelinePermutations(S3DDevice& device,
				const std::string& shadowGenVertFile, const S3DShaderSpecialization& specializationVert, const std::string& shadowOutputVertFile,
				const std::string& shadowGenFragFile, const S3DShaderSpecialization& specializationFrag, const std::string& shadowOutputFragFile,
				S3DGraphicsPipelineConfigInfo& pipelineConfigInfo, const std::vector<std::string>& definitions, 
				std::unordered_map<SurfaceMaterialPipelineClassPermutationFlags, VkPipelineLayout>& pipelineLayouts, bool requireFragmentShader
			);
			~ShadowMapPipelinePermutations();

			std::unordered_map<SurfaceMaterialPipelineClassPermutationFlags, S3DGraphicsPipeline*> pipelines;
		};
		class ShadowMap {
		public:
			DELETE_COPY(ShadowMap);

			ShadowMap(ResourceSystem* resourceSystem_, ShadowMapType t, ShadowMapConvention c, S3DDevice& device, uPtr<S3DDescriptorSetLayout>& shadowMapBufferDescriptorSetLayout);

			virtual ~ShadowMap();
			virtual void process(FrameInfo& frameInfo, int framebufferIndex) {}
			virtual S3DRenderTarget* getOpaqueAttachment() const {
				return nullptr;
			}
			virtual S3DRenderTarget* getTranslucentAttachment() const {
				return nullptr;
			}
			virtual S3DRenderTarget* getTranslucentColorAttachment() const {
				return nullptr;
			}
			virtual S3DRenderTarget* getTranslucentRevealAttachment() const {
				return nullptr;
			}
			virtual S3DRenderTarget* getDepthAttachment() const {
				return nullptr;
			}
			double getDrawTimeCPU() const {
				return cpuDrawTime->getTime();
			}
			double getDrawTimeGPU() const  {
				return gpuDrawTime->getTime();
			}
			int getEstimatedShadowMapSize() const {
				return shadowMapSizeBytes / 1000.0f;
			}

			uPtr<TelemetryGPUMetric> gpuDrawTime{};
			uPtr<TelemetryCPUMetric> cpuDrawTime{};

			S3DFramebuffer* framebuffer[6]{};
			S3DRenderPass* renderPass{};
			S3DFramebuffer* translucentCastFramebuffer[6]{};
			S3DRenderPass* translucentCastRenderPass{};
			S3DFramebuffer* translucentTintFramebuffer[6]{};
			S3DRenderPass* translucentTintRenderPass{};

			S3DGraphicsPipeline* castTerrainPipeline{};
			ShadowMapPipelinePermutations* castPipelines{};
			ShadowMapPipelinePermutations* translucentCastPipelines{};
			ShadowMapPipelinePermutations* translucentPipelines{};

			ShaderResourceIndex shadowMap_id{};
			ShaderResourceIndex shadowMapTranslucent_id{};
			ShaderResourceIndex shadowMapTranslucentColor_id{};
			ShaderResourceIndex shadowMapTranslucentReveal_id{};
			std::vector<S3DCamera> cameras{};

			std::vector<VkDescriptorSet> shadowMapBufferInfoDescriptorSet;
			std::vector<uPtr<S3DBuffer>> shadowMapBufferInfo;

			ReflectiveShadowMap* reflectiveShadowMap{};

			float oFarZ = 10.0f;
			uint32_t instancingCount = 1;
			uint32_t numPasses = 1;
			float shadowMapSizeBytes = 0.0f;

			bool castTerrainShadows = false;

			bool castTranslucentShadows = false;
			const ShadowMapType type;
			const ShadowMapConvention convention;
			ShadowMapLight lightType{};
			ResourceSystem* resourceSystem;
		protected:
			S3DDevice& engineDevice;

			void freeDescriptor();
		};
		class ShadowVarianceMap : public ShadowMap {
		public:
			struct pushinfo {
				glm::vec2 screensize;
				glm::vec2 dir;
			};
			ShadowVarianceMap(ResourceSystem* resourceSystem_, ShadowMapConvention c, S3DDevice& device, uPtr<S3DDescriptorSetLayout>& shadowMapBufferDescriptorSetLayout) : ShadowMap(resourceSystem_, ShadowMapType::Variance, c, device, shadowMapBufferDescriptorSetLayout) {}

			virtual ~ShadowVarianceMap();
			virtual void process(FrameInfo& frameInfo, int framebufferIndex) override;
			virtual S3DRenderTarget* getOpaqueAttachment() const override;
			virtual S3DRenderTarget* getTranslucentAttachment() const override;
			virtual S3DRenderTarget* getTranslucentColorAttachment() const override;
			virtual S3DRenderTarget* getTranslucentRevealAttachment() const override;
			virtual S3DRenderTarget* getDepthAttachment() const override;

			S3DRenderTarget* varianceMSAttachment{};
			S3DRenderTarget* varianceAttachment{};
			S3DRenderTarget* depthAttachment{};

			S3DRenderTarget* varianceTranslucentCastMSAttachment{};
			S3DRenderTarget* varianceTranslucentCastAttachment{};
			S3DRenderTarget* depthTranslucentCastAttachment{};

			S3DRenderTarget* varianceTranslucentColorMSAttachment{};
			S3DRenderTarget* varianceTranslucentColorAttachment{};
			S3DRenderTarget* varianceTranslucentRevealMSAttachment{};
			S3DRenderTarget* varianceTranslucentRevealAttachment{};
			uPtr<PostProcessingEffect> fastGaussian;
			uPtr<PostProcessingEffect> fastGaussianB;
			uPtr<PostProcessingEffect> fastGaussianT;
			uPtr<PostProcessingEffect> fastGaussianTB;
			uPtr<PostProcessingEffect> fastGaussianTC;
			uPtr<PostProcessingEffect> fastGaussianTCB;
			uPtr<PostProcessingEffect> fastGaussianTR;
			uPtr<PostProcessingEffect> fastGaussianTRB;
		};
		class ShadowPCFMap : public ShadowMap {
		public:
			ShadowPCFMap(ResourceSystem* resourceSystem_, ShadowMapConvention c, S3DDevice& device, uPtr<S3DDescriptorSetLayout>& shadowMapBufferDescriptorSetLayout) : ShadowMap(resourceSystem_, ShadowMapType::PCF, c, device, shadowMapBufferDescriptorSetLayout) {}
			virtual ~ShadowPCFMap();
			virtual S3DRenderTarget* getOpaqueAttachment() const  override;
			virtual S3DRenderTarget* getTranslucentAttachment() const  override;
			virtual S3DRenderTarget* getTranslucentColorAttachment() const  override;
			virtual S3DRenderTarget* getTranslucentRevealAttachment() const  override;
			virtual S3DRenderTarget* getDepthAttachment() const override;

			S3DRenderTarget* depthAttachment{};
			S3DRenderTarget* depthTranslucentAttachment{};
			S3DRenderTarget* translucentColorAttachment{};
			S3DRenderTarget* translucentRevealAttachment{};
		};

		class ShadowMappingSystem {		
		public:
			ShadowMappingSystem(S3DDevice& device, ResourceSystem* resourceSystem, VkDescriptorSetLayout riggedSkeletonLayout, VkDescriptorSetLayout terrainSetLayout, uint32_t framesInFlight);
			~ShadowMappingSystem();

			ShadowMappingSystem(const ShadowMappingSystem&) = delete;
			ShadowMappingSystem& operator=(const ShadowMappingSystem&) = delete;

			void render(FrameInfo& frameInfo);
			void runGarbageCollector(uint32_t frameIndex, VkFence fence);
			
			void toggleRSM(bool enabled) {
				rsmEnabled = enabled;
			}

			const ShadowMap* getShadowMapForDebug(Actor actor) const {
				auto iter = shadowMaps.find(actor);
				if (iter != shadowMaps.end())
					return iter->second;
				return nullptr;
			}
		private:
			struct ShadowMapIDPtrs {
				ShaderResourceIndex* shadowMap_id_PTR{ nullptr };
				ShaderResourceIndex* shadowMapTransCast_id_PTR{ nullptr };
				ShaderResourceIndex* shadowMapTransCol_id_PTR{ nullptr };
				ShaderResourceIndex* shadowMapTransRev_id_PTR{ nullptr };
			};
			struct _LightComponentInfo {
				Actor actor;
				ShadowMapLight lightType;
				ShadowMapConvention lightConvention;
				int resolution;
				bool castTranslucent;
				ShadowMapIDPtrs idPtrs{};
				bool rsm;
				bool instancedDraw;
			};
			void renderDirectional(FrameInfo& frameInfo);
			void renderSpot(FrameInfo& frameInfo);
			void renderPoint(FrameInfo& frameInfo);

			ShadowVarianceMap* createVarianceShadowMap(int resolution, ShadowMapIDPtrs outIDs, ShadowMapLight type, ShadowMapConvention convention, bool castTranslucentShadows, bool generateRSM);
			ShadowPCFMap* createPCFShadowMap(int resolution, ShadowMapIDPtrs outIDs, ShadowMapLight type, ShadowMapConvention convention, bool castTranslucentShadows, bool generateRSM, bool allowInstancedDrawing);
			ReflectiveShadowMap* createRSM(int resolution, int targetDownsampleRes, VkSampleCountFlagBits samples, VkImageViewType viewType, uint32_t layerCount);
			void createPipelineLayouts();
			void renderShadowMap(FrameInfo& frameInfo, ShadowMap* shadowMap, float slopeBias);
			void renderShadowOmni(FrameInfo& frameInfo, ShadowMap* shadowMap, float slopeBias);

			void pushShadowMapConstants(FrameInfo& frameInfo, SurfaceMaterialRenderInfo& surfaceMaterialInfo, 
				SurfaceMaterialPipelineClassPermutationFlags flags, bool omni, uint32_t layerIndex,
				uint32_t materialIndex, bool dynamic, float farZ);
			
			Telemetry* shadowMapTelemetry;

			ResourceSystem* resourceSystem;

			std::vector<SurfaceMaterialPipelineClassPermutationFlags> opaqueMaterials{};
			std::vector<SurfaceMaterialPipelineClassPermutationFlags> translucentMaterials{};

			std::vector<std::pair<_LightComponentInfo, bool>> rebuildShadows{};
			std::vector<std::pair<ShadowMap*, S3DDestructionObject>> destroyShadowQueue{};
			std::unordered_map<entt::entity, ShadowMap*> shadowMaps{};
			std::unordered_set<entt::entity> removedShadowMaps{};
			
			uint32_t currentFrameIndex = 0;
			uint32_t maxFramesInFlight;

			S3DDevice& engineDevice;

			VkPipelineLayout terrainPipelineLayout;
			std::unordered_map<SurfaceMaterialPipelineClassPermutationFlags, VkPipelineLayout> pipelineLayouts;
			std::unordered_map<SurfaceMaterialPipelineClassPermutationFlags, VkPipelineLayout> omniPipelineLayouts;

			uPtr<S3DDescriptorSetLayout> shadowMapBufferDataDescriptorSetLayout;
			VkDescriptorSetLayout riggedSkeletonDescriptorSetLayout;
			VkDescriptorSetLayout terrainDescriptorSetLayout;

			friend class LightPropagationVolumeSystem;
			bool rsmEnabled = true;
		};
	}
}