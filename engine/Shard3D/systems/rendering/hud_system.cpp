#include "hud_system.h"
#include "../handlers/resource_system.h"
#include <fstream>
#include <shudvk/src_impl/api_impl.hh>
#include <shudvk/src_impl/vk_context_impl.hh>
#undef DrawText;

namespace Shard3D {

	HUDRenderSystem::HUDRenderSystem(S3DDevice& device, S3DWindow& window, ResourceSystem* resourceSystem, 
		S3DRenderTarget* sceneToOverlay, glm::vec2 resolution) 
		: device(device), window(window), resolution(resolution), resourceSystem(resourceSystem) {
		createRenderPass(sceneToOverlay);
		createFramebuffer(sceneToOverlay);

		SHUD::VulkanContextCreateInfo shudCreateInfo{};
		shudCreateInfo.mDevice = device.device();
		shudCreateInfo.mPhysicalDevice = device.physicalDevice();
		shudCreateInfo.mFramebufferFormat = sceneToOverlay->getAttachmentDescription().format;
		shudCreateInfo.mMSAASamples = VK_SAMPLE_COUNT_1_BIT;
		shudCreateInfo.mRenderPass = renderPass->getRenderPass();
		shudCreateInfo.mSubPass = 0;
		shudCreateInfo.mSwapChainImageCount = ProjectSystem::getGraphicsSettings().renderer.framesInFlight;
		shudCreateInfo.mDrawCullMode = VK_CULL_MODE_NONE;

		context = new SHUD::VulkanContext(shudCreateInfo);
		VkCommandBuffer cmd = device.beginSingleTimeCommands();
		context->CreateResources(cmd);
		device.endSingleTimeCommands(cmd);

		context->SetResolution((SHUD::fvec2&)resolution);

		AssetID dftx = AssetID("engine/textures/hud/default_btn.png.s3dasset");
		resourceSystem->loadTexture(dftx);
		defaultbtn = allocateTexture(dftx);

		elements["main"] = {};
	}

	HUDRenderSystem::~HUDRenderSystem() {
		delete framebuffer;
		delete renderPass;
		delete context;
		for (auto& [str, el] : elements) {
			eraseHUDElement(str, el);
		}
	}

	static glm::vec4 toVec4Col(SHUD::RGBAColor col) {
		return {
			col.r / 255.f,
			col.g / 255.f,
			col.b / 255.f,
			col.a / 255.f
		};
	}

	void HUDElementData::createHUDElementAsset(AssetID asset, const std::string& name) {
		std::ofstream fout(asset.getAbsolute());

		nlohmann::ordered_json out{};

		out["version"] = ENGINE_VERSION;
		out["assetType"] = "hud_element";

		serializeHUDElement(out, name);
		fout << out.dump(4);

		fout.flush();
		fout.close();
	}

	std::string HUDElementData::loadHUDElementAsset(const AssetID& asset, ResourceSystem* resourceSystem) {
		simdjson::dom::parser parser;
		if (!std::filesystem::exists(asset.getAbsolute())) return {};
		simdjson::padded_string json = simdjson::padded_string::load(asset.getAbsolute());
		const auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			SHARD3D_WARN("Failed to load {0} (ID {1})", asset.getAsset(), asset.getID());
			return {};
		}
		if (data["assetType"].get_string().value() != "hud_layer") {
			SHARD3D_WARN("Trying to load non hud_layer as a hud_layer asset!");
			return {};
		}
		return deserializeHUDElement(data.value_unsafe(), resourceSystem);
	}

	void HUDElementData::serializeHUDElement(nlohmann::ordered_json& elementOut, const std::string& elementName) {
		elementOut["name"] = elementName;
		elementOut["layer"] = genericBase->mLayer;
		elementOut["anchorOffset"] = (glm::vec2&)genericBase->mAnchorOffset;
		if (genericBase->mType == SHUD::Element::Type::Button) {
			auto* btnElement = dynamic_cast<SHUD::Element::Button*>(genericBase);
			elementOut["type"] = "button";
			elementOut["tabIndex"] = btnElement->mTabIndex;
			elementOut["color"] = toVec4Col(btnElement->mColor);
			elementOut["transform"]["position"] = (glm::vec2&)btnElement->mTransform.mPosition;
			elementOut["transform"]["scale"] = (glm::vec2&)btnElement->mTransform.mScale;
			elementOut["transform"]["rotation"] = btnElement->mTransform.mRotation;
			elementOut["transform"]["transformOffset"] = (glm::vec2&)btnElement->mTransform.mTransformOffset;
			elementOut["textureAtlas"] = textureAtlasAsset.getAsset();
			elementOut["textureAtlasConfig"]["defaultTextureCoords"] = (glm::vec4&)btnElement->mTexture.mDefaultTextureCoords;
			elementOut["textureAtlasConfig"]["hoverTextureCoords"] = (glm::vec4&)btnElement->mTexture.mHoverTextureCoords;
			elementOut["textureAtlasConfig"]["pressTextureCoords"] = (glm::vec4&)btnElement->mTexture.mPressTextureCoords;
			elementOut["textureAtlasConfig"]["selectTextureCoords"] = (glm::vec4&)btnElement->mTexture.mSelectTextureCoords;
			elementOut["textureAtlasConfig"]["disabledTextureCoords"] = (glm::vec4&)btnElement->mTexture.mDisabledTextureCoords;
			elementOut["alphaButtonCutoff"] = btnElement->mAlphaButtonCutoff;
			elementOut["scriptClass"] = scriptClass;
			elementOut["enabled"] = btnElement->mEnabled;
		} else if (genericBase->mType == SHUD::Element::Type::Text) {
			auto* txtElement = dynamic_cast<SHUD::Element::Text*>(genericBase);
			elementOut["type"] = "text";
			elementOut["color"] = toVec4Col(txtElement->mColor);
			elementOut["transform"]["position"] = (glm::vec2&)txtElement->mTransform.mPosition;
			elementOut["transform"]["scale"] = (glm::vec2&)txtElement->mTransform.mScale;
			elementOut["transform"]["rotation"] = txtElement->mTransform.mRotation;
			elementOut["transform"]["transformOffset"] = (glm::vec2&)txtElement->mTransform.mTransformOffset;
			elementOut["textString"] = txtElement->mText;
			elementOut["localizable"] = localizable;
			elementOut["formattable"] = formattable;
			elementOut["formatting"]["bold"] = txtElement->mFormatting.mBold;
			elementOut["formatting"]["italic"] = txtElement->mFormatting.mItalic;
			elementOut["formatting"]["strikeThrough"] = false;
			elementOut["formatting"]["underline"] = false;
			elementOut["formatting"]["outlineColor"] = toVec4Col(txtElement->mFormatting.mOutlineColor);
			elementOut["formatting"]["outlineWidth"] = txtElement->mFormatting.mOutlineWidth;
			elementOut["formatting"]["sizePx"] = txtElement->mFormatting.mSizePx;
			elementOut["formatting"]["lineSpacing"] = txtElement->mFormatting.mLineSpacing;
			elementOut["formatting"]["hAlignment"] = (int)txtElement->mFormatting.mHAlignment;
			elementOut["formatting"]["vAlignment"] = (int)txtElement->mFormatting.mVAlignment;
		} else if (genericBase->mType == SHUD::Element::Type::Line) {
			auto* linElement = dynamic_cast<SHUD::Element::Line*>(genericBase);
			elementOut["type"] = "line";
			elementOut["color"] = toVec4Col(linElement->mColor);
			elementOut["pointA"] = (glm::vec2&)linElement->mPointA;
			elementOut["pointB"] = (glm::vec2&)linElement->mPointB;
			elementOut["width"] = linElement->mWidth;
		} else if (genericBase->mType == SHUD::Element::Type::Rect) {
			auto* rctElement = dynamic_cast<SHUD::Element::Rect*>(genericBase);
			elementOut["type"] = "rect";
			elementOut["color"] = toVec4Col(rctElement->mColor);
			elementOut["transform"]["position"] = (glm::vec2&)rctElement->mTransform.mPosition;
			elementOut["transform"]["scale"] = (glm::vec2&)rctElement->mTransform.mScale;
			elementOut["transform"]["rotation"] = rctElement->mTransform.mRotation;
			elementOut["transform"]["transformOffset"] = (glm::vec2&)rctElement->mTransform.mTransformOffset;
		} else if (genericBase->mType == SHUD::Element::Type::Image) {
			auto* imgElement = dynamic_cast<SHUD::Element::Image*>(genericBase);
			elementOut["type"] = "image";
			elementOut["color"] = toVec4Col(imgElement->mColor);
			elementOut["transform"]["position"] = (glm::vec2&)imgElement->mTransform.mPosition;
			elementOut["transform"]["scale"] = (glm::vec2&)imgElement->mTransform.mScale;
			elementOut["transform"]["rotation"] = imgElement->mTransform.mRotation;
			elementOut["transform"]["transformOffset"] = (glm::vec2&)imgElement->mTransform.mTransformOffset;
			elementOut["textureAtlas"] = textureAtlasAsset.getAsset();
		} else if (genericBase->mType == SHUD::Element::Type::Panel) {
			auto* pnlElement = dynamic_cast<SHUD::Element::Panel*>(genericBase);
			elementOut["type"] = "panel";
			elementOut["transform"]["position"] = (glm::vec2&)pnlElement->mTransform.mPosition;
			elementOut["transform"]["scale"] = (glm::vec2&)pnlElement->mTransform.mScale;
			elementOut["transform"]["rotation"] = pnlElement->mTransform.mRotation;
			elementOut["transform"]["transformOffset"] = (glm::vec2&)pnlElement->mTransform.mTransformOffset;
			elementOut["children"] = nlohmann::ordered_json::array();
			for (auto& [string, element] : children) {
				elementOut["children"].push_back({});
				element->serializeHUDElement(elementOut["children"].back(), string);
			}
		}
	}

#define SIMDJSON_READ_SHUD_FVEC2(dom) SHUD::fvec2{ (float)dom.get_array().at(0).get_double().value(), (float)dom.get_array().at(1).get_double().value() }
#define SIMDJSON_READ_SHUD_FVEC3(dom) SHUD::fvec3{ (float)dom.get_array().at(0).get_double().value(), (float)dom.get_array().at(1).get_double().value(), (float)dom.get_array().at(2).get_double().value() }
#define SIMDJSON_READ_SHUD_FVEC4(dom) SHUD::fvec4{ (float)dom.get_array().at(0).get_double().value(), (float)dom.get_array().at(1).get_double().value(), (float)dom.get_array().at(2).get_double().value(), (float)dom.get_array().at(3).get_double().value() }
#define SIMDJSON_READ_SHUD_FTXC(dom) SHUD::TexCoordLimits{ SHUD::fvec2{ (float)dom.get_array().at(0).get_double().value(), (float)dom.get_array().at(1).get_double().value()}, SHUD::fvec2{(float)dom.get_array().at(2).get_double().value(), (float)dom.get_array().at(3).get_double().value()} }

	std::string HUDElementData::deserializeHUDElement(const simdjson::dom::element& data, ResourceSystem* resourceSystem) {
		std::string elementName = std::string(data["name"].get_string().value());
		if (data["type"].get_string().value() == "button") {
			auto* btnElement = new SHUD::Element::Button();
			genericBase = btnElement;
			btnElement->mTabIndex = data["tabIndex"].get_int64().value();
			btnElement->mColor = SHUD::RGBAColor(SIMDJSON_READ_SHUD_FVEC4(data["color"]));
			btnElement->mTransform.mPosition = SIMDJSON_READ_SHUD_FVEC2(data["transform"]["position"]);
			btnElement->mTransform.mScale = SIMDJSON_READ_SHUD_FVEC2(data["transform"]["scale"]);
			btnElement->mTransform.mRotation = data["transform"]["rotation"].get_double().value();
			btnElement->mTransform.mTransformOffset = SIMDJSON_READ_SHUD_FVEC2(data["transform"]["transformOffset"]);
			textureAtlasAsset = std::string(data["textureAtlas"].get_string().value());
			btnElement->mTexture.mDefaultTextureCoords = SIMDJSON_READ_SHUD_FTXC(data["textureAtlasConfig"]["defaultTextureCoords"]);
			btnElement->mTexture.mHoverTextureCoords = SIMDJSON_READ_SHUD_FTXC(data["textureAtlasConfig"]["hoverTextureCoords"]);
			btnElement->mTexture.mPressTextureCoords = SIMDJSON_READ_SHUD_FTXC(data["textureAtlasConfig"]["pressTextureCoords"]);
			btnElement->mTexture.mSelectTextureCoords = SIMDJSON_READ_SHUD_FTXC(data["textureAtlasConfig"]["selectTextureCoords"]);
			btnElement->mTexture.mDisabledTextureCoords = SIMDJSON_READ_SHUD_FTXC(data["textureAtlasConfig"]["disabledTextureCoords"]);
			btnElement->mAlphaButtonCutoff = data["alphaButtonCutoff"].get_double().value();
			scriptClass = std::string(data["scriptClass"].get_string().value());
			btnElement->mEnabled = data["enabled"].get_bool().value();
			btnElement->SetUniqueTag(elementName);
		} else if (data["type"].get_string().value() == "text") {
			auto* txtElement = new SHUD::Element::Text();
			genericBase = txtElement;
			txtElement->mColor = SHUD::RGBAColor(SIMDJSON_READ_SHUD_FVEC4(data["color"]));
			txtElement->mTransform.mPosition = SIMDJSON_READ_SHUD_FVEC2(data["transform"]["position"]);
			txtElement->mTransform.mScale = SIMDJSON_READ_SHUD_FVEC2(data["transform"]["scale"]);
			txtElement->mTransform.mRotation = data["transform"]["rotation"].get_double().value();
			txtElement->mTransform.mTransformOffset = SIMDJSON_READ_SHUD_FVEC2(data["transform"]["transformOffset"]);
			txtElement->mText = std::string(data["textString"].get_string().value());

			localizable = data["localizable"].get_bool().value();
			formattable = data["formattable"].get_bool().value();
			txtElement->mFormatting.mBold  = data["formatting"]["bold"].get_bool().value();
			txtElement->mFormatting.mItalic = data["formatting"]["italic"].get_bool().value();
			//txtElement-> data["formatting"]["strikeThrough"] = false;
			//txtElement-> data["formatting"]["underline"] = false;
			txtElement->mFormatting.mOutlineColor = SHUD::RGBAColor(SIMDJSON_READ_SHUD_FVEC4(data["formatting"]["outlineColor"]));
			txtElement->mFormatting.mOutlineWidth = data["formatting"]["outlineWidth"].get_double().value();
			txtElement->mFormatting.mSizePx = data["formatting"]["sizePx"].get_double().value();
			txtElement->mFormatting.mLineSpacing = data["formatting"]["lineSpacing"].get_double().value();
			txtElement->mFormatting.mHAlignment = (SHUD::TextFormattingHAlignment)data["formatting"]["hAlignment"].get_int64().value();
			txtElement->mFormatting.mVAlignment = (SHUD::TextFormattingVAlignment)data["formatting"]["vAlignment"].get_int64().value();
		} else if (data["type"].get_string().value() == "line") {
			auto* linElement = new SHUD::Element::Line();
			genericBase = linElement;
			linElement->mColor = SHUD::RGBAColor(SIMDJSON_READ_SHUD_FVEC4(data["color"]));
			linElement->mPointA = SIMDJSON_READ_SHUD_FVEC2(data["pointA"]);
			linElement->mPointB = SIMDJSON_READ_SHUD_FVEC2(data["pointB"]);
			linElement->mWidth = data["width"].get_double().value();
		} else if (data["type"].get_string().value() == "rect") {
			auto* rctElement = new SHUD::Element::Rect();
			genericBase = rctElement;
			rctElement->mColor = SHUD::RGBAColor(SIMDJSON_READ_SHUD_FVEC4(data["color"]));
			rctElement->mTransform.mPosition = SIMDJSON_READ_SHUD_FVEC2(data["transform"]["position"]);
			rctElement->mTransform.mScale = SIMDJSON_READ_SHUD_FVEC2(data["transform"]["scale"]);
			rctElement->mTransform.mRotation = data["transform"]["rotation"].get_double().value();
			rctElement->mTransform.mTransformOffset = SIMDJSON_READ_SHUD_FVEC2(data["transform"]["transformOffset"]);
		} else if (data["type"].get_string().value() == "image") {
			auto* imgElement = new SHUD::Element::Image();
			genericBase = imgElement;
			imgElement->mColor = SHUD::RGBAColor(SIMDJSON_READ_SHUD_FVEC4(data["color"]));
			imgElement->mTransform.mPosition = SIMDJSON_READ_SHUD_FVEC2(data["transform"]["position"]);
			imgElement->mTransform.mScale = SIMDJSON_READ_SHUD_FVEC2(data["transform"]["scale"]);
			imgElement->mTransform.mRotation = data["transform"]["rotation"].get_double().value();
			imgElement->mTransform.mTransformOffset = SIMDJSON_READ_SHUD_FVEC2(data["transform"]["transformOffset"]);
			textureAtlasAsset = std::string(data["textureAtlas"].get_string().value());
		} else if (data["type"].get_string().value() == "panel") {
			auto* pnlElement = new SHUD::Element::Panel();
			genericBase = pnlElement;
			pnlElement->mTransform.mPosition = SIMDJSON_READ_SHUD_FVEC2(data["transform"]["position"]);
			pnlElement->mTransform.mScale = SIMDJSON_READ_SHUD_FVEC2(data["transform"]["scale"]);
			pnlElement->mTransform.mRotation = data["transform"]["rotation"].get_double().value();
			pnlElement->mTransform.mTransformOffset = SIMDJSON_READ_SHUD_FVEC2(data["transform"]["transformOffset"]);
			for (int i = 0; i < data["children"].get_array().size(); i++) {
				HUDElementData* child = new HUDElementData();
				std::string childName = child->deserializeHUDElement(data["children"].get_array().at(i).value(), resourceSystem);
				children.emplace(childName, child);
			}
		}
		if (textureAtlasAsset) {
			resourceSystem->loadTexture(textureAtlasAsset);
		}

		genericBase->mLayer = data["layer"].get_int64().value();
		genericBase->mAnchorOffset = SIMDJSON_READ_SHUD_FVEC2(data["anchorOffset"]);
		
		return elementName;
	}

	void HUDRenderSystem::resize(S3DRenderTarget* newScene, glm::vec2 newSize) {
		resolution = newSize;
		context->SetResolution((SHUD::fvec2&)newSize);
		createFramebuffer(newScene);
	}

	void HUDRenderSystem::newFrame() {
		elementActions.clear();
		context->GetDrawList()->Clear();
	}

	const std::vector<HUDElementData*>& HUDRenderSystem::getElementActionList() {
		return elementActions;
	}

	void HUDRenderSystem::setCursorPosition(glm::vec2 extentOffset, glm::vec2 trueExtent, glm::vec2 position) {
		context->UpdateIOFrame();

		glm::vec2 uv = (position - extentOffset) / (trueExtent);
		context->GetIO().mCursorPosition = (SHUD::fvec2&)(uv * resolution);
		context->GetIO().mButtonState[GLFW_MOUSE_BUTTON_LEFT] = window.isMouseButtonDown(GLFW_MOUSE_BUTTON_LEFT);
		context->Pick();
	}

	void HUDRenderSystem::renderHUDLayer(SHUD::DrawList* drawList, const std::string& hudLayer) {
		for (auto& [string, element] : elements[hudLayer]) {
			renderElementData(drawList, element);
		}
	}

	void HUDRenderSystem::renderElementData(SHUD::DrawList* drawList, HUDElementData* elementData) {
		if (!elementData->visible) return;
		if (elementData->genericBase->mType == SHUD::Element::Type::Button) {
			auto* btnElement = dynamic_cast<SHUD::Element::Button*>(elementData->genericBase);
			btnElement->mTexture.mAtlas = allocateTexture(elementData->textureAtlasAsset);
			elementData->state = drawList->DrawButton(*btnElement).GetPressState();
			if (reinterpret_cast<uint32_t&>(elementData->state)) {
				elementActions.push_back(elementData);
			}
		} else if (elementData->genericBase->mType == SHUD::Element::Type::Text) {
			auto* txtElement = dynamic_cast<SHUD::Element::Text*>(elementData->genericBase);
			if (txtElement->mText.size()) {
				if (elementData->formattable) {
					std::string temp = txtElement->mText;
					for (const auto& [param, value] : elementData->textFormattingData) {
						strUtils::replace(txtElement->mText, '{' + param + '}', value);
					}
					drawList->DrawText(*txtElement);
					txtElement->mText = temp;
				} else {
					drawList->DrawText(*txtElement);
				}
			}
		} else if (elementData->genericBase->mType == SHUD::Element::Type::Line) {
			auto* linElement = dynamic_cast<SHUD::Element::Line*>(elementData->genericBase);
			drawList->DrawLine(*linElement);
		} else if (elementData->genericBase->mType == SHUD::Element::Type::Rect) {
			auto* rctElement = dynamic_cast<SHUD::Element::Rect*>(elementData->genericBase);
			drawList->DrawRect(*rctElement);
		} else if (elementData->genericBase->mType == SHUD::Element::Type::Image) {
			auto* imgElement = dynamic_cast<SHUD::Element::Image*>(elementData->genericBase);
			imgElement->mTexture.mAtlas = allocateTexture(elementData->textureAtlasAsset);
			drawList->DrawImage(*imgElement);
		} else if (elementData->genericBase->mType == SHUD::Element::Type::Panel) {
			auto* pnlElement = dynamic_cast<SHUD::Element::Panel*>(elementData->genericBase);
			drawList->BeginPanel(*pnlElement);
			for (auto& [string, element] : elementData->children) {
				renderElementData(drawList, element);
			}
			drawList->EndPanel();
		}
	}

	void HUDRenderSystem::render(VkCommandBuffer commandBuffer, uint32_t frameIndex) {
		renderPass->beginRenderPass(commandBuffer, framebuffer);
		SHUD::VulkanFrameInfo frameInfo_vk;
		frameInfo_vk.mCommandBuffer = commandBuffer;
		frameInfo_vk.mFrameIndex = frameIndex;
		context->Render(&frameInfo_vk);
		renderPass->endRenderPass(commandBuffer);
		context->Cleanup();
	}

	SHUD::ResourceObject HUDRenderSystem::allocateTexture(AssetID asset) {
		auto iter = allocatedTextures.find(asset);
		if (iter != allocatedTextures.end()) {
			return iter->second;
		}
		Texture2D* texture = resourceSystem->retrieveTexture(asset);
		VkDescriptorImageInfo imageInfo;
		imageInfo.sampler = texture->getSampler();
		imageInfo.imageView = texture->getImageView();
		imageInfo.imageLayout = texture->getImageLayout();
		SHUD::ResourceObject obj = context->CreateTexture(imageInfo);
		allocatedTextures[asset] = obj;
		return obj;
	}

	SHUD::TextureObject HUDRenderSystem::getDefaultButtonAtlas() {
		SHUD::TextureObject atlas;
		atlas.mAtlas = defaultbtn;
		atlas.mDefaultTextureCoords = { {0.0f, 0.0f}, { 0.5f, 0.5f } };
		atlas.mSelectTextureCoords = { {0.0f, 0.0f}, { 0.5f, 0.5f } };
		atlas.mHoverTextureCoords = { {0.5f, 0.5f}, { 1.0f, 1.0f } };//{ {0.5f, 0.0f}, { 1.0f, 0.5f } };
		atlas.mPressTextureCoords = { {0.0f, 0.5f}, { 0.5f, 1.0f } };
		atlas.mDisabledTextureCoords = { {0.5f, 0.5f}, { 1.0f, 1.0f } };
		return atlas;
	}

	void HUDRenderSystem::createHUDLayerAsset(const std::string& path, const std::unordered_map<std::string, HUDElementData*>& list) {
		std::ofstream fout(path);

		nlohmann::ordered_json out{};

		out["version"] = ENGINE_VERSION;
		out["assetType"] = "hud_layer";

		out["elements"] = nlohmann::ordered_json::array();
		for (auto& [string, element] : list) {
			out["elements"].push_back({});
			element->serializeHUDElement(out["elements"].back(), string);
		}

		fout << out.dump(4);

		fout.flush();
		fout.close();
	}
	void HUDRenderSystem::loadHUDLayerAsset(const std::string& path, std::unordered_map<std::string, HUDElementData*>& list, ResourceSystem* resourceSystem) {
		simdjson::dom::parser parser;
		if (!std::filesystem::exists(path)) return;
		simdjson::padded_string json = simdjson::padded_string::load(path);
		const auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			SHARD3D_WARN("Failed to load {0}", path);
			return;
		}
		if (data["assetType"].get_string().value() != "hud_layer") {
			SHARD3D_WARN("Trying to load non hud_layer as a hud_layer asset!");
			return;
		}
		for (int i = 0; i < data["elements"].get_array().size(); i++) {
			HUDElementData* element = new HUDElementData();
			std::string elementName = element->deserializeHUDElement(data["elements"].get_array().at(i).value(), resourceSystem);
			list.emplace(elementName, element);
		}
	}

	void HUDRenderSystem::createRenderPass(S3DRenderTarget* scene) {
		AttachmentInfo info{};
		info.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
		info.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		info.framebufferAttachment = scene;
		
		renderPass = new S3DRenderPass(device, { info });
	}

	void HUDRenderSystem::createFramebuffer(S3DRenderTarget* scene) {
		if (framebuffer) delete framebuffer;
		framebuffer = new S3DFramebuffer(device, renderPass, { scene });
	}
}