#include "brush_system.h"
#include "../handlers/resource_system.h"
#include <glm/gtx/hash.hpp>

namespace Shard3D::Systems {
	struct Vertex {
		glm::vec3 position;
		glm::vec3 normal;
		glm::vec2 uv;
	};
	BrushData::BrushData(const BrushCompositeRoad& composite) : composite(composite){
		
	}
	void BrushData::addNode(glm::vec3 node, uint32_t index) {
		if (index == -1) {
			pathNodes.push_back(node);
		} else {
			pathNodes.insert(pathNodes.begin() + index, node);
		}
	}
	void BrushData::rmvNode(uint32_t index) {
		assert(index < pathNodes.size());
		pathNodes.erase(pathNodes.begin() + index);
	}

	static glm::vec2 stepDir(glm::vec2 cs) {
		bool isEast = cs.x > 0.0f;
		bool isNorth = cs.y > 0.0f;
		bool cosGreater = abs(cs.x) > abs(cs.y);

		return { cosGreater ? isEast : -1, cosGreater ? -1 : isNorth};
	}

	static int getorientation(float c, float s) {
		int orientation;
		if (s > 0.0f) {
			orientation = 0;
		} else {
			orientation = 2;
		}
		if (abs(c) > abs(s)) {
			if (c > 0.0f) {
				orientation = 1;
			} else {
				orientation = 3;
			}
		}
		return orientation;
	}

	void BrushData::build(S3DDevice& device) {
		//BrushableMesh mesh{};
		//mesh.points = {
		//	{ -2.00f, 0.08f },
		//	{ -1.05f, 0.08f },
		//	{ -1.00f, 0.12f },
		//	{ -0.95f, 0.12f },
		//	{ -0.90f, 0.04f },
		//	{ +0.90f, 0.04f },
		//	{ +0.95f, 0.12f },
		//	{ +1.00f, 0.12f },
		//	{ +1.05f, 0.08f },
		//	{ +2.00f, 0.08f }
		//};

		//std::vector<Vertex> built;
		//std::vector<uint32_t> indices;

		transforms.clear();
		brushedMeshes.clear();

		struct BrushBlockObject {
			int orientation = 0; // 0 = N, 1 = W, 2 = S, 3 = E
			int type = 0; // 0 = straight, 1 = curve, 2 = t branch, 3 = cross
		};

		std::unordered_map<glm::vec3, BrushBlockObject> bbos;

		for (int i = 0; i < pathNodes.size(); i++) {
			glm::vec3 direction;
			if (i = pathNodes.size()) {
				direction = pathNodes[i] - pathNodes[i - 1];
			} else {
				direction = pathNodes[i + 1] - pathNodes[i];
			}

			if (dot(direction, direction) == 0.0) {
				continue;
			}
			
			float d = glm::length(direction);
			glm::vec3 V = direction / d;

			glm::vec2 sc = stepDir(V);

			float s = sc.x;
			float c = sc.y;

			auto iter = bbos.find(pathNodes[i]);
			if (iter == bbos.end()) {
				int orientation = getorientation(c, s);

				bbos[pathNodes[i]] = { orientation, 0 };
			} else {
				if (bbos[pathNodes[i]].type == 3) continue;
				else if (bbos[pathNodes[i]].type == 0) {
					bbos[pathNodes[i]].orientation = getorientation(c, s);
				}
				else if (bbos[pathNodes[i]].type == 1) {
					int ori = getorientation(c, s);

					switch (bbos[pathNodes[i]].type) {
					case(0): // curve is pointing down, right (|-)
						if (ori == 2) {
							continue;
						} else if(ori == 3) {
							continue;
						} else if (ori == 1) {
							bbos[pathNodes[i]].orientation = 0;
						} else if (ori == 0) {
							bbos[pathNodes[i]].orientation = 1;
						}
						break;
					case(1): // curve is pointing up, right (L)
						if (ori == 0) {
							continue;
						} else if (ori == 3) {
							continue;
						} else if (ori == 1) {
							bbos[pathNodes[i]].orientation = 2;
						} else if (ori == 2) {
							bbos[pathNodes[i]].orientation = 1;
						}
						break;
					case(2): // curve is pointing up, left (_|)
						if (ori == 0) {
							continue;
						} else if (ori == 1) {
							continue;
						} else if (ori == 2) {
							bbos[pathNodes[i]].orientation = 3;
						} else if (ori == 3) {
							bbos[pathNodes[i]].orientation = 2;
						}
						break; 
					case(3): // curve is pointing down, left (-|)
						if (ori == 2) {
							continue;
						} else if (ori == 1) {
							continue;
						} else if (ori == 3) {
							bbos[pathNodes[i]].orientation = 0;
						} else if (ori == 0) {
							bbos[pathNodes[i]].orientation = 3;
						}
						break;
					}
				}
				
				bbos[pathNodes[i]].type++;
			}

		}

		for (auto& [pos, bbo] : bbos) {
			float c, s;

			switch (bbo.orientation) {
			case(0):
				c = 0.0f;
				s = 1.0f;
				break;
			case(1):
				c = -1.0f;
				s = 0.0f;
				break;
			case(2):
				c = 0.0f;
				s = -1.0f;
				break;
			case(3):
				c = 1.0f;
				s = 0.0f;
				break;
			}

			glm::mat4 transform{
			{c, 0.f, -s, 0.f},
			{0, 1.f, 0, 0.f},
			{s, 0.f, c, 0.f},
			{pos, 1.f},
			};

			transforms.push_back(transform);

			switch (bbo.type) {
			case(0):
				brushedMeshes.push_back(composite.straight);
				break;
			case(1):
				brushedMeshes.push_back(composite.curveR);
				break;
			case(2):
				brushedMeshes.push_back(composite.branchT);
				break;
			case(3):
				brushedMeshes.push_back(composite.cross);
				break;
			}
		}
	}
	BrushSystem::BrushSystem(S3DDevice& device, VkRenderPass renderPass, ResourceSystem* resourceSystem, VkDescriptorSetLayout globalSetLayout) : engineDevice(device) {
		createPipelineLayout(globalSetLayout);
		createPipeline(renderPass);

		resourceSystem->loadMesh(AssetID("engine/meshes/brushable/roadblocks_s3d_straight.s3dasset"));
		resourceSystem->loadMesh(AssetID("engine/meshes/brushable/roadblocks_s3d_curve.s3dasset"));
		resourceSystem->loadMesh(AssetID("engine/meshes/brushable/roadblocks_s3d_tbranch.s3dasset"));
		resourceSystem->loadMesh(AssetID("engine/meshes/brushable/roadblocks_s3d_cross.s3dasset"));

		brushDatas.push_back(make_sPtr<BrushData>(BrushCompositeRoad{
			resourceSystem->retrieveMesh(AssetID("engine/meshes/brushable/roadblocks_s3d_straight.s3dasset")),
			resourceSystem->retrieveMesh(AssetID("engine/meshes/brushable/roadblocks_s3d_curve.s3dasset")),
			resourceSystem->retrieveMesh(AssetID("engine/meshes/brushable/roadblocks_s3d_tbranch.s3dasset")),
			resourceSystem->retrieveMesh(AssetID("engine/meshes/brushable/roadblocks_s3d_cross.s3dasset")),
			2.0f
		}));
	}
	BrushSystem::~BrushSystem() {
	
	}
	void BrushSystem::render(FrameInfo& frameInfo) {
		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer, 
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			pipelineLayout->getPipelineLayout(), 
			0, 
			1, 
			&frameInfo.globalDescriptorSet, 
			0, 
			nullptr
		);
		for (int i = 0; i < brushDatas.at(0)->getMeshes().size(); i++) {
			auto* mesh = brushDatas.at(0)->getMeshes()[i];
			auto& transform = brushDatas.at(0)->getTransforms()[i];

			vkCmdPushConstants(
				frameInfo.commandBuffer,
				pipelineLayout->getPipelineLayout(),
				VK_SHADER_STAGE_VERTEX_BIT,
				0,
				sizeof(glm::mat4),
				&transform
			);
			mesh->drawAll(frameInfo.commandBuffer);
		}
	}
	void BrushSystem::renderPreviewTiles(FrameInfo& frameInfo) {
		
	}
	void BrushSystem::createPipelineLayout(VkDescriptorSetLayout globalSetLayout) {
		push = S3DPushConstant(sizeof(glm::mat4), VK_SHADER_STAGE_VERTEX_BIT);
		pipelineLayout = make_uPtr<S3DPipelineLayout>(engineDevice, std::vector<VkPushConstantRange>{ push.getRange() }, std::vector<VkDescriptorSetLayout>{globalSetLayout});

	}
	void BrushSystem::createPipeline(VkRenderPass renderPass) {
		S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo(ProjectSystem::getEngineSettings().postfx.enableMotionBlur ? 6 : 5);
		if (pipelineConfig.colorBlendAttachments.size() == 6) pipelineConfig.colorBlendAttachments[5].colorWriteMask = 0;
		pipelineConfig.setCullMode(VK_CULL_MODE_BACK_BIT);
		pipelineConfig.enableVertexDescriptions(VertexDescriptionOptions_Position | VertexDescriptionOptions_Normal);
		pipelineConfig.setCullMode(VK_CULL_MODE_BACK_BIT);

		if (ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit) {
			pipelineConfig.reverseDepth();
		}
		graphicsPipeline = make_uPtr<S3DGraphicsPipeline>(
			engineDevice, std::vector<S3DShader>{
			S3DShader{ ShaderType::Vertex, "resources/shaders/editor/mesh_brush_unbaked.vert", "editor" },
			S3DShader{ ShaderType::Vertex, "resources/shaders/editor/mesh_brush_unbaked_deferred.frag", "editor" },
		},
		pipelineConfig
		);
	}
}
