#include "../../s3dpch.h" 

#include "shadow_mapping_system.h"
#include "../../core/vulkan_api/graphics_pipeline.h"
#include "../../core/vulkan_api/descriptors.h"
#include "../../ecs.h"
#include <glm/ext/matrix_clip_space.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtx/projection.hpp>
#include "../../core/misc/engine_settings.h"
#include "../handlers/project_system.h"
#include "terrain_system.h"
#include <magic_enum.hpp>

// shadow maps are the worst thing to do in computer graphics, next to everything else 
namespace Shard3D::Systems {
	struct StaticShadowTexturedPushConstants {
		glm::mat4 model;
		uint32_t materialID;
		int layer;
		float maxPlane;
	};
	struct StaticShadowMMTexturedPushConstants {
		glm::mat4 model;
		uint32_t materialID;
		int layer;
		float maxPlane;
	};

	struct DynamicShadowTexturedPushConstants {
		uint32_t bufferID;
		uint32_t materialID;
		int layer;
		float maxPlane;
	};
	struct DynamicShadowMMTexturedPushConstants {
		uint32_t bufferID;
		uint32_t materialID;
		int layer;
		float maxPlane;
	};

	struct ShadowMatrixData {
		glm::mat4 viewProjection[4];
		glm::mat4 invView[4];
		glm::mat4 invProjection[4];
		alignas(16)glm::vec3 RSMLIGHT_direction;
		alignas(16)glm::vec3 RSMLIGHT_intensity;
		alignas(16)glm::vec3 RSMLIGHT_position;
		alignas(16)glm::vec2 RSMLIGHT_cosAngles;
	};
	struct OmniShadowMatrixData {
		glm::mat4 viewProjection[6];
		glm::mat4 invView[6];
		glm::mat4 invProjection[6];
	};

	struct TerrainPushData {
		glm::vec4 translation;
		glm::vec2 tiles;
		float tileExtent;
		float heightMul;
		uint32_t LOD;
		int layer;
	};

	static float getPow2Size(float in) {
		return pow(2.0, round(log2(in)));
	}

	void ReflectiveShadowMap::downSampleMaps(VkCommandBuffer commandBuffer, S3DDevice& device) {
		if (fluxMap->getAttachmentDescription().mipLevels > 1) {
			device.transitionImageLayout(
				commandBuffer,
				fluxMap->getImage(),
				fluxMap->getAttachmentDescription().format,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
				0,
				1,
				0,
				1
			);
			device.transitionImageLayout(
				commandBuffer,
				fluxMap->getImage(),
				fluxMap->getAttachmentDescription().format,
				VK_IMAGE_LAYOUT_UNDEFINED,
				VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
				1,
				fluxMap->getAttachmentDescription().mipLevels - 1,
				0,
				1
			);
			device.transitionImageLayout(
				commandBuffer,
				normalMap->getImage(),
				normalMap->getAttachmentDescription().format,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
				0,
				1,
				0,
				1
			);
			device.transitionImageLayout(
				commandBuffer,
				normalMap->getImage(),
				normalMap->getAttachmentDescription().format,
				VK_IMAGE_LAYOUT_UNDEFINED,
				VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
				1,
				normalMap->getAttachmentDescription().mipLevels - 1,
				0,
				1
			);
			device.generateMipMaps(
				commandBuffer,
				fluxMap->getImage(),
				fluxMap->getAttachmentDescription().format,
				fluxMap->getAttachmentDescription().dimensions.x,
				fluxMap->getAttachmentDescription().dimensions.y,
				1,
				fluxMap->getAttachmentDescription().mipLevels,
				1,
				fluxMap->getImageLayout()
			);
			device.generateMipMaps(
				commandBuffer,
				normalMap->getImage(),
				normalMap->getAttachmentDescription().format,
				normalMap->getAttachmentDescription().dimensions.x,
				normalMap->getAttachmentDescription().dimensions.y,
				1,
				normalMap->getAttachmentDescription().mipLevels,
				1,
				normalMap->getImageLayout()
			);
		}
	}

	ShadowMap::ShadowMap(ResourceSystem* resourceSystem_, ShadowMapType t, ShadowMapConvention c, S3DDevice& device, uPtr<S3DDescriptorSetLayout>& shadowMapBufferDescriptorSetLayout)
		: type(t), convention(c), resourceSystem(resourceSystem_), engineDevice(device),
		shadowMap_id(resourceSystem_->getDescriptor()->allocateIndex(BINDLESS_SAMPLER_BINDING)),
		shadowMapTranslucent_id(resourceSystem_->getDescriptor()->allocateIndex(BINDLESS_SAMPLER_BINDING)),
		shadowMapTranslucentColor_id(resourceSystem_->getDescriptor()->allocateIndex(BINDLESS_SAMPLER_BINDING)),
		shadowMapTranslucentReveal_id(resourceSystem_->getDescriptor()->allocateIndex(BINDLESS_SAMPLER_BINDING))
	{
		shadowMapBufferInfoDescriptorSet.resize(ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		for (int i = 0; i < ProjectSystem::getGraphicsSettings().renderer.framesInFlight; i++) {
			shadowMapBufferInfo.push_back(make_uPtr<S3DBuffer>(
				device,
				(c == ShadowMapConvention::Paraboloid ||
				c == ShadowMapConvention::DualParaboloid ||
				c == ShadowMapConvention::Cube) ?
				sizeof(OmniShadowMatrixData) :
				sizeof(ShadowMatrixData),
				1,
				VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
			));
			shadowMapBufferInfo.back()->map();
			auto bufferInfo = shadowMapBufferInfo.back()->descriptorInfo();
			S3DDescriptorWriter(*shadowMapBufferDescriptorSetLayout, *device.staticMaterialPool)
				.writeBuffer(0, &bufferInfo)
				.build(shadowMapBufferInfoDescriptorSet[i]);
		}
		SHARD3D_LOG("Shadow Map was created!", shadowMap_id);
	}

	ShadowMap::~ShadowMap() {
		for (int i = 0; i < 6; i++) {
			PTR_DELETE(framebuffer[i]);
			PTR_DELETE(translucentCastFramebuffer[i]);
			PTR_DELETE(translucentTintFramebuffer[i]);
		}
		PTR_DELETE(renderPass);
		PTR_DELETE(translucentCastRenderPass);
		PTR_DELETE(translucentTintRenderPass);
		if (reflectiveShadowMap) {
			PTR_DELETE(reflectiveShadowMap->normalMap);
			PTR_DELETE(reflectiveShadowMap->fluxMap);
			delete reflectiveShadowMap;
		}
		PTR_DELETE(castTerrainPipeline);
		PTR_DELETE(castPipelines);
		PTR_DELETE(translucentCastPipelines);
		PTR_DELETE(translucentPipelines);
		engineDevice.staticMaterialPool->freeDescriptors(shadowMapBufferInfoDescriptorSet);
	}

	void ShadowMap::freeDescriptor() {
		SHARD3D_INFO("Shadow Map {0} was destroyed!", shadowMap_id);
		resourceSystem->getDescriptor()->freeIndex(BINDLESS_SAMPLER_BINDING, shadowMap_id);
		resourceSystem->getDescriptor()->freeIndex(BINDLESS_SAMPLER_BINDING, shadowMapTranslucent_id);
		resourceSystem->getDescriptor()->freeIndex(BINDLESS_SAMPLER_BINDING, shadowMapTranslucentColor_id);
		resourceSystem->getDescriptor()->freeIndex(BINDLESS_SAMPLER_BINDING, shadowMapTranslucentReveal_id);
	}

	ShadowVarianceMap::~ShadowVarianceMap() {
		freeDescriptor();
		PTR_DELETE(varianceMSAttachment);
		PTR_DELETE(varianceAttachment);
		PTR_DELETE(depthAttachment);
		PTR_DELETE(varianceTranslucentCastMSAttachment);
		PTR_DELETE(varianceTranslucentCastAttachment);
		PTR_DELETE(depthTranslucentCastAttachment);
		PTR_DELETE(varianceTranslucentColorMSAttachment);
		PTR_DELETE(varianceTranslucentColorAttachment);
		PTR_DELETE(varianceTranslucentRevealMSAttachment);
		PTR_DELETE(varianceTranslucentRevealAttachment);
	}

	void ShadowVarianceMap::process(FrameInfo& frameInfo, int framebufferIndex) {
		if (lightType == ShadowMapLight::Directional && (ProjectSystem::getGraphicsSettings().shadows.varianceDirectionalBlurQuality == 2)
			|| lightType == ShadowMapLight::Spot && (ProjectSystem::getGraphicsSettings().shadows.varianceSpotBlurQuality == 2)) { // dual pass blur
			glm::vec2 dim = static_cast<glm::vec2>(fastGaussian->getAttachment()->getDimensions());
			pushinfo pnfo = pushinfo{};
			pnfo.screensize = dim;
			pnfo.dir = { 1.0f, 0.0f };
			pushinfo pnfo2 = pushinfo{};
			pnfo2.screensize = dim;
			pnfo2.dir = { 0.0f, 1.0f };
			fastGaussian->render(frameInfo.commandBuffer, pnfo, framebufferIndex);
			fastGaussianB->render(frameInfo.commandBuffer, pnfo2, framebufferIndex);

			if (castTranslucentShadows) {
				fastGaussianT->render(frameInfo.commandBuffer, pnfo, framebufferIndex);
				fastGaussianTB->render(frameInfo.commandBuffer, pnfo2, framebufferIndex);
				fastGaussianTC->render(frameInfo.commandBuffer, pnfo, framebufferIndex);
				fastGaussianTCB->render(frameInfo.commandBuffer, pnfo2, framebufferIndex);
				fastGaussianTR->render(frameInfo.commandBuffer, pnfo, framebufferIndex);
				fastGaussianTRB->render(frameInfo.commandBuffer, pnfo2, framebufferIndex);
			}
		} else if (lightType == ShadowMapLight::Directional && (ProjectSystem::getGraphicsSettings().shadows.varianceDirectionalBlurQuality == 1)
			|| lightType == ShadowMapLight::Spot && (ProjectSystem::getGraphicsSettings().shadows.varianceSpotBlurQuality == 1)) { // single pass blur

			glm::vec2 dim = static_cast<glm::vec2>(fastGaussian->getAttachment()->getDimensions());
			fastGaussian->render(frameInfo.commandBuffer, dim, framebufferIndex);
			if (castTranslucentShadows) {
				fastGaussianT->render(frameInfo.commandBuffer, dim, framebufferIndex);
				fastGaussianTC->render(frameInfo.commandBuffer, dim, framebufferIndex);
				fastGaussianTR->render(frameInfo.commandBuffer, dim, framebufferIndex);
			}
		}
		// 0 == no blur option
	}

	 S3DRenderTarget* ShadowVarianceMap::getOpaqueAttachment() const  {
		if (lightType == ShadowMapLight::Directional && (ProjectSystem::getGraphicsSettings().shadows.varianceDirectionalBlurQuality == 2)
			|| lightType == ShadowMapLight::Spot && (ProjectSystem::getGraphicsSettings().shadows.varianceSpotBlurQuality == 2)) { // dual pass blur
			return fastGaussianB->getAttachment();
		} else if (lightType == ShadowMapLight::Directional && (ProjectSystem::getGraphicsSettings().shadows.varianceDirectionalBlurQuality == 1)
			|| lightType == ShadowMapLight::Spot && (ProjectSystem::getGraphicsSettings().shadows.varianceSpotBlurQuality == 1)) { // single pass blur
			return fastGaussian->getAttachment();
		}
		return varianceAttachment;
	}
	 S3DRenderTarget* ShadowVarianceMap::getTranslucentAttachment() const  {
		if (castTranslucentShadows) {
			if (lightType == ShadowMapLight::Directional && (ProjectSystem::getGraphicsSettings().shadows.varianceDirectionalBlurQuality == 2)
				|| lightType == ShadowMapLight::Spot && (ProjectSystem::getGraphicsSettings().shadows.varianceSpotBlurQuality == 2)) { // dual pass blur
				return fastGaussianTB->getAttachment();
			} else if (lightType == ShadowMapLight::Directional && (ProjectSystem::getGraphicsSettings().shadows.varianceDirectionalBlurQuality == 1)
				|| lightType == ShadowMapLight::Spot && (ProjectSystem::getGraphicsSettings().shadows.varianceSpotBlurQuality == 1)) { // single pass blur
				return fastGaussianT->getAttachment();
			}
			return varianceTranslucentCastAttachment;
		}
		return nullptr;
	}
	S3DRenderTarget* ShadowVarianceMap::getTranslucentColorAttachment() const {
		if (castTranslucentShadows) {
			if (lightType == ShadowMapLight::Directional && (ProjectSystem::getGraphicsSettings().shadows.varianceDirectionalBlurQuality == 2)
				|| lightType == ShadowMapLight::Spot && (ProjectSystem::getGraphicsSettings().shadows.varianceSpotBlurQuality == 2)) { // dual pass blur
				return fastGaussianTCB->getAttachment();
			} else if (lightType == ShadowMapLight::Directional && (ProjectSystem::getGraphicsSettings().shadows.varianceDirectionalBlurQuality == 1)
				|| lightType == ShadowMapLight::Spot && (ProjectSystem::getGraphicsSettings().shadows.varianceSpotBlurQuality == 1)) { // single pass blur
				return fastGaussianTC->getAttachment();
			}
			return varianceTranslucentColorAttachment;
		}
		return nullptr;
	}
	S3DRenderTarget* ShadowVarianceMap::getTranslucentRevealAttachment() const {
		if (castTranslucentShadows) {
			if (lightType == ShadowMapLight::Directional && (ProjectSystem::getGraphicsSettings().shadows.varianceDirectionalBlurQuality == 2)
				|| lightType == ShadowMapLight::Spot && (ProjectSystem::getGraphicsSettings().shadows.varianceSpotBlurQuality == 2)) { // dual pass blur
				return fastGaussianTRB->getAttachment();
			} else if (lightType == ShadowMapLight::Directional && (ProjectSystem::getGraphicsSettings().shadows.varianceDirectionalBlurQuality == 1)
				|| lightType == ShadowMapLight::Spot && (ProjectSystem::getGraphicsSettings().shadows.varianceSpotBlurQuality == 1)) { // single pass blur
				return fastGaussianTR->getAttachment();
			}
			return varianceTranslucentRevealAttachment;
		}
		return nullptr;
	}

	S3DRenderTarget* ShadowVarianceMap::getDepthAttachment() const {
		return depthAttachment;
	}
	
	ShadowPCFMap::~ShadowPCFMap() {
		freeDescriptor();
		PTR_DELETE(depthAttachment);
		PTR_DELETE(depthTranslucentAttachment);
		PTR_DELETE(translucentColorAttachment);
		PTR_DELETE(translucentRevealAttachment);
	}

	S3DRenderTarget* ShadowPCFMap::getOpaqueAttachment() const {
		return depthAttachment;
	}

	S3DRenderTarget* ShadowPCFMap::getTranslucentAttachment() const {
		if (castTranslucentShadows) {
			return depthTranslucentAttachment;
		}
		return nullptr;
	}

	S3DRenderTarget* ShadowPCFMap::getTranslucentColorAttachment() const 	{
		if (castTranslucentShadows) {
			return translucentColorAttachment;
		}
		return nullptr;
	}

	S3DRenderTarget* ShadowPCFMap::getTranslucentRevealAttachment() const 	{
		if (castTranslucentShadows) {
			return translucentRevealAttachment;
		}
		return nullptr;
	}

	S3DRenderTarget* ShadowPCFMap::getDepthAttachment() const {
		return depthAttachment;
	}
	
	ShadowMapPipelinePermutations::ShadowMapPipelinePermutations(S3DDevice& device,
		const std::string& shadowGenVertFile, const S3DShaderSpecialization& specializationVert, const std::string& shadowOutputVertFile, 
		const std::string& shadowGenFragFile, const S3DShaderSpecialization& specializationFrag, const std::string& shadowOutputFragFile,
		S3DGraphicsPipelineConfigInfo& pipelineConfigInfo, const std::vector<std::string>& definitions,
		std::unordered_map<SurfaceMaterialPipelineClassPermutationFlags, VkPipelineLayout>& pipelineLayouts, bool requireFragmentShader) {

		for (auto [flags, layout] : pipelineLayouts) {
			std::vector<std::string> definitionsPermutation = definitions;
			std::string shadowOutputVertFilePermutation = shadowOutputVertFile;
			std::string shadowOutputFragFilePermutation = shadowOutputFragFile;

			bool alphaTestEnable = false;
			if (flags & SurfaceMaterialPipelineClassPermutationOptions_AlphaTest) {
				definitionsPermutation.push_back("ENABLE_ALPHA_TEST");
				shadowOutputVertFilePermutation.append("_alphatest");
				shadowOutputFragFilePermutation.append("_alphatest");
				alphaTestEnable = true;
			}
			if (flags & SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates) {
				definitionsPermutation.push_back("ACTOR_DYNAMIC");
				shadowOutputVertFilePermutation.append("_dynamic");
			}
			if (flags & SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms) {
				definitionsPermutation.push_back("RIGGED_MESH");
				shadowOutputVertFilePermutation.append("_rigged");
			}

			pipelineConfigInfo.pipelineLayout = layout;

			std::vector<S3DShader> shadowShaders = {
				{ ShaderType::Vertex, shadowGenVertFile + ".vert", "shadows", specializationVert, definitionsPermutation, shadowOutputVertFilePermutation + ".vert" }
			};
			if (requireFragmentShader || alphaTestEnable) {
				shadowShaders.push_back({ ShaderType::Pixel, shadowGenFragFile + ".frag", "shadows", specializationFrag, definitionsPermutation, shadowOutputFragFilePermutation + ".frag" });
			}

			if (flags & ~SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling)
				pipelineConfigInfo.setCullMode(VK_CULL_MODE_NONE);
			if (flags & SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling)
				pipelineConfigInfo.setCullMode(VK_CULL_MODE_BACK_BIT);

			// disable uv vertex input when not needed

			pipelines[flags] = new S3DGraphicsPipeline(
				device, shadowShaders,
				pipelineConfigInfo
			);
		}
	}

	ShadowMapPipelinePermutations::~ShadowMapPipelinePermutations() {
		for (auto [flags, pipeline] : pipelines) {
			delete pipeline;
		}
	}



	ShadowMappingSystem::ShadowMappingSystem(S3DDevice& device, ResourceSystem* resourceSystem, VkDescriptorSetLayout riggedSkeletonLayout, VkDescriptorSetLayout terrainSetLayout, uint32_t framesInFlight) :
		engineDevice(device), resourceSystem(resourceSystem), maxFramesInFlight(framesInFlight), shadowMapTelemetry(new Telemetry(device)), riggedSkeletonDescriptorSetLayout(riggedSkeletonLayout), terrainDescriptorSetLayout(terrainSetLayout){
		for (auto& class_ : ProjectSystem::getEngineSettings().allowedMaterialPermutations) {
			if (!(class_ & SurfaceMaterialPipelineClassPermutationOptions_Translucent))
				opaqueMaterials.push_back(class_);
			else
				translucentMaterials.push_back(class_);
		}
		shadowMapBufferDataDescriptorSetLayout = S3DDescriptorSetLayout::Builder(device)
			.addBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT)
			.build();
		createPipelineLayouts();
	}
	ShadowMappingSystem::~ShadowMappingSystem() {
		for (auto& [id, map]: shadowMaps) {
			delete map; // free the shadow maps
		}
		for (auto& [map, destruct] : destroyShadowQueue) {
			delete map; 
		}
		vkDestroyPipelineLayout(engineDevice.device(), terrainPipelineLayout, nullptr);
		for (auto [flags, layout] : pipelineLayouts) {
			vkDestroyPipelineLayout(engineDevice.device(), layout, nullptr);
		}
		for (auto [flags, layout] : omniPipelineLayouts) {
			vkDestroyPipelineLayout(engineDevice.device(), layout, nullptr);
		}
		delete shadowMapTelemetry;
	}

	ReflectiveShadowMap* ShadowMappingSystem::createRSM(int resolution, int targetDownsampleRes, VkSampleCountFlagBits samples, VkImageViewType viewType, uint32_t layerCount) {
		ReflectiveShadowMap* rsm = new ReflectiveShadowMap();
		uint32_t mipLevels = std::log2(resolution) - std::log2(targetDownsampleRes) + 1;
		rsm->fluxMap = new S3DRenderTarget(engineDevice, {
			VK_FORMAT_B10G11R11_UFLOAT_PACK32,
			VK_IMAGE_ASPECT_COLOR_BIT,
			viewType,
			glm::ivec3(resolution, resolution, 1),
			VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT ,
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
			S3DRenderTargetType::Color,
			true,
			samples,
			false,
			layerCount,
			mipLevels
			}
		);
		rsm->normalMap = new S3DRenderTarget(engineDevice, {
			VK_FORMAT_A2B10G10R10_UNORM_PACK32,
			VK_IMAGE_ASPECT_COLOR_BIT,
			viewType,
			glm::ivec3(resolution, resolution, 1),
			VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT,
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
			S3DRenderTargetType::Color,
			true,
			samples,
			false,
			layerCount,
			mipLevels
			}
		);
		return rsm;
	}

	ShadowVarianceMap* ShadowMappingSystem::createVarianceShadowMap(int resolution, ShadowMapIDPtrs outIDs, ShadowMapLight type, ShadowMapConvention convention, bool castTranslucentShadows, bool generateRSM) {
		ShadowVarianceMap* shadowMap = new ShadowVarianceMap(resourceSystem, convention, engineDevice, shadowMapBufferDataDescriptorSetLayout);
		shadowMap->castTerrainShadows = false;
		shadowMap->gpuDrawTime = make_uPtr<TelemetryGPUMetric>(shadowMapTelemetry, "Shadow Map", "Draw GPU");
		shadowMap->cpuDrawTime = make_uPtr<TelemetryCPUMetric>(shadowMapTelemetry, "Shadow Map", "Draw CPU");
		shadowMap->lightType = type;
		shadowMap->castTranslucentShadows = castTranslucentShadows;
		*outIDs.shadowMap_id_PTR = shadowMap->shadowMap_id;
		if (castTranslucentShadows) {
			*outIDs.shadowMapTransCast_id_PTR = shadowMap->shadowMapTranslucent_id;
			*outIDs.shadowMapTransCol_id_PTR = shadowMap->shadowMapTranslucentColor_id;
			*outIDs.shadowMapTransRev_id_PTR = shadowMap->shadowMapTranslucentReveal_id;
		} else {
			*outIDs.shadowMapTransCast_id_PTR = 0;
			*outIDs.shadowMapTransCol_id_PTR = 0;
			*outIDs.shadowMapTransRev_id_PTR = 0;
		}
		VkFormat shadowMapFormat = VK_FORMAT_R16G16_UNORM;
		VkFormat shadowMapTranslucentColorFormat = VK_FORMAT_R5G6B5_UNORM_PACK16;
		VkFormat shadowMapTranslucentRevealFormat = VK_FORMAT_R8_UNORM;

		bool msSh = (type == ShadowMapLight::Directional && ProjectSystem::getGraphicsSettings().shadows.varianceDirectionalMultiSamples > 1)
			|| (type == ShadowMapLight::Spot && ProjectSystem::getGraphicsSettings().shadows.varianceSpotMultiSamples > 1);
		int mSamples = 1;
		if (msSh) {
			if (type == ShadowMapLight::Directional) mSamples = ProjectSystem::getGraphicsSettings().shadows.varianceDirectionalMultiSamples;
			if (type == ShadowMapLight::Spot) mSamples = ProjectSystem::getGraphicsSettings().shadows.varianceSpotMultiSamples;
		}
		mSamples = std::min(mSamples, static_cast<int>(engineDevice.getMaxUsableSampleCount()));
		uint32_t layerCount = 1;
		switch (convention) {
		case (ShadowMapConvention::DualParaboloid): layerCount = 2; break;
		case (ShadowMapConvention::PSSM2): layerCount = 2; break;
		case (ShadowMapConvention::PSSM3): layerCount = 3; break;
		case (ShadowMapConvention::PSSM4): layerCount = 4; break;
		case (ShadowMapConvention::Cube): layerCount = 6; break;
		}
		shadowMap->cameras.resize(layerCount);

		if (msSh) {
			shadowMap->varianceMSAttachment = new S3DRenderTarget(engineDevice, {
				shadowMapFormat,
				VK_IMAGE_ASPECT_COLOR_BIT,
				VK_IMAGE_VIEW_TYPE_2D,
				glm::ivec3(resolution, resolution, 1),
				VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
				VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
				S3DRenderTargetType::Color,
				false,
				static_cast<VkSampleCountFlagBits>(mSamples),
				false,
				layerCount
			});
			shadowMap->shadowMapSizeBytes += shadowMap->varianceMSAttachment->getResourceSize();
			shadowMap->varianceAttachment = new S3DRenderTarget(engineDevice, {
				shadowMapFormat,
				VK_IMAGE_ASPECT_COLOR_BIT,
				VK_IMAGE_VIEW_TYPE_2D,
				glm::ivec3(resolution, resolution, 1),
				VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				S3DRenderTargetType::Resolve,
				true,
				VK_SAMPLE_COUNT_1_BIT,
				false,
				layerCount
			});
			shadowMap->shadowMapSizeBytes += shadowMap->varianceAttachment->getResourceSize();
		} else {
			shadowMap->varianceAttachment = new S3DRenderTarget(engineDevice, {
				shadowMapFormat,
				VK_IMAGE_ASPECT_COLOR_BIT,
				VK_IMAGE_VIEW_TYPE_2D,
				glm::ivec3(resolution, resolution, 1),
				VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				S3DRenderTargetType::Color,
				true,
				VK_SAMPLE_COUNT_1_BIT,
				false,
				layerCount
			});
			shadowMap->shadowMapSizeBytes += shadowMap->varianceAttachment->getResourceSize();
		}
		if (castTranslucentShadows) {
			if (msSh) {
				shadowMap->varianceTranslucentCastMSAttachment = new S3DRenderTarget(engineDevice, {
					shadowMapFormat,
					VK_IMAGE_ASPECT_COLOR_BIT,
					VK_IMAGE_VIEW_TYPE_2D,
					glm::ivec3(resolution, resolution, 1),
					VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
					VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
					S3DRenderTargetType::Color,
					false,
					static_cast<VkSampleCountFlagBits>(mSamples),
					false,
					layerCount
					});
				shadowMap->shadowMapSizeBytes += shadowMap->varianceTranslucentCastMSAttachment->getResourceSize();
				shadowMap->varianceTranslucentCastAttachment = new S3DRenderTarget(engineDevice, {
					shadowMapFormat,
					VK_IMAGE_ASPECT_COLOR_BIT,
					VK_IMAGE_VIEW_TYPE_2D,
					glm::ivec3(resolution, resolution, 1),
					VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
					VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
					S3DRenderTargetType::Resolve,
					true,
					VK_SAMPLE_COUNT_1_BIT,
					false,
					layerCount
				});
				shadowMap->shadowMapSizeBytes += shadowMap->varianceTranslucentCastAttachment->getResourceSize();

				shadowMap->varianceTranslucentColorMSAttachment = new S3DRenderTarget(engineDevice, {
					shadowMapTranslucentColorFormat,
					VK_IMAGE_ASPECT_COLOR_BIT,
					VK_IMAGE_VIEW_TYPE_2D,
					glm::ivec3(resolution, resolution, 1),
					VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
					VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
					S3DRenderTargetType::Color,
					false,
					static_cast<VkSampleCountFlagBits>(mSamples),
					false,
					layerCount
					});
				shadowMap->shadowMapSizeBytes += shadowMap->varianceTranslucentColorMSAttachment->getResourceSize();
				shadowMap->varianceTranslucentColorAttachment = new S3DRenderTarget(engineDevice, {
					shadowMapTranslucentColorFormat,
					VK_IMAGE_ASPECT_COLOR_BIT,
					VK_IMAGE_VIEW_TYPE_2D,
					glm::ivec3(resolution, resolution, 1),
					VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
					VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
					S3DRenderTargetType::Resolve,
					true,
					VK_SAMPLE_COUNT_1_BIT,
					false,
					layerCount
				});
				shadowMap->shadowMapSizeBytes += shadowMap->varianceTranslucentColorAttachment->getResourceSize();

				shadowMap->varianceTranslucentRevealMSAttachment = new S3DRenderTarget(engineDevice, {
					shadowMapTranslucentRevealFormat,
					VK_IMAGE_ASPECT_COLOR_BIT,
					VK_IMAGE_VIEW_TYPE_2D,
					glm::ivec3(resolution, resolution, 1),
					VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
					VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
					S3DRenderTargetType::Color,
					false,
					static_cast<VkSampleCountFlagBits>(mSamples),
					false,
					layerCount
					});
				shadowMap->shadowMapSizeBytes += shadowMap->varianceTranslucentRevealMSAttachment->getResourceSize();
				shadowMap->varianceTranslucentRevealAttachment = new S3DRenderTarget(engineDevice, {
					shadowMapTranslucentRevealFormat,
					VK_IMAGE_ASPECT_COLOR_BIT,
					VK_IMAGE_VIEW_TYPE_2D,
					glm::ivec3(resolution, resolution, 1),
					VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
					VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
					S3DRenderTargetType::Resolve,
					true,
					VK_SAMPLE_COUNT_1_BIT,
					false,
					layerCount
				});
				shadowMap->shadowMapSizeBytes += shadowMap->varianceTranslucentRevealAttachment->getResourceSize();
			} else {
				shadowMap->varianceTranslucentCastAttachment = new S3DRenderTarget(engineDevice, {
					shadowMapFormat,
					VK_IMAGE_ASPECT_COLOR_BIT,
					VK_IMAGE_VIEW_TYPE_2D,
					glm::ivec3(resolution, resolution, 1),
					VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
					VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
					S3DRenderTargetType::Color,
					true,
					VK_SAMPLE_COUNT_1_BIT,
					false,
					layerCount
				});
				shadowMap->shadowMapSizeBytes += shadowMap->varianceTranslucentCastAttachment->getResourceSize();

				shadowMap->varianceTranslucentColorAttachment = new S3DRenderTarget(engineDevice, {
					shadowMapTranslucentColorFormat,
					VK_IMAGE_ASPECT_COLOR_BIT,
					VK_IMAGE_VIEW_TYPE_2D,
					glm::ivec3(resolution, resolution, 1),
					VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
					VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
					S3DRenderTargetType::Color,
					true,
					VK_SAMPLE_COUNT_1_BIT,
					false,
					layerCount
				});
				shadowMap->shadowMapSizeBytes += shadowMap->varianceTranslucentColorAttachment->getResourceSize();

				shadowMap->varianceTranslucentRevealAttachment = new S3DRenderTarget(engineDevice, {
					shadowMapTranslucentRevealFormat,
					VK_IMAGE_ASPECT_COLOR_BIT,
					VK_IMAGE_VIEW_TYPE_2D,
					glm::ivec3(resolution, resolution, 1),
					VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
					VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
					S3DRenderTargetType::Color,
					true,
					VK_SAMPLE_COUNT_1_BIT,
					false,
					layerCount
				});
				shadowMap->shadowMapSizeBytes += shadowMap->varianceTranslucentRevealAttachment->getResourceSize();
			}
		}
		shadowMap->depthAttachment = new S3DRenderTarget(engineDevice, {
			VK_FORMAT_D16_UNORM,
			VK_IMAGE_ASPECT_DEPTH_BIT,
			VK_IMAGE_VIEW_TYPE_2D,
			glm::ivec3(resolution, resolution, 1),
			VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
			VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
			S3DRenderTargetType::Depth,
			false,
			static_cast<VkSampleCountFlagBits>(mSamples),
			false,
			layerCount
			}
		);
		shadowMap->shadowMapSizeBytes += shadowMap->depthAttachment->getResourceSize();
		if (castTranslucentShadows) {
			shadowMap->depthTranslucentCastAttachment = new S3DRenderTarget(engineDevice, {
			VK_FORMAT_D16_UNORM,
			VK_IMAGE_ASPECT_DEPTH_BIT,
			VK_IMAGE_VIEW_TYPE_2D,
			glm::ivec3(resolution, resolution, 1),
			VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
			VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
			S3DRenderTargetType::Depth,
			false,
			static_cast<VkSampleCountFlagBits>(mSamples),
			false,
			layerCount
			}
			);
			shadowMap->shadowMapSizeBytes += shadowMap->depthTranslucentCastAttachment->getResourceSize();
		}
		AttachmentInfo depthAttachmentInfo{};
		depthAttachmentInfo.framebufferAttachment = shadowMap->depthAttachment;
		depthAttachmentInfo.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		depthAttachmentInfo.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

		AttachmentInfo varianceRenderMapAttachmentInfo{};
		varianceRenderMapAttachmentInfo.framebufferAttachment = msSh ? shadowMap->varianceMSAttachment : shadowMap->varianceAttachment;
		varianceRenderMapAttachmentInfo.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		varianceRenderMapAttachmentInfo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		varianceRenderMapAttachmentInfo.clear.color = { 1.f, 1.f };

		if (msSh) {
			AttachmentInfo varianceResolveMapAttachmentInfo{};
			varianceResolveMapAttachmentInfo.framebufferAttachment = shadowMap->varianceAttachment;
			varianceResolveMapAttachmentInfo.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			varianceResolveMapAttachmentInfo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

			shadowMap->renderPass = new S3DRenderPass(
				engineDevice, {
				depthAttachmentInfo,
				varianceRenderMapAttachmentInfo,
				varianceResolveMapAttachmentInfo
			});
			for (int i = 0; i < layerCount; i++)
				shadowMap->framebuffer[i] = new S3DFramebuffer(engineDevice, shadowMap->renderPass, { shadowMap->depthAttachment, shadowMap->varianceMSAttachment, shadowMap->varianceAttachment }, i);
		} else {
			shadowMap->renderPass = new S3DRenderPass(
				engineDevice, {
				depthAttachmentInfo,
				varianceRenderMapAttachmentInfo
			});

			for (int i = 0; i < layerCount; i++)
				shadowMap->framebuffer[i] = new S3DFramebuffer(engineDevice, shadowMap->renderPass, { shadowMap->depthAttachment, shadowMap->varianceAttachment }, i);
		}

		if (castTranslucentShadows) {
			AttachmentInfo depthTranslucentCastAttachmentInfo{};
			depthTranslucentCastAttachmentInfo.framebufferAttachment = shadowMap->depthTranslucentCastAttachment;
			depthTranslucentCastAttachmentInfo.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			depthTranslucentCastAttachmentInfo.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

			AttachmentInfo varianceTranslucentCastRenderMapAttachmentInfo{};
			varianceTranslucentCastRenderMapAttachmentInfo.framebufferAttachment = msSh ? shadowMap->varianceTranslucentCastMSAttachment : shadowMap->varianceTranslucentCastAttachment;
			varianceTranslucentCastRenderMapAttachmentInfo.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			varianceTranslucentCastRenderMapAttachmentInfo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			varianceTranslucentCastRenderMapAttachmentInfo.clear.color = { 1.f, 1.f };

			AttachmentInfo varianceTranslucentColorRenderMapAttachmentInfo{};
			varianceTranslucentColorRenderMapAttachmentInfo.framebufferAttachment = msSh ? shadowMap->varianceTranslucentColorMSAttachment : shadowMap->varianceTranslucentColorAttachment;
			varianceTranslucentColorRenderMapAttachmentInfo.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			varianceTranslucentColorRenderMapAttachmentInfo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			varianceTranslucentColorRenderMapAttachmentInfo.clear.color = { 1.f, 1.f, 1.f };
			AttachmentInfo varianceTranslucentRevealRenderMapAttachmentInfo{};
			varianceTranslucentRevealRenderMapAttachmentInfo.framebufferAttachment = msSh ? shadowMap->varianceTranslucentRevealMSAttachment : shadowMap->varianceTranslucentRevealAttachment;
			varianceTranslucentRevealRenderMapAttachmentInfo.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			varianceTranslucentRevealRenderMapAttachmentInfo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			varianceTranslucentRevealRenderMapAttachmentInfo.clear.color = { 1.f };

			if (msSh) {
				AttachmentInfo varianceTranslucentCastResolveMapAttachmentInfo{};
				varianceTranslucentCastResolveMapAttachmentInfo.framebufferAttachment = shadowMap->varianceTranslucentCastAttachment;
				varianceTranslucentCastResolveMapAttachmentInfo.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
				varianceTranslucentCastResolveMapAttachmentInfo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

				AttachmentInfo varianceTranslucentColorResolveMapAttachmentInfo{};
				varianceTranslucentColorResolveMapAttachmentInfo.framebufferAttachment = shadowMap->varianceTranslucentColorAttachment;
				varianceTranslucentColorResolveMapAttachmentInfo.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
				varianceTranslucentColorResolveMapAttachmentInfo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

				AttachmentInfo varianceTranslucentRevealResolveMapAttachmentInfo{};
				varianceTranslucentRevealResolveMapAttachmentInfo.framebufferAttachment = shadowMap->varianceTranslucentRevealAttachment;
				varianceTranslucentRevealResolveMapAttachmentInfo.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
				varianceTranslucentRevealResolveMapAttachmentInfo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

				shadowMap->translucentCastRenderPass = new S3DRenderPass(
					engineDevice, {
					depthAttachmentInfo,
					varianceTranslucentCastRenderMapAttachmentInfo,
					varianceTranslucentCastResolveMapAttachmentInfo
				});
				for (int i = 0; i < layerCount; i++)
					shadowMap->translucentCastFramebuffer[i] = new S3DFramebuffer(engineDevice, shadowMap->translucentCastRenderPass, { shadowMap->depthTranslucentCastAttachment, shadowMap->varianceTranslucentCastMSAttachment, shadowMap->varianceTranslucentCastAttachment }, i);

				shadowMap->translucentTintRenderPass = new S3DRenderPass(
						engineDevice, {
						varianceTranslucentColorRenderMapAttachmentInfo,
						varianceTranslucentRevealRenderMapAttachmentInfo,
						varianceTranslucentColorResolveMapAttachmentInfo,
						varianceTranslucentRevealResolveMapAttachmentInfo
					});
				for (int i = 0; i < layerCount; i++)
					shadowMap->translucentTintFramebuffer[i] = new S3DFramebuffer(engineDevice, shadowMap->translucentTintRenderPass, { shadowMap->varianceTranslucentColorMSAttachment, shadowMap->varianceTranslucentRevealMSAttachment, shadowMap->varianceTranslucentColorAttachment,  shadowMap->varianceTranslucentRevealAttachment }, i);
			} else {
				shadowMap->translucentCastRenderPass = new S3DRenderPass(
					engineDevice, {
					depthTranslucentCastAttachmentInfo,
					varianceTranslucentCastRenderMapAttachmentInfo
				});
				for (int i = 0; i < layerCount; i++)
					shadowMap->translucentCastFramebuffer[i] = new S3DFramebuffer(engineDevice, shadowMap->translucentCastRenderPass, { shadowMap->depthTranslucentCastAttachment, shadowMap->varianceTranslucentCastAttachment }, i);

				shadowMap->translucentTintRenderPass = new S3DRenderPass(
					engineDevice, {
					varianceTranslucentColorRenderMapAttachmentInfo,
					varianceTranslucentRevealRenderMapAttachmentInfo
				});

				for (int i = 0; i < layerCount; i++)
					shadowMap->translucentTintFramebuffer[i] = new S3DFramebuffer(engineDevice, shadowMap->translucentTintRenderPass, { shadowMap->varianceTranslucentColorAttachment,  shadowMap->varianceTranslucentRevealAttachment }, i);
			}
		}

		int blurQual = 0;
		if (type == ShadowMapLight::Directional) blurQual = ProjectSystem::getGraphicsSettings().shadows.varianceDirectionalBlurQuality;
		if (type == ShadowMapLight::Spot) blurQual = ProjectSystem::getGraphicsSettings().shadows.varianceSpotBlurQuality;

		if (blurQual == 2) {
			shadowMap->fastGaussian = make_uPtr<PostProcessingEffect>(
				engineDevice, glm::vec2(resolution),
				S3DShader{ ShaderType::Fragment,  "resources/shaders/postfx/fast_gaussian_9rg.frag", "postfx" },
				std::vector<VkDescriptorImageInfo>{ shadowMap->varianceAttachment->getDescriptor() },
				shadowMapFormat
			);
			shadowMap->shadowMapSizeBytes += shadowMap->fastGaussian->getAttachment()->getResourceSize();
			shadowMap->fastGaussianB = make_uPtr<PostProcessingEffect>(
				engineDevice, glm::vec2(resolution),
				S3DShader{ ShaderType::Fragment,  "resources/shaders/postfx/fast_gaussian_9rg.frag", "postfx" },
				std::vector<VkDescriptorImageInfo>{ shadowMap->fastGaussian->getAttachment()->getDescriptor() },
				shadowMapFormat
				);
			shadowMap->shadowMapSizeBytes += shadowMap->fastGaussianB->getAttachment()->getResourceSize();
			SmartDescriptorWriter(*resourceSystem->getDescriptor())
				.writeImage(BINDLESS_SAMPLER_BINDING, shadowMap->shadowMap_id,
					&shadowMap->fastGaussianB->getAttachment()->getDescriptor())
				.build();
		} else if (blurQual == 1) {
			shadowMap->fastGaussian = make_uPtr<PostProcessingEffect>(
				engineDevice, glm::vec2(resolution),
				S3DShader{ ShaderType::Fragment,  "resources/shaders/postfx/fast_gaussian_9xy_rg.frag", "postfx" },
				std::vector<VkDescriptorImageInfo>{ shadowMap->varianceAttachment->getDescriptor() },
				shadowMapFormat
				);
			shadowMap->shadowMapSizeBytes += shadowMap->fastGaussian->getAttachment()->getResourceSize();

			SmartDescriptorWriter(*resourceSystem->getDescriptor())
				.writeImage(BINDLESS_SAMPLER_BINDING, shadowMap->shadowMap_id,
					&shadowMap->fastGaussian->getAttachment()->getDescriptor())
				.build();
		} else {
			SmartDescriptorWriter(*resourceSystem->getDescriptor())
				.writeImage(BINDLESS_SAMPLER_BINDING, shadowMap->shadowMap_id,
					&shadowMap->varianceAttachment->getDescriptor())
				.build();
		}

		if (castTranslucentShadows) {
			if (blurQual == 2) {
				shadowMap->fastGaussianT = make_uPtr<PostProcessingEffect>(
					engineDevice, glm::vec2(resolution),
					S3DShader{ ShaderType::Fragment,  "resources/shaders/postfx/fast_gaussian_9rg.frag", "postfx" },
					std::vector<VkDescriptorImageInfo>{ shadowMap->varianceTranslucentCastAttachment->getDescriptor() },
					shadowMapFormat
					);
				shadowMap->shadowMapSizeBytes += shadowMap->fastGaussianT->getAttachment()->getResourceSize();
				shadowMap->fastGaussianTB = make_uPtr<PostProcessingEffect>(
					engineDevice, glm::vec2(resolution),
					S3DShader{ ShaderType::Fragment,  "resources/shaders/postfx/fast_gaussian_9rg.frag", "postfx" },
					std::vector<VkDescriptorImageInfo>{ shadowMap->fastGaussianT->getAttachment()->getDescriptor() },
					shadowMapFormat
					);
				shadowMap->shadowMapSizeBytes += shadowMap->fastGaussianTB->getAttachment()->getResourceSize();
				SmartDescriptorWriter(*resourceSystem->getDescriptor())
					.writeImage(BINDLESS_SAMPLER_BINDING, shadowMap->shadowMapTranslucent_id,
						&shadowMap->fastGaussianTB->getAttachment()->getDescriptor())
					.build();

				shadowMap->fastGaussianTC = make_uPtr<PostProcessingEffect>(
					engineDevice, glm::vec2(resolution),
					S3DShader{ ShaderType::Fragment,  "resources/shaders/postfx/fast_gaussian_9rgb.frag", "postfx" },
					std::vector<VkDescriptorImageInfo>{ shadowMap->varianceTranslucentColorAttachment->getDescriptor() },
					shadowMapTranslucentColorFormat
					);
				shadowMap->shadowMapSizeBytes += shadowMap->fastGaussianTC->getAttachment()->getResourceSize();
				shadowMap->fastGaussianTCB = make_uPtr<PostProcessingEffect>(
					engineDevice, glm::vec2(resolution),
					S3DShader{ ShaderType::Fragment,  "resources/shaders/postfx/fast_gaussian_9rgb.frag", "postfx" },
					std::vector<VkDescriptorImageInfo>{ shadowMap->fastGaussianTC->getAttachment()->getDescriptor() },
					shadowMapTranslucentColorFormat
					);
				shadowMap->shadowMapSizeBytes += shadowMap->fastGaussianTCB->getAttachment()->getResourceSize();
				SmartDescriptorWriter(*resourceSystem->getDescriptor())
					.writeImage(BINDLESS_SAMPLER_BINDING, shadowMap->shadowMapTranslucentColor_id,
						&shadowMap->fastGaussianTCB->getAttachment()->getDescriptor())
					.build();

				shadowMap->fastGaussianTR = make_uPtr<PostProcessingEffect>(
					engineDevice, glm::vec2(resolution),
					S3DShader{ ShaderType::Fragment,  "resources/shaders/postfx/fast_gaussian_9r.frag", "postfx" },
					std::vector<VkDescriptorImageInfo>{ shadowMap->varianceTranslucentRevealAttachment->getDescriptor() },
					shadowMapTranslucentRevealFormat
					);
				shadowMap->shadowMapSizeBytes += shadowMap->fastGaussianTR->getAttachment()->getResourceSize();
				shadowMap->fastGaussianTRB = make_uPtr<PostProcessingEffect>(
					engineDevice, glm::vec2(resolution),
					S3DShader{ ShaderType::Fragment,  "resources/shaders/postfx/fast_gaussian_9r.frag", "postfx" },
					std::vector<VkDescriptorImageInfo>{ shadowMap->fastGaussianTR->getAttachment()->getDescriptor() },
					shadowMapTranslucentRevealFormat
					);
				shadowMap->shadowMapSizeBytes += shadowMap->fastGaussianTRB->getAttachment()->getResourceSize();
				SmartDescriptorWriter(*resourceSystem->getDescriptor())
					.writeImage(BINDLESS_SAMPLER_BINDING, shadowMap->shadowMapTranslucentReveal_id,
						&shadowMap->fastGaussianTRB->getAttachment()->getDescriptor())
					.build();
			} else if (blurQual == 1) {
				shadowMap->fastGaussianT = make_uPtr<PostProcessingEffect>(
					engineDevice, glm::vec2(resolution),
					S3DShader{ ShaderType::Fragment,  "resources/shaders/postfx/fast_gaussian_9xy_rg.frag", "postfx" },
					std::vector<VkDescriptorImageInfo>{ shadowMap->varianceTranslucentCastAttachment->getDescriptor() },
					shadowMapFormat
					);
				shadowMap->shadowMapSizeBytes += shadowMap->fastGaussianT->getAttachment()->getResourceSize();
				SmartDescriptorWriter(*resourceSystem->getDescriptor())
					.writeImage(BINDLESS_SAMPLER_BINDING, shadowMap->shadowMapTranslucent_id,
						&shadowMap->fastGaussianT->getAttachment()->getDescriptor())
					.build();

				shadowMap->fastGaussianTC = make_uPtr<PostProcessingEffect>(
					engineDevice, glm::vec2(resolution),
					S3DShader{ ShaderType::Fragment,  "resources/shaders/postfx/fast_gaussian_9xy_rgb.frag", "postfx" },
					std::vector<VkDescriptorImageInfo>{ shadowMap->varianceTranslucentColorAttachment->getDescriptor() },
					shadowMapTranslucentColorFormat
					);
				shadowMap->shadowMapSizeBytes += shadowMap->fastGaussianTC->getAttachment()->getResourceSize();
				SmartDescriptorWriter(*resourceSystem->getDescriptor())
					.writeImage(BINDLESS_SAMPLER_BINDING, shadowMap->shadowMapTranslucentColor_id,
						&shadowMap->fastGaussianTC->getAttachment()->getDescriptor())
					.build();

				shadowMap->fastGaussianTR = make_uPtr<PostProcessingEffect>(
					engineDevice, glm::vec2(resolution),
					S3DShader{ ShaderType::Fragment,  "resources/shaders/postfx/fast_gaussian_9xy_r.frag", "postfx" },
					std::vector<VkDescriptorImageInfo>{ shadowMap->varianceTranslucentRevealAttachment->getDescriptor() },
					shadowMapTranslucentRevealFormat
					);
				shadowMap->shadowMapSizeBytes += shadowMap->fastGaussianTR->getAttachment()->getResourceSize();
				SmartDescriptorWriter(*resourceSystem->getDescriptor())
					.writeImage(BINDLESS_SAMPLER_BINDING, shadowMap->shadowMapTranslucentReveal_id,
						&shadowMap->fastGaussianTR->getAttachment()->getDescriptor())
					.build();
			} else {
				SmartDescriptorWriter(*resourceSystem->getDescriptor())
					.writeImage(BINDLESS_SAMPLER_BINDING, shadowMap->shadowMapTranslucent_id,
						&shadowMap->varianceTranslucentCastAttachment->getDescriptor())
					.build();

				SmartDescriptorWriter(*resourceSystem->getDescriptor())
					.writeImage(BINDLESS_SAMPLER_BINDING, shadowMap->shadowMapTranslucentColor_id,
						&shadowMap->varianceTranslucentColorAttachment->getDescriptor())
					.build();

				SmartDescriptorWriter(*resourceSystem->getDescriptor())
					.writeImage(BINDLESS_SAMPLER_BINDING, shadowMap->shadowMapTranslucentReveal_id,
						&shadowMap->varianceTranslucentRevealAttachment->getDescriptor())
					.build();
			}
		}

		std::vector<std::string> sdefs{};
		sdefs.push_back("VarianceShadow");
		std::string file = "shadow_variance";
		std::string shadowGenFile{};

		switch (convention) {
		case (ShadowMapConvention::Conventional):
		case (ShadowMapConvention::PSSM2):
		case (ShadowMapConvention::PSSM3):
		case (ShadowMapConvention::PSSM4):
			shadowGenFile = "resources/shaders/shadows/shadow";
			break;
		case (ShadowMapConvention::DualParaboloid):
		case (ShadowMapConvention::Paraboloid):
			shadowGenFile = "resources/shaders/shadows/shadow_paraboloid";
			file += "_paraboloid"; break;
		case (ShadowMapConvention::Cube):
			shadowGenFile = "resources/shaders/shadows/shadow_cube";
			file += "_cube"; break;
		}

		S3DShaderSpecialization specialization{};
		uint32_t shaderLightType;
		switch (shadowMap->lightType) {
		case(ShadowMapLight::Directional):
			shaderLightType = 0;
			break;
		case(ShadowMapLight::Spot):
			shaderLightType = 1;
			break;
		case(ShadowMapLight::Point):
			shaderLightType = 2;
			break;
		}
		specialization.addConstant(0, &shaderLightType, sizeof(uint32_t));
		std::string fileOpaque = file;
		if (generateRSM) {
			fileOpaque += "_rsm";
		}

		S3DGraphicsPipelineConfigInfo opaque_pipelineConfig = S3DGraphicsPipelineConfigInfo();
		opaque_pipelineConfig.setSampleCount(static_cast<VkSampleCountFlagBits>(mSamples));
		opaque_pipelineConfig.enableVertexDescriptions(VertexDescriptionOptions_Position | VertexDescriptionOptions_UV);

		opaque_pipelineConfig.renderPass = shadowMap->renderPass->getRenderPass();

		shadowMap->castPipelines = new ShadowMapPipelinePermutations(engineDevice,
			shadowGenFile, S3DShaderSpecialization{}, fileOpaque,
			shadowGenFile, specialization, fileOpaque,
			opaque_pipelineConfig, sdefs, (convention == ShadowMapConvention::Cube || convention == ShadowMapConvention::Paraboloid || convention == ShadowMapConvention::DualParaboloid) ? omniPipelineLayouts : pipelineLayouts,
			true
		);
		opaque_pipelineConfig.pipelineLayout = terrainPipelineLayout;

		if (shadowMap->castTerrainShadows) {

			shadowMap->castTerrainPipeline = new S3DGraphicsPipeline(engineDevice, {
					S3DShader{ ShaderType::Vertex, "resources/shaders/shadows/terrain_shadow.vert", "shadows", {}, sdefs, "terrain_" + fileOpaque + ".vert" },
					S3DShader{ ShaderType::Fragment, "resources/shaders/shadows/terrain_shadow.frag", "shadows", {}, sdefs, "terrain_" + fileOpaque + ".frag" }
				}, opaque_pipelineConfig
			);

		}
		if (shadowMap->castTranslucentShadows) {
			S3DGraphicsPipelineConfigInfo translucentCast_pipelineConfig = S3DGraphicsPipelineConfigInfo();
			translucentCast_pipelineConfig.setSampleCount(static_cast<VkSampleCountFlagBits>(mSamples));
			translucentCast_pipelineConfig.enableVertexDescriptions(VertexDescriptionOptions_Position | VertexDescriptionOptions_UV);

			translucentCast_pipelineConfig.renderPass = shadowMap->translucentCastRenderPass->getRenderPass();

			shadowMap->translucentCastPipelines = new ShadowMapPipelinePermutations(engineDevice, 
				shadowGenFile, S3DShaderSpecialization{}, fileOpaque,
				shadowGenFile, specialization, fileOpaque,
				translucentCast_pipelineConfig, sdefs, (convention == ShadowMapConvention::Cube || convention == ShadowMapConvention::Paraboloid || convention == ShadowMapConvention::DualParaboloid) ? omniPipelineLayouts : pipelineLayouts,
				true			
			);

			S3DGraphicsPipelineConfigInfo translucentTint_pipelineConfig = S3DGraphicsPipelineConfigInfo(2);
			translucentTint_pipelineConfig.setSampleCount(static_cast<VkSampleCountFlagBits>(mSamples));
			translucentTint_pipelineConfig.enableVertexDescriptions(VertexDescriptionOptions_Position | VertexDescriptionOptions_UV);
			translucentTint_pipelineConfig.disableDepthWrite();
			translucentTint_pipelineConfig.enableAlphaBlending(0);
			translucentTint_pipelineConfig.enableAlphaBlending(1);
			translucentTint_pipelineConfig.colorBlendAttachments[0].colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT;
			translucentTint_pipelineConfig.colorBlendAttachments[0].srcColorBlendFactor = VK_BLEND_FACTOR_ZERO;
			translucentTint_pipelineConfig.colorBlendAttachments[0].dstColorBlendFactor = VK_BLEND_FACTOR_SRC_COLOR;
			translucentTint_pipelineConfig.colorBlendAttachments[1].colorWriteMask = VK_COLOR_COMPONENT_R_BIT;
			translucentTint_pipelineConfig.colorBlendAttachments[1].srcColorBlendFactor = VK_BLEND_FACTOR_ZERO;
			translucentTint_pipelineConfig.colorBlendAttachments[1].dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR;

			translucentTint_pipelineConfig.renderPass = shadowMap->translucentTintRenderPass->getRenderPass();

			shadowMap->translucentPipelines = new ShadowMapPipelinePermutations(engineDevice,
				shadowGenFile+ "_translucent", {}, file + "_translucent",
				shadowGenFile+ "_translucent", {}, file + "_translucent",
				translucentTint_pipelineConfig, sdefs, (convention == ShadowMapConvention::Cube || convention == ShadowMapConvention::Paraboloid || convention == ShadowMapConvention::DualParaboloid) ? omniPipelineLayouts : pipelineLayouts,
				true
			);
		}
		return shadowMap;
	}
	ShadowPCFMap* ShadowMappingSystem::createPCFShadowMap(int resolution, ShadowMapIDPtrs outIDs, ShadowMapLight type, ShadowMapConvention convention, bool castTranslucentShadows, bool generateRSM, bool allowInstancedDrawing) {
		ShadowPCFMap* shadowMap = new ShadowPCFMap(resourceSystem, convention, engineDevice, shadowMapBufferDataDescriptorSetLayout);
		shadowMap->castTerrainShadows = type == ShadowMapLight::Directional;
		shadowMap->gpuDrawTime = make_uPtr<TelemetryGPUMetric>(shadowMapTelemetry, "Shadow Map", "Draw GPU");
		shadowMap->cpuDrawTime = make_uPtr<TelemetryCPUMetric>(shadowMapTelemetry, "Shadow Map", "Draw CPU");
		shadowMap->lightType = type;
		*outIDs.shadowMap_id_PTR = shadowMap->shadowMap_id;
		if (castTranslucentShadows) {
			*outIDs.shadowMapTransCast_id_PTR = shadowMap->shadowMapTranslucent_id;
			*outIDs.shadowMapTransCol_id_PTR = shadowMap->shadowMapTranslucentColor_id;
			*outIDs.shadowMapTransRev_id_PTR = shadowMap->shadowMapTranslucentReveal_id;
		} else {
			*outIDs.shadowMapTransCast_id_PTR = 0;
			*outIDs.shadowMapTransCol_id_PTR = 0;
			*outIDs.shadowMapTransRev_id_PTR = 0;
		}
		shadowMap->castTranslucentShadows = castTranslucentShadows;

		bool instanceLayers = false;
		bool parallelSplit = false;

		uint32_t layerCount = 1;
		VkImageViewType viewType = VK_IMAGE_VIEW_TYPE_2D;
		switch (convention) {
		case (ShadowMapConvention::DualParaboloid):
			layerCount = 2;
			viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
			instanceLayers = allowInstancedDrawing;
			break;
		case (ShadowMapConvention::PSSM2):
			layerCount = 2;
			viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
			parallelSplit = true;
			break;
		case (ShadowMapConvention::PSSM3):
			layerCount = 3;
			viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
			parallelSplit = true;
			break;
		case (ShadowMapConvention::PSSM4):
			layerCount = 4;
			viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
			parallelSplit = true;
			break;
		case (ShadowMapConvention::Cube):
			layerCount = 6;
			viewType = VK_IMAGE_VIEW_TYPE_CUBE;
			instanceLayers = allowInstancedDrawing;
			break;
		}
		shadowMap->cameras.resize(layerCount);
		shadowMap->depthAttachment = new S3DRenderTarget(engineDevice, {
			VK_FORMAT_D16_UNORM,
			VK_IMAGE_ASPECT_DEPTH_BIT,
			viewType,
			glm::ivec3(resolution, resolution, 1),
			VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
			S3DRenderTargetType::Depth,
			true,
			VK_SAMPLE_COUNT_1_BIT,
			true,
			layerCount,
			1,
			instanceLayers
			}
		); // hardware accelerated PCF benefits from linear sampling

		shadowMap->shadowMapSizeBytes += shadowMap->depthAttachment->getResourceSize();
		AttachmentInfo depthAttachmentInfo{};
		depthAttachmentInfo.framebufferAttachment = shadowMap->depthAttachment;
		depthAttachmentInfo.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		depthAttachmentInfo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

		shadowMap->numPasses = instanceLayers ? 1 : layerCount;
		shadowMap->instancingCount = instanceLayers ? layerCount : 1;
		if (generateRSM) {
			shadowMap->reflectiveShadowMap = createRSM(resolution, std::min(resolution, 256), VK_SAMPLE_COUNT_1_BIT, viewType, layerCount);

			AttachmentInfo rsmFluxAttachmentInfo{};
			rsmFluxAttachmentInfo.framebufferAttachment = shadowMap->reflectiveShadowMap->fluxMap;
			rsmFluxAttachmentInfo.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			rsmFluxAttachmentInfo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			rsmFluxAttachmentInfo.clear.color = { 0.f, 0.f, 0.f };
			AttachmentInfo rsmNormalAttachmentInfo{};
			rsmNormalAttachmentInfo.framebufferAttachment = shadowMap->reflectiveShadowMap->normalMap;
			rsmNormalAttachmentInfo.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			rsmNormalAttachmentInfo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			rsmNormalAttachmentInfo.clear.color = { 0.f, 0.f, 0.f, 0.f };

			shadowMap->renderPass = new S3DRenderPass(
				engineDevice, {
				depthAttachmentInfo,
				rsmFluxAttachmentInfo,
				rsmNormalAttachmentInfo
			});


			for (int i = 0; i < shadowMap->numPasses; i++)
				shadowMap->framebuffer[i] = new S3DFramebuffer(engineDevice, shadowMap->renderPass, { shadowMap->depthAttachment, shadowMap->reflectiveShadowMap->fluxMap, shadowMap->reflectiveShadowMap->normalMap }, i);
		} else {
			shadowMap->renderPass = new S3DRenderPass(
				engineDevice, {
				depthAttachmentInfo
			});


			for (int i = 0; i < shadowMap->numPasses; i++)
				shadowMap->framebuffer[i] = new S3DFramebuffer(engineDevice, shadowMap->renderPass, { shadowMap->depthAttachment }, i);
		}

		SmartDescriptorWriter(*resourceSystem->getDescriptor())
			.writeImage(BINDLESS_SAMPLER_BINDING, shadowMap->shadowMap_id,
				&shadowMap->depthAttachment->getDescriptor())
			.build();

		if (shadowMap->castTranslucentShadows) {

			shadowMap->depthTranslucentAttachment = new S3DRenderTarget(engineDevice, {
				VK_FORMAT_D16_UNORM,
				VK_IMAGE_ASPECT_DEPTH_BIT,
				viewType,
				glm::ivec3(resolution, resolution, 1),
				VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				S3DRenderTargetType::Depth,
				false,
				VK_SAMPLE_COUNT_1_BIT,
				false,
				layerCount,
				1,
				instanceLayers
				}
			); // hardware accelerated PCF benefits from linear sampling
			shadowMap->shadowMapSizeBytes += shadowMap->depthTranslucentAttachment->getResourceSize();

			AttachmentInfo depthTranslucentAttachmentInfo{};
			depthTranslucentAttachmentInfo.framebufferAttachment = shadowMap->depthTranslucentAttachment;
			depthTranslucentAttachmentInfo.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			depthTranslucentAttachmentInfo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

			shadowMap->translucentCastRenderPass = new S3DRenderPass(
				engineDevice, {
				depthTranslucentAttachmentInfo
			});

			for (int i = 0; i < shadowMap->numPasses; i++)
				shadowMap->translucentCastFramebuffer[i] = new S3DFramebuffer(engineDevice, shadowMap->translucentCastRenderPass, { shadowMap->depthTranslucentAttachment }, i);

			SmartDescriptorWriter(*resourceSystem->getDescriptor())
				.writeImage(BINDLESS_SAMPLER_BINDING, shadowMap->shadowMapTranslucent_id,
					&shadowMap->depthTranslucentAttachment->getDescriptor())
				.build();

			shadowMap->translucentColorAttachment = new S3DRenderTarget(engineDevice, {
				VK_FORMAT_R5G6B5_UNORM_PACK16, // hopefully we can get away with 16 bit colours
				VK_IMAGE_ASPECT_COLOR_BIT,
				viewType,
				glm::ivec3(resolution, resolution, 1),
				VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				S3DRenderTargetType::Color,
				true,
				VK_SAMPLE_COUNT_1_BIT,
				false,
				layerCount,
				1,
				instanceLayers
				}
			);
			shadowMap->shadowMapSizeBytes += shadowMap->translucentColorAttachment->getResourceSize();

			shadowMap->translucentRevealAttachment = new S3DRenderTarget(engineDevice, {
				VK_FORMAT_R8_UNORM,
				VK_IMAGE_ASPECT_COLOR_BIT,
				viewType,
				glm::ivec3(resolution, resolution, 1),
				VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				S3DRenderTargetType::Color,
				true,
				VK_SAMPLE_COUNT_1_BIT,
				false,
				layerCount,
				1,
				instanceLayers
				}
			);
			shadowMap->shadowMapSizeBytes += shadowMap->translucentRevealAttachment->getResourceSize();

			AttachmentInfo transColAttachmentInfo{};
			transColAttachmentInfo.framebufferAttachment = shadowMap->translucentColorAttachment;
			transColAttachmentInfo.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			transColAttachmentInfo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			transColAttachmentInfo.clear.color = { 1.0, 1.0, 1.0 };

			AttachmentInfo transRevAttachmentInfo{};
			transRevAttachmentInfo.framebufferAttachment = shadowMap->translucentRevealAttachment;
			transRevAttachmentInfo.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			transRevAttachmentInfo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			transRevAttachmentInfo.clear.color.float32[0] = 1.0;

			shadowMap->translucentTintRenderPass = new S3DRenderPass(
				engineDevice, {
				transColAttachmentInfo,
				transRevAttachmentInfo
			});

			for (int i = 0; i < shadowMap->numPasses; i++)
				shadowMap->translucentTintFramebuffer[i] = new S3DFramebuffer(engineDevice, shadowMap->translucentTintRenderPass, { shadowMap->translucentColorAttachment,  shadowMap->translucentRevealAttachment }, i);

			SmartDescriptorWriter(*resourceSystem->getDescriptor())
				.writeImage(BINDLESS_SAMPLER_BINDING, shadowMap->shadowMapTranslucentColor_id,
					&shadowMap->translucentColorAttachment->getDescriptor())
				.build();
			SmartDescriptorWriter(*resourceSystem->getDescriptor())
				.writeImage(BINDLESS_SAMPLER_BINDING, shadowMap->shadowMapTranslucentReveal_id,
					&shadowMap->translucentRevealAttachment->getDescriptor())
				.build();
		}

		std::vector<std::string> sdefs{};
		sdefs.push_back("PCFShadow");
		std::string file = "shadow_pcf";
		std::string shadowGenVertFile{};
		std::string shadowGenFragFile{};
		switch (convention) {
		case (ShadowMapConvention::PSSM2):
		case (ShadowMapConvention::PSSM3):
		case (ShadowMapConvention::PSSM4):
		case (ShadowMapConvention::Conventional):
			shadowGenFragFile = shadowGenVertFile = "resources/shaders/shadows/shadow";
			break;
		case (ShadowMapConvention::DualParaboloid):
		case (ShadowMapConvention::Paraboloid):
			shadowGenVertFile = "resources/shaders/shadows/shadow_paraboloid";
			shadowGenFragFile = "resources/shaders/shadows/shadow_omni";
			file += "_paraboloid"; break;
		case (ShadowMapConvention::Cube):
			shadowGenVertFile = "resources/shaders/shadows/shadow_cube";
			shadowGenFragFile = "resources/shaders/shadows/shadow_omni";
			file += "_cube"; break;
		}

		S3DShaderSpecialization specialization{};
		uint32_t shaderLightType;
		switch (shadowMap->lightType) {
		case(ShadowMapLight::Directional):
			shaderLightType = 0;
			break;
		case(ShadowMapLight::Spot):
			shaderLightType = 1;
			break;
		case(ShadowMapLight::Point):
			shaderLightType = 2;
			break;
		}
		specialization.addConstant(0, &shaderLightType, sizeof(uint32_t));
		if (allowInstancedDrawing) {
			file += "_instanced";
			sdefs.push_back("InstancedDrawing");
		}
		std::string fileOpaque = file;
		if (generateRSM) {
			fileOpaque += "_rsm";
			sdefs.push_back("ReflectiveShadow");
		}

		bool omni = (convention == ShadowMapConvention::Cube || convention == ShadowMapConvention::Paraboloid || convention == ShadowMapConvention::DualParaboloid);
		S3DGraphicsPipelineConfigInfo opaque_pipelineConfig = S3DGraphicsPipelineConfigInfo(generateRSM ? 2 : 0);
		opaque_pipelineConfig.enableVertexDescriptions(VertexDescriptionOptions_Position | VertexDescriptionOptions_UV);
		opaque_pipelineConfig.rasterizationInfo.depthBiasEnable = true;
		opaque_pipelineConfig.dynamicStateEnables.push_back(VK_DYNAMIC_STATE_DEPTH_BIAS);

		opaque_pipelineConfig.renderPass = shadowMap->renderPass->getRenderPass();

		shadowMap->castPipelines = new ShadowMapPipelinePermutations(engineDevice,
			shadowGenVertFile, S3DShaderSpecialization{}, fileOpaque,
			shadowGenFragFile, specialization, fileOpaque,
			opaque_pipelineConfig, sdefs, omni ? omniPipelineLayouts : pipelineLayouts,
			generateRSM || omni
		);

		if (shadowMap->castTerrainShadows) {
			opaque_pipelineConfig.rasterizationInfo.depthBiasEnable = false;
			opaque_pipelineConfig.pipelineLayout = terrainPipelineLayout;
			opaque_pipelineConfig.setCullMode(VK_CULL_MODE_FRONT_BIT);
			shadowMap->castTerrainPipeline = new S3DGraphicsPipeline(engineDevice, {
					S3DShader{ ShaderType::Vertex, "resources/shaders/shadows/terrain_shadow.vert", "shadows", {}, sdefs, "terrain_" + fileOpaque + ".vert" }
				}, opaque_pipelineConfig
			);
		}

		if (shadowMap->castTranslucentShadows) {
			S3DGraphicsPipelineConfigInfo translucentCast_pipelineConfig = S3DGraphicsPipelineConfigInfo(0);
			translucentCast_pipelineConfig.enableVertexDescriptions(VertexDescriptionOptions_Position | VertexDescriptionOptions_UV);
			translucentCast_pipelineConfig.renderPass = shadowMap->translucentCastRenderPass->getRenderPass();

			sdefs.pop_back();
			shadowMap->translucentCastPipelines = new  ShadowMapPipelinePermutations(engineDevice,
				shadowGenVertFile, S3DShaderSpecialization{}, fileOpaque,
				shadowGenFragFile, specialization, fileOpaque,
				translucentCast_pipelineConfig, sdefs, (convention == ShadowMapConvention::Cube || convention == ShadowMapConvention::Paraboloid || convention == ShadowMapConvention::DualParaboloid) ? omniPipelineLayouts : pipelineLayouts,
				false
			);

			S3DGraphicsPipelineConfigInfo translucentTint_pipelineConfig = S3DGraphicsPipelineConfigInfo(2);
			translucentTint_pipelineConfig.enableVertexDescriptions(VertexDescriptionOptions_Position | VertexDescriptionOptions_UV);
			translucentTint_pipelineConfig.disableDepthTest();
			translucentTint_pipelineConfig.enableAlphaBlending(0);
			translucentTint_pipelineConfig.enableAlphaBlending(1);
			translucentTint_pipelineConfig.colorBlendAttachments[0].colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT;
			translucentTint_pipelineConfig.colorBlendAttachments[0].srcColorBlendFactor = VK_BLEND_FACTOR_ZERO;
			translucentTint_pipelineConfig.colorBlendAttachments[0].dstColorBlendFactor = VK_BLEND_FACTOR_SRC_COLOR;
			translucentTint_pipelineConfig.colorBlendAttachments[1].colorWriteMask = VK_COLOR_COMPONENT_R_BIT;
			translucentTint_pipelineConfig.colorBlendAttachments[1].srcColorBlendFactor = VK_BLEND_FACTOR_ZERO;
			translucentTint_pipelineConfig.colorBlendAttachments[1].dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR;

			translucentTint_pipelineConfig.renderPass = shadowMap->translucentTintRenderPass->getRenderPass();

			shadowMap->translucentPipelines = shadowMap->translucentPipelines = new ShadowMapPipelinePermutations(engineDevice,
				shadowGenVertFile + "_translucent", {}, file + "_translucent",
				shadowGenFragFile + "_translucent", {}, file + "_translucent",
				translucentTint_pipelineConfig, sdefs, (convention == ShadowMapConvention::Cube || convention == ShadowMapConvention::Paraboloid || convention == ShadowMapConvention::DualParaboloid) ? omniPipelineLayouts : pipelineLayouts,
				true
			);
		}
		return shadowMap;
	}

	void ShadowMappingSystem::createPipelineLayouts() {
		VkPushConstantRange pushConstantRange{};
		pushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		pushConstantRange.offset = 0;
		pushConstantRange.size = sizeof(StaticShadowTexturedPushConstants);

		VkPushConstantRange dynamicPushConstantRange{};
		dynamicPushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		dynamicPushConstantRange.offset = 0;
		dynamicPushConstantRange.size = sizeof(DynamicShadowTexturedPushConstants);

		VkPushConstantRange pushConstantRangeOmni{};
		pushConstantRangeOmni.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		pushConstantRangeOmni.offset = 0;
		pushConstantRangeOmni.size = sizeof(StaticShadowMMTexturedPushConstants);

		VkPushConstantRange dynamicPushConstantRangeOmni{};
		dynamicPushConstantRangeOmni.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		dynamicPushConstantRangeOmni.offset = 0;
		dynamicPushConstantRangeOmni.size = sizeof(DynamicShadowMMTexturedPushConstants);

		VkDescriptorSetLayout layouts[3]{ resourceSystem->getDescriptor()->getLayout(), shadowMapBufferDataDescriptorSetLayout->getDescriptorSetLayout(), riggedSkeletonDescriptorSetLayout };

		for (auto class_ : ProjectSystem::getEngineSettings().allowedMaterialPermutations) {
			if (class_ & SurfaceMaterialPipelineClassPermutationOptions_Translucent) continue;

			bool dyn = class_ & SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates;
			bool rig = class_ & SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms;
			assert(pipelineLayouts.find(class_) == pipelineLayouts.end());
			assert(omniPipelineLayouts.find(class_) == omniPipelineLayouts.end());

			VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
			pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
			pipelineLayoutInfo.setLayoutCount = rig ? 3 : 2;
			pipelineLayoutInfo.pSetLayouts = layouts;
			pipelineLayoutInfo.pushConstantRangeCount = 1;
			pipelineLayoutInfo.pPushConstantRanges = dyn ? &dynamicPushConstantRange : &pushConstantRange;
			VK_ASSERT(vkCreatePipelineLayout(engineDevice.device(), &pipelineLayoutInfo, nullptr, &pipelineLayouts[class_]), "failed to create pipeline layout!");

			VkPipelineLayoutCreateInfo pipelineLayoutOmniInfo{};
			pipelineLayoutOmniInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
			pipelineLayoutOmniInfo.setLayoutCount = rig ? 3 : 2;
			pipelineLayoutOmniInfo.pSetLayouts = layouts;
			pipelineLayoutOmniInfo.pushConstantRangeCount = 1;
			pipelineLayoutOmniInfo.pPushConstantRanges = dyn ? &dynamicPushConstantRangeOmni : &pushConstantRangeOmni;
			VK_ASSERT(vkCreatePipelineLayout(engineDevice.device(), &pipelineLayoutOmniInfo, nullptr, &omniPipelineLayouts[class_]), "failed to create pipeline layout!");
		}

		VkPushConstantRange terrainPushConstantRange{};
		terrainPushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT;
		terrainPushConstantRange.offset = 0;
		terrainPushConstantRange.size = sizeof(TerrainPushData);
		
		VkDescriptorSetLayout layoutsTerrain[3]{ resourceSystem->getDescriptor()->getLayout(), shadowMapBufferDataDescriptorSetLayout->getDescriptorSetLayout(), terrainDescriptorSetLayout };
		VkPipelineLayoutCreateInfo pipelineTerrainLayoutInfo{};
		pipelineTerrainLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		pipelineTerrainLayoutInfo.setLayoutCount = 3;
		pipelineTerrainLayoutInfo.pSetLayouts = layoutsTerrain;
		pipelineTerrainLayoutInfo.pushConstantRangeCount = 1;
		pipelineTerrainLayoutInfo.pPushConstantRanges = &terrainPushConstantRange;
		VK_ASSERT(vkCreatePipelineLayout(engineDevice.device(), &pipelineTerrainLayoutInfo, nullptr, &terrainPipelineLayout), "failed to create pipeline layout!");
	}

	void ShadowMappingSystem::pushShadowMapConstants(
		FrameInfo& frameInfo, SurfaceMaterialRenderInfo& surfaceMaterialInfo, SurfaceMaterialPipelineClassPermutationFlags flags,
		bool omni, uint32_t layerIndex, uint32_t materialIndex, bool dynamic, float farZ
	) {
		if (!omni) {
			if (dynamic) {
				DynamicShadowTexturedPushConstants push{};
				push.bufferID = surfaceMaterialInfo.mesh->actorBufferIndices[frameInfo.frameIndex];
				assert(resourceSystem->getDescriptor()->isAllocated(BINDLESS_SSBO_BINDING, push.bufferID));
				push.materialID = frameInfo.resourceSystem->retrieveMaterial(surfaceMaterialInfo.mesh->materials[materialIndex])->getShaderMaterialID();
				push.layer = layerIndex;
				push.maxPlane = farZ;
				vkCmdPushConstants(
					frameInfo.commandBuffer,
					pipelineLayouts[flags],
					VK_SHADER_STAGE_VERTEX_BIT,
					0,
					sizeof(DynamicShadowTexturedPushConstants),
					&push
				);
			} else {
				StaticShadowTexturedPushConstants push{};
				push.model = surfaceMaterialInfo.mesh->transform;
				push.materialID = frameInfo.resourceSystem->retrieveMaterial(surfaceMaterialInfo.mesh->materials[materialIndex])->getShaderMaterialID();
				push.layer = layerIndex;
				push.maxPlane = farZ;
				vkCmdPushConstants(
					frameInfo.commandBuffer,
					pipelineLayouts[flags],
					VK_SHADER_STAGE_VERTEX_BIT,
					0,
					sizeof(StaticShadowTexturedPushConstants),
					&push
				);
			}
		} else {
			if (dynamic) {
				DynamicShadowMMTexturedPushConstants push{};
				push.bufferID = surfaceMaterialInfo.mesh->actorBufferIndices[frameInfo.frameIndex];
				assert(resourceSystem->getDescriptor()->isAllocated(BINDLESS_SSBO_BINDING, push.bufferID));
				push.materialID = frameInfo.resourceSystem->retrieveMaterial(surfaceMaterialInfo.mesh->materials[materialIndex])->getShaderMaterialID();
				push.layer = layerIndex;
				push.maxPlane = farZ;
				vkCmdPushConstants(
					frameInfo.commandBuffer,
					pipelineLayouts[flags],
					VK_SHADER_STAGE_VERTEX_BIT,
					0,
					sizeof(DynamicShadowMMTexturedPushConstants),
					&push
				);
			} else {
				StaticShadowMMTexturedPushConstants push{};
				push.model = surfaceMaterialInfo.mesh->transform;
				push.materialID = frameInfo.resourceSystem->retrieveMaterial(surfaceMaterialInfo.mesh->materials[materialIndex])->getShaderMaterialID();
				push.layer = layerIndex;
				push.maxPlane = farZ;
				vkCmdPushConstants(
					frameInfo.commandBuffer,
					omniPipelineLayouts[flags],
					VK_SHADER_STAGE_VERTEX_BIT,
					0,
					sizeof(StaticShadowMMTexturedPushConstants),
					&push
				);
			}
		}
	}

	void ShadowMappingSystem::renderShadowMap(FrameInfo& frameInfo, ShadowMap* shadowMap, float slopeBias) {
		shadowMap->gpuDrawTime->record(frameInfo.commandBuffer);
		shadowMap->cpuDrawTime->record();
		VkDescriptorSet totalSets[2]{
			resourceSystem->getDescriptor()->getDescriptor(),
			shadowMap->shadowMapBufferInfoDescriptorSet[frameInfo.frameIndex]
		};

		for (int i = 0; i < shadowMap->numPasses; i++) {
			MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::GENERIC, std::string("SHADOW_MAP::conv=" + std::string(magic_enum::enum_name(shadowMap->convention)) + ",type=" + std::string(dynamic_cast<ShadowPCFMap*>(shadowMap) ? "PCF" : "VSM") + ",pass=" + std::to_string(i)).c_str(), frameInfo.commandBuffer);

			if (frameInfo.renderObjects.terrainList->getTerrain()) {
				frameInfo.renderObjects.terrainList->getTerrain()->updateVisibilityCalcs(frameInfo.commandBuffer, shadowMap->cameras[i], 16.0f, i * 1.55f);
			}
			
			shadowMap->renderPass->beginRenderPass(frameInfo.commandBuffer, shadowMap->framebuffer[i], shadowMap->instancingCount);

			if (shadowMap->castTerrainPipeline) {
				if (frameInfo.renderObjects.terrainList->getTerrain()) {
					shadowMap->castTerrainPipeline->bind(frameInfo.commandBuffer);
					VkDescriptorSet sets[3]{
						frameInfo.resourceSystem->getDescriptor()->getDescriptor(),
						shadowMap->shadowMapBufferInfoDescriptorSet[frameInfo.frameIndex],
						frameInfo.renderObjects.terrainList->getTerrain()->terrainDescriptor
					};
					vkCmdBindDescriptorSets(
						frameInfo.commandBuffer,
						VK_PIPELINE_BIND_POINT_GRAPHICS,
						terrainPipelineLayout,
						0,
						3,
						sets,
						0,
						nullptr
					);
					TerrainPushData push;
					push.heightMul = 1.0f;
					push.tileExtent = frameInfo.renderObjects.terrainList->getTerrain()->tileExtent;
					push.tiles = { frameInfo.renderObjects.terrainList->getTerrain()->numTilesX, frameInfo.renderObjects.terrainList->getTerrain()->numTilesY };
					push.translation = glm::vec4(0.f);
					push.layer = i;
					for (int j = 0; j < frameInfo.renderObjects.terrainList->getTerrain()->getLODCount(); j++) {
						push.LOD = j;
						vkCmdPushConstants(frameInfo.commandBuffer, terrainPipelineLayout, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(push), &push);
						frameInfo.renderObjects.terrainList->getTerrain()->render(frameInfo, j);
					}
				}
			}
			vkCmdSetDepthBias(frameInfo.commandBuffer, 0.0, 0.0, slopeBias);
			for (auto& iter : frameInfo.renderObjects.renderList->getIterator(opaqueMaterials, MeshMobilityOption_All)) {
				bool useDynamic = iter.meshMoblity == MeshMobilityOption_Dynamic;
				iter.materialClass &= ~SurfaceMaterialPipelineClassPermutationOptions_Translucent;
				iter.materialClass |= useDynamic ? SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates : 0;
				iter.materialClass |= iter.rigged ? SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms : 0;
				shadowMap->castPipelines->pipelines[iter.materialClass]->bind(frameInfo.commandBuffer);
				vkCmdBindDescriptorSets(
					frameInfo.commandBuffer,
					VK_PIPELINE_BIND_POINT_GRAPHICS,
					pipelineLayouts[iter.materialClass],
					0,
					2,
					totalSets,
					0,
					nullptr
				);
				for (auto& [uuid, actor] : iter.renderList) {
					if (!actor.mesh->castShadows) continue;
					if (!shadowMap->cameras[i].isInFrustum(actor.mesh->cullInfo.sphereBounds, actor.mesh->cullInfo.worldPosition)) { 
						continue;
					}
					Mesh3D* mesh_r = frameInfo.resourceSystem->retrieveMesh(actor.mesh->mesh);

					if (iter.rigged) {
						VkDescriptorSet descriptor = frameInfo.renderObjects.renderList->getSkeletonDescriptor(uuid, frameInfo.frameIndex);
						vkCmdBindDescriptorSets(
							frameInfo.commandBuffer,
							VK_PIPELINE_BIND_POINT_GRAPHICS,
							pipelineLayouts[iter.materialClass],
							2,
							1,
							&descriptor,
							0,
							nullptr
						);
					}
					for (int index : actor.material_indices) {
						if (!shadowMap->cameras[i].isInFrustum(actor.mesh->cullInfo.transformedMaterialAABBs[index])) {
							continue;
						}
						pushShadowMapConstants(
							frameInfo, actor, iter.materialClass,
							false, i, index, useDynamic, shadowMap->oFarZ
						);
						mesh_r->draw(frameInfo.commandBuffer, index, shadowMap->instancingCount);
					}
				}
			}
			shadowMap->renderPass->endRenderPass(frameInfo.commandBuffer);
			vkCmdSetDepthBias(frameInfo.commandBuffer, 0.0, 0.0, 0.0);
			if (shadowMap->castTranslucentShadows) {
				shadowMap->translucentCastRenderPass->beginRenderPass(frameInfo.commandBuffer, shadowMap->translucentCastFramebuffer[i], shadowMap->instancingCount);

				for (auto& iter : frameInfo.renderObjects.renderList->getIterator(translucentMaterials, MeshMobilityOption_All)) {
					bool useDynamic = iter.meshMoblity == MeshMobilityOption_Dynamic;
					iter.materialClass &= ~SurfaceMaterialPipelineClassPermutationOptions_Translucent;
					iter.materialClass |= useDynamic ? SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates : 0;
					iter.materialClass |= iter.rigged ? SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms : 0;
					shadowMap->translucentCastPipelines->pipelines[iter.materialClass]->bind(frameInfo.commandBuffer);
					vkCmdBindDescriptorSets(
						frameInfo.commandBuffer,
						VK_PIPELINE_BIND_POINT_GRAPHICS,
						pipelineLayouts[iter.materialClass],
						0,
						2,
						totalSets,
						0,
						nullptr
					);
					for (auto& [uuid, actor] : iter.renderList) {
						if (!actor.mesh->castShadows) continue;
						if (!shadowMap->cameras[i].isInFrustum(actor.mesh->cullInfo.sphereBounds, actor.mesh->cullInfo.worldPosition)) {
							continue;
						}
						Mesh3D* mesh_r = frameInfo.resourceSystem->retrieveMesh(actor.mesh->mesh);

						if (iter.rigged) {
							VkDescriptorSet descriptor = frameInfo.renderObjects.renderList->getSkeletonDescriptor(uuid, frameInfo.frameIndex);
							vkCmdBindDescriptorSets(
								frameInfo.commandBuffer,
								VK_PIPELINE_BIND_POINT_GRAPHICS,
								pipelineLayouts[iter.materialClass],
								2,
								1,
								&descriptor,
								0,
								nullptr
							);
						}
						for (int index : actor.material_indices) {
							if (!shadowMap->cameras[i].isInFrustum(actor.mesh->cullInfo.transformedMaterialAABBs[index])) {
								continue;
							}
							pushShadowMapConstants(
								frameInfo, actor, iter.materialClass,
								false, i, index, useDynamic, shadowMap->oFarZ
							);
							mesh_r->draw(frameInfo.commandBuffer, index, shadowMap->instancingCount);
						}
					}
				}
				shadowMap->translucentCastRenderPass->endRenderPass(frameInfo.commandBuffer);
				shadowMap->translucentTintRenderPass->beginRenderPass(frameInfo.commandBuffer, shadowMap->translucentTintFramebuffer[i], shadowMap->instancingCount);
				for (auto& iter : frameInfo.renderObjects.renderList->getIterator(translucentMaterials, MeshMobilityOption_All)) {
					bool useDynamic = iter.meshMoblity == MeshMobilityOption_Dynamic;
					iter.materialClass &= ~SurfaceMaterialPipelineClassPermutationOptions_Translucent;
					iter.materialClass |= useDynamic ? SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates : 0;
					iter.materialClass |= iter.rigged ? SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms : 0;
					shadowMap->translucentPipelines->pipelines[iter.materialClass]->bind(frameInfo.commandBuffer);
					vkCmdBindDescriptorSets(
						frameInfo.commandBuffer,
						VK_PIPELINE_BIND_POINT_GRAPHICS,
						pipelineLayouts[iter.materialClass],
						0,
						2,
						totalSets,
						0,
						nullptr
					);
					for (auto& [uuid, actor] : iter.renderList) {
						if (!actor.mesh->castShadows) continue;
						if (!shadowMap->cameras[i].isInFrustum(actor.mesh->cullInfo.sphereBounds, actor.mesh->cullInfo.worldPosition)) {
							continue;
						}
						Mesh3D* mesh_r = frameInfo.resourceSystem->retrieveMesh(actor.mesh->mesh);
						if (iter.rigged) {
							VkDescriptorSet descriptor = frameInfo.renderObjects.renderList->getSkeletonDescriptor(uuid, frameInfo.frameIndex);
							vkCmdBindDescriptorSets(
								frameInfo.commandBuffer,
								VK_PIPELINE_BIND_POINT_GRAPHICS,
								pipelineLayouts[iter.materialClass],
								2,
								1,
								&descriptor,
								0,
								nullptr
							);
						}
						for (int index : actor.material_indices) {
							if (!shadowMap->cameras[i].isInFrustum(actor.mesh->cullInfo.transformedMaterialAABBs[index])) {
								continue;
							}
							pushShadowMapConstants(
								frameInfo, actor, iter.materialClass,
								false, i, index, useDynamic, shadowMap->oFarZ
							);
							mesh_r->draw(frameInfo.commandBuffer, index, shadowMap->instancingCount);
						}
					}
				}
				shadowMap->translucentCastRenderPass->endRenderPass(frameInfo.commandBuffer);
			}
			shadowMap->process(frameInfo, i);
		}

		if (shadowMap->reflectiveShadowMap) {
			shadowMap->reflectiveShadowMap->downSampleMaps(frameInfo.commandBuffer, engineDevice);
		}
		shadowMap->gpuDrawTime->end(frameInfo.commandBuffer);
		shadowMap->cpuDrawTime->end();
	}

	void ShadowMappingSystem::renderShadowOmni(FrameInfo& frameInfo, ShadowMap* shadowMap, float slopeBias) {
		shadowMap->gpuDrawTime->record(frameInfo.commandBuffer);
		shadowMap->cpuDrawTime->record();
		VkDescriptorSet totalSets[2]{
			resourceSystem->getDescriptor()->getDescriptor(),
			shadowMap->shadowMapBufferInfoDescriptorSet[frameInfo.frameIndex]
		};
		vkCmdSetDepthBias(frameInfo.commandBuffer, 0.0, 0.0, slopeBias);
		for (int i = 0; i < shadowMap->numPasses; i++) {
			MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::GENERIC, std::string("SHADOW_MAP::conv=" + std::string(magic_enum::enum_name(shadowMap->convention)) + ",type=" + std::string(dynamic_cast<ShadowPCFMap*>(shadowMap) ? "PCF" : "VSM") + ",pass=" + std::to_string(i)).c_str(), frameInfo.commandBuffer);
			shadowMap->renderPass->beginRenderPass(frameInfo.commandBuffer, shadowMap->framebuffer[i], shadowMap->instancingCount);
			for (auto& iter : frameInfo.renderObjects.renderList->getIterator(opaqueMaterials, MeshMobilityOption_All)) {
				shadowMap->castPipelines->pipelines[iter.materialClass]->bind(frameInfo.commandBuffer);
				vkCmdBindDescriptorSets(
					frameInfo.commandBuffer,
					VK_PIPELINE_BIND_POINT_GRAPHICS,
					omniPipelineLayouts[iter.materialClass],
					0,
					2,
					totalSets,
					0,
					nullptr
				);
				bool useSkeletonRigs = iter.materialClass & SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms;
				bool useDynamic = iter.materialClass & SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates;
				for (auto& [uuid, actor] : iter.renderList) {
					if (!actor.mesh->castShadows) continue;
					if (!shadowMap->cameras[i].isInFrustum(actor.mesh->cullInfo.sphereBounds, actor.mesh->cullInfo.worldPosition)) {
						continue;
					}
					if (useSkeletonRigs) {
						VkDescriptorSet descriptor = frameInfo.renderObjects.renderList->getSkeletonDescriptor(uuid, frameInfo.frameIndex);
						vkCmdBindDescriptorSets(
							frameInfo.commandBuffer,
							VK_PIPELINE_BIND_POINT_GRAPHICS,
							pipelineLayouts[iter.materialClass],
							2,
							1,
							&descriptor,
							0,
							nullptr
						);
					}
					Mesh3D* mesh_r = frameInfo.resourceSystem->retrieveMesh(actor.mesh->mesh);
					for (int index : actor.material_indices) {
						//if (!shadowMap->cameras[i].isInFrustum(actor.mesh->transformedMaterialAABBs[index])) {
						//	continue;
						//}
						pushShadowMapConstants(
							frameInfo, actor, iter.materialClass,
							true, i, index, useDynamic, shadowMap->oFarZ
						);
						mesh_r->draw(frameInfo.commandBuffer, index, shadowMap->instancingCount);
					}
				}
			}
			shadowMap->renderPass->endRenderPass(frameInfo.commandBuffer);
		}
		vkCmdSetDepthBias(frameInfo.commandBuffer, 0.0, 0.0, 0.0);
		shadowMap->gpuDrawTime->end(frameInfo.commandBuffer);
		shadowMap->cpuDrawTime->end();
	}

	void ShadowMappingSystem::renderDirectional(FrameInfo& frameInfo) {
		glm::vec4 lpos = frameInfo.level->getPossessedCameraActor().getTransform().transformMatrix[3];
		auto viewCam = frameInfo.level->registry.view<Components::DirectionalLightComponent, RENDER_ITER>();
		for (auto obj : viewCam) {
			ECS::Actor actor = { obj, frameInfo.level.get() };
			auto& l_transform = actor.getTransform();
			auto& l_component = actor.getComponent<Components::DirectionalLightComponent>();
			bool actorShadowMapPresent = shadowMaps.find(actor) != shadowMaps.end();
			bool recreateShadowMap = false;
			if (l_component.castShadows) {
				if (actorShadowMapPresent) {
					ShadowMap* shadowMap = shadowMaps[actor];
					if (l_component.castTranslucentShadows != shadowMap->castTranslucentShadows || l_component.shadowMapConvention != (int)shadowMap->convention || (l_component.indirectLighting && rsmEnabled) != (bool)shadowMap->reflectiveShadowMap) {
						recreateShadowMap = true;
					} else {
						if (shadowMap->framebuffer[0]->getDimensions().x != getPow2Size(l_component.shadowMapResolution * ProjectSystem::getGraphicsSettings().shadows.directionalResolutionMultiplier)) recreateShadowMap = true;
						else {
							float clipBounds = l_component.shadowMapClippingDistance;
							glm::mat4 realTransform = l_transform.transformMatrix;
							realTransform = glm::rotate(realTransform, glm::half_pi<float>(), glm::vec3(1.f, 0.f, 0.f));
							realTransform[3] = lpos;

							shadowMap->cameras[0].setViewYXZ(realTransform);
							shadowMap->cameras[0].setOrthographicProjection(
								-clipBounds,
								clipBounds,
								-clipBounds,
								clipBounds,
								-800.000f, 800.000f
							);

							l_component.lightSpaceMatrix = shadowMap->cameras[0].getProjection() * shadowMap->cameras[0].getView();
							
							// avoid jaggedness on static objects when moving the camera by snapping the position to the nearest original pixel
							float halfRes = l_component.shadowMapResolution / 2.0f;

							// pick random point, doesn't matter
							glm::vec4 originPoint = glm::vec4(0.0, 0.0, 0.0, 1.0);
							glm::vec4 clip = l_component.lightSpaceMatrix * originPoint;

							clip /= clip.w;
							glm::vec2 full = glm::floor(clip * halfRes);
							glm::vec4 newClip = shadowMap->cameras[0].getInverseProjection() * glm::vec4(full / halfRes, clip.z, 1.0);
							newClip /= newClip.w;
							glm::vec4 newWorld = shadowMap->cameras[0].getInverseView() * newClip;
							glm::vec4 difference = originPoint - newWorld;
							realTransform[3] += difference;
							realTransform[3].w = 1.0f;
							l_component.lightSpaceMatrix = shadowMap->cameras[0].getProjection() * shadowMap->cameras[0].getView();
							for (int i = 0; i < shadowMap->numPasses; i++) {
								float cascadeMul = float(1 << (2 * i));
								shadowMap->cameras[i].setViewYXZ(realTransform);
								shadowMap->cameras[i].setOrthographicProjection(
								-clipBounds * cascadeMul,
								clipBounds * cascadeMul,
								-clipBounds * cascadeMul,
								clipBounds * cascadeMul,
								-800.000f, 800.000f
								);
							}
							ShadowMatrixData shadowMapBufferData{};

							for (int i = 0; i < shadowMap->numPasses; i++) {
								shadowMapBufferData.viewProjection[i] = shadowMap->cameras[i].getProjection() * shadowMap->cameras[i].getView();
								shadowMapBufferData.invView[i] = shadowMap->cameras[i].getInverseView();
								shadowMapBufferData.invProjection[i] = shadowMap->cameras[i].getInverseProjection();
							}
							shadowMapBufferData.RSMLIGHT_direction = glm::normalize(glm::vec3(realTransform[2]));
							shadowMapBufferData.RSMLIGHT_intensity = l_component.color * l_component.lightIntensity;
							shadowMap->shadowMapBufferInfo[frameInfo.frameIndex]->writeToBuffer(&shadowMapBufferData);
							shadowMap->shadowMapBufferInfo[frameInfo.frameIndex]->flush();
							renderShadowMap(frameInfo, shadowMap, l_component.shadowMapSlopeBiasFactor);
						}
					}
				}
				else recreateShadowMap = true;
			} else {
				actorShadowMapPresent = false;
			}
			if (recreateShadowMap) {
				SHARD3D_LOG("Shadow map rebuild flag for light actor {0}", actor.getTag());
				ShadowMapIDPtrs idptrs{};
				idptrs.shadowMap_id_PTR = &l_component.shadowMap_id;
				idptrs.shadowMapTransCast_id_PTR = &l_component.shadowMapTranslucent_id;
				idptrs.shadowMapTransCol_id_PTR = &l_component.shadowMapTranslucentColor_id;
				idptrs.shadowMapTransRev_id_PTR = &l_component.shadowMapTranslucentReveal_id;
				actorShadowMapPresent = false;
				rebuildShadows.emplace_back(_LightComponentInfo{ actor, ShadowMapLight::Directional,(ShadowMapConvention)l_component.shadowMapConvention, l_component.shadowMapResolution, l_component.castTranslucentShadows, idptrs , l_component.indirectLighting && rsmEnabled, false }, l_component.castShadows);
			}
			if (actorShadowMapPresent) removedShadowMaps.erase(actor);
		}
	}

	void ShadowMappingSystem::renderSpot(FrameInfo& frameInfo) {
		auto viewCam = frameInfo.level->registry.view<Components::SpotLightComponent, RENDER_ITER>();
		for (auto obj : viewCam) {
			ECS::Actor actor = { obj, frameInfo.level.get() };
			auto& l_transform = actor.getTransform();
			auto& l_component = actor.getComponent<Components::SpotLightComponent>();
			bool actorShadowMapPresent = shadowMaps.find(actor) != shadowMaps.end();
			bool recreateShadowMap = false;
			if (l_component.castShadows) {
				if (actorShadowMapPresent) {
					ShadowMap* shadowMap = shadowMaps[actor];
					if (l_component.castTranslucentShadows != shadowMap->castTranslucentShadows || l_component.shadowMapConvention != (int)shadowMap->convention || (l_component.indirectLighting && rsmEnabled) != (bool)shadowMap->reflectiveShadowMap) {
						recreateShadowMap = true;
					} else {
						if (shadowMap->framebuffer[0]->getDimensions().x != getPow2Size(l_component.shadowMapResolution * ProjectSystem::getGraphicsSettings().shadows.spotResolutionMultiplier)) {
							recreateShadowMap = true;
						} else {
							glm::mat4 realTransform = l_transform.transformMatrix;
							realTransform = glm::rotate(realTransform, glm::half_pi<float>(), glm::vec3(1.f, 0.f, 0.f));
							shadowMap->cameras[0].setViewYXZ(realTransform);
							if (shadowMap->convention == ShadowMapConvention::Conventional) {
								float FOV = acos(l_component.outerAngle) * 2.f;
								float farClip = l_component.lightRadius;
								float nearClip = l_component.lightRadius < 1.f ? l_component.lightRadius * 0.05f : 0.1f;
								shadowMap->oFarZ = farClip;
								shadowMap->cameras[0].setPerspectiveProjection(FOV, 1.f, nearClip, farClip);
								
								l_component.lightSpaceMatrix = shadowMap->cameras[0].getProjection() * shadowMap->cameras[0].getView();

								ShadowMatrixData shadowMapBufferData{};
								shadowMapBufferData.viewProjection[0] = l_component.lightSpaceMatrix;
								shadowMapBufferData.invView[0] = shadowMap->cameras[0].getInverseView();
								shadowMapBufferData.invProjection[0] = shadowMap->cameras[0].getInverseProjection();
								shadowMapBufferData.RSMLIGHT_direction = glm::normalize(glm::vec3(realTransform[2]));
								shadowMapBufferData.RSMLIGHT_intensity = l_component.color * l_component.lightIntensity;
								shadowMapBufferData.RSMLIGHT_position = realTransform[3];
								shadowMapBufferData.RSMLIGHT_cosAngles = { l_component.outerAngle, l_component.innerAngle };
								shadowMap->shadowMapBufferInfo[frameInfo.frameIndex]->writeToBuffer(&shadowMapBufferData);
								shadowMap->shadowMapBufferInfo[frameInfo.frameIndex]->flush();

								renderShadowMap(frameInfo, shadowMap, l_component.shadowMapSlopeBiasFactor);
							} else if (shadowMap->convention == ShadowMapConvention::Paraboloid) {
								glm::mat4 matrix = shadowMap->cameras[0].getView();
								l_component.lightSpaceMatrix = l_transform.getInverseTransform();
								matrix[3] = l_transform.transformMatrix[3];
								// TODO rsm for paraboloid shadows?
								shadowMap->shadowMapBufferInfo[frameInfo.frameIndex]->writeToBuffer(&matrix);
								shadowMap->shadowMapBufferInfo[frameInfo.frameIndex]->flush();
								renderShadowOmni(frameInfo, shadowMap, l_component.shadowMapSlopeBiasFactor);
							}
						}
					}
				}
				else recreateShadowMap = true;
			} else {
				actorShadowMapPresent = false;
			}
			if (recreateShadowMap) {
				SHARD3D_LOG("Shadow map rebuild flag for light actor {0}", actor.getTag());
				ShadowMapIDPtrs idptrs{};
				idptrs.shadowMap_id_PTR = &l_component.shadowMap_id;
				idptrs.shadowMapTransCast_id_PTR = &l_component.shadowMapTranslucent_id;
				idptrs.shadowMapTransCol_id_PTR = &l_component.shadowMapTranslucentColor_id;
				idptrs.shadowMapTransRev_id_PTR = &l_component.shadowMapTranslucentReveal_id;
				actorShadowMapPresent = false;
				rebuildShadows.emplace_back(_LightComponentInfo{ actor, ShadowMapLight::Spot, (ShadowMapConvention)l_component.shadowMapConvention, l_component.shadowMapResolution, l_component.castTranslucentShadows, idptrs, l_component.indirectLighting && rsmEnabled, false }, l_component.castShadows);
			}
			if (actorShadowMapPresent) removedShadowMaps.erase(actor);
		}
	}

	void ShadowMappingSystem::renderPoint(FrameInfo& frameInfo) {
		auto viewCam = frameInfo.level->registry.view<Components::PointLightComponent, RENDER_ITER>();
		for (auto obj : viewCam) {
			ECS::Actor actor = { obj, frameInfo.level.get() };
			auto& l_transform = actor.getTransform();
			auto& l_component = actor.getComponent<Components::PointLightComponent>();
			bool actorShadowMapPresent = shadowMaps.find(actor) != shadowMaps.end();
			bool recreateShadowMap = false;
			if (l_component.castShadows) {
				if (actorShadowMapPresent) {
					ShadowMap* shadowMap = shadowMaps[actor];
					if (l_component.castTranslucentShadows != shadowMap->castTranslucentShadows || (l_component.shadowMapInstancedDraw && engineDevice.supportedExtensions.ext_shader_viewport_index_layer) != (shadowMap->instancingCount != 1) || l_component.shadowMapConvention != (int)shadowMap->convention) {
						recreateShadowMap = true;
					} else {
						if (shadowMap->framebuffer[0]->getDimensions().x != getPow2Size(l_component.shadowMapResolution * ProjectSystem::getGraphicsSettings().shadows.pointResolutionMultiplier)) {
							recreateShadowMap = true;
						} else {
							const float zFar = l_component.lightRadius;
							const float zNear = l_component.lightRadius < 1.0f ? l_component.lightRadius * 0.05f : 0.1f;
							shadowMap->oFarZ = zFar;
							if (shadowMap->convention == ShadowMapConvention::Cube) {
								static glm::mat3 viewMatrices[6] =
								{
									{	// X+
										{ 0.0, 0.0, -1.0 },
										{ 0.0, 1.0, 0.0 },
										{ 1.0, 0.0, 0.0 }
									},
									{	// X-
										{ 0.0, 0.0, 1.0 },
										{ 0.0, 1.0, 0.0 },
										{ -1.0, 0.0, 0.0 }
									},
									{	// Y-
										{ -1.0, 0.0, 0.0 },
										{ 0.0, 0.0, -1.0 },
										{ 0.0, -1.0, 0.0 }
									},
									{	// Y+
										{ -1.0, 0.0, 0.0 },
										{ 0.0, 0.0, 1.0 },
										{ 0.0, 1.0, 0.0 }
									},
									{	// Z+
										{ -1.0, 0.0, 0.0 },
										{ 0.0, 1.0, 0.0 },
										{ 0.0, 0.0, -1.0 }
									},
									{	// Z-
										{ 1.0, 0.0, 0.0 },
										{ 0.0, 1.0, 0.0 },
										{ 0.0, 0.0, 1.0 }
									}
								};
								glm::mat4 projection = { // pi/2 radians 1:1 AR
									{ 1.0, 0.0, 0.0, 0.0 },
									{ 0.0, 1.0, 0.0, 0.0 },
									{ 0.0, 0.0, zFar / (zFar - zNear), 1.0 },
									{ 0.0, 0.0, -(zFar * zNear) / (zFar - zNear), 0.0 }
								};
								OmniShadowMatrixData matrixData{};

								for (int i = 0; i < 6; i++) {
									glm::mat4 view = glm::mat4(viewMatrices[i]);
									const glm::vec3 u = glm::vec3(viewMatrices[i][0][0], viewMatrices[i][1][0], viewMatrices[i][2][0]);
									const glm::vec3 v = glm::vec3(viewMatrices[i][0][1], viewMatrices[i][1][1], viewMatrices[i][2][1]);
									const glm::vec3 w = glm::vec3(viewMatrices[i][0][2], viewMatrices[i][1][2], viewMatrices[i][2][2]);
									view[3][0] = -glm::dot(u, glm::vec3(l_transform.transformMatrix[3]));
									view[3][1] = -glm::dot(v, glm::vec3(l_transform.transformMatrix[3]));
									view[3][2] = -glm::dot(w, glm::vec3(l_transform.transformMatrix[3]));
									matrixData.invView[i] = glm::inverse(view);
									matrixData.viewProjection[i] = projection * view;
									shadowMap->cameras[i].setViewMatrix(view);
									shadowMap->cameras[i].setPerspectiveProjection(glm::half_pi<float>(), 1.0f, zNear, zFar);
								} 

								shadowMap->shadowMapBufferInfo[frameInfo.frameIndex]->writeToBuffer(& matrixData);
								shadowMap->shadowMapBufferInfo[frameInfo.frameIndex]->flush();

								renderShadowOmni(frameInfo, shadowMap, l_component.shadowMapSlopeBiasFactor);
							} else if (shadowMap->convention == ShadowMapConvention::DualParaboloid) {
								static glm::mat3 viewMatrices[2] =
								{
									{	// Y-
										{ -1.0, 0.0, 0.0 },
										{ 0.0, 0.0, -1.0 },
										{ 0.0, -1.0, 0.0 }
									},
									{	// Y+
										{ -1.0, 0.0, 0.0 },
										{ 0.0, 0.0, 1.0 },
										{ 0.0, 1.0, 0.0 }
									},
								};

								OmniShadowMatrixData matrixData{};

								for (int i = 0; i < 2; i++) {
									glm::mat4 view = glm::mat4(viewMatrices[i]);
									const glm::vec3 u = glm::vec3(viewMatrices[i][0][0], viewMatrices[i][1][0], viewMatrices[i][2][0]);
									const glm::vec3 v = glm::vec3(viewMatrices[i][0][1], viewMatrices[i][1][1], viewMatrices[i][2][1]);
									const glm::vec3 w = glm::vec3(viewMatrices[i][0][2], viewMatrices[i][1][2], viewMatrices[i][2][2]);
									view[3][0] = -glm::dot(u, glm::vec3(l_transform.transformMatrix[3]));
									view[3][1] = -glm::dot(v, glm::vec3(l_transform.transformMatrix[3]));
									view[3][2] = -glm::dot(w, glm::vec3(l_transform.transformMatrix[3]));
									matrixData.invView[i] = glm::inverse(view);
									matrixData.viewProjection[i] = view;
									shadowMap->cameras[i].setViewMatrix(view);
									shadowMap->cameras[i].setPerspectiveProjection(glm::pi<float>() * 0.999f, 1.0f, zNear, zFar);
								}

								shadowMap->shadowMapBufferInfo[frameInfo.frameIndex]->writeToBuffer(&matrixData);
								shadowMap->shadowMapBufferInfo[frameInfo.frameIndex]->flush();

								renderShadowOmni(frameInfo, shadowMap, l_component.shadowMapSlopeBiasFactor);
							}
						}
					}
				} else recreateShadowMap = true;
			} else {
				actorShadowMapPresent = false;
			}
			if (recreateShadowMap) {
				SHARD3D_LOG("Shadow map rebuild flag for light actor {0}", actor.getTag());
				ShadowMapIDPtrs idptrs{};
				idptrs.shadowMap_id_PTR = &l_component.shadowMap_id;
				idptrs.shadowMapTransCast_id_PTR = &l_component.shadowMapTranslucent_id;
				idptrs.shadowMapTransCol_id_PTR = &l_component.shadowMapTranslucentColor_id;
				idptrs.shadowMapTransRev_id_PTR = &l_component.shadowMapTranslucentReveal_id;
				actorShadowMapPresent = false;
				rebuildShadows.emplace_back(_LightComponentInfo{ actor, ShadowMapLight::Point, (ShadowMapConvention)l_component.shadowMapConvention, l_component.shadowMapResolution, l_component.castTranslucentShadows, idptrs,false, l_component.shadowMapInstancedDraw && engineDevice.supportedExtensions.ext_shader_viewport_index_layer }, l_component.castShadows);
			}
			if (actorShadowMapPresent) removedShadowMaps.erase(actor);
		}
	}

	void ShadowMappingSystem::runGarbageCollector(uint32_t frameIndex, VkFence fence) {
		currentFrameIndex = frameIndex;
		uint32_t lastOccupiedFrameIndex = (currentFrameIndex + 1) % maxFramesInFlight;
		for (auto& [info, recreate] : rebuildShadows) {
			destroyShadowQueue.push_back({ shadowMaps[info.actor], {engineDevice, fence} });
			shadowMaps.erase(info.actor);

			if (!recreate) continue;
			int res = info.resolution;
			switch (info.lightType) {
			case(ShadowMapLight::Directional):
				res *= ProjectSystem::getGraphicsSettings().shadows.directionalResolutionMultiplier;
				res = getPow2Size(std::clamp(res, 256, 8192));
				if (ProjectSystem::getEngineSettings().shading.directionalShadowMapType == ESET::ShadowMap::PCF)
					shadowMaps[info.actor] = std::move(createPCFShadowMap(res, info.idPtrs, info.lightType, info.lightConvention, info.castTranslucent, info.rsm, false));
				else if (ProjectSystem::getEngineSettings().shading.directionalShadowMapType == ESET::ShadowMap::Variance)
					shadowMaps[info.actor] = std::move(createVarianceShadowMap(res, info.idPtrs, info.lightType, info.lightConvention, info.castTranslucent, false));
				break;
			case(ShadowMapLight::Spot):
				res *= ProjectSystem::getGraphicsSettings().shadows.spotResolutionMultiplier;
				res = getPow2Size(std::clamp(res, 64, 2048));
				if (ProjectSystem::getEngineSettings().shading.spotShadowMapType == ESET::ShadowMap::PCF)
					shadowMaps[info.actor] = std::move(createPCFShadowMap(res, info.idPtrs, info.lightType, info.lightConvention, info.castTranslucent, info.rsm, false));
				else if (ProjectSystem::getEngineSettings().shading.spotShadowMapType == ESET::ShadowMap::Variance)
					shadowMaps[info.actor] = std::move(createVarianceShadowMap(res, info.idPtrs, info.lightType, info.lightConvention, info.castTranslucent, false));
				break;
			case(ShadowMapLight::Point):
				res *= ProjectSystem::getGraphicsSettings().shadows.pointResolutionMultiplier;
				res = getPow2Size(std::clamp(res, 32, 1024));
				shadowMaps[info.actor] = std::move(createPCFShadowMap(res, info.idPtrs, info.lightType, info.lightConvention, false, false, info.instancedDraw));
				break;
			}
		}
		rebuildShadows.clear();
		for (int i = 0; i < destroyShadowQueue.size(); i++) {
			auto& pair = destroyShadowQueue[i];
			if (pair.second.getFence() == fence) continue; // from same frame
			if (pair.second.isFree()) {
				delete pair.first;
				destroyShadowQueue.erase(destroyShadowQueue.begin() + i);
				i--;
			}
		}
	}
	void ShadowMappingSystem::render(FrameInfo& frameInfo) {
		for (auto& [actor, shadow] : shadowMaps) {
			removedShadowMaps.insert(actor);
		}
		renderDirectional(frameInfo);
		renderSpot(frameInfo);
		renderPoint(frameInfo);
		for (auto& map : removedShadowMaps) {
			Actor actor = {map, frameInfo.level.get()};
			if (!actor.isInvalid()) {
				if (actor.hasComponent<Components::DirectionalLightComponent>()) {
					actor.getComponent<Components::DirectionalLightComponent>().shadowMap_id = 0;
					actor.getComponent<Components::DirectionalLightComponent>().shadowMapTranslucent_id = 0;
					actor.getComponent<Components::DirectionalLightComponent>().shadowMapTranslucentColor_id = 0;
					actor.getComponent<Components::DirectionalLightComponent>().shadowMapTranslucentReveal_id = 0;
				}
				if (actor.hasComponent<Components::SpotLightComponent>()) {
					actor.getComponent<Components::SpotLightComponent>().shadowMap_id = 0;
					actor.getComponent<Components::SpotLightComponent>().shadowMapTranslucent_id = 0;
					actor.getComponent<Components::SpotLightComponent>().shadowMapTranslucentColor_id = 0;
					actor.getComponent<Components::SpotLightComponent>().shadowMapTranslucentReveal_id = 0;
				}
				if (actor.hasComponent<Components::PointLightComponent>()) {
					actor.getComponent<Components::PointLightComponent>().shadowMap_id = 0;
					actor.getComponent<Components::PointLightComponent>().shadowMapTranslucent_id = 0;
					actor.getComponent<Components::PointLightComponent>().shadowMapTranslucentColor_id = 0;
					actor.getComponent<Components::PointLightComponent>().shadowMapTranslucentReveal_id = 0;
				}
			}
			rebuildShadows.emplace_back(_LightComponentInfo{ actor, ShadowMapLight::None, ShadowMapConvention::None, 0, false, nullptr, false, false }, false);
		}
		removedShadowMaps.clear();
	}
}