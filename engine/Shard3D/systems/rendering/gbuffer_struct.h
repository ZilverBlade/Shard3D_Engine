#pragma once

namespace Shard3D {
	class S3DRenderTarget;
	inline namespace Systems {
		struct SceneBufferInputData {
			S3DRenderTarget* colorAttachment;
			S3DRenderTarget* depthSceneInfo;
			S3DRenderTarget* gbuffer0; // diffuse.rgb + reflectivity (RGBA8)
			S3DRenderTarget* gbuffer1; //	specular + glossiness (RG8)
			S3DRenderTarget* gbuffer2; // emission.rgb (R11G11B10)
			S3DRenderTarget* gbuffer3; // normal packed (R32)
			S3DRenderTarget* gbuffer4; // shading model (R8)
		};
		struct LightingBufferInputData {
			S3DRenderTarget* lbuffer0; // diffuse lighting (RGB_UFLOAT_PACK32)
			S3DRenderTarget* lbuffer1; // specular lighting (RGB_UFLOAT_PACK32)
		};
	}
}