#include "sky_atmosphere_system.h"
namespace Shard3D::Systems {
	struct AtmosphericScatteringPush {
		glm::mat4 projectionViewNoTranslate;
		glm::vec4 direction;
		float rayleighTerm;
		float mieTerm;
		float height;
		float sunBoost;
	};

	SkyAtmosphereSystem::SkyAtmosphereSystem(S3DDevice& device, ResourceSystem* resourceSystem, VkDescriptorSetLayout globalSetLayout,
		VkRenderPass renderPass, bool renderSun) : engineDevice(device), resourceSystem(resourceSystem){
		sphereDome = resourceSystem->retrieveMesh(ResourceSystem::coreAssets.m_sphere);
		createPipeline(globalSetLayout, renderPass, renderSun);
	}
	SkyAtmosphereSystem::~SkyAtmosphereSystem() {
		for (auto [actor, refl] : skyAtmosphereReflections) {
			for (int i = 0; i < 6; i++) {
				delete refl->atmosphericScatteringCubeFramebuffers[i];
			}
			delete refl->atmosphericScatteringCubeRenderPass;
			delete refl->envIrradianceGen;
			delete refl->atmosphericScatteringCubeRenderTarget;
			delete refl;
		}
	}

	bool SkyAtmosphereSystem::skyAtmosphereDrawCall(FrameInfo& frameInfo, Components::SkyAtmosphereComponent& atc, glm::mat4 projectionViewNoTranslate) {
		Actor dirLight = { atc.directionalLightActor, frameInfo.level.get() };
		if (dirLight.isInvalid()) return false;
		
		glm::mat4 realTransform = dirLight.getTransform().transformMatrix;
		realTransform = glm::rotate(realTransform, glm::half_pi<float>(), glm::vec3(1.f, 0.f, 0.f));
		glm::vec3 direction = glm::normalize(realTransform[2]); // convert rotation to direction

		AtmosphericScatteringPush push;
		push.projectionViewNoTranslate = projectionViewNoTranslate;
		push.direction = glm::vec4(direction, 0.0);
		push.rayleighTerm = atc.rayleighScattering;
		push.mieTerm = atc.mieScattering;
		push.sunBoost = atc.sunIntensity;
		push.height = atc.height;

		atmosphericScatteringPush.push(frameInfo.commandBuffer, atmosphericScatteringPipelineLayout->getPipelineLayout(), push);

		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			atmosphericScatteringPipelineLayout->getPipelineLayout(),
			0,
			1,
			&frameInfo.globalDescriptorSet,
			0,
			nullptr
		);
		sphereDome->drawAll(frameInfo.commandBuffer);

		return true;
	}

	void SkyAtmosphereSystem::render(FrameInfo& frameInfo) {
		auto& view = frameInfo.level->registry.view<Components::SkyAtmosphereComponent>();
		for (auto ent : view) {
			Actor actor = { ent, frameInfo.level.get() };
			auto& atc = actor.getComponent<Components::SkyAtmosphereComponent>();
			atmosphericScatteringPipeline->bind(frameInfo.commandBuffer);
			MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::DRAW, "SKY_ATMOSPHERE_RENDER_SKY", frameInfo.commandBuffer);
			skyAtmosphereDrawCall(frameInfo, atc, frameInfo.camera.getProjection() * glm::mat4(glm::mat3(frameInfo.camera.getView())));
		}
		
	}
	void SkyAtmosphereSystem::renderSkyLightCubemap(FrameInfo& frameInfo) {
		for (auto& [actor, shadow] : skyAtmosphereReflections) {
			removedReflections.insert(actor);
		}
		auto viewCam = frameInfo.level->registry.view<Components::SkyLightComponent, RENDER_ITER>();
		for (auto obj : viewCam) {
			ECS::Actor actor = { obj, frameInfo.level.get() };
			auto& s_transform = actor.getTransform();
			auto& s_component = actor.getComponent<Components::SkyLightComponent>();
			if (s_component.reflectionType != 1) continue;
			Actor atmosphere = { s_component.skyAtmosphereActor, frameInfo.level.get() };
			if (atmosphere.isInvalid()) continue;
			if (!atmosphere.hasComponent < Components::SkyAtmosphereComponent>())continue;
			auto& atc = atmosphere.getComponent < Components::SkyAtmosphereComponent>();
			Actor dirLight = { atc.directionalLightActor, frameInfo.level.get() };
			if (dirLight.isInvalid()) continue;

			bool actorReflectionMapPresent = skyAtmosphereReflections.find(actor) != skyAtmosphereReflections.end();
			bool recreateReflectionMap = false;
			if (actorReflectionMapPresent) {
				SkyAtmosphereCubemap* reflectionMap = skyAtmosphereReflections[actor];
				if (s_component.skyAtmosphereReflectionResolution != reflectionMap->atmosphericScatteringCubeRenderTarget->getAttachmentDescription().dimensions.x) {
					recreateReflectionMap = true;
				} else {
					static glm::mat3 viewMatrices[6] =
					{
						{	// X+
							{ 0.0, 0.0, -1.0 },
							{ 0.0, 1.0, 0.0 },
							{ 1.0, 0.0, 0.0 }
						},
						{	// X-
							{ 0.0, 0.0, 1.0 },
							{ 0.0, 1.0, 0.0 },
							{ -1.0, 0.0, 0.0 }
						},
						{	// Y-
							{ -1.0, 0.0, 0.0 },
							{ 0.0, 0.0, -1.0 },
							{ 0.0, -1.0, 0.0 }
						},
						{	// Y+
							{ -1.0, 0.0, 0.0 },
							{ 0.0, 0.0, 1.0 },
							{ 0.0, 1.0, 0.0 }
						},
						{	// Z+
							{ -1.0, 0.0, 0.0 },
							{ 0.0, 1.0, 0.0 },
							{ 0.0, 0.0, -1.0 }
						},
						{	// Z-
							{ 1.0, 0.0, 0.0 },
							{ 0.0, 1.0, 0.0 },
							{ 0.0, 0.0, 1.0 }
						}
					};

					S3DCamera projection;
					projection.setPerspectiveProjection(glm::half_pi<float>(), 1.0f, 0.1, 1.0);// we only care about the FOV and aspect, the rest doesnt matter 

					MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::DRAW, "SKY_ATMOSPHERE_CUBEMAP_REFL_RENDER", frameInfo.commandBuffer);
					for (int i = 0; i < 6; i++) {
						reflectionMap->atmosphericScatteringCubeRenderPass->beginRenderPass(frameInfo.commandBuffer, reflectionMap->atmosphericScatteringCubeFramebuffers[i]);
						reflectionMap->atmosphericScatteringPipelineCubemap->bind(frameInfo.commandBuffer);

						glm::mat4 noTranslateProjView = projection.getProjection() * glm::mat4(viewMatrices[i]);

						skyAtmosphereDrawCall(frameInfo, atc, noTranslateProjView);

						reflectionMap->atmosphericScatteringCubeRenderPass->endRenderPass(frameInfo.commandBuffer);
					}
					MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::DRAW, "SKY_ATMOSPHERE_CUBEMAP_IRR_RENDER", frameInfo.commandBuffer);
					for (int i = 0; i < 6; i++) {
						glm::mat4 noTranslateProjView = projection.getProjection() * glm::mat4(viewMatrices[i]);
						reflectionMap->envIrradianceGen->render(frameInfo.commandBuffer, noTranslateProjView, i);
					}
				}
			} else {
				recreateReflectionMap = true;
			}

			if (recreateReflectionMap) {
				SHARD3D_LOG("recreateReflectionMap rebuild flag for planar reflection actor {0}", actor.getTag());
				actorReflectionMapPresent = false;
				rebuildReflections.emplace_back(SkyAtmosphereComponentInfo_{ actor, s_component.skyAtmosphereReflectionResolution, 6, &s_component.reflectionCubemap_id, &s_component.irradianceCubemap_id }, true);
			}
			if (actorReflectionMapPresent) removedReflections.erase(actor);
		}
	}
	void SkyAtmosphereSystem::runGarbageCollector(uint32_t frameIndex, VkFence fence) {
		//currentFrameIndex = frameIndex;
		//uint32_t lastOccupiedFrameIndex = (currentFrameIndex + 1) % maxFramesInFlight;
		for (auto& [info, recreate] : rebuildReflections) {
			destroyReflectionQueue.push_back({ skyAtmosphereReflections[info.actor], {engineDevice, fence} });
			skyAtmosphereReflections.erase(info.actor);
			if (recreate) {
				vkDeviceWaitIdle(engineDevice.device());
				SkyAtmosphereCubemap* cubemap = new SkyAtmosphereCubemap();
				cubemap->reflection_id = resourceSystem->getDescriptor()->allocateIndex(BINDLESS_SAMPLER_BINDING);
				cubemap->irradiance_id = resourceSystem->getDescriptor()->allocateIndex(BINDLESS_SAMPLER_BINDING);
				{
					cubemap->atmosphericScatteringCubeRenderTarget = new S3DRenderTarget(engineDevice, {
						VK_FORMAT_B10G11R11_UFLOAT_PACK32,
						VK_IMAGE_ASPECT_COLOR_BIT,
						VK_IMAGE_VIEW_TYPE_CUBE,
						glm::ivec3(info.resolution, info.resolution, 1),
						VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
						VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
						S3DRenderTargetType::Color,
						true,
						VK_SAMPLE_COUNT_1_BIT,
						false,
						6
					}
					);

					AttachmentInfo attachment{};
					attachment.clear = { 0.0, 0.0, 0.0 };
					attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
					attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
					attachment.framebufferAttachment = cubemap->atmosphericScatteringCubeRenderTarget;

					cubemap->atmosphericScatteringCubeRenderPass = new S3DRenderPass(engineDevice, { attachment });

					for (int i = 0; i < 6; i++) {
						cubemap->atmosphericScatteringCubeFramebuffers[i] = new S3DFramebuffer(engineDevice, cubemap->atmosphericScatteringCubeRenderPass, { cubemap->atmosphericScatteringCubeRenderTarget }, i);
					}

					S3DGraphicsPipelineConfigInfo configInfo{};
					configInfo.disableDepthWrite();
					configInfo.disableDepthTest();
					configInfo.enableVertexDescriptions(VertexDescriptionOptions_Position);
					configInfo.renderPass = cubemap->atmosphericScatteringCubeRenderPass->getRenderPass();
					configInfo.pipelineLayout = atmosphericScatteringPipelineLayout->getPipelineLayout();

					S3DShaderSpecialization specialization;
					specialization.addConstant(0, VK_FALSE); // ENABLE_SUN
					cubemap->atmosphericScatteringPipelineCubemap = make_uPtr<S3DGraphicsPipeline>(
						engineDevice, std::vector<S3DShader>{
						S3DShader{ ShaderType::Vertex, "resources/shaders/rendering/sky_atmosphere.vert", "rendering" },
							S3DShader{ ShaderType::Fragment, "resources/shaders/rendering/sky_atmosphere.frag", "rendering", specialization },
					}, configInfo
					);

					BindlessDescriptorWriter::BindlessDescriptorWriter(*resourceSystem->getDescriptor())
						.writeImage(BINDLESS_SAMPLER_BINDING, cubemap->reflection_id, &cubemap->atmosphericScatteringCubeRenderTarget->getDescriptor())
						.build();
				}
				{
					S3DShaderSpecialization specialization;
					const float sampleDelta = 3.1415926f / 8.f;
					specialization.addConstant(0, sampleDelta);
					cubemap->envIrradianceGen = new PostProcessingEffect(
						engineDevice,
						{ 64, 64 },
						S3DShader(ShaderType::Fragment, "resources/shaders/processing/envirrgen_b10g11r11.frag", "misc", specialization),
						{ cubemap->atmosphericScatteringCubeRenderTarget->getDescriptor() },
						VK_FORMAT_B10G11R11_UFLOAT_PACK32,
						VK_IMAGE_VIEW_TYPE_CUBE,
						6,
						1
					);
					BindlessDescriptorWriter::BindlessDescriptorWriter(*resourceSystem->getDescriptor())
						.writeImage(BINDLESS_SAMPLER_BINDING, cubemap->irradiance_id, &cubemap->envIrradianceGen->getAttachment()->getDescriptor())
						.build();
				}
				skyAtmosphereReflections[info.actor] = std::move(cubemap);
				*info.reflectionTexture = cubemap->reflection_id;
				*info.irradianceTexture = cubemap->irradiance_id;
			}
		}
		rebuildReflections.clear();
		for (int i = 0; i < destroyReflectionQueue.size(); i++) {
			auto& pair = destroyReflectionQueue[i];
			if (pair.first) {
				vkDeviceWaitIdle(engineDevice.device());
				resourceSystem->getDescriptor()->freeIndex(BINDLESS_SAMPLER_BINDING, pair.first->reflection_id);
				resourceSystem->getDescriptor()->freeIndex(BINDLESS_SAMPLER_BINDING, pair.first->irradiance_id);
				for (int i = 0; i < 6; i++) {
					delete pair.first->atmosphericScatteringCubeFramebuffers[i];
				}
				delete pair.first->atmosphericScatteringCubeRenderPass;
				delete pair.first->atmosphericScatteringCubeRenderTarget;
				delete pair.first->envIrradianceGen;
				delete pair.first;
			}
			
			destroyReflectionQueue.erase(destroyReflectionQueue.begin() + i);
			i--;
		}
	}
	void SkyAtmosphereSystem::createPipeline(VkDescriptorSetLayout globalSetLayout, VkRenderPass renderPass, bool renderSun) {
		atmosphericScatteringPush = S3DPushConstant(sizeof(AtmosphericScatteringPush), VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT);
		atmosphericScatteringPipelineLayout = make_uPtr<S3DPipelineLayout>(engineDevice, std::vector<VkPushConstantRange>{atmosphericScatteringPush.getRange()}, std::vector<VkDescriptorSetLayout>{globalSetLayout});

		S3DGraphicsPipelineConfigInfo configInfo{};
		configInfo.disableDepthWrite();
		configInfo.enableVertexDescriptions(VertexDescriptionOptions_Position);
		if (ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit) {
			configInfo.reverseDepth();
		}
		configInfo.renderPass = renderPass;
		configInfo.pipelineLayout = atmosphericScatteringPipelineLayout->getPipelineLayout();

		S3DShaderSpecialization specialization;
		specialization.addConstant(0, static_cast<VkBool32>(renderSun)); // ENABLE_SUN
		atmosphericScatteringPipeline = make_uPtr<S3DGraphicsPipeline>(
			engineDevice, std::vector<S3DShader>{
			S3DShader{ ShaderType::Vertex, "resources/shaders/rendering/sky_atmosphere.vert", "rendering" },
			S3DShader{ ShaderType::Fragment, "resources/shaders/rendering/sky_atmosphere.frag", "rendering", specialization },
		}, configInfo
		);

	}
}