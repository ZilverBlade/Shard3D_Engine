#include "velocity_buffer_system.h"

namespace Shard3D::Systems {
	VelocityBufferSystem::VelocityBufferSystem(
		S3DDevice& device,
		glm::vec2 resolution,
		S3DRenderTarget* depthStencilImage,
		S3DRenderTarget* gbuffer4Image,
		S3DRenderTarget* velocityImage,
		VkDescriptorSetLayout uboSetLayout
	) : engineDevice(device), vBufferRenderTarget(velocityImage), vBufferFormat(velocityImage->getAttachmentDescription().format), currentDepth(depthStencilImage) {
		depthStencilInputDescriptorSetLayout = S3DDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT)
			.addBinding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT)
			.build();

		createPasses(resolution);
		createDescriptors(depthInFlight->getDescriptor(), gbuffer4Image->getDescriptor());
		createPipelineLayout(uboSetLayout);
		createPipeline();
	}

	VelocityBufferSystem::~VelocityBufferSystem() {
		engineDevice.staticMaterialPool->freeDescriptors({ depthStencilInputDescriptor });
		delete vBufferFramebuffer;
		delete vBufferRenderPass;
		delete depthInFlight;
	}

	void VelocityBufferSystem::renderAndUpdate(FrameInfo& frameInfo) {
		if (!depthIsStored) {
			engineDevice.transitionImageLayout(
				frameInfo.commandBuffer,
				depthInFlight->getImage(),
				depthInFlight->getAttachmentDescription().format,
				VK_IMAGE_LAYOUT_UNDEFINED,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 
				0,
				1,
				0,
				1
			);
		}

		VkImageSubresourceLayers srcLayers = {};
		srcLayers.aspectMask = currentDepth->getAttachmentDescription().imageAspect;
		srcLayers.baseArrayLayer = 0;
		srcLayers.layerCount = 1;
		srcLayers.mipLevel = 0;
		VkImageSubresourceLayers dstLayers = {};
		dstLayers.aspectMask = depthInFlight->getAttachmentDescription().imageAspect;
		dstLayers.baseArrayLayer = 0;
		dstLayers.layerCount = 1;
		dstLayers.mipLevel = 0;

		engineDevice.transitionImageLayout(
			frameInfo.commandBuffer,
			depthInFlight->getImage(),
			depthInFlight->getAttachmentDescription().format,
			depthInFlight->getImageLayout(),
			VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			0,
			1,
			0,
			1
		);


		engineDevice.transitionImageLayout(
			frameInfo.commandBuffer,
			currentDepth->getImage(),
			currentDepth->getAttachmentDescription().format,
			depthIsStored ? currentDepth->getImageLayout() : VK_IMAGE_LAYOUT_UNDEFINED,
			VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
			0,
			1,
			0,
			1
		);

		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::IMAGE_COPY, "VELOCITY_BUFFER_DEPTH", frameInfo.commandBuffer);
		engineDevice.copyImage(
			frameInfo.commandBuffer,
			{ (uint32_t)currentDepth->getDimensions().x, (uint32_t)currentDepth->getDimensions().y, (uint32_t)currentDepth->getDimensions().z },
			currentDepth->getImage(),
			VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
			srcLayers,
			{ 0, 0, 0 },
			depthInFlight->getImage(),
			VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			dstLayers,
			{ 0, 0, 0 }
		);

		engineDevice.transitionImageLayout(
			frameInfo.commandBuffer,
			depthInFlight->getImage(),
			depthInFlight->getAttachmentDescription().format,
			VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
			0,
			1,
			0,
			1
		);
		engineDevice.transitionImageLayout(
			frameInfo.commandBuffer,
			currentDepth->getImage(),
			currentDepth->getAttachmentDescription().format,
			VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
			currentDepth->getImageLayout(),
			0,
			1,
			0,
			1
		);
		vBufferRenderPass->beginRenderPass(frameInfo.commandBuffer, vBufferFramebuffer);

		VkDescriptorSet setsM[2]{
			frameInfo.globalDescriptorSet,
			depthStencilInputDescriptor
		};
		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			vBufferPipelineLayout->getPipelineLayout(),
			0,
			2,
			setsM,
			0,
			nullptr
		);

		vBufferPush.push(frameInfo.commandBuffer, vBufferPipelineLayout->getPipelineLayout(), oldCamera);

		vBufferPipeline->bind(frameInfo.commandBuffer);

		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::DRAW, "VELOCITY_BUFFER_GEN", frameInfo.commandBuffer);
		vkCmdDraw(frameInfo.commandBuffer, 6, 1, 0, 0);

		vBufferRenderPass->endRenderPass(frameInfo.commandBuffer);
		oldCamera.invView = frameInfo.camera.getInverseView();
		oldCamera.invProj = frameInfo.camera.getInverseProjection();
		depthIsStored = true;
	}

	void VelocityBufferSystem::resize(glm::vec2 newResolution, S3DRenderTarget* newDepthImage, S3DRenderTarget* newGBuffer4Image) {
		currentDepth = newDepthImage;
		createPasses(newResolution);
		createDescriptors(depthInFlight->getDescriptor(), newGBuffer4Image->getDescriptor());
	}

	void VelocityBufferSystem::createPipelineLayout(VkDescriptorSetLayout uboSetLayout) {
		vBufferPush = S3DPushConstant(sizeof(VelocityBufferPush), VK_SHADER_STAGE_FRAGMENT_BIT);
		vBufferPipelineLayout = make_uPtr<S3DPipelineLayout>(engineDevice, std::vector<VkPushConstantRange>{ vBufferPush.getRange() }, std::vector<VkDescriptorSetLayout>{ uboSetLayout, depthStencilInputDescriptorSetLayout->getDescriptorSetLayout() });
	}

	void VelocityBufferSystem::createPipeline() {
		S3DGraphicsPipelineConfigInfo vBufferConfigInfo(1);
		vBufferConfigInfo.disableDepthTest();
		vBufferConfigInfo.setCullMode(VK_CULL_MODE_BACK_BIT);
		vBufferConfigInfo.pipelineLayout = vBufferPipelineLayout->getPipelineLayout();
		vBufferConfigInfo.renderPass = vBufferRenderPass->getRenderPass();

		vBufferPipeline = make_uPtr<S3DGraphicsPipeline>(engineDevice, std::vector<S3DShader>{
			S3DShader{ ShaderType::Vertex, "resources/shaders/fullscreen_quad.vert", "misc" },
			S3DShader{ ShaderType::Fragment, "resources/shaders/rendering/velocity_buffer.frag", "rendering" },
		}, vBufferConfigInfo);
	}

	void VelocityBufferSystem::createPasses(glm::vec2 resolution) {
		PTR_DELETE(vBufferFramebuffer);
		PTR_DELETE(vBufferRenderPass);
		PTR_DELETE(depthInFlight);

		AttachmentInfo vBufferAttachmentInfo = {};
		vBufferAttachmentInfo.framebufferAttachment = vBufferRenderTarget;
		vBufferAttachmentInfo.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
		vBufferAttachmentInfo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

		vBufferRenderPass = new S3DRenderPass(engineDevice, { vBufferAttachmentInfo });

		vBufferFramebuffer = new S3DFramebuffer(engineDevice, vBufferRenderPass, { vBufferRenderTarget });

		depthInFlight = new S3DRenderTarget(engineDevice, {
				currentDepth->getAttachmentDescription().format,
				VK_IMAGE_ASPECT_DEPTH_BIT,
				VK_IMAGE_VIEW_TYPE_2D,
				{resolution, 1},
				VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				S3DRenderTargetType::Depth,
				false,
				VK_SAMPLE_COUNT_1_BIT
			}
		);

		depthIsStored = false;
		oldCamera = {};
	}

	void VelocityBufferSystem::createDescriptors(VkDescriptorImageInfo depthInFlightImageInfo, VkDescriptorImageInfo gbuffer4ImageInfo) {
		depthInFlightImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL; // we transition the layout
		S3DDescriptorWriter(*depthStencilInputDescriptorSetLayout, *engineDevice.staticMaterialPool)
			.writeImage(0, &depthInFlightImageInfo)
			.writeImage(1, &gbuffer4ImageInfo)
			.build(depthStencilInputDescriptor);
	}
}

