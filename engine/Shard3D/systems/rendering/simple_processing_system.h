#pragma once

#include "../../s3dstd.h"
#include "../../vulkan_abstr.h"
#include "../../core/misc/frame_info.h"	  
#include "../../core/vulkan_api/graphics_pipeline.h"
#include "../../core/rendering/render_pass.h"


namespace Shard3D {
	inline namespace Systems {
		// Very easy custom post processing class, however is not the best performing and is meant for static processing
		class SimpleProcessingSystem {
		public:
			SimpleProcessingSystem(
				S3DDevice& device,
				S3DShader shader,
				VkDescriptorImageInfo* imageInfo, // arbitrary image
				glm::vec2 resolution,
				VkFormat format,
				void* buffer = nullptr,
				size_t bufferSize = 0
			);
			~SimpleProcessingSystem();

			DELETE_COPY(SimpleProcessingSystem);

			void process(VkCommandBuffer commandBuffer);
			uPtr<S3DRenderTarget>& getWrittenAttachment() { return render_attachment; } // hack because i dont feel like refactoring code moment
		private:
			void createRenderPass();
			void createPipelineLayout();
			void createPipelines(VkRenderPass renderPass);

			uPtr<S3DRenderTarget> render_attachment;
			S3DFramebuffer* framebuffer;
			S3DRenderPass* renderpass;
			S3DDevice& engineDevice;

			uPtr<S3DGraphicsPipeline> shaderPipeline;
			uPtr<S3DBuffer> shaderBuffer;

			VkDescriptorSet descriptorSet{};
			uPtr<S3DDescriptorSetLayout> descriptorLayout{};

			VkPipelineLayout pipelineLayout{}; 
			uPtr<S3DDescriptorPool> localPool{};

			VkFormat format;
			S3DShader shader;
			glm::vec2 resolution;
		};
	}
}