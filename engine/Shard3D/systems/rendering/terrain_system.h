#pragma once
#include "../../s3dstd.h"
#include "../../vulkan_abstr.h"
#include "../../core/misc/frame_info.h"	  
#include "../../core/ecs/level.h"
#include "../../core/ecs/actor.h"
#include "../../core/asset/importer_model.h"
#include "../../core/asset/mesh3d.h"
#include "../../core/asset/material.h"
#include "../../core/rendering/render_target.h"

namespace Shard3D {
	struct TerrainMaterialBlendBufferData {
		uint32_t maxMaterials;
		alignas(16)TerrainMaterialBufferData materials[8];
	};
	struct TerrainTileData {
		glm::vec2 coord;
		float LOD;
	};

	class TerrainTexture {
	public:
		TerrainTexture(S3DDevice& device);
		~TerrainTexture();

		void createTexture(glm::ivec2 resolution, int levels, VkFormat editorFormat, VkFormat gameFormat);
		void loadTexture(ktxTexture2* ktx, VkFormat editorFormat, glm::uvec2 editorResolution);
		ktxTexture2* getKTXTexture(float resolutionFactor = 1.0f);

		VkImage tileImage = VK_NULL_HANDLE;
		VkImageView tileImageView = VK_NULL_HANDLE;
		VkImageView tileImageViewMips = VK_NULL_HANDLE;
		VkDeviceMemory tileImageMemory = VK_NULL_HANDLE;
		VkSampler tileSampler = VK_NULL_HANDLE;
		VkImageLayout imageLayout = VK_IMAGE_LAYOUT_GENERAL;

		VkFormat editorFormat;
		VkFormat gameFormat;

		VkDeviceSize sizeBytes;
		glm::ivec2 resolution;
		int mipLevels = 1;
	private:
		void recreateSampler();
		void createImageView();

		S3DDevice& device;
	};

	enum class TerrainTileDivisions {
		D16x16 = 16,
		D32x32 = 32,
		D64x64 = 64,
		D128x128 = 128,
		D256x256 = 256
	};

	class TerrainActor {
	public:
		TerrainActor(S3DDevice& device);
		~TerrainActor();

		TerrainActor(const TerrainActor&) = delete;
		TerrainActor& operator=(const TerrainActor&) = delete;

		void render(FrameInfo& frameInfo, uint32_t lod);

		void populateMaterialBuffer(ResourceSystem* resourceSystem, const std::vector<TerrainMaterial*>& terrainMaterials);
		void createBufferData();
		void createTerrainUpdateObjects();
		void createDescriptorData(uPtr<S3DDescriptorSetLayout>& descriptorSetLayout);
		void setMeshData(ResourceSystem* resourceSystem);

		void updateVisibilityCalcs(VkCommandBuffer commandBuffer, S3DCamera& camera, float quality, float lodBias = 0.0f);

		void loadFromAsset(Actor actor, AssetID asset);
		uint32_t getLODCount() {
			return heightMap->mipLevels;
		}

		TerrainTileDivisions divisions;
		VkDescriptorSet terrainDescriptor{};

		int numTilesX, numTilesY;

		float tileExtent;
		float heightMul = 1.0f;
		float heightOffset;

		uPtr<S3DBuffer> terrainMaterialBuffer;
		uPtr<S3DBuffer> terrainAABBBuffer;

		std::vector<VolumeBounds> terrainAABBs;
		uPtr<TerrainTexture> heightMap{};
		uPtr<TerrainTexture> materialMap{};
	private:

		VkDescriptorSet terrainCullDescriptorSet{};
		uPtr<S3DDescriptorSetLayout> terrainCullDescriptorSetLayout;
		uPtr<S3DBuffer> terrainVisibilityBuffer;
		uPtr<S3DBuffer> terrainDrawIndirectBuffer;
		uPtr<S3DBuffer> terrainMeshDataBuffer;

		uPtr<S3DComputePipeline> terrainUpdatePipeline;
		uPtr<S3DPipelineLayout> terrainUpdatePipelineLayout;
		S3DPushConstant terrainUpdatePush;

		S3DDevice& device;
		uint32_t framesInFlight;
	};

	class TerrainList {
	public:
		TerrainList(S3DDevice& device, uPtr<S3DDescriptorSetLayout>& terrainDescriptorSetLayout, VkDescriptorSetLayout globalSetLayout);
		void update(sPtr<Level>& level, ResourceSystem* resourceSystem); 
		Actor getActor() {
			return actor;
		}
		sPtr<TerrainActor>& getTerrain() {
			return terrain;
		}
	private:
		S3DDevice& device;
		Actor actor;
		sPtr<TerrainActor> terrain;
		uPtr<S3DDescriptorSetLayout>& terrainDescriptorSetLayout;
		VkDescriptorSetLayout globalSetLayout;
	};

	struct TerrainPhysicsTile {
		std::vector<float> heights;
		std::vector<uint8_t> materialData;
		int materialDataResX, materialDataResY;
		int verticesX, verticesY;
		float widthX, widthY;
		float offsetX, offsetY;
		VolumeBounds aabb;
	};
	struct TerrainPhysicsData {
		std::vector<TerrainPhysicsTile> tiles;
		glm::vec3 position;
	};

	struct TerrainData {
		sPtr<TerrainPhysicsData> physicsData;
		sPtr<TerrainActor> textureData;
	};

}