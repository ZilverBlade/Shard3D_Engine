#pragma once
/*
#include "../../s3dstd.h"
#include "../../vulkan_abstr.h"
#include "../../core/misc/frame_info.h"	  
#include "../../core/rendering/render_pass.h"
#include "../../core/asset/assetmgr.h"
#include "../post_fx/post_processing_fx.h"
namespace Shard3D {
	inline namespace Systems {
		struct AmbientOcclusionMap {
			DELETE_COPY(AmbientOcclusionMap);

			AmbientOcclusionMap(ResourceSystem* resourceSystem_) : resourceSystem(resourceSystem_), shadowMap_id(resourceSystem_->getDescriptor()->allocateIndex(BINDLESS_SAMPLER_BINDING)) { SHARD3D_INFO("Shadow Map {0} was created!", shadowMap_id); }

			~AmbientOcclusionMap() {
				PTR_DELETE(densityAttachment)
				PTR_DELETE(depthAttachment);
				PTR_DELETE(depthFramebuffer);
				PTR_DELETE(densityFramebuffer);
				PTR_DELETE(depthRenderPass);
				PTR_DELETE(densityRenderPass);
				PTR_DELETE(opaque_var_pipeline);
				PTR_DELETE(masked_var_pipeline);
				PTR_DELETE(opaque_den_pipeline);
				PTR_DELETE(masked_den_pipeline);
				//PTR_DELETE(translucent_pipeline);
				SHARD3D_INFO("Shadow Map {0} was destroyed!", shadowMap_id);
				resourceSystem->getDescriptor()->freeIndex(BINDLESS_SAMPLER_BINDING, shadowMap_id);
			}

			S3DRenderTarget* depthAttachment{};
			S3DRenderTarget* densityAttachment{};

			S3DFramebuffer* depthFramebuffer{};
			S3DFramebuffer* densityFramebuffer{};
			S3DRenderPass* depthRenderPass{};
			S3DRenderPass* densityRenderPass{};

			S3DGraphicsPipeline* opaque_var_pipeline{};
			S3DGraphicsPipeline* masked_var_pipeline{};			// bind mask texture for this pipeline
			S3DGraphicsPipeline* opaque_den_pipeline{};
			S3DGraphicsPipeline* masked_den_pipeline{};			// bind mask texture for this pipeline

			ShaderResourceIndex shadowMap_id;
			S3DCamera camera{};

			uPtr<PostProcessingEffect> vfaoFilter;

			ResourceSystem* resourceSystem;
		};
		class VarianceFilteredAmbientOcclusionSystem {
		public:
			VarianceFilteredAmbientOcclusionSystem(S3DDevice& device);
			~VarianceFilteredAmbientOcclusionSystem();

			VarianceFilteredAmbientOcclusionSystem(const VarianceFilteredAmbientOcclusionSystem&) = delete;
			VarianceFilteredAmbientOcclusionSystem& operator=(const VarianceFilteredAmbientOcclusionSystem&) = delete;

			void render(FrameInfo& frameInfo);
			void runGarbageCollector(sPtr<Level>& level);
		private:

			AmbientOcclusionMap* createSkyLightAmbientOcclusionMap(int resolution, ShaderResourceIndex* outID);
			void createPipelineLayout();

			std::vector<uint32_t> opaqueMaterials{};
			std::vector<uint32_t> maskedMaterials{};
			std::vector<uint32_t> translucentMaterials{};

			//std::vector<std::pair<_LightComponentInfo, bool>> rebuildMaps{};
			std::unordered_map<UUID, AmbientOcclusionMap*> shadowMaps{};
			std::vector<UUID> removedShadowMaps{};

			bool madeShadowMap = false;
			ShaderResourceIndex test;

			S3DDevice& engineDevice;

			S3DPipelineLayout* opaque_var_pipelineLayout;
			S3DPipelineLayout* masked_var_pipelineLayout;
			S3DPipelineLayout* opaque_den_pipelineLayout;
			S3DPipelineLayout* masked_den_pipelineLayout;
		};
	}
}
*/