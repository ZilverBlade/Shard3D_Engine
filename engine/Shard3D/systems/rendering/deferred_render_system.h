#pragma once

#include "../../s3dstd.h"
#include "../../vulkan_abstr.h"
#include "../../core/misc/frame_info.h"	  
#include "../../core/ecs/level.h"
#include "../../core/ecs/components.h"
#include "../handlers/render_list.h"
#include "gbuffer_struct.h"
#include "../post_fx/post_processing_fx.h"
namespace Shard3D {
	inline namespace Resources {
		class TextureCube;
		class Model3D;
	}
	namespace Components {
		struct MobilityComponent;
	}	
	struct SurfaceMaterialMeshIteratorObjectEntity;
	class MaterialSystem;

	inline namespace Systems {
		class DeferredRenderSystem {
			static inline std::unordered_set<DeferredRenderSystem*> these{};
	  	public:
			struct RendererFeatures {
				bool enableSSAO = true;
				bool enableTranslucency = true;
				bool enableEarlyZ = true;
				bool enableMSAA = false;
				bool enableClipPlane = false;
				bool enableNormalDecals = false;
				bool enableVelocityBuffer = false; // for dynamic transforms
				VkFormat velocityBufferFormat = VK_FORMAT_R8G8_SNORM;
				bool enableStencilBuffer = false;
				bool enableDeferredLighting = false;
				bool enableDeferredReflections = false; // requires deferred lighting to be enabled (NOT IMPLEMENTED)

				VkFormat deferredLightingFormat = VK_FORMAT_B10G11R11_UFLOAT_PACK32;
				VkFormat finalFrameBufferFormat = VK_FORMAT_R16G16B16A16_SFLOAT;
				VkSampleCountFlagBits msaaSamples = VK_SAMPLE_COUNT_2_BIT;
			};
			DeferredRenderSystem(S3DDevice& device, ResourceSystem* resourceSystem, DeferredRenderSystem::RendererFeatures rendererFeatures, VkDescriptorSetLayout globalSetLayout, VkDescriptorSetLayout sceneSetLayout, VkDescriptorSetLayout riggedSkeletonSetLayout, VkDescriptorSetLayout terrainSetLayout, glm::vec2 resolution);
			~DeferredRenderSystem();

			DELETE_COPY(DeferredRenderSystem)

			static void recompileShaders() {
				for (auto* thiss : these) {
					thiss->getMaterialSystem()->recompileSurface(ProjectSystem::getEngineSettings().allowedMaterialPermutations);
				}
			}
			void renderEarlyDepthMesh3D(FrameInfo& frameInfo, MeshMobilityOptionsFlags mobility);
			void renderGBufferMesh3D(FrameInfo& frameInfo, MeshMobilityOptionsFlags mobility);
			void renderEarlyDepthTerrain(FrameInfo& frameInfo);
			void renderGBufferTerrain(FrameInfo& frameInfo);
			void renderForwardMesh3D(FrameInfo& frameInfo, MeshMobilityOptionsFlags mobility);
			void renderSkybox(FrameInfo& frameInfo);
			void renderSSAO(FrameInfo& frameInfo);

			void beginEarlyZPass(FrameInfo& frameInfo);
			void endEarlyZPass(FrameInfo& frameInfo);
			void beginGBufferPass(FrameInfo& frameInfo);
			void endGBufferPass(FrameInfo& frameInfo);
			void renderDeferredLighting(FrameInfo& frameInfo, SceneBuffers& ssbo);
			void composeLighting(FrameInfo& frameInfo);
			void composeLightingMS(FrameInfo& frameInfo);
			void beginBlendingPass(FrameInfo& frameInfo);
			void endBlendingPass(FrameInfo& frameInfo);
			void beginCompositionPass(FrameInfo& frameInfo);
			void composeAlpha(FrameInfo& frameInfo);
			void endComposition(FrameInfo& frameInfo);

			// call after creation and after resize
			void writeDescriptors(VkDescriptorImageInfo normalDBuffer = {});


			const RendererFeatures& getRendererFeatures() {
				return rendererFeatures;
			}

			SceneBufferInputData getGBufferAttachments() {
				return { 			
					finalRenderTarget,
					depthRenderTarget,
					gbuffer0,
					gbuffer1,
					gbuffer2,
					gbuffer3,
					gbuffer4
				};
			}
			LightingBufferInputData getLBufferAttachments() {
				 return {
					 lbuffer0,
					 lbuffer1
				 };
			 }

			void resize(glm::ivec3 newSize);
			S3DRenderPass* getEarlyZRenderPass() { return renderPassEarlyDepth; }
			S3DRenderPass* getDeferredRenderPass() { return renderPassGBuffer; }
			S3DRenderPass* getTransparencyRenderPass() { return renderPassWeightedBlending; }
			S3DRenderPass* getCompositionRenderPass() { return renderPassComposition; }
			uPtr<S3DDescriptorSetLayout>& getGBufferAttachmentInputLayout() { return lightingInputLayout; }
			VkDescriptorSet& getGBufferAttachmentInputDescriptorSet() { return lightingInputDescriptorSet; }
			uPtr<S3DDescriptorSetLayout>& getAlphaCompositInputLayout() { return alphaCompositeInputLayout; }
			
			uPtr<S3DDescriptorSetLayout>& getDeferredLitInputLayout() { return deferredLitLayout; }

			VkDescriptorSet& getAlphaCompositeInputDescriptorSet() { return alphaCompositeInputDescriptorSet; }
			MaterialSystem* getMaterialSystem() { return materialSystem; }
			
			S3DRenderTarget* getVelocityBuffer() { return velocityBuffer; }
			S3DRenderTarget* getSSAOImageAttachment() { return ssaoProcess->getAttachment(); }
			S3DRenderTarget* getWBColAccumAttachment() { return colorAccumulationAttachment; }
			S3DRenderTarget* getWBColRevealAttachment() { return colorRevealAttachment; }
		private:
			void bindDeferredShading(VkCommandBuffer commandBuffer, VkDescriptorSet globalSet, VkDescriptorSet sceneSSBO, bool msPass = false);
			void bindDeferredComposition(VkCommandBuffer commandBuffer);

			void createRenderPasses(glm::vec2 resolution);
			void createFramebuffers();

			void createSSAOSampleBuffer();

			void createSkyboxPipeline(VkDescriptorSetLayout globalSetLayout, VkDescriptorSetLayout sceneSetLayout);
			void createSSAOPipeline(VkDescriptorSetLayout globalSetLayout);
			void createTerrainPipelines(VkDescriptorSetLayout globalSetLayout, VkDescriptorSetLayout terrainSetLayout);
			void createGBufferResolvePipeline(VkDescriptorSetLayout globalSetLayout);
			
			void createDeferredPipelines(VkDescriptorSetLayout globalSetLayout, VkDescriptorSetLayout sceneSetLayout);


			void renderClass(FrameInfo& frameInfo, SurfaceMaterialMeshIteratorObjectEntity* obj, SurfaceMaterialPipelineClassPermutationFlags flags);
			void bindClass(FrameInfo& frameInfo, SurfaceMaterialPipelineClassPermutationFlags flags);

			void resolveGBuffer(FrameInfo& frameInfo);

			S3DRenderTarget* finalRenderTarget;
			S3DRenderTarget* depthRenderTarget;
			S3DRenderTarget* gbuffer0; // diffuse, reflectivity
			S3DRenderTarget* gbuffer1; // specular, glossiness
			S3DRenderTarget* gbuffer2; // emission
			S3DRenderTarget* gbuffer3; // normals
			S3DRenderTarget* gbuffer4; // shading model
			S3DRenderTarget* lbuffer0; // diffuse lighting (DEFERRED LIGHTING)
			S3DRenderTarget* lbuffer1; // specular lighting (DEFERRED LIGHTING)
			//S3DRenderTarget* lbuffer2; // reflections (DEFERRED LIGHTING + DEFERRED REFLECTIONS)
			//S3DRenderTarget* lbuffer3; // irradiance (DEFERRED LIGHTING + DEFERRED REFLECTIONS)
			S3DRenderTarget* velocityBuffer;
			S3DRenderTarget* colorAccumulationAttachment;
			S3DRenderTarget* colorRevealAttachment;
			//S3DRenderTarget* translucentRefractionAttachment;
			//S3DRenderTarget* translucentRefractionRoughnessAttachment;

			S3DRenderTarget* depthRenderTargetMS;
			S3DRenderTarget* gbuffer0MS;
			S3DRenderTarget* gbuffer1MS;
			S3DRenderTarget* gbuffer2MS;
			S3DRenderTarget* gbuffer3MS;
			S3DRenderTarget* gbuffer4MS;
			S3DRenderTarget* velocityBufferMS;
			S3DRenderTarget* colorAccumulationAttachmentMS;
			S3DRenderTarget* colorRevealAttachmentMS;
			S3DRenderTarget* msaaSampleWeightBuffer;
			S3DRenderTarget* msaaSampleMaskStencilBuffer;

			S3DFramebuffer* framebufferGBufferMSResolve;
			S3DRenderPass* renderPassGBufferMSResolve;
			S3DFramebuffer* framebufferGBufferMSResolveStencilWrite;
			S3DRenderPass* renderPassGBufferMSResolveStencilWrite;
			S3DRenderPass* renderPassDeferredShadingMS;

			S3DFramebuffer* framebufferEarlyDepth;
			S3DRenderPass* renderPassEarlyDepth;
			S3DFramebuffer* framebufferGBuffer;
			S3DRenderPass* renderPassGBuffer;
			S3DFramebuffer* framebufferWeightedBlending;
			S3DRenderPass* renderPassWeightedBlending;
			S3DFramebuffer* framebufferDeferredLighting;
			S3DRenderPass* renderPassDeferredLighting;
			S3DFramebuffer* framebufferDeferredShading;
			S3DRenderPass* renderPassDeferredShading;
			S3DFramebuffer* framebufferComposition;
			S3DRenderPass* renderPassComposition;


			uPtr<S3DDescriptorSetLayout> lightingInputLayout{};
			VkDescriptorSet lightingInputDescriptorSet{};
			uPtr<S3DDescriptorSetLayout> ssaoInputLayout{};
			VkDescriptorSet ssaoInputDescriptorSet{};
			uPtr<S3DDescriptorSetLayout> alphaCompositeInputLayout{};
			VkDescriptorSet alphaCompositeInputDescriptorSet{};

			uPtr<S3DDescriptorSetLayout> deferredLitLayout{};
			VkDescriptorSet deferredLitDescriptorSet{};
			

			uPtr<S3DDescriptorSetLayout> deferredResolveInputLayout{};
			VkDescriptorSet deferredResolveInputDescriptorSet{};

			S3DRenderPass* renderPassSSAO;
			S3DRenderTarget* ssaoAttachmentInterleave[2][2];
			S3DFramebuffer* framebufferSSAOInterleave[2][2];
			uPtr<PostProcessingEffect> ssaoInterleaveCombine;
			uPtr<PostProcessingEffect> ssaoBlurH0;
			uPtr<PostProcessingEffect> ssaoBlurV0;
			uPtr<PostProcessingEffect> ssaoProcess;

			S3DDevice& engineDevice;
			ResourceSystem* resourceSystem;

			// special stuff
			uPtr<S3DGraphicsPipeline> skyboxPipeline{};
			S3DPipelineLayout* skyboxPipelineLayout{};

			uPtr<S3DGraphicsPipeline> ssaoPipeline{};
			S3DPipelineLayout* ssaoPipelineLayout{};

			uPtr<S3DGraphicsPipeline> terrainGBufferPipeline{};
			uPtr<S3DGraphicsPipeline> terrainEarlyZPipeline{};
			uPtr<S3DPipelineLayout> terrainPipelineLayout{};
			S3DPushConstant terrainPush{};

			uPtr<S3DGraphicsPipeline> deferredResolvePipeline{};
			S3DPipelineLayout* deferredResolvePipelineLayout{};
			S3DPushConstant deferredResolvePush{};
			uPtr<S3DGraphicsPipeline> deferredResolveMSStencilWritePipeline{};
			S3DPipelineLayout* deferredResolveMSStencilWritePipelineLayout{};

			S3DPushConstant deferredLightingPush;
			S3DPipelineLayout* deferredLightingPipelineLayout{};
			uPtr<S3DGraphicsPipeline> deferredLightingDirectionalPipeline{};
			uPtr<S3DGraphicsPipeline> deferredLightingSpotPipeline{};
			uPtr<S3DGraphicsPipeline> deferredLightingPointPipeline{};
			S3DPipelineLayout* deferredShadingPipelineLayout{};
			uPtr<S3DGraphicsPipeline> deferredShadingPipeline{};
			uPtr<S3DGraphicsPipeline> deferredShadingPipelineMS{};
			S3DPipelineLayout* deferredCompositionPipelineLayout{};
			uPtr<S3DGraphicsPipeline> deferredCompositionPipeline{};

			DeferredRenderSystem::RendererFeatures rendererFeatures;

			uPtr<S3DBuffer> ssaoSampleBuffer;
			uPtr<S3DDescriptorSetLayout> ssaoSamplesInputDescriptorLayout{};
			VkDescriptorSet ssaoSamplesInputDescriptorSet{};
			
			bool useReverseDepth = false;

			MaterialSystem* materialSystem{};
			friend class MaterialSystem;
		};
	}
}