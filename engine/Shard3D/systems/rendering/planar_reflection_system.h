#pragma once
#include "../../core/ecs/actor.h"
#include "deferred_render_system.h"
#include "decal_render_system.h"
#include "sky_atmosphere_system.h"
#include "../../core/vulkan_api/destruction.h"
namespace Shard3D {
	class PlanarReflectionInstance {
	public:
		PlanarReflectionInstance(S3DDevice& device, ResourceSystem* resourceSystem, bool irradiance, DeferredRenderSystem::RendererFeatures rendererFeatures, glm::vec2 resolution, VkDescriptorSetLayout sceneSetLayout, VkDescriptorSetLayout skeletonSetLayout, VkDescriptorSetLayout terrainSetLayout, uint32_t framesInFlight);
		~PlanarReflectionInstance();

		void updateResourceSize();

		DELETE_COPY(PlanarReflectionInstance);
		DELETE_MOVE(PlanarReflectionInstance);

		void render(VkCommandBuffer commandBuffer, VkDescriptorSet sceneSSBO, uint32_t frameIndex, float aspectRatio, Actor mainCameraActor, glm::vec4 clipPlane, float clipOffset, glm::mat3 orientation, float irradianceSampleRadius, sPtr<Level>& level, RenderObjects renderObjects);
		void resize(glm::vec2 newResolution, VkDescriptorImageInfo depthSceneInfo);
		DeferredRenderSystem* getRenderer() const {
			return deferredRenderSystem;
		}
		S3DRenderTarget* getFinalImage() const {
			return deferredRenderSystem->getGBufferAttachments().colorAttachment;
		}
		SceneBufferInputData getGBufferImages() const {
			return deferredRenderSystem->getGBufferAttachments();
		}
		VkDescriptorSetLayout getGlobalSetLayout() const {
			return globalSetLayout->getDescriptorSetLayout();
		}

		Telemetry* getTelemetry() const {
			return telemetry;
		}
		float multiplier = 0.5f;
		glm::vec2 getResolution() const {
			return resolution;
		}
		uint32_t getReflectionDescriptor() const {
			return reflectionDescriptorID;
		}
		uint32_t getIrradianceDescriptor() const {
			return irradianceDescriptorID;
		}
		size_t getEstimatedResourceSize() const {
			return resourceSize / 1000; // kB
		}
		double getDrawTimeCPU() const {
			return renderCPU->getTime();
		}
		double getDrawTimeGPU() const {
			return renderGPU->getTime();
		}
		double getIrradianceTimeGPU() const {
			return irradianceGPU->getTime();
		}
	private:
		void createIrradianceGenPasses();
		void createDescriptor();

		uint32_t reflectionDescriptorID = 0;
		uint32_t irradianceDescriptorID = 0;
		Telemetry* telemetry;

		uPtr<TelemetryCPUMetric> renderCPU;
		uPtr<TelemetryGPUMetric> renderGPU;
		uPtr<TelemetryGPUMetric> irradianceGPU;

		S3DDevice& engineDevice;
		ResourceSystem* resourceSystem;

		uPtr<S3DDescriptorPool> globalPool{};
		uPtr<S3DDescriptorSetLayout> globalSetLayout{};
		std::vector<uPtr<S3DBuffer>> uboBuffers{};
		std::vector<VkDescriptorSet> globalDescriptorSets{};

		DeferredRenderSystem* deferredRenderSystem{};
		DecalRenderSystem* decalRenderSystem{};
		SkyAtmosphereSystem* skyAtmosphere{};
		PostProcessingEffect* envIrradianceGen{};
		PostProcessingEffect* envIrradianceBlurH{};
		PostProcessingEffect* envIrradianceBlurV{};

		size_t resourceSize = 0;
		glm::vec2 resolution;
		bool irradianceGen;
	};

	class PlanarReflectionSystem {
	public:
		PlanarReflectionSystem(S3DDevice& device, ResourceSystem* resourceSystem, VkDescriptorImageInfo depthSceneInfo, VkDescriptorSetLayout sceneSetLayout, VkDescriptorSetLayout skeletonSetLayout, VkDescriptorSetLayout terrainSetLayout, uint32_t framesInFlight);
		~PlanarReflectionSystem();

		DELETE_COPY(PlanarReflectionSystem);
		DELETE_MOVE(PlanarReflectionSystem);

		void render(FrameInfo& frameInfo);
		void resize(glm::vec2 newScreenSize, VkDescriptorImageInfo depthSceneInfo);
		void runGarbageCollector(uint32_t frameIndex, VkFence fence);
		
		const PlanarReflectionInstance* getReflection(Actor actor) {
			auto seek = reflectionMaps.find(actor);
			if (seek == reflectionMaps.end()) return nullptr;
			return seek->second;
		}
	private:
		struct ReflectionComponentInfo_ {
			Actor actor;
			bool generateIrradiance;
			float resolutionMultiplier;
			VkFormat finalImageFormat;
			ShaderResourceIndex* reflectionTexture{};
			ShaderResourceIndex* irradianceTexture{};
		};
		S3DDevice& engineDevice;
		ResourceSystem* resourceSystem;
		VkDescriptorSetLayout sceneSetLayout;
		VkDescriptorSetLayout skeletonSetLayout;
		VkDescriptorSetLayout terrainDescriptorSetLayout;
		VkDescriptorImageInfo depthSceneInfo;

		glm::vec2 currentResolution;

		std::vector<std::pair<ReflectionComponentInfo_, bool>> rebuildReflections{};
		std::vector<std::pair<PlanarReflectionInstance*, S3DDestructionObject>> destroyReflectionQueue{};
		std::unordered_map<entt::entity, PlanarReflectionInstance*> reflectionMaps{};
		std::unordered_set<entt::entity> removedReflectionMaps{};
		uint32_t currentFrameIndex = 0;
		uint32_t maxFramesInFlight = 0;
	};
}