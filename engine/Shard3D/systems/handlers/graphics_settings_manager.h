#pragma once
#include "project_system.h"
namespace Shard3D {
	class GraphicsSettingsManager {
	public:
		static void cmdChangeDisplay(char* setting, void* pVal);
		static void cmdChangeRenderer(char* setting, void* pVal);
		static void cmdChangeTexture(char* setting, void* pVal);
		static void cmdChangePostFX(char* setting, void* pVal);
		static void cmdChangeShadow(char* setting, void* pVal);

		static std::vector<std::function<void(char*, void*)>> cmdChangeCallbackDisplays;
		static std::vector<std::function<void(char*, void*)>> cmdChangeCallbackRenderers;
		static std::vector<std::function<void(char*, void*)>> cmdChangeCallbackTextures;
		static std::vector<std::function<void(char*, void*)>> cmdChangeCallbackPostFXs;
		static std::vector<std::function<void(char*, void*)>> cmdChangeCallbackShadows;
	};
}