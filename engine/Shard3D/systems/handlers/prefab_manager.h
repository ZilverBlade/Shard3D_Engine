#pragma once
#include <entt.hpp>
#include "../../core/misc/UUID.h"
#include "../../s3dstd.h"
#include "../../core.h"
#include "../../core/asset/assetid.h"
#include "../../core/ecs/prefab.h" 

namespace Shard3D {
	inline namespace ECS {
		class Actor;
		class Prefab;
		class Level;
		class PrefabManager {
		public:
			PrefabManager(ResourceSystem* resourceSystem);
			Actor duplicateActor(Actor toCopy, bool addCopyTag = true, UUID uuid = UUID(), bool forceKeepUUID = false, bool isPrefab = false, UUID prefabInheritID = 0, bool constructingPrefab = false, entt::entity actorInheritsHandle = entt::null);

			bool cachePrefab(AssetID asset);
			sPtr<Prefab> getPrefab(AssetID prefab);
			Actor instPrefab(Level* level, UUID scopeUUID, UUID id, AssetID prefab, const std::string& name = "Some kind of prefab");
			Actor reloadPrefab(Actor actor, AssetID prefab);
			void killPrefab(AssetID prefab);

			Actor recursiveGetDerivedTopPrefab(Actor trappedActor);
			UUID recursiveGetUUIDHashTopPrefab(Actor prefabActor);

		private:
			void killAllPrefabs();
			// map
			std::unordered_map<AssetID, sPtr<Prefab>> basePrefabMap{};
			std::unordered_map<AssetID, std::vector<UUID>> instantiatedPrefabs;

			ResourceSystem* resourceSystem;

			friend class Actor;
			friend class Prefab;
			friend class LevelManager;
		};
	}
}