#include "vertex_arena_allocator.h"
#include "../../core/asset/mesh3d.h"

namespace Shard3D {
	VertexArenaAllocator::VertexArenaAllocator(S3DDevice& device, uint32_t maxFramesInFlight) : engineDevice(device), maxFramesInFlight(maxFramesInFlight) {
		VkBufferUsageFlags dlBufferUsage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
		VkMemoryPropertyFlags dlMemoryProperty = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
		indexBuffer = new S3DBuffer(engineDevice, sizeof(uint32_t), ProjectSystem::getEngineSettings().memory.arenaAllocatorDefaultVertexSize * 3u, 
			VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, dlMemoryProperty, true);

		vertexPositionBuffer = new S3DBuffer(engineDevice, sizeof(VTX_pos_offset_12b_), ProjectSystem::getEngineSettings().memory.arenaAllocatorDefaultVertexSize, dlBufferUsage, dlMemoryProperty, true);
		vertexUVBuffer = new S3DBuffer(engineDevice, sizeof(VTX_uv_cmpr_offset_4b_), ProjectSystem::getEngineSettings().memory.arenaAllocatorDefaultVertexSize, dlBufferUsage, dlMemoryProperty, true);
		vertexNormalBuffer = new S3DBuffer(engineDevice, sizeof(VTX_norm_cmpr_offset_4b_), ProjectSystem::getEngineSettings().memory.arenaAllocatorDefaultVertexSize, dlBufferUsage, dlMemoryProperty, true);
		if (ProjectSystem::getEngineSettings().memory.enableVertexTangents)
			vertexTangentBuffer = new S3DBuffer(engineDevice, sizeof(VTX_tg_cmpr_offset_4b_), ProjectSystem::getEngineSettings().memory.arenaAllocatorDefaultVertexSize, dlBufferUsage, dlMemoryProperty, true);
	}

	VertexArenaAllocator::~VertexArenaAllocator() {
		delete indexBuffer;
		delete vertexPositionBuffer;
		delete vertexUVBuffer;
		delete vertexNormalBuffer;
		if (ProjectSystem::getEngineSettings().memory.enableVertexTangents)
			delete vertexTangentBuffer;
		for (int i = 0; i < bufferDestructors.size(); i++) {
			auto& pair = bufferDestructors[i];
			delete pair.first;
		}
	}

	void VertexArenaAllocator::bind(VkCommandBuffer commandBuffer) {
		vkCmdBindIndexBuffer(commandBuffer, indexBuffer->getBuffer(), 0, VK_INDEX_TYPE_UINT32);

		VkBuffer positionBuffer = vertexPositionBuffer->getBuffer();
		VkBuffer uvBuffer = vertexUVBuffer->getBuffer();
		VkBuffer normalBuffer = vertexNormalBuffer->getBuffer();
		VkBuffer tangentBuffer = nullptr;
		if (ProjectSystem::getEngineSettings().memory.enableVertexTangents) tangentBuffer = vertexTangentBuffer->getBuffer();
		VkBuffer buffers[]{
			positionBuffer,
			uvBuffer,
			normalBuffer,
			tangentBuffer,
		};
		VkDeviceSize offsets[]{
			0,0,0,0
		};
		uint32_t bufferCount;
		if (ProjectSystem::getEngineSettings().memory.enableVertexTangents) {
			bufferCount = 4;
		} else {
			bufferCount = 3;
		}
		vkCmdBindVertexBuffers(commandBuffer, 0, bufferCount, buffers, offsets);
	}

	void VertexArenaAllocator::runGarbageCollector(uint32_t frameIndex, VkFence fence) {
		currentFrameIndex = frameIndex;
		currentFence = fence;
		uint32_t lastOccupiedFrameIndex = (currentFrameIndex + 1) % maxFramesInFlight;
		for (int i = 0; i < bufferDestructors.size(); i++) {
			auto& pair = bufferDestructors[i];
			if (pair.second.getFence() == fence) continue; // from same frame
			if (pair.second.isFree()) {
				vkDeviceWaitIdle(engineDevice.device());
				delete pair.first;
				bufferDestructors.erase(bufferDestructors.begin() + i);
				i--;
			}
		}
	}

	VertexArenaAllocator::OffsetData VertexArenaAllocator::getOffsetData(uint32_t meshID) {
		SHARD3D_ASSERT(offsetData.find(meshID) != offsetData.end() && "MeshID must be a valid ID!");
		return offsetData[meshID];
	}

	void VertexArenaAllocator::resizeDeviceLocalBuffer(VkCommandBuffer commandBuffer, S3DBuffer** buffer_, size_t instanceSize, size_t newInstanceCount, bool isIndexBuffer) {
		VkBufferUsageFlags stagingBufferUsage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
		VkMemoryPropertyFlags stagingMemoryProperty = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

		VkBufferUsageFlags dlBufferUsage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | (isIndexBuffer ? VK_BUFFER_USAGE_INDEX_BUFFER_BIT : VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);
		VkMemoryPropertyFlags dlMemoryProperty = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

		S3DBuffer* oldbuffer = *buffer_;
		
		uint32_t minNewSize = oldbuffer->getInstanceCount() * ProjectSystem::getEngineSettings().memory.arenaAllocatorCapacityGrowMultiplier;
		uint32_t instanceCount = newInstanceCount > minNewSize ? newInstanceCount : minNewSize;
		instanceCount += oldbuffer->getInstanceCount();

		S3DBuffer* resizedStagingBuffer = new S3DBuffer(engineDevice, instanceSize, instanceCount, stagingBufferUsage, stagingMemoryProperty, true);
		size_t previousBufferSize = oldbuffer->getBufferSize();


		S3DSynchronization sync = S3DSynchronization(SynchronizationType::Transfer);
		sync.addBufferMemoryBarrier(VK_ACCESS_NONE, VK_ACCESS_TRANSFER_READ_BIT, oldbuffer);
		sync.addBufferMemoryBarrier(VK_ACCESS_NONE, VK_ACCESS_TRANSFER_WRITE_BIT, resizedStagingBuffer);
		sync.syncBarrier(commandBuffer);
		engineDevice.copyBuffer(commandBuffer, oldbuffer->getBuffer(), resizedStagingBuffer->getBuffer(), previousBufferSize);

		S3DBuffer* newBuffer = new S3DBuffer(engineDevice, instanceSize, instanceCount, dlBufferUsage, dlMemoryProperty, true);
		*buffer_ = newBuffer;

		S3DSynchronization sync2 = S3DSynchronization(SynchronizationType::Transfer);
		sync2.addBufferMemoryBarrier(VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_TRANSFER_READ_BIT, resizedStagingBuffer);
		sync2.addBufferMemoryBarrier(VK_ACCESS_NONE, VK_ACCESS_TRANSFER_WRITE_BIT, newBuffer);
		sync2.syncBarrier(commandBuffer);
		engineDevice.copyBuffer(commandBuffer, resizedStagingBuffer->getBuffer(), newBuffer->getBuffer(), previousBufferSize);

		SHARD3D_LOG("Resizing vertex/index buffer ({0} -> {1} bytes)", previousBufferSize, newBuffer->getBufferSize());

		freeBuffer(oldbuffer);
		freeBuffer(resizedStagingBuffer);
	}

	void VertexArenaAllocator::shrinkDeviceLocalBuffer(VkCommandBuffer commandBuffer, S3DBuffer** buffer_, size_t instanceSize, size_t allocatedInstanceCount, size_t toRemoveInstanceCount, size_t toRemoveInstanceOffset, bool isIndexBuffer) {
		VkBufferUsageFlags stagingBufferUsage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
		VkMemoryPropertyFlags stagingMemoryProperty = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

		VkBufferUsageFlags dlBufferUsage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | (isIndexBuffer ? VK_BUFFER_USAGE_INDEX_BUFFER_BIT : VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);
		VkMemoryPropertyFlags dlMemoryProperty = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

		S3DBuffer* oldbuffer = *buffer_;

		size_t beginCopySize = toRemoveInstanceOffset * instanceSize; 
		size_t endCopyStartInstanceOffset = toRemoveInstanceOffset + toRemoveInstanceCount;
		size_t endCopyStartOffset = endCopyStartInstanceOffset * instanceSize;
		size_t endCopyInstanceCount = std::max(static_cast<int>(allocatedInstanceCount) - static_cast<int>(endCopyStartInstanceOffset), 0);
		size_t endCopySize = endCopyInstanceCount * instanceSize;

		size_t newInstanceCount = oldbuffer->getInstanceCount() * ProjectSystem::getEngineSettings().memory.arenaAllocatorCapacityShrinkThreshold;
		bool shrinkBufferSize = allocatedInstanceCount < newInstanceCount;
		if (shrinkBufferSize) {
			S3DBuffer* resizedStagingBuffer = new S3DBuffer(engineDevice, instanceSize, newInstanceCount, stagingBufferUsage, stagingMemoryProperty, true);
			S3DSynchronization sync = S3DSynchronization(SynchronizationType::Transfer);
			sync.addBufferMemoryBarrier(VK_ACCESS_NONE, VK_ACCESS_TRANSFER_READ_BIT, oldbuffer);
			sync.addBufferMemoryBarrier(VK_ACCESS_NONE, VK_ACCESS_TRANSFER_WRITE_BIT, resizedStagingBuffer);
			sync.syncBarrier(commandBuffer);
			if (beginCopySize > 0) {
				SHARD3D_ASSERT(resizedStagingBuffer->getBufferSize() >= beginCopySize);
				engineDevice.copyBuffer(commandBuffer, oldbuffer->getBuffer(), resizedStagingBuffer->getBuffer(), beginCopySize); // copy buffer before offset allocation
			}
			if (endCopySize > 0) {
				SHARD3D_ASSERT(resizedStagingBuffer->getBufferSize() >= endCopySize && endCopySize <= resizedStagingBuffer->getBufferSize() - toRemoveInstanceOffset * instanceSize);
				engineDevice.copyBuffer(commandBuffer, oldbuffer->getBuffer(), resizedStagingBuffer->getBuffer(), endCopySize, endCopyStartOffset, toRemoveInstanceOffset * instanceSize); // copy buffer after offset allocation
			}
			freeBuffer(oldbuffer);
			freeBuffer(resizedStagingBuffer);
			S3DBuffer* newBuffer = new S3DBuffer(engineDevice, instanceSize, newInstanceCount, dlBufferUsage, dlMemoryProperty, true);
			*buffer_ = newBuffer;

			S3DSynchronization sync2 = S3DSynchronization(SynchronizationType::Transfer);
			sync2.addBufferMemoryBarrier(VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_TRANSFER_READ_BIT, resizedStagingBuffer);
			sync2.addBufferMemoryBarrier(VK_ACCESS_TRANSFER_READ_BIT, VK_ACCESS_TRANSFER_WRITE_BIT, newBuffer);
			sync2.syncBarrier(commandBuffer);
			engineDevice.copyBuffer(commandBuffer, resizedStagingBuffer->getBuffer(), newBuffer->getBuffer(), resizedStagingBuffer->getBufferSize());
		} else {
			if (endCopySize > 0) {
				S3DBuffer* stagingBufferDislocator = new S3DBuffer(engineDevice, instanceSize, endCopyInstanceCount, stagingBufferUsage, stagingMemoryProperty, true);

				SHARD3D_ASSERT(stagingBufferDislocator->getBufferSize() == endCopySize);

				S3DSynchronization sync = S3DSynchronization(SynchronizationType::Transfer);
				sync.addBufferMemoryBarrier(VK_ACCESS_NONE, VK_ACCESS_TRANSFER_READ_BIT, oldbuffer);
				sync.addBufferMemoryBarrier(VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_TRANSFER_READ_BIT, stagingBufferDislocator);
				sync.syncBarrier(commandBuffer);
				engineDevice.copyBuffer(commandBuffer, oldbuffer->getBuffer(), stagingBufferDislocator->getBuffer(), endCopySize, endCopyStartOffset);

				S3DSynchronization sync2 = S3DSynchronization(SynchronizationType::Transfer);
				sync2.addBufferMemoryBarrier(VK_ACCESS_TRANSFER_READ_BIT, VK_ACCESS_TRANSFER_WRITE_BIT, oldbuffer);
				sync2.addBufferMemoryBarrier(VK_ACCESS_TRANSFER_READ_BIT, VK_ACCESS_NONE, stagingBufferDislocator);
				sync2.syncBarrier(commandBuffer);
				engineDevice.copyBuffer(commandBuffer, stagingBufferDislocator->getBuffer(), oldbuffer->getBuffer(), endCopySize, 0, beginCopySize);
				freeBuffer(stagingBufferDislocator);
			}
		}
	}

	void VertexArenaAllocator::writeDeviceLocalBuffer(VkCommandBuffer commandBuffer, S3DBuffer* buffer, const void* data, size_t instanceSize, size_t instanceCount, size_t offset) {
		VkBufferUsageFlags bufferUsage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
		VkMemoryPropertyFlags memoryProperty = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

		S3DBuffer* stagingBuffer = new S3DBuffer(engineDevice, instanceSize, instanceCount, bufferUsage, memoryProperty, true);
		stagingBuffer->map();
		stagingBuffer->writeToBuffer(data);
		SHARD3D_ASSERT(stagingBuffer->getBufferSize() <= buffer->getBufferSize() - offset * instanceSize);
		
		engineDevice.copyBuffer(commandBuffer, stagingBuffer->getBuffer(), buffer->getBuffer(), stagingBuffer->getBufferSize(), 0, offset * instanceSize);
		freeBuffer(stagingBuffer);
	}

	void VertexArenaAllocator::freeBuffer(S3DBuffer* buffer) {
		bufferDestructors.push_back({ buffer, {engineDevice, currentFence} });
	} 
	void VertexArenaAllocator::write(uint32_t id, const VertexDataInfo vertexData, const void* indexData) {
		if (invalid) return;

		size_t indexOffset = static_cast<size_t>(offsetData[id].indexOffset);
		size_t indexCount = static_cast<size_t>(offsetData[id].indexCount);
		size_t vertexOffset = static_cast<size_t>(offsetData[id].vertexOffset);
		size_t vertexCount = static_cast<size_t>(offsetData[id].vertexCount);

		VkCommandBuffer commandBuffer = engineDevice.beginSingleTimeCommands();
		writeDeviceLocalBuffer(commandBuffer, indexBuffer, indexData, sizeof(uint32_t), indexCount, indexOffset);

		writeDeviceLocalBuffer(commandBuffer, vertexPositionBuffer, vertexData.position, sizeof(VTX_pos_offset_12b_), vertexCount, vertexOffset);
		writeDeviceLocalBuffer(commandBuffer, vertexUVBuffer, vertexData.uv, sizeof(VTX_uv_cmpr_offset_4b_), vertexCount, vertexOffset);
		writeDeviceLocalBuffer(commandBuffer, vertexNormalBuffer, vertexData.normal, sizeof(VTX_norm_cmpr_offset_4b_), vertexCount, vertexOffset);
		if (ProjectSystem::getEngineSettings().memory.enableVertexTangents)
			writeDeviceLocalBuffer(commandBuffer, vertexTangentBuffer, vertexData.tangent, sizeof(VTX_tg_cmpr_offset_4b_), vertexCount, vertexOffset);
		engineDevice.endSingleTimeCommands(commandBuffer);

	}
	void VertexArenaAllocator::invalidate() {
		invalid = true;
	}
	uint32_t VertexArenaAllocator::allocate(uint32_t vertexCount, uint32_t indexCount) {
		if (invalid) return 0xffffffff;

		VkCommandBuffer commandBuffer = engineDevice.beginSingleTimeCommands();
		if (static_cast<int>(indexBuffer->getInstanceCount()) - static_cast<int>(indexCount) < static_cast<int>(lastIndexInstance)) {
			int newCount = static_cast<int>(indexBuffer->getInstanceCount()) + static_cast<int>(indexCount);
			resizeDeviceLocalBuffer(commandBuffer, &indexBuffer, sizeof(uint32_t), newCount, true);
		}

		if (static_cast<int>(vertexPositionBuffer->getInstanceCount()) - static_cast<int>(vertexCount) < static_cast<int>(lastVertexInstance)) {
			int newCount = static_cast<int>(vertexPositionBuffer->getInstanceCount()) + static_cast<int>(vertexCount);
			resizeDeviceLocalBuffer(commandBuffer, &vertexPositionBuffer, sizeof(VTX_pos_offset_12b_), newCount);
			resizeDeviceLocalBuffer(commandBuffer, &vertexUVBuffer, sizeof(VTX_uv_cmpr_offset_4b_), newCount);
			resizeDeviceLocalBuffer(commandBuffer, &vertexNormalBuffer, sizeof(VTX_norm_cmpr_offset_4b_), newCount);
			if (ProjectSystem::getEngineSettings().memory.enableVertexTangents)
			resizeDeviceLocalBuffer(commandBuffer, &vertexTangentBuffer, sizeof(VTX_tg_cmpr_offset_4b_), newCount);
		}

		engineDevice.endSingleTimeCommands(commandBuffer);
		uint32_t meshID = 0;
		while (offsetData.find(meshID) != offsetData.end()) {
			meshID++;
		}
		

		// something sketchy as hell here
		OffsetData offsets{};
		offsets.indexCount = indexCount;
		offsets.vertexCount = vertexCount;
		offsets.indexOffset = lastIndexInstance;
		offsets.vertexOffset = lastVertexInstance;
		lastIndexInstance += indexCount;
		lastVertexInstance += vertexCount;

		offsetData[meshID] = offsets;
		return meshID;
	}
	bool VertexArenaAllocator::deallocate(uint32_t id) {
		if (invalid) return false;
		OffsetData data = offsetData[id];
		offsetData.erase(id);

		VkCommandBuffer commandBuffer = engineDevice.beginSingleTimeCommands();
		shrinkDeviceLocalBuffer(commandBuffer , &indexBuffer, sizeof(uint32_t), lastIndexInstance, data.indexCount, data.indexOffset, true);

		shrinkDeviceLocalBuffer(commandBuffer, &vertexPositionBuffer, sizeof(VTX_pos_offset_12b_), lastVertexInstance, data.vertexCount, data.vertexOffset);
		shrinkDeviceLocalBuffer(commandBuffer, &vertexUVBuffer, sizeof(VTX_uv_cmpr_offset_4b_), lastVertexInstance, data.vertexCount, data.vertexOffset);
		shrinkDeviceLocalBuffer(commandBuffer, &vertexNormalBuffer, sizeof(VTX_norm_cmpr_offset_4b_), lastVertexInstance, data.vertexCount, data.vertexOffset);
		if (ProjectSystem::getEngineSettings().memory.enableVertexTangents)
			shrinkDeviceLocalBuffer(commandBuffer , &vertexTangentBuffer, sizeof(VTX_tg_cmpr_offset_4b_), lastVertexInstance, data.vertexCount, data.vertexOffset);

		engineDevice.endSingleTimeCommands(commandBuffer);
		for (auto& [id, offset] : offsetData) {
			if (offset.indexOffset >= data.indexOffset) {
				offsetData[id].indexOffset -= data.indexCount;
			}
			if (offset.vertexOffset >= data.vertexOffset) {
				offsetData[id].vertexOffset -= data.vertexCount;
			}
		}

		lastIndexInstance -= data.indexCount;
		lastVertexInstance -= data.vertexCount; 
		
		return true;
	}
}