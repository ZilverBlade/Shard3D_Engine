#pragma once
/*
#include "../../core.h"
#include "../../core/asset/assetmgr.h"
#include <vector>

namespace Shard3D {
	class RenderManager {
	public:
		using MeshInstanceID = size_t;
		using MeshInstanceTransformInfoID = size_t;
		struct MeshInfo {
			uint32_t meshID{};
			uint32_t submeshIndex{};
			uint32_t instanceCount{};
		};
		struct MeshInstanceInfo {
			uint32_t meshID{};
			std::vector<uint32_t> materialIDs{};
			bool castShadows; // for shadow maps
			uint32_t activeLOD = 0; // unused atm
			MeshInstanceID hash() {
				size_t seed = 0;
				for (uint32_t i : materialIDs)
					hashCombine(seed, i, meshID, castShadows, activeLOD);
				return seed;
			}
		};
		struct MeshInstanceTransformInfo {
			MeshInstanceID instanceID{};
			uint32_t instanceIndex{};
			MeshInstanceTransformInfoID hash() {
				size_t seed = 0xAF893480;
				hashCombine(seed, instanceIndex, instanceID);
				return seed;
			}
		};
		struct MeshTransformBufferInfo {
			S3DBuffer* buffer;
			uint32_t bindlessIndex = BINDLESS_UNALLOCATED_INDEX;
		};

		struct DrawCommandInfo {
			Mesh3D* mesh{};
			MeshInfo meshInfo{};
		};
		struct DrawInfo {
			VkDescriptorSet transformBufferDescriptor{};
			std::vector<std::pair<uint32_t, DrawCommandInfo>> commandInfos{}; // material id and draw command info pair
			bool castShadows; // for shadow maps
		};

		RenderManager(const std::vector<SurfaceMaterialPipelineClassPermutationOptions>& shaders, S3DDevice& device, ResourceSystem* resourceSystem, uint32_t maxFramesInFlight);
		~RenderManager();

		void runGarbageCollector();
		void add(Actor actor);
		void updateTransform(Actor actor);
		void remove(Actor actor);
		void refresh(Actor actor);
		void clear();

		VkDescriptorSetLayout getTransformLookupArrayDescriptorSetLayout() {
			return transformLookupLayout->getDescriptorSetLayout();
		}

	private:
		void addMesh(Mesh3D* mesh, const std::vector<AssetID>& materials, UUID actorID, bool castShadows);
		uint32_t getNewInstance(MeshInstanceInfo instanceCreateInfo);

		void addTransform(MeshInstanceTransformInfo meshInfo, const glm::mat4& transform, const glm::mat3& normal);
		S3DBuffer* createTransformBuffer();
		void writeTransformBuffer(S3DBuffer* buffer, const glm::mat4& transform, const glm::mat3& normal);
		void writeTBBIToDrawCallTransformLookupArrayBuffer(S3DBuffer* transformLookupBuffer, uint32_t instanceIndex, uint32_t bufferIndex);
		S3DBuffer* createDrawCallTransformLookupArrayBuffer(size_t instanceCount);
		S3DBuffer* resizeDrawCallTransformLookupArrayBuffer(S3DBuffer* oldBuffer, size_t newInstanceCount);
		VkDescriptorSet writeTransformLookupDescriptor(S3DBuffer* transformLookupBuffer);
		void destroyTransformLookup(VkDescriptorSet descriptor, S3DBuffer* buffer);

		void destroyTransformBuffer(MeshTransformBufferInfo mtbi);

		uPtr<S3DDescriptorSetLayout> transformLookupLayout{};
		S3DDevice& engineDevice;
		ResourceSystem* resourceSystem;

		const uint32_t maxMeshInstances;
		const uint32_t maxFrameQueue;
		uint32_t currentFrameDestructionIndex;
		std::unordered_map<uint32_t, std::vector<MeshTransformBufferInfo>> destroyTransformMap;

		// actor UUID vs transform instance info
		std::unordered_map<UUID, MeshInstanceTransformInfo> actorInstances{};

		// mesh instance id vs transform id array buffer
		std::unordered_map<MeshInstanceID, S3DBuffer*> transformLookupArrayBuffer{};

		 // mesh instance id vs draw command info
		std::unordered_map<MeshInstanceID, DrawCommandInfo> meshDrawCommandInfo;

		 // mesh-submesh-instance hash vs transform buffer
		std::unordered_map<MeshInstanceTransformInfoID, MeshTransformBufferInfo> transformBuffers{};

		 // unordered map of shader pipeline and unordered map of the mesh instance ID
		std::unordered_map<SurfaceMaterialPipelineClassPermutationOptions, std::unordered_map<MeshInstanceID, DrawInfo>> drawBuffers{};
	};
}
*/