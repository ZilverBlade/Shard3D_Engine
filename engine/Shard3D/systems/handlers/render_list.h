#pragma once
#define GLM_FORCE_QUAT_DATA_WXYZ
#include <tuple>
#include "../../core.h"
#include "../../core/asset/assetmgr.h"
#include "../../core/ecs/components/core_components.h"

namespace Shard3D {
	inline namespace ECS {
		class Actor;
		class Level;
	}
	class ResourceSystem;
	enum MeshMobilityOptions {
		MeshMobilityOption_Static = 1,
		MeshMobilityOption_Stationary = 2,
		MeshMobilityOption_Dynamic = 4,
		MeshMobilityOption_All = 7
	};
	
	struct SkeletonRenderInfo {
		std::vector<S3DBuffer*> actorBufferFrames;
		std::vector<VkDescriptorSet> actorBufferDescriptors;
		AssetID skeleton{ AssetID::null() };
	};
	struct Mesh3DCullInfo {
		VolumeBounds transformedAABB;
		std::vector<VolumeBounds> transformedMaterialAABBs;
		std::vector<bool> materialVisibilities;
		std::vector<glm::ivec3> octreeTiles;
		SphereBounds sphereBounds;
		float maxScale;
		glm::vec4 worldPosition;
	};
	struct Mesh3DRenderInfo {
		glm::mat4 transform;
		glm::mat4 dynamicActorPrevFrameTransform;
		glm::mat3 normal;
		bool castShadows;
		bool receiveDecals;
		AssetID mesh{ AssetID::null() };
		std::vector<AssetID> materials;
		Mobility mobility;
		Mesh3DCullInfo cullInfo;

		std::vector<S3DBuffer*> actorBufferFrames;
		std::vector<ShaderResourceIndex> actorBufferIndices;
	};
	struct SurfaceMaterialRenderInfo {
		Mesh3DRenderInfo* mesh;
		std::vector<uint32_t> material_indices;
		bool pipelinesCompatible; // TODO, if all materials use the same pipeline, allow for optimisations
	};
	typedef uint32_t MeshMobilityOptionsFlags;
	struct SurfaceMaterialMeshIteratorObjectEntity {
		MeshMobilityOptionsFlags meshMoblity;
		bool rigged;
		SurfaceMaterialPipelineClassPermutationFlags materialClass;
		std::unordered_map<entt::entity, SurfaceMaterialRenderInfo>& renderList;
		SurfaceMaterialMeshIteratorObjectEntity(std::unordered_map<entt::entity, SurfaceMaterialRenderInfo>& v) : renderList(v) {}
	};
	class SurfaceMaterialMeshIterator {
	public:
		SurfaceMaterialMeshIterator(
			std::unordered_map<bool, std::unordered_map<MeshMobilityOptionsFlags, std::unordered_map<SurfaceMaterialPipelineClassPermutationFlags, std::unordered_map<entt::entity, SurfaceMaterialRenderInfo>>>>& m,
			std::vector<std::tuple<bool, MeshMobilityOptionsFlags, SurfaceMaterialPipelineClassPermutationFlags>>& f,
			uint32_t iterIndex = 0
		) : mapRef(m), flagsComboRef(f), iteratorIndex(iterIndex) {}
		inline SurfaceMaterialMeshIteratorObjectEntity operator*() const {
			bool rigged = std::get<0>(flagsComboRef[iteratorIndex]);
			MeshMobilityOptionsFlags mobf = std::get<1>(flagsComboRef[iteratorIndex]);
			SurfaceMaterialPipelineClassPermutationFlags smtf = std::get<2>(flagsComboRef[iteratorIndex]);
			SurfaceMaterialMeshIteratorObjectEntity e = SurfaceMaterialMeshIteratorObjectEntity(mapRef[rigged][mobf][smtf]);
			e.materialClass = smtf;
			e.meshMoblity = mobf;
			e.rigged = rigged;
			return e;
		}

		// Prefix increment
		inline SurfaceMaterialMeshIterator& operator++() { iteratorIndex++; return *this; }
		// Postfix increment
		inline SurfaceMaterialMeshIterator operator++(int) { SurfaceMaterialMeshIterator tmp = *this; ++(*this); return tmp; }
		inline friend bool operator== (const SurfaceMaterialMeshIterator& a, const SurfaceMaterialMeshIterator& b) { return a.iteratorIndex == b.iteratorIndex; };
		inline friend bool operator!= (const SurfaceMaterialMeshIterator& a, const SurfaceMaterialMeshIterator& b) { return a.iteratorIndex != b.iteratorIndex; };
	private:
		std::unordered_map<bool, std::unordered_map<MeshMobilityOptionsFlags, std::unordered_map<SurfaceMaterialPipelineClassPermutationFlags, std::unordered_map<entt::entity, SurfaceMaterialRenderInfo>>>>& mapRef;
		std::vector<std::tuple<bool, MeshMobilityOptionsFlags, SurfaceMaterialPipelineClassPermutationFlags>> flagsComboRef;
		uint32_t iteratorIndex;
	};	
	typedef struct SurfaceMaterialRenderIDFlagBits {
		SurfaceMaterialRenderIDFlagBits(std::unordered_map<bool, std::unordered_map<MeshMobilityOptionsFlags, std::unordered_map<SurfaceMaterialPipelineClassPermutationFlags, std::unordered_map<entt::entity, SurfaceMaterialRenderInfo>>>>& m) : mapRef(m) {}
		inline SurfaceMaterialMeshIterator begin() { return SurfaceMaterialMeshIterator(mapRef, flagsCombo, 0); }
		inline SurfaceMaterialMeshIterator end() { return SurfaceMaterialMeshIterator(mapRef, flagsCombo, flagsCombo.size()); }
	private:
		std::unordered_map<bool, std::unordered_map<MeshMobilityOptionsFlags, std::unordered_map<SurfaceMaterialPipelineClassPermutationFlags, std::unordered_map<entt::entity, SurfaceMaterialRenderInfo>>>>& mapRef;
		std::vector<std::tuple<bool, MeshMobilityOptionsFlags, SurfaceMaterialPipelineClassPermutationFlags>> flagsCombo;
		friend class RenderList;
	};

	class RenderList {
	public:
		RenderList(const std::vector<SurfaceMaterialPipelineClassPermutationFlags>& shaders, uPtr<S3DDescriptorSetLayout>& skeletonDescriptorSetLayout, ResourceSystem* resourceSystem);
		~RenderList();
		DELETE_COPY(RenderList);

		void updateRenderList(sPtr<Level>& level);

		void addActorToRenderList(Actor actor);
		void rmvActorFromRenderList(Actor actor);
		void cleanUpActorFromRenderList(Actor actor);
		void refreshActor(Actor actor);
		void updateActorTransform(Actor actor, glm::mat4& transformMatrix, glm::mat3& normalMatrix);
		void updateActorCastsShadows(Actor actor, bool castShadows);
		void updateActorReceivesDecals(Actor actor, bool receiveDecals);
		void updateActorAABBs(Mesh3DRenderInfo& renderData);
		void updateActor(Actor actor);
		SurfaceMaterialRenderInfo buildRenderInfoFromActor(Actor actor, SurfaceMaterialPipelineClassPermutationFlags flags);
		std::vector<SurfaceMaterialPipelineClassPermutationFlags> getRenderUsingClasses(Actor actor, bool rigged, MeshMobilityOptionsFlags mobility);
		SurfaceMaterialRenderIDFlagBits getIterator(std::vector<SurfaceMaterialPipelineClassPermutationFlags> flags, MeshMobilityOptionsFlags mobility);

		void clear();
		
		VkDescriptorSet getSkeletonDescriptor(entt::entity actor, uint32_t frameIndex);

		void updateDynamicTransformBufferData(uint32_t frameIndex);
		void updateDynamicRiggingBufferData(sPtr<Level>& level, uint32_t frameIndex);
		void runGarbageCollector(uint32_t frameIndex, VkFence fence);

		void makeActorInvisible(Actor actor);
		void makeActorVisible(Actor actor);
		void modifyActorMobility(Actor actor, uint32_t oldMobility, uint32_t newMobility);
		void modifyMaterialType(AssetID material, sPtr<Level>& level, SurfaceMaterialPipelineClassPermutationFlags old_flags, SurfaceMaterialPipelineClassPermutationFlags new_flags);

		void addToSurfaceMaterialRenderingList(Actor actor, bool rigged, MeshMobilityOptionsFlags mobility, SurfaceMaterialPipelineClassPermutationFlags flags, const SurfaceMaterialRenderInfo& renderInfo);
		void rmvFromSurfaceMaterialRenderingList(Actor actor, bool rigged, MeshMobilityOptionsFlags mobility, SurfaceMaterialPipelineClassPermutationFlags flags);
		void switchSurfaceMaterialRenderingList(Actor actor, bool rigged, MeshMobilityOptionsFlags mobility, SurfaceMaterialPipelineClassPermutationFlags old_flags, SurfaceMaterialPipelineClassPermutationFlags new_flags, const SurfaceMaterialRenderInfo& new_renderInfo);
		auto& getSurfaceMaterialRenderingList() { return materialRendering; }
		auto& getMeshInfos() {
			return meshInfos;
		}
		
		void swapAsset(AssetType type, AssetID oldAsset, AssetID newAsset);


	private:
		void createActorBufferData(Actor actor);
		void createActorBufferSkeleton(Actor actor);

		friend class MaterialSystem;
		// rigged? mobility? surface material shader? actor -> render info
		std::unordered_map<bool, std::unordered_map<MeshMobilityOptionsFlags, std::unordered_map<SurfaceMaterialPipelineClassPermutationFlags, std::unordered_map<entt::entity, SurfaceMaterialRenderInfo>>>> materialRendering;
		std::unordered_map<entt::entity, Mesh3DRenderInfo> meshInfos;
		std::unordered_map<entt::entity, SkeletonRenderInfo> skeletalInfos;
		std::vector<SurfaceMaterialPipelineClassPermutationFlags> allowedShaders;
		ResourceSystem* resourceSystem;
		uPtr<S3DDescriptorSetLayout>& skeletonDescriptorSetLayout;

		VkFence currentFence = nullptr;
		std::vector<std::pair<std::pair<S3DBuffer*, ShaderResourceIndex>, S3DDestructionObject>> destroyBufferQueue;
	};
}
