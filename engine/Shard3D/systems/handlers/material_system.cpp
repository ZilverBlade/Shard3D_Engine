#include "../../s3dpch.h" 

#include "material_system.h"
#include "render_list.h"
#include "../../core/asset/assetmgr.h"
#include <fstream>
#include "../computational/shader_system.h"
#include "../rendering/deferred_render_system.h"
#include "../../core/misc/engine_settings.h"
#include <filesystem>
#include "../../core/vulkan_api/bindless.h"

#include "../../core/rendering/render_pass.h"
#include "project_system.h"

namespace Shard3D {
	MaterialSystem::MaterialSystem(Systems::DeferredRenderSystem* renderer, ResourceSystem* resourceSystem, VkDescriptorSetLayout globalSetLayout, VkDescriptorSetLayout sceneSSBOLayout, VkDescriptorSetLayout riggedSkeletonLayout) :
		mDevice(renderer->engineDevice), 
		deferredRenderingSystem(renderer),
		mGlobalSetLayout(globalSetLayout),
		mSceneSetLayout(sceneSSBOLayout),
		mSkeletonSetLayout(riggedSkeletonLayout),
		useReverseDepth(renderer->useReverseDepth),
		resourceSystem(resourceSystem)
	{
	}
	
	MaterialSystem::~MaterialSystem() {
		for (auto& [_, item] : materialClassesGlobal) {
			delete item;
		}
		materialClassesForward.clear();
		materialClassesDeferred.clear();
		materialClassesGlobal.clear();
		materialClassesOptionsGlobal.clear();
		materialClassesOptionsDeferred.clear();
		materialClassesOptionsForward.clear();
	}

	void MaterialSystem::setAllAvailableMaterialShaderPermutations(const std::vector<SurfaceMaterialPipelineClassPermutationFlags>& shaders) {
		for (auto& [_, item] : materialClassesGlobal) {
			delete item;
		}
		materialClassesForward.clear();
		materialClassesDeferred.clear();
		materialClassesGlobal.clear();
		materialClassesOptionsGlobal.clear();
		materialClassesOptionsDeferred.clear();
		materialClassesOptionsForward.clear();

		for (uint32_t permutation : shaders) {
			SurfaceMaterialClass* xClass;
			bool registerClass = true;
			if (permutation & SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates || permutation & SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms) {
				registerClass = false;
			}
			if (permutation & SurfaceMaterialPipelineClassPermutationOptions_Translucent) {
				xClass = new SurfaceMaterialClassForward(this, permutation);
				materialClassesForward[permutation] = reinterpret_cast<SurfaceMaterialClassForward*>(xClass);
				if (registerClass)
					materialClassesOptionsForward.push_back(permutation);
			}
			else {
				xClass = new SurfaceMaterialClassDeferred(this, permutation);
				materialClassesDeferred[permutation] = reinterpret_cast<SurfaceMaterialClassDeferred*>(xClass);
				if (registerClass)
					materialClassesOptionsDeferred.push_back(permutation);
			}
			if (registerClass)
				materialClassesOptionsGlobal.push_back(permutation);
			materialClassesGlobal[permutation] = xClass;
			if (!ProjectSystem::getEngineSettings().renderer.enableDoubleSidedMaterials) continue; // skip making the noCulling permutation		
		}
	}
	void MaterialSystem::recompileSurface(const std::vector<SurfaceMaterialPipelineClassPermutationFlags>& shaders) {
		IOUtils::clearDirectory(ProjectSystem::getAssetLocation() + "shaderdata/materials");
		IOUtils::clearDirectory(ProjectSystem::getAssetLocation() + "shaderdata/rendering");
		materialClassesGlobal.clear();
		materialClassesDeferred.clear();
		materialClassesForward.clear();
		for (auto& [key, class_] : MaterialSystem::materialClassesGlobal) {
			delete class_;
		}
		for (uint32_t permutation : shaders) {
			SurfaceMaterialClass* xClass;

			if (permutation & SurfaceMaterialPipelineClassPermutationOptions_Translucent) {
				xClass = new SurfaceMaterialClassForward(this, permutation);
				materialClassesForward[permutation] = reinterpret_cast<SurfaceMaterialClassForward*>(xClass);
			}
			else {
				xClass = new SurfaceMaterialClassDeferred(this, permutation);
				materialClassesDeferred[permutation] = reinterpret_cast<SurfaceMaterialClassDeferred*>(xClass);
			}
			materialClassesGlobal[permutation] = xClass;
		}

		for (auto& [key, material] : resourceSystem->getMaterialAssets()) {
			resourceSystem->rebuildMaterial(material);
		}
	}
	
	void MaterialSystem::createSurfacePipelineLayout(VkPipelineLayout* pipelineLayout, std::vector<VkDescriptorSetLayout> layouts, VkPushConstantRange* pushConstantRange, bool noSSBO) {
		SHARD3D_ASSERT(pipelineLayout != nullptr && "No pipeline layout provided!");
		std::vector<VkDescriptorSetLayout> descriptorSetLayouts{};

		descriptorSetLayouts.reserve(layouts.size() + 2);
		descriptorSetLayouts.push_back(mGlobalSetLayout);
		if (!noSSBO) descriptorSetLayouts.push_back(mSceneSetLayout);
		for (int i = 0; i < layouts.size(); i++) descriptorSetLayouts.push_back(layouts[i]);

		VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
		pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		pipelineLayoutInfo.setLayoutCount = static_cast<uint32_t>(descriptorSetLayouts.size());
		pipelineLayoutInfo.pSetLayouts = descriptorSetLayouts.data();
		pipelineLayoutInfo.pushConstantRangeCount = pushConstantRange? 1 : 0;
		pipelineLayoutInfo.pPushConstantRanges = pushConstantRange;
		VK_VALIDATE(vkCreatePipelineLayout(mDevice.device(), &pipelineLayoutInfo, nullptr, pipelineLayout), "failed to create pipeline layout!");
	}
	void MaterialSystem::createSurfacePipeline(uPtr<S3DGraphicsPipeline>* pipeline, VkPipelineLayout pipelineLayout, 
		S3DGraphicsPipelineConfigInfo& pipelineConfig, DeferredRenderStage stage, const std::string& fragment_shader, const std::string& vertex_shader) {
		SHARD3D_ASSERT(pipelineLayout != nullptr && "Cannot create pipeline before pipeline layout");

		switch (stage) {
		case (DeferredRenderStage::GBufferGen):
			pipelineConfig.renderPass = deferredRenderingSystem->getDeferredRenderPass()->getRenderPass(); break;
		case (DeferredRenderStage::Forward):
			pipelineConfig.renderPass = deferredRenderingSystem->getTransparencyRenderPass()->getRenderPass(); break;
		}

		pipelineConfig.pipelineLayout = pipelineLayout;

		S3DShaderSpecialization specialization{};
		S3DShader fragmentShader = { ShaderType::Fragment, fragment_shader, "materials" };
		
		*pipeline =  make_uPtr<S3DGraphicsPipeline>(
			mDevice, std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, vertex_shader, "rendering" },
				fragmentShader
			},
			pipelineConfig
		);
	}


	void MaterialSystem::createSurfacePipeline(uPtr<S3DGraphicsPipeline>* pipeline, VkPipelineLayout pipelineLayout, S3DGraphicsPipelineConfigInfo& pipelineConfig, SurfaceMaterialClass* self, DeferredRenderStage stage) {
		SHARD3D_ASSERT(pipelineLayout != nullptr && "Cannot create pipeline before pipeline layout");
		switch (stage) {
		case (DeferredRenderStage::EarlyZ):
			pipelineConfig.renderPass = deferredRenderingSystem->getEarlyZRenderPass()->getRenderPass(); break;
		case (DeferredRenderStage::GBufferGen):
			pipelineConfig.renderPass = deferredRenderingSystem->getDeferredRenderPass()->getRenderPass(); break;
		case (DeferredRenderStage::Forward):
			pipelineConfig.renderPass = deferredRenderingSystem->getTransparencyRenderPass()->getRenderPass(); break;
		}
		pipelineConfig.pipelineLayout = pipelineLayout;

		bool alphaTestEnable = false;
		std::vector<std::string> defines{};
		std::stringstream cacheDirVert;
		std::stringstream cacheDirFrag;
		std::string finalFileVert;
		std::string finalFileFrag;
		cacheDirFrag << "surface";
		cacheDirVert << "mesh";
		switch (stage) {
		case (DeferredRenderStage::EarlyZ):
			cacheDirFrag << "_earlyz";
			cacheDirVert << "_earlyz";
			break;
		case (DeferredRenderStage::GBufferGen):
			cacheDirFrag << "_deferred"; 
			break;
		case (DeferredRenderStage::Forward):
			cacheDirFrag << "_forward";
			cacheDirVert << "_forward";
			break;
		}

		if (self->getClass() & SurfaceMaterialPipelineClassPermutationOptions_Translucent) {
			cacheDirFrag << "_translucent";
		} else cacheDirFrag << "_opaque";

		if (self->getClass() & SurfaceMaterialPipelineClassPermutationOptions_AlphaTest && 
			!(deferredRenderingSystem->rendererFeatures.enableEarlyZ && stage == DeferredRenderStage::GBufferGen)) {
			defines.push_back("ENABLE_ALPHA_TEST");
			cacheDirFrag << "_alphatest";
			cacheDirVert << "_alphatest";
			alphaTestEnable = true;
		}
		if (self->getClass() & SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates) {
			defines.push_back("ACTOR_DYNAMIC");
			cacheDirFrag << "_dynamic";
			cacheDirVert << "_dynamic";
		}
		if (self->getClass() & SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms) {
			defines.push_back("RIGGED_MESH");
			cacheDirVert << "_rigged";
		}

		S3DShaderSpecialization specialization{};
		specialization.addConstant(0, static_cast<VkBool32>(deferredRenderingSystem->rendererFeatures.enableClipPlane));

		cacheDirVert << ".vert";
		cacheDirFrag << ".frag";

		finalFileFrag = cacheDirFrag.str();
		finalFileVert = cacheDirVert.str();
		std::string file;
		switch (stage) {
		case (DeferredRenderStage::EarlyZ):
			defines.push_back("S3DSDEF_SHADER_MATERIAL_SURFACE_EARLY_Z");
			file = "resources/shaders/materials/surface_material_earlyz_uber.frag";
			break;
		case (DeferredRenderStage::GBufferGen):
			defines.push_back("S3DSDEF_SHADER_MATERIAL_SURFACE_DEFERRED");
			file = "resources/shaders/materials/surface_material_deferred_uber.frag"; 
			break;
		case (DeferredRenderStage::Forward):
			defines.push_back("S3DSDEF_SHADER_MATERIAL_SURFACE_FORWARD");
			file = "resources/shaders/materials/surface_material_forward_uber.frag"; 
			break;
		}
		std::vector<S3DShader> shaders =
			{ S3DShader{ ShaderType::Vertex, "resources/shaders/rendering/mesh.vert", "rendering", specialization, defines, finalFileVert } };
		if (alphaTestEnable || stage != DeferredRenderStage::EarlyZ) {
			shaders.push_back(S3DShader{ ShaderType::Fragment, file, "materials", {}, defines, finalFileFrag });
		}
		*pipeline = make_uPtr<S3DGraphicsPipeline>(
			mDevice, shaders,
			pipelineConfig
		);

		
	}

	void MaterialSystem::bindMaterialClassDeferred(SurfaceMaterialPipelineClassPermutationFlags flags, VkCommandBuffer commandBuffer) {
		materialClassesDeferred[flags]->bindDeferred(commandBuffer);
	}
	void MaterialSystem::bindMaterialClassForward(SurfaceMaterialPipelineClassPermutationFlags flags, VkCommandBuffer commandBuffer) {
		materialClassesForward[flags]->bindForward(commandBuffer);
	}
	SurfaceMaterialClass* MaterialSystem::getMaterialClass(SurfaceMaterialPipelineClassPermutationFlags flags) {
		SHARD3D_ASSERT(materialClassesGlobal.find(flags) != materialClassesGlobal.end() && "Trying to access a material class (combination) that is either unsupported or does not exist!");
		return materialClassesGlobal[flags];
	}

	void MaterialSystem::bindMaterialClassEarlyDepth(SurfaceMaterialPipelineClassPermutationFlags flags, VkCommandBuffer commandBuffer) {
		materialClassesDeferred[flags]->bindEarlyDepth(commandBuffer);
	}

	void SurfaceMaterialClassForward::bindForward(VkCommandBuffer commandBuffer) {
		SHARD3D_ASSERT(options_flags & SurfaceMaterialPipelineClassPermutationOptions_Translucent && "Cannot bind a forward shader to a non transparent class with in deferred rendering!");
		SHARD3D_ASSERT(this->materialPipelineConfig.shaderPipelineLayout && "Cannot bind material if shader pipeline was never created!");

		materialPipelineConfig.shaderPipeline->bind(commandBuffer);

		if (engineDevice.supportedExtensions.khr_fragment_shading_rate) {
			VkExtent2D fragmentSize = { 2, 2 };
			VkFragmentShadingRateCombinerOpKHR combineOps[2]{ VK_FRAGMENT_SHADING_RATE_COMBINER_OP_KEEP_KHR , VK_FRAGMENT_SHADING_RATE_COMBINER_OP_KEEP_KHR };
			vkCmdSetFragmentShadingRateKHR(commandBuffer, &fragmentSize, combineOps);
		}
	}
	void SurfaceMaterialClassDeferred::bindDeferred(VkCommandBuffer commandBuffer) {
		SHARD3D_ASSERT(!(options_flags & SurfaceMaterialPipelineClassPermutationOptions_Translucent) && "Cannot bind a transparent material to a deferred rendering pass!");
		SHARD3D_ASSERT(this->materialPipelineConfig.shaderPipelineLayout && "Cannot bind material if shader pipeline was never created!");
		
		materialPipelineConfig.shaderPipeline->bind(commandBuffer);
	}
	void SurfaceMaterialClassDeferred::bindEarlyDepth(VkCommandBuffer commandBuffer) {
		SHARD3D_ASSERT(!(options_flags & SurfaceMaterialPipelineClassPermutationOptions_Translucent) && "Cannot bind a transparent material to a deferred rendering pass!");
		SHARD3D_ASSERT(this->materialPipelineConfig.shaderPipelineLayout && "Cannot bind material if shader pipeline was never created!");

		earlyZPipelineConfig.shaderPipeline->bind(commandBuffer);
	}
	SurfaceMaterialClass::SurfaceMaterialClass(S3DDevice& device, SurfaceMaterialPipelineClassPermutationFlags flags) : engineDevice(device), options_flags(flags) {}
	SurfaceMaterialClass::~SurfaceMaterialClass() {
		vkDestroyPipelineLayout(engineDevice.device(), materialPipelineConfig.shaderPipelineLayout, nullptr);
	}
//
// FORWARD
//
	SurfaceMaterialClassForward::SurfaceMaterialClassForward(MaterialSystem* system, SurfaceMaterialPipelineClassPermutationFlags flags) : SurfaceMaterialClass(system->mDevice, flags) {
		bool enableVBufferWrite = flags & SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates;
		bool enableRiggedTransforms = flags & SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms;

		VkPushConstantRange pushConstantRange{};
		pushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		pushConstantRange.offset = 0;
		pushConstantRange.size = enableVBufferWrite ? 8 : 116; // sizeof(uint32) * 2 or sizeof(MeshPushConstantData)

		std::vector<VkDescriptorSetLayout> descriptorSetLayouts{ system->resourceSystem->getDescriptor()->getLayout(), system->deferredRenderingSystem->getDeferredLitInputLayout()->getDescriptorSetLayout() };
		if (enableRiggedTransforms) {
			descriptorSetLayouts.push_back(system->mSkeletonSetLayout);
		}
		materialPipelineConfig = _MaterialS3DGraphicsPipelineConfigInfo();
		system->createSurfacePipelineLayout(
			&materialPipelineConfig.shaderPipelineLayout,
			descriptorSetLayouts,
			&pushConstantRange
		);
		S3DGraphicsPipelineConfigInfo pipelineConfigInfo = S3DGraphicsPipelineConfigInfo(2);
		pipelineConfigInfo.enableVertexDescriptions(VertexDescriptionOptions_Position | VertexDescriptionOptions_UV | VertexDescriptionOptions_Normal | VertexDescriptionOptions_Tangent);
		pipelineConfigInfo.enableWeightedBlending();
		pipelineConfigInfo.disableDepthWrite();

		if (options_flags &~ SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling)
			pipelineConfigInfo.setCullMode(VK_CULL_MODE_NONE);
		if (options_flags & SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling)
			pipelineConfigInfo.setCullMode(VK_CULL_MODE_BACK_BIT);

		if (system->useReverseDepth) {
			pipelineConfigInfo.reverseDepth();
		}
		if (engineDevice.supportedExtensions.khr_fragment_shading_rate)
			pipelineConfigInfo.dynamicStateEnables.push_back(VK_DYNAMIC_STATE_FRAGMENT_SHADING_RATE_KHR);
		if (system->deferredRenderingSystem->getRendererFeatures().enableStencilBuffer) {
			pipelineConfigInfo.depthStencilInfo.stencilTestEnable = false;
			pipelineConfigInfo.depthStencilInfo.front.compareOp = VK_COMPARE_OP_ALWAYS;
			pipelineConfigInfo.depthStencilInfo.front.depthFailOp = VK_STENCIL_OP_KEEP;
			pipelineConfigInfo.depthStencilInfo.front.passOp = VK_STENCIL_OP_REPLACE;
			pipelineConfigInfo.depthStencilInfo.front.failOp = VK_STENCIL_OP_REPLACE;
			pipelineConfigInfo.depthStencilInfo.front.compareMask = 0xFF;
			pipelineConfigInfo.depthStencilInfo.front.writeMask = 0x02;
			pipelineConfigInfo.depthStencilInfo.front.reference = 0x02; // translucent write bits
			pipelineConfigInfo.depthStencilInfo.back = pipelineConfigInfo.depthStencilInfo.front;
		}
		if (system->deferredRenderingSystem->getRendererFeatures().enableMSAA) {
			pipelineConfigInfo.setSampleCount(system->deferredRenderingSystem->getRendererFeatures().msaaSamples);
		}
		pipelineConfigInfo.enablePipelineDerivatives = true;
		pipelineConfigInfo.basePipeline = system->forwardBasePipeline;
		system->createSurfacePipeline(
			&materialPipelineConfig.shaderPipeline,
			materialPipelineConfig.shaderPipelineLayout,
			pipelineConfigInfo,
			this, DeferredRenderStage::Forward);
		if (system->forwardBasePipeline == VK_NULL_HANDLE) {
			system->forwardBasePipeline = materialPipelineConfig.shaderPipeline->getPipeline();
		}
	}

	SurfaceMaterialClassForward::~SurfaceMaterialClassForward() {
	
	}

//
// DEFERRED
//
	SurfaceMaterialClassDeferred::SurfaceMaterialClassDeferred(MaterialSystem* system, SurfaceMaterialPipelineClassPermutationFlags flags) : SurfaceMaterialClass(system->mDevice, flags) {
		bool enableDynamicWrite = flags & SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates;
		bool enableRiggedTransforms = flags & SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms;

		VkPushConstantRange pushConstantRange{};
		pushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		pushConstantRange.offset = 0;
		pushConstantRange.size = enableDynamicWrite ? 12 : 120; // sizeof(uint32) * 3 or sizeof(MeshPushConstantData)

		std::vector<VkDescriptorSetLayout> descriptorSetLayouts{ system->resourceSystem->getDescriptor()->getLayout() };
		if (enableRiggedTransforms) {
			descriptorSetLayouts.push_back(system->mSkeletonSetLayout);
		}

		materialPipelineConfig = _MaterialS3DGraphicsPipelineConfigInfo();
		system->createSurfacePipelineLayout(
			&materialPipelineConfig.shaderPipelineLayout,
			descriptorSetLayouts,
			&pushConstantRange
		);
		S3DGraphicsPipelineConfigInfo pipelineConfigInfo = S3DGraphicsPipelineConfigInfo(system->deferredRenderingSystem->getRendererFeatures().enableVelocityBuffer ? 6 : 5); // 5 gbuffers (or 6 if include velocity buffer)
		pipelineConfigInfo.enableVertexDescriptions(VertexDescriptionOptions_Position | VertexDescriptionOptions_UV | VertexDescriptionOptions_Normal | VertexDescriptionOptions_Tangent);
		if (options_flags & ~SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling)
			pipelineConfigInfo.setCullMode(VK_CULL_MODE_NONE);
		if (options_flags & SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling)
			pipelineConfigInfo.setCullMode(VK_CULL_MODE_BACK_BIT);
		if (system->deferredRenderingSystem->getRendererFeatures().enableEarlyZ) {
			pipelineConfigInfo.disableDepthWrite();
		}
		if (system->useReverseDepth) {
			pipelineConfigInfo.reverseDepth();
		}
		if (system->deferredRenderingSystem->getRendererFeatures().enableEarlyZ == true && ProjectSystem::getEngineSettings().renderer.earlyZMode == ESET::EarlyDepthMode::OpaqueAndDiscardMasks) {
			pipelineConfigInfo.depthStencilInfo.depthCompareOp = VK_COMPARE_OP_EQUAL;
		}
		if (system->deferredRenderingSystem->getRendererFeatures().enableStencilBuffer) {
			pipelineConfigInfo.depthStencilInfo.stencilTestEnable = true;
			pipelineConfigInfo.depthStencilInfo.front.compareOp = VK_COMPARE_OP_ALWAYS;
			pipelineConfigInfo.depthStencilInfo.front.depthFailOp = VK_STENCIL_OP_KEEP;
			pipelineConfigInfo.depthStencilInfo.front.passOp = VK_STENCIL_OP_REPLACE;
			pipelineConfigInfo.depthStencilInfo.front.failOp = VK_STENCIL_OP_REPLACE;
			pipelineConfigInfo.depthStencilInfo.front.compareMask = 0xFF;
			pipelineConfigInfo.depthStencilInfo.front.writeMask = 0x04;
			if (enableDynamicWrite) {
				pipelineConfigInfo.depthStencilInfo.front.reference = 0x04;// velocity write bits
			}
			pipelineConfigInfo.depthStencilInfo.back = pipelineConfigInfo.depthStencilInfo.front;
		}

		if (!enableDynamicWrite && system->deferredRenderingSystem->getRendererFeatures().enableVelocityBuffer) {
			pipelineConfigInfo.colorBlendAttachments[5].colorWriteMask = 0;
		}
		if (system->deferredRenderingSystem->getRendererFeatures().enableMSAA) {
			pipelineConfigInfo.setSampleCount(system->deferredRenderingSystem->getRendererFeatures().msaaSamples);
		}
		pipelineConfigInfo.enablePipelineDerivatives = true;
		pipelineConfigInfo.basePipeline = system->deferredBasePipeline;

		system->createSurfacePipeline(
			&materialPipelineConfig.shaderPipeline,
			materialPipelineConfig.shaderPipelineLayout,
			pipelineConfigInfo,
			this, DeferredRenderStage::GBufferGen);

		if (system->deferredBasePipeline == VK_NULL_HANDLE) {
			system->deferredBasePipeline = materialPipelineConfig.shaderPipeline->getPipeline();
		}

		if (system->deferredRenderingSystem->getRendererFeatures().enableEarlyZ) {
			VkPushConstantRange ezpushConstantRange{};
			ezpushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
			ezpushConstantRange.offset = 0;
			ezpushConstantRange.size = enableDynamicWrite ? 8 : 68; // sizeof(uint32) * 2 or sizeof(MeshPushConstantData)

			system->createSurfacePipelineLayout(
				&earlyZPipelineConfig.shaderPipelineLayout,
				descriptorSetLayouts,
				& ezpushConstantRange,
				true
			);
			S3DGraphicsPipelineConfigInfo earlyZpipelineConfigInfo = S3DGraphicsPipelineConfigInfo(0);
			earlyZpipelineConfigInfo.enableVertexDescriptions(VertexDescriptionOptions_Position);
			if (options_flags & ~SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling)
				earlyZpipelineConfigInfo.setCullMode(VK_CULL_MODE_NONE);
			if (options_flags & SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling)
				earlyZpipelineConfigInfo.setCullMode(VK_CULL_MODE_BACK_BIT);
			if (options_flags & SurfaceMaterialPipelineClassPermutationOptions_AlphaTest){
				earlyZpipelineConfigInfo.enableVertexDescriptions(VertexDescriptionOptions_UV);
			}
			if (system->useReverseDepth) {
				earlyZpipelineConfigInfo.reverseDepth();
			}
			if (system->deferredRenderingSystem->getRendererFeatures().enableMSAA) {
				earlyZpipelineConfigInfo.setSampleCount(system->deferredRenderingSystem->getRendererFeatures().msaaSamples);
			}
			earlyZpipelineConfigInfo.enablePipelineDerivatives = true;
			earlyZpipelineConfigInfo.basePipeline = system->earlyZBasePipeline;
			system->createSurfacePipeline(
				&earlyZPipelineConfig.shaderPipeline,
				earlyZPipelineConfig.shaderPipelineLayout,
				earlyZpipelineConfigInfo,
				this, DeferredRenderStage::EarlyZ);
			if (system->earlyZBasePipeline == VK_NULL_HANDLE) {
				system->earlyZBasePipeline = earlyZPipelineConfig.shaderPipeline->getPipeline();
			}
		}
	}

	SurfaceMaterialClassDeferred::~SurfaceMaterialClassDeferred() {
		vkDestroyPipelineLayout(engineDevice.device(), earlyZPipelineConfig.shaderPipelineLayout, nullptr);
	}

}
