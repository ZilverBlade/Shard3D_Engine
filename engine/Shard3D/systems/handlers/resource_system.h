#pragma once
#include "../../s3dstd.h"
#include "../../core/asset/importer_model.h"
#include "../../core/asset/importer_texture.h"
#include "../../core/asset/mesh3d.h"
#include "../../core/asset/skeleton.h"
#include "../../core/asset/material.h"
#include "../../core/asset/assetid.h"
#include "../../core/asset/texture2d.h"
#include "../../core/asset/texture_cube.h"
#include "../../core/vulkan_api/destruction.h"

namespace Shard3D {
#define RESMGR_RETURNRES(resource) std::unordered_map<AssetID, ##resource*>&
	enum class ResourceType {
		Deprecated,
		Texture2D,
		Model3D,
		TextureCube,
		Audio
	};
	enum class AssetType {
		Unknown,
		Deprecated,
		Texture2D, // functional
		Model3D, // functional
		TextureCube, // functional
		Material, // functional
		RenderTarget, // ***future
		Video, // ***future
		BakedLighting, // functional
		Prefab, // functional
		Level, // functional
		PhysicsAsset, // functional
		PhysicsMaterial, // functional
		PhysicsMaterialCollection, // functional
		PhysicsTireModel, // ***TODO***
		SkeletonRig, // functional
		SkeletonAnimation, // functional
		SkeletonInverseKinematicsData, // ***future
		HUDLayer,  // functional
		HUDElementPrefab, // ***TODO***
		Terrain,  // functional
		CustomMaterialShaderDescription, // ***future
		ShaderSourceGLSL, // ***future
		ScriptSourceCSharp, // ***future
		ScriptSourceCPlusPlus, // ***future
	};
	struct PackageInvocationIndex {
		uint8_t index = 1;
		ResourceType resource;
	};
	struct ResourceInfo {
		PackageInvocationIndex PII;
		std::vector<std::string> resources; // resource(s) in the virtual path
		std::string origin;
		std::string fileExtension;
	};
	class ResourceSystem {
	public:
		DELETE_COPY(ResourceSystem);
		struct __CoreAssets {
			const AssetID t_errorTexture = AssetID("engine/textures/null_tex.s3dasset");		// Purple checkerboard texture
			const AssetID t_errorMaterialTexture = AssetID("engine/textures/null_mat.s3dasset");		// Blank checkerboard texture
			const AssetID t_errorMemoryTexture = AssetID("engine/textures/null_mem.s3dasset");		// Blank checkerboard texture
			const AssetID t_blackTexture = AssetID("engine/textures/0x000000.s3dasset");		// Black texture
			const AssetID t_whiteTexture = AssetID("engine/textures/0xffffff.s3dasset");		// White texture
			const AssetID t_normalTexture = AssetID("engine/textures/0x8080ff.s3dasset");		// Blank normal texture;

			const AssetID c_blankCubemap = AssetID("engine/textures/cubemaps/blank_cube.s3dasset");	// Blank cube texture;
			const AssetID c_defaultSkybox = AssetID("engine/textures/cubemaps/sky0.s3dasset");			// Sky

			const AssetID m_errorMesh = AssetID("engine/meshes/null_mdl.s3dasset");		// No Mesh model
			const AssetID m_defaultModel = AssetID("engine/meshes/cube.s3dasset");			// Cube Mesh model
			const AssetID m_sphere = AssetID("engine/meshes/sphere.fbx.s3dasset");			// Sphere Mesh model
			const AssetID m_cylinder = AssetID("engine/meshes/cylinder.fbx.s3dasset");		// Cylinder Mesh model
			const AssetID m_arrow = AssetID("engine/meshes/arrow.fbx.s3dasset"); // arrow model

			const AssetID sm_errorMaterial = AssetID("engine/materials/world_grid.s3dasset");		// Opaque surface material (world grid)
			const AssetID sm_blankMaterial = AssetID("engine/materials/world_grid_blank.s3dasset");		// Opaque surface material (world grid)
		} static inline coreAssets;

		static Version getAssetVersion(const AssetID& asset);
		static AssetType discoverAssetType(const std::string& assetPath);
		static ResourceInfo discoverResourceInformation(const AssetID& asset);
		static std::string generateResourcePath(const std::string& origin, const std::string& destDir);
		static bool hasTextureBeenCached(const AssetID& asset, TextureLoadInfo loadInfo);
		static std::string getTextureCacheLocation(const AssetID& asset);
		static std::string getTextureCacheMetadataLocation(const AssetID& asset);

		//Clears textureAssets
		void clearTextureAssets();
		//Clears textureCubeAssets
		void clearTextureCubeAssets();
		//Clears meshAssets
		void clearMeshAssets();
		//Clears materialAssets
		void clearMaterialAssets();
		//Clears all of the asset maps
		void clearAllAssets();
		void clearAllUnusedAssets();
		static std::string getVirtualPath(AssetID asset);
		static AssetID getVirtualAssetPath(AssetID asset);
		static std::vector<std::string> getVirtualPathArray(AssetID asset);
		static std::vector<AssetID> getVirtualAssetPathArray(AssetID asset);
		static AssetID getVirtualMesh3DLocation(AssetID meshAsset);
		static AssetID discoverAttachedSkeletalMeshAsset(AssetID animation);
		static AssetID discoverAttachedMeshAsset(AssetID skeleton);

		/* Loads all of the materials in use by the level into the asset maps.
		Make sure to clear before loading, since you dont want to waste resources pointing to unused assets!
		*/

		ResourceSystem(S3DDevice& dvc, uint32_t framesInFlight);
		// Destroys all assets and doesnt keep the core engine ones either
		~ResourceSystem();

		void loadCoreAssets();
		inline uPtr<SmartDescriptorSet>& getDescriptor() { return staticMaterialPool; }

		void swapAsset(AssetType type, const AssetID& old_asset, const AssetID& new_asset);
		void unloadAsset(AssetType type, const AssetID& asset);

		bool loadMesh(const AssetID& asset, bool force = false);
		void unloadMesh(const AssetID& asset);
		inline Mesh3D* retrieveMesh(const AssetID& asset) {
#ifndef ENSET_UNSAFE_ASSETS
			return retrieveMesh_safe(asset);
#else			
			return retrieveMesh_unsafe(asset);
#endif
		}

		bool loadSkeleton(const AssetID& asset, bool force = false);
		void unloadSkeleton(const AssetID& asset);
		inline Skeleton* retrieveSkeleton(const AssetID& asset) {
#ifndef ENSET_UNSAFE_ASSETS
			return retrieveSkeleton_safe(asset);
#else			
			return retrieveSkeleton_unsafe(asset);
#endif
		}

		bool loadTexture(const AssetID& asset, bool force = false);
		void unloadTexture(const AssetID& asset);
		inline Texture2D* retrieveTexture(const AssetID& asset) {
#ifndef ENSET_UNSAFE_ASSETS
			return retrieveTexture_safe(asset);
#else
			return retrieveTexture_unsafe(asset);
#endif
		}

		bool loadBakedReflectionCubeAsset(const AssetID& asset, bool force = false);

		bool loadTextureCube(const AssetID& asset, bool force = false);
		void unloadTextureCube(const AssetID& asset);
		inline TextureCube* retrieveTextureCube(const AssetID& asset) {
#ifndef ENSET_UNSAFE_ASSETS
			return retrieveTextureCube_safe(asset);
#else
			return retrieveTextureCube_unsafe(asset);
#endif
		}

		bool loadMaterial(const AssetID& asset, bool force = false);
		bool loadMaterialRecursive(const AssetID& asset, bool force = false);
		void unloadMaterial(const AssetID& asset);

		void rebuildMaterial(Material* material);
		void runtimeUpdateSurfaceMaterial(SurfaceMaterial* material);
		inline Material* retrieveMaterial(const AssetID& asset) {
#ifndef ENSET_UNSAFE_ASSETS
			return retrieveMaterial_safe(asset);
#else
			return retrieveMaterial_unsafe(asset);
#endif
		}

		RESMGR_RETURNRES(Mesh3D) getMeshAssets();
		RESMGR_RETURNRES(Skeleton) getSkeletonAssets();
		RESMGR_RETURNRES(Texture2D) getTextureAssets();
		RESMGR_RETURNRES(TextureCube) getTextureCubeAssets();
		RESMGR_RETURNRES(Material) getMaterialAssets();
		void runGarbageCollector(uint32_t frameIndex, VkFence fence);

		void updateSurfaceMaterials(VkCommandBuffer commandBuffer);

		AssetID createNewSurfaceMaterialInstance(AssetID material);
		void destroyAllMaterialInstances();

		inline VertexArenaAllocator* getVertexAllocator() {
			return vertexArenaAllocator;
		}

	private:
		void buildMaterial(Material* material);
		void destroyCoreAssets();

		Mesh3D* retrieveMesh_unsafe(const AssetID& asset);
		Mesh3D* retrieveMesh_safe(const AssetID& asset);
		Skeleton* retrieveSkeleton_unsafe(const AssetID& asset);
		Skeleton* retrieveSkeleton_safe(const AssetID& asset);
		Texture2D* retrieveTexture_unsafe(const AssetID& asset);
		Texture2D* retrieveTexture_safe(const AssetID& asset);
		TextureCube* retrieveTextureCube_unsafe(const AssetID& asset);
		TextureCube* retrieveTextureCube_safe(const AssetID& asset);
		Material* retrieveMaterial_unsafe(const AssetID& asset);
		Material* retrieveMaterial_safe(const AssetID& asset);

		uPtr<SmartDescriptorSet> staticMaterialPool{};
		VertexArenaAllocator* vertexArenaAllocator{};

		uint32_t currentFrameIndex;
		VkFence currentFence = nullptr;
		const uint32_t maxFramesInFlight;
		std::unordered_map<AssetID, bool> resourceOccupationState;
		std::vector<std::pair<std::pair<AssetType, AssetID>, S3DDestructionObject>> destroyResourceMap;

		std::unordered_set<Material*> rebuildMaterialQueue;
		std::unordered_set<SurfaceMaterial*> runtimeUpdateSurfaceMaterialQueue;
		std::vector<AssetID> surfaceMaterialInstances;

		std::unordered_map<AssetID, Mesh3D*> meshAssets;
		std::unordered_map<AssetID, Skeleton*> skeletonAssets;
		std::unordered_map<AssetID, Texture2D*> textureAssets;
		std::unordered_map<AssetID, TextureCube*> textureCubeAssets;
		std::unordered_map<AssetID, Material*> materialAssets;
		S3DDevice& engineDevice;
		// mmmm need device class dont feel like adjusting codes
		friend class ModelImporter; 
		friend class LevelPeekingPanel;

		friend class RenderList; // why do i even do programming, flagrans pls dont report me 
	};

}