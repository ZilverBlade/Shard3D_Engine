#pragma once

#include "../../core.h"
#include "../../vulkan_abstr.h"
namespace Shard3D {
	inline namespace Systems {
		class DeferredRenderSystem;
		class PostProcessingSystem;
	}
	namespace Components {
		struct TransformComponent;
		struct Mesh3DComponent;
	}
	
	enum class DeferredRenderStage {
		EarlyZ,
		GBufferGen,
		Forward
	};
	/*
		** ancient system**
			GOOD | bind mesh once
			GOOD | iterate over all meshes once per frame
			BAD  | bind material pipeline multiple times

		**material class draw vectors:**
			GOOD | no iterations
			GOOD | bind pipeline for each material class once per frame
			BAD  | bind mesh multiple times per frame

		**material category vectors per submesh slot per mesh:**
			GOOD | bind mesh once
			GOOD | bind pipeline for each material class once per frame
			BAD  | iterate over meshes X amount of times, of which X is the amount of material class combinations

			**old system:**
	>>	**material category vectors per submesh slot per mesh in master rendering vector:**
	>>		MEH  | bind mesh the amount times the submeshes have unique material classes
	>>		GOOD | bind pipeline for each material class once per frame
	>>		GOOD | iterate over meshes X amount of times, of which X is the amount of unique material classes per mesh (mesh binding is done the least amount of times required)

			**new system:**
	>>	**material category vectors per submesh slot per mesh in master rendering vector:**
	>>		EPIC | bind arena allocator once per frame
	>>		GOOD | bind pipeline for each material class once per frame
	>>		GOOD | iterate over meshes X amount of times, of which X is the amount of unique material classes per mesh 
*/

	enum SurfaceMaterialPipelineClassPermutationOptions : uint32_t {
		SurfaceMaterialPipelineClassPermutationOptions_Opaque = 0x00,
		SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling = 0x01,
		SurfaceMaterialPipelineClassPermutationOptions_Translucent = 0x02,
		SurfaceMaterialPipelineClassPermutationOptions_AlphaTest = 0x04,
		SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates = 0x1000,
		SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms = 0x2000 // rigging and morphing
	};

	//typedef uint32_t SurfaceMaterialClassFlags;
	typedef uint32_t SurfaceMaterialPipelineClassPermutationFlags;

	struct _MaterialS3DGraphicsPipelineConfigInfo {
		VkPipelineLayout shaderPipelineLayout{};
		uPtr<S3DGraphicsPipeline> shaderPipeline{};
	};
	class MaterialSystem;
	class SurfaceMaterialClass {
	public:
		DELETE_COPY(SurfaceMaterialClass)

		SurfaceMaterialClass(S3DDevice& device, SurfaceMaterialPipelineClassPermutationFlags flags);
		virtual ~SurfaceMaterialClass();
		VkPipelineLayout getPipelineLayout() { return materialPipelineConfig.shaderPipelineLayout; }
		virtual VkPipelineLayout getEarlyZPipelineLayout() {
			return nullptr;
		}
		SurfaceMaterialPipelineClassPermutationFlags getClass() { return options_flags; }
	protected:
		_MaterialS3DGraphicsPipelineConfigInfo materialPipelineConfig{};	
		SurfaceMaterialPipelineClassPermutationFlags options_flags;
		S3DDevice& engineDevice;
	};
	
	class SurfaceMaterialClassForward : public SurfaceMaterialClass {
	public:
		SurfaceMaterialClassForward(MaterialSystem* system, SurfaceMaterialPipelineClassPermutationFlags flags);
		~SurfaceMaterialClassForward();
		void bindForward(VkCommandBuffer commandBuffer);
	};
	class SurfaceMaterialClassDeferred : public SurfaceMaterialClass {
	public:
		SurfaceMaterialClassDeferred(MaterialSystem* system, SurfaceMaterialPipelineClassPermutationFlags flags);
		~SurfaceMaterialClassDeferred();
		void bindDeferred(VkCommandBuffer commandBuffer);
		void bindEarlyDepth(VkCommandBuffer commandBuffer);
		virtual VkPipelineLayout getEarlyZPipelineLayout() override {
			return earlyZPipelineConfig.shaderPipelineLayout;
		}
	protected:
		_MaterialS3DGraphicsPipelineConfigInfo earlyZPipelineConfig{};
	};

	class SurfaceMaterial;
	class MaterialSystem {
	public:
		DELETE_COPY(MaterialSystem)
		MaterialSystem(Systems::DeferredRenderSystem* renderer, ResourceSystem* resourceSystem, VkDescriptorSetLayout globalSetLayout, VkDescriptorSetLayout sceneSSBOLayout, VkDescriptorSetLayout riggedSkeletonLayout);
		
		~MaterialSystem();

		void setAllAvailableMaterialShaderPermutations(const std::vector<SurfaceMaterialPipelineClassPermutationFlags>& shaders);

		void recompileSurface(const std::vector<SurfaceMaterialPipelineClassPermutationFlags>& shaders);

		void createSurfacePipelineLayout(
			VkPipelineLayout* pipelineLayout, 
			std::vector<VkDescriptorSetLayout> layouts,
			VkPushConstantRange* pushConstantRange = nullptr,
			bool noSSBO = false
		);

		void createSurfacePipeline(
			uPtr<S3DGraphicsPipeline>* pipeline,
			VkPipelineLayout pipelineLayout,
			S3DGraphicsPipelineConfigInfo& pipelineConfig, 
			DeferredRenderStage stage,
			const std::string& fragment_shader, 
			const std::string& vertex_shader
		);

		void createSurfacePipeline(uPtr<S3DGraphicsPipeline>* pipeline, 
			VkPipelineLayout pipelineLayout, 
			S3DGraphicsPipelineConfigInfo& pipelineConfig, 
			SurfaceMaterialClass* self,
			DeferredRenderStage stage
		);

		// RENDERING LIST
		SurfaceMaterialClass* getMaterialClass(SurfaceMaterialPipelineClassPermutationFlags flags);
		void bindMaterialClassEarlyDepth(SurfaceMaterialPipelineClassPermutationFlags flags, VkCommandBuffer commandBuffer);
		void bindMaterialClassDeferred(SurfaceMaterialPipelineClassPermutationFlags flags, VkCommandBuffer commandBuffer);
		void bindMaterialClassForward(SurfaceMaterialPipelineClassPermutationFlags flags, VkCommandBuffer commandBuffer);

		auto& getAvailableClasses() { return materialClassesOptionsGlobal; }

		auto& getDeferrableClasses() { return materialClassesOptionsDeferred; }
		auto& getForwardOnlyClasses() { return materialClassesOptionsForward; }
	private:
		S3DDevice& mDevice;
		ResourceSystem* resourceSystem{};
		DeferredRenderSystem* deferredRenderingSystem;
		VkDescriptorSetLayout mGlobalSetLayout;
		VkDescriptorSetLayout mSceneSetLayout;
		VkDescriptorSetLayout mSkeletonSetLayout;

		friend class SurfaceMaterialClassDeferredLighting;
		friend class SurfaceMaterialClassDeferred;
		friend class SurfaceMaterialClassForward;

		// unordered map instead of vector to speed up removing/adding elements if necessary on the fly
		std::vector<SurfaceMaterialPipelineClassPermutationFlags> materialClassesOptionsGlobal;

		std::vector<SurfaceMaterialPipelineClassPermutationFlags> materialClassesOptionsDeferred;
		std::vector<SurfaceMaterialPipelineClassPermutationFlags> materialClassesOptionsForward;

		std::map<SurfaceMaterialPipelineClassPermutationFlags, SurfaceMaterialClass*> materialClassesGlobal;
		std::map<SurfaceMaterialPipelineClassPermutationFlags, SurfaceMaterialClassDeferred*> materialClassesDeferred;
		std::map<SurfaceMaterialPipelineClassPermutationFlags, SurfaceMaterialClassForward*> materialClassesForward;

		VkPipeline earlyZBasePipeline = VK_NULL_HANDLE; // for pipeline derivatives
		VkPipeline deferredBasePipeline = VK_NULL_HANDLE; // for pipeline derivatives
		VkPipeline forwardBasePipeline = VK_NULL_HANDLE; // for pipeline derivatives

		bool useReverseDepth = false;
	};
}
