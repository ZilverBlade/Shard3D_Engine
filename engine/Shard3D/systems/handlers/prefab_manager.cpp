#include "prefab_manager.h"
#include "../../ecs.h" 
#include  <magic_enum.hpp>

#define NO_PREFAB_INHERIT_ACTOR 0
namespace Shard3D {
	inline namespace ECS {
		sPtr<Prefab> PrefabManager::getPrefab(AssetID prefab) {
			if (basePrefabMap.find(prefab) != basePrefabMap.end())
				return basePrefabMap[prefab];
			SHARD3D_ERROR("Prefab {0} does not exist!", prefab.getAsset());
			return nullptr;
		}

		PrefabManager::PrefabManager(ResourceSystem* resourceSystem) : resourceSystem(resourceSystem) {
		
		}

		bool PrefabManager::cachePrefab(AssetID asset) {
			if (!IOUtils::doesFileExist(asset.getAbsolute())) return false;
			if (basePrefabMap.find(asset) == basePrefabMap.end()) {
				Prefab* bp = new Prefab(resourceSystem);
				LevelManager lvlMgr(*bp->getLocalLevel(), resourceSystem);
				lvlMgr.load(asset.getAbsolute());
				simdjson::dom::parser parser;
				simdjson::padded_string json = simdjson::padded_string::load(asset.getAbsolute());
				const auto& data = parser.parse(json);
				if (simdjson::error_code ec = data.error(); ec != simdjson::error_code::SUCCESS) {
					SHARD3D_ERROR("simdjson parser error. error code: {0} ({1})", magic_enum::enum_name(ec), (int)ec);
				}
				bp->enableScript = data["enableScript"].get_bool().value();
				bp->scriptModule = data["scriptModule"].get_string().value();

				for (auto& noInheritActor : bp->localLevel->actorMap) {
					Actor nihActor = { noInheritActor.second, bp->localLevel };
					if (nihActor.hasRelationship()) {
						if (nihActor.getComponent<Components::RelationshipComponent>().parentActor == entt::null) {
							bp->baseSceneActorHandle = noInheritActor.second;
							break;
						}
					} else {
						bp->baseSceneActorHandle = noInheritActor.second;
						break;
					}
				}
				basePrefabMap.emplace(asset, bp);
			}
			return true;
		}

		Actor PrefabManager::instPrefab(Level* level, UUID scopeUUID, UUID id, AssetID prefab, const std::string& name) {
			if (basePrefabMap.find(prefab) == basePrefabMap.end()) {
				if (!cachePrefab(prefab)) {
					SHARD3D_ERROR("Prefab {0} does not exist!", prefab.getAsset());
					return Actor();
				} 
			}
			Actor actor = basePrefabMap[prefab]->createActor(level, name, scopeUUID, id, prefab);
			instantiatedPrefabs[prefab].push_back(getTrueActorUUID(actor.getInstancerUUID(), actor.getScopedID()));

			return actor;
		}

		Actor PrefabManager::reloadPrefab(Actor actor, AssetID prefab) {
			//UUID uuid = actor.getScopedID();
			//killActor(actor);
			//Actor newActor = instPrefab(uuid, prefab);
			//return newActor;
			SHARD3D_NOIMPL_C;
			return Actor();
		}

		void PrefabManager::killPrefab(AssetID prefab) {
			if (basePrefabMap.find(prefab) != basePrefabMap.end())
				basePrefabMap.erase(prefab);
			else
				SHARD3D_ERROR("Prefab {0} does not exist!", prefab.getAsset());
		}
	}
}