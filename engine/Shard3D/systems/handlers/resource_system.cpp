
#include "resource_system.h"
#include "vertex_arena_allocator.h"

#include "../../core/asset/assetmgr.h"

#include "../../core.h"
#include "../../utils/json_ext.h"
#include <ktx.h>

#include <fstream>
#include <filesystem>
#include "../../core/vulkan_api/bindless.h"
#include <vulkan/vk_enum_string_helper.h>

namespace Shard3D {


	// Check if is a core texture 
	static bool isCoreAsset_T(AssetKey asset) {
		return	asset == AssetID("engine/textures/null_tex.s3dasset").getID() ||
			asset == AssetID("engine/textures/null_mat.s3dasset").getID() ||
			asset == AssetID("engine/textures/null_mem.s3dasset").getID() ||
			asset == AssetID("engine/textures/0x000000.s3dasset").getID() ||
			asset == AssetID("engine/textures/0xffffff.s3dasset").getID() ||
			asset == AssetID("engine/textures/0x8080ff.s3dasset").getID();
	}

	// Check if is a core cubemap
	static bool isCoreAsset_C(AssetKey asset) {
		return	asset == AssetID("engine/textures/cubemaps/blank_cube.s3dasset").getID() ||
			asset == AssetID("engine/textures/cubemaps/sky0.s3dasset").getID() ||
			asset == AssetID("engine/textures/cubemaps/sky1.s3dasset").getID();
	}

	// Check if is a core mesh
	static bool isCoreAsset_M(AssetKey asset) {
		return	asset == AssetID("engine/meshes/null_mdl.s3dasset").getID() ||
			asset == AssetID("engine/meshes/sphere.fbx.s3dasset").getID() ||
			asset == AssetID("engine/meshes/cylinder.fbx.s3dasset").getID() ||
			asset == AssetID("engine/meshes/arrow.fbx.s3dasset").getID() ||
			asset == AssetID("engine/meshes/cube.s3dasset").getID();
	}

	// Check if is a core surface material
	static bool isCoreAsset_S(AssetKey asset) {
		return	asset == AssetID("engine/materials/world_grid.s3dasset").getID() ||
			asset == AssetID("engine/materials/world_grid_blank.s3dasset").getID();
	}

	void ResourceSystem::clearTextureAssets() {
		SHARD3D_INFO("Clearing all texture assets");
		for (const auto& [asset, resource] : textureAssets)
			if (!isCoreAsset_T(asset.getID()))
				this->unloadTexture(asset);
	}
	void ResourceSystem::clearTextureCubeAssets() {
		SHARD3D_INFO("Clearing all cubemap assets");
		for (const auto& [asset, resource] : textureCubeAssets)
			if (!isCoreAsset_C(asset.getID()))
				this->unloadTextureCube(asset);
	}
	void ResourceSystem::clearMeshAssets() {
		SHARD3D_INFO("Clearing all mesh assets");
		for (const auto& [asset, resource] : meshAssets)
			if (!isCoreAsset_M(asset.getID()))
				this->unloadMesh(asset);
	}
	void ResourceSystem::clearMaterialAssets() {
		SHARD3D_INFO("Clearing all material assets");
		for (const auto& [asset, resource] : materialAssets)
			if (!isCoreAsset_M(asset.getID()))
				this->unloadMaterial(asset);
	}
	void ResourceSystem::clearAllAssets() {
		clearTextureAssets();
		clearTextureCubeAssets();
		clearMeshAssets();
		clearMaterialAssets();
	}
	void ResourceSystem::clearAllUnusedAssets() {

	}
	static std::string getAbsNumberFromChar(uint8_t num) {
		std::stringstream stream{};
		if (num > 99) return std::to_string(99);
		if (num < 10) stream << 0;
		stream << std::to_string(num);
		return stream.str();
	}

	std::string ResourceSystem::getVirtualPath(AssetID asset) {
		simdjson::dom::parser parser;
		simdjson::padded_string json = simdjson::padded_string::load(asset.getAbsolute());
		const auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			SHARD3D_ERROR("Failed retrieving virtual path for asset '{0}'! simdjson error code: {1}", asset.getAsset(), ec);
			return std::string();
		}
		return std::string(data["assetFile"].get_string().value());
	}
	AssetID ResourceSystem::getVirtualAssetPath(AssetID asset) {
		return AssetID(getVirtualPath(asset));
	}
	std::vector<std::string> ResourceSystem::getVirtualPathArray(AssetID asset) {
		simdjson::dom::parser parser;
		simdjson::padded_string json = simdjson::padded_string::load(asset.getAbsolute());
		const auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			SHARD3D_ERROR("Failed retrieving virtual path for asset '{0}'! simdjson error code: {1}", asset.getAsset(), ec);
			return std::vector<std::string>();
		}	
		std::vector<std::string> mydata{};
		for (int i = 0; i < data["assetFiles"].get_array().size(); i++) {
			mydata.push_back(std::string(data["assetFiles"].get_array().at(i).get_string().value()));
		}
		return mydata;
	}
	std::vector<AssetID> ResourceSystem::getVirtualAssetPathArray(AssetID asset) {
		simdjson::dom::parser parser;
		simdjson::padded_string json = simdjson::padded_string::load(asset.getAbsolute());
		const auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			SHARD3D_ERROR("Failed retrieving virtual path for asset '{0}'! simdjson error code: {1}", asset.getAsset(), ec);
			return std::vector<AssetID>();
		}
		std::vector<AssetID> mydata{};
		for (int i = 0; i < data["assetFiles"].get_array().size(); i++) {
			mydata.push_back(AssetID(std::string(data["assetFiles"].get_array().at(i).get_string().value())));
		}
		return mydata;
	}
	AssetID ResourceSystem::getVirtualMesh3DLocation(AssetID meshAsset) {
		simdjson::dom::parser parser;
		if (!std::filesystem::exists(meshAsset.getAbsolute())) return false;
		simdjson::padded_string json = simdjson::padded_string::load(meshAsset.getAbsolute());
		const auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			SHARD3D_WARN("Failed to load {0} (ID {1})", meshAsset.getAsset(), meshAsset.getID());
			return false;
		}
		if (data["assetType"].get_string().value() != "mesh3d") {
			SHARD3D_WARN("Trying to load non mesh3d as a mesh3d asset!");
			return false;
		}

		if (SIMDJSON_READ_S3DVERSION(data["version"]) < Version(0, 13, 0)) {
			return getVirtualAssetPathArray(meshAsset)[0];
		} else {
			return getVirtualAssetPath(meshAsset);
		}
	}

	AssetID ResourceSystem::discoverAttachedSkeletalMeshAsset(AssetID animation) {
		simdjson::dom::parser parser;
		if (!std::filesystem::exists(animation.getAbsolute())) return nullptr;
		simdjson::padded_string json = simdjson::padded_string::load(animation.getAbsolute());
		const auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			SHARD3D_WARN("Failed to load {0} (ID {1})", animation.getAsset(), animation.getID());
			return nullptr;
		}
		if (data["assetType"].get_string().value() != "skeletal_animation") {
			SHARD3D_WARN("Trying to load non skeletal_animation as a skeletal_animation asset!");
			return nullptr;
		}


		return std::string(data["attachedSkeletonAsset"].get_string().value());
	}

	AssetID ResourceSystem::discoverAttachedMeshAsset(AssetID skeleton) {
		simdjson::dom::parser parser;
		if (!std::filesystem::exists(skeleton.getAbsolute())) return nullptr;
		simdjson::padded_string json = simdjson::padded_string::load(skeleton.getAbsolute());
		const auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			SHARD3D_WARN("Failed to load {0} (ID {1})", skeleton.getAsset(), skeleton.getID());
			return nullptr;
		}
		if (data["assetType"].get_string().value() != "skeleton") {
			SHARD3D_WARN("Trying to load non skeleton as a skeleton asset!");
			return nullptr;
		}

		return std::string(data["attachedMeshAsset"].get_string().value());
	}

	//static std::string getMagicPath(AssetID asset, const char* extension, PackageInvocationIndex invocationIndex) {
	//	std::stringstream builder{};
	//	builder << "assets/.virtual/pkg" << getAbsNumberFromChar(invocationIndex.index);
	//	if (!std::filesystem::exists(builder.str())) std::filesystem::create_directory(builder.str());
	//	switch (invocationIndex.resource) {
	//	case (ResourceType::Texture): // texture and texturecube get placed in the same folder as they are both image resources
	//	case (ResourceType::TextureCube):
	//		builder << "/texturedata";
	//		break;
	//	case (ResourceType::Model3D):
	//		builder << "/modeldata";
	//		break;
	//	case (ResourceType::Audio):
	//		builder << "/audiodata";
	//		break;
	//	}
	//	if (!std::filesystem::exists(builder.str())) std::filesystem::create_directory(builder.str());
	//	builder << "/" << asset.getID() << "." << extension;
	//	return builder.str();
	//}

	Version ResourceSystem::getAssetVersion(const AssetID& asset) {
		ResourceInfo info{};
		simdjson::dom::parser parser;
		if (!std::filesystem::exists(asset.getAbsolute())) return Version(VersionState::PreAlpha, 0, 0,0);
		simdjson::padded_string json = simdjson::padded_string::load(asset.getAbsolute());
		const auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			SHARD3D_WARN("Failed to load {0} (ID {1})", asset.getAsset(), asset.getID());
			return Version(VersionState::PreAlpha, 0, 0, 0);
		}
		if (data["version"].error() != simdjson::SUCCESS) {
			SHARD3D_ERROR("Asset file {0} is missing version!!!", asset.getAsset());
			return Version(VersionState::PreAlpha, 0, 0, 0);
		}
		return SIMDJSON_READ_S3DVERSION(data["version"]);
	}

	AssetType ResourceSystem::discoverAssetType(const std::string& assetPath) {
		if (strUtils::hasEnding(assetPath, ".s3dlevel")) return AssetType::Level;
		if (strUtils::hasEnding(assetPath, ".s3dprefab")) return AssetType::Prefab;
		if (strUtils::hasEnding(assetPath, ".cs")) return AssetType::ScriptSourceCSharp;
		if (strUtils::hasEnding(assetPath, ".cpp") || strUtils::hasEnding(assetPath, ".h")) return AssetType::ScriptSourceCPlusPlus;
		if (strUtils::hasEnding(assetPath, ".comp") || strUtils::hasEnding(assetPath, ".glsl") || strUtils::hasEnding(assetPath, ".vert") || strUtils::hasEnding(assetPath, ".frag")) return AssetType::ShaderSourceGLSL;
		if (!strUtils::hasEnding(assetPath, ".s3dasset")) return AssetType::Unknown;

		simdjson::dom::parser parser;
		simdjson::padded_string json = simdjson::padded_string::load(assetPath);
		const auto& data = parser.parse(json);
		if (int ec = data.error(); ec == simdjson::error_code::SUCCESS) {
			std::string itemType = std::string(data["assetType"].get_string().value());	
			if (itemType == std::string("texture2d"))
				return AssetType::Texture2D;
			if (itemType == std::string("texturecube"))
				return AssetType::TextureCube;
			if (itemType == std::string("mesh3d"))
				return AssetType::Model3D;
			if (itemType == std::string("skeleton"))
				return AssetType::SkeletonRig;
			if (itemType == std::string("skeletal_animation"))
				return AssetType::SkeletonAnimation;
			if (itemType == std::string("surface_material") /*pre 0.13.0-b230923 versions*/ || itemType == std::string("material"))
				return AssetType::Material;
			if (itemType == std::string("baked_lighting"))
				return AssetType::BakedLighting;
			if (itemType == std::string("physics_asset"))
				return AssetType::PhysicsAsset;
			if (itemType == std::string("physics_material"))
				return AssetType::PhysicsMaterial;
			if (itemType == std::string("physics_material_collection"))
				return AssetType::PhysicsMaterialCollection;
			if (itemType == std::string("terrain"))
				return AssetType::Terrain;
			if (itemType == std::string("hud_layer"))
				return AssetType::HUDLayer;
		}
		
		return AssetType::Unknown;
	}

	ResourceSystem::ResourceSystem(S3DDevice& dvc, uint32_t framesInFlight) : engineDevice(dvc), maxFramesInFlight(framesInFlight) {
		staticMaterialPool = SmartDescriptorSet::Builder(dvc)
			.setMaximumSize(0x0000FFFF)
			.setShaderStages(VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_VERTEX_BIT /* | VK_SHADER_STAGE_GEOMETRY_BIT*/)
			.addDescriptorType(VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, BINDLESS_SSBO_BINDING)
			.addDescriptorType(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, BINDLESS_SAMPLER_BINDING)
			.build();
		vertexArenaAllocator = new VertexArenaAllocator(dvc, ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
	}
	ResourceSystem::~ResourceSystem() {
		vertexArenaAllocator->invalidate();
		for (auto& [asset, res] : meshAssets) {
			delete res;
		}
		for (auto& [asset, res] : skeletonAssets) {
			delete res;
		}
		for (auto& [asset, res] : textureAssets) {
			delete res;
		}
		for (auto& [asset, res] : textureCubeAssets) {
			delete res;
		}
		for (auto& [asset, res] : materialAssets) {
			delete res;
		}
		delete vertexArenaAllocator;
	}

	void ResourceSystem::loadCoreAssets() {
		loadTexture(coreAssets.t_errorTexture);
		loadTexture(coreAssets.t_errorMaterialTexture);
		loadTexture(coreAssets.t_errorMemoryTexture);
		loadTexture(coreAssets.t_whiteTexture);
		loadTexture(coreAssets.t_blackTexture);
		loadTexture(coreAssets.t_normalTexture);
		loadTextureCube(coreAssets.c_blankCubemap);
		loadTextureCube(coreAssets.c_defaultSkybox);
		loadMaterial(coreAssets.sm_errorMaterial);
		loadMaterial(coreAssets.sm_blankMaterial);
		loadMaterial(AssetID("engine/materials/decals/bullet.s3dasset"));
		loadMesh(coreAssets.m_defaultModel);
		loadMesh(coreAssets.m_errorMesh);
		loadMesh(coreAssets.m_sphere);
		loadMesh(coreAssets.m_cylinder);
		loadMesh(coreAssets.m_arrow);
		
		for (auto& entry : std::filesystem::directory_iterator(AssetID("engine/textures/internal").getAbsolute())) {
			if (entry.is_regular_file()) {
				if (entry.path().extension() == std::filesystem::path(".s3dasset")) {
					std::string reldp = std::filesystem::relative(entry.path().string(), std::filesystem::path(ProjectSystem::getAssetLocation())).string();
					loadTexture(AssetID(reldp));
				}
			}
		}
		for (auto& entry : std::filesystem::directory_iterator(AssetID("engine/meshes/terrain").getAbsolute())) {
			if (entry.path().extension() == ".s3dasset") {
				AssetID asset = AssetID(entry.path().string().substr(ProjectSystem::getAssetLocation().size()));
				loadMesh(asset);
			}
		}
	}

	ResourceInfo ResourceSystem::discoverResourceInformation(const AssetID& asset) {
		ResourceInfo info{};
		simdjson::dom::parser parser;
		simdjson::padded_string json = simdjson::padded_string::load(asset.getAbsolute());
		const auto& data = parser.parse(json);
		AssetType type = ResourceSystem::discoverAssetType(asset.getAbsolute());
		ResourceType rtype;
		switch (type) {
		case(AssetType::Texture2D): rtype = ResourceType::Texture2D; break;
		case(AssetType::TextureCube): rtype = ResourceType::TextureCube; break;
		case(AssetType::Model3D): rtype = ResourceType::Model3D; break;
		}
		info.origin = data["assetOrig"].get_string().value();
		if (type == AssetType::TextureCube || type == AssetType::Model3D) {
			info.resources = getVirtualPathArray(asset);
		}
		else {
			info.resources.push_back(getVirtualPath(asset));
		}
		info.PII = { static_cast<uint8_t>(data["resourcePII"].get_uint64().value()), rtype };
		info.fileExtension = data["fileExt"].get_string().value();
		return info;
	}

	std::string ResourceSystem::generateResourcePath(const std::string& origin, const std::string& destDir) {
		std::string file = IOUtils::getFileOnly(origin);
		return std::string(destDir + "/" + file);
	}

	static bool cmpTextureCacheData(const AssetID& asset, TextureLoadInfo loadInfoData) {
		ResourceInfo info{};
		simdjson::dom::parser parser;
		std::string location = ResourceSystem::getTextureCacheLocation(asset) + ".s3dcache";
		if (!std::filesystem::exists(location)) {
			return false;
		}
		simdjson::padded_string json = simdjson::padded_string::load(location);
		const auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			SHARD3D_ERROR("Failed to load cache data for {0} (ID {1})", asset.getAsset(), asset.getID());
			return false;
		}
		if (data["cacheType"].get_string().value() != "zktx2") {
			SHARD3D_ERROR("BAD CACHE FORMAT!!!");
			return false;
		}
		bool formatMatches = getVkFormatTextureFormat(loadInfoData.format, loadInfoData.colorSpace, loadInfoData.channels, loadInfoData.compression) == enum_VkFormat(std::string(data["cacheData"]["vkFormat"].get_string().value()));
		bool genMipsMatches = loadInfoData.enableMipMaps == data["cacheData"]["enableMipMaps"].get_bool().value();
		bool isNormalMapMatches = data["cacheData"]["isNormalMap"].get_bool().error() == simdjson::SUCCESS ? loadInfoData.isNormalMap == data["cacheData"]["isNormalMap"].get_bool().value() : true;
		return formatMatches && genMipsMatches && isNormalMapMatches;
	}

	static void cacheTexture(const AssetID& asset, ktxTexture2* texture, TextureLoadInfo loadInfo) {
		SHARD3D_INFO("Caching texture {0}", asset.getAsset());
		std::string cl = ResourceSystem::getTextureCacheLocation(asset);
		std::ofstream fout(cl + ".s3dcache");

		if (!std::filesystem::exists(ProjectSystem::getCacheLocation() + "ktx2")) {
			std::filesystem::create_directories(ProjectSystem::getCacheLocation() + "ktx2");
		}

		nlohmann::ordered_json out{};
		out["version"] = ENGINE_VERSION;
		out["cacheType"] = "zktx2";
		out["cacheManifestDataInt64"] = 0i64;
		out["cacheData"]["vkFormat"] = string_VkFormat(getVkFormatTextureFormat(loadInfo.format, loadInfo.colorSpace, loadInfo.channels, loadInfo.compression));
		out["cacheData"]["enableMipMaps"] = loadInfo.enableMipMaps;
		out["cacheData"]["isNormalMap"] = loadInfo.isNormalMap;

		fout << out.dump(4);

		fout.flush();
		fout.close();
		TextureImporter::exportCompressedTextureKTX(texture, cl.c_str());
	}

	bool ResourceSystem::hasTextureBeenCached(const AssetID& asset, TextureLoadInfo loadInfo) {
		if (std::filesystem::exists(getTextureCacheLocation(asset))) {
			return cmpTextureCacheData(asset, loadInfo);
		}
		return false;
	}

	std::string ResourceSystem::getTextureCacheMetadataLocation(const AssetID& asset) {
		return ProjectSystem::getCacheLocation() + "ktx2/" + std::to_string(asset.getID()) + ".s3dcache";
	}
	std::string ResourceSystem::getTextureCacheLocation(const AssetID& asset) {
		return ProjectSystem::getCacheLocation() + "ktx2/" + std::to_string(asset.getID()) + ".zktx2";
	}

	RESMGR_RETURNRES(Mesh3D) ResourceSystem::getMeshAssets() {
		return meshAssets;
	}

	RESMGR_RETURNRES(Texture2D) ResourceSystem::getTextureAssets() {
		return textureAssets;
	}

	RESMGR_RETURNRES(TextureCube) ResourceSystem::getTextureCubeAssets() {
		return textureCubeAssets;
	}

	RESMGR_RETURNRES(Material) ResourceSystem::getMaterialAssets() {
		return materialAssets;
	}

	void ResourceSystem::runGarbageCollector(uint32_t frameIndex, VkFence fence) {
		currentFrameIndex = frameIndex; 
		uint32_t lastOccupiedFrameIndex = (frameIndex + 1) % maxFramesInFlight;
		// TODO FIX:
		//for (int i = 0; i < destroyResourceMap.size(); i++) {
		//	auto& pair = destroyResourceMap[i];
		//	if (pair.second.getFence() == fence) continue; // from same frame
		//	if (pair.second.isFree()) {
		//		AssetID asset = pair.first.second;
		//		AssetType rtype = pair.first.first;
		//		if (!resourceOccupationState[asset]) {
		//			switch (rtype) {
		//			case(AssetType::Model3D):
		//				delete meshAssets[asset];
		//				meshAssets.erase(asset);
		//				resourceOccupationState.erase(asset);
		//				break;
		//			case(AssetType::Texture2D):
		//				delete textureAssets[asset];
		//				textureAssets.erase(asset);
		//				resourceOccupationState.erase(asset);
		//				break;
		//			case(AssetType::TextureCube):
		//				delete textureCubeAssets[asset];
		//				textureCubeAssets.erase(asset);
		//				resourceOccupationState.erase(asset);
		//				break;
		//			case(AssetType::Material):
		//				auto iter = std::find(rebuildMaterialQueue.begin(), rebuildMaterialQueue.end(), materialAssets[asset]);
		//				if (iter != rebuildMaterialQueue.end()) {
		//					rebuildMaterialQueue.erase(iter);
		//				}
		//				delete materialAssets[asset];
		//				materialAssets.erase(asset);
		//				resourceOccupationState.erase(asset);
		//				break;
		//			}
		//			destroyResourceMap.erase(destroyResourceMap.begin() + i);
		//			i--;
		//		}
		//	}
		//}
		
		for (auto& [asset, bv] : resourceOccupationState)
			resourceOccupationState[asset] = false;
		if (rebuildMaterialQueue.size() != 0)   {
			vkDeviceWaitIdle(engineDevice.device());
			for (Material* material : rebuildMaterialQueue) {
				buildMaterial(material);
			}
			rebuildMaterialQueue.clear();
		}
	}

	void ResourceSystem::updateSurfaceMaterials(VkCommandBuffer commandBuffer) {
		for (SurfaceMaterial* sf : runtimeUpdateSurfaceMaterialQueue) {
			MaterialBufferData sfData = sf->getSurfaceMaterialBufferData();
			vkCmdUpdateBuffer(commandBuffer, sf->getBuffer()->getBuffer(), 0, sizeof(MaterialBufferData), &sfData);
		}
		runtimeUpdateSurfaceMaterialQueue.clear();
	}

	AssetID ResourceSystem::createNewSurfaceMaterialInstance(AssetID material) {
		Material* mat = retrieveMaterial(material);
		if (mat->getClass() != MaterialClass::Surface) {
			SHARD3D_ERROR("Cannot create a surface material instance out of a decal material!!!");
			return AssetID::null();
		}
		SurfaceMaterial* sf = reinterpret_cast<SurfaceMaterial*>(mat);
		UUID instanceID = UUID();
		AssetID instAsset = material.getAsset() + "/$" + std::to_string(instanceID);
		SurfaceMaterial* instance= new SurfaceMaterial(this);
		instance->shadingModel = sf->shadingModel;

		instance->reflectivity = sf->reflectivity;
		instance->reflectivityTex = sf->reflectivityTex;
		instance->specular = sf->specular;
		instance->specularTex = sf->specularTex;
		instance->glossiness = sf->glossiness;
		instance->glossinessTex = sf->glossinessTex;
		instance->diffuseColor = sf->diffuseColor;
		instance->diffuseTex = sf->diffuseTex;
		instance->emissiveColor = sf->emissiveColor;
		instance->emissiveTex = sf->emissiveTex;
		instance->normalTex = sf->normalTex;
		instance->maskTex = sf->maskTex;
		instance->opacity = sf->opacity;
		instance->opacityTex = sf->opacityTex;

		instance->sfExt_DeferredFoliage_Thickness = sf->sfExt_DeferredFoliage_Thickness;
		instance->sfExt_ForwardRefractive_Dispersion = sf->sfExt_ForwardRefractive_Dispersion;
		instance->sfExt_ForwardRefractive_IOR = sf->sfExt_ForwardRefractive_IOR;

		instance->texCoordMultiplier = sf->texCoordMultiplier;
		instance->texCoordOffset = sf->texCoordOffset;
		
		instance->setDoubleSided(sf->isDoubleSided());

		materialAssets[instAsset] = instance;
		surfaceMaterialInstances.push_back(instAsset);

		rebuildMaterial(instance);

		return instAsset;
	}

	void ResourceSystem::destroyAllMaterialInstances() {
		for (AssetID sfAsset : surfaceMaterialInstances) {
			unloadMaterial(sfAsset);
		}
		surfaceMaterialInstances.clear();
	}

	void ResourceSystem::swapAsset(AssetType type, const AssetID& old_asset, const AssetID& new_asset) {
		if (type == AssetType::Level) return;

		switch (type) {
		case(AssetType::Model3D): {
			if (meshAssets.find(old_asset) == meshAssets.end()) break;
			auto get = meshAssets.extract(old_asset);
			get.key() = new_asset;
			meshAssets.insert(std::move(get));
		} break;
		case(AssetType::Texture2D): {
			if (textureAssets.find(old_asset) == textureAssets.end()) break;
			auto get = textureAssets.extract(old_asset);
			get.key() = new_asset;
			textureAssets.insert(std::move(get));
		} break;
		case(AssetType::TextureCube): {
			if (textureCubeAssets.find(old_asset) == textureCubeAssets.end()) break;
			auto get = textureCubeAssets.extract(old_asset);
			get.key() = new_asset;
			textureCubeAssets.insert(std::move(get));
		} break;
		case(AssetType::Material): {
			if (materialAssets.find(old_asset) == materialAssets.end()) break;
			auto get = materialAssets.extract(old_asset);
			get.key() = new_asset;
			materialAssets.insert(std::move(get));
		} break;
		}
	}

	void ResourceSystem::unloadAsset(AssetType type, const AssetID& asset) {
		if (type == AssetType::Level) return;

		switch (type) {
		case(AssetType::Model3D): 
			if (!isCoreAsset_M(asset))
				unloadMesh(asset);
			break;
		case(AssetType::Texture2D):
			if (!isCoreAsset_T(asset))
				unloadTexture(asset);
			break;
		case(AssetType::TextureCube):
			if (!isCoreAsset_C(asset))
				unloadTextureCube(asset);
			 break;
		case(AssetType::Material):
			if (!isCoreAsset_S(asset))
				unloadMaterial(asset);
			break;
		}
	}
	
	bool ResourceSystem::loadMesh(const AssetID& assetPath, bool force) {
		ResourceInfo info{};
		simdjson::dom::parser parser;
		if (!std::filesystem::exists(assetPath.getAbsolute())) return false;
		simdjson::padded_string json = simdjson::padded_string::load(assetPath.getAbsolute());
		const auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			SHARD3D_WARN("Failed to load {0} (ID {1})", assetPath.getAsset(), assetPath.getID());
			return false;
		}
		if (data["assetType"].get_string().value() != "mesh3d") {
			SHARD3D_WARN("Trying to load non mesh3d as a mesh3d asset!");
			return false;
		}

		if (!force) if (meshAssets.find(assetPath) != meshAssets.end())
			return false;

		std::vector<MeshRawData*> mshData{};
		std::vector<AssetID> vpArray;
		if (SIMDJSON_READ_S3DVERSION(data["version"]) < Version(0, 13, 0)) {
			vpArray = getVirtualAssetPathArray(assetPath);
		} else {
			vpArray.push_back(getVirtualAssetPath(assetPath));
		}
		for (int i = 0; i < vpArray.size(); i++) {
			mshData.push_back(ModelImporter::loadMeshData(vpArray[i]));
			if (!mshData[i]) {
				SHARD3D_ERROR("Error loading mesh");
				return false;
			}
		}
		std::vector<AssetID> materials{};
		for (int i = 0; i < data["materials"].get_array().size(); i++) {
			materials.push_back(AssetID(std::string(data["materials"].get_array().at(i).get_string().value())));
		}
		Mesh3D* mesh = new Mesh3D(engineDevice, vertexArenaAllocator, mshData[0], materials);
		for (MeshRawData* md : mshData)
			ModelImporter::freeMeshData(md);
		if (!mesh)
			return false;
		SHARD3D_LOG("Loaded asset to resource map '{0}'", assetPath.getAsset());
		for (auto& material : mesh->materials) ResourceSystem::loadMaterialRecursive(material);
		meshAssets[assetPath] = std::move(mesh);
		return true;
	}

	bool ResourceSystem::loadSkeleton(const AssetID& asset, bool force) {
		ResourceInfo info{};
		simdjson::dom::parser parser;
		if (!std::filesystem::exists(asset.getAbsolute())) return false;
		simdjson::padded_string json = simdjson::padded_string::load(asset.getAbsolute());
		const auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			SHARD3D_WARN("Failed to load {0} (ID {1})", asset.getAsset(), asset.getID());
			return false;
		}
		if (data["assetType"].get_string().value() != "skeleton") {
			SHARD3D_WARN("Trying to load non skeleton as a skeleton asset!");
			return false;
		}

		if (!force) if (skeletonAssets.find(asset) != skeletonAssets.end())
			return false;

		SkeletonRawData* sklData{};
		AssetID vpath = getVirtualAssetPath(asset);
		sklData = ModelImporter::loadSkeletalData(vpath);
		if (!sklData) {
			SHARD3D_ERROR("Error loading skeleton");
			return false;
		}
		Skeleton* skeleton = new Skeleton(engineDevice, this, sklData);
		ModelImporter::freeSkeletalData(sklData);
		if (!skeleton)
			return false;
		SHARD3D_LOG("Loaded asset to resource map '{0}'", asset.getAsset());
		skeletonAssets[asset] = std::move(skeleton);
		return true;
	}
	bool ResourceSystem::loadTexture(const AssetID& asset, bool force) {
		simdjson::dom::parser parser;
		if (!std::filesystem::exists(asset.getAbsolute())) return false;
		simdjson::padded_string json = simdjson::padded_string::load(asset.getAbsolute());
		const auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			SHARD3D_WARN("Failed to load {0} (ID {1})", asset.getAsset(), asset.getID());
			return false;
		}
		if (data["assetType"].get_string().value() != "texture2d") {
			SHARD3D_ERROR("Trying to load non texture2d as a texture2d asset!");
			return false;
		}
		TextureLoadInfo loadInfo{};
		loadInfo.filter = enum_TextureFilter(std::string(data["properties"]["filter"].get_string().value()));
		loadInfo.addressMode = enum_TextureSamplerMode(std::string(data["properties"]["addressMode"].get_string().value()));
		loadInfo.format = enum_TextureFormat(std::string(data["properties"]["format"].get_string().value()));
		loadInfo.compression = enum_TextureCompression(std::string(data["properties"]["compression"].get_string().value()));
		loadInfo.channels = enum_TextureChannels(std::string(data["properties"]["channels"].get_string().value()));
		loadInfo.colorSpace = enum_TextureColorSpace(std::string(data["properties"]["colorSpace"].get_string().value()));
		loadInfo.enableMipMaps = data["properties"]["enableMipMaps"].get_bool().value();
		if (data["properties"]["isNormalMap"].get_bool().error() == simdjson::SUCCESS) // new param, check if it exists
			loadInfo.isNormalMap = data["properties"]["isNormalMap"].get_bool().value();
		loadInfo.trueResolutionWidth = data["properties"]["trueResolutionWidth"].get_int64().value();
		loadInfo.trueResolutionHeight = data["properties"]["trueResolutionHeight"].get_int64().value();

		AssetID texture2DAsset = asset;
		bool textureExists = textureAssets.find(texture2DAsset) != textureAssets.end();
		if (textureExists) {
			if (!force) {
				return false;
			} else if (force) {
				delete textureAssets.at(texture2DAsset);
			}
		}
		ktxTexture2* ktxTex;
		if (hasTextureBeenCached(asset, loadInfo)) {
			SHARD3D_LOG("Reading cached texture {0}", asset.getAsset());
			ktxTex = TextureImporter::loadCompressedCachedTextureKTX(getTextureCacheLocation(asset).c_str());
		} else {
			ktxTex = TextureImporter::loadTextureKTX(engineDevice, this, loadInfo, getVirtualAssetPath(asset));
			cacheTexture(asset, ktxTex, loadInfo);
		}
		Texture2D* texture = new Texture2D(engineDevice, staticMaterialPool, ktxTex, loadInfo);
		TextureImporter::freeKTX(ktxTex);

		if (!texture) return false;
		SHARD3D_LOG("Loaded texture to resource map '{0}'", texture2DAsset.getAsset());
		textureAssets[texture2DAsset] = std::move(texture);
		return true;
	}


	bool ResourceSystem::loadBakedReflectionCubeAsset(const AssetID& asset, bool force) {
		simdjson::dom::parser parser;
		if (!std::filesystem::exists(asset.getAbsolute())) return false;
		simdjson::padded_string json = simdjson::padded_string::load(asset.getAbsolute());
		const auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			SHARD3D_WARN("Failed to load {0} (ID {1})", asset.getAsset(), asset.getID());
			return false;
		}
		if (data["assetType"].get_string().value() != "baked_lighting") {
			SHARD3D_ERROR("Trying to load non baked_lighting as a baked_lighting asset!");
			return false;
		}
		if (data["bakedType"].get_string().value() != "hdr_cube_reflection") {
			SHARD3D_ERROR("Trying to load non baked hdr_cube_reflection as a baked hdr_cube_reflection asset!");
			return false;
		}
		if (VkFormat format = enum_VkFormat(std::string(data["properties"]["vkFormat"].get_string().value())); format == VK_FORMAT_E5B9G9R9_UFLOAT_PACK32 && ProjectSystem::getEngineSettings().memory.use16bitReflections || format != VK_FORMAT_R16_UINT && ProjectSystem::getEngineSettings().memory.use16bitReflections) {
			SHARD3D_ERROR("Baked asset is incompatible with current engine config! Please regenerate baked asset!");
			return false;
		}
		TextureLoadInfo loadInfo{};
		loadInfo.filter = TextureFilter::Linear;
		loadInfo.format = TextureFormat::FloatingEBGR;
		loadInfo.compression = TextureCompression::Lossless;
		loadInfo.channels = TextureChannels::RGB;
		loadInfo.colorSpace = TextureColorSpace::Linear; // must be linear due to the alpha multiplier
		loadInfo.enableMipMaps = true;

		AssetID textureCubeAsset = asset;
		bool textureExists = textureCubeAssets.find(textureCubeAsset) != textureCubeAssets.end();
		if (textureExists) {
			if (!force) {
				return false;
			} else if (force) {
				delete textureCubeAssets.at(textureCubeAsset);
			}
		}
		ktxTexture2* ktxTex = TextureImporter::loadCompressedCachedTextureKTX(AssetID(std::string(data["assetFile"].get_string().value())).getAbsolute().c_str());
		TextureCube* texture = new TextureCube(engineDevice, staticMaterialPool, ktxTex, loadInfo);
		TextureImporter::freeKTX(ktxTex);
		if (!texture) return false;
		SHARD3D_LOG("Loaded texture cubemap to resource map '{0}'", textureCubeAsset.getAsset());
		textureCubeAssets[textureCubeAsset] = std::move(texture);
		return true;
	}

	bool ResourceSystem::loadTextureCube(const AssetID& asset, bool force) {
		simdjson::dom::parser parser;
		if (!std::filesystem::exists(asset.getAbsolute())) return false;
		simdjson::padded_string json = simdjson::padded_string::load(asset.getAbsolute());
		const auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			SHARD3D_WARN("Failed to load {0} (ID {1})", asset.getAsset(), asset.getID());
			return false;
		}
		if (data["assetType"].get_string().value() != "texturecube") {
			SHARD3D_ERROR("Trying to load non texturecube as a texturecube asset!");
			return false;
		}
		TextureLoadInfo loadInfo{};
		loadInfo.filter = enum_TextureFilter(std::string(data["properties"]["filter"].get_string().value()));
		loadInfo.addressMode = enum_TextureSamplerMode(std::string(data["properties"]["addressMode"].get_string().value()));
		loadInfo.format = enum_TextureFormat(std::string(data["properties"]["format"].get_string().value()));
		loadInfo.compression = enum_TextureCompression(std::string(data["properties"]["compression"].get_string().value()));
		loadInfo.format = enum_TextureFormat(std::string(data["properties"]["format"].get_string().value()));
		loadInfo.channels = enum_TextureChannels(std::string(data["properties"]["channels"].get_string().value()));
		loadInfo.colorSpace = enum_TextureColorSpace(std::string(data["properties"]["colorSpace"].get_string().value()));
		loadInfo.enableMipMaps = false;

		AssetID textureCubeAsset = asset;
		bool textureExists = textureCubeAssets.find(textureCubeAsset) != textureCubeAssets.end();
		if (textureExists) {
			if (!force) {
				return false;
			} else if (force) {
				delete textureCubeAssets.at(textureCubeAsset);
			}
		}
		ktxTexture2* ktxTex;
		if (hasTextureBeenCached(asset, loadInfo)) {
			SHARD3D_LOG("Reading cached texture {0}", asset.getAsset());
			ktxTex = TextureImporter::loadCompressedCachedTextureKTX(getTextureCacheLocation(asset).c_str());
		} else {
			ktxTex = TextureImporter::loadTextureKTX(engineDevice, this, loadInfo, getVirtualAssetPath(asset));
			cacheTexture(asset, ktxTex, loadInfo);
		}
		TextureCube* texture = new TextureCube(engineDevice, staticMaterialPool, ktxTex, loadInfo);
		TextureImporter::freeKTX(ktxTex);
		if (!texture) return false;
		SHARD3D_LOG("Loaded texture cubemap to resource map '{0}'", textureCubeAsset.getAsset());
		textureCubeAssets[textureCubeAsset] = std::move(texture);
		return true;
	}


	bool ResourceSystem::loadMaterial(const AssetID& asset, bool force) {
		bool materialExists = materialAssets.find(asset) != materialAssets.end();
		if (materialExists) {
			if (!force) {
				return true;
			} else if (force) {
				delete materialAssets.at(asset);
			}
		}
		if (!std::filesystem::exists(asset.getAbsolute())) {
			SHARD3D_ERROR("material does not exist!!!");
			return false;
		}
		MaterialClass class_ = Material::discoverMaterialClass(asset);
		Material* material;
		switch (class_) {
		case(MaterialClass::Surface):
			material = new SurfaceMaterial(this);
			break;
		case(MaterialClass::DeferredDecal):
			material = new DecalMaterial(this);
			break;
		case(MaterialClass::Terrain):
			material = new TerrainMaterial(this);
			break;
		}

		material->deserialize(asset.getAbsolute());
		materialAssets[asset] = material;
		rebuildMaterial(material);
		SHARD3D_LOG("Loaded material to resource map '{0}'", asset.getAsset());
		return true;
	}

	bool ResourceSystem::loadMaterialRecursive(const AssetID& asset, bool force) {
		if (loadMaterial(asset, force)) {
			materialAssets[asset]->loadAllTextures();
			return true;
		}

		SHARD3D_ERROR("Surface Material {0} not found! Not loading respective textures...", asset.getAsset());
		return false;
	}

	void ResourceSystem::rebuildMaterial(Material* material) {
		material->loadAllTextures();
		if (!material->isBuilt()) buildMaterial(material);
		rebuildMaterialQueue.emplace(material);
	}

	void ResourceSystem::runtimeUpdateSurfaceMaterial(SurfaceMaterial* material) {
		runtimeUpdateSurfaceMaterialQueue.emplace(material);
	}

	void ResourceSystem::buildMaterial(Material* material) {
		material->createMaterial(engineDevice);
	}

	void ResourceSystem::destroyCoreAssets() {
		unloadTexture(coreAssets.t_errorTexture);
		unloadTexture(coreAssets.t_errorMaterialTexture);
		unloadTexture(coreAssets.t_errorMemoryTexture);
		unloadTexture(coreAssets.t_whiteTexture);
		unloadTexture(coreAssets.t_blackTexture);
		unloadTexture(coreAssets.t_normalTexture);
		unloadTextureCube(coreAssets.c_blankCubemap);
		unloadTextureCube(coreAssets.c_defaultSkybox);
		unloadMaterial(coreAssets.sm_errorMaterial);
		unloadMaterial(coreAssets.sm_blankMaterial);
		unloadMesh(coreAssets.m_defaultModel);
		unloadMesh(coreAssets.m_errorMesh);
		unloadMesh(coreAssets.m_sphere);
		unloadMesh(coreAssets.m_cylinder);
	}

	void ResourceSystem::unloadTexture(const AssetID& asset) {
		destroyResourceMap.push_back(std::make_pair(std::make_pair(AssetType::Texture2D, asset), S3DDestructionObject(engineDevice, currentFence)));
	}
	void ResourceSystem::unloadMesh(const AssetID& asset) {
		destroyResourceMap.push_back(std::make_pair(std::make_pair(AssetType::Model3D, asset), S3DDestructionObject(engineDevice, currentFence)));
	}
	void ResourceSystem::unloadSkeleton(const AssetID& asset) {
		destroyResourceMap.push_back(std::make_pair(std::make_pair(AssetType::SkeletonRig, asset), S3DDestructionObject(engineDevice, currentFence)));
	}
	void ResourceSystem::unloadTextureCube(const AssetID& asset) {
		destroyResourceMap.push_back(std::make_pair(std::make_pair(AssetType::TextureCube, asset), S3DDestructionObject(engineDevice, currentFence)));
	}
	void ResourceSystem::unloadMaterial(const AssetID& asset) {
		destroyResourceMap.push_back(std::make_pair(std::make_pair(AssetType::Material, asset), S3DDestructionObject(engineDevice, currentFence)));
	}

	Texture2D* ResourceSystem::retrieveTexture_unsafe(const AssetID& asset) {
		resourceOccupationState[asset] = true;
		Resource* res = textureAssets.at(asset);
		return reinterpret_cast<Texture2D*>(res);
	}
	Texture2D* ResourceSystem::retrieveTexture_safe(const AssetID& asset) {
		if (textureAssets.find(asset) != textureAssets.end())
			return retrieveTexture_unsafe(asset);
		return textureAssets.at(coreAssets.t_errorTexture);
	}
	Mesh3D* ResourceSystem::retrieveMesh_unsafe(const AssetID& asset) {
		resourceOccupationState[asset] = true;
		Resource* res = meshAssets.at(asset);
		return reinterpret_cast<Mesh3D*>(res);
	}
	Mesh3D* ResourceSystem::retrieveMesh_safe(const AssetID& asset) {
		if (meshAssets.find(asset) != meshAssets.end())
			return retrieveMesh_unsafe(asset);
		return meshAssets.at(coreAssets.m_errorMesh);
	}
	Skeleton* ResourceSystem::retrieveSkeleton_unsafe(const AssetID& asset) {
		resourceOccupationState[asset] = true;
		Resource* res = skeletonAssets.at(asset);
		return reinterpret_cast<Skeleton*>(res);
	}
	Skeleton* ResourceSystem::retrieveSkeleton_safe(const AssetID& asset) {
		if (skeletonAssets.find(asset) != skeletonAssets.end())
			return retrieveSkeleton_unsafe(asset);
		return skeletonAssets.at(coreAssets.m_errorMesh);
	}
	TextureCube* ResourceSystem::retrieveTextureCube_unsafe(const AssetID& asset) {
		resourceOccupationState[asset] = true;
		Resource* res = textureCubeAssets.at(asset);
		return reinterpret_cast<TextureCube*>(res);
	}
	TextureCube* ResourceSystem::retrieveTextureCube_safe(const AssetID& asset) {
		if (textureCubeAssets.find(asset) != textureCubeAssets.end())
			return retrieveTextureCube_unsafe(asset);
		return textureCubeAssets.at(coreAssets.c_blankCubemap);
	}
	Material* ResourceSystem::retrieveMaterial_unsafe(const AssetID& asset) {
		resourceOccupationState[asset] = true;
		Resource* res = materialAssets.at(asset);
		Material* surface = reinterpret_cast<Material*>(res);


		for (auto& tx : surface->getAllTextures())
			resourceOccupationState[tx] = true;
		return surface;
	}
	Material* ResourceSystem::retrieveMaterial_safe(const AssetID& asset) {
		if (materialAssets.find(asset) != materialAssets.end())
			return retrieveMaterial_unsafe(asset);
		return materialAssets.at(coreAssets.sm_errorMaterial);
	}

}