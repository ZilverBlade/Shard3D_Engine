#include "project_system.h"
#include "../../utils/json_ext.h"
#include "../../utils/logger.h"
#include "material_system.h"
#include <fstream>
#include <codecvt>
#include <locale>
#include <ShlObj_core.h>

#define getShaderDefineString(classification, value) std::string("S3DSDEF_SHADER_PERMUTATION_"#classification"_") + value 
#define getShaderDefineStringD(classification) std::string("S3DSDEF_SHADER_PERMUTATION_"#classification)
#define getShaderDefineValue(define, value) {std::string("S3DSDEF_SHADER_VALUE_"#define), value}

namespace Shard3D {
	void ProjectSystem::init(const std::string& project_file, bool editorMode) {
		editorModeOn = editorMode;
		projectLocation = project_file.substr(0, project_file.find_last_of("/") + 1);
		projectAsset = project_file;
		SHARD3D_INFO("Loading project: {0} (located in {1})", project_file, projectLocation);

		simdjson::dom::parser parser;
		simdjson::padded_string json = simdjson::padded_string::load(project_file);
		const auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			SHARD3D_FATAL("Failed to load project!");
		}
		if (data["project"].error() != simdjson::SUCCESS)
			SHARD3D_FATAL("Invalid project file!! Aborting...");
		gameName = data["properties"]["gameName"].get_string().value();
		engineSettingsLocation = projectLocation + std::string(data["properties"]["engineConfigFile"].get_string().value());
		assetLocation = projectLocation + std::string(data["properties"]["assetPath"].get_string().value())+ "/";
		cacheLocation = assetLocation + "cache/";

		SHARD3D_INFO("Game name: {0}", gameName);
		SHARD3D_INFO("Engine settings located in: {0}", engineSettingsLocation);
		SHARD3D_INFO("Graphics settings located in: {0}", graphicsSettingsLocation);
		SHARD3D_INFO("Assets located in: {0}", assetLocation);

		{
			std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;
			PWSTR pbuff{};
			SHGetKnownFolderPath(FOLDERID_RoamingAppData, 0, 0, &pbuff);
			std::wstring appdataLoc = reinterpret_cast<wchar_t*>(pbuff);
			std::wstring engineAppdataLoc = appdataLoc + L"/Shard3D/V" + ENGINE_VERSION.getMajorMinorWP();
			if (!std::filesystem::exists(engineAppdataLoc)) {
				std::filesystem::create_directories(engineAppdataLoc);
			}
			std::wstring gameAppdataLoc = appdataLoc + L"/Shard3D/V" + ENGINE_VERSION.getMajorMinorWP() + L"/Games/" + converter.from_bytes(gameName);
			if (!std::filesystem::exists(gameAppdataLoc)) {
				std::filesystem::create_directories(gameAppdataLoc);
			}
			engineAppDataLocation = converter.to_bytes(engineAppdataLoc);
			gameAppDataLocation = converter.to_bytes(gameAppdataLoc);
			CoTaskMemFree(pbuff);
		}
		graphicsSettingsLocation = gameAppDataLocation + "/graphics_settings.ini";
		if (!std::filesystem::exists(engineSettingsLocation)) {
			saveEngineSettings(engineSettingsLocation);
		}
		if (!std::filesystem::exists(graphicsSettingsLocation)) {
			saveGraphicsSettings(graphicsSettingsLocation);
		}
		loadEngineSettings(engineSettingsLocation);
		loadGraphicsSettings(graphicsSettingsLocation);
		wasInit = true;
	}
	void ProjectSystem::stop() {
	}
	void ProjectSystem::loadEngineSettings(const std::string& engine_settings_path) {
		CSimpleIniA ini{};
		ini.SetUnicode();
		ini.LoadFile(engine_settings_path.c_str());

		shadingQualityLevels.clear();
		globalShaderDefines.clear();
		shadingQualityLevels[0] = {}; // initialize LSD 0

		engineSettings.cpu.maximumAllowedFrameTimeMilliseconds = ini.GetLongValue("CPU", "maximumAllowedFrameTimeMilliseconds");
		engineSettings.physics.updateRate = ini.GetLongValue("PHYSICS", "updateRate");
		engineSettings.physics.useQuickStep = ini.GetBoolValue("PHYSICS", "useQuickStep");
		
		engineSettings.window.resizable = ini.GetBoolValue("WINDOW", "resizable");
		engineSettings.window.defaultWidth = ini.GetLongValue("WINDOW", "defaultWidth");
		engineSettings.window.defaultHeight = ini.GetLongValue("WINDOW", "defaultHeight");

		engineSettings.renderer.enableDoubleSidedMaterials = ini.GetBoolValue("RENDERER", "enableDoubleSidedMaterials");
		engineSettings.renderer.enableDeferredDecals = ini.GetBoolValue("RENDERER", "enableDeferredDecals");
		engineSettings.renderer.allowFragmentShadingRateEXT = ini.GetBoolValue("RENDERER", "allowFragmentShadingRateEXT");
		engineSettings.renderer.allowRayTracingUsage = ini.GetBoolValue("RENDERER", "allowRayTracingUsage");

		engineSettings.renderer.earlyZMode = (ESET::EarlyDepthMode)ini.GetLongValue("RENDERER", "earlyDepthPassMode");
		if (engineSettings.renderer.earlyZMode == ESET::EarlyDepthMode::OpaqueAndDiscardMasks) {
			globalShaderDefines.push_back(getShaderDefineStringD(EarlyZDiscard));
		}
		engineSettings.renderer.depthBufferFormat = (ESET::DepthBufferFormat)ini.GetLongValue("RENDERER", "depthBufferFormat");
		switch (engineSettings.renderer.depthBufferFormat) {
		case (ESET::DepthBufferFormat::Log16Bit): globalShaderDefines.push_back(getShaderDefineString(DepthBufferFormat, "L16")); break;
		case (ESET::DepthBufferFormat::StdF32Bit): globalShaderDefines.push_back(getShaderDefineString(DepthBufferFormat, "F32")); break;
		case (ESET::DepthBufferFormat::ReverseF32Bit): globalShaderDefines.push_back(getShaderDefineString(DepthBufferFormat, "F32R")); break;
		}
		engineSettings.renderer.infiniteFarZ = ini.GetBoolValue("RENDERER", "infiniteFarZ");
		if (engineSettings.renderer.infiniteFarZ && engineSettings.renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit) {
			globalShaderDefines.push_back("S3DSDEF_SHADER_PERMUTATION_INFINITE_FAR_Z");
		}
		engineSettings.renderer.rendererMode = (ESET::RendererMode)ini.GetLongValue("RENDERER", "rendererMode");

		engineSettings.postfx.enableBloom = ini.GetBoolValue("POSTFX", "enableBloom");
		engineSettings.postfx.fastBloom = ini.GetBoolValue("POSTFX", "fastBloom");
		engineSettings.postfx.enableSSAO = ini.GetBoolValue("POSTFX", "enableSSAO");
		engineSettings.postfx.enableMotionBlur = ini.GetBoolValue("POSTFX", "enableMotionBlur");
		if (engineSettings.postfx.enableMotionBlur) {
			globalShaderDefines.push_back("S3DSDEF_SHADER_PERMUTATION_EnableMotionBlur");
		}

		engineSettings.memory.enableVertexTangents = ini.GetBoolValue("MEMORY", "enableVertexTangents");
		if (engineSettings.memory.enableVertexTangents) globalShaderDefines.push_back(getShaderDefineStringD(EnableVertexTangents));

		//engineSettings.memory.use16bitReflections = ini.GetBoolValue("MEMORY", "use16bitReflections");

		engineSettings.memory.arenaAllocatorDefaultVertexSize = ini.GetLongValue("MEMORY", "arenaAllocatorDefaultVertexSize");
		engineSettings.memory.arenaAllocatorCapacityGrowMultiplier = ini.GetDoubleValue("MEMORY", "arenaAllocatorCapacityGrowMultiplier");
		engineSettings.memory.arenaAllocatorCapacityShrinkThreshold = ini.GetDoubleValue("MEMORY", "arenaAllocatorCapacityShrinkThreshold");
		engineSettings.memory.memoryAllocatorSizeBytes = ini.GetLongValue("MEMORY", "memoryAllocatorSizeBytes");
		engineSettings.memory.memoryAllocatorBlockSizeBytes = ini.GetLongValue("MEMORY", "memoryAllocatorBlockSizeBytes");
		
		engineSettings.shading.renderTransparentShadows = (ESET::TransparentShadows)ini.GetLongValue("SHADING", "renderTransparentShadows");
		switch (engineSettings.shading.renderTransparentShadows) {
		case (ESET::TransparentShadows::DontRender): globalShaderDefines.push_back(getShaderDefineString(TransparentShadows, "DontRender")); break;
		case (ESET::TransparentShadows::RenderOpaque): globalShaderDefines.push_back(getShaderDefineString(TransparentShadows, "RenderOpaque")); break;
		case (ESET::TransparentShadows::RenderTransparentGrayScale): globalShaderDefines.push_back(getShaderDefineString(TransparentShadows, "RenderTransparentGrayScale")); break;
		case (ESET::TransparentShadows::RenderTransparentColored): globalShaderDefines.push_back(getShaderDefineString(TransparentShadows, "RenderTransparentColored")); break;
		}



		engineSettings.shading.renderMaskedShadows = (ESET::MaskedShadows)ini.GetLongValue("SHADING", "renderMaskedShadows");
		switch (engineSettings.shading.renderMaskedShadows) {
		case (ESET::MaskedShadows::RenderOpaque): globalShaderDefines.push_back(getShaderDefineString(MaskedShadows, "RenderOpaque")); break;
		case (ESET::MaskedShadows::RenderMasked): globalShaderDefines.push_back(getShaderDefineString(MaskedShadows, "RenderMasked")); break;
		}

		engineSettings.shading.specularModel = (ESET::SpecularModel)ini.GetLongValue("SHADING", "specularModel");
		switch (engineSettings.shading.specularModel) {
		case (ESET::SpecularModel::Phong): globalShaderDefines.push_back(getShaderDefineString(SpecularModel, "Phong")); break;
		case (ESET::SpecularModel::BlinnPhong): globalShaderDefines.push_back(getShaderDefineString(SpecularModel, "BlinnPhong")); break;
		case (ESET::SpecularModel::Gaussian): globalShaderDefines.push_back(getShaderDefineString(SpecularModel, "Gaussian")); break;
		}

		engineSettings.shading.specularAffectsReflectionIntensity = ini.GetBoolValue("SHADING", "specularAffectsReflectionIntensity");
		if (engineSettings.shading.specularAffectsReflectionIntensity) globalShaderDefines.push_back("S3DSDEF_SHADER_PERMUTATION_SpecularAffectsReflectionIntensity");

		engineSettings.shading.directionalShadowMapType = (ESET::ShadowMap)ini.GetLongValue("SHADING", "directionalShadowMapType");
		switch (engineSettings.shading.directionalShadowMapType) {
		case (ESET::ShadowMap::PCF): globalShaderDefines.push_back(getShaderDefineString(DirectionalShadowMapType, "PCF")); break;
		case (ESET::ShadowMap::Variance): globalShaderDefines.push_back(getShaderDefineString(DirectionalShadowMapType, "Variance")); break;
		}
		engineSettings.shading.spotShadowMapType = (ESET::ShadowMap)ini.GetLongValue("SHADING", "spotShadowMapType");
		switch (engineSettings.shading.spotShadowMapType) {
		case (ESET::ShadowMap::PCF): globalShaderDefines.push_back(getShaderDefineString(SpotShadowMapType, "PCF")); break;
		case (ESET::ShadowMap::Variance): globalShaderDefines.push_back(getShaderDefineString(SpotShadowMapType, "Variance")); break;
		}		
		engineSettings.shading.pointShadowMapType = (ESET::ShadowMap)ini.GetLongValue("SHADING", "pointShadowMapType");
		switch (engineSettings.shading.pointShadowMapType) {
		case (ESET::ShadowMap::PCF): globalShaderDefines.push_back(getShaderDefineString(PointShadowMapType, "PCF")); break;
		case (ESET::ShadowMap::Variance): globalShaderDefines.push_back(getShaderDefineString(PointShadowMapType, "Variance")); break;
		}

		engineSettings.allowedMaterialPermutations = {
			SurfaceMaterialPipelineClassPermutationOptions_Opaque | SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling,
			SurfaceMaterialPipelineClassPermutationOptions_Translucent | SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling,
			SurfaceMaterialPipelineClassPermutationOptions_Opaque | SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates,
			SurfaceMaterialPipelineClassPermutationOptions_Translucent | SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates,
			SurfaceMaterialPipelineClassPermutationOptions_Opaque | SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms,
			SurfaceMaterialPipelineClassPermutationOptions_Translucent | SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms,
			SurfaceMaterialPipelineClassPermutationOptions_Opaque | SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms,
			SurfaceMaterialPipelineClassPermutationOptions_Translucent | SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms,
		};
		if (engineSettings.renderer.enableDoubleSidedMaterials) {
			engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_Opaque);
			engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_Opaque | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates);
			engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_Translucent);
			engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_Translucent | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates);
			engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_Opaque | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms);
			engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_Opaque | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms);
			engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_Translucent | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms);
			engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_Translucent | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms);
		}
		if (engineSettings.renderer.enableAlphaTest) {
			engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_AlphaTest | SurfaceMaterialPipelineClassPermutationOptions_Opaque | SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling);
			engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_AlphaTest | SurfaceMaterialPipelineClassPermutationOptions_Translucent | SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling);
			engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_AlphaTest | SurfaceMaterialPipelineClassPermutationOptions_Opaque | SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates);
			engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_AlphaTest | SurfaceMaterialPipelineClassPermutationOptions_Translucent | SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates); 
			engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_AlphaTest | SurfaceMaterialPipelineClassPermutationOptions_Opaque | SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms);
			engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_AlphaTest | SurfaceMaterialPipelineClassPermutationOptions_Translucent | SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms);
			engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_AlphaTest | SurfaceMaterialPipelineClassPermutationOptions_Opaque | SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms);
			engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_AlphaTest | SurfaceMaterialPipelineClassPermutationOptions_Translucent | SurfaceMaterialPipelineClassPermutationOptions_BackFaceCulling | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms);

			if (engineSettings.renderer.enableDoubleSidedMaterials) {
				engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_AlphaTest | SurfaceMaterialPipelineClassPermutationOptions_Opaque);
				engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_AlphaTest | SurfaceMaterialPipelineClassPermutationOptions_Opaque | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates);
				engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_AlphaTest | SurfaceMaterialPipelineClassPermutationOptions_Translucent);
				engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_AlphaTest | SurfaceMaterialPipelineClassPermutationOptions_Translucent | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates);
				engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_AlphaTest | SurfaceMaterialPipelineClassPermutationOptions_Opaque | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms);
				engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_AlphaTest | SurfaceMaterialPipelineClassPermutationOptions_Opaque | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms);
				engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_AlphaTest | SurfaceMaterialPipelineClassPermutationOptions_Translucent | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms);
				engineSettings.allowedMaterialPermutations.push_back(SurfaceMaterialPipelineClassPermutationOptions_AlphaTest | SurfaceMaterialPipelineClassPermutationOptions_Translucent | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicUpdates | SurfaceMaterialPipelineClassPermutationOptions_ExplicitEnableDynamicVertexTransforms);
			}
		}
	}
	void ProjectSystem::loadGraphicsSettings(const std::string& graphics_settings_path) {
		CSimpleIniA ini{};
		ini.SetUnicode();
		ini.LoadFile(graphics_settings_path.c_str());

		graphicsSettings.renderer.frameRateLimit = ini.GetLongValue("RENDERER", "frameRateLimit");
		graphicsSettings.renderer.verticalSync = ini.GetBoolValue("RENDERER", "verticalSync");
		graphicsSettings.renderer.framesInFlight = ini.GetLongValue("RENDERER", "framesInFlight");
		graphicsSettings.renderer.msaa = ini.GetBoolValue("RENDERER", "msaa");
		graphicsSettings.renderer.msaaSamples = ini.GetLongValue("RENDERER", "msaaSamples");
		graphicsSettings.renderer.parallaxMapping = ini.GetBoolValue("RENDERER", "parallaxMapping");
		graphicsSettings.display.gamma = ini.GetDoubleValue("DISPLAY", "gamma");

		graphicsSettings.texture.anisotropy = ini.GetDoubleValue("TEXTURE", "anisotropy");
		graphicsSettings.texture.mipMapping = ini.GetBoolValue("TEXTURE", "mipMapping");

		graphicsSettings.shadows.directional = ini.GetBoolValue("SHADOWS", "directional");
		graphicsSettings.shadows.spot = ini.GetBoolValue("SHADOWS", "spot");
		graphicsSettings.shadows.point = ini.GetBoolValue("SHADOWS", "point");
		graphicsSettings.shadows.directionalResolutionMultiplier = ini.GetDoubleValue("SHADOWS", "directionalResolutionMultiplier");
		graphicsSettings.shadows.spotResolutionMultiplier = ini.GetDoubleValue("SHADOWS", "spotResolutionMultiplier");
		graphicsSettings.shadows.pointResolutionMultiplier = ini.GetDoubleValue("SHADOWS", "pointResolutionMultiplier");
		graphicsSettings.shadows.varianceDirectionalBlurQuality = ini.GetLongValue("SHADOWS", "varianceDirectionalBlurQuality");
		graphicsSettings.shadows.varianceSpotBlurQuality = ini.GetLongValue("SHADOWS", "varianceSpotBlurQuality");
		graphicsSettings.shadows.variancePointBlurQuality = ini.GetLongValue("SHADOWS", "variancePointBlurQuality");
		graphicsSettings.shadows.varianceDirectionalMultiSamples = ini.GetLongValue("SHADOWS", "varianceDirectionalMultiSamples");
		graphicsSettings.shadows.varianceSpotMultiSamples = ini.GetLongValue("SHADOWS", "varianceSpotMultiSamples");
		graphicsSettings.shadows.variancePointMultiSamples = ini.GetLongValue("SHADOWS", "variancePointMultiSamples");

		graphicsSettings.postfx.ssao.quality = ini.GetDoubleValue("POSTFX", "ssaoQuality");
		graphicsSettings.postfx.ssao.kernelQuality = ini.GetLongValue("POSTFX", "ssaoKernelQuality");
		graphicsSettings.postfx.bloom.bloomQuality = ini.GetLongValue("POSTFX", "bloomQuality");
		graphicsSettings.postfx.motionBlur.samples = ini.GetLongValue("POSTFX", "motionBlurSamples");

		graphicsSettings.postfx.aa.fxaa = ini.GetBoolValue("POSTFX", "fxaa");
		graphicsSettings.postfx.aa.fxaaSoftness = ini.GetDoubleValue("POSTFX", "fxaaSoftness");

	}
	void ProjectSystem::saveEngineSettings(const std::string& engine_settings_path) {
		CSimpleIniA ini{};
		ini.SetUnicode();

		//ini.SaveFile(engineSettingsLocation.c_str());
	}
	void ProjectSystem::saveGraphicsSettings(const std::string& graphics_settings_path) {
		CSimpleIniA ini{};
		ini.SetUnicode();

		ini.SetLongValue("RENDERER", "frameRateLimit", graphicsSettings.renderer.frameRateLimit);
		ini.SetBoolValue("RENDERER", "verticalSync", graphicsSettings.renderer.verticalSync );
		ini.SetLongValue("RENDERER", "framesInFlight", graphicsSettings.renderer.framesInFlight);
		ini.SetBoolValue("RENDERER", "msaa", graphicsSettings.renderer.msaa);
		ini.SetLongValue("RENDERER", "msaaSamples", graphicsSettings.renderer.msaaSamples);
		ini.SetBoolValue("RENDERER", "parallaxMapping", graphicsSettings.renderer.parallaxMapping);
		ini.SetDoubleValue("DISPLAY", "gamma", graphicsSettings.display.gamma);

		ini.SetDoubleValue("TEXTURE", "anisotropy", graphicsSettings.texture.anisotropy);
		ini.SetBoolValue("TEXTURE", "mipMapping", graphicsSettings.texture.mipMapping);

		ini.SetBoolValue("SHADOWS", "directional", graphicsSettings.shadows.directional);
		ini.SetBoolValue("SHADOWS", "spot", graphicsSettings.shadows.spot);
		ini.SetBoolValue("SHADOWS", "point", graphicsSettings.shadows.point);
		ini.SetDoubleValue("SHADOWS", "directionalResolutionMultiplier", graphicsSettings.shadows.directionalResolutionMultiplier);
		ini.SetDoubleValue("SHADOWS", "spotResolutionMultiplier", graphicsSettings.shadows.spotResolutionMultiplier );
		ini.SetDoubleValue("SHADOWS", "pointResolutionMultiplier", graphicsSettings.shadows.pointResolutionMultiplier );
		ini.SetLongValue("SHADOWS", "varianceDirectionalBlurQuality", graphicsSettings.shadows.varianceDirectionalBlurQuality );
		ini.SetLongValue("SHADOWS", "varianceSpotBlurQuality", graphicsSettings.shadows.varianceSpotBlurQuality );
		ini.SetLongValue("SHADOWS", "variancePointBlurQuality", graphicsSettings.shadows.variancePointBlurQuality);
		ini.SetLongValue("SHADOWS", "varianceDirectionalMultiSamples", graphicsSettings.shadows.varianceDirectionalMultiSamples);
		ini.SetLongValue("SHADOWS", "varianceSpotMultiSamples", graphicsSettings.shadows.varianceSpotMultiSamples);
		ini.SetLongValue("SHADOWS", "variancePointMultiSamples", graphicsSettings.shadows.variancePointMultiSamples);

		ini.SetDoubleValue("POSTFX", "ssaoQuality", graphicsSettings.postfx.ssao.quality);
		ini.SetLongValue("POSTFX", "ssaoKernelQuality", graphicsSettings.postfx.ssao.kernelQuality);
		ini.SetLongValue("POSTFX", "bloomQuality", graphicsSettings.postfx.bloom.bloomQuality);
		ini.SetLongValue("POSTFX", "motionBlurSamples", graphicsSettings.postfx.motionBlur.samples);
		ini.SetBoolValue("POSTFX", "fxaa", graphicsSettings.postfx.aa.fxaa);
		ini.SetDoubleValue("POSTFX", "fxaaSoftness", graphicsSettings.postfx.aa.fxaaSoftness);
		ini.SaveFile(graphics_settings_path.c_str());
	}
}