#include "graphics_settings_manager.h"

namespace Shard3D {
	void GraphicsSettingsManager::cmdChangeDisplay(char* setting, void* pVal) {
		for (auto& func : cmdChangeCallbackDisplays) {
			func(setting, pVal);
		}
	}
	void GraphicsSettingsManager::cmdChangeRenderer(char* setting, void* pVal) {
		for (auto& func : cmdChangeCallbackRenderers) {
			func(setting, pVal);
		}
	}
	void GraphicsSettingsManager::cmdChangeTexture(char* setting, void* pVal) {
		for (auto& func : cmdChangeCallbackTextures) {
			func(setting, pVal);
		}
	}
	void GraphicsSettingsManager::cmdChangePostFX(char* setting, void* pVal) {
		for (auto& func : cmdChangeCallbackPostFXs) {
			func(setting, pVal);
		}
	}
	void GraphicsSettingsManager::cmdChangeShadow(char* setting, void* pVal) {
		for (auto& func : cmdChangeCallbackShadows) {
			func(setting, pVal);
		}
	}
}