#pragma once
#include "../../s3dstd.h"
#include "../../core/misc/engine_settings.h"
#include "../../core/misc/graphics_settings.h"
namespace Shard3D {
	class ProjectSystem {
	public:
		static void init(const std::string& project_file, bool editorMode);
		static void stop();
		static bool initialized() { return wasInit; }
		const static inline std::string& getGameName() { return gameName; }

		static inline EngineSettings& getEngineSettings() { return engineSettings; }
		static inline GraphicsSettings& getGraphicsSettings() { return graphicsSettings; }
		// returns absolute location of the project folder
		const static inline std::string& getProjectLocation() { return projectLocation; }
		const static inline std::string& getProjectAsset() { return projectAsset; }
		// returns absolute location of the asset folder
		const static inline std::string& getAssetLocation() { return assetLocation; }
		const static inline std::string& getCacheLocation() { return cacheLocation; }
		const static inline std::vector<std::string>& getShaderPermutationDefinitions() { return globalShaderDefines; }
		const static inline std::string& getEngineAppDataLoc() {
			return engineAppDataLocation;
		}
		const static inline std::string& getGameAppDataLoc() {
			return gameAppDataLocation;
		}
		const static inline bool isEditorMode() {
			return editorModeOn;
		}
	private:
		static inline EngineSettings engineSettings{};
		static inline std::string gameName{};
		static inline std::map<uint32_t, ShadingSettings> shadingQualityLevels{};
		static inline std::vector<std::string> globalShaderDefines;
		static inline std::vector<std::pair<std::string, int>> globalShaderValues;

		static inline GraphicsSettings graphicsSettings{};

		static inline std::string projectLocation;
		static inline std::string projectAsset;
		static inline std::string engineSettingsLocation;
		static inline std::string graphicsSettingsLocation;
		static inline std::string assetLocation;
		static inline std::string cacheLocation;
			
		static inline std::string engineAppDataLocation;
		static inline std::string gameAppDataLocation;
		
		static inline bool wasInit = false;

		static inline bool editorModeOn = false;
		static void loadEngineSettings(const std::string& engine_settings_path);
		static void loadGraphicsSettings(const std::string& graphics_settings_path);
		static void saveEngineSettings(const std::string& engine_settings_path);
		static void saveGraphicsSettings(const std::string& graphics_settings_path);
	};
}