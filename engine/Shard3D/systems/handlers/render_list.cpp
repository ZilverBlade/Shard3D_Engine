#include "render_list.h"
#include "../../core/asset/assetmgr.h"
#include "../../core/ecs/actor.h"
#include <fstream>
#include "../computational/shader_system.h"
#include "vertex_arena_allocator.h"
#include "../../core/misc/engine_settings.h"
#include "../../core.h"
#include "material_system.h"
#define GLM_FORCE_QUAT_DATA_WXYZ
#include <glm/gtx/quaternion.hpp>
namespace Shard3D {
	inline static MeshMobilityOptionsFlags mobilityToBits(uint32_t mob) {
		return mob * 2 | uint32_t(mob == 0x0) * 1;
	}

	struct DynamicActorInfo {
		glm::mat4 modelMatrix;
		glm::mat3x4 normalMatrix;
		glm::mat4 modelMatrixPF;
	};

	struct RiggedMeshData {
		uint32_t indexOffset;
		VkBool32 hasSkeleton;
		VkBool32 hasMorphTarget;

		ShaderResourceIndex skeletonBoneIDBuffer;
		ShaderResourceIndex skeletonBoneWeightBuffer;
		ShaderResourceIndex morphTargetBuffer;

		uint32_t morphTargetCount;
		uint32_t align;
		AlignedType<float, 16> morphTargetWeights[16];
		std::vector<std::pair<glm::mat4, glm::mat3x4>> boneMatrixData;
	};

	RenderList::RenderList(const std::vector<SurfaceMaterialPipelineClassPermutationFlags>& shaders, uPtr<S3DDescriptorSetLayout>& skeletonDescriptorSetLayout, ResourceSystem* rs) 
		: resourceSystem(rs), skeletonDescriptorSetLayout(skeletonDescriptorSetLayout) {
		bool riglist[2]{
			false,
			true
		};
		uint32_t mblist[3]{
			0x1,
			0x2,
			0x4
		};
		for (bool riggable : riglist) {
			materialRendering[riggable] = {};
			for (uint32_t mobility : mblist) {
				materialRendering[riggable][mobility] = {};
				for (uint32_t permutation : shaders) {
					materialRendering[riggable][mobility][permutation] = {};
					allowedShaders.push_back(permutation);
				}
			}
		}
	}

	RenderList::~RenderList() {
		vkDeviceWaitIdle(resourceSystem->engineDevice.device());
		for (auto& [actor, info] : meshInfos) {
			for (int i = 0; i < info.actorBufferFrames.size(); i++) {
				resourceSystem->getDescriptor()->freeIndex(BINDLESS_SSBO_BINDING, info.actorBufferIndices[i]);
				delete info.actorBufferFrames[i];
			}
		}
		for (auto& [actor, info] : skeletalInfos) {
			resourceSystem->engineDevice.staticMaterialPool->freeDescriptors(info.actorBufferDescriptors);
			for (int i = 0; i < info.actorBufferFrames.size(); i++) {
				delete info.actorBufferFrames[i];
			}
		}
		for (auto& [pair, destruct] : destroyBufferQueue) {
			delete pair.first;
			resourceSystem->getDescriptor()->freeIndex(BINDLESS_SSBO_BINDING, pair.second);
		}
	}

	void RenderList::createActorBufferData(Actor actor) {
		for (int i = 0; i < resourceSystem->maxFramesInFlight; i++) {
			meshInfos.at(actor).actorBufferFrames.push_back(
				new S3DBuffer(resourceSystem->engineDevice, sizeof(DynamicActorInfo), 1, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
			);
			meshInfos.at(actor).actorBufferFrames[i]->map();
			meshInfos.at(actor).actorBufferIndices.push_back(resourceSystem->getDescriptor()->allocateIndex(BINDLESS_SSBO_BINDING));
			BindlessDescriptorWriter(*resourceSystem->getDescriptor())
				.writeBuffer(BINDLESS_SSBO_BINDING, meshInfos.at(actor).actorBufferIndices[i], &meshInfos.at(actor).actorBufferFrames[i]->descriptorInfo())
				.build();
		}
	}
	void RenderList::createActorBufferSkeleton(Actor actor) {
		skeletalInfos[actor] = {};
		skeletalInfos[actor].actorBufferDescriptors.resize(resourceSystem->maxFramesInFlight);

		auto& skeleton = actor.getComponent<Components::SkeletonRigComponent>();
		for (int i = 0; i < resourceSystem->maxFramesInFlight; i++) {
			skeletalInfos[actor].actorBufferFrames.push_back(
				new S3DBuffer(resourceSystem->engineDevice, sizeof(RiggedMeshData) - sizeof(RiggedMeshData::boneMatrixData) + (sizeof(glm::mat4) + sizeof(glm::mat3x4)) * skeleton.boneTransforms.size(), 1, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
			);
			skeletalInfos[actor].actorBufferFrames[i]->map();

			// TODO: no skeleton
			S3DDescriptorWriter(*skeletonDescriptorSetLayout, *resourceSystem->engineDevice.staticMaterialPool)
				.writeBuffer(0, &skeletalInfos[actor].actorBufferFrames[i]->descriptorInfo())
				.build(skeletalInfos[actor].actorBufferDescriptors[i]);
		}
	}

	void RenderList::updateRenderList(sPtr<Level>& level) {
		auto view = level->registry.view<Components::Mesh3DComponent, Components::IsVisibileComponent>();
		std::vector<entt::entity> presentActors;
		presentActors.reserve(view.size_hint());

		for (auto obj : view) {
			presentActors.push_back(obj);
			Actor actor = { obj, level.get() };
			auto iter = meshInfos.find(actor); 
			if (iter == meshInfos.end()) {
				addActorToRenderList(actor);
				continue;
			}
			if (actor.nocheck_getComponent<Components::Mesh3DComponent>().dirty == true) {
				actor.nocheck_getComponent<Components::Mesh3DComponent>().dirty = false;
				updateActor(actor);
				continue;
			}

			auto& actorRenderData = iter->second;
			if (ProjectSystem::isEditorMode()) {
				if (Mobility mobility = actor.getMobility(); actorRenderData.mobility != mobility) {
					modifyActorMobility(actor, mobilityToBits(static_cast<uint32_t>(iter->second.mobility)), mobilityToBits(static_cast<uint32_t>(mobility)));
				}
			}
			if (ProjectSystem::isEditorMode() || actor.getMobility() == Mobility::Dynamic) {
				updateActorAABBs(actorRenderData);
				updateActorCastsShadows(actor, actor.nocheck_getComponent<Components::Mesh3DComponent>().castShadows);
				updateActorReceivesDecals(actor, actor.nocheck_getComponent<Components::Mesh3DComponent>().receiveDecals);
			}
		}

		std::vector<entt::entity> destroy;
		destroy.reserve(presentActors.size());
		for (const auto& [actor, info] : meshInfos) {
			if (std::find(presentActors.begin(), presentActors.end(), actor) == presentActors.end()) {
				destroy.push_back(actor);
			}
		}
		for (entt::entity actor : destroy) {
			cleanUpActorFromRenderList({ actor , level .get()});
		}
	}

	void RenderList::addActorToRenderList(Actor actor) {
		Mesh3DRenderInfo meshInfo{};
		auto& mc = actor.nocheck_getComponent<Components::Mesh3DComponent>();
		auto& tc = actor.getTransform();
		meshInfo.materials = mc.getMaterials();
		meshInfo.mesh = mc.asset;
		meshInfo.normal = tc.normalMatrix;
		meshInfo.transform = tc.transformMatrix;
		meshInfo.castShadows = mc.castShadows;
		meshInfo.receiveDecals = mc.receiveDecals;
		meshInfo.cullInfo.sphereBounds = resourceSystem->retrieveMesh(mc.asset)->sphereBounds;
		glm::vec3 scal = tc.getScaleWorld();
		meshInfo.cullInfo.maxScale = glm::max(scal.x, glm::max(scal.y, scal.z));
		updateActorAABBs(meshInfo);
		meshInfo.mobility = actor.getMobility();
		meshInfos[actor] = meshInfo;
		Mobility mobility = actor.getMobility();
		if (mobility == Mobility::Dynamic) {
			createActorBufferData(actor);
		}
		bool rigged = actor.hasComponent<Components::SkeletonRigComponent>();
		if (rigged) {
			createActorBufferSkeleton(actor);
		}
		for (SurfaceMaterialPipelineClassPermutationFlags class_ : allowedShaders) {
			addToSurfaceMaterialRenderingList(actor, rigged, mobilityToBits(static_cast<uint32_t>(mobility)), class_, buildRenderInfoFromActor(actor, class_));
		}
	}
	void RenderList::rmvActorFromRenderList(Actor actor) {
		uint32_t mob = mobilityToBits(static_cast<uint32_t>(actor.getMobility()));
		bool rigged = actor.hasComponent<Components::SkeletonRigComponent>();
		for (SurfaceMaterialPipelineClassPermutationFlags class_ : getRenderUsingClasses(actor, rigged, mob)) {
			rmvFromSurfaceMaterialRenderingList(actor, rigged, mob, class_);
		}
		if (meshInfos.at(actor).actorBufferFrames.size() != 0) {
			for (int i = 0; i < meshInfos.at(actor).actorBufferFrames.size(); i++) {
				destroyBufferQueue.push_back({ { meshInfos.at(actor).actorBufferFrames[i], meshInfos.at(actor).actorBufferIndices[i] }, {resourceSystem->engineDevice, currentFence} });
			}
		}
		meshInfos.erase(actor);
	}
	void RenderList::cleanUpActorFromRenderList(Actor actor) {
		// dynamic actors more likely to get destroyed at runtime, check them first
		for (int b = 0; b < 2; b++) {
			for (SurfaceMaterialPipelineClassPermutationFlags class_ : getRenderUsingClasses(actor, b, MeshMobilityOption_Dynamic)) {
				materialRendering[b][MeshMobilityOption_Dynamic][class_].erase(actor);
			}
			for (SurfaceMaterialPipelineClassPermutationFlags class_ : getRenderUsingClasses(actor, b, MeshMobilityOption_Static)) {
				materialRendering[b][MeshMobilityOption_Static][class_].erase(actor);
			}
			for (SurfaceMaterialPipelineClassPermutationFlags class_ : getRenderUsingClasses(actor, b, MeshMobilityOption_Stationary)) {
				materialRendering[b][MeshMobilityOption_Stationary][class_].erase(actor);
			}
		}
		if (meshInfos.at(actor).actorBufferFrames.size() != 0) {
			for (int i = 0; i < meshInfos.at(actor).actorBufferFrames.size(); i++) {
				destroyBufferQueue.push_back({ { meshInfos.at(actor).actorBufferFrames[i], meshInfos.at(actor).actorBufferIndices[i] }, {resourceSystem->engineDevice, currentFence} });
			}
		}
		meshInfos.erase(actor);
	}
	void RenderList::modifyMaterialType(AssetID material, sPtr<Level>& level, SurfaceMaterialPipelineClassPermutationFlags old_flags, SurfaceMaterialPipelineClassPermutationFlags new_flags) {
		for (auto& [rigged, moblist] : materialRendering) {
			for (auto& [mobility, matlist] : moblist) {
				std::vector<Actor> outdated_actors{};
				for (auto& [actor, old_renderInfo] : materialRendering[rigged][mobility][old_flags]) {
					SHARD3D_LOG("checking actor {0}", Actor{ actor, level.get() }.getTag());
					for (size_t i = 0; i < old_renderInfo.material_indices.size(); i++) {
						uint32_t mxIndex = old_renderInfo.material_indices[i];
						if (old_renderInfo.mesh->materials[mxIndex].getID() != material.getID()) continue;
						VectorUtils::eraseItemAtIndex(old_renderInfo.material_indices, i);
						materialRendering[rigged][mobility][new_flags][actor].material_indices.push_back(mxIndex);
						materialRendering[rigged][mobility][new_flags][actor].mesh = &meshInfos.at(actor);
						SHARD3D_LOG("Replaced material data from old class ({0}) to new ({1})", old_flags, new_flags);
					}
					if (old_renderInfo.material_indices.empty()) {
						outdated_actors.push_back({ actor, level.get() });
						SHARD3D_LOG("Removed material {0} from Actor {1} since references have been destroyed", material.getAsset(), Actor{ actor, level.get() }.getTag());
					}
				}
				for (Actor actor : outdated_actors) {
					rmvFromSurfaceMaterialRenderingList(actor, actor.hasComponent<Components::SkeletonRigComponent>(), mobility, old_flags);
				}
			}
		}
	}
	void RenderList::refreshActor(Actor actor) {
		auto& mc = actor.nocheck_getComponent<Components::Mesh3DComponent>();
		auto& tc = actor.getTransform();
		auto& inf = meshInfos.at(actor);
		inf.materials = mc.getMaterials();
		inf.mesh = mc.asset;
		inf.castShadows = mc.castShadows;
		inf.receiveDecals = mc.receiveDecals;
		inf.normal = tc.normalMatrix;
		inf.transform = tc.transformMatrix;
		inf.dynamicActorPrevFrameTransform = tc.transformMatrix;
		inf.cullInfo.sphereBounds = resourceSystem->retrieveMesh(mc.asset)->sphereBounds;
		updateActorAABBs(inf);
	}

	void RenderList::updateActorTransform(Actor actor, glm::mat4& transformMatrix, glm::mat3& normalMatrix) {
		auto& iter = meshInfos.find(actor);
		if (iter == meshInfos.end()) return;
		iter->second.normal = normalMatrix;
		//if (actor.getComponent<Components::MobilityComponent>().mobility == Mobility::Dynamic) {
			iter->second.dynamicActorPrevFrameTransform = iter->second.transform;
		//}
		iter->second.transform = transformMatrix;
	}

	void RenderList::updateActorAABBs(Mesh3DRenderInfo& renderData) {
		Mesh3D* mesh = resourceSystem->retrieveMesh(renderData.mesh);
		renderData.cullInfo.worldPosition = renderData.transform[3];
		renderData.cullInfo.maxScale = glm::max(glm::length(renderData.transform[0]), glm::max(glm::length(renderData.transform[1]), glm::length(renderData.transform[2])));
		renderData.cullInfo.transformedAABB = mesh->volumeBounds;
		renderData.cullInfo.sphereBounds = mesh->sphereBounds;
		renderData.cullInfo.sphereBounds.radius *= renderData.cullInfo.maxScale;
		renderData.cullInfo.transformedAABB.transform(renderData.transform);
		renderData.cullInfo.transformedMaterialAABBs.resize(renderData.materials.size());
		for (int i = 0; i < renderData.materials.size(); i++) {
			renderData.cullInfo.transformedMaterialAABBs[i] = mesh->submeshes[i].volumeBounds;
			renderData.cullInfo.transformedMaterialAABBs[i].transform(renderData.transform);
		}
	}
	
	void RenderList::updateActorCastsShadows(Actor actor, bool castShadows) {
		meshInfos.at(actor).castShadows = castShadows;
	}

	void RenderList::updateActorReceivesDecals(Actor actor, bool receiveDecals) {
		meshInfos.at(actor).receiveDecals = receiveDecals;
	}

	void RenderList::updateActor(Actor actor) {
		//rmvActorFromRenderList(actor);
		cleanUpActorFromRenderList(actor);
		addActorToRenderList(actor);
	}

	SurfaceMaterialRenderInfo RenderList::buildRenderInfoFromActor(Actor actor, SurfaceMaterialPipelineClassPermutationFlags flags) {
		SHARD3D_ASSERT(actor.hasComponent<Components::Mesh3DComponent>() && "Actor must have a Mesh3DComponent if trying to be submitted to surface material rendering list!");
		
		SurfaceMaterialRenderInfo renderInfo{};
		renderInfo.mesh = &meshInfos[actor];

		for (int i = 0; i < renderInfo.mesh->materials.size(); i++) {
			if (reinterpret_cast<SurfaceMaterial*>(resourceSystem->retrieveMaterial(renderInfo.mesh->materials[i]))->getPipelineClass() != flags) continue;
			renderInfo.material_indices.push_back(i);
		}

		return renderInfo;//SurfaceMaterialRenderInfo{renderInfo.transform, renderInfo.mesh, renderInfo.material_indices };
	}
	SurfaceMaterialRenderIDFlagBits RenderList::getIterator(std::vector<SurfaceMaterialPipelineClassPermutationFlags> flags, MeshMobilityOptionsFlags mobility) {
		SurfaceMaterialRenderIDFlagBits idfb(materialRendering);
		// TODO: alpha tested objects must be rendered last (in opaque scenes), as they are most likely to be intensive
		for (int b = 0; b < 2; b ++) {
			for (auto flag : flags) { // kys -lukasino1214
				if (mobility & MeshMobilityOption_Static) idfb.flagsCombo.push_back({ b , MeshMobilityOption_Static, flag });
				if (mobility & MeshMobilityOption_Stationary) idfb.flagsCombo.push_back({ b , MeshMobilityOption_Stationary, flag });
				if (mobility & MeshMobilityOption_Dynamic) idfb.flagsCombo.push_back({ b , MeshMobilityOption_Dynamic, flag });
			}
		}
		return idfb;
	}
	void RenderList::addToSurfaceMaterialRenderingList(Actor actor, bool rigged, MeshMobilityOptionsFlags mobility, SurfaceMaterialPipelineClassPermutationFlags flags, const SurfaceMaterialRenderInfo& renderInfo) {
		if (renderInfo.material_indices.empty()) return;
		materialRendering[rigged][mobility][flags][actor] = renderInfo;
	}
	void RenderList::rmvFromSurfaceMaterialRenderingList(Actor actor, bool rigged, MeshMobilityOptionsFlags mobility, SurfaceMaterialPipelineClassPermutationFlags flags) {
		materialRendering[mobility][flags].erase(actor);
		SHARD3D_ASSERT(materialRendering[mobility][flags].find(actor) == materialRendering[mobility][flags].end() && "Failed to erase flag!");
		SHARD3D_VERBOSE("Erased actor {0} from material class flags {1}", actor.getTag(), flags);
	}
	void RenderList::switchSurfaceMaterialRenderingList(Actor actor, bool rigged, MeshMobilityOptionsFlags mobility, SurfaceMaterialPipelineClassPermutationFlags old_flags, SurfaceMaterialPipelineClassPermutationFlags new_flags, const SurfaceMaterialRenderInfo& new_renderInfo) {
		auto renderInfo = materialRendering[mobility][old_flags].extract(actor);
		materialRendering[mobility][new_flags].insert(std::move(renderInfo));
		// wtf is this?? past zilver can you remind me why this exists
	}
	void RenderList::swapAsset(AssetType type, AssetID oldAsset, AssetID newAsset) {
		SHARD3D_ASSERT(type == AssetType::Model3D || type == AssetType::Material && "Invalid asset type!");
		if (type == AssetType::Model3D){
			for (auto& [k, inf] : this->meshInfos) {
				if (inf.mesh == oldAsset) {
					inf.mesh = newAsset;
				}
			}
		} else {
			for (auto& [k, inf] : this->meshInfos) {
				for (auto& mat : inf.materials) {
					if (mat == oldAsset) {
						mat = newAsset;
					}	
				}
			}
		}
	}

	std::vector<SurfaceMaterialPipelineClassPermutationFlags> RenderList::getRenderUsingClasses(Actor actor, bool rigged, MeshMobilityOptionsFlags mobility) {
		std::vector<SurfaceMaterialPipelineClassPermutationFlags> vector;
		for (auto& [class_, val] : materialRendering[rigged][mobility]) {
			if (val.find(actor) != val.end()) vector.push_back(class_);
		}
		return std::vector<SurfaceMaterialPipelineClassPermutationFlags>(vector.begin(), vector.end());
	}
	void RenderList::clear() {
		for (auto& [flags, data] : materialRendering)
			data.clear();
	}

	VkDescriptorSet RenderList::getSkeletonDescriptor(entt::entity actor, uint32_t frameIndex) {
		SHARD3D_ASSERT(skeletalInfos.find(actor) != skeletalInfos.end());
		return skeletalInfos[actor].actorBufferDescriptors[frameIndex];
	}
	
	void RenderList::updateDynamicTransformBufferData(uint32_t frameIndex) {
		for (auto& [actor, info] : meshInfos) {
			if (info.actorBufferFrames.size() == 0) continue;
			DynamicActorInfo transform;
			transform.modelMatrix = info.transform;
			transform.normalMatrix = info.normal;
			transform.modelMatrixPF = info.dynamicActorPrevFrameTransform;
			info.actorBufferFrames[frameIndex]->writeToBuffer(&transform);
			info.actorBufferFrames[frameIndex]->flush();
		}
	}

	void RenderList::updateDynamicRiggingBufferData(sPtr<Level>& level, uint32_t frameIndex) {
		auto view = level->registry.view<Components::Mesh3DComponent, Components::SkeletonRigComponent, Components::IsVisibileComponent>();
		for (auto ent : view) {
			if (skeletalInfos[ent].actorBufferFrames.size() == 0) continue;
			Actor actor = { ent, level.get() };
			auto& skeletonComponent = actor.getComponent<Components::SkeletonRigComponent>();
			RiggedMeshData renderInfo;
			renderInfo.indexOffset = resourceSystem->getVertexAllocator()->getOffsetData(resourceSystem->retrieveMesh(meshInfos[ent].mesh)->meshID).vertexOffset;
			renderInfo.hasSkeleton = skeletonComponent.enableBones;
			renderInfo.hasMorphTarget = skeletonComponent.enableMorphTargets;

			Skeleton* skeleton = resourceSystem->retrieveSkeleton(skeletonComponent.asset);

			if (renderInfo.hasMorphTarget) {
				renderInfo.morphTargetBuffer = skeleton->morphTargetBufferID;
				renderInfo.morphTargetCount = std::min(skeletonComponent.morphTargetWeights.size(), 16ui64);
				for (int i = 0; i < renderInfo.morphTargetCount; i++) {
					renderInfo.morphTargetWeights[i] = skeletonComponent.morphTargetWeights[i];
				}
			}
			renderInfo.skeletonBoneIDBuffer = skeleton->vertexIDBufferID;
			renderInfo.skeletonBoneWeightBuffer = skeleton->vertexWeightBufferID;
			skeletalInfos[actor].actorBufferFrames[frameIndex]->writeToBuffer(&renderInfo, 0, sizeof(RiggedMeshData) - sizeof(RiggedMeshData::boneMatrixData));
			if (renderInfo.hasSkeleton) {
				std::vector<glm::mat4> localBoneTransforms(skeleton->offsetMatrices.size());

				for (int i = 0; i < localBoneTransforms.size(); i++) {
					

					localBoneTransforms[i] = skeletonComponent.boneTransforms[i].transformMatrix();
				}

				std::function<glm::mat4(glm::mat4, uint8_t)> transformHierarchy = [&](glm::mat4 transform, uint8_t parent){
					if (parent == 0xFF) {
						return transform;
					} else {
						return transformHierarchy(localBoneTransforms[parent] * transform, skeleton->relationships[parent].parent);
					}
				};
				renderInfo.boneMatrixData.resize(localBoneTransforms.size());
				for (int i = 0; i < skeleton->relationships.size(); i++) {
					glm::mat4 boneTransform = transformHierarchy(localBoneTransforms[i], skeleton->relationships[i].parent);
					renderInfo.boneMatrixData[i].first = boneTransform * skeleton->offsetMatrices[i];
					renderInfo.boneMatrixData[i].second = glm::inverse(glm::transpose(glm::mat3(renderInfo.boneMatrixData[i].first)));
				}

				skeletalInfos[actor].actorBufferFrames[frameIndex]->writeToBuffer(renderInfo.boneMatrixData.data(), sizeof(RiggedMeshData) - sizeof(RiggedMeshData::boneMatrixData), (sizeof(glm::mat4) + sizeof(glm::mat3x4)) * renderInfo.boneMatrixData.size());
			}
			skeletalInfos[actor].actorBufferFrames[frameIndex]->flush();
		}
	}

	void RenderList::makeActorInvisible(Actor actor) {
		if (actor.hasComponent<Components::IsVisibileComponent>())
			actor.killComponent<Components::IsVisibileComponent>();
		actor.nocheck_getComponent<Components::VisibilityComponent>().isVisible = false;
		if (actor.hasComponent<Components::Mesh3DComponent>()) rmvActorFromRenderList(actor);
	}
	void RenderList::runGarbageCollector(uint32_t frameIndex, VkFence fence) {
		currentFence = fence;
		for (int i = 0; i < destroyBufferQueue.size(); i++) {
			auto& pair = destroyBufferQueue[i];
			if (pair.second.getFence() == fence) continue; // from same frame
			if (pair.second.isFree()) {
				resourceSystem->getDescriptor()->freeIndex(BINDLESS_SSBO_BINDING, pair.first.second);
				delete pair.first.first;
				destroyBufferQueue.erase(destroyBufferQueue.begin() + i);
				i--;
			}
		}
	}
	void RenderList::makeActorVisible(Actor actor) {
		actor.addComponent<Components::IsVisibileComponent>();
		actor.nocheck_getComponent<Components::VisibilityComponent>().isVisible = true;
		if (actor.hasComponent<Components::Mesh3DComponent>()) addActorToRenderList(actor);
	}
	void RenderList::modifyActorMobility(Actor actor, uint32_t oldMobility, uint32_t newMobility) {
		if (!actor.hasComponent<Components::Mesh3DComponent>()) return;
		if (newMobility == oldMobility) return; // return in case mobility is the same
		bool rigged = actor.hasComponent<Components::SkeletonRigComponent>();
		std::vector<SurfaceMaterialPipelineClassPermutationFlags> rc = getRenderUsingClasses(actor, rigged, oldMobility);
		for (auto rf : rc) {
			materialRendering[rigged][newMobility][rf][actor] = materialRendering[rigged][oldMobility][rf].at(actor);
			materialRendering[rigged][oldMobility][rf].erase(actor);
		}
		switch (newMobility) {
		case(1):
			meshInfos.at(actor).mobility = Mobility::Static;
			break;
		case(2):
			meshInfos.at(actor).mobility = Mobility::Stationary;
			break;
		case(4):
			meshInfos.at(actor).mobility = Mobility::Dynamic;
			break;
		}
		if (meshInfos.at(actor).actorBufferFrames.size() > 0) {
			for (int i = 0; i < meshInfos.at(actor).actorBufferFrames.size(); i++) {
				destroyBufferQueue.push_back({ {meshInfos.at(actor).actorBufferFrames[i], meshInfos.at(actor).actorBufferIndices[i] }, {resourceSystem->engineDevice, currentFence} });
			}
			meshInfos.at(actor).actorBufferFrames.clear();
			meshInfos.at(actor).actorBufferIndices.clear();
		}
		if (newMobility == 4) {
			createActorBufferData(actor);
		}
	}
}
