#include "render_manager.h"
/*
#include "vertex_arena_allocator.h"
#include "../../ecs.h"
#include "../../utils/engine_utils.h"
namespace Shard3D {
	RenderManager::RenderManager(
		const std::vector<SurfaceMaterialPipelineClassPermutationOptions>& shaders,
		S3DDevice& device, 
		ResourceSystem* resourceSystem, 
		uint32_t maxFramesInFlight
	) : engineDevice(device), resourceSystem(resourceSystem), maxMeshInstances(ProjectSystem::getEngineSettings().renderer.maximumMeshInstances), maxFrameQueue(maxFramesInFlight + 1) {
		size_t bufferSize = maxMeshInstances * sizeof(uint32_t);
		VkDescriptorType descriptorType{};
		if (bufferSize > 16384u) descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
		else descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;

		transformLookupLayout = S3DDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, descriptorType, VK_SHADER_STAGE_FRAGMENT_BIT)
			.build();

		for (int i = 0; i < maxFrameQueue; i++) {
			destroyTransformMap[i] = std::vector<MeshTransformBufferInfo>();
		}
	}

	RenderManager::~RenderManager() {
		for (int i = 0; i < maxFrameQueue; i++) {
			runGarbageCollector();
		}
	}

	void RenderManager::runGarbageCollector() {
		currentFrameDestructionIndex = (currentFrameDestructionIndex + 1) % maxFrameQueue; // manage own frame index to avoid mismatched destruction issues
		uint32_t lastOccupiedFrameIndex = (currentFrameDestructionIndex + 1) % maxFrameQueue;
		for (MeshTransformBufferInfo tf : destroyTransformMap[lastOccupiedFrameIndex]) {
			delete tf.buffer;
			if (tf.bindlessIndex != BINDLESS_UNALLOCATED_INDEX)
				resourceSystem->getDescriptor()->freeIndex(BINDLESS_SSBO_BINDING, tf.bindlessIndex);
		}
		destroyTransformMap[lastOccupiedFrameIndex].clear();
	}

	void RenderManager::add(Actor actor) {
		SHARD3D_ASSERT(actor.hasComponent<Components::Mesh3DComponent>());
		auto& msc = actor.getComponent<Components::Mesh3DComponent>();
		Mesh3D* mesh = resourceSystem->retrieveMesh(msc.asset);
		UUID uuid = actor.getUUID();
		addMesh(mesh, msc.materials, uuid, msc.castShadows);
		auto& tfc = actor.getTransform();
		writeTransformBuffer(transformBuffers[actorInstances[uuid].hash()].buffer, tfc.transformMatrix, tfc.normalMatrix);
	}

	void RenderManager::updateTransform(Actor actor) {
		UUID uuid = actor.getUUID();
		auto& tfc = actor.getTransform();
		writeTransformBuffer(transformBuffers[actorInstances[uuid].hash()].buffer, tfc.transformMatrix, tfc.normalMatrix);
		
	}

	void RenderManager::addMesh(Mesh3D* mesh, const std::vector<AssetID>& materials, UUID actorID, bool castShadows) {
		uint32_t meshID = mesh->meshes[0].meshID;
		actorInstances[actorID] = MeshInstanceTransformInfo{};

		uint32_t submeshCount = mesh->meshes[0].submeshes.size();
		MeshInstanceInfo meshInstanceInfo{};
		meshInstanceInfo.meshID = meshID;
		meshInstanceInfo.castShadows = castShadows;
		bool instanced = true;

		for (uint32_t i = 0; i < submeshCount; i++) {
			meshInstanceInfo.materialIDs.push_back(resourceSystem->retrieveMaterial(materials[i])->getShaderMaterialID());
		}
		size_t seed = meshInstanceInfo.hash();
		if (meshDrawCommandInfo.find(seed) == meshDrawCommandInfo.end()) {
			instanced = false; 
			meshDrawCommandInfo[seed] = {};
		}
		actorInstances[actorID].instanceID = seed;
		// grab instance index
		uint32_t instanceIndex = getNewInstance(meshInstanceInfo);
		actorInstances[actorID].instanceIndex = instanceIndex;

		addTransform(actorInstances[actorID], glm::mat4(1.f), glm::mat3(1.f));
		uint32_t bufferIndex = transformBuffers[actorInstances[actorID].hash()].bindlessIndex;
		if (instanced = false) {
			transformLookupArrayBuffer[meshID] = createDrawCallTransformLookupArrayBuffer(4u); // 4 is the default size;
		} else {
			transformLookupArrayBuffer[meshID] = resizeDrawCallTransformLookupArrayBuffer(transformLookupArrayBuffer[meshID], instanceIndex + 1);
		}
	}

	void RenderManager::addTransform(MeshInstanceTransformInfo meshInfo, const glm::mat4& transform, const glm::mat3& normal) {
		size_t seed = meshInfo.hash();
		transformBuffers[seed].buffer = createTransformBuffer();
		writeTransformBuffer(transformBuffers[seed].buffer, transform, normal);
		transformBuffers[seed].bindlessIndex = resourceSystem->getDescriptor()->allocateIndex(BINDLESS_SSBO_BINDING);
	}

	S3DBuffer* RenderManager::createTransformBuffer() {
		VkBufferUsageFlags bufferUsageFlags{};
		S3DBuffer* buffer = new S3DBuffer(engineDevice, sizeof(glm::mat4) * 2, 1, bufferUsageFlags, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
		buffer->map();
		return buffer;
	}

	void RenderManager::writeTransformBuffer(S3DBuffer* buffer, const glm::mat4& transform, const glm::mat3& normal) {
		glm::mat4 transformData[2]{ transform, normal };
		buffer->writeToBuffer(transformData);
	}

	void RenderManager::writeTBBIToDrawCallTransformLookupArrayBuffer(S3DBuffer* transformLookupBuffer, uint32_t instanceIndex, uint32_t bufferIndex) {
		transformLookupBuffer->writeToBuffer(&bufferIndex, instanceIndex * sizeof(uint32_t), sizeof(uint32_t));
	}

	S3DBuffer* RenderManager::createDrawCallTransformLookupArrayBuffer(size_t instanceCount) {
		size_t bufferSize = maxMeshInstances * sizeof(uint32_t);
		VkBufferUsageFlags bufferUsageFlags{};
		if (maxMeshInstances > 16384u / 4u) bufferUsageFlags = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
		else bufferUsageFlags = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
		S3DBuffer* buffer = new S3DBuffer(engineDevice, instanceCount * sizeof(uint32_t), 1, bufferUsageFlags, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
		buffer->map();
		return buffer;
	}

	S3DBuffer* RenderManager::resizeDrawCallTransformLookupArrayBuffer(S3DBuffer* oldBuffer, size_t newInstanceCount) {
		S3DBuffer* newBuffer{};
		if (newInstanceCount >= oldBuffer->getInstanceCount()) { // grow
			newBuffer = createDrawCallTransformLookupArrayBuffer(oldBuffer->getInstanceCount() * 1.5);
			newBuffer->writeToBuffer(oldBuffer->getMappedMemory());
			destroyTransformBuffer({ oldBuffer });
		} else if (uint32_t newSize = oldBuffer->getInstanceCount() * 0.5; newInstanceCount <= newSize) { // shrink
			newBuffer = createDrawCallTransformLookupArrayBuffer(newSize);
			newBuffer->writeToBuffer(oldBuffer->getMappedMemory());
			destroyTransformBuffer({ oldBuffer });
		} else newBuffer = oldBuffer;
		return newBuffer;
	}

	VkDescriptorSet RenderManager::writeTransformLookupDescriptor(S3DBuffer* transformLookupBuffer) {
		VkDescriptorSet set{};
		S3DDescriptorWriter::S3DDescriptorWriter(*transformLookupLayout, *device.staticMaterialPool)
			.writeBuffer(0, &transformLookupBuffer->descriptorInfo())
			.build(set);
		return set;
	}

	void RenderManager::destroyTransformLookup(VkDescriptorSet descriptor, S3DBuffer* buffer) {
		device.staticMaterialPool->freeDescriptors({ descriptor });
		destroyTransformBuffer({ buffer });
	}

	void RenderManager::destroyTransformBuffer(MeshTransformBufferInfo mtbi) {
		destroyTransformMap[currentFrameDestructionIndex].push_back(mtbi);
	}

	uint32_t RenderManager::getNewInstance(MeshInstanceInfo instanceCreateInfo) {
		size_t seed = instanceCreateInfo.hash();
		uint32_t highestInstance = meshDrawCommandInfo[seed].meshInfo.instanceCount;
		if (highestInstance <= maxMeshInstances - 100) SHARD3D_WARN("Mesh3D instance limit being approached! Less than 100 instances left!");
		else if (highestInstance == maxMeshInstances) SHARD3D_FATAL(fmt::format("Too many instances being rendered! Engine limit: {} Mesh3D instances", maxMeshInstances));
		meshDrawCommandInfo[seed].meshInfo.instanceCount += 1;
		return highestInstance;
	}

	//VkDrawIndexedIndirectCommand RenderManager::getIndirectDrawCommand(Mesh3D* mesh, uint32_t submeshIndex) {
	//	auto offsetData = resourceSystem->getVertexAllocator()->getOffsetData(mesh->meshes[0].meshID);
	//	VkDrawIndexedIndirectCommand indirectCommand{};
	//	indirectCommand.firstInstance = 0;
	//	indirectCommand.instanceCount = 1;
	//	indirectCommand.firstIndex = offsetData.indexOffset + mesh->meshes[0].submeshes[submeshIndex].indexOffset;
	//	indirectCommand.indexCount = mesh->meshes[0].submeshes[submeshIndex].indexCount;
	//	indirectCommand.vertexOffset = offsetData.vertexOffset + mesh->meshes[0].submeshes[submeshIndex].vertexOffset;
	//
	//	// multi draw -> material
	//	// instancing -> mesh & material
	//	return indirectCommand;
	//}
	//void RenderManager::resizeIndirectDrawCommand(S3DBuffer** drawCommand, uint32_t newDrawCount) {
	//	S3DBuffer* oldIndirectDraw = *drawCommand;
	//	uint32_t previousDrawCount = oldIndirectDraw->getInstanceCount();
	//	VkDrawIndexedIndirectCommand* draws = reinterpret_cast<VkDrawIndexedIndirectCommand*>(oldIndirectDraw->getMappedMemory());
	//	S3DBuffer* newIndirectDraw = new S3DBuffer(
	//		engineDevice,
	//		sizeof(VkDrawIndexedIndirectCommand),
	//		newDrawCount,
	//		VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT,
	//		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
	//	);
	//	newIndirectDraw->map();
	//	if (newDrawCount > previousDrawCount) {
	//		newIndirectDraw->writeToBuffer(draws, 0, newDrawCount * sizeof(VkDrawIndexedIndirectCommand));
	//	} else {
	//		newIndirectDraw->writeToBuffer(draws);
	//	}
	//
	//	*drawCommand = newIndirectDraw;
	//}
	//void RenderManager::updateIndirectDrawCommand(S3DBuffer* drawCommand, uint32_t drawIndex, uint32_t newInstanceCount) {
	//	drawCommand->writeToBuffer(&newInstanceCount, offsetof(VkDrawIndexedIndirectCommand, instanceCount) + drawIndex * sizeof(VkDrawIndexedIndirectCommand), sizeof(uint32_t));
	//	auto cmb = engineDevice.beginSingleTimeCommands();
	//	vkCmdUpdateBuffer(
	//		cmb,
	//		drawCommand->getBuffer(),
	//		offsetof(VkDrawIndexedIndirectCommand, instanceCount),
	//		sizeof(uint32_t),
	//		&newInstanceCount
	//	);
	//	engineDevice.endSingleTimeCommands(cmb);
	//}
	//S3DBuffer* RenderManager::createIndirectDrawCommand(VkDrawIndexedIndirectCommand indirectDraw) {
	//	S3DBuffer* indirectBuffer = new S3DBuffer(
	//		engineDevice,
	//		sizeof(VkDrawIndexedIndirectCommand),
	//		1,
	//		VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT,
	//		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
	//	);
	//	indirectBuffer->map();
	//	indirectBuffer->writeToBuffer(&indirectDraw);
	//	//auto cmb = engineDevice.beginSingleTimeCommands();
	//	//vkCmdUpdateBuffer(
	//	//	cmb,
	//	//	indirectBuffer->getBuffer(),
	//	//	0,
	//	//	sizeof(VkDrawIndexedIndirectCommand),
	//	//	&indirectDraw
	//	//);
	//	//engineDevice.endSingleTimeCommands(cmb);
	//
	//	return indirectBuffer;
	//}
}
*/