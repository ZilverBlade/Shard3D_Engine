#pragma once
#include "../../s3dstd.h"
#include "../../core.h"
#include "../../core/vulkan_api/device.h"
#include "../../core/vulkan_api/buffer.h"
#include "../../core/vulkan_api/destruction.h"

namespace Shard3D {
	struct VertexDataInfo {
		void* position;
		void* uv;
		void* normal;
		void* tangent;
	};
	class VertexArenaAllocator {
	public:
		struct VertexBufferTypeReturnBuilder {
			const S3DBuffer* index;
			const S3DBuffer* vertexPosition;
			const S3DBuffer* vertexUV;
			const S3DBuffer* vertexNormal;
			const S3DBuffer* vertexTangent;

		private:
			VertexBufferTypeReturnBuilder( S3DBuffer* idxbf, S3DBuffer* vtxpbf, S3DBuffer* vtxubf, S3DBuffer* vtxnbf, S3DBuffer* vtxtbf)
				: index(idxbf), vertexPosition(vtxpbf), vertexUV(vtxubf), vertexNormal(vtxnbf), vertexTangent(vtxtbf){}
			friend class VertexArenaAllocator;
		};		 
		struct OffsetData {
			uint32_t vertexOffset;
			uint32_t vertexCount;
			uint32_t indexOffset; 
			uint32_t indexCount;
		};
		VertexArenaAllocator(S3DDevice& device, uint32_t maxFramesInFlight);
		~VertexArenaAllocator();
		DELETE_COPY(VertexArenaAllocator);

		void write(uint32_t id, const VertexDataInfo vertexData, const void* indexData);
		// after invalidating the arena allocator is no longer capable of resizing, allocating or deallocating buffers, and is only meant to be used upon shutdown of the application
		void invalidate();

		uint32_t allocate(uint32_t vertexCount, uint32_t indexCount);
		bool deallocate(uint32_t id);

		void bind(VkCommandBuffer commandBuffer);
		void runGarbageCollector(uint32_t frameIndex, VkFence fence);

		OffsetData getOffsetData(uint32_t meshID);

		VertexBufferTypeReturnBuilder getBuffer() {
			return VertexBufferTypeReturnBuilder(indexBuffer, vertexPositionBuffer, vertexUVBuffer, vertexNormalBuffer, vertexTangentBuffer);
		}
	private:
		void resizeDeviceLocalBuffer(VkCommandBuffer commandBuffer,S3DBuffer** buffer, size_t instanceSize, size_t newInstanceCount, bool isIndexBuffer = false);
		void shrinkDeviceLocalBuffer(VkCommandBuffer commandBuffer,S3DBuffer** buffer, size_t instanceSize, size_t allocatedInstanceCount, size_t toRemoveInstanceCount, size_t toRemoveInstanceOffset, bool isIndexBuffer = false);
		void writeDeviceLocalBuffer(VkCommandBuffer commandBuffer, S3DBuffer* buffer, const void* data, size_t instanceSize, size_t instanceCount, size_t offset);
		void freeBuffer(S3DBuffer* buffer);

		std::vector<std::pair<S3DBuffer*, S3DDestructionObject>> bufferDestructors{}; // kv = frame index, val = buffers to free
		uint32_t currentFrameIndex = 0;
		VkFence currentFence = nullptr;
		const uint32_t maxFramesInFlight;

		bool invalid = false;

		std::unordered_map<uint32_t, OffsetData> offsetData{};

		uint32_t lastIndexInstance{};
		uint32_t lastVertexInstance{};

		S3DDevice& engineDevice;

		S3DBuffer* indexBuffer;
		S3DBuffer* vertexPositionBuffer{};
		S3DBuffer* vertexUVBuffer{};
		S3DBuffer* vertexNormalBuffer{};
		S3DBuffer* vertexTangentBuffer{};
	};
}
