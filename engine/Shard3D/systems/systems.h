#pragma once

#include "rendering/deferred_render_system.h"
#include "rendering/decal_render_system.h"
#include "rendering/billboard_system.h"
#include "rendering/shadow_mapping_system.h"
#include "rendering/light_propagation_volume_system.h"
#include "rendering/velocity_buffer_system.h"
#include "rendering/planar_reflection_system.h"
#include "rendering/volumetric_lighting_system.h"
#include "rendering/terrain_system.h"
#include "rendering/hud_system.h"

#include "post_fx/post_processing_system.h"

#include "buffers/light_system.h"
#include "buffers/reflection_system.h"
#include "buffers/sky_system.h"
#include "buffers/lighting_volume_system.h"
#include "buffers/exponential_fog_system.h"
#include "buffers/ppfx_apply_system.h"
#include "handlers/material_system.h"

#include "computational/particle_system.h"
#include "computational/physics_system.h"
#include "computational/shader_system.h"