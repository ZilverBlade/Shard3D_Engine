#include "../../ecs.h"
#include <glm/gtx/rotate_vector.hpp>
#include "sky_system.h"
#include "../../systems/handlers/resource_system.h"

namespace Shard3D {
	void SkySystem::update(FrameInfo& frameInfo, SceneBuffers& ssbo) {
		int skylightIndex = 0;
		frameInfo.level->registry.view<Components::SkyLightComponent, Components::IsVisibileComponent>().each([&](Components::SkyLightComponent& env, auto& visibility) {
			glm::vec4 sky = glm::vec4{ env.skyTint, env.skyIntensity };
			
			ssbo.sceneEnvironment.skylights[skylightIndex].reflection_id = frameInfo.resourceSystem->retrieveTextureCube(ResourceSystem::coreAssets.c_blankCubemap)->getResourceIndex();
			ssbo.sceneEnvironment.skylights[skylightIndex].irradiance_id = frameInfo.resourceSystem->retrieveTextureCube(ResourceSystem::coreAssets.c_blankCubemap)->getResourceIndex();
			if (env.reflectionType == 0) {

				Actor skybox = { env.skyboxActor, frameInfo.level.get() };
				if (!skybox.isInvalid()) {
					if (skybox.hasComponent < Components::SkyboxComponent>()) {

						auto& skb = skybox.getComponent < Components::SkyboxComponent>();
						ssbo.sceneEnvironment.skylights[skylightIndex].reflection_id = frameInfo.resourceSystem->retrieveTextureCube(skb.environment)->getResourceIndex();
						ssbo.sceneEnvironment.skylights[skylightIndex].irradiance_id = frameInfo.resourceSystem->retrieveTextureCube(skb.irradiance)->getResourceIndex();
						ssbo.sceneEnvironment.skylights[skylightIndex].reflectionTint = glm::vec4{ skb.tint, skb.intensity } * sky;
						ssbo.sceneEnvironment.skylights[skylightIndex].dynamicSky = 0;
					}
				}
			} else if (env.reflectionType == 1) {
				if (env.reflectionCubemap_id != 0) {
					ssbo.sceneEnvironment.skylights[skylightIndex].dynamicSky = 1;
					ssbo.sceneEnvironment.skylights[skylightIndex].reflection_id = env.reflectionCubemap_id;
					ssbo.sceneEnvironment.skylights[skylightIndex].irradiance_id = env.irradianceCubemap_id;
					ssbo.sceneEnvironment.skylights[skylightIndex].reflectionTint = sky;
				}
				
			}

			skylightIndex+=1;
		});
		ssbo.sceneEnvironment.numSkylights = skylightIndex;

		// reset in case there are no skyboxes
		ssbo.sceneEnvironment.activeSkybox.skybox_id = frameInfo.resourceSystem->retrieveTextureCube(ResourceSystem::coreAssets.c_blankCubemap)->getResourceIndex();
		ssbo.sceneEnvironment.activeSkybox.tint = glm::vec4{ 1.0f };

		frameInfo.level->registry.view<Components::SkyboxComponent, Components::IsVisibileComponent>().each([&](Components::SkyboxComponent& env, auto& visibility) {
			ssbo.sceneEnvironment.activeSkybox.skybox_id = frameInfo.resourceSystem->retrieveTextureCube(env.environment)->getResourceIndex();
			ssbo.sceneEnvironment.activeSkybox.tint = glm::vec4{ env.tint, env.intensity };
		});
	}

}