#pragma once

#include "../../s3dstd.h"
#include "../../core/misc/frame_info.h"	  
namespace Shard3D {
	inline namespace Systems {
		class ExponentialFogSystem {
		public:
			ExponentialFogSystem();
			~ExponentialFogSystem();

			DELETE_COPY(ExponentialFogSystem)

			void update(FrameInfo& frameInfo, SceneBuffers& ssbo);
		private:
			
		};
	}
}