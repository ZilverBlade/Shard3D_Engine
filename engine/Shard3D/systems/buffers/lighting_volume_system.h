#pragma once
#include "../../core/misc/frame_info.h"	  
#include "../../core/ecs/level.h"

namespace Shard3D {
	class LightingVolumeSystem {
	public:
		LightingVolumeSystem() = default;
		DELETE_COPY(LightingVolumeSystem);

		void update(FrameInfo& frameInfo, SceneBuffers& ssbo);
	};

}