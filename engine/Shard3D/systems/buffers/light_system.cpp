#include "../../ecs.h"
#include "light_system.h"
#include <glm/gtx/rotate_vector.hpp>
#include "../handlers/resource_system.h"

namespace Shard3D {
	void LightSystem::update(FrameInfo& frameInfo, SceneBuffers& ssbo) {
		int shadowMapIndex = 0;
		{
			int lightIndex = 0;
			int lightComplexIndex = 0;
			frameInfo.level->registry.view<Components::PointLightComponent, RENDER_ITER>().each([&](Components::PointLightComponent light, Components::TransformComponent transform, auto& visibility) {
				glm::vec3 basePosition = transform.transformMatrix[3];
				glm::vec3 color = light.color * light.lightIntensity;
				bool complex = light.sourceLength > 0.0 || light.sourceRadius > 0.0;
				if (light.castShadows) {
					if (!complex) {
						ssbo.sceneOmniLights.pointLights[lightIndex].shadowMapInfoIndex = shadowMapIndex;
					} else {
						ssbo.sceneOmniComplexLights.pointLights[lightComplexIndex].shadowMapInfoIndex = shadowMapIndex;
					}
					ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].lightSpaceMatrix = light.lightSpaceMatrix;
					ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].bias = light.shadowMapBias;
					ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].shadowMapConvention = light.shadowMapConvention;
					ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].shadowMap_id = light.shadowMap_id;
					ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].shadowMapTranslucent_ID = light.castTranslucentShadows ? light.shadowMapTranslucent_id : 0;
					ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].shadowMapTranslucentColor_ID = light.castTranslucentShadows ? light.shadowMapTranslucentColor_id : 0;
					ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].shadowMapTranslucentReveal_ID = light.castTranslucentShadows ? light.shadowMapTranslucentReveal_id : 0;
					ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].sharpness = light.shadowMapSharpness;
					ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].poissonRadius = light.shadowMapPoissonRadius;
					ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].poissonSamples = light.shadowMapPoissonSamples;
					shadowMapIndex += 1;
 				} else {
					if (!complex) {
						ssbo.sceneOmniLights.pointLights[lightIndex].shadowMapInfoIndex = -1;
					} else {
						ssbo.sceneOmniComplexLights.pointLights[lightComplexIndex].shadowMapInfoIndex = -1;
					}
				}
				if (!complex) {
					ssbo.sceneOmniLights.pointLights[lightIndex].position = basePosition;
					ssbo.sceneOmniLights.pointLights[lightIndex].color = color;
					ssbo.sceneOmniLights.pointLights[lightIndex].radius = light.lightRadius;
					lightIndex += 1;
				} else {
					ssbo.sceneOmniComplexLights.pointLights[lightComplexIndex].color = glm::vec4(color, 1.0f);
					ssbo.sceneOmniComplexLights.pointLights[lightComplexIndex].radius = light.lightRadius;
					ssbo.sceneOmniComplexLights.pointLights[lightComplexIndex].sourceRadius = light.sourceRadius;
					if (light.sourceLength > 0.0) {
						ssbo.sceneOmniComplexLights.pointLights[lightComplexIndex].isPipe = true;
						glm::vec3 right = glm::normalize(glm::vec3(transform.transformMatrix[0]));
						ssbo.sceneOmniComplexLights.pointLights[lightComplexIndex].positionPipeEnd0 = glm::vec4(basePosition - right * light.sourceLength, 0.f);
						ssbo.sceneOmniComplexLights.pointLights[lightComplexIndex].positionPipeEnd1 = glm::vec4(basePosition + right * light.sourceLength, 0.f);
					} else {
						ssbo.sceneOmniComplexLights.pointLights[lightComplexIndex].isPipe = false;
						ssbo.sceneOmniComplexLights.pointLights[lightComplexIndex].positionPipeEnd0 = glm::vec4(basePosition, 0.f);
					}
					lightComplexIndex += 1;
				}
			});
			ssbo.sceneOmniLights.numPointLights = lightIndex;
			ssbo.sceneOmniComplexLights.numPointLights = lightComplexIndex;
		}
		{
			int lightIndex = 0;
			int lightComplexIndex = 0;
			frameInfo.level->registry.view<Components::SpotLightComponent, RENDER_ITER>().each([&](Components::SpotLightComponent light, Components::TransformComponent transform, auto& visibility) {
				glm::mat4 realTransform = transform.transformMatrix;
				realTransform = glm::rotate(realTransform, glm::half_pi<float>(), glm::vec3(1.f, 0.f, 0.f));
				glm::vec3 direction = glm::normalize(realTransform[2]); // convert rotation to direction
				glm::vec3 basePosition = transform.transformMatrix[3];
				glm::vec3 color = light.color * light.lightIntensity;

				bool complex = light.sourceRadius > 0.0;
				bool useShadowMapInfo = false;
				if (!light.castShadows && light.hasCookie) {
					S3DCamera camera;
					camera.setViewYXZ(realTransform);
					float FOV = acos(light.outerAngle) * 2.f;
					camera.setPerspectiveProjection(FOV, 1.f, 0.1f, 1.0f);

					ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].lightSpaceMatrix = camera.getProjection() * camera.getView();
					useShadowMapInfo = true;
				} else if (light.castShadows ){
					useShadowMapInfo = true;
					ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].lightSpaceMatrix = light.lightSpaceMatrix;
				}

				if (useShadowMapInfo) {
					if (!complex) {
						ssbo.sceneOmniLights.spotLights[lightIndex].shadowMapInfoIndex = shadowMapIndex;
					} else {
						ssbo.sceneOmniComplexLights.spotLights[lightComplexIndex].shadowMapInfoIndex = shadowMapIndex;
					}
					if (light.castShadows) {
						ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].shadowMapConvention = light.shadowMapConvention;
						ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].bias = light.shadowMapBias;
						ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].shadowMap_id = light.shadowMap_id;
						ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].shadowMapTranslucent_ID = light.castTranslucentShadows ? light.shadowMapTranslucent_id : 0;
						ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].shadowMapTranslucentColor_ID = light.castTranslucentShadows ? light.shadowMapTranslucentColor_id : 0;
						ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].shadowMapTranslucentReveal_ID = light.castTranslucentShadows ? light.shadowMapTranslucentReveal_id : 0;
						ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].sharpness = light.shadowMapSharpness;
						ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].poissonRadius = light.shadowMapPoissonRadius;
						ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].poissonSamples = light.shadowMapPoissonSamples;
					}
					shadowMapIndex += 1;
				} else {
					if (!complex) {
						ssbo.sceneOmniLights.spotLights[lightIndex].shadowMapInfoIndex = -1;
					} else {
						ssbo.sceneOmniComplexLights.spotLights[lightComplexIndex].shadowMapInfoIndex = -1;
					}
				}
				if (!complex) {
					ssbo.sceneOmniLights.spotLights[lightIndex].cookie_ID = light.hasCookie ? frameInfo.resourceSystem->retrieveTexture(light.cookieTexture)->getResourceIndex() : 0;
				} else {
					ssbo.sceneOmniComplexLights.spotLights[lightComplexIndex].cookie_ID = light.hasCookie ? frameInfo.resourceSystem->retrieveTexture(light.cookieTexture)->getResourceIndex() : 0;
				}
				if (!complex) {
					ssbo.sceneOmniLights.spotLights[lightIndex].position = glm::vec4(basePosition, 0.0f);
					ssbo.sceneOmniLights.spotLights[lightIndex].color = glm::vec4(color, 1.0f);
					ssbo.sceneOmniLights.spotLights[lightIndex].direction = { direction };
					ssbo.sceneOmniLights.spotLights[lightIndex].angle = { light.outerAngle, light.innerAngle };
					ssbo.sceneOmniLights.spotLights[lightIndex].radius = light.lightRadius;
					lightIndex += 1;
				} else {
					ssbo.sceneOmniComplexLights.spotLights[lightComplexIndex].position = glm::vec4(basePosition, 0.0f);
					ssbo.sceneOmniComplexLights.spotLights[lightComplexIndex].color = glm::vec4(color, 1.0f);
					ssbo.sceneOmniComplexLights.spotLights[lightComplexIndex].direction = { direction };
					ssbo.sceneOmniComplexLights.spotLights[lightComplexIndex].angle = { light.outerAngle, light.innerAngle };
					ssbo.sceneOmniComplexLights.spotLights[lightComplexIndex].radius = light.lightRadius;
					ssbo.sceneOmniComplexLights.spotLights[lightComplexIndex].sourceRadius = light.sourceRadius;
					lightComplexIndex += 1;
				}
			});
			ssbo.sceneOmniLights.numSpotLights = lightIndex;
			ssbo.sceneOmniComplexLights.numSpotLights = lightComplexIndex;
		}
		{
			int lightIndex = 0;
			frameInfo.level->registry.view<Components::DirectionalLightComponent, RENDER_ITER>().each([&](Components::DirectionalLightComponent light, Components::TransformComponent transform, auto& visibility) {
				glm::mat4 realTransform = transform.transformMatrix;
				realTransform = glm::rotate(realTransform, glm::half_pi<float>(), glm::vec3(1.f, 0.f, 0.f));
				glm::vec3 direction = glm::normalize(realTransform[2]); // convert rotation to direction

				ssbo.sceneEnvironment.directionalLights[lightIndex].position = transform.transformMatrix[3];
				ssbo.sceneEnvironment.directionalLights[lightIndex].color = glm::vec4(light.color * light.lightIntensity, 1.0f);
				ssbo.sceneEnvironment.directionalLights[lightIndex].direction = direction;
				if (light.castShadows) {
					ssbo.sceneEnvironment.directionalLights[lightIndex].shadowMapInfoIndex = shadowMapIndex;
					ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].lightSpaceMatrix = light.lightSpaceMatrix;
					ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].bias = light.shadowMapBias * 0.4f * (light.shadowMapClippingDistance * 0.05);
					ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].shadowMapConvention = light.shadowMapConvention;
					ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].shadowMap_id = light.castShadows ? light.shadowMap_id : 0;
					ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].shadowMapTranslucent_ID = light.castTranslucentShadows ? light.shadowMapTranslucent_id : 0;
					ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].shadowMapTranslucentColor_ID = light.castTranslucentShadows ? light.shadowMapTranslucentColor_id : 0;
					ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].shadowMapTranslucentReveal_ID = light.castTranslucentShadows ? light.shadowMapTranslucentReveal_id : 0;
					ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].sharpness = light.shadowMapSharpness;
					ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].poissonRadius = light.shadowMapPoissonRadius;
					ssbo.sceneShadowMapInfos.shadowMaps[shadowMapIndex].poissonSamples = light.shadowMapPoissonSamples;
					shadowMapIndex += 1;
				}
				lightIndex += 1;
			});
			ssbo.sceneEnvironment.numDirectionalLights = lightIndex;
		}
	}
}