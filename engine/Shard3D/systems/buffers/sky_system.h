#pragma once
#include "../../core/misc/frame_info.h"	  
#include "../../core/ecs/level.h"

namespace Shard3D {
	class SkySystem {
	public:
		SkySystem() = default;
		DELETE_COPY(SkySystem);

		void update(FrameInfo& frameInfo, SceneBuffers& ssbo);
	};
}