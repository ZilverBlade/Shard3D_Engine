#include "../../ecs.h"
#include <glm/gtx/rotate_vector.hpp>
#include "reflection_system.h"
#include "../handlers/resource_system.h"

namespace Shard3D {
	void ReflectionSystem::update(FrameInfo& frameInfo, SceneBuffers& ssbo) 	{
		int reflectionIndex = 0;
		frameInfo.level->registry.view<Components::BoxReflectionCaptureComponent, Components::BoxVolumeComponent, RENDER_ITER>().each([&](auto& brfcc, auto& volume, auto& transform, auto& visibility) {
			ssbo.sceneSotAFX.reflectionCubes[reflectionIndex].invtransform = transform.getInverseTransform();
			ssbo.sceneSotAFX.reflectionCubes[reflectionIndex].boundSize = volume.bounds;
			ssbo.sceneSotAFX.reflectionCubes[reflectionIndex].intersectionDist = volume.transitionDistance;
			ssbo.sceneSotAFX.reflectionCubes[reflectionIndex].color = glm::vec4(brfcc.tint, brfcc.intensity);
			ssbo.sceneSotAFX.reflectionCubes[reflectionIndex].reflection_id = frameInfo.resourceSystem->retrieveTextureCube(brfcc.reflectionAsset)->getResourceIndex();
			ssbo.sceneSotAFX.reflectionCubes[reflectionIndex].irradiance_id = brfcc.irradianceAsset ? frameInfo.resourceSystem->retrieveTextureCube(brfcc.irradianceAsset)->getResourceIndex() : 0;
			reflectionIndex += 1;
		});
		ssbo.sceneSotAFX.numReflectionCubes = reflectionIndex;

		int planarReflectionIndex = 0;
		frameInfo.level->registry.view<Components::PlanarReflectionComponent, RENDER_ITER>().each([&](Components::PlanarReflectionComponent& prfcc, auto& transform, auto& visibility) {
			glm::mat4 realTransform = transform.transformMatrix;
			realTransform = glm::rotate(realTransform, -glm::half_pi<float>(), glm::vec3(1.f, 0.f, 0.f));
			const glm::vec3 planeNormal = glm::normalize(realTransform[2]); // convert rotation to direction
			const float d = -glm::dot(planeNormal, glm::vec3(transform.transformMatrix[3]));
			glm::vec4 clip = { planeNormal.x, planeNormal.y, planeNormal.z, d };

			ssbo.sceneSotAFX.reflectionPlanes[planarReflectionIndex].color = { prfcc.tint, prfcc.intensity };
			ssbo.sceneSotAFX.reflectionPlanes[planarReflectionIndex].cartesianPlane = clip;
			ssbo.sceneSotAFX.reflectionPlanes[planarReflectionIndex].maxHeight = prfcc.maxInfluenceHeight;
			ssbo.sceneSotAFX.reflectionPlanes[planarReflectionIndex].intersectionDist = prfcc.transitionDistance;
			ssbo.sceneSotAFX.reflectionPlanes[planarReflectionIndex].distortDuDvFactor = prfcc.distortDuDvFactor;
			ssbo.sceneSotAFX.reflectionPlanes[planarReflectionIndex].distortionDeltaLimit = prfcc.distortionDeltaLimit;
			ssbo.sceneSotAFX.reflectionPlanes[planarReflectionIndex].reflection_id = prfcc.reflectionShaderTexture_id;
			ssbo.sceneSotAFX.reflectionPlanes[planarReflectionIndex].irradiance_id = prfcc.irradiance ? prfcc.irradianceShaderTexture_id : 0;
			planarReflectionIndex += 1;
		});
		ssbo.sceneSotAFX.numReflectionPlanes = planarReflectionIndex;
	}
}