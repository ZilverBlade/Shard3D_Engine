#include "../../ecs.h"
#include <glm/gtx/rotate_vector.hpp>
#include "lighting_volume_system.h"

namespace Shard3D {
	void LightingVolumeSystem::update(FrameInfo& frameInfo, SceneBuffers& ssbo) {
		int volumeIndex = 0;
		frameInfo.level->registry.view<Components::BoxAmbientOcclusionVolumeComponent, Components::BoxVolumeComponent, RENDER_ITER>().each([&](auto& aov, auto& volume, auto& transform, auto& visibility) {
			ssbo.sceneSotAFX.aoVolumes[volumeIndex].invtransform = transform.getInverseTransform();
			ssbo.sceneSotAFX.aoVolumes[volumeIndex].boundSize = volume.bounds;
			ssbo.sceneSotAFX.aoVolumes[volumeIndex].intersectionDist = volume.transitionDistance;
			ssbo.sceneSotAFX.aoVolumes[volumeIndex].tint = aov.tint;
			ssbo.sceneSotAFX.aoVolumes[volumeIndex].maxOcclusion = aov.minOcclusion;
			volumeIndex += 1;
		});
		ssbo.sceneSotAFX.numAOVolumes = volumeIndex;
	}
	
}