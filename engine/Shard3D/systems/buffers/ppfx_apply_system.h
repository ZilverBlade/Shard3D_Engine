#pragma once
#include "../../core/misc/frame_info.h"	  
#include "../../core/ecs/level.h"

namespace Shard3D {
	class PostProcessingApplySystem {
	public:
		PostProcessingApplySystem() = default;
		DELETE_COPY(PostProcessingApplySystem);

		void update(sPtr<Level>& level, PostFXData& postProcessing, Actor cameraActor);
	};
}