#include "exponential_fog_system.h"

#include "../../core/ecs/level.h"
#include "../../core/ecs/components.h"
#include "../handlers/project_system.h"

namespace Shard3D::Systems {
	
	ExponentialFogSystem::ExponentialFogSystem() {
		
	}
	ExponentialFogSystem::~ExponentialFogSystem() {
		
	}
	void ExponentialFogSystem::update(FrameInfo& frameInfo, SceneBuffers& ssbo) {
		int fogCount{0};
		frameInfo.level->registry.view<Components::ExponentialFogComponent, Components::IsVisibileComponent>().each([&](Components::ExponentialFogComponent fog, auto& visibility) {
			ssbo.sceneEnvironment.exponentialFogs[fogCount].color = glm::vec4(fog.color, 1.0);
			ssbo.sceneEnvironment.exponentialFogs[fogCount].falloff = fog.falloff;
			ssbo.sceneEnvironment.exponentialFogs[fogCount].density = fog.density;
			ssbo.sceneEnvironment.exponentialFogs[fogCount].sunScattPow = fog.sunScatteringPow;
			ssbo.sceneEnvironment.exponentialFogs[fogCount].updownbias = fog.upDownBias;
			fogCount += 1;
		});
		ssbo.sceneEnvironment.numExponentialFogs = fogCount;
	}
}