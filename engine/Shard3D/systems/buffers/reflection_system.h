#pragma once
#include "../../core/misc/frame_info.h"	  
#include "../../core/ecs/level.h"

namespace Shard3D {
	class ReflectionSystem {
	public:
		ReflectionSystem() = default;
		DELETE_COPY(ReflectionSystem);

		void update(FrameInfo& frameInfo, SceneBuffers& ssbo);
	};

}