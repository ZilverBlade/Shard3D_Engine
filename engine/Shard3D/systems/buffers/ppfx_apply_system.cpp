#include "../../ecs.h"
#include "ppfx_apply_system.h"

namespace Shard3D {
#define LERP_PPFX(setting) oldData.setting = glm::mix(oldData.setting, newData.setting, lerpWeight)

	static bool isPointInCube(glm::vec3 pt, glm::vec3 cubeDimensions, glm::mat4 invCubeTransform) {
		glm::vec4 point = invCubeTransform * glm::vec4(pt, 1.0);

		return glm::all(glm::lessThanEqual(-cubeDimensions, glm::vec3(point))) && glm::all(glm::lessThanEqual(glm::vec3(point), cubeDimensions));
	}

	static float calcPointBetweenBoxLimits(glm::vec3 pt, glm::vec3 lowerLimit, glm::vec3 upperLimit, glm::mat4 invCubeTransform) {
		glm::vec3 point = glm::abs(invCubeTransform * glm::vec4(pt, 1.0));
		
		glm::vec3 higherX = glm::vec3(upperLimit.x, point.y, point.z);
		glm::vec3 higherY = glm::vec3(point.x, upperLimit.y, point.z);
		glm::vec3 higherZ = glm::vec3(point.x, point.y, upperLimit.z);

		float distX =glm:: distance(point, higherX);
		float distY =glm:: distance(point, higherY);
		float distZ =glm:: distance(point, higherZ);

		glm::vec3 distLowToHigh = abs(lowerLimit - upperLimit);

		float diffX = glm::clamp(distX / distLowToHigh.x, 0.f, 1.f);
		float diffY = glm::clamp(distY / distLowToHigh.y, 0.f, 1.f);
		float diffZ = glm::clamp(distZ / distLowToHigh.z, 0.f, 1.f);

		return diffX * diffY * diffZ;
	}

	static void applyEffect(PostFXData& oldData, const PostFXData& newData, float lerpWeight) {
		SHARD3D_ASSERT(lerpWeight >= 0.0f && lerpWeight <= 1.0f && "lerp must be valid!");
		LERP_PPFX(bloom.knee);
		LERP_PPFX(bloom.threshold);
		LERP_PPFX(bloom.strength);

		LERP_PPFX(hdr.exposure);
		LERP_PPFX(hdr.lim);
		LERP_PPFX(hdr.toneMappingAlgorithm);

		LERP_PPFX(grading.contrast);
		LERP_PPFX(grading.saturation);
		LERP_PPFX(grading.gain);
		LERP_PPFX(grading.temperature);
		LERP_PPFX(grading.hueShift);
		LERP_PPFX(grading.shadows);
		LERP_PPFX(grading.midtones);
		LERP_PPFX(grading.highlights);

		LERP_PPFX(motionBlur.targetFramerate);
		LERP_PPFX(motionBlur.maxLength);

		LERP_PPFX(ssao.bias);
		LERP_PPFX(ssao.radius);
		LERP_PPFX(ssao.intensity);
		LERP_PPFX(ssao.power);

		LERP_PPFX(lightPropagationVolume.extent);
		LERP_PPFX(lightPropagationVolume.numPropagations);
		LERP_PPFX(lightPropagationVolume.numCascades);
		LERP_PPFX(lightPropagationVolume.boost);
		LERP_PPFX(lightPropagationVolume.fadeRatio);
		LERP_PPFX(lightPropagationVolume.temporalBlend);
		LERP_PPFX(lightPropagationVolume.fixed);
		LERP_PPFX(lightPropagationVolume.fixedPosition);

		LERP_PPFX(mist.enable);
		LERP_PPFX(mist.tint);
		LERP_PPFX(mist.constantScattering);
		LERP_PPFX(mist.mieScattering);
	}

	void PostProcessingApplySystem::update(sPtr<Level>& level, PostFXData& postProcessing, Actor cameraActor) {
		if (cameraActor.hasComponent<Components::PostFXComponent>()) 
			applyEffect(postProcessing, cameraActor.getComponent<Components::PostFXComponent>().postfx, 1.0f);
		glm::vec3 cameraPosition = cameraActor.getTransform().transformMatrix[3];
		level->registry.view<
			Components::PostFXComponent,
			Components::BoxVolumeComponent,
			Components::TransformComponent,
			Components::IsVisibileComponent
		>().each([&](
			Components::PostFXComponent ppfx, 
			Components::BoxVolumeComponent volume, 
			Components::TransformComponent transform, 
			auto& guaranteedVisibility) {
			glm::vec3 bdsMax = volume.bounds + volume.transitionDistance;

			if (isPointInCube(cameraPosition, bdsMax, transform.getInverseTransform())) {
				applyEffect(
					postProcessing,
					ppfx.postfx,
					calcPointBetweenBoxLimits(cameraPosition, volume.bounds, bdsMax, transform.getInverseTransform())
				);
			}
		});
	}

}