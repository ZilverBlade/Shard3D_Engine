#include "convex_hull_generator.h"
namespace Shard3D {

	ConvexHullGenerator::ConvexHullGenerator(S3DDevice& device, VertexArenaAllocator* allocator, VertexArenaAllocator::OffsetData mesh) 
	: engineDevice(device) {
		vertices.resize(mesh.vertexCount);

		S3DBuffer hostVertex = S3DBuffer(engineDevice, sizeof(glm::vec3), vertices.size(), VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

		VkCommandBuffer commandBuffer = engineDevice.beginSingleTimeCommands();
		engineDevice.copyBuffer(commandBuffer, allocator->getBuffer().vertexPosition->getBuffer(), hostVertex.getBuffer(), hostVertex.getBufferSize(), mesh.vertexOffset * sizeof(glm::vec3));
		engineDevice.endSingleTimeCommands(commandBuffer);

		hostVertex.map();

		memcpy(vertices.data(), hostVertex.getMappedMemory(), hostVertex.getBufferSize());
	}


	std::vector<glm::vec3> ConvexHullGenerator::generateConvexHull(uint32_t maxTargetPoints) {
		
		glm::vec3 extremeMax{}, extremeMin{};

		std::vector<glm::vec3> hull;
		// build the initial tetrahedron, with no degenreate shapes
		for (glm::vec3 vertex : vertices) {
			if (hull.size() == 0) { 
				hull.push_back(vertex);
			} else if (hull.size() == 1) {
				if (vertex != hull[0]) {
					hull.push_back(vertex);
				}
			} else if (hull.size() == 2) {
				glm::vec3 ray = glm::normalize(hull[1] - hull[0]);
				glm::vec3 secondary = glm::normalize(vertex - hull[0]);
				if (glm::dot(ray, secondary) < 0.99f && glm::dot(ray, secondary) > -0.99f) {
					hull.push_back(vertex);
				}
			} else if (hull.size() == 3) {
				glm::vec3 faceNormal = glm::normalize(glm::cross(hull[2] - hull[0], hull[1] - hull[0]));
				float d = -glm::dot(faceNormal, (hull[2] + hull[0] + hull[1]) / 3.0f);
				if (glm::dot(glm::vec4(faceNormal, d), glm::vec4(vertex, 1.0f)) > 0.0f) {
					hull.push_back(vertex);
					break;
				}
			} 
		}

	//	using namespace RPhys;
	//	ConvexHull hullShape;
	//	
	//	bool externalPointsExist = true;
	//	while (externalPointsExist) {
	//		hullShape = buildConvexHull(hull);
	//		eraseInternalPoints(hullShape, vertices);
	//		externalPointsExist = false;
	//		for (Triangle tri : hullShape.triangles) {
	//			glm::vec3 normal = glm::normalize(glm::cross(hullShape.points[tri.c] - hullShape.points[tri.a], hullShape.points[tri.b] - hullShape.points[tri.a]));
	//			glm::vec3 candidate = findPointFurthestInDirection(vertices, normal);
	//			if (candidate == hullShape.points[tri.c] || candidate == hullShape.points[tri.b] || candidate == hullShape.points[tri.a]) { 
	//				continue; // furthest point has been reached
	//			}
	//			hull.push_back(candidate);
	//			externalPointsExist = true;
	//		}
	//	}
	//
	//	// clean up and merge close vertices until desired limit is reached
	//
	//	float limit = 0.01f;
	//	while (hull.size() > maxTargetPoints) {
	//		for (int i = 0; i < hull.size(); i++) {
	//			for (int j = 0; j < hull.size(); j++) {
	//				if (i == j) continue;
	//				if (glm::distance2(hull[i], hull[j]) <= limit * limit) {
	//					glm::vec3 avg = (hull[i] + hull[j]) / 2.0f;
	//					hull.erase(hull.begin() + i);
	//					hull.erase(hull.begin() + j);
	//					hull.push_back(avg);
	//					i--;
	//					j--;
	//				}
	//			}
	//		}
	//
	//		limit *= 2.0f;
	//	}

		return hull;
	}
}