#pragma once
#include "../../core.h"
#include "../../ecs.h"
#include <glm/gtx/hash.hpp>
namespace Shard3D {
	struct Mesh3DCullInfo;
	enum class BoundingShape {
		Sphere, AABB, OBB
	};
	struct BVHGroup {
		bool lowest = false;
		int octreeLevel;
		VolumeBounds octreeAABB;
		std::unordered_map<glm::ivec3, BVHGroup> groups;
		std::vector<std::pair<entt::entity, Mesh3DCullInfo*>> actors;
	};
	class FrustumCullingSystem {
	public:
		//static void createBVH(Mesh3DRenderInfo& meshInfoRef, BVHGroup& group, int smallestOctreeLevel, int largestOctreeLevel);
		static void fillBVH(Actor actor, Mesh3DCullInfo& cullInfoRef, BVHGroup& group, int smallestOctreeLevel, int largestOctreeLevel);

		static void cull(std::unordered_map<entt::entity, bool>&, BVHGroup& tlBVH, S3DCamera& camera);
	};
}
