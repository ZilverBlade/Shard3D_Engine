#pragma once

#define GLM_FORCE_QUAT_DATA_WXYZ
#include <iostream> 
#include <ode/ode.h>
#include "../../core/misc/frame_info.h"	
#include "../../core/asset/physics_asset.h"	
#include <glm/gtx/quaternion.hpp>

namespace Shard3D {
	inline namespace ECS {
		class Level;
	}

	class PhysicsSystem {
	public:

		struct PhysicsVehicleData {
			dGeomID chassis;

			dGeomID wheelFL;
			dGeomID wheelFR;
			dGeomID wheelRL;
			dGeomID wheelRR;

			//dBodyID independentSuspensionFL;
			//dBodyID independentSuspensionFR;
			//dBodyID independentSuspensionRL;
			//dBodyID independentSuspensionRR;

			float wheelBase;
			float cgToFrontAxle;
			float cgToRearAxle;
			float cgRelHeight;

			glm::vec3 lastVelocity{};
			glm::vec3 acceleration{};
		};

		class TransformState {
		public:
			struct TransformData {
				glm::vec3 translation;
				glm::quat rotation;
				glm::vec3 linearVelocity;
				glm::vec3 angularVelocity;

				PhysicsColliderType colliderType;
			};

			struct HingeData {
				glm::vec3 translation;
			};

			struct Hinge2Data {
				glm::vec3 angle;
			};
			std::unordered_map<dBodyID, TransformData> bodies;
			std::unordered_map<dBodyID, TransformData> jointHinge;
			std::unordered_map<dBodyID, TransformData> jointHinge2;
		};

		PhysicsSystem(ResourceSystem* resourceSystem, float tickSpeed, bool quickStep);
		~PhysicsSystem();
		void createPhysicsActors(sPtr<Level>& level);
		void begin(sPtr<Level>& level);
		void end(sPtr<Level>& level);
		void simulate(sPtr<Level>& level);
		void applyTransform(sPtr<Level>& level, float factor);
		PhysicsCollisionData rayCast(glm::vec3 origin, glm::vec3 direction, float maxDistance);

		dWorldID getWorld() {
			return physicsWorld;
		}

		// only call when transform is different from the physics trajectory, coordinates are Y up translation, quaternion rotation 
		void overrideTransform(dBodyID body, glm::vec3 translation, glm::quat rotation);
		void registerActor(Actor actor);

		float getStepSize() {
			return stepSize;
		}
	private:
		PhysicsAsset loadPhysicsAsset(AssetID asset);
		PhysicsMaterial loadPhysicsMaterial(AssetID asset);
		PhysicsTireMaterial loadTirePhysicsMaterial(AssetID asset);
		void loadTerrainPhysicsMaterial(std::vector<PhysicsMaterial>& materials, AssetID asset);
		

		std::vector<dHeightfieldDataID> terrainData;
		std::vector<dTriMeshDataID> trimeshData;
		std::vector<dGeomID> terrainGeometries;

		std::vector<PhysicsVehicleData*> vehicleDatas;
		struct PhyiscsActor {
			dBodyID body;
			dGeomID geometry;
		};

		std::unordered_map<AssetID, PhysicsAsset> physicsAssets;
		std::unordered_map<AssetID, PhysicsMaterial> physicsMaterials;
		std::unordered_map<AssetID, PhysicsTireMaterial> physicsTireMaterials;

		uPtr<TransformState> previousTransformStates;

		float stepSize;
		bool quickStep;
		float lastFactor = 0.0f;
		dWorldID physicsWorld;
		dSpaceID physicsSpace;
		dSpaceID physicsRays;
		dThreadingImplementationID threading;
		dThreadingThreadPoolID threadingPool;

		dJointGroupID collisionContactGroup;
		dJointGroupID universalJointGroup;
		std::vector<PhyiscsActor> actors;
		ResourceSystem* resourceSystem;
	};
}
