#pragma once
#include "../../s3dstd.h"
#include "../../vulkan_abstr.h"
#include "../../core/misc/frame_info.h"	  
#include "../../core/ecs/level.h"
#include "../handlers/vertex_arena_allocator.h"
namespace Shard3D {
	class ConvexHullGenerator {
	public:
		ConvexHullGenerator(S3DDevice& device, VertexArenaAllocator* allocator, VertexArenaAllocator::OffsetData mesh);

		std::vector<glm::vec3> generateConvexHull(uint32_t maxTargetPoints = 16);
	private:
		S3DDevice& engineDevice;
		std::vector<glm::vec3> vertices;
	};
}