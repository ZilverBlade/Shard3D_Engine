#include "frustum_culling_system.h"
#include "../handlers/render_list.h"

namespace Shard3D {
	void FrustumCullingSystem::fillBVH(Actor actor, Mesh3DCullInfo& cullInfoRef, BVHGroup& group, int smallestOctreeLevel, int largestOctreeLevel) {
		float radius = cullInfoRef.sphereBounds.radius* cullInfoRef.maxScale;
		glm::vec3 position = cullInfoRef.worldPosition;

		cullInfoRef.octreeTiles.resize(largestOctreeLevel - smallestOctreeLevel, glm::ivec3(0x7fffffff));

		auto& parentPartition = group;
		bool lowestPossible = false;
		int64_t sl;
		for (sl = largestOctreeLevel; sl >= smallestOctreeLevel; sl--) {
			if (lowestPossible) {
				parentPartition.actors.push_back({ actor, &cullInfoRef });
				return;
			}
			float extentXYZ = sl > 0 ? float(1Ui64 << sl) : 1.f / float(1Ui64 << -sl);
			if (radius > extentXYZ) {  // too big for this octree partition
				lowestPossible = true;
				sl += 2;
				continue;
			}
			glm::vec3 partitionSector = glm::floor(position / extentXYZ);
			glm::vec3 partitionCenter = (partitionSector + 0.5f) * extentXYZ;
			VolumeBounds partitionAABB = VolumeBounds(partitionCenter, glm::vec3(extentXYZ));
			glm::vec3 sphereToPoint = -glm::normalize(partitionCenter - position) * radius; // furthest point
			if (!partitionAABB.isInVolume(sphereToPoint + position)) {  // bad position in this octree partition
				lowestPossible = true;
				sl += 2;
				continue;
			}
			cullInfoRef.octreeTiles[sl - smallestOctreeLevel] = partitionSector;
			parentPartition = parentPartition.groups[partitionSector];
			parentPartition.lowest = sl == smallestOctreeLevel;
			parentPartition.actors.push_back({ actor, &cullInfoRef });
			parentPartition.octreeLevel = sl;
			parentPartition.octreeAABB = partitionAABB;
		}
	}
	void FrustumCullingSystem::cull(std::unordered_map<entt::entity, bool>& actors, BVHGroup& tlBVH, S3DCamera& camera) {
		for (auto& [grid, bvh] : tlBVH.groups) {
			VolumeBounds& partitionAABB = bvh.octreeAABB;
			if (!bvh.lowest) {
				for (auto& [actor, cd] : bvh.actors) {
					cd->sphereBounds.radius *= cd->maxScale;
					bool visible = camera.isInFrustum(cd->sphereBounds, cd->worldPosition);
					actors[actor] = visible;
					if (visible) {
						if (cd->transformedMaterialAABBs.size() == 1) {
							cd->materialVisibilities[0] = true;
						} else {
							for (int m = 0; m < cd->transformedMaterialAABBs.size(); m++) {
								cd->materialVisibilities[m] = camera.isInFrustum(cd->transformedMaterialAABBs[m]);
							}
						}
					}
				}
				if (camera.isInFrustum(partitionAABB) == false) {
					for (auto [actor, cd] : bvh.actors) {
						actors[actor] = false;
					}
				} else {
					cull(actors, bvh, camera);
				}
			} else {
				bool cullResult = camera.isInFrustum(partitionAABB); // region is so small (likely mesh is not complex), batch all actors in this level
				for (auto [actor, cd] : bvh.actors) {
					actors[actor] = cullResult;
				}
			}
		}
	}
}