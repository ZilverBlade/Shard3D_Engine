#include "physics_system.h"
#include "../rendering/terrain_system.h"

#include "../../core/ecs/components.h"
#include "../../core/ecs/level.h"
#include "../../core/ecs/actor.h"
#include "../../core/asset/assetmgr.h"

#include <glm/gtx/matrix_decompose.hpp>

#include <ReactorPhysicsEngine/colliders/convex_hull.h>
#include <ReactorPhysicsEngine/convex_shape.h>
#include "../../../resources/shaders/include/compression.glsl"

struct BodyPair {
	dBodyID bodyA, bodyB;

	bool operator==(BodyPair const& pair) const {
		return pair.bodyA == bodyA && pair.bodyB == bodyB || pair.bodyA == bodyB && pair.bodyB == bodyA;
	}

};
namespace std {
	template <>
	struct hash<BodyPair> {
		size_t operator()(BodyPair const& pair) const {
			return (size_t)pair.bodyA * (size_t)pair.bodyB + (size_t)pair.bodyA + (size_t)pair.bodyB;
		}
	};
}

namespace Shard3D {

	enum class PhysicsActorType {
		None = 0,
		Default = 1,
		Tire = 2
	};
	
	struct PhysicsTireActorData {
		PhysicsActorType type = PhysicsActorType::Tire;
		Actor actor;

		PhysicsTireMaterial material;
		dBodyID connectedAxle;
		dBodyID connectedSuspension;
		PhysicsSystem::PhysicsVehicleData* vehicleData;
	};

	struct PhysicsActorData {
		PhysicsActorType type = PhysicsActorType::Default;
		Actor actor;

		PhysicsMaterial material;
		std::vector<dBodyID> connectedBodies;
		bool isTriggerVolume = false;
	};
	struct PhysicsBodyData {
		glm::vec3 rotationAxisWorld;
	}; 
	struct PhysicsTerrainActorData {
		std::vector<PhysicsMaterial> materials;
		Actor actor;
	};
	struct PhysicsTerrainActorTileData {
		PhysicsTerrainActorData* terrainData;
		uint8_t* materialData;
		float resX, resY;
		float tileX, tileY;
		float extentX, extentY;
	};
	struct CollisionData {
		dWorldID world;
		dJointGroupID contactGroup;
		//int maxContacts;
		glm::vec3 gravity;
		float ts;
		float worldFriction = 100.0f;
	};
	struct RayTestData {
		dWorldID world;
		dJointGroupID contactGroup;
		//int maxContacts;
		glm::vec4 hitPosition;
		glm::vec3 hitNormal;
		Actor hitActor;
	};


	void messagefun(int errnum, const char* msg, va_list ap) {

	}

	PhysicsSystem::PhysicsSystem(ResourceSystem* resourceSystem, float tickSpeed, bool quickStep) 
		: resourceSystem(resourceSystem), stepSize(1.0f / tickSpeed), quickStep(quickStep) {
		if (!dInitODE2(0)) {
			SHARD3D_FATAL("Failed to intitialize Open Dynamics Engine!!");
		}
		SHARD3D_INFO("ODE configuration: \n{0}", dGetConfiguration());
		physicsWorld = dWorldCreate();

		collisionContactGroup = dJointGroupCreate(0);
		universalJointGroup = dJointGroupCreate(0);
		dWorldSetQuickStepNumIterations(physicsWorld, 30U);
		dWorldSetQuickStepW(physicsWorld, 1.60f);
	//	dWorldSetContactSurfaceLayer(physicsWorld, 0.0);
		dWorldSetERP(physicsWorld, 0.1); // default == 0.2, affects stability
		dWorldSetCFM(physicsWorld, 3.1e-5); // default (fp32) == 1e-5, affects stability and performance

		// disable logging
		dSetErrorHandler(messagefun);
		dSetDebugHandler(messagefun);
		dSetMessageHandler(messagefun);
	}
	PhysicsSystem::~PhysicsSystem() {
		dWorldDestroy(physicsWorld);
		dCloseODE();
	}

	void PhysicsSystem::begin(sPtr<Level>& level) {
		const uint32_t processor_count = std::thread::hardware_concurrency();
		threading = dThreadingAllocateMultiThreadedImplementation();
		threadingPool = dThreadingAllocateThreadPool(processor_count > 4 ? processor_count / 4 : 2, 0, dAllocateFlagBasicData, NULL);
		dThreadingThreadPoolServeMultiThreadedImplementation(threadingPool, threading);
		// dWorldSetStepIslandsProcessingMaxThreadCount(world, 1);
		dWorldSetStepThreadingImplementation(physicsWorld, dThreadingImplementationGetFunctions(threading), threading);

		physicsSpace = dHashSpaceCreate(0);
		physicsRays = dSimpleSpaceCreate(0);
		previousTransformStates = make_uPtr<TransformState>();
		dHashSpaceSetLevels(physicsSpace, -1, 5);

		static glm::vec3 gravity = { 0.f, -9.81f, 0.f };
		dWorldSetGravity(physicsWorld, gravity.x, gravity.y, gravity.z);

		// create terrain actor
		auto terrainView = level->registry.view<Components::Rigidbody3DComponent, Components::TerrainComponent>();
		for (auto& actor_ : terrainView) {
			Actor actor = { actor_, level.get() };
			auto& terrain = actor.getComponent<Components::TerrainComponent>();
			auto& phys = actor.getComponent<Components::Rigidbody3DComponent>();
			if (!terrain.physData) return;

			PhysicsTerrainActorData* terrainActorData = new PhysicsTerrainActorData();
			loadTerrainPhysicsMaterial(terrainActorData->materials, phys.material);
			terrainActorData->actor = actor;
			for (auto& tile : terrain.physData->tiles) {
				dHeightfieldDataID tileData = dGeomHeightfieldDataCreate();
				terrainData.push_back(tileData);
				dGeomHeightfieldDataBuildSingle(tileData, tile.heights.data(), 0, tile.widthX, tile.widthY, tile.verticesX, tile.verticesY, 1.0f, 0.0f, 0.05f, 0);
				dGeomHeightfieldDataSetBounds(tileData, tile.aabb.minXYZ.y, tile.aabb.maxXYZ.y);
				dGeomID tileGeo = dCreateHeightfield(physicsSpace, tileData, 1);
				dGeomSetPosition(
					tileGeo,
					tile.offsetX, 
					0.0f,
					tile.offsetY
				);

				PhysicsTerrainActorTileData* data = new PhysicsTerrainActorTileData();
				data->terrainData = terrainActorData;
				data->resX = tile.materialDataResX;
				data->resY = tile.materialDataResY;
				data->tileX = tile.offsetX;
				data->tileY = tile.offsetY;
				data->extentX = tile.widthX / 2.0f;
				data->extentY = tile.widthY / 2.0f;
				data->materialData = reinterpret_cast<uint8_t*>(malloc(tile.materialData.size()));
				memcpy(data->materialData, tile.materialData.data(), tile.materialData.size());

				dGeomSetData(tileGeo, data);
				terrainGeometries.push_back(tileGeo);
			}
		}
		createPhysicsActors(level);
		SHARD3D_INFO("Activated physics");
	}
	void PhysicsSystem::end(sPtr<Level>& level) {
		dThreadingImplementationShutdownProcessing(threading);
		dThreadingFreeThreadPool(threadingPool);
		dWorldSetStepThreadingImplementation(physicsWorld, NULL, NULL);
		dThreadingFreeImplementation(threading);
		level->registry.view<Components::Rigidbody3DComponent>().each([&](Components::Rigidbody3DComponent& phys) {
			phys.instantiatedActor = 0;
		});
		level->registry.view<Components::PhysicsConstraintComponent>().each([&](Components::PhysicsConstraintComponent& phys) {
			phys.instantiatedActor = 0;
		});
		if (terrainGeometries.size()) {
			delete reinterpret_cast<PhysicsTerrainActorTileData*>(dGeomGetData(terrainGeometries[0]))->terrainData;
		}
		for (auto& geom : terrainGeometries) {
			delete reinterpret_cast<PhysicsTerrainActorTileData*>(dGeomGetData(geom));
			dGeomDestroy(geom);
		}
		terrainGeometries.clear();
		for (auto& heightField : terrainData) {
			dGeomHeightfieldDataDestroy(heightField);
		}
		terrainData.clear();
		for (auto& trimesh : trimeshData) {
			dGeomTriMeshDataDestroy(trimesh);
		}
		trimeshData.clear();
		for (auto& actor : actors) {
			switch (*reinterpret_cast<PhysicsActorType*>(actor.geometry)) {
			case(PhysicsActorType::Default):
				delete reinterpret_cast<PhysicsActorData*>(dGeomGetData(actor.geometry));
				break;
			case(PhysicsActorType::Tire):
				delete reinterpret_cast<PhysicsTireActorData*>(dGeomGetData(actor.geometry));
				break;
			}
			delete reinterpret_cast<PhysicsBodyData*>(dBodyGetData(actor.body));
			dGeomDestroy(actor.geometry);
			dBodyDestroy(actor.body);
		}
		physicsAssets.clear();
		physicsMaterials.clear();
		actors.clear();
		dSpaceDestroy(physicsSpace);
		dSpaceDestroy(physicsRays);
		dJointGroupEmpty(universalJointGroup);
		SHARD3D_INFO("Deactivated physics");
	}

	static glm::mat3 odeMat3ToGLM(const dMatrix3 odeMat3) {
		return {
			odeMat3[0], odeMat3[4], odeMat3[8],
			odeMat3[1], odeMat3[5], odeMat3[9],
			odeMat3[2], odeMat3[6], odeMat3[10]
		}; 
	}

	static float pacejkaTireModel(float slip, float B, float C, float D, float E) {
		return D * sin(atan(B * slip - E * (B * slip - atan(B * slip))));
	}
	const static int MAX_CONTACTS = 8;
	// Check ray collision against a space
	void rayCallback(void* data, dGeomID o1, dGeomID o2) {
		RayTestData* rayData = static_cast<RayTestData*>(data);
		dReal* hitPosition = reinterpret_cast<dReal*>(&rayData->hitPosition);
		dReal* hitNormal = reinterpret_cast<dReal*>(&rayData->hitNormal);

		// Check collisions
		dContact contacts[MAX_CONTACTS];
		int numc = dCollide(o1, o2, MAX_CONTACTS, &contacts[0].geom, sizeof(dContact));
		for (int i = 0; i < numc; i++) {
			// Check depth against current closest hit
			if (contacts[i].geom.depth < hitPosition[3]) {
				dCopyVector3(hitPosition, contacts[i].geom.pos);
				dCopyVector3(hitNormal, contacts[i].geom.normal);
				hitPosition[3] = contacts[i].geom.depth;

				if (dGeomGetClass(o2) == dRayClass) {
					PhysicsActorData& actorDataA = *(PhysicsActorData*)dGeomGetData(o1);
					rayData->hitActor = actorDataA.actor;
				} else {
					PhysicsActorData& actorDataB = *(PhysicsActorData*)dGeomGetData(o2);
					rayData->hitActor = actorDataB.actor;
				}
			}
		}
	}
	static void collisionCallback(void* data, dGeomID o1, dGeomID o2) noexcept {
		CollisionData* collisionData = static_cast<CollisionData*>(data);
		if (dGeomIsSpace(o1) || dGeomIsSpace(o2)) {
			// colliding a space with something :
			dSpaceCollide2(o1, o2, data, &collisionCallback);
			// collide all geoms internal to the space(s)
			if (dGeomIsSpace(o1))
				dSpaceCollide((dSpaceID)o1, data, &collisionCallback);
			if (dGeomIsSpace(o2))
				dSpaceCollide((dSpaceID)o2, data, &collisionCallback);
		} else {
			// colliding two non-space geoms, so generate contact
			// points between o1 and o2

			dBodyID bodyA, bodyB;
			bodyA = dGeomGetBody(o1);
			bodyB = dGeomGetBody(o2);

			if (!bodyA && !bodyB) {
				return;
			}

			Actor actorA, actorB;

			dGeomID wheelGeom;
			dBodyID wheelBod;
			PhysicsActorType a_type, b_type;
			bool a_triggerVolume = false, b_triggerVolume = false;
			bool tireSim = false;
			float a_frictionCoef = 1.0f; 
			float a_looseFrictionCoef = 0.0f; 
			float a_elasticity = 0.0f; 
			float a_softness = 0.0f; 
			float a_absorption = 0.0f;
			float b_frictionCoef = 1.0f;
			float b_looseFrictionCoef = 0.0f;
			float b_elasticity = 0.0f;
			float b_softness = 0.0f;
			float b_absorption = 0.0f;

			PhysicsCollisionData collidingWithA, collidingWithB;
			PhysicsTerrainActorTileData* terrainData{};
			bool terrainIsActorA = false;
			if (dGeomGetClass(o1) == dHeightfieldClass) {
				terrainData = reinterpret_cast<PhysicsTerrainActorTileData*>(dGeomGetData(o1));

				b_type = *(PhysicsActorType*)dGeomGetData(o2);

				PhysicsMaterial* b_material;
				if (b_type == PhysicsActorType::Default) {
					b_material = &((PhysicsActorData*)dGeomGetData(o2))->material;
					actorB = ((PhysicsActorData*)dGeomGetData(o2))->actor;
				} else if (b_type == PhysicsActorType::Tire) {
					b_material = &((PhysicsTireActorData*)dGeomGetData(o2))->material;
					actorB = ((PhysicsTireActorData*)dGeomGetData(o2))->actor;
					tireSim = true;
					wheelGeom = o2;
					wheelBod = bodyB;
				}

				float b_frictionCoef = b_material->frictionCoef;
				float b_looseFrictionCoef = b_material->looseFrictionCoef;
				float b_elasticity = b_material->elasticity;
				float b_softness = b_material->softness;
				float b_absorption = b_material->absorption;

				collidingWithB.actor = terrainData->terrainData->actor;
				actorA = terrainData->terrainData->actor;

				terrainIsActorA = true;
			} else if (dGeomGetClass(o2) == dHeightfieldClass) {
				terrainData = reinterpret_cast<PhysicsTerrainActorTileData*>(dGeomGetData(o2));

				a_type = *(PhysicsActorType*)dGeomGetData(o1);
				PhysicsMaterial* a_material;
				if (a_type == PhysicsActorType::Default) {
					a_material = &((PhysicsActorData*)dGeomGetData(o1))->material;
					actorA = ((PhysicsActorData*)dGeomGetData(o1))->actor;
				} else if (a_type == PhysicsActorType::Tire) {
					a_material = &((PhysicsTireActorData*)dGeomGetData(o1))->material;
					actorA = ((PhysicsTireActorData*)dGeomGetData(o1))->actor;
					tireSim = true;
					wheelGeom = o1;
					wheelBod = bodyA;
				}


				float a_frictionCoef = a_material->frictionCoef;
				float a_looseFrictionCoef = a_material->looseFrictionCoef;
				float a_elasticity = a_material->elasticity;
				float a_softness = a_material->softness;
				float a_absorption = a_material->absorption;

				collidingWithA.actor = terrainData->terrainData->actor;
				actorB = terrainData->terrainData->actor;

			} else {
				a_type = *(PhysicsActorType*)dGeomGetData(o1);
				b_type = *(PhysicsActorType*)dGeomGetData(o2);

				if (a_type == PhysicsActorType::Default && b_type == PhysicsActorType::Default) {
					std::vector<dBodyID>& a_connectedBodies = ((PhysicsActorData*)dGeomGetData(o1))->connectedBodies;
					std::vector<dBodyID>& b_connectedBodies = ((PhysicsActorData*)dGeomGetData(o2))->connectedBodies;
					if (std::find(a_connectedBodies.begin(), a_connectedBodies.end(), bodyB) != a_connectedBodies.end()) {
						return;
					}
				}
				PhysicsMaterial* a_material;
				PhysicsMaterial* b_material;
				if (a_type == PhysicsActorType::Default) {
					a_material = &((PhysicsActorData*)dGeomGetData(o1))->material;
					actorA = ((PhysicsActorData*)dGeomGetData(o1))->actor;
				} else if(a_type == PhysicsActorType::Tire) {
					a_material = &((PhysicsTireActorData*)dGeomGetData(o1))->material;
					actorA = ((PhysicsTireActorData*)dGeomGetData(o1))->actor;
					tireSim = true;
					wheelGeom = o1;
					wheelBod = bodyA;
				}
				if (b_type == PhysicsActorType::Default) {
					b_material = &((PhysicsActorData*)dGeomGetData(o2))->material;
					actorB = ((PhysicsActorData*)dGeomGetData(o2))->actor;
				} else if (b_type == PhysicsActorType::Tire) {
					b_material = &((PhysicsTireActorData*)dGeomGetData(o2))->material;
					actorB = ((PhysicsTireActorData*)dGeomGetData(o2))->actor;
					tireSim = true;
					wheelGeom = o2;
					wheelBod = bodyB;
				}


				float a_frictionCoef = a_material->frictionCoef;
				float a_looseFrictionCoef = a_material->looseFrictionCoef;
				float a_elasticity = a_material->elasticity;
				float a_softness = a_material->softness;
				float a_absorption = a_material->absorption;

				float b_frictionCoef = b_material->frictionCoef;
				float b_looseFrictionCoef = b_material->looseFrictionCoef;
				float b_elasticity = b_material->elasticity;
				float b_softness = b_material->softness;
				float b_absorption = b_material->absorption;

				collidingWithA.actor = actorB;
				collidingWithB.actor = actorA;
			}
			if (a_triggerVolume || b_triggerVolume) {
				collidingWithB.normal = collidingWithA.normal = {};
				collidingWithB.position = collidingWithA.position = {};
				actorA.getComponent<Components::Rigidbody3DComponent>().collidingWith.push_back(collidingWithA);
				actorB.getComponent<Components::Rigidbody3DComponent>().collidingWith.push_back(collidingWithB);
				return;
			}

			dContact contacts[MAX_CONTACTS];
			int numc = dCollide(o1, o2, MAX_CONTACTS, &contacts[0].geom, sizeof(dContact));

			glm::vec3 averagePosition = {};
			glm::vec3 averageNormal = {};

			for (int i = 0; i < numc; i++) {
				glm::vec3 position = *(glm::vec3*)contacts[i].geom.pos;
				averagePosition += position;
				averageNormal += *(glm::vec3*)contacts[i].geom.normal;

				if (terrainData) {
					glm::vec2 terrainProj = { position.x, position.z };
					glm::vec2 terrainTileSpace = (terrainProj - glm::vec2{terrainData->tileX, terrainData->tileY})
						/ glm::vec2{ terrainData->extentX, terrainData->extentY };
					glm::vec2 terrainUV = glm::clamp(terrainTileSpace * 0.5f + 0.5f, 0.0f, 1.0f);
					glm::ivec2 terrainCoord = glm::round(terrainUV * glm::vec2{ terrainData->resX, terrainData->resY });
					int byteOffset = terrainCoord.y * terrainData->resX + terrainCoord.x;
					uint8_t materialBits = terrainData->materialData[byteOffset];
					PhysicsMaterial materials[4]{};
					uint32_t materialCount = 0;
					for (int m = 0; m < 4; m++) {
						if (materialBits & (1 << m)) {
							materials[materialCount] = terrainData->terrainData->materials[m];
							materialCount++;
						}
					}
					float ivmc = 1.0 / static_cast<float>(materialCount);

					float& mref_frictionCoef = terrainIsActorA ? a_frictionCoef : b_frictionCoef;
					float& mref_looseFrictionCoef = terrainIsActorA ? a_looseFrictionCoef : b_looseFrictionCoef;
					float& mref_elasticity = terrainIsActorA ? a_elasticity : b_elasticity;
					float& mref_absorption = terrainIsActorA ? a_absorption : b_absorption;
					float& mref_softness = terrainIsActorA ? a_softness : b_softness;
					mref_frictionCoef = 0.0f;
					mref_looseFrictionCoef = 0.0f;
					mref_elasticity = 0.0f;
					mref_absorption = 0.0f;
					mref_softness = 0.0f;
					for (int m = 0; m < materialCount; m++) {
						mref_frictionCoef += materials[m].frictionCoef;
						mref_looseFrictionCoef += materials[m].looseFrictionCoef;
						mref_elasticity += materials[m].elasticity;
						mref_absorption += materials[m].absorption;
						mref_softness += materials[m].softness;
					}
					mref_frictionCoef *= ivmc;
					mref_looseFrictionCoef *= ivmc;
					mref_elasticity *= ivmc;
					mref_absorption *= ivmc;
					mref_softness *= ivmc;
				}

				contacts[i].surface.mu = (a_looseFrictionCoef * b_looseFrictionCoef + a_frictionCoef * b_frictionCoef) * collisionData->worldFriction;
				if (tireSim) {
					PhysicsTireMaterial& material = ((PhysicsTireActorData*)dGeomGetData(wheelGeom))->material;
					PhysicsSystem::PhysicsVehicleData* vehicleData = ((PhysicsTireActorData*)dGeomGetData(wheelGeom))->vehicleData;
					dJointID axle = nullptr;
					for (int j = 0; j < dBodyGetNumJoints(wheelBod); j++) {// get hinge joint
						axle = dBodyGetJoint(wheelBod, j);
						if (dJointGetType(axle) == dJointTypeHinge) {
							break;
						}
					}
					dBodyID axleBody = dJointGetBody(axle, 0);
					dJointID piston = nullptr;
					for (int j = 0; j < dBodyGetNumJoints(axleBody); j++){// get piston joint
						piston = dBodyGetJoint(axleBody, j);
						if (dJointGetType(piston) == dJointTypePiston) {
							break;
						}
					}
					dBodyID chassis = dGeomGetBody(vehicleData->chassis);
					dMass chassisMass;
					dBodyGetMass(chassis, &chassisMass);

					glm::vec3 chassisVelocity = *(glm::vec3*)dBodyGetLinearVel(chassis);
					glm::vec3 axleAxis;
					glm::vec3 suspensionAxis;
					
					dJointGetHingeAxis(axle, (dVector3&)axleAxis);
					dBodySetFiniteRotationAxis(wheelBod, axleAxis.x, axleAxis.y, axleAxis.z);
					dJointGetPistonAxis(piston, (dVector3&)suspensionAxis);

					glm::vec3 front = -glm::cross(suspensionAxis, axleAxis);
					glm::vec3 right = -axleAxis; // axle axis should point left

					float wheelRadius, wheelWidth;
					dGeomCylinderGetParams(wheelGeom, &wheelRadius, &wheelWidth);
					float wheelAngularSpeed = glm::dot(*(glm::vec3*)dBodyGetAngularVel(wheelBod), axleAxis);
					glm::vec3 wheelLinearVelocity = *(glm::vec3*)dBodyGetLinearVel(wheelBod);
					glm::vec3 chassisLinearVelocity = *(glm::vec3*)dBodyGetLinearVel(chassis);
					float wheelLongitudinalSpeed = glm::dot(wheelLinearVelocity, front);

					bool lowSpeed = abs(wheelAngularSpeed * wheelRadius) < 1.0f && abs(wheelLongitudinalSpeed) < 1.0f;

					bool isRearWheel = wheelGeom == vehicleData->wheelRL || wheelGeom == vehicleData->wheelRR;
					if (lowSpeed || true) {
						contacts[i].surface.mu2 = contacts[i].surface.mu / 8.0f;
						contacts[i].surface.mode = dContactFDir1 | dContactApprox1;
						((glm::vec3&)contacts[i].fdir1) = front;
					} else {
						contacts[i].surface.mode = 0;
						contacts[i].surface.mu = 0.0f;
						//contacts[i].surface.mode = dContactFDir1 | dContactMu2 ; 

						bool wheelStopped = abs(wheelAngularSpeed) < 1e-3f;
						float slideSign = wheelStopped ? glm::sign(wheelLongitudinalSpeed) : glm::sign(wheelAngularSpeed);
						float wheelSlipSpeed = ((wheelAngularSpeed * wheelRadius) - wheelLongitudinalSpeed) * slideSign;
						float slipRatio = wheelSlipSpeed / std::max(abs(wheelLongitudinalSpeed), 1e-6f);

						float driveWheel = isRearWheel ? 1.f : 0.f;
						float driveWheelTorque = dJointGetHingeParam(axle, dParamFMax) /*glm::dot(*(glm::vec3*)dBodyGetTorque(wheelBod), axleAxis)*/ * driveWheel;
			
						glm::vec3 driveWheelTorqueVector = -driveWheelTorque * axleAxis;

						float wheelLateralSpeed = glm::dot(wheelLinearVelocity, axleAxis);
						float slipAngle = atan2(wheelLateralSpeed, std::max(abs(wheelLongitudinalSpeed), 1e-6f));

						glm::vec3 axlePosition = *(glm::vec3*)dBodyGetPosition(axleBody);
						glm::vec3 chassisPosition = *(glm::vec3*)dBodyGetPosition(chassis);

						float wheelBase = vehicleData->wheelBase;
						glm::vec3 CoGworld = (glm::vec3&)chassisMass.c + chassisPosition;
						float weight = chassisMass.mass * 9.81f;
						float weightOnAxle = 0.0f; 


						float signedAcceleration = glm::sign(glm::dot(vehicleData->acceleration, chassisLinearVelocity)) * glm::length(vehicleData->acceleration);
						if (isRearWheel) {
							weightOnAxle = (vehicleData->cgToFrontAxle / vehicleData->wheelBase) * weight +
								(vehicleData->cgRelHeight / vehicleData->wheelBase) * chassisMass.mass * signedAcceleration;
						} else {
							weightOnAxle = (vehicleData->cgToRearAxle / vehicleData->wheelBase) * weight -
								(vehicleData->cgRelHeight / vehicleData->wheelBase) * chassisMass.mass * signedAcceleration;
						}
						SHARD3D_INFO("driveTorque {0}", driveWheelTorque);
						SHARD3D_INFO("axleWeight {0}", weightOnAxle);

						float wheelLoadForce = weightOnAxle;
						float longitudinalForceScalar, lateralForceScalar;
						glm::vec3 longitudinalForce, lateralForce;

						//float maxLongitudinalForce = (wheelLoadForce * wheelSlipSpeed)
						//	+ (driveWheelTorque * slideSign / wheelRadius);

						const float rollingResitance = 0.0f;
						float rollingResitanceForce = -rollingResitance * wheelLongitudinalSpeed;

						longitudinalForceScalar = std::max((driveWheel * 
							pacejkaTireModel(slipRatio, 
								material.longSlipStiffness,
								material.longSlipShape,
								material.longSlipPeak, 
								material.longSlipCurvature
							) * wheelLoadForce - driveWheelTorque) + rollingResitanceForce, 0.0f);

						longitudinalForce = front * longitudinalForceScalar;//(longitudinalForceScalar < 0.0 ?
							//std::max(longitudinalForceScalar, maxLongitudinalForce) : std::min(longitudinalForceScalar, maxLongitudinalForce));
						lateralForceScalar = (
							pacejkaTireModel(
								glm::degrees(slipAngle),
								material.latSlipStiffness,
								material.latSlipShape,
								material.latSlipPeak,
								material.latSlipCurvature
							) * wheelLoadForce);
						lateralForce = right * lateralForceScalar;
						//if (glm::dot(longitudinalForce, longitudinalForce) > 0.00001f)
						//	dBodyAddForce(wheelBod, longitudinalForce.x, longitudinalForce.y, longitudinalForce.z);
						//if (glm::dot(lateralForce, lateralForce) > 0.00001f)
						//	dBodyAddForce(wheelBod, lateralForce.x, lateralForce.y, lateralForce.z);

						glm::vec3 sumForce = lateralForce + longitudinalForce;
						dBodySetForce(wheelBod, sumForce.x, sumForce.y, sumForce.z);


						//dBodyAddTorque(wheelBod, tractionTorqueVec.x, tractionTorqueVec.y, tractionTorqueVec.z);


						SHARD3D_INFO("slip ratio {0}, slip angle {1}", slipRatio, slipAngle);
						SHARD3D_INFO("force longi {0}, force lati {1}", glm::length(longitudinalForce) , glm::length(lateralForce) );
						//
						//contacts[i].surface.mu = pacejkaTireModel(1.0 - abs(std::min(slipRatio, 1.0f)), 2.5f, 2.0f, 1.02f, 0.6f) * tractionLon * wheelLoadForce;
						//contacts[i].surface.mu2 = pacejkaTireModel(glm::degrees(3.1415f - abs(std::min(slipRatio, 3.1415f))), 0.714f, 1.40f, 1.02f, -0.20f) * tractionLat * wheelLoadForce;

					}


					//float colVel = glm::dot((glm::vec3&)contacts[i].geom.normal, velocity);
					//if (colVel > 2.0f) {
					//	SHARD3D_INFO("SCRRRRR");
					//}
				} else {
					contacts[i].surface.mode = dContactBounce | dContactApprox1;
					if (a_softness > 0.0f || b_softness > 0.0f) {
						contacts[i].surface.mode |= dContactSoftCFM;
						contacts[i].surface.soft_cfm = a_softness + b_softness;
					}
					if (a_absorption > 0.0f || b_absorption > 0.0f) {
						contacts[i].surface.mode |= dContactSoftERP;
						contacts[i].surface.soft_erp = a_absorption + b_absorption;
					}
					contacts[i].surface.bounce = (a_elasticity + b_elasticity) * 0.5f;

					// TODO: implement back
					// 
					//if (glm::length2(materialA.conveyorDirection) > 0.0f || glm::length2(materialB.conveyorDirection) > 0.0f) {
					//	glm::vec3 summedDir = odeMat3ToGLM(dGeomGetRotation(o1)) * materialA.conveyorDirection + 
					//		odeMat3ToGLM(dGeomGetRotation(o2)) * materialB.conveyorDirection;
					//	contacts[i].surface.motion1 = glm::length(summedDir);
					//	if (contacts[i].surface.motion1 > 0.0f) {
					//		contacts[i].surface.mode |= dContactMotion1 | dContactFDir1;
					//		glm::vec3 normalized = summedDir / contacts[i].surface.motion1;
					//		contacts[i].fdir1[0] = normalized[0]; contacts[i].fdir1[1] = normalized[1]; contacts[i].fdir1[2] = normalized[2];
					//	}
					//}
				}

				if (numc == 1) {
					collidingWithB.position = collidingWithA.position = averagePosition;
					collidingWithB.normal = collidingWithA.normal = averageNormal;
				} else {
					float invc = 1.0f / float(numc);
					collidingWithB.position = collidingWithA.position = averagePosition * invc;
					collidingWithB.normal = collidingWithA.normal = averageNormal * invc;
				}

				dJointID contact = dJointCreateContact(collisionData->world,
					collisionData->contactGroup, &contacts[i]);

				dJointAttach(contact, bodyA, bodyB);
			}
		}
	}


	void PhysicsSystem::overrideTransform(dBodyID body, glm::vec3 translation, glm::quat rotation) {
		previousTransformStates->bodies[body].translation = translation;
		previousTransformStates->bodies[body].rotation = rotation;
		dBodySetPosition(body, translation.x, translation.y, translation.z);
		dBodySetQuaternion(body, (dQuaternion&)rotation);
	}

	void PhysicsSystem::registerActor(Actor actor) {
		
	}

	PhysicsAsset PhysicsSystem::loadPhysicsAsset(AssetID asset) {
		auto iter = physicsAssets.find(asset);
		if (iter == physicsAssets.end()) {
			physicsAssets[asset] = AssetManager::loadPhysicsAsset(asset);
		} else {
			return iter->second;
		}
	}

	PhysicsMaterial PhysicsSystem::loadPhysicsMaterial(AssetID asset) {
		auto iter = physicsMaterials.find(asset);
		if (iter == physicsMaterials.end()) {
			PhysicsMaterial material = {};
			material.deserialize(asset.getAbsolute());
			physicsMaterials[asset] = material;
			return material;
		} else {
			return iter->second;
		}
	}

	PhysicsTireMaterial PhysicsSystem::loadTirePhysicsMaterial(AssetID asset) {
		auto iter = physicsTireMaterials.find(asset);
		if (iter == physicsTireMaterials.end()) {
			PhysicsTireMaterial material = {};
			material.deserialize(asset.getAbsolute());
			material.deserializeTire(asset.getAbsolute());
			physicsTireMaterials[asset] = material;
			return material;
		} else {
			return iter->second;
		}
	}

	void PhysicsSystem::loadTerrainPhysicsMaterial(std::vector<PhysicsMaterial>& materials, AssetID asset) {
		simdjson::dom::parser parser;
		simdjson::padded_string json = simdjson::padded_string::load(asset.getAbsolute());
		auto& data = parser.parse(json);
		if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
			return;
		}
		if (data["assetType"].get_string().value() != "physics_material_collection") {
			SHARD3D_ERROR("Invalid terarin physics material collection asset! {0}", asset.getAsset());
			return;
		}
		for (int i = 0; i < data["materials"].get_array().size(); i++) {
			materials.push_back(loadPhysicsMaterial(std::string(data["materials"].get_array().at(i).get_string().value())));
		}
	}

	void PhysicsSystem::createPhysicsActors(sPtr<Level>& level) {
		std::vector<PhysicsVehicleData*> vehicleDatas;
		std::vector<std::pair<Actor, int>> tireActors;
		auto vehicleView = level->registry.view<Components::VehicleComponent, Components::TransformComponent, Components::MobilityComponent>();
		for (auto& actor_ : vehicleView) {
			Actor vehicleActor = { actor_, level.get() };
			Actor fsBar = { vehicleActor.getComponent<Components::VehicleComponent>().frontSwayBar, level.get() };
			Actor rsBar = { vehicleActor.getComponent<Components::VehicleComponent>().rearSwayBar, level.get() };

			Actor wheelFL = { fsBar.getComponent<Components::SwayBarComponent>().wheels[0], level.get() };
			Actor wheelFR = { fsBar.getComponent<Components::SwayBarComponent>().wheels[1], level.get() };
			Actor wheelRL = { rsBar.getComponent<Components::SwayBarComponent>().wheels[0], level.get() };
			Actor wheelRR = { rsBar.getComponent<Components::SwayBarComponent>().wheels[1], level.get() };

			tireActors.push_back({wheelFL, vehicleDatas.size()});
			tireActors.push_back({wheelFR, vehicleDatas.size()});
			tireActors.push_back({wheelRL, vehicleDatas.size()});
			tireActors.push_back({wheelRR, vehicleDatas.size()});
			PhysicsVehicleData* vhd = new PhysicsVehicleData();
			vehicleDatas.push_back(vhd);
		}
		auto rigidbodyView = level->registry.view<Components::Rigidbody3DComponent, Components::TransformComponent, Components::MobilityComponent>();
		for (auto& actor_ : rigidbodyView) {
			Actor actor = { actor_, level.get() };
			auto& phys = actor.getComponent<Components::Rigidbody3DComponent>();
			auto& mobility = actor.getComponent<Components::MobilityComponent>();
			auto& transform = actor.getTransform();

			if (phys.instantiatedActor) continue;
			if (phys.asset.colliderType == PhysicsColliderType::Terrain) continue;
			PhyiscsActor physicsActor{};
			dBodyID body = dBodyCreate(physicsWorld);
			PhysicsBodyData* bdata = new PhysicsBodyData();
			dBodySetData(body, bdata);
			dBodySetAutoDisableFlag(body, true);
			dBodySetAutoDisableAverageSamplesCount(body, 40);
			dBodySetAutoDisableLinearThreshold(body, 0.001f);
			dBodySetAutoDisableAngularThreshold(body, 0.01f);
			physicsActor.body = body;
			dGeomID geometry;

			bool isStatic = false;

			if (mobility.mobility == Mobility::Static) {
				isStatic = true;
			}

			glm::mat3 rotOff{ // for cylinder and capsules
				{1.0, 0.0, 0.0},
				{0.0, 0.0, 1.0},
				{0.0, -1.0, 0.0},
			};

			PhysicsAsset physAsset = phys.usePhysicsAssetFromFile ? loadPhysicsAsset(phys.physicsAssetFromFile) : phys.asset;

			dMass mass;
			switch ((int)physAsset.colliderType) {
			case(0): // sphere
				mass.setSphereTotal(physAsset.mass, physAsset.sphereRadius);
				geometry = dCreateSphere(physicsSpace, physAsset.sphereRadius);
				break;
			case(1): // Box
				mass.setBoxTotal(physAsset.mass, physAsset.boxExtent.x * 2.0f, physAsset.boxExtent.y * 2.0f, physAsset.boxExtent.z * 2.0f);
				geometry = dCreateBox(physicsSpace, physAsset.boxExtent.x * 2.0f, physAsset.boxExtent.y * 2.0f, physAsset.boxExtent.z * 2.0f);
				break;
			case(2): // infinite plane
			{
				glm::mat4 realTransform = transform.transformMatrix;
				realTransform = glm::rotate(realTransform, -glm::half_pi<float>(), glm::vec3(1.f, 0.f, 0.f));
				const glm::vec3 planeNormal = glm::normalize(realTransform[2]); // convert rotation to direction
				const float d = glm::dot(planeNormal, glm::vec3(transform.transformMatrix[3]));
				geometry = dCreatePlane(physicsSpace, planeNormal.x, planeNormal.y, planeNormal.z, d);
				isStatic = true; // planes must be static
				break;
			}
			case(3): // Capsule
				mass.setCapsuleTotal(physAsset.mass, 2, physAsset.cylindricalRadius, physAsset.cylindricalLength - physAsset.cylindricalRadius * 2.0f);
				geometry = dCreateCapsule(physicsSpace, physAsset.cylindricalRadius, physAsset.cylindricalLength - physAsset.cylindricalRadius * 2.0f);
				break;
			case(4): // Cylinder
				mass.setCylinderTotal(physAsset.mass, 2, physAsset.cylindricalRadius, physAsset.cylindricalLength);
				geometry = dCreateCylinder(physicsSpace, physAsset.cylindricalRadius, physAsset.cylindricalLength);
				break;
			case(5):
			{ // convex hull
				mass.setParameters(physAsset.mass, physAsset.computedCOM.x, physAsset.computedCOM.y, physAsset.computedCOM.z,
					physAsset.computedInteriaTensor[0][0],
					physAsset.computedInteriaTensor[1][1],
					physAsset.computedInteriaTensor[2][2],
					physAsset.computedInteriaTensor[1][0],
					physAsset.computedInteriaTensor[2][0],
					physAsset.computedInteriaTensor[2][1]
				);
				mass.translate(-physAsset.computedCOM.x, -physAsset.computedCOM.y, -physAsset.computedCOM.z);
				RPhys::ConvexHull hull = RPhys::buildConvexHull(physAsset.convexPoints);
				std::vector<glm::vec4> planes = std::vector<glm::vec4>(hull.triangles.size());
				std::vector<uint32_t> polys;

				for (int i = 0; i < hull.triangles.size(); i++) {
					auto tri = hull.triangles[i];
					glm::vec3 a = hull.points[tri.a], b = hull.points[tri.b], c = hull.points[tri.c];
					glm::vec3 normal = glm::normalize(glm::cross(b - a, c - a));
					float d = glm::dot(a, normal);
					planes[i] = { normal, d };
					polys.push_back(3);
					if (glm::dot(glm::vec4(0, 0, 0, 1.0), glm::vec4(normal, d)) < 0.0f) {
						polys.push_back(tri.c); // make CCW
						polys.push_back(tri.b);
						polys.push_back(tri.a);
					} else {
						polys.push_back(tri.a);
						polys.push_back(tri.b);
						polys.push_back(tri.c);
					}
				}

				// is this a memory leak? not sure if ODE deals with the convex data or if i have to free it after the geometry is destroyed?
				glm::vec4* planesv = (glm::vec4*)malloc(planes.size() * 16);
				glm::vec3* pointsv = (glm::vec3*)malloc(hull.points.size() * 12);
				uint32_t* polysv = (uint32_t*)malloc(polys.size() * 4);

				memcpy(planesv, planes.data(), planes.size() * 16);
				memcpy(pointsv, hull.points.data(), hull.points.size() * 12);
				memcpy(polysv, polys.data(), polys.size() * 4);

				geometry = dCreateConvex(physicsSpace, (dReal*)planesv, planes.size(), (dReal*)pointsv, hull.points.size(), (uint32_t*)polysv);
			} break; 
			case(6):
			{ // trimesh
				MeshRawData* mesh = ModelImporter::loadMeshData(ResourceSystem::getVirtualMesh3DLocation(phys.asset.trimeshAsset));
				if (mesh) {
					dTriMeshDataID trimesh = dGeomTriMeshDataCreate();
					trimeshData.push_back(trimesh);
					std::vector<glm::vec3> normals;
					normals.resize(mesh->submeshes[0]->vtxCount);
					for (int i = 0; i < mesh->submeshes[0]->vtxCount; i++) {
						normals[i] = LOWP_NVEC3_UNPACK(mesh->submeshes[0]->vtxNormals[i].cmp_normal);
					}
					dGeomTriMeshDataBuildSingle1(trimesh,
						mesh->submeshes[0]->vtxPositions, sizeof(VTX_pos_offset_12b_), mesh->submeshes[0]->vtxCount,
						mesh->submeshes[0]->vtxIndices, mesh->submeshes[0]->indCount, 3 * sizeof(uint32_t),
						normals.data());
					//dGeomTriMeshDataPreprocess2(trimesh, (1u << dTRIDATAPREPROCESS_BUILD_FACE_ANGLES), nullptr);
					geometry = dCreateTriMesh(physicsSpace, trimesh, nullptr, nullptr, nullptr);
					//dGeomTriMeshEnableTC(geometry, dTriMeshClass, 1);
				} else {
					continue; // invalid
				}
			} break;
			}
			if (physAsset.enableCustomCOM && physAsset.colliderType != PhysicsColliderType::ConvexHullAsset) {
				mass.c[0] = physAsset.computedCOM.x;
				mass.c[1] = physAsset.computedCOM.y;
				mass.c[2] = physAsset.computedCOM.z;
				mass.translate(-physAsset.computedCOM.x, -physAsset.computedCOM.y, -physAsset.computedCOM.z);
			}
			bool isTire = false;
			std::pair<Actor, int> tirePairData;
			for (int i = 0; i < tireActors.size(); i++) {
				if (tireActors[i].first == actor) {
					tirePairData = tireActors[i];
					isTire = true;
					break;
				}
			}
			if (isTire) {
				PhysicsTireActorData* data = new PhysicsTireActorData();
				data->material = loadTirePhysicsMaterial(phys.material);
				data->vehicleData = vehicleDatas[tirePairData.second];
				data->actor = actor;
				dGeomSetData(geometry, data);
			} else {
				PhysicsActorData* data = new PhysicsActorData();
				data->material = loadPhysicsMaterial(phys.material);
				data->isTriggerVolume = !phys.enableCollision;
				data->actor = actor;
				dGeomSetData(geometry, data);
			}

			if (!isStatic) {
				dBodySetLinearVel(body, phys.linearVelocity.x, phys.linearVelocity.y, phys.linearVelocity.z);
				dBodySetAngularVel(body, phys.angularVelocity.x, phys.angularVelocity.y, phys.angularVelocity.z);
				dBodySetMass(body, &mass);
				dGeomSetBody(geometry, body);
				dBodyEnable(body);

				// TODO: fix this provoking bad simulations
				//if (phys.finiteRotation) { // TODO: make this correlate to the axis they connect to
				//	dBodySetFiniteRotationMode(body, 1);
				//	dBodySetFiniteRotationAxis(body, 1.0, 0.0, 0.0);
				//	bdata->rotationAxisWorld = glm::inverse(transform.getRotationMatrix()) * glm::vec3{ 1, 0, 0 };
				//} else {
				//	dBodySetMaxAngularSpeed(body, 40.0f * 6.283185307f);
				//}
			}

			glm::mat3 rotation = transform.getRotationMatrix();

			if ((int)physAsset.colliderType == 3 || (int)physAsset.colliderType == 4) {
				if (isStatic) {
					rotation = rotOff * rotation;
				} else {
					dMatrix3 off{
						rotOff[0][0],rotOff[1][0],rotOff[2][0],0.f,
						rotOff[0][1],rotOff[1][1],rotOff[2][1],0.f,
						rotOff[0][2],rotOff[1][2],rotOff[2][2],0.f
					};
					dGeomSetOffsetRotation(geometry, off);
				}
			} else if (physAsset.colliderType == PhysicsColliderType::ConvexHullAsset) {

			}

			glm::quat quaternion = glm::toQuat(rotation);
			if (physAsset.colliderType != PhysicsColliderType::Plane) {
				dGeomSetPosition(geometry, transform.transformMatrix[3].x, transform.transformMatrix[3].y, transform.transformMatrix[3].z);
				dGeomSetQuaternion(geometry, (dQuaternion&)quaternion);
			}

			dGeomEnable(geometry);

			physicsActor.geometry = geometry;
			phys.instantiatedActor = (size_t)geometry;
			actors.push_back(physicsActor);
			if (!isStatic) {
				previousTransformStates->bodies[body] = { transform.transformMatrix[3], quaternion };
				previousTransformStates->bodies[body].colliderType = phys.asset.colliderType;
			}
		}

		struct ConstraintPairInfo {
			glm::vec3 point;
			PhysicsConstraintType constraintType = PhysicsConstraintType::Fixed;
			glm::vec3 axis;
			glm::vec3 axisPerp;
			glm::vec3 axisPerp2;
			size_t* actor;
			float suspensionSoftness;
			float hingeAngle;
			float hingeAngleMin;
			float hingeAngleMax;

			float fudgeFactor;
			float bounce;
			float stopCFM;

			float sliderPosition;
		};


		std::unordered_map<BodyPair, ConstraintPairInfo> constraints;

		level->registry.view<Components::PhysicsConstraintComponent, Components::TransformComponent>()
			.each([&](Components::PhysicsConstraintComponent& cnstr, Components::TransformComponent& transform) {
			if (cnstr.instantiatedActor) return;
			Actor actorA = { cnstr.actorA, level.get() };
			Actor actorB = { cnstr.actorB, level.get() };
			if (!actorA.isInvalid() && !actorB.isInvalid()) {
				if (actorA.hasComponent<Components::Rigidbody3DComponent>() && actorB.hasComponent<Components::Rigidbody3DComponent>()) {
					auto& rbccA = actorA.getComponent<Components::Rigidbody3DComponent>();
					auto& rbccB = actorB.getComponent<Components::Rigidbody3DComponent>();
					BodyPair pair = { dGeomGetBody((dGeomID)rbccA.instantiatedActor), dGeomGetBody((dGeomID)rbccB.instantiatedActor) };
					if (constraints.find(pair) == constraints.end()) {
						ConstraintPairInfo constraint{};
						constraint.point = transform.transformMatrix[3];
						glm::mat4 realTransform = transform.transformMatrix;
						realTransform = glm::rotate(realTransform, -glm::half_pi<float>(), glm::vec3(1.f, 0.f, 0.f));
						constraint.axis = glm::normalize(realTransform[2]); // convert rotation to direction

						Components::TransformComponent tf{};
						tf.setRotation(cnstr.perpendicularAxisOffsetRotation);

						constraint.axisPerp = glm::normalize((tf.calculateMat4() * glm::mat4(realTransform))[2]);
						constraint.axisPerp2 = glm::normalize(realTransform[0]);
						constraint.constraintType = cnstr.constraintType;
						constraint.suspensionSoftness = cnstr.suspensionSoftness;
						constraint.actor = &cnstr.instantiatedActor;

						constraint.stopCFM = cnstr.spring;
						constraint.fudgeFactor = cnstr.fudge;
						constraint.bounce = cnstr.bounce;

						if (cnstr.limitConstraint) {
							constraint.hingeAngleMax = cnstr.limitMax;
							constraint.hingeAngleMin = cnstr.limitMin;
						} else {
							constraint.hingeAngleMax = dInfinity;
							constraint.hingeAngleMin = -dInfinity;
						}
						constraints[pair] = constraint;

						reinterpret_cast<PhysicsActorData*>(dGeomGetData((dGeomID)rbccA.instantiatedActor))->connectedBodies.push_back(pair.bodyB);
						reinterpret_cast<PhysicsActorData*>(dGeomGetData((dGeomID)rbccB.instantiatedActor))->connectedBodies.push_back(pair.bodyA);
					}
				}
			}
		});

		level->registry.view<Components::IndependentSuspensionComponent, Components::TransformComponent>()
			.each([&](Components::IndependentSuspensionComponent& cnstr, Components::TransformComponent& transform) {
			if (cnstr.suspensionActor) return;
			Actor chassis = { cnstr.chassis, level.get() };
			Actor wheel = { cnstr.wheel, level.get() };
			if (!chassis.isInvalid() && !wheel.isInvalid()) {
				if (chassis.hasComponent<Components::Rigidbody3DComponent>() && wheel.hasComponent<Components::Rigidbody3DComponent>()) {
					auto& rbccC = chassis.getComponent<Components::Rigidbody3DComponent>();
					auto& rbccW = wheel.getComponent<Components::Rigidbody3DComponent>();
					dBodyID axleBody = dBodyCreate(physicsWorld); // TODO: fix memory leak from body not being destroyed
					glm::vec3 wheelPosition = wheel.getTransform().transformMatrix[3];
					glm::vec3 suspensionPosition = transform.transformMatrix[3];
					glm::vec3 suspensionAxis;
					{
						glm::mat4 realTransform = transform.transformMatrix;
						realTransform = glm::rotate(realTransform, -glm::half_pi<float>(), glm::vec3(1.f, 0.f, 0.f));
						suspensionAxis = glm::normalize(realTransform[2]); // convert rotation to direction
					} // place the axle adjacent to the wheel, but along the suspension axis
					glm::vec3 axlePosition = suspensionPosition + suspensionAxis * glm::dot(suspensionAxis, wheelPosition - suspensionPosition);


					dBodySetPosition(axleBody, axlePosition.x, axlePosition.y, axlePosition.z);
					//SHARD3D_INFO("{0},{1},{2}", axlePosition.x, axlePosition.y, axlePosition.z);
					dBodyEnable(axleBody); // HELP FUCKING WORKKK 
					cnstr.axleBodyActor = (size_t)axleBody;
					dBodyID chassisBody = dGeomGetBody((dGeomID)rbccC.instantiatedActor);
					dBodyID wheelBody = dGeomGetBody((dGeomID)rbccW.instantiatedActor);

					{ // chassis<->axle
						BodyPair pair = { chassisBody, axleBody };
						ConstraintPairInfo constraint{};
						constraint.point = suspensionPosition;
						constraint.axis = suspensionAxis;

						constraint.constraintType = PhysicsConstraintType::Piston;
						constraint.actor = &cnstr.suspensionActor;

						constraint.stopCFM = 0.f;
						constraint.fudgeFactor = 0.f;
						constraint.bounce = 0.f;

						constraint.hingeAngleMax = dInfinity;
						constraint.hingeAngleMin = cnstr.baseHeight;
						constraints[pair] = constraint;
					}
					{ // axle<->wheel
						BodyPair pair = { axleBody, wheelBody };
						ConstraintPairInfo constraint{};
						constraint.point = axlePosition;
						glm::mat4 realTransform = transform.transformMatrix;
						realTransform = glm::rotate(realTransform, -glm::half_pi<float>(), glm::vec3(1.f, 0.f, 0.f));

						Components::TransformComponent tf{};
						tf.setRotation(cnstr.perpendicularAxisOffsetRotation);
						constraint.axis = glm::normalize((tf.calculateMat4() * glm::mat4(realTransform))[2]);

						constraint.constraintType = PhysicsConstraintType::Hinge;
						constraint.actor = &cnstr.axleActor;

						constraint.stopCFM = 0.f;
						constraint.fudgeFactor = 0.f;
						constraint.bounce = 0.f;

						constraint.hingeAngleMax = dInfinity;
						constraint.hingeAngleMin = -dInfinity;
						constraints[pair] = constraint;
					}

					reinterpret_cast<PhysicsActorData*>(dGeomGetData((dGeomID)rbccC.instantiatedActor))->connectedBodies.push_back(wheelBody);
					//reinterpret_cast<PhysicsActorData*>(dGeomGetData((dGeomID)rbccW.instantiatedActor))->connectedBodies.push_back(chassisBody);
				}
			}
		});

		for (auto& [pair, c] : constraints) {
			dJointID joint;
			switch (c.constraintType) {
			case(PhysicsConstraintType::Fixed):
				joint = dJointCreateFixed(physicsWorld, universalJointGroup);
				dJointAttach(joint, pair.bodyA, pair.bodyB);
				dJointSetFixed(joint);
				dJointSetFixedParam(joint, dParamStopCFM, c.stopCFM);
				dJointSetFixedParam(joint, dParamFudgeFactor, c.fudgeFactor);
				dJointSetFixedParam(joint, dParamBounce, c.fudgeFactor);
				break;
			case(PhysicsConstraintType::Ball):
				joint = dJointCreateBall(physicsWorld, universalJointGroup);
				dJointAttach(joint, pair.bodyA, pair.bodyB);
				dJointSetBallAnchor(joint, c.point.x, c.point.y, c.point.z);
				break;
			case(PhysicsConstraintType::Hinge):
				joint = dJointCreateHinge(physicsWorld, universalJointGroup);
				dJointAttach(joint, pair.bodyA, pair.bodyB);
				dJointSetHingeAxis(joint, c.axis.x, c.axis.y, c.axis.z);
				dJointSetHingeAnchor(joint, c.point.x, c.point.y, c.point.z);

				dJointSetHingeParam(joint, dParamLoStop, c.hingeAngleMin);
				dJointSetHingeParam(joint, dParamHiStop, c.hingeAngleMax);
				dJointSetHingeParam(joint, dParamLoStop, c.hingeAngleMin);
				dJointSetHingeParam(joint, dParamHiStop, c.hingeAngleMax);

				dJointSetHingeParam(joint, dParamStopCFM, c.stopCFM);
				dJointSetHingeParam(joint, dParamFudgeFactor, c.fudgeFactor);
				dJointSetHingeParam(joint, dParamBounce, c.fudgeFactor);
				break;
			case(PhysicsConstraintType::DualHinge):
				joint = dJointCreateHinge2(physicsWorld, universalJointGroup);
				dJointAttach(joint, pair.bodyA, pair.bodyB);
				{
					dReal axis1[4]{ c.axisPerp.x, c.axisPerp.y, c.axisPerp.z };
					dReal axis2[4]{ c.axis.x, c.axis.y, c.axis.z };
					dJointSetHinge2Axes(joint, axis1, axis2);
				}
				dJointSetHinge2Anchor(joint, c.point.x, c.point.y, c.point.z);
				dJointSetHinge2Param(joint, dParamSuspensionCFM1, c.suspensionSoftness);
				dJointSetHinge2Param(joint, dParamLoStop1, 0.0);
				dJointSetHinge2Param(joint, dParamHiStop1, 0.0);
				dJointSetHinge2Param(joint, dParamLoStop1, 0.0);
				dJointSetHinge2Param(joint, dParamHiStop1, 0.0);

				dJointSetHinge2Param(joint, dParamLoStop2, c.hingeAngleMin);
				dJointSetHinge2Param(joint, dParamHiStop2, c.hingeAngleMax);
				dJointSetHinge2Param(joint, dParamLoStop2, c.hingeAngleMin);
				dJointSetHinge2Param(joint, dParamHiStop2, c.hingeAngleMax);

				dJointSetHinge2Param(joint, dParamStopCFM2, c.stopCFM);
				dJointSetHinge2Param(joint, dParamFudgeFactor2, c.fudgeFactor);
				dJointSetHinge2Param(joint, dParamBounce2, c.fudgeFactor);

				break;
			case(PhysicsConstraintType::Piston):
				joint = dJointCreatePiston(physicsWorld, universalJointGroup);
				dJointAttach(joint, pair.bodyA, pair.bodyB);
				dJointSetPistonAxis(joint, c.axis.x, c.axis.y, c.axis.z);
				dJointSetPistonAnchor(joint, c.point.x, c.point.y, c.point.z);

				dJointSetPistonParam(joint, dParamLoStop, c.hingeAngleMin);
				dJointSetPistonParam(joint, dParamHiStop, c.hingeAngleMax);
				dJointSetPistonParam(joint, dParamLoStop, c.hingeAngleMin);
				dJointSetPistonParam(joint, dParamHiStop, c.hingeAngleMax);

				dJointSetPistonParam(joint, dParamLoStop2, 0.f);
				dJointSetPistonParam(joint, dParamHiStop2, 0.f);
				dJointSetPistonParam(joint, dParamLoStop2, 0.f);
				dJointSetPistonParam(joint, dParamHiStop2, 0.f);

				dJointSetPistonParam(joint, dParamStopCFM, c.stopCFM);
				dJointSetPistonParam(joint, dParamFudgeFactor, c.fudgeFactor);
				dJointSetPistonParam(joint, dParamBounce, c.fudgeFactor);
				break;
			case(PhysicsConstraintType::Slider):
				joint = dJointCreateSlider(physicsWorld, universalJointGroup);
				dJointAttach(joint, pair.bodyA, pair.bodyB);
				dJointSetSliderAxis(joint, c.axis.x, c.axis.y, c.axis.z);

				dJointSetSliderParam(joint, dParamLoStop, c.hingeAngleMin);
				dJointSetSliderParam(joint, dParamHiStop, c.hingeAngleMax);
				dJointSetSliderParam(joint, dParamLoStop, c.hingeAngleMin);
				dJointSetSliderParam(joint, dParamHiStop, c.hingeAngleMax);

				dJointSetSliderParam(joint, dParamStopCFM, c.stopCFM);
				dJointSetSliderParam(joint, dParamFudgeFactor, c.fudgeFactor);
				dJointSetSliderParam(joint, dParamBounce, c.fudgeFactor);
				break;
				//case(PhysicsConstraintType::AngularMotor):
				//	joint = dJointCreateAMotor(physicsWorld, universalJointGroup);
				//	dJointAttach(joint, pair.bodyA, pair.bodyB);
				//	dJointSetAMotorMode(joint, dAMotorEuler);
				//	//dJointSetAMotorNumAxes(joint, 1);
				//
				//	dJointSetAMotorAxis(joint, 0, 1, c.axis.x, c.axis.y, c.axis.z);
				//	dJointSetAMotorAxis(joint, 2, 2, c.axisPerp.x, c.axisPerp.y, c.axisPerp.z);
				//	//dJointSetAMotorAxis(joint, 3, 0, c.axisPerp2.x, c.axisPerp2.y, c.axisPerp2.z);
				//	//dJointSetAMotorAngle(joint, 1, 0.75f);
				//	break;
			default:
				throw std::invalid_argument("invalid joint type (as of current build)");
			}

			*c.actor = (size_t)joint;
			dJointEnable(joint);
		}
		
		for (auto& actor_ : vehicleView) {
			Actor vehicleActor = { actor_, level.get() };
			Actor chassisActor = { vehicleActor.getComponent<Components::VehicleComponent>().chassis, level.get() };
			Actor fsBar = { vehicleActor.getComponent<Components::VehicleComponent>().frontSwayBar, level.get() };
			Actor rsBar = { vehicleActor.getComponent<Components::VehicleComponent>().rearSwayBar, level.get() };

			Actor iSusFL = { fsBar.getComponent<Components::SwayBarComponent>().suspension[0], level.get() };
			Actor iSusFR = { fsBar.getComponent<Components::SwayBarComponent>().suspension[1], level.get() };
			Actor iSusRL = { rsBar.getComponent<Components::SwayBarComponent>().suspension[0], level.get() };
			Actor iSusRR = { rsBar.getComponent<Components::SwayBarComponent>().suspension[1], level.get() };
			Actor wheelFL = { fsBar.getComponent<Components::SwayBarComponent>().wheels[0], level.get() };
			Actor wheelFR = { fsBar.getComponent<Components::SwayBarComponent>().wheels[1], level.get() };
			Actor wheelRL = { rsBar.getComponent<Components::SwayBarComponent>().wheels[0], level.get() };
			Actor wheelRR = { rsBar.getComponent<Components::SwayBarComponent>().wheels[1], level.get() };


			PhysicsVehicleData* vhd{}; // NOT DESTROYED AFTER USED!! memory leak when physics simulation ends
			for (int i = 0; i < tireActors.size(); i++) {
				if (tireActors[i].first == wheelFL) {
					vhd = vehicleDatas[tireActors[i].second];
				}
			}
			SHARD3D_ASSERT(vhd);
			vhd->chassis = (dGeomID)chassisActor.getComponent<Components::Rigidbody3DComponent>().instantiatedActor;
			//vhd->indepenedentSuspensionFL = (dBodyID)iSusFL.getComponent<Components::IndependentSuspensionComponent>().suspensionActor;
			//vhd->indepenedentSuspensionFR = (dBodyID)iSusFR.getComponent<Components::IndependentSuspensionComponent>().suspensionActor;
			//vhd->indepenedentSuspensionRL = (dBodyID)iSusRL.getComponent<Components::IndependentSuspensionComponent>().suspensionActor;
			//vhd->indepenedentSuspensionRR = (dBodyID)iSusRR.getComponent<Components::IndependentSuspensionComponent>().suspensionActor;
			vhd->wheelFL = (dGeomID)wheelFL.getComponent<Components::Rigidbody3DComponent>().instantiatedActor;
			vhd->wheelFR = (dGeomID)wheelFR.getComponent<Components::Rigidbody3DComponent>().instantiatedActor;
			vhd->wheelRL = (dGeomID)wheelRL.getComponent<Components::Rigidbody3DComponent>().instantiatedActor;
			vhd->wheelRR = (dGeomID)wheelRR.getComponent<Components::Rigidbody3DComponent>().instantiatedActor;
			
			const glm::vec3 axlePlaneRight = wheelRR.getTransform().getTranslationWorld() - wheelRL.getTransform().getTranslationWorld();
			const glm::vec3 axlePlaneFront = wheelFL.getTransform().getTranslationWorld() - wheelRL.getTransform().getTranslationWorld();
			const glm::vec3 axlePlaneNormal = glm::normalize(glm::cross(axlePlaneRight, axlePlaneFront));
			const float axlePlaneD = glm::dot(axlePlaneNormal,
				(wheelRR.getTransform().getTranslationWorld() + wheelRL.getTransform().getTranslationWorld() +
				wheelFL.getTransform().getTranslationWorld() + wheelFR.getTransform().getTranslationWorld()) / 4.0f
			);
			vhd->cgRelHeight = glm::dot(glm::vec4(chassisActor.getComponent<Components::Rigidbody3DComponent>().asset.computedCOM, 1.0f), 
				glm::vec4(axlePlaneNormal, axlePlaneD));

			const glm::vec3 frontAxle = (wheelFL.getTransform().getTranslationWorld() + wheelFR.getTransform().getTranslationWorld()) / 2.0f;
			float frontHipoteneuse = glm::distance(chassisActor.getTransform().getTranslationWorld() + chassisActor.getComponent<Components::Rigidbody3DComponent>().asset.computedCOM, frontAxle);
			float cosFront = glm::abs(vhd->cgRelHeight) / frontHipoteneuse;
			vhd->cgToFrontAxle = std::sin(std::acos(cosFront))* frontHipoteneuse;

			const glm::vec3 rearAxle = (wheelRL.getTransform().getTranslationWorld() + wheelRR.getTransform().getTranslationWorld()) / 2.0f;
			float rearHipoteneuse = glm::distance(chassisActor.getTransform().getTranslationWorld() + chassisActor.getComponent<Components::Rigidbody3DComponent>().asset.computedCOM, rearAxle);
			float cosRear = glm::abs(vhd->cgRelHeight) / rearHipoteneuse;
			vhd->cgToRearAxle = std::sin(std::acos(cosRear)) * rearHipoteneuse;

			vhd->wheelBase = vhd->cgToRearAxle + vhd->cgToFrontAxle;

			vehicleDatas.push_back(vhd);
		}
	}
	PhysicsCollisionData PhysicsSystem::rayCast(glm::vec3 origin, glm::vec3 direction, float maxDistance) {
		PhysicsCollisionData collisionData;
		RayTestData rd{};
		rd.world = physicsWorld;
		rd.contactGroup = collisionContactGroup;
		rd.hitPosition = { 0.f, 0.f, 0.f, dInfinity };
		rd.hitActor = Actor();
		dGeomID ray = dCreateRay(physicsRays, maxDistance);
		glm::vec3 range = direction * maxDistance;
		dGeomRaySet(ray, origin.x, origin.y, origin.z, range.x, range.y, range.z);
		dSpaceCollide2(ray, (dGeomID)physicsSpace, &rd, &rayCallback);
		dGeomDestroy(ray);
		collisionData.actor = rd.hitActor;
		collisionData.position = rd.hitPosition;
		collisionData.normal = rd.hitNormal;
		return collisionData;
	}
	void PhysicsSystem::simulate(sPtr<Level>& level) {
		for (auto& [body, transform] : previousTransformStates->bodies) {
			transform.translation = *(glm::vec3*)dBodyGetPosition(body);
			transform.rotation = *(glm::quat*)dBodyGetQuaternion(body);

			transform.linearVelocity = *(glm::vec3*)dBodyGetLinearVel(body);
			transform.angularVelocity = *(glm::vec3*)dBodyGetAngularVel(body);

			if (dBodyGetFiniteRotationMode(body)) {
				PhysicsBodyData& data = *(PhysicsBodyData*)dBodyGetData(body);

				glm::vec3 axis = glm::mat3_cast(transform.rotation) * data.rotationAxisWorld;
			//	dBodySetFiniteRotationAxis(body, axis.x, axis.y, axis.z);
			}

			//if (dBodyGetNumJoints(body) > 4) { // downforce test
			//	float v = glm::length(transform.linearVelocity);
			//	dBodyAddForceAtRelPos(body, 0.0, -v * 0.29f * stepSize, -v * v * v * 0.2f * stepSize, 0.6, 0.0, 1.5);
			//	dBodyAddForceAtRelPos(body, 0.0, -v * 0.29f * stepSize, -v * v * v * 0.2f * stepSize, -0.6, 0.0, 1.5);
			//	dBodyAddForceAtRelPos(body, 0.0, -v * 0.99f * stepSize, -v * v * v * 0.9f * stepSize, 0.6, 0.4, -1.5);
			//	dBodyAddForceAtRelPos(body, 0.0, -v * 0.99f * stepSize, -v * v * v * 0.9f * stepSize, -0.6, 0.4, -1.5);
			//}

			if (transform.colliderType == PhysicsColliderType::MeshAsset) {
				glm::mat4 matrix = glm::transpose(glm::translate(glm::toMat4(transform.rotation), transform.translation));
				dGeomID trimeshGeom = dBodyGetFirstGeom(body);
				dGeomTriMeshSetLastTransform(trimeshGeom, *(dMatrix4*)&matrix);
			}
		}

		for (auto actor_ : level->registry.view<Components::Rigidbody3DComponent>()) {
			Actor actor = { actor_, level.get() };
			actor.getComponent<Components::Rigidbody3DComponent>().collidingWith.clear();
		}

		CollisionData cd{};
		cd.world = physicsWorld;
		cd.contactGroup = collisionContactGroup;
		cd.ts = stepSize;
		dWorldGetGravity(physicsWorld, (dVector3&)cd.gravity);
		dSpaceCollide(physicsSpace, &cd, &collisionCallback);

		for (auto* vhd : vehicleDatas) {
			glm::vec3 currVel = *(glm::vec3*)dBodyGetLinearVel(dGeomGetBody(vhd->chassis));
			vhd->acceleration = currVel - vhd->lastVelocity;
			vhd->lastVelocity = currVel;
		}

		if (1)level->registry.view<Components::IndependentSuspensionComponent>()
			.each([&](Components::IndependentSuspensionComponent& suspension) {

			dJointID suspensionRod = (dJointID)suspension.suspensionActor;
			if (!suspensionRod) return;
			dBodyID wheel = dGeomGetBody((dGeomID)Actor(suspension.wheel, level.get()).getComponent<Components::Rigidbody3DComponent>().instantiatedActor);
			dBodyID chassis = dGeomGetBody((dGeomID)Actor(suspension.chassis, level.get()).getComponent<Components::Rigidbody3DComponent>().instantiatedActor);

			dMass chassisMass;
			dBodyGetMass(chassis, &chassisMass);
			dMass wheelMass;
			dBodyGetMass(wheel, &wheelMass);
			float sprungCornerMass = chassisMass.mass / 4.0f; // todo: weight distribution based on center of mass

			glm::vec3 suspensionAxis;
			float displacement_m;
			float displacementVelocity_m;
			glm::vec3 pointOnChassis;
			glm::vec3 pointOnWheel;

			displacement_m = dJointGetPistonPosition(suspensionRod);
			displacementVelocity_m = dJointGetPistonPositionRate(suspensionRod);
			dJointGetPistonAxis(suspensionRod, (dVector3&)suspensionAxis);
			dJointGetPistonAnchor(suspensionRod, (dVector3&)pointOnChassis);
			dJointGetPistonAnchor2(suspensionRod, (dVector3&)pointOnWheel);

			// suspension force
			constexpr float FOUR_PI_SQUARED = 4.0f * glm::pi<float>() * glm::pi<float>();
			const float rate_N = FOUR_PI_SQUARED * suspension.frequency * suspension.frequency * sprungCornerMass;
			float force = rate_N * displacement_m;
			
			// damper force

			glm::vec3 chassisVelocity;
			glm::vec3 wheelVelocity;
			dBodyGetPointVel(chassis, pointOnChassis.x, pointOnChassis.y, pointOnChassis.z, (dVector3&)chassisVelocity);
			dBodyGetPointVel(wheel, pointOnWheel.x, pointOnWheel.y, pointOnWheel.z, (dVector3&)wheelVelocity);

			float df = glm::dot(chassisVelocity, suspensionAxis) - glm::dot(wheelVelocity, suspensionAxis);

			float dp2 = displacement_m * displacement_m;
			float potentialSpringEnergy = 0.5f * rate_N * dp2;
			float MAX_DISP_VELOCITY = 10.0f;
			float dampingForce = 2.0f * suspension.damping * glm::min(abs(displacementVelocity_m), abs(MAX_DISP_VELOCITY)) * potentialSpringEnergy; // b * (dx/dt) + m * (d�x/dt�) == 0 for ideal dampening

			float netForce = force + glm::sign(displacementVelocity_m) * dampingForce;

			//SHARD3D_INFO("\nsuspension force: {0}\ndamping force:    {1}\nnet force:	  {2}", force, dampingForce, netForce);
			dJointAddPistonForce(suspensionRod, -netForce);

			//glm::vec3 vecForce = suspensionAxis * netForce;
			//dBodyAddForce(wheel, vecForce.x, vecForce.y, vecForce.z);
			//dBodyAddForceAtPos(chassis, -vecForce.x, -vecForce.y, -vecForce.z, pointOnChassis.x, pointOnChassis.y, pointOnChassis.z);
		});
		level->registry.view<Components::SwayBarComponent>()
			.each([&](Components::SwayBarComponent& cnstr) {

			glm::vec3 bodyPoint;
			glm::vec3 hingePoint;
			glm::vec3 axis;
			float displacement;
			
			dJointID pistons[2] = { 
				(dJointID)Actor(cnstr.suspension[0], level.get()).getComponent<Components::IndependentSuspensionComponent>().suspensionActor,
				(dJointID)Actor(cnstr.suspension[1], level.get()).getComponent<Components::IndependentSuspensionComponent>().suspensionActor
			};
			if (!pistons[0]) return;
			dBodyID wheels[2] = {
				dGeomGetBody((dGeomID)Actor(cnstr.wheels[0], level.get()).getComponent<Components::Rigidbody3DComponent>().instantiatedActor),
				dGeomGetBody((dGeomID)Actor(cnstr.wheels[1], level.get()).getComponent<Components::Rigidbody3DComponent>().instantiatedActor)
			};
			dBodyID chassis = dGeomGetBody((dGeomID)Actor(cnstr.chassis, level.get()).getComponent<Components::Rigidbody3DComponent>().instantiatedActor);

			for (int i = 0; i < 2; i++) {
				dJointGetPistonAnchor(pistons[i], &bodyPoint.x);
				dJointGetPistonAnchor2(pistons[i], &hingePoint.x);
				dJointGetPistonAxis(pistons[i], &axis.x);
				displacement = -dJointGetPistonPosition(pistons[i]);
				if (displacement > 0.0f) {
					float amt = glm::min(cnstr.swayForceMAX, displacement * cnstr.swayForce);
					dJointAddPistonForce(pistons[i], -amt);
					dJointAddPistonForce(pistons[i ^ 1], amt);
				}
			}
		});

		if (quickStep) {
			dWorldQuickStep(physicsWorld, stepSize);
		} else {
			dWorldStep(physicsWorld, stepSize);
		}
		dJointGroupEmpty(collisionContactGroup);
	}
	void PhysicsSystem::applyTransform(sPtr<Level>& level, float factor) {
		for (auto& ac : level->registry.view<Components::Rigidbody3DComponent, Components::TransformComponent, Components::DynamicActorComponent>()) {
			Actor actor = { ac , level.get() };
			Components::Rigidbody3DComponent& phys = actor.getComponent<Components::Rigidbody3DComponent>();
			Components::TransformComponent& transform = actor.getComponent<Components::TransformComponent>();
			dBodyID body = dGeomGetBody((dGeomID&)phys.instantiatedActor);



			glm::vec3 prevLinVelocity = previousTransformStates->bodies[body].linearVelocity;
			glm::vec3 prevAngVelocity = previousTransformStates->bodies[body].angularVelocity;

			glm::vec3 nextLinVelocity = *(glm::vec3*)dBodyGetLinearVel(body);
			glm::vec3 nextAngVelocity = *(glm::vec3*)dBodyGetAngularVel(body);

			glm::vec3 linVelocity = glm::mix(prevLinVelocity, nextLinVelocity, factor);
			glm::vec3 angVelocity = glm::mix(prevAngVelocity, nextAngVelocity, factor);

			phys.linearVelocity = { linVelocity.x, linVelocity.y, linVelocity.z };
			phys.angularVelocity = { angVelocity.x, angVelocity.y, angVelocity.z };


			glm::vec3 prevTransform = previousTransformStates->bodies[body].translation;
			glm::vec4 prevQuaternion = (glm::vec4&)previousTransformStates->bodies[body].rotation;

			glm::vec3 nextTransform = *(glm::vec3*)dBodyGetPosition(body);
			glm::vec4 nextQuaternion = *(glm::vec4*)dBodyGetQuaternion(body);

			glm::vec3 translation = glm::mix(prevTransform, nextTransform, factor);
			// custom slerp because glm provides incorrect results
			using namespace glm;
			vec4 slerped;
			slerped = nextQuaternion;

			float cosTheta = dot(prevQuaternion, nextQuaternion);

			// If cosTheta < 0, the interpolation will take the long way around the sphere.
			// To fix this, one quat must be negated.
			if (cosTheta < 0.0f) {
				slerped = -nextQuaternion;
				cosTheta = -cosTheta;
			}

			// Perform a linear interpolation when cosTheta is close to 1 to avoid side effect of sin(angle) becoming a zero denominator
			if (cosTheta > static_cast<float>(1) - epsilon<float>()) {
				// Linear interpolation

				slerped = vec4(
					mix(prevQuaternion.x, nextQuaternion.x, factor),
					mix(prevQuaternion.y, nextQuaternion.y, factor),
					mix(prevQuaternion.z, nextQuaternion.z, factor),
					mix(prevQuaternion.w, nextQuaternion.w, factor));
			} else {
				// Essential Mathematics, page 467
				float angle = acos(cosTheta);
				slerped = (sin((static_cast<float>(1) - factor) * angle) * prevQuaternion + sin(factor * angle) * slerped) / sin(angle);
			}

			bool hasParent = false;
			if (actor.hasRelationship()) {
				if (actor.getComponent<Components::RelationshipComponent>().parentActor != entt::null) {
					hasParent = true;
				}
			}

			glm::quat quaternion = (glm::quat&)slerped;
			glm::mat3 rotation = glm::toMat3(glm::normalize(quaternion));

			//const dReal* rotationt = dBodyGetRotation(body);
			//
			//rotation = {
			//	rotationt[0], rotationt[4], rotationt[8],
			//	rotationt[1], rotationt[5], rotationt[9],
			//	rotationt[2], rotationt[6], rotationt[10]
			//};

			if (hasParent) { // fix the position if parented (hopefully parent isnt scaled because that fucks with the entire physics system)
				glm::mat4 actorTransform = rotation;
				actorTransform[3] = glm::vec4(translation, 1.0f);
				auto& ptc = Actor(actor.getComponent<Components::RelationshipComponent>().parentActor, level.get()).getTransform();
				glm::mat4 parentTransform = ptc.transformMatrix;

				glm::vec3 invParentScale = glm::vec3(1.0) / glm::vec3{ glm::length(parentTransform[0]),glm::length(parentTransform[1]), glm::length(parentTransform[2]) };

				glm::mat4 bullshit = glm::inverse(parentTransform) * actorTransform;

				actorTransform[0] *= invParentScale.x;
				actorTransform[1] *= invParentScale.y;
				actorTransform[2] *= invParentScale.z;

				glm::mat3 parentRotation = glm::mat3(
					 parentTransform[0] * invParentScale.x,
					 parentTransform[1] * invParentScale.y,
					 parentTransform[2] * invParentScale.z
				);

				rotation = glm::inverse(parentRotation) * glm::mat3(actorTransform);

				translation = bullshit[3];
			}
			transform.setTranslation(translation);
			
			glm::vec3 eulerRot = TransformMath::decomposeEulerYXZ(rotation);
			transform.setRotation(eulerRot);
		}
	}
}
