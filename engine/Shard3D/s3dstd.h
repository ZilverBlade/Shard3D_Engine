#pragma once

#include <string>
#include <memory>
#include <unordered_map>
#define GLM_FORCE_QUAT_DATA_WXYZ
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <volk.h>
#include <vulkan/vk_enum_string_helper.h>
namespace Shard3D {
#pragma region Smart Pointers
	// Unique Pointer
	template<typename T>
	using uPtr = std::unique_ptr<T>;
	template<typename T, typename ... Args>
	constexpr inline std::unique_ptr<T> make_uPtr(Args&& ... args) {
		return std::make_unique<T>(std::forward<Args>(args)...);
	}

	// Shared Pointer
	template<typename T>
	using sPtr = std::shared_ptr<T>;
	template<typename T, typename ... Args>
	constexpr inline std::shared_ptr<T> make_sPtr(Args&& ... args) {
		return std::make_shared<T>(std::forward<Args>(args)...);
	}

	// Resource Pointer
	template<typename T>
	class rPtr {
	public:
		rPtr() = default;
		rPtr(const uPtr<T>& uniPtr) : ptr{ uniPtr } {}
		rPtr(const sPtr<T>& sharedPtr) : ptr{ sharedPtr } {}
		rPtr(uPtr<T>&& uniPtr) : ptr{ std::move(uniPtr) } {}
		rPtr(sPtr<T>&& sharedPtr) : ptr{ std::move(sharedPtr) } {}
		rPtr(std::nullptr_t) : ptr{ nullptr } {}

		//template<typename... Args>
		//rPtr(Args&&... args) : ptr{ make_sPtr<T>(std::forward<Args>(args)...) } {}

		T* operator->() const noexcept {
			return ptr.get();
		}

		T& operator*() const noexcept {
			return *ptr.get();
		}

		T* get() const noexcept {
			return ptr.get();
		}
		operator bool() const noexcept {
			return ptr.get();
		}
	private:
		sPtr<T> ptr{};
	};
	template<typename T, typename ... Args>
	constexpr inline rPtr<T> make_rPtr(Args&& ... args) {
		return rPtr<T>(make_sPtr<T>(std::forward<Args>(args)...));
	}

	// Weak Pointer
	template<typename T>
	using wPtr = std::weak_ptr<T>;
#pragma endregion 

	enum class Types {
		Int8, Int16, Int32, Int64,
		UInt8, UInt16, UInt32, UInt64,
		Float, Float2, Float3, Float4
	};

	template <typename T = int8_t>	static Types getTypeEnum() { return Types::Int8; }
	template <>	static inline Types	getTypeEnum<uint8_t>() { return Types::UInt8; }

	template <>	static inline Types	getTypeEnum<int16_t>()		{ return Types::Int16; }
	template <>	static inline Types	getTypeEnum<int32_t>()		{ return Types::Int32; }
	template <>	static inline Types	getTypeEnum<int64_t>()		{ return Types::Int64; }
	template <>	static inline Types	getTypeEnum<uint16_t>()		{ return Types::UInt16; }
	template <>	static inline Types	getTypeEnum<uint32_t>()		{ return Types::UInt32; }
	template <>	static inline Types	getTypeEnum<uint64_t>()		{ return Types::UInt64; }
	template <>	static inline Types	getTypeEnum<float>()		{ return Types::Float; }
	template <>	static inline Types	getTypeEnum<glm::vec2>()	{ return Types::Float2; }
	template <>	static inline Types	getTypeEnum<glm::vec3>()	{ return Types::Float3; }
	template <>	static inline Types	getTypeEnum<glm::vec4>()	{ return Types::Float4; }

	template <typename T = int>	static constexpr size_t sizeof_gpu(){ return 4ULL; }
	template <>	static constexpr size_t sizeof_gpu<float>()		{ return 4ULL; }
	template <>	static constexpr size_t sizeof_gpu<glm::vec2>()	{ return 16ULL; }
	template <>	static constexpr size_t sizeof_gpu<glm::vec3>()	{ return 16ULL; }
	template <>	static constexpr size_t sizeof_gpu<glm::vec4>()	{ return 16ULL; }

	template <size_t S>
	struct VoidData {
		VoidData() = default;
		template <typename T>
		VoidData(const T& myVal) { 
			static_assert(sizeof(myVal) <= S && "Input value must be able to fit in VoidData<size_t>!"); 
			memcpy(myData, &myVal, sizeof(T)); 
		}
		~VoidData() {}
		VoidData(const VoidData& other) {
			const volatile uint8_t* ptr = (const volatile uint8_t*)other.myData;
			memcpy(myData, &ptr, S);
		}
		VoidData& operator= (const VoidData& other) {
			return VoidData(other); 
		}
		void* getDataPointer() const { return reinterpret_cast<uint8_t*>(myData); }
		template <typename T>
		const T getData() const { return *reinterpret_cast<T*>(myData); }
	private:
		mutable uint8_t myData[S]{};
	};
	template <size_t S>
	struct VoidRef {
		VoidRef() = delete;
		VoidRef (const VoidRef&) = delete;
		VoidRef& operator= (const VoidRef&) = delete;
		template <typename T>
		VoidRef(const T& myVal) : myData(reinterpret_cast<uint8_t*>(const_cast<T*>(&myVal))){
			static_assert(sizeof(myVal) <= S, "Input value must be able to fit in VoidRef<size_t>!");
		}
		~VoidRef() {}
		void* getDataPointer() const { return reinterpret_cast<uint8_t*>(myData); }
		template <typename T>
		const T getData() const { return *reinterpret_cast<T*>(myData); }
	private:
		mutable uint8_t* myData{};
	};
	template <typename T, size_t Alignment>
	struct AlignedType {
	public:
		AlignedType() = default;
		constexpr operator T&() noexcept {
			return value;
		}

		constexpr operator const T() const noexcept {
			return value;
		}

		AlignedType(const AlignedType&) = default;
		AlignedType& operator=(const AlignedType&) = default;
		AlignedType(AlignedType&&) = default;
		AlignedType& operator=(AlignedType&&) = default;

		constexpr AlignedType(const T& rhs) noexcept : value(rhs) {

		}
		constexpr AlignedType(T&& rhs) noexcept : value(rhs) {

		}

		constexpr void operator =(const T& rhs) noexcept {
			value = rhs;
		}

		constexpr void operator =(T&& rhs) noexcept {
			value = rhs;
		}
	private:
		static_assert(Alignment != sizeof(T) && "Alignment is not necessary!");
		static_assert(Alignment > sizeof(T) && "Alignment must be larger than type's size!");
		T value{};
		char padding[Alignment - sizeof(T)]{};
	};

	
#define PTR_DELETE(ptr) if (ptr) { delete ptr; }

#define bitswitch(entry) const auto __bcase_value = entry;
#define bitcase(candidate) if (__bcase_value & candidate)


#define DELETE_COPY(myClass)	myClass(const myClass&) = delete; \
								myClass& operator=(const myClass&) = delete;
#define DELETE_MOVE(myClass)	myClass(myClass&&) = delete; \
								myClass& operator=(myClass&&) = delete;

	// Vulkan helpers

#define VK_VALIDATE(function) if (VkResult result = function; result != VK_SUCCESS) SHARD3D_ERROR("{0} failed! Error code: {1} ({2})", #function, string_VkResult(result), (int)result)
#define VK_VALIDATE(function, message) if (VkResult result = function; result != VK_SUCCESS) SHARD3D_ERROR("{0} failed! '{1}' Error code: {2} ({3})", #function, message, string_VkResult(result), (int)result)
#define VK_ASSERT(function, message) if (VkResult result = function; result != VK_SUCCESS) SHARD3D_FATAL(fmt::format("{}! Error code: {} ({})", message, string_VkResult(result), (int)result))
}