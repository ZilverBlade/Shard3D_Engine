#pragma once

extern "C" {
	typedef struct _MonoString MonoString;
	typedef struct _MonoArray MonoArray;
	typedef struct _MonoReflectionType MonoReflectionType;
	typedef struct _MonoType MonoType;
}

struct CppCustomInternalCallData {
	const char* name;
	const void* function;
};
struct CppCustomInternalCallCreateInfo {
	int numInteralCalls;
	CppCustomInternalCallData* cppUserInternalCalls;
};

namespace Shard3D {
	class S3DWindow;
#define S3D_ICALL(callName) mono_add_internal_call("Shard3D.Core.InternalCalls::"#callName, (void*)InternalScriptCalls::callName)
	class ScriptEngineLinker {
	public:
		static void registerLinker(S3DWindow* mainWindowPtr, const CppCustomInternalCallCreateInfo& info);
	private:
		static void registerInternalCalls(const CppCustomInternalCallCreateInfo& info);
		static void registerComponents();
	};
}