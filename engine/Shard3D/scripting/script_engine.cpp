#include "../s3dpch.h"
#include <mono/jit/jit.h>
#include <mono/metadata/assembly.h>
#include <mono/metadata/image.h>
#include <mono/metadata/class.h>

#include <filesystem>
#include <fstream>

#include "script_engine.h"
#include "script_engine_linker.h"

#include "../core.h"
#include "../core/misc/UUID.h"
#include "../core/ecs/actor.h"
#include "../core/ecs/levelmgr.h"
#include "../core/ecs/prefab.h"
#include "../core/ecs/component.h"
#include "../systems/handlers/prefab_manager.h"
#include "../core/asset/animation.h"
#include "../core/misc/graphics_settings.h"
#include "../core/misc/engine_settings.h"

namespace Shard3D {

	static ScriptEngineData* scriptEngineData;
	static GlobalScriptEngineData* globalData;
	static std::vector<HMODULE> loadedCPPLibraries;
	static bool requireExit = false;

	namespace MonoUtils {
		static char* readBytes(const std::string& filepath, uint32_t* outSize) {
			std::ifstream stream(filepath, std::ios::binary | std::ios::ate);

			if (!stream)
			{
				// Failed to open the file
				return nullptr;
			}

			std::streampos end = stream.tellg();
			stream.seekg(0, std::ios::beg);
			uint32_t size = end - stream.tellg();

			if (size == 0)
			{
				// File is empty
				return nullptr;
			}

			char* buffer = new char[size];
			stream.read((char*)buffer, size);
			stream.close();

			*outSize = size;
			return buffer;
		}

		static MonoAssembly* loadAssembly(const std::string& assemblyPath)
		{
			uint32_t fileSize = 0;
			char* fileData = readBytes(assemblyPath, &fileSize);

			// NOTE: We can't use this image for anything other than loading the assembly because this image doesn't have a reference to the assembly
			MonoImageOpenStatus status;
			MonoImage* image = mono_image_open_from_data_full(fileData, fileSize, 1, &status, 0);

			if (status != MONO_IMAGE_OK)
			{
				const char* errorMessage = mono_image_strerror(status);
				// Log some error message using the errorMessage data
				return nullptr;
			}

			MonoAssembly* assembly = mono_assembly_load_from_full(image, assemblyPath.c_str(), &status, 0);
			mono_image_close(image);

			// Don't forget to free the file data
			delete[] fileData;

			return assembly;
		}

		static void printAssemblyTypes(ScriptEngineData* data, MonoAssembly* assembly)
		{
			MonoImage* image = mono_assembly_get_image(assembly);
			const MonoTableInfo* typeDefinitionsTable = mono_image_get_table_info(image, MONO_TABLE_TYPEDEF);
			int32_t numTypes = mono_table_info_get_rows(typeDefinitionsTable);

			SHARD3D_INFO("Loaded C# Assembly modules: ");
			for (int32_t i = 1; i < numTypes; i++) {
				uint32_t cols[MONO_TYPEDEF_SIZE];
				mono_metadata_decode_row(typeDefinitionsTable, i, cols, MONO_TYPEDEF_SIZE);

				const char* nameSpace = mono_metadata_string_heap(image, cols[MONO_TYPEDEF_NAMESPACE]);
				const char* name = mono_metadata_string_heap(image, cols[MONO_TYPEDEF_NAME]);
				if (!strUtils::hasStarting(nameSpace, "Shard3D.")) continue; // dont log core stuff
				SHARD3D_INFO("\t{0}.{1}", nameSpace, name);
			}
		}
	}
	void ScriptEngine::loadAssemblyClasses(ScriptEngineData* data) {
		data->actorClasses.clear();

		const MonoTableInfo* typeDefinitionsTable = mono_image_get_table_info(data->appAssemblyImage, MONO_TABLE_TYPEDEF);
		int32_t numTypes = mono_table_info_get_rows(typeDefinitionsTable);

		for (int32_t i = 0; i < numTypes; i++) {
			uint32_t cols[MONO_TYPEDEF_SIZE];
			mono_metadata_decode_row(typeDefinitionsTable, i, cols, MONO_TYPEDEF_SIZE);

			const char* nameSpace = mono_metadata_string_heap(data->appAssemblyImage, cols[MONO_TYPEDEF_NAMESPACE]);
			const char* name = mono_metadata_string_heap(data->appAssemblyImage, cols[MONO_TYPEDEF_NAME]);

			std::string fullname = fmt::format("{}.{}", nameSpace, name);

			MonoClass* monoClass{};
			monoClass = mono_class_from_name(data->appAssemblyImage, nameSpace, name);

			if (!monoClass) continue;
			bool isActor = mono_class_is_subclass_of(monoClass, globalData->actorClass.getClass(), false);
			bool isHUDElement = mono_class_is_subclass_of(monoClass, globalData->hudElementClass.getClass(), false);
			bool isSurfaceMaterial = mono_class_is_subclass_of(monoClass, globalData->surfaceMaterialClass.getClass(), false);

			sPtr<ScriptClass> ptr = make_sPtr<ScriptClass>(nameSpace, name, false);
			if (isActor) {
				data->actorClasses[fullname] = ptr;
			} else if (isHUDElement) {
				data->hudElementClasses[fullname] = ptr;
			} else if (isSurfaceMaterial) {
				data->surfaceMaterialClasses[fullname] = ptr;
			}
		}
	}

	typedef int(__stdcall*loadFunction)(const void* engineInfo, void* internalCallInfo);
	typedef int(__stdcall*changeWkdFunction)(const char* engineInfo);

	struct EngineData {
		int version;
		const char* workingDirectory;
		void* getMonoStringFunc;
		void* freeMonoStringFunc;
	};
	
	static char* getMonoString(void* monoString) {
		MonoString* str = (MonoString*)monoString;
		char* cstr = mono_string_to_utf8(str);
		return cstr;
	}
	static void freeMonoString(void* monoString) {
		mono_free(monoString);
	}

	static CppCustomInternalCallCreateInfo loadCppDllFunctions(HMODULE dll, const char* workingDir) {
		EngineData data;
		data.version = ENGINE_VERSION.getVersionInt();

		int wkdl = strlen(workingDir) + 1;
		data.workingDirectory = (char*)calloc(wkdl, 1);
		strncpy((char*)data.workingDirectory, workingDir, wkdl);

		data.getMonoStringFunc = (void*)getMonoString;
		data.freeMonoStringFunc = (void*)freeMonoString;
		CppCustomInternalCallCreateInfo createInfo;

		loadFunction getInternalCalls = (loadFunction)GetProcAddress(dll, "S3DRetrieveInternalCalls");
		if (!getInternalCalls) {
			SHARD3D_FATAL("C++ DLL initialization function is incorrect or does not exist!");
		}

		int result = getInternalCalls(&data, &createInfo);
		if (result != EXIT_SUCCESS) {
			SHARD3D_FATAL("Failed to retrieve user generated internal calls!");
		}
		return createInfo;
	}

	MonoImage* ScriptEngine::getCoreAssemblyImage() {
		return globalData->coreAssemblyImage;
	}

	MonoDomain* ScriptEngine::getDomain() {
		return globalData->rootDomain;
	}

	GlobalScriptEngineData* ScriptEngine::getGlobalData() {
		return globalData;
	}

	ScriptEngineData* ScriptEngine::getScriptEngineData() {
		return scriptEngineData;
	}

	MonoObject* ScriptEngine::instClass(MonoClass* monoClass) {
		MonoObject* instance = mono_object_new(globalData->appDomain, monoClass);
		mono_runtime_object_init(instance);
		return instance;
	}

	void ScriptEngine::init(S3DWindow* window) {
		scriptEngineData = new ScriptEngineData();
		
		globalData = new GlobalScriptEngineData();
		globalData->animationSystem = new SkeletalAnimationManager();

		mono_set_assemblies_path(ENGINE_SCRIPT_LIBRARY_PATH);
		MonoDomain* rootDomain = mono_jit_init("Shard3D_JIT_Runtime");
		if (rootDomain == nullptr) {
			SHARD3D_FATAL("Failed to initialize Shard3D_JIT_Runtime!!!");
		}

		globalData->rootDomain = rootDomain;

		globalData->appDomain = mono_domain_create_appdomain((char*)("Shard3D_Runtime"), nullptr);
		mono_domain_set(globalData->appDomain, true);
		_loadCoreAssembly();
		
		reloadAssembly();
		if (!doesAssemblyExist()) return;

		globalData->actorClass = ScriptClass("Shard3D", "Actor", true);
		globalData->hudElementClass = ScriptClass("Shard3D.UI", "HUDElement", true);
		globalData->surfaceMaterialClass = ScriptClass("Shard3D.Graphics", "SurfaceMaterial", true);
		loadAssemblyClasses(scriptEngineData);

		CSimpleIniA ini;
		ini.LoadFile((ProjectSystem::getProjectLocation() + "/configdata/script_settings.ini").c_str());
		auto splitIntoVector = [&](const char* in, std::vector<std::string>& out) {
			if (in == nullptr) return;
			std::string str = in;
			int lastNonCommaEntry = 0;
			for (int idx = 0; idx < str.length(); idx++) {
				if (in[idx] == ',') {
					out.push_back(str.substr(lastNonCommaEntry, idx - lastNonCommaEntry));
					lastNonCommaEntry = idx + 1;
					idx++;
				}
				if (idx + 1 == str.length()) {
					out.push_back(str.substr(lastNonCommaEntry, idx - lastNonCommaEntry + 1));
				}
			}
		};

		std::vector<std::string> loadDlls;
		splitIntoVector(ini.GetValue("LOW_LEVEL_CPP", "preloadDLLs"), loadDlls);
		CppCustomInternalCallCreateInfo info{};
		std::string pathCppDll = std::string(ProjectSystem::getAssetLocation() + "scriptdata/bin/") + ProjectSystem::getGameName() + "Cpp.dll";

		for (auto& dll : loadDlls) {
			HMODULE library = LoadLibraryA((ProjectSystem::getAssetLocation() + "scriptdata/" + dll).c_str());
			if (library) {
				loadedCPPLibraries.push_back(library);
			}
		}
		HMODULE dll = LoadLibraryA(pathCppDll.c_str());
		if (dll) {
			loadedCPPLibraries.push_back(dll);
			info = loadCppDllFunctions(dll, std::string(ProjectSystem::getAssetLocation() + "scriptdata/bin/").c_str());
			SHARD3D_INFO("Registered {0} user created internal calls", info.numInteralCalls);
		}
		ScriptEngineLinker::registerLinker(window, info);

	}

	void ScriptEngine::destroy() {
		globalData->rootDomain = nullptr;
		destroyMono(scriptEngineData);
		for (HMODULE dll : loadedCPPLibraries) {
			BOOL result = FreeLibrary(dll);
		}
		//mono_jit_cleanup(scriptEngineData->rootDomain); // script
		delete scriptEngineData;
		delete globalData->animationSystem;
		delete globalData;
	}

	void ScriptEngine::reloadAssembly() {
		_reloadAssembly(scriptEngineData);
	}

	void ScriptEngine::_loadCoreAssembly() {
		globalData->coreAssembly = MonoUtils::loadAssembly(std::string(ENGINE_SCRIPT_LIBRARY_PATH"/s3dcorelib.dll").c_str());
		globalData->coreAssemblyImage = mono_assembly_get_image(globalData->coreAssembly);

		MonoImage* image = mono_assembly_get_image(globalData->coreAssembly);
		const MonoTableInfo* typeDefinitionsTable = mono_image_get_table_info(image, MONO_TABLE_TYPEDEF);
		int32_t numTypes = mono_table_info_get_rows(typeDefinitionsTable);

		SHARD3D_INFO("Loaded Core Assembly modules: ");
		for (int32_t i = 0; i < numTypes; i++) {
			if (i == 0) continue;
			uint32_t cols[MONO_TYPEDEF_SIZE];
			mono_metadata_decode_row(typeDefinitionsTable, i, cols, MONO_TYPEDEF_SIZE);

			const char* nameSpace = mono_metadata_string_heap(image, cols[MONO_TYPEDEF_NAMESPACE]);
			const char* name = mono_metadata_string_heap(image, cols[MONO_TYPEDEF_NAME]);
			std::printf("\t%s.%s\n", nameSpace, name);
		}
	}

	void inline ScriptEngine::_reloadAssembly(ScriptEngineData* scriptEngine)  {
		std::string path = std::string(ProjectSystem::getAssetLocation() + "scriptdata/bin/") + ProjectSystem::getGameName() + std::string(".dll");
		scriptEngine->appAssembly = MonoUtils::loadAssembly(path);
		if (!scriptEngine->appAssembly) return;
		scriptEngine->appAssemblyImage = mono_assembly_get_image(scriptEngine->appAssembly);
		//MonoUtils::printAssemblyTypes(scriptEngine, scriptEngine->appAssembly);
	}

	std::unordered_map<std::string, sPtr<ScriptClass>> ScriptEngine::getActorClasses() {
		return scriptEngineData->actorClasses;
	}
	std::unordered_map<std::string, sPtr<ScriptClass>> ScriptEngine::getHUDElementClasses() {
		return scriptEngineData->hudElementClasses;
	}

	MonoClass* ScriptEngine::getActorClass() {
		return globalData->actorClass.getClass();
	}

	void ScriptEngine::runtimeStart(sPtr<ECS::Level>& level, ResourceSystem* resourceSystem) {
		globalData->levelContext = level;
		globalData->resourceSystem = resourceSystem;
	}

	void ScriptEngine::runtimeStop() {
		//globalData->levelContext = nullptr;
		globalData->animationSystem->clearAllAnimations();
		globalData->animationRegistry.clear();
		scriptEngineData->actorInstances.clear();
	}

	void ScriptEngine::updateSkeletonAnimations(float dt) {
		for (auto& [actor_, data] : globalData->animationRegistry) {
			if (!std::get<0>(data)) return;

			Actor actor = { actor_, globalData->levelContext.get() };
			auto& skr = actor.getComponent<Components::SkeletonRigComponent>();
			const SkeletalAnimation& animation = globalData->animationSystem->retrieveAnimation(std::get<2>(data));
			float& time = std::get<1>(data); 
			time = std::fmod(time + dt, animation.getMaxDuration());
			for (int i = 0; i < animation.boneAnimations.size(); i++) {
				skr.boneTransforms[i].setTranslation(animation.boneAnimations[i].getTranslation(time));
				skr.boneTransforms[i].setRotationQuat(animation.boneAnimations[i].getQuaternion(time));
				skr.boneTransforms[i].setScale(animation.boneAnimations[i].getScale(time));
			}
		}
	}

	bool ScriptEngine::doesActorClassExist(const std::string& fullClassName) {
		return scriptEngineData->actorClasses.find(fullClassName) != scriptEngineData->actorClasses.end();
	}
	bool ScriptEngine::doesHUDElementClassExist(const std::string& fullClassName) {
		return scriptEngineData->hudElementClasses.find(fullClassName) != scriptEngineData->hudElementClasses.end();
	}
	bool ScriptEngine::doesSurfaceMaterialClassExist(const std::string& fullClassName) {
		return scriptEngineData->surfaceMaterialClasses.find(fullClassName) != scriptEngineData->surfaceMaterialClasses.end();
	}

	bool ScriptEngine::doesAssemblyExist() {
		return scriptEngineData->appAssembly;
	}

	sPtr<Level>& ScriptEngine::getContext() {
		return globalData->levelContext;
	}
	void ScriptEngine::switchLevel(AssetID path) {
		if (globalData->levelContext->simulationState != PlayState::Stopped) {
			ScriptEngine::actorScript().endEvent();
			ScriptEngine::runtimeStop();
		}
		sPtr<ECS::Level> newlevel = make_sPtr<ECS::Level>(globalData->resourceSystem);
		Level::shallowCopy(newlevel, globalData->levelContext);
		newlevel->createSystem();
		LevelManager levelMan(*newlevel, globalData->resourceSystem);
		levelMan.load(path.getAbsolute(), true);
		globalData->levelContext.swap(newlevel);
		ScriptEngine::runtimeStart(globalData->levelContext, globalData->resourceSystem);
		ScriptEngine::instantiateActors();
		ScriptEngine::actorScript().beginEvent();
		requireExit = true;
	}
	ResourceSystem* ScriptEngine::getResourceSystem() {
		return globalData->resourceSystem;
	}

	void ScriptEngine::destroyMono(ScriptEngineData* scriptEngine) {
		//unload is hard >w<
		//mono_jit_cleanup(scriptEngine->rootDomain);

		//mono_domain_unload(scriptEngine->appDomain);
		globalData->appDomain = nullptr;
	}


	void ScriptEngine::instantiateHUDList(std::unordered_map<std::string, HUDElementData*> list) {
		for (auto& [string, element] : list) {
			if (element->genericBase->mType == SHUD::Element::Type::Panel) {
				for (auto& [string, element] : element->children) {
					instantiateHUDList(list);
				}
			} else if (element->genericBase->mType == SHUD::Element::Type::Button) {
				if (doesHUDElementClassExist(element->scriptClass)) {
					auto& data = scriptEngineData;
					sPtr<HUDElementScriptInstance> instance = make_sPtr<HUDElementScriptInstance>(data->hudElementClasses[element->scriptClass], element, string);
					data->hudElementInstances[(UUID)(size_t)element->genericBase] = instance;
				}
			}
		}
	}

	void ScriptEngine::instantiateActors() {
		requireExit = false;
		for (auto a : globalData->levelContext->registry.view<Components::PrefabComponent>()) {
			Actor actor = { a,  globalData->levelContext.get() };
			const auto& scr = actor.getComponent<Components::PrefabComponent>();
			const sPtr<Prefab> prefab = globalData->levelContext->getPrefabManager()->getPrefab(scr.prefab);
			if (!prefab->enableScript) continue;
			if (doesActorClassExist(prefab->scriptModule)) {
				auto& data = scriptEngineData;
				sPtr<ActorScriptInstance> instance = make_sPtr<ActorScriptInstance>(data->actorClasses[prefab->scriptModule], actor);
				data->actorInstances[actor.getGlobalUUID()] = instance;
			}
		}
	}


	void ScriptEngine::VirtualActorCalls::beginEvent() {
		for (auto& actor : scriptEngineData->actorInstances) {
			actor.second->invokeEvent().beginEvent();
		}
	}

	void ScriptEngine::VirtualActorCalls::endEvent() {
		for (auto& actor : scriptEngineData->actorInstances) {
			actor.second->invokeEvent().endEvent();
		}
	}

	bool ScriptEngine::VirtualActorCalls::tickEvent(float dt) {
		size_t old = globalData->levelContext->levelAsset.getID();
		for (auto& actor : scriptEngineData->actorInstances) {
			actor.second->invokeEvent().tickEvent(dt);
			if (old != globalData->levelContext->levelAsset.getID()) {
				return false;
			}
		}
		return true;
	}
	void ScriptEngine::VirtualActorCalls::physicsTickEvent(float ts) {
		for (auto& actor : scriptEngineData->actorInstances) {
			actor.second->invokeEvent().physicsTickEvent(ts);
		}
	}
	void ScriptEngine::VirtualActorCalls::spawnEvent(ECS::Actor actor) {
		const auto& scr = actor.getComponent<Components::PrefabComponent>();
		const sPtr<Prefab> prefab = globalData->levelContext->getPrefabManager()->getPrefab(scr.prefab);
		if (!prefab->enableScript) return;
		if (doesActorClassExist(prefab->scriptModule)) {
			auto& data = scriptEngineData;
			sPtr<ActorScriptInstance> instance = make_sPtr<ActorScriptInstance>(data->actorClasses[prefab->scriptModule], actor);
			data->actorInstances[actor.getGlobalUUID()] = instance;
			scriptEngineData->actorInstances[actor.getGlobalUUID()]->invokeEvent().spawnEvent();
		}
	}

	void ScriptEngine::VirtualActorCalls::killEvent(ECS::Actor actor) {
		const auto guid = actor.getGlobalUUID();
		if (scriptEngineData->actorInstances.find(guid) != scriptEngineData->actorInstances.end()) {
			scriptEngineData->actorInstances[guid]->invokeEvent().killEvent();
			scriptEngineData->actorInstances.erase(guid);
		}
	}

	void ScriptEngine::VirtualHUDElementCalls::clickEvent(SHUD::Element::Base* element) {
		scriptEngineData->hudElementInstances.at((UUID)(size_t)element)->invokeEvent().clickEvent();
	}
	void ScriptEngine::VirtualHUDElementCalls::hoverEvent(SHUD::Element::Base* element) {
		scriptEngineData->hudElementInstances.at((UUID)(size_t)element)->invokeEvent().hoverEvent();
	}
	void ScriptEngine::VirtualHUDElementCalls::pressEvent(SHUD::Element::Base* element) {
		scriptEngineData->hudElementInstances.at((UUID)(size_t)element)->invokeEvent().pressEvent();
	}

	void ScriptEngine::VirtualSurfaceMaterialCalls::update(float dt) {
		for (auto& sf : scriptEngineData->surfaceMaterialInstances) {
			sf.second->invokeEvent().update(dt);
		}
	}

	ScriptClass::ScriptClass(const std::string& c_ns, const std::string& c_n, bool global) : classNamespace(c_ns), className(c_n) {
		if (global) monoClass = mono_class_from_name(globalData->coreAssemblyImage, c_ns.c_str(), c_n.c_str());
		else	monoClass = mono_class_from_name(scriptEngineData->appAssemblyImage, c_ns.c_str(), c_n.c_str());
	}
	
	MonoObject* ScriptClass::inst() {
		return ScriptEngine::instClass(monoClass);
	}

	MonoMethod* ScriptClass::getMethod(const std::string& name, int parameterCount) {
		return mono_class_get_method_from_name(monoClass, name.c_str(), parameterCount);
	}

	MonoObject* ScriptClass::invokeMethod(MonoObject* instance, MonoMethod* method, void** params) {
		return mono_runtime_invoke(method, instance, params, nullptr);
	}

	ScriptInstance::ScriptInstance(sPtr<ScriptClass> scriptClass) : scriptClass(scriptClass) {}

	ActorScriptInstance::ActorScriptInstance(sPtr<ScriptClass> s_class, ECS::Actor actor) : ScriptInstance(s_class) {
		instance = s_class->inst();

		constructor = globalData->actorClass.getMethod(".ctor", 2);
		scriptEvents = ScriptEvents(s_class, instance);

		uint64_t scope = actor.getInstancerUUID();
		uint64_t id = actor.getScopedID();
		void* params[] = { &scope, &id };
		scriptClass->invokeMethod(instance, constructor, params);
	}
	ActorScriptInstance::ScriptEvents ActorScriptInstance::invokeEvent() { return scriptEvents; }
	ActorScriptInstance::ScriptEvents::ScriptEvents(sPtr<ScriptClass> ptr, MonoObject* i) : s_c(ptr), i_(i) {
		beginEventMethod = s_c->getMethod("BeginEvent", 0);
		endEventMethod = s_c->getMethod("EndEvent", 0);
		tickEventMethod = s_c->getMethod("TickEvent", 1);
		physicsTickEventMethod = s_c->getMethod("PhysicsTickEvent", 1);
		spawnEventMethod = s_c->getMethod("SpawnEvent", 0);
		killEventMethod = s_c->getMethod("KillEvent", 0);
	}
	void ActorScriptInstance::ScriptEvents::beginEvent() { if (!beginEventMethod) return; s_c->invokeMethod(i_, beginEventMethod); }
	void ActorScriptInstance::ScriptEvents::endEvent() { if (!endEventMethod) return; s_c->invokeMethod(i_, endEventMethod); }
	void ActorScriptInstance::ScriptEvents::tickEvent(float dt) { if (!tickEventMethod) return; void* param = &dt; s_c->invokeMethod(i_, tickEventMethod, &param); }
	void ActorScriptInstance::ScriptEvents::physicsTickEvent(float ts) { if (!physicsTickEventMethod) return; void* param = &ts; s_c->invokeMethod(i_, physicsTickEventMethod, &param); }
	void ActorScriptInstance::ScriptEvents::spawnEvent() { if (!spawnEventMethod) return; s_c->invokeMethod(i_, spawnEventMethod); }
	void ActorScriptInstance::ScriptEvents::killEvent() { if (!killEventMethod) return; s_c->invokeMethod(i_, killEventMethod); }

	HUDElementScriptInstance::HUDElementScriptInstance(sPtr<ScriptClass> s_class, HUDElementData* data, std::string elementName) : ScriptInstance(s_class) {
		instance = s_class->inst();

		constructor = globalData->hudElementClass.getMethod(".ctor", 2);
		scriptEvents = ScriptEvents(s_class, instance);

		MonoString* string = mono_string_new(globalData->appDomain, elementName.c_str());
		void* params[] = { &reinterpret_cast<uint64_t&>(data), &string };
		scriptClass->invokeMethod(instance, constructor, params);
	}
	HUDElementScriptInstance::ScriptEvents HUDElementScriptInstance::invokeEvent() {
		return scriptEvents;
	}
	HUDElementScriptInstance::ScriptEvents::ScriptEvents(sPtr<ScriptClass> ptr, MonoObject* i) : s_c(ptr), i_(i) {
		clickEventMethod = s_c->getMethod("ClickEvent", 0);
		pressEventMethod = s_c->getMethod("PressEvent", 0);
		hoverEventMethod = s_c->getMethod("HoverEvent", 0);
	}
		
	void HUDElementScriptInstance::ScriptEvents::clickEvent() { if (!clickEventMethod) return; s_c->invokeMethod(i_, clickEventMethod, nullptr); }
	void HUDElementScriptInstance::ScriptEvents::pressEvent() { if (!pressEventMethod) return; s_c->invokeMethod(i_, pressEventMethod, nullptr); }
	void HUDElementScriptInstance::ScriptEvents::hoverEvent() { if (!hoverEventMethod) return; s_c->invokeMethod(i_, hoverEventMethod, nullptr); }

	SurfaceMaterialScriptInstance::SurfaceMaterialScriptInstance(sPtr<ScriptClass> s_class, AssetID material) : ScriptInstance(s_class) {
		instance = s_class->inst();

		constructor = globalData->surfaceMaterialClass.getMethod(".ctor", 2);
		scriptEvents = ScriptEvents(s_class, instance);

		MonoString* assetString = mono_string_new(globalData->appDomain, material.getAsset().c_str());
		void* params[] = { assetString };
		scriptClass->invokeMethod(instance, constructor, params);
	}
	SurfaceMaterialScriptInstance::ScriptEvents SurfaceMaterialScriptInstance::invokeEvent() {
		return scriptEvents;
	}
	SurfaceMaterialScriptInstance::ScriptEvents::ScriptEvents(sPtr<ScriptClass> ptr, MonoObject* i) : s_c(ptr), i_(i) {
		updateMethod = s_c->getMethod("Update", 1);
	}

	void SurfaceMaterialScriptInstance::ScriptEvents::update(float dt) { void* param = &dt; s_c->invokeMethod(i_, updateMethod, &param); }
}