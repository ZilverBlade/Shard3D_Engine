#include "../s3dpch.h"
#include "script_engine_linker.h"
#include "script_engine.h"

#include <mono/metadata/object.h>
#include <mono/metadata/reflection.h>

#include "../ecs.h"
#include "../core/ecs/level_simulation.h"
#include "../systems/handlers/render_list.h"
#include "../systems/handlers/prefab_manager.h"
#include "../systems/computational/physics_system.h"
#include "../systems/rendering/hud_system.h"

// http://docs.go-mono.com/?link=root:/embed

namespace Shard3D {
	struct _helper_container {
		std::vector<std::function<bool(ECS::Actor)>> monoTypeRegistryAppenderHasComponent{};
		std::vector<std::function<void(ECS::Actor)>> monoTypeRegistryAppenderAddComponent{};
		std::vector<std::function<void(ECS::Actor)>> monoTypeRegistryAppenderRmvComponent{};
		std::vector<MonoType*> monoTypeRegistryVector{};
	};
	static _helper_container* h_ctr{}; // i dont know why i have to do it like this, it's just the only way it works
	static S3DWindow* windowPtr;
}

namespace Shard3D::InternalScriptHelpers {
	static Shard3D::Actor INTERNAL_GetTrueActor(Shard3D::Level* level, std::size_t scope, std::size_t id) {
		return level->getActorFromUUID(scope, id);
	}
	static std::string INTERNAL_MonoStringToString(MonoString* string) {
		char* t = mono_string_to_utf8(string);
		std::string str= std::string(t);
		mono_free(t);
		return str;
	}
	static Shard3D::AssetID INTERNAL_MonoStringToAssetID(MonoString* string) {
		char* t = mono_string_to_utf8(string);
		AssetID ass = AssetID(std::string(t));
		mono_free(t);
		return ass;
	}
}

namespace Shard3D::InternalScriptCalls {
	using namespace Shard3D::InternalScriptHelpers;
	static void Log(MonoString* string, int severity) {
		char* t = mono_string_to_utf8(string);
		std::string text(t);
		if (severity == 0) {
			SHARD3D_LOG(text);
		}
		else if (severity == 1) {
			SHARD3D_INFO(text);
		}
		else if (severity == 2) {
			SHARD3D_WARN(text);
		}
		else if (severity == 3) {
			SHARD3D_ERROR(text);
		}
		else {
			SHARD3D_ERROR("Internal call logged with invalid severity");
		}
		mono_free(t);
	}
	static void LogNoImpl() {
		SHARD3D_NOIMPL;
	}
	static bool IsKeyDown(int keyCode) {
		return windowPtr->isKeyDown(keyCode);
	}
	static bool IsMouseButtonDown(int button) {
		return windowPtr->isMouseButtonDown(button);
	}

	static bool IsGamepadPresent(int gamepadIndex) {
		GLFWgamepadstate state;
		return glfwGetGamepadState(gamepadIndex, &state);
	}
	static bool IsGamepadButtonDown(int button, int gamepadIndex) {
		if (!windowPtr->isWindowFocussed()) return false;
		GLFWgamepadstate state;
		if (glfwGetGamepadState(gamepadIndex, &state) == GLFW_FALSE) return false;
		return state.buttons[button] == GLFW_PRESS;
	}
	static float GetGamepadAnalogAxis(int analog, int gamepadIndex, float deadzoneRange) {
		if (!windowPtr->isWindowFocussed()) return 0.0f;
		GLFWgamepadstate state;
		if (glfwGetGamepadState(gamepadIndex, &state) == GLFW_FALSE) return 0.0f;
		float axis = state.axes[analog];
		return glm::sign(axis) * std::max(std::abs(axis) - deadzoneRange, 0.0f) / (1.0F - deadzoneRange);
	}

	static void GetMousePosition(glm::vec2* position) {
		double x, y;
		glfwGetCursorPos(windowPtr->getGLFWwindow(), &x, &y);
		*position = glm::vec2(x, y);
	}

	static void GetWindowResolution(glm::vec2* resolution) {
		*resolution = glm::vec2(windowPtr->getWindowWidth(), windowPtr->getWindowHeight());
	}


	static void Actor_GetPrefabInterface(size_t scopeUUID, size_t actorID, MonoString* interfaceGet, MonoObject** outInstance) {
		char* t = mono_string_to_utf8(interfaceGet);
		std::string str = t;
		std::string ns = str.substr(0, str.find_last_of("."));
		std::string n = str.substr(str.find_last_of(".") + 1);
		mono_free(t);

		UUID actor = static_cast<UUID>(getTrueActorUUID(scopeUUID, actorID));
		if (ScriptEngine::getScriptEngineData()->actorInstances.find(actor) != ScriptEngine::getScriptEngineData()->actorInstances.end()) {
			//MonoClass* klass = mono_class_from_name(ScriptEngine::getScriptEngineData()->appAssemblyImage, ns.c_str(), n.c_str());
			//MonoObject* instance = mono_object_new(ScriptEngine::getGlobalData()->appDomain, klass);
			//mono_runtime_object_init(instance);
			//MonoMethod* constructor = ScriptEngine::getGlobalData()->actorClass.getMethod(".ctor", 2);
			//void* params[] = { &scopeUUID, &actorID };
			//MonoObject* invoked = mono_runtime_invoke(constructor, instance, params, nullptr);
			//*outInstantiated = invoked;
			*outInstance = ScriptEngine::getScriptEngineData()->actorInstances.at(static_cast<UUID>(actor))->getInstance();
		} else {
			SHARD3D_ERROR("Failed to find actor {0} for class {1}!", actor, str);
			*outInstance = nullptr;
		}
	}

	static bool Actor_HasComponent(uint64_t scopeUUID, uint64_t actorID, MonoReflectionType* componentType) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		MonoType* monoComponentType = mono_reflection_type_get_type(componentType);
		int i = 0;
		for (MonoType* object : h_ctr->monoTypeRegistryVector) {
			if (object == monoComponentType)
				return h_ctr->monoTypeRegistryAppenderHasComponent.at(i)(actor);
			i++;
		}
		return false;
	}
	static void Actor_AddComponent(uint64_t scopeUUID, uint64_t actorID, MonoReflectionType* componentType) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		MonoType* monoComponentType = mono_reflection_type_get_type(componentType);
		int i = 0;
		for (MonoType* object : h_ctr->monoTypeRegistryVector) {
			if (object == monoComponentType)
				h_ctr->monoTypeRegistryAppenderAddComponent.at(i)(actor);
			i++;
		}
	}
	static void Actor_RmvComponent(uint64_t scopeUUID, uint64_t actorID, MonoReflectionType* componentType) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		MonoType* monoComponentType = mono_reflection_type_get_type(componentType);
		int i = 0;
		for (MonoType* object : h_ctr->monoTypeRegistryVector) {
			if (object == monoComponentType)
				h_ctr->monoTypeRegistryAppenderRmvComponent.at(i)(actor);
			i++;
		}
	}
	static void Actor_Reparent(uint64_t scopeUUID, uint64_t actorID, uint64_t childScopeUUID, uint64_t childID) {
		Actor parentActor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		Actor childActor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);

		ScriptEngine::getContext()->parentActor(childActor, parentActor);
	}

	static void CreateEmptyActor(uint64_t scopeUUID, uint64_t* actorID, MonoString* string, bool stationary) {
		char* t = mono_string_to_utf8(string);
		std::string text(t);
		Actor actor = ScriptEngine::getContext()->createActor(scopeUUID, text);
		if (stationary) {
			actor.makeStationary();
		} else {
			actor.makeDynamic();
		}
		*actorID = actor.getScopedID();
		mono_free(t);
	}
	static void SpawnPrefabActor(uint64_t scopeUUID, uint64_t* actorID, MonoString* asset, MonoString* string) {
		if (scopeUUID != 0) SHARD3D_NOIMPL_C;

		char* t = mono_string_to_utf8(asset);
		char* t2 = mono_string_to_utf8(string);
		AssetID ass = AssetID(std::string(t));
		if (Actor bpActor = ScriptEngine::getContext()->getPrefabManager()->instPrefab(
			ScriptEngine::getContext().get(), scopeUUID, UUID(), ass, t2); bpActor) {
			*actorID = bpActor.getScopedID();
			ScriptEngine::actorScript().spawnEvent(bpActor);
		} else {
			SHARD3D_ERROR("Failed to find prefab '{0}'!", ass.getAsset());
			*actorID = 1;
		}
		mono_free(t);
		mono_free(t2);
	}

	static void CreateEmptyActorUnderActor(uint64_t scopeUUID, uint64_t actorID, uint64_t* outscopeUUID, uint64_t* outactorID, MonoString* string, bool stationary) {
		Actor parentActor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		UUID scope;
		if (!parentActor.hasComponent<Components::PrefabComponent>() && parentActor.isPrefabChild()) {
			scope = parentActor.getInstancerUUID();
		} else if (parentActor.hasComponent<Components::PrefabComponent>()) {
			scope = parentActor.getGlobalUUID();
		} else {
			scope = 0; // spawned under a non prefab actor
		}

		char* t = mono_string_to_utf8(string);
		std::string text(t);
		Actor actor = ScriptEngine::getContext()->createActor(scope, text);
		if (stationary) {
			actor.makeStationary();
		} else {
			actor.makeDynamic();
		}
		ScriptEngine::getContext()->parentActor(actor, parentActor);
		*outactorID = actor.getScopedID();
		*outscopeUUID = actor.getInstancerUUID();
		mono_free(t);
	}
	static void SpawnPrefabActorUnderActor(uint64_t scopeUUID, uint64_t actorID, uint64_t* outscopeUUID, uint64_t* outactorID, MonoString* asset, MonoString* string) {
		Actor parentActor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);

		char* t = mono_string_to_utf8(asset);
		char* t2 = mono_string_to_utf8(string);

		UUID scope;
		if (!parentActor.hasComponent<Components::PrefabComponent>() && parentActor.isPrefabChild()) {
			scope = parentActor.getInstancerUUID();
		} else if (parentActor.hasComponent<Components::PrefabComponent>()) {
			scope = parentActor.getGlobalUUID();
		} else {
			scope = 0; // spawned under a non prefab actor
		}

		AssetID ass = AssetID(std::string(t));
		if (Actor bpActor = ScriptEngine::getContext()->getPrefabManager()->instPrefab(
			ScriptEngine::getContext().get(), scope, UUID(), ass, t2); bpActor) {
			ScriptEngine::getContext()->parentActor(bpActor, parentActor);
			*outactorID = bpActor.getScopedID();
			*outscopeUUID = bpActor.getInstancerUUID();
			ScriptEngine::actorScript().spawnEvent(bpActor);
		} else {
			SHARD3D_ERROR("Failed to find prefab '{0}'!", ass.getAsset());
			*outscopeUUID = 0;
			*outactorID = 1;
		}
		mono_free(t);
		mono_free(t2);
	}

	static void KillActor(uint64_t scopeUUID, uint64_t actorID) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		ScriptEngine::getContext()->killActor(actor);
	}

	static void Utils_UUIDCombine(size_t inheritID, size_t thisID, uint64_t* nextInheritID) {
		if (inheritID == 0) {
			*nextInheritID = thisID;
		} else {
			size_t seed = inheritID;
			hashCombine(seed, thisID);
			*nextInheritID = seed;
		}
	}

	static void GetActorByTag(uint64_t scopeUUID, uint64_t* actorID, MonoString* string) {
		char* t = mono_string_to_utf8(string);
		std::string text(t);
		Actor actor = ScriptEngine::getContext()->getActorFromTag(text);
		*actorID = actor ? actor.getScopedID() : 1;
		mono_free(t);
	}
	static void GetActorByPrefabTag(uint64_t scopeUUID, uint64_t actorID, uint64_t* actorID_out, MonoString* string) {
		SHARD3D_NOIMPL_C;
		char* t = mono_string_to_utf8(string);
		std::string text(t);
		auto& bpc = ScriptEngine::getContext()->getActorFromUUID(scopeUUID, actorID).getComponent<Components::PrefabComponent>();
	//	*actorID_out = ScriptEngine::getContext()->;
		mono_free(t);
	}
	static void GetActorFromPrefabBase(uint64_t scopeUUID, uint64_t actorID, uint64_t* actorID_out, MonoString* string) {
		char* t = mono_string_to_utf8(string);
		std::string text(t);
		auto& bpc = ScriptEngine::getContext()->getActorFromUUID(scopeUUID, actorID).getComponent<Components::PrefabComponent>();
		//	*actorID_out = ScriptEngine::getContext()->;
		mono_free(t);
	}

	static void Level_PossessCameraActor(uint64_t scopeUUID, uint64_t actorID) {
		ScriptEngine::getContext()->setPossessedCameraActor(
			ScriptEngine::getContext()->getActorFromUUID(scopeUUID, actorID));
	}
	static void Level_Load(MonoString* string) {
		char* t = mono_string_to_utf8(string);
		std::string text(t);

		ScriptEngine::switchLevel(text);

		mono_free(t);
	}

	static void TransformComponent_GetTranslation(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*v = actor.getTransform().getTranslation();
	}
	static void TransformComponent_SetTranslation(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getTransform().setTranslation(*v);
	}
	static void TransformComponent_GetRotation(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*v = actor.getTransform().getRotation();
	}
	static void TransformComponent_SetRotation(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getTransform().setRotation(*v);
	}
	static void TransformComponent_SetRotationWithMatrix(uint64_t scopeUUID, uint64_t actorID, glm::mat3x3* m) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getTransform().setRotation(TransformMath::decomposeEulerYXZ(*m));
	}
	static void TransformComponent_GetScale(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*v = actor.getTransform().getScale();
	}
	static void TransformComponent_SetScale(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getTransform().setScale(*v);
	}
	static void TransformComponent_GetTransformTranslationWorld(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*v = actor.getTransform().transformMatrix[3];
	}
	static void TransformComponent_GetTransformVectorRight(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*v = glm::normalize(actor.getTransform().transformMatrix[0]);
	}
	static void TransformComponent_GetTransformVectorUp(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*v = glm::normalize(actor.getTransform().transformMatrix[1]);
	}
	static void TransformComponent_GetTransformVectorForward(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*v = glm::normalize(actor.getTransform().transformMatrix[2]);
	}

	static void TransformComponent_SetRotationWithMatrixWorld(uint64_t scopeUUID, uint64_t actorID, glm::mat3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		bool rsc = actor.hasComponent<Components::RelationshipComponent>();
		glm::mat3 rotation = *v;
		if (rsc) {
			if (actor.getComponent<Components::RelationshipComponent>().parentActor != entt::null) {
				glm::mat3 actorTransform = rotation;
				auto& ptc = Actor(actor.getComponent<Components::RelationshipComponent>().parentActor, ScriptEngine::getContext().get()).getTransform();
				glm::mat3 parentTransform = ptc.transformMatrix;

				glm::vec3 invParentScale = glm::vec3(1.0) / glm::vec3{ glm::length(parentTransform[0]),glm::length(parentTransform[1]), glm::length(parentTransform[2]) };

				actorTransform[0] *= invParentScale.x;
				actorTransform[1] *= invParentScale.y;
				actorTransform[2] *= invParentScale.z;

				glm::mat3 parentRotation = glm::mat3(
					 parentTransform[0] * invParentScale.x,
					 parentTransform[1] * invParentScale.y,
					 parentTransform[2] * invParentScale.z
				);

				rotation = glm::inverse(parentRotation) * glm::mat3(actorTransform);
			}
		}
		glm::vec3 eulerRot = TransformMath::decomposeEulerYXZ(rotation);
		actor.getTransform().setRotation(eulerRot);
	}
	static void TransformComponent_SetTransformTranslationWorld(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		bool rsc = actor.hasComponent<Components::RelationshipComponent>();
		glm::vec3 translation = *v;
		if (rsc) { 
			if (actor.getComponent<Components::RelationshipComponent>().parentActor != entt::null) {

				glm::mat4 actorTransform = actor.getTransform().transformMatrix;
				actorTransform[3] = glm::vec4(*v, 1.0f);
				auto& ptc = Actor(actor.getComponent<Components::RelationshipComponent>().parentActor, ScriptEngine::getContext().get()).getTransform();
				glm::mat4 bullshit = glm::inverse(ptc.transformMatrix) * actorTransform;
				translation = bullshit[3];
			}
		}

		actor.getTransform().setTranslation(translation);
		
	}
	static void TransformComponent_SetTransformRotationWorld(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		//*v = glm::normalize(actor.getTransform().transformMatrix[2]);
	}
	static void TransformComponent_SetTransformScaleWorld(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		//*v = glm::normalize(actor.getTransform().transformMatrix[2]);
	}

	static void CameraComponent_GetFOV(uint64_t scopeUUID, uint64_t actorID, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*v = actor.getComponent<Components::CameraComponent>().fov;
	}

	static void CameraComponent_SetFOV(uint64_t scopeUUID, uint64_t actorID, float* v) {
		ScriptEngine::getContext()->getActorFromUUID(scopeUUID, actorID).getComponent<Components::CameraComponent>().fov = *v;
	}

	static void CameraComponent_GetProjectionType(uint64_t scopeUUID, uint64_t actorID, int* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*v = (int)actor.getComponent<Components::CameraComponent>().projectionType;
	}

	static void CameraComponent_SetProjectionType(uint64_t scopeUUID, uint64_t actorID, int* v) {
		ScriptEngine::getContext()->getActorFromUUID(scopeUUID, actorID).getComponent<Components::CameraComponent>().projectionType = (ProjectionType)*v;
	}

	static void CameraComponent_GetNearClip(uint64_t scopeUUID, uint64_t actorID, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*v = actor.getComponent<Components::CameraComponent>().nearClip;
	}

	static void CameraComponent_SetNearClip(uint64_t scopeUUID, uint64_t actorID, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getComponent<Components::CameraComponent>().nearClip = *v;
	}

	static void CameraComponent_GetFarClip(uint64_t scopeUUID, uint64_t actorID, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*v = actor.getComponent<Components::CameraComponent>().farClip;
	}

	static void CameraComponent_SetFarClip(uint64_t scopeUUID, uint64_t actorID, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getComponent<Components::CameraComponent>().farClip = *v;
	}
	
	static void PostProcessingComponent_GetProperty_HDR(uint64_t scopeUUID, uint64_t actorID, PostFXData::HDR* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		PostFXData& postFxData = actor.getComponent<Components::PostFXComponent>().postfx;
		v->exposure = postFxData.hdr.exposure;
	}
	static void PostProcessingComponent_SetProperty_HDR(uint64_t scopeUUID, uint64_t actorID, PostFXData::HDR* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		PostFXData& postFxData = actor.getComponent<Components::PostFXComponent>().postfx;
		postFxData.hdr.exposure = v->exposure;
	}
	static void PostProcessingComponent_GetProperty_ColorGrading(uint64_t scopeUUID, uint64_t actorID, PostFXData::ColorGrading* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		PostFXData& postFxData = actor.getComponent<Components::PostFXComponent>().postfx;
		v->contrast = postFxData.grading.contrast;
		v->saturation = postFxData.grading.saturation;
		v->gain = postFxData.grading.gain;
		v->temperature = postFxData.grading.temperature;
		v->hueShift = postFxData.grading.hueShift;
	}
	static void PostProcessingComponent_SetProperty_ColorGrading(uint64_t scopeUUID, uint64_t actorID, PostFXData::ColorGrading* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		PostFXData& postFxData = actor.getComponent<Components::PostFXComponent>().postfx;
		 postFxData.grading.contrast = v->contrast;
		 postFxData.grading.saturation = v->saturation;
		 postFxData.grading.gain = v->gain;
		 postFxData.grading.temperature = v->temperature;
		 postFxData.grading.hueShift = v->hueShift;
	}

	static void AudioComponent_GetAsset(uint64_t scopeUUID, uint64_t actorID, MonoString* string) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		string = mono_string_new(ScriptEngine::getDomain(), actor.getComponent<Components::AudioComponent>().file.c_str());
	}

	static void AudioComponent_SetAsset(uint64_t scopeUUID, uint64_t actorID, MonoString* string) {
		char* t = mono_string_to_utf8(string);
		std::string text(t);

		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getComponent<Components::AudioComponent>().file = text;
		mono_free(t);
	}

	static void AudioComponent_GetPropertiesVolume(uint64_t scopeUUID, uint64_t actorID, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*v = actor.getComponent<Components::AudioComponent>().properties.volume;
	}

	static void AudioComponent_SetPropertiesVolume(uint64_t scopeUUID, uint64_t actorID, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getComponent<Components::AudioComponent>().properties.volume = *v;
	}

	static void AudioComponent_GetPropertiesPitch(uint64_t scopeUUID, uint64_t actorID, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*v = actor.getComponent<Components::AudioComponent>().properties.pitch;
	}

	static void AudioComponent_SetPropertiesPitch(uint64_t scopeUUID, uint64_t actorID, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getComponent<Components::AudioComponent>().properties.pitch = *v;
	}

	static void AudioComponent_Play(uint64_t scopeUUID, uint64_t actorID) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getComponent<Components::AudioComponent>().play();
	}

	static void AudioComponent_Pause(uint64_t scopeUUID, uint64_t actorID) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getComponent<Components::AudioComponent>().pause();
	}

	static void AudioComponent_Stop(uint64_t scopeUUID, uint64_t actorID) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getComponent<Components::AudioComponent>().stop();
	}

	static void AudioComponent_Resume(uint64_t scopeUUID, uint64_t actorID) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getComponent<Components::AudioComponent>().resume();
	}

	static void AudioComponent_Update(uint64_t scopeUUID, uint64_t actorID) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getComponent<Components::AudioComponent>().update();
	}

	static void BoxVolumeComponent_CalculateInfluence(uint64_t scopeUUID, uint64_t actorID, glm::vec3* point_, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		auto& bxc = actor.getComponent<Components::BoxVolumeComponent>();
		auto& tfc = actor.getTransform();
		glm::mat4 invTransform = tfc.getInverseTransform();
		glm::vec3 lowerLimit = bxc.bounds;
		glm::vec3 upperLimit = bxc.bounds + bxc.transitionDistance;
		glm::vec3 point = invTransform * glm::vec4(*point_, 1.0);

		if (!glm::all(glm::lessThanEqual(-upperLimit, glm::vec3(point))) && glm::all(glm::lessThanEqual(glm::vec3(point), upperLimit))) {
			*v = 0.0f;
			return;
		}

		glm::vec3 higherX = glm::vec3(upperLimit.x, point.y, point.z);
		glm::vec3 higherY = glm::vec3(point.x, upperLimit.y, point.z);
		glm::vec3 higherZ = glm::vec3(point.x, point.y, upperLimit.z);

		float distX = glm::distance(point, higherX);
		float distY = glm::distance(point, higherY);
		float distZ = glm::distance(point, higherZ);

		glm::vec3 distLowToHigh = abs(lowerLimit - upperLimit);

		float diffX = glm::clamp(distX / distLowToHigh.x, 0.f, 1.f);
		float diffY = glm::clamp(distY / distLowToHigh.y, 0.f, 1.f);
		float diffZ = glm::clamp(distZ / distLowToHigh.z, 0.f, 1.f);

		*v = diffX * diffY * diffZ;
	}
	static void BoxVolumeComponent_GetBounds(uint64_t scopeUUID, uint64_t actorID, glm::vec3* b) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		auto& volume = actor.getComponent<Components::BoxVolumeComponent>();
		*b = volume.bounds;
	}
	static void BoxVolumeComponent_SetBounds(uint64_t scopeUUID, uint64_t actorID, glm::vec3* b) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		auto& volume = actor.getComponent<Components::BoxVolumeComponent>();
		volume.bounds = *b;
	}

	static void Mesh3DComponent_GetAsset(uint64_t scopeUUID, uint64_t actorID, MonoString* string) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		string = mono_string_new(ScriptEngine::getDomain(), actor.getComponent<Components::Mesh3DComponent>().asset.getAsset().c_str());
	}
	static void Mesh3DComponent_SetAsset(uint64_t scopeUUID, uint64_t actorID, MonoString* string) {
		AssetID meshAsset = INTERNAL_MonoStringToAssetID(string);
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		auto& c = actor.getComponent<Components::Mesh3DComponent>();
		
		c.asset = meshAsset;
		ScriptEngine::getResourceSystem()->loadMesh(meshAsset);
		c = Components::Mesh3DComponent(ScriptEngine::getResourceSystem(), meshAsset);
	}

	static void Mesh3DComponent_GetCastShadows(uint64_t scopeUUID, uint64_t actorID, bool* vis) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		auto& mesh = actor.getComponent<Components::Mesh3DComponent>();
		*vis = mesh.castShadows;
	}
	static void Mesh3DComponent_SetCastShadows(uint64_t scopeUUID, uint64_t actorID, bool vis) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		auto& mesh = actor.getComponent<Components::Mesh3DComponent>();
		mesh.castShadows = vis;
	}
	static void Mesh3DComponent_GetSurfaceMaterialList(uint64_t scopeUUID, uint64_t actorID, MonoString*** strings) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		auto& mesh = actor.getComponent<Components::Mesh3DComponent>();

		MonoString** array = reinterpret_cast<MonoString**>(malloc(mesh.getMaterials().size() * sizeof(MonoString*)));
		
		for (int i = 0; i < mesh.getMaterials().size(); i++) {
			array[i] = mono_string_new(ScriptEngine::getDomain(), mesh.getMaterials()[i].getAsset().c_str());
		}

		*strings = array;
	}
	static void Mesh3DComponent_SetSurfaceMaterialListEntry(uint64_t scopeUUID, uint64_t actorID, MonoString* string, size_t index) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		auto& mesh = actor.getComponent<Components::Mesh3DComponent>();
		if (index < mesh.getMaterials().size()) {
			mesh.setMaterial(INTERNAL_MonoStringToAssetID(string), index);
		}
	}

	static void DeferredDecalComponent_GetMaterialAsset(uint64_t scopeUUID, uint64_t actorID, MonoString** str) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		auto& decal = actor.getComponent<Components::DeferredDecalComponent>();
		*str = mono_string_new(ScriptEngine::getDomain(), decal.material.getAsset().c_str());
	}
	static void DeferredDecalComponent_SetMaterialAsset(uint64_t scopeUUID, uint64_t actorID, MonoString* str) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		if (actor.getMobility() == Mobility::Dynamic) {
			actor.killComponent<Components::DeferredDecalComponent>();
		}
		AssetID asset = INTERNAL_MonoStringToAssetID(str);
		actor.addComponent<Components::DeferredDecalComponent>().material = asset;
		ScriptEngine::getResourceSystem()->loadMaterial(asset);
	}
	static void DeferredDecalComponent_GetOpacity(uint64_t scopeUUID, uint64_t actorID, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		auto& decal = actor.getComponent<Components::DeferredDecalComponent>();
		*v = decal.opacity;
	}
	static void DeferredDecalComponent_SetOpacity(uint64_t scopeUUID, uint64_t actorID, float v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getComponent<Components::DeferredDecalComponent>().opacity = v;
	}
	static void DeferredDecalComponent_GetPriority(uint64_t scopeUUID, uint64_t actorID, uint32_t* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		auto& decal = actor.getComponent<Components::DeferredDecalComponent>();
		*v = decal.priority;
	}
	static void DeferredDecalComponent_SetPriority(uint64_t scopeUUID, uint64_t actorID, uint32_t v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getComponent<Components::DeferredDecalComponent>().priority = v;
	}

	static void BillboardComponent_GetAsset(uint64_t scopeUUID, uint64_t actorID, MonoString* string) {
		//string = mono_string_new(ScriptEngine::getDomain(), ScriptEngine::getContext()->getActorFromUUID(scopeUUID, actorID).getComponent<Components::BillboardComponent>().file.c_str());
	}

	static void BillboardComponent_SetAsset(uint64_t scopeUUID, uint64_t actorID, MonoString* string) {
		char* t = mono_string_to_utf8(string);
		std::string text(t);

		//ScriptEngine::getContext()->getActorFromUUID(scopeUUID, actorID).getComponent<Components::BillboardComponent>().cacheAsset = text;
		mono_free(t);
	}

	static void PointLightComponent_GetColor(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*v = actor.getComponent<Components::PointLightComponent>().color;
	}

	static void PointLightComponent_SetColor(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getComponent<Components::PointLightComponent>().color = *v;
	}

	static void PointLightComponent_GetIntensity(uint64_t scopeUUID, uint64_t actorID, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*v = actor.getComponent<Components::PointLightComponent>().lightIntensity;
	}

	static void PointLightComponent_SetIntensity(uint64_t scopeUUID, uint64_t actorID, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getComponent<Components::PointLightComponent>().lightIntensity = *v;
	}

	static void SpotLightComponent_GetColor(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*v = actor.getComponent<Components::SpotLightComponent>().color;
	}

	static void SpotLightComponent_SetColor(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getComponent<Components::SpotLightComponent>().color = *v;
	}

	static void SpotLightComponent_GetIntensity(uint64_t scopeUUID, uint64_t actorID, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*v = actor.getComponent<Components::SpotLightComponent>().lightIntensity;
	}

	static void SpotLightComponent_SetIntensity(uint64_t scopeUUID, uint64_t actorID, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getComponent<Components::SpotLightComponent>().lightIntensity = *v;
	}

	static void SpotLightComponent_GetOuterAngle(uint64_t scopeUUID, uint64_t actorID, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*v = 2.0f * glm::acos(actor.getComponent<Components::SpotLightComponent>().outerAngle);
	}

	static void SpotLightComponent_SetOuterAngle(uint64_t scopeUUID, uint64_t actorID, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getComponent<Components::SpotLightComponent>().outerAngle = glm::cos(*v / 2.0f);
	}

	static void SpotLightComponent_GetInnerAngle(uint64_t scopeUUID, uint64_t actorID, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*v = 2.0f * glm::acos(actor.getComponent<Components::SpotLightComponent>().innerAngle);
	}

	static void SpotLightComponent_SetInnerAngle(uint64_t scopeUUID, uint64_t actorID, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getComponent<Components::SpotLightComponent>().innerAngle = glm::cos(*v / 2.0f);
	}

	static void DirectionalLightComponent_GetColor(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*v = actor.getComponent<Components::DirectionalLightComponent>().color;
	}

	static void DirectionalLightComponent_SetColor(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getComponent<Components::DirectionalLightComponent>().color = *v;
	}

	static void DirectionalLightComponent_GetIntensity(uint64_t scopeUUID, uint64_t actorID, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*v = actor.getComponent<Components::DirectionalLightComponent>().lightIntensity;
	}

	static void DirectionalLightComponent_SetIntensity(uint64_t scopeUUID, uint64_t actorID, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		actor.getComponent<Components::DirectionalLightComponent>().lightIntensity = *v;
	}

	static void SkeletonRigComponent_BoneTransform_GetTranslation(uint64_t scopeUUID, uint64_t actorID, uint32_t boneID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		auto& skr = actor.getComponent<Components::SkeletonRigComponent>();
		*v = skr.boneTransforms[boneID].getTranslation();
	}
	static void SkeletonRigComponent_BoneTransform_SetTranslation(uint64_t scopeUUID, uint64_t actorID, uint32_t boneID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		auto& skr = actor.getComponent<Components::SkeletonRigComponent>();
		skr.boneTransforms[boneID].setTranslation(*v);
	}
	static void SkeletonRigComponent_BoneTransform_GetRotation(uint64_t scopeUUID, uint64_t actorID, uint32_t boneID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
	}
	static void SkeletonRigComponent_BoneTransform_SetRotation(uint64_t scopeUUID, uint64_t actorID, uint32_t boneID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
	}
	static void SkeletonRigComponent_BoneTransform_GetScale(uint64_t scopeUUID, uint64_t actorID, uint32_t boneID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		auto& skr = actor.getComponent<Components::SkeletonRigComponent>();
		*v = skr.boneTransforms[boneID].getScale();
	}
	static void SkeletonRigComponent_BoneTransform_SetScale(uint64_t scopeUUID, uint64_t actorID, uint32_t boneID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		auto& skr = actor.getComponent<Components::SkeletonRigComponent>();
		skr.boneTransforms[boneID].setScale(*v);
	}

	static void SkeletonRigComponent_SetAnimation(uint64_t scopeUUID, uint64_t actorID, MonoString* str) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		AssetID asset = INTERNAL_MonoStringToAssetID(str);
		ScriptEngine::getGlobalData()->animationSystem->loadAnimation(asset);
		ScriptEngine::getGlobalData()->animationRegistry[actor] = { false, 0.f, asset };
	}
	static void SkeletonRigComponent_StartAnimation(uint64_t scopeUUID, uint64_t actorID) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		std::get<0>(ScriptEngine::getGlobalData()->animationRegistry[actor]) = true;
	}
	static void SkeletonRigComponent_StopAnimation(uint64_t scopeUUID, uint64_t actorID) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		std::get<0>(ScriptEngine::getGlobalData()->animationRegistry[actor]) = false;
	}
	static void SkeletonRigComponent_SeekAnimation(uint64_t scopeUUID, uint64_t actorID, float time) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		std::get<1>(ScriptEngine::getGlobalData()->animationRegistry[actor]) = time;
	}
	static void SkeletonRigComponent_GetAnimationTimeElasped(uint64_t scopeUUID, uint64_t actorID, float* time) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*time = std::get<1>(ScriptEngine::getGlobalData()->animationRegistry[actor]);
	}


	static void Physics_SetGravity(glm::vec3* force) {
		dWorldSetGravity(LevelSimulationState::getPhysicsSystem()->getWorld(), force->x, force->z, force->y);
	}
	static void Physics_RayCast(uint64_t* actorID, uint64_t* scopeUUID, glm::vec3* position, glm::vec3* normal, glm::vec3* p, glm::vec3* d, float md) {
		auto res = LevelSimulationState::getPhysicsSystem()->rayCast(*p, *d, md);
		if (res.actor != entt::null) {
			Actor actor = Actor(res.actor, ScriptEngine::getContext().get());
			UUID scope = actor.getInstancerUUID();
			while (scope != 0) {
				actor = ScriptEngine::getContext()->getActorFromUUID(0, actor.getInstancerUUID());
				scope = actor.getInstancerUUID();
			}

			*actorID = actor.getScopedID();
			*scopeUUID = actor.getInstancerUUID();
			*position = res.position;
			*normal = res.normal;
		} else {
			*actorID = 1;
			*scopeUUID = 0;
		}
	}


	static void Rigidbody3DComponent_GetLinearVelocity(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		dBodyID body = (dBodyID)actor.getComponent<Components::Rigidbody3DComponent>().instantiatedActor;
		*v = *(glm::vec3*)dBodyGetLinearVel(body);
	}
	static void Rigidbody3DComponent_SetLinearVelocity(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		dBodyID body = (dBodyID)actor.getComponent<Components::Rigidbody3DComponent>().instantiatedActor;
		dBodySetLinearVel(body, v->x, v->z, v->y);
	}
	static void Rigidbody3DComponent_GetAngularVelocity(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		dBodyID body = (dBodyID)actor.getComponent<Components::Rigidbody3DComponent>().instantiatedActor;
		*v = *(glm::vec3*)dBodyGetAngularVel(body);
	}
	static void Rigidbody3DComponent_SetAngularVelocity(uint64_t scopeUUID, uint64_t actorID, glm::vec3* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		dBodyID body = (dBodyID)actor.getComponent<Components::Rigidbody3DComponent>().instantiatedActor;
		dBodySetAngularVel(body, v->x, v->z, v->y);
	}

	static void Rigidbody3DComponent_AddForceImpulse(uint64_t scopeUUID, uint64_t actorID, glm::vec3* i) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		dBodyID body = (dBodyID)actor.getComponent<Components::Rigidbody3DComponent>().instantiatedActor;
		dBodyAddForce(body, i->x, i->z, i->y);
	}
	static void Rigidbody3DComponent_AddForceImpulseAtPoint(uint64_t scopeUUID, uint64_t actorID, glm::vec3* i, glm::vec3* rp) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		dBodyID body = (dBodyID)actor.getComponent<Components::Rigidbody3DComponent>().instantiatedActor;
		dBodyAddForceAtRelPos(body, i->x, i->z, i->y, rp->x, rp->z, rp->y);
	}
	static void Rigidbody3DComponent_AddTorqueImpulse(uint64_t scopeUUID, uint64_t actorID, glm::vec3* i) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		dBodyID body = (dBodyID)actor.getComponent<Components::Rigidbody3DComponent>().instantiatedActor;
		dBodyAddTorque(body, i->x, i->z, i->y);
	}

	static bool Rigidbody3DComponent_CollidesWith(uint64_t scopeUUID, uint64_t actorID, uint64_t check_prefabParentInvocationID, uint64_t check_actorID) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		Actor check_actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), check_prefabParentInvocationID, check_actorID);
		
		bool found = false;
		int i = 0;
		auto& vec = actor.getComponent<Components::Rigidbody3DComponent>().collidingWith;
		while (!found && i < vec.size()) {
			Actor collidee = { vec[i].actor ,ScriptEngine::getContext().get() };
			UUID scope = collidee.getInstancerUUID();
			while (scope != 0) {
				collidee = ScriptEngine::getContext()->getActorFromUUID(0, collidee.getInstancerUUID());
				scope = collidee.getInstancerUUID();
			}
			found = collidee == check_actor.actorHandle;
		}
		return found;
	}
	static void Rigidbody3DComponent_GetCollidingWithArraySize(uint64_t scopeUUID, uint64_t actorID, uint64_t* size) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		auto& colliders = actor.getComponent<Components::Rigidbody3DComponent>().collidingWith;
		*size = colliders.size();
	}
	static void Rigidbody3DComponent_GetCollidingWithArrayData(uint64_t scopeUUID, uint64_t actorID, MonoArray** listOfColliding) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		auto& colliders = actor.getComponent<Components::Rigidbody3DComponent>().collidingWith;
		struct internalPhyicsContact_s {
			uint64_t scope;
			uint64_t id;
			glm::vec3 point;
			glm::vec3 normal;
		};
		int index = 0;
		std::vector<UUID> uniqueColliderActors{};

		for (int i = 0; i < colliders.size(); i++) {
			Actor collidingActor = Actor(colliders[i].actor, ScriptEngine::getContext().get());

			std::vector<entt::entity> isolatedArray(colliders.size());
			UUID scope = collidingActor.getInstancerUUID();
			while (scope != 0) {
				collidingActor = ScriptEngine::getContext()->getActorFromUUID(0, collidingActor.getInstancerUUID());
				scope = collidingActor.getInstancerUUID();
			}
			if (std::find(uniqueColliderActors.begin(), uniqueColliderActors.end(), collidingActor.getScopedID()) == uniqueColliderActors.end()) {
				internalPhyicsContact_s contactData;
				contactData.scope = collidingActor.getInstancerUUID();
				contactData.id = collidingActor.getScopedID();
				contactData.normal = colliders[i].normal;
				contactData.point = colliders[i].position;
				mono_array_set(*listOfColliding, internalPhyicsContact_s, index, contactData);
				uniqueColliderActors.push_back(static_cast<UUID>(contactData.id));
				index++;
			}
		}
	}


	static void IndependentSuspensionComponent_GetSteeringAngle(uint64_t scopeUUID, uint64_t actorID, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		*v = actor.getComponent<Components::IndependentSuspensionComponent>().steeringAngle;
	}
	static void IndependentSuspensionComponent_SetSteeringAngle(uint64_t scopeUUID, uint64_t actorID, float* v) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		auto& pct = actor.getComponent<Components::IndependentSuspensionComponent>();
		pct.steeringAngle = *v;
		dJointSetPistonParam((dJointID)pct.suspensionActor, dParamLoStop2, *v);
		dJointSetPistonParam((dJointID)pct.suspensionActor, dParamHiStop2, *v);
		dJointSetPistonParam((dJointID)pct.suspensionActor, dParamLoStop2, *v);
		dJointSetPistonParam((dJointID)pct.suspensionActor, dParamHiStop2, *v);
	}

	static void IndependentSuspensionComponent_GetAxleVelocity(uint64_t scopeUUID, uint64_t actorID, float* vel) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		auto& pct = actor.getComponent<Components::IndependentSuspensionComponent>();
		*vel = dJointGetHingeAngleRate((dJointID)pct.axleActor);
	}
	static void IndependentSuspensionComponent_SetAxleVelocity(uint64_t scopeUUID, uint64_t actorID, float* vel, float* maxF) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		auto& pct = actor.getComponent<Components::IndependentSuspensionComponent>();
		dJointSetHingeParam((dJointID)pct.axleActor, dParamVel, *vel);
		dJointSetHingeParam((dJointID)pct.axleActor, dParamFMax, *maxF);
	}
	static void IndependentSuspensionComponent_SetAxleAcceleration(uint64_t scopeUUID, uint64_t actorID, float* accel, float* maxF) {
		Actor actor = INTERNAL_GetTrueActor(ScriptEngine::getContext().get(), scopeUUID, actorID);
		auto& pct = actor.getComponent<Components::IndependentSuspensionComponent>();
		dJointSetHingeParam((dJointID)pct.axleActor, dParamVel, dJointGetHingeParam((dJointID)pct.axleActor, dParamVel) + *accel);
		dJointSetHingeParam((dJointID)pct.axleActor, dParamFMax, *maxF);
	}





	//HUD

	static void HUDLayer_Display(MonoString* asset) {
		AssetID hudAsset = INTERNAL_MonoStringToAssetID(asset);
		auto& hudLayer = LevelSimulationState::getHUDSystem()->getHUDLayer(hudAsset.getAsset());
		HUDRenderSystem::loadHUDLayerAsset(hudAsset.getAbsolute(), hudLayer, ScriptEngine::getResourceSystem());
		ScriptEngine::instantiateHUDList(hudLayer);
	}
	static void HUDLayer_Close(MonoString* asset) {
		AssetID hudAsset = INTERNAL_MonoStringToAssetID(asset);
		LevelSimulationState::getHUDSystem()->eraseHUDLayer(hudAsset.getAsset());
	}
	static std::string randomString(const int len) {
		static const char alphanum[] =
			"0123456789"
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			"abcdefghijklmnopqrstuvwxyz";
		std::string tmp_s;
		tmp_s.reserve(len);

		for (int i = 0; i < len; ++i) {
			tmp_s += alphanum[UUID() % (sizeof(alphanum) - 1)];
		}

		return tmp_s;
	}
	static void HUDLayer_CreateElement(MonoString* layer, MonoString* type, HUDElementData** outHandle, MonoString** outElementName) {
		std::string layerString = INTERNAL_MonoStringToString(layer);
		std::string typeString = INTERNAL_MonoStringToString(type);
		auto& hudLayer = LevelSimulationState::getHUDSystem()->getHUDLayer(layerString);

		std::string str;
		SHUD::Element::Base* genericBase;
		if (strUtils::contains(typeString, "Line")) {
			str = "Line " + randomString(12);
			genericBase = new SHUD::Element::Line();
		} else if (strUtils::contains(typeString, "Rect")) {
			str = "Rect " + randomString(12);
			genericBase = new SHUD::Element::Rect();
		} else if (strUtils::contains(typeString, "Image")) {
			str = "Image " + randomString(12);
			genericBase = new SHUD::Element::Image();
		}
		*outHandle = new HUDElementData();
		(*outHandle)->genericBase = genericBase;
		hudLayer.emplace(str, *outHandle);
		*outElementName = mono_string_new(ScriptEngine::getDomain(), str.c_str());
	}

	static HUDElementData* searchHUDElement(const std::string& element, std::unordered_map<std::string, HUDElementData*>& list) {
		HUDElementData* data = nullptr;
		if (auto iter = list.find(element); iter == list.end()) {
			for (auto& [str, subData] : list) {
				data = searchHUDElement(element, subData->children);
			}
		} else {
			data = iter->second;
		}
		return data;
	}
	static void HUDLayer_GetElement(MonoString* layer, MonoString* elementName, HUDElementData** outHandle) {
		std::string layerString = INTERNAL_MonoStringToString(layer);
		std::string elementString = INTERNAL_MonoStringToString(elementName);
		auto& hudLayer = LevelSimulationState::getHUDSystem()->getHUDLayer(layerString);

		*outHandle = searchHUDElement(elementString, hudLayer);
	}
	static void HUDLayer_EraseElement(MonoString* layer, MonoString* elementName) {
		std::string layerString = INTERNAL_MonoStringToString(layer);
		std::string elementString = INTERNAL_MonoStringToString(elementName);
		auto& hudLayer = LevelSimulationState::getHUDSystem()->getHUDLayer(layerString);
		LevelSimulationState::getHUDSystem()->eraseHUDElement(elementString, hudLayer);
	}


	static SHUD::fvec4 toSHUDFVec4Col(SHUD::RGBAColor col) {
		return {
			col.r / 255.f,
			col.g / 255.f,
			col.b / 255.f,
			col.a / 255.f
		};
	}
	
	static void HUDElement_GetVisible(HUDElementData* data, bool* v) {
		*v = data->visible;
	}
	static void HUDElement_SetVisible(HUDElementData* data, bool v) {
		data->visible = v;
	}
	
	static void HUDElement_Line_GetPointA(HUDElementData* data, SHUD::fvec2* v) {
		*v = dynamic_cast<SHUD::Element::Line*>(data->genericBase)->mPointA;
	}
	static void HUDElement_Line_SetPointA(HUDElementData* data, SHUD::fvec2* v) {
		dynamic_cast<SHUD::Element::Line*>(data->genericBase)->mPointA = *v;
	}
	static void HUDElement_Line_GetPointB(HUDElementData* data, SHUD::fvec2* v) {
		*v = dynamic_cast<SHUD::Element::Line*>(data->genericBase)->mPointB;
	}
	static void HUDElement_Line_SetPointB(HUDElementData* data, SHUD::fvec2* v) {
		dynamic_cast<SHUD::Element::Line*>(data->genericBase)->mPointB = *v;
	}
	static void HUDElement_Line_GetWidth(HUDElementData* data, float* v) {
		*v = dynamic_cast<SHUD::Element::Line*>(data->genericBase)->mWidth;
	}
	static void HUDElement_Line_SetWidth(HUDElementData* data, float* v) {
		dynamic_cast<SHUD::Element::Line*>(data->genericBase)->mWidth = *v;
	}
	static void HUDElement_Line_GetColor(HUDElementData* data, SHUD::fvec4* v) {
		*v = toSHUDFVec4Col(dynamic_cast<SHUD::Element::Line*>(data->genericBase)->mColor);
	}
	static void HUDElement_Line_SetColor(HUDElementData* data, SHUD::fvec4* v) {
		dynamic_cast<SHUD::Element::Line*>(data->genericBase)->mColor = SHUD::RGBAColor(*v);
	}
	
	static void HUDElement_Rect_GetPosition(HUDElementData* data, SHUD::fvec2* v) {
		*v = dynamic_cast<SHUD::Element::Rect*>(data->genericBase)->mTransform.mPosition;
	}
	static void HUDElement_Rect_SetPosition(HUDElementData* data, SHUD::fvec2* v) {
		dynamic_cast<SHUD::Element::Rect*>(data->genericBase)->mTransform.mPosition = *v;
	}
	static void HUDElement_Rect_GetScale(HUDElementData* data, SHUD::fvec2* v) {
		*v = dynamic_cast<SHUD::Element::Rect*>(data->genericBase)->mTransform.mScale;
	}
	static void HUDElement_Rect_SetScale(HUDElementData* data, SHUD::fvec2* v) {
		dynamic_cast<SHUD::Element::Rect*>(data->genericBase)->mTransform.mScale = *v;
	}
	static void HUDElement_Rect_GetRotation(HUDElementData* data, float* v) {
		*v = dynamic_cast<SHUD::Element::Rect*>(data->genericBase)->mTransform.mRotation;
	}
	static void HUDElement_Rect_SetRotation(HUDElementData* data, float* v) {
		dynamic_cast<SHUD::Element::Rect*>(data->genericBase)->mTransform.mRotation = *v;
	}
	static void HUDElement_Rect_GetColor(HUDElementData* data, SHUD::fvec4* v) {
		*v = toSHUDFVec4Col(dynamic_cast<SHUD::Element::Rect*>(data->genericBase)->mColor);
	}
	static void HUDElement_Rect_SetColor(HUDElementData* data, SHUD::fvec4* v) {
		dynamic_cast<SHUD::Element::Rect*>(data->genericBase)->mColor = SHUD::RGBAColor(*v);
	}

	static void HUDElement_Image_GetPosition(HUDElementData* data, SHUD::fvec2* v) {
		*v = dynamic_cast<SHUD::Element::Image*>(data->genericBase)->mTransform.mPosition;
	}
	static void HUDElement_Image_SetPosition(HUDElementData* data, SHUD::fvec2* v) {
		dynamic_cast<SHUD::Element::Image*>(data->genericBase)->mTransform.mPosition = *v;
	}
	static void HUDElement_Image_GetScale(HUDElementData* data, SHUD::fvec2* v) {
		*v = dynamic_cast<SHUD::Element::Image*>(data->genericBase)->mTransform.mScale;
	}
	static void HUDElement_Image_SetScale(HUDElementData* data, SHUD::fvec2* v) {
		dynamic_cast<SHUD::Element::Image*>(data->genericBase)->mTransform.mScale = *v;
	}
	static void HUDElement_Image_GetRotation(HUDElementData* data, float* v) {
		*v = dynamic_cast<SHUD::Element::Image*>(data->genericBase)->mTransform.mRotation;
	}
	static void HUDElement_Image_SetRotation(HUDElementData* data, float* v) {
		dynamic_cast<SHUD::Element::Image*>(data->genericBase)->mTransform.mRotation = *v;
	}
	static void HUDElement_Image_GetColor(HUDElementData* data, SHUD::fvec4* v) {
		*v = toSHUDFVec4Col(dynamic_cast<SHUD::Element::Image*>(data->genericBase)->mColor);
	}
	static void HUDElement_Image_SetColor(HUDElementData* data, SHUD::fvec4* v) {
		dynamic_cast<SHUD::Element::Image*>(data->genericBase)->mColor = SHUD::RGBAColor(*v);
	}
	static void HUDElement_Image_GetTexture(HUDElementData* data, MonoString** asset) {
		*asset = mono_string_new(ScriptEngine::getDomain(), data->textureAtlasAsset.getAsset().c_str());
	}
	static void HUDElement_Image_SetTexture(HUDElementData* data, MonoString* asset) {
		AssetID texAsset = INTERNAL_MonoStringToAssetID(asset);
		data->textureAtlasAsset = texAsset;
	}

	static void HUDElement_Text_GetPosition(HUDElementData* data, SHUD::fvec2* v) {
		*v = dynamic_cast<SHUD::Element::Text*>(data->genericBase)->mTransform.mPosition;
	}
	static void HUDElement_Text_SetPosition(HUDElementData* data, SHUD::fvec2* v) {
		dynamic_cast<SHUD::Element::Text*>(data->genericBase)->mTransform.mPosition = *v;
	}
	static void HUDElement_Text_GetScale(HUDElementData* data, SHUD::fvec2* v) {
		*v = dynamic_cast<SHUD::Element::Text*>(data->genericBase)->mTransform.mScale;
	}
	static void HUDElement_Text_SetScale(HUDElementData* data, SHUD::fvec2* v) {
		dynamic_cast<SHUD::Element::Text*>(data->genericBase)->mTransform.mScale = *v;
	}
	static void HUDElement_Text_GetRotation(HUDElementData* data, float* v) {
		*v = dynamic_cast<SHUD::Element::Text*>(data->genericBase)->mTransform.mRotation;
	}
	static void HUDElement_Text_SetRotation(HUDElementData* data, float* v) {
		dynamic_cast<SHUD::Element::Text*>(data->genericBase)->mTransform.mRotation = *v;
	}
	static void HUDElement_Text_GetColor(HUDElementData* data, SHUD::fvec4* v) {
		*v = toSHUDFVec4Col(dynamic_cast<SHUD::Element::Text*>(data->genericBase)->mColor);
	}
	static void HUDElement_Text_SetColor(HUDElementData* data, SHUD::fvec4* v) {
		dynamic_cast<SHUD::Element::Text*>(data->genericBase)->mColor = SHUD::RGBAColor(*v);
	}
	static void HUDElement_Text_GetText(HUDElementData* data, MonoString** string) {
		*string = mono_string_new(ScriptEngine::getDomain(), dynamic_cast<SHUD::Element::Text*>(data->genericBase)->mText.c_str());
	}
	static void HUDElement_Text_SetText(HUDElementData* data, MonoString* string) {
		std::string text = INTERNAL_MonoStringToString(string);
		dynamic_cast<SHUD::Element::Text*>(data->genericBase)->mText = text;
	}

	static void HUDElement_Text_FormatTextParam(HUDElementData* data, MonoString* param, MonoString* value) {
		std::string paramString = INTERNAL_MonoStringToString(param);
		std::string valueString = INTERNAL_MonoStringToString(value);
		data->textFormattingData[paramString] = valueString;
	}



	//MATERIAL

	static void Material_SurfaceMaterial_CreateInstance(MonoString* asset, MonoString** outInstAsset) {
		AssetID sfAsset = INTERNAL_MonoStringToAssetID(asset);
		AssetID instAsset = ScriptEngine::getResourceSystem()->createNewSurfaceMaterialInstance(sfAsset);
		*outInstAsset = mono_string_new(ScriptEngine::getDomain(), instAsset.getAsset().c_str());
	}

	static void Material_SurfaceMaterial_Property_GetEmissiveStrength(MonoString* asset, float* v) {
		SurfaceMaterial* sf = (SurfaceMaterial*)ScriptEngine::getResourceSystem()->retrieveMaterial(INTERNAL_MonoStringToAssetID(asset));
		if (sf->shadingModel == SurfaceShadingModel::Unshaded || sf->shadingModel == SurfaceShadingModel::Emissive) {
			*v = sf->emissiveColor.w;
			return;
		}
		*v = NAN;
	}
	static void Material_SurfaceMaterial_Property_SetEmissiveStrength(MonoString* asset, float v) {
		SurfaceMaterial* sf = (SurfaceMaterial*)ScriptEngine::getResourceSystem()->retrieveMaterial(INTERNAL_MonoStringToAssetID(asset));
		if (sf->shadingModel == SurfaceShadingModel::Unshaded || sf->shadingModel == SurfaceShadingModel::Emissive) {
			sf->emissiveColor.w = v;
			ScriptEngine::getResourceSystem()->runtimeUpdateSurfaceMaterial(sf);
		}
	}
	static void Material_SurfaceMaterial_Property_GetEmissiveColor(MonoString* asset, glm::vec3* v) {
		SurfaceMaterial* sf = (SurfaceMaterial*)ScriptEngine::getResourceSystem()->retrieveMaterial(INTERNAL_MonoStringToAssetID(asset));
		if (sf->shadingModel == SurfaceShadingModel::Unshaded || sf->shadingModel == SurfaceShadingModel::Emissive) {
			*v = glm::vec3(sf->emissiveColor);
			return;
		}
		*v = glm::vec3(NAN);
	}
	static void Material_SurfaceMaterial_Property_SetEmissiveColor(MonoString* asset, glm::vec3* v) {
		SurfaceMaterial* sf = (SurfaceMaterial*)ScriptEngine::getResourceSystem()->retrieveMaterial(INTERNAL_MonoStringToAssetID(asset));
		if (sf->shadingModel == SurfaceShadingModel::Unshaded || sf->shadingModel == SurfaceShadingModel::Emissive) {
			(glm::vec3&)(sf->emissiveColor) = *v;
			ScriptEngine::getResourceSystem()->runtimeUpdateSurfaceMaterial(sf);
		}
	}
	static void Material_SurfaceMaterial_Property_GetDiffuseColor(MonoString* asset, glm::vec3* v) {
		SurfaceMaterial* sf = (SurfaceMaterial*)ScriptEngine::getResourceSystem()->retrieveMaterial(INTERNAL_MonoStringToAssetID(asset));
		if (sf->shadingModel == SurfaceShadingModel::Shaded || sf->shadingModel == SurfaceShadingModel::Emissive) {
			*v = glm::vec3(sf->diffuseColor);
			return;
		}
		*v = glm::vec3(NAN);
	}
	static void Material_SurfaceMaterial_Property_SetDiffuseColor(MonoString* asset, glm::vec3* v) {
		SurfaceMaterial* sf = (SurfaceMaterial*)ScriptEngine::getResourceSystem()->retrieveMaterial(INTERNAL_MonoStringToAssetID(asset));
		if (sf->shadingModel == SurfaceShadingModel::Shaded || sf->shadingModel == SurfaceShadingModel::Emissive) {
			(glm::vec3&)(sf->diffuseColor) = *v;
			ScriptEngine::getResourceSystem()->runtimeUpdateSurfaceMaterial(sf);
		}
	}
	static void Material_SurfaceMaterial_Property_GetSpecular(MonoString* asset, float* v) {
		SurfaceMaterial* sf = (SurfaceMaterial*)ScriptEngine::getResourceSystem()->retrieveMaterial(INTERNAL_MonoStringToAssetID(asset));
		if (sf->shadingModel == SurfaceShadingModel::Shaded || sf->shadingModel == SurfaceShadingModel::Emissive) {
			*v = sf->specular;
			return;
		}
		*v = NAN;
	}
	static void Material_SurfaceMaterial_Property_SetSpecular(MonoString* asset, float v) {
		SurfaceMaterial* sf = (SurfaceMaterial*)ScriptEngine::getResourceSystem()->retrieveMaterial(INTERNAL_MonoStringToAssetID(asset));
		if (sf->shadingModel == SurfaceShadingModel::Shaded || sf->shadingModel == SurfaceShadingModel::Emissive) {
			sf->specular = v;
			ScriptEngine::getResourceSystem()->runtimeUpdateSurfaceMaterial(sf);
		}
	}
	static void Material_SurfaceMaterial_Property_GetGlossiness(MonoString* asset, float* v) {
		SurfaceMaterial* sf = (SurfaceMaterial*)ScriptEngine::getResourceSystem()->retrieveMaterial(INTERNAL_MonoStringToAssetID(asset));
		if (sf->shadingModel == SurfaceShadingModel::Shaded || sf->shadingModel == SurfaceShadingModel::Emissive) {
			*v = sf->specular;
			return;
		}
		*v = NAN;
	}
	static void Material_SurfaceMaterial_Property_SetGlossiness(MonoString* asset, float v) {
		SurfaceMaterial* sf = (SurfaceMaterial*)ScriptEngine::getResourceSystem()->retrieveMaterial(INTERNAL_MonoStringToAssetID(asset));
		if (sf->shadingModel == SurfaceShadingModel::Shaded || sf->shadingModel == SurfaceShadingModel::Emissive) {
			sf->specular = v;
			ScriptEngine::getResourceSystem()->runtimeUpdateSurfaceMaterial(sf);
		}
	}
	static void Material_SurfaceMaterial_Property_GetReflectivity(MonoString* asset, float* v) {
		SurfaceMaterial* sf = (SurfaceMaterial*)ScriptEngine::getResourceSystem()->retrieveMaterial(INTERNAL_MonoStringToAssetID(asset));
		if (sf->shadingModel == SurfaceShadingModel::Shaded || sf->shadingModel == SurfaceShadingModel::Emissive) {
			*v = sf->specular;
			return;
		}
		*v = NAN;
	}
	static void Material_SurfaceMaterial_Property_SetReflectivity(MonoString* asset, float v) {
		SurfaceMaterial* sf = (SurfaceMaterial*)ScriptEngine::getResourceSystem()->retrieveMaterial(INTERNAL_MonoStringToAssetID(asset));
		if (sf->shadingModel == SurfaceShadingModel::Shaded || sf->shadingModel == SurfaceShadingModel::Emissive) {
			sf->specular = v;
			ScriptEngine::getResourceSystem()->runtimeUpdateSurfaceMaterial(sf);
		}
	}
}

namespace Shard3D {
	template <typename _MyComponent>
	void registerComponent() {
		std::string_view type_name = typeid(_MyComponent).name();
		size_t pos = type_name.find_last_of(":");
		std::string_view struct_name = type_name.substr(pos + 1);
		std::string corrected_typename = fmt::format("Shard3D.Components.{}", struct_name);
		MonoType* managedType = mono_reflection_type_from_name(corrected_typename.data(), ScriptEngine::getCoreAssemblyImage());
		if (!managedType) {
			SHARD3D_ERROR("Failed to find component {0}!", corrected_typename);
			return;
		}

		h_ctr->monoTypeRegistryVector.push_back(managedType);
		h_ctr->monoTypeRegistryAppenderHasComponent.push_back([](ECS::Actor actor) { return actor.hasComponent<_MyComponent>(); });
		h_ctr->monoTypeRegistryAppenderAddComponent.push_back([](ECS::Actor actor) { actor.addComponent<_MyComponent>(); });
		h_ctr->monoTypeRegistryAppenderRmvComponent.push_back([](ECS::Actor actor) { actor.killComponent<_MyComponent>(); });
		SHARD3D_INFO("Registered component: '{0}'", corrected_typename);
	}

	void ScriptEngineLinker::registerLinker(S3DWindow* mainWindowPtr, const CppCustomInternalCallCreateInfo& info) {
		windowPtr = mainWindowPtr;
		registerInternalCalls(info);
		h_ctr = new _helper_container();
		registerComponents();
	}
	void ScriptEngineLinker::registerInternalCalls(const CppCustomInternalCallCreateInfo& info) {

		for (int i = 0; i < info.numInteralCalls; i++) {
			CppCustomInternalCallData& data = info.cppUserInternalCalls[i];
			mono_add_internal_call(data.name, data.function);
		}

		S3D_ICALL(Log);
		S3D_ICALL(LogNoImpl);
		
		S3D_ICALL(Utils_UUIDCombine);

		S3D_ICALL(SpawnPrefabActor);
		S3D_ICALL(CreateEmptyActor);
		S3D_ICALL(SpawnPrefabActorUnderActor);
		S3D_ICALL(CreateEmptyActorUnderActor);

		S3D_ICALL(KillActor);
		S3D_ICALL(Actor_HasComponent);
		S3D_ICALL(Actor_AddComponent);
		S3D_ICALL(Actor_RmvComponent);
		S3D_ICALL(Actor_GetPrefabInterface);
		S3D_ICALL(GetActorByTag);
		S3D_ICALL(GetActorByPrefabTag);
		
		S3D_ICALL(Level_PossessCameraActor);
		S3D_ICALL(Level_Load);
		
		S3D_ICALL(IsKeyDown);
		S3D_ICALL(IsMouseButtonDown);
		S3D_ICALL(GetMousePosition);
		S3D_ICALL(IsGamepadPresent);
		S3D_ICALL(IsGamepadButtonDown);
		S3D_ICALL(GetGamepadAnalogAxis);

		S3D_ICALL(GetWindowResolution);

		S3D_ICALL(TransformComponent_SetTranslation);
		S3D_ICALL(TransformComponent_GetTranslation);
		S3D_ICALL(TransformComponent_SetRotation);
		S3D_ICALL(TransformComponent_GetRotation);
		S3D_ICALL(TransformComponent_SetRotationWithMatrix);
		S3D_ICALL(TransformComponent_SetScale);
		S3D_ICALL(TransformComponent_GetScale);
		S3D_ICALL(TransformComponent_GetTransformTranslationWorld);
		S3D_ICALL(TransformComponent_GetTransformVectorRight);
		S3D_ICALL(TransformComponent_GetTransformVectorUp);
		S3D_ICALL(TransformComponent_GetTransformVectorForward);
		S3D_ICALL(TransformComponent_SetRotationWithMatrixWorld);
		S3D_ICALL(TransformComponent_SetTransformTranslationWorld);

		S3D_ICALL(CameraComponent_GetFOV);
		S3D_ICALL(CameraComponent_SetFOV);
		S3D_ICALL(CameraComponent_GetProjectionType);
		S3D_ICALL(CameraComponent_SetProjectionType);
		S3D_ICALL(CameraComponent_GetNearClip);
		S3D_ICALL(CameraComponent_SetNearClip);
		S3D_ICALL(CameraComponent_GetFarClip);
		S3D_ICALL(CameraComponent_SetFarClip);

		S3D_ICALL(PostProcessingComponent_GetProperty_HDR);
		S3D_ICALL(PostProcessingComponent_SetProperty_HDR);
		S3D_ICALL(PostProcessingComponent_GetProperty_ColorGrading);
		S3D_ICALL(PostProcessingComponent_SetProperty_ColorGrading);

		S3D_ICALL(AudioComponent_GetAsset);
		S3D_ICALL(AudioComponent_SetAsset);
		S3D_ICALL(AudioComponent_GetPropertiesPitch);
		S3D_ICALL(AudioComponent_SetPropertiesPitch);
		S3D_ICALL(AudioComponent_GetPropertiesVolume);
		S3D_ICALL(AudioComponent_SetPropertiesVolume);
		S3D_ICALL(AudioComponent_Play);
		S3D_ICALL(AudioComponent_Stop);
		S3D_ICALL(AudioComponent_Pause);
		S3D_ICALL(AudioComponent_Resume);
		S3D_ICALL(AudioComponent_Update);

		S3D_ICALL(BoxVolumeComponent_CalculateInfluence);
		S3D_ICALL(BoxVolumeComponent_GetBounds);
		S3D_ICALL(BoxVolumeComponent_SetBounds);

		S3D_ICALL(Mesh3DComponent_GetAsset);
		S3D_ICALL(Mesh3DComponent_SetAsset);
		S3D_ICALL(Mesh3DComponent_GetCastShadows);
		S3D_ICALL(Mesh3DComponent_SetCastShadows);
		S3D_ICALL(Mesh3DComponent_GetSurfaceMaterialList);
		S3D_ICALL(Mesh3DComponent_SetSurfaceMaterialListEntry);

		S3D_ICALL(DeferredDecalComponent_GetMaterialAsset);
		S3D_ICALL(DeferredDecalComponent_SetMaterialAsset);
		S3D_ICALL(DeferredDecalComponent_GetOpacity);
		S3D_ICALL(DeferredDecalComponent_SetOpacity);
		S3D_ICALL(DeferredDecalComponent_GetPriority);
		S3D_ICALL(DeferredDecalComponent_SetPriority);

		S3D_ICALL(BillboardComponent_GetAsset);
		S3D_ICALL(BillboardComponent_SetAsset);
		
		S3D_ICALL(PointLightComponent_GetIntensity);
		S3D_ICALL(PointLightComponent_SetIntensity);
		S3D_ICALL(PointLightComponent_GetColor);
		S3D_ICALL(PointLightComponent_SetColor);
		
		S3D_ICALL(SpotLightComponent_GetColor);
		S3D_ICALL(SpotLightComponent_SetColor);
		S3D_ICALL(SpotLightComponent_GetIntensity);
		S3D_ICALL(SpotLightComponent_SetIntensity);
		S3D_ICALL(SpotLightComponent_GetOuterAngle);
		S3D_ICALL(SpotLightComponent_SetOuterAngle);
		S3D_ICALL(SpotLightComponent_GetInnerAngle);
		S3D_ICALL(SpotLightComponent_SetInnerAngle);
		
		S3D_ICALL(DirectionalLightComponent_GetColor);
		S3D_ICALL(DirectionalLightComponent_SetColor);
		S3D_ICALL(DirectionalLightComponent_GetIntensity);
		S3D_ICALL(DirectionalLightComponent_SetIntensity);

		S3D_ICALL(SkeletonRigComponent_BoneTransform_GetTranslation);
		S3D_ICALL(SkeletonRigComponent_BoneTransform_SetTranslation);
		S3D_ICALL(SkeletonRigComponent_BoneTransform_GetRotation);
		S3D_ICALL(SkeletonRigComponent_BoneTransform_SetRotation);
		S3D_ICALL(SkeletonRigComponent_BoneTransform_GetScale);
		S3D_ICALL(SkeletonRigComponent_BoneTransform_SetScale);
		S3D_ICALL(SkeletonRigComponent_SetAnimation);
		S3D_ICALL(SkeletonRigComponent_StartAnimation);
		S3D_ICALL(SkeletonRigComponent_StopAnimation);
		S3D_ICALL(SkeletonRigComponent_SeekAnimation);
		S3D_ICALL(SkeletonRigComponent_GetAnimationTimeElasped);
		

		S3D_ICALL(Physics_SetGravity);
		S3D_ICALL(Physics_RayCast);

		S3D_ICALL(Rigidbody3DComponent_GetLinearVelocity);
		S3D_ICALL(Rigidbody3DComponent_SetLinearVelocity);
		S3D_ICALL(Rigidbody3DComponent_GetAngularVelocity);
		S3D_ICALL(Rigidbody3DComponent_SetAngularVelocity);
		S3D_ICALL(Rigidbody3DComponent_AddForceImpulse);
		S3D_ICALL(Rigidbody3DComponent_AddForceImpulseAtPoint);
		S3D_ICALL(Rigidbody3DComponent_AddTorqueImpulse);
		S3D_ICALL(Rigidbody3DComponent_CollidesWith);
		S3D_ICALL(Rigidbody3DComponent_GetCollidingWithArraySize);
		S3D_ICALL(Rigidbody3DComponent_GetCollidingWithArrayData);

		S3D_ICALL(IndependentSuspensionComponent_GetSteeringAngle);
		S3D_ICALL(IndependentSuspensionComponent_SetSteeringAngle);
		S3D_ICALL(IndependentSuspensionComponent_GetAxleVelocity);
		S3D_ICALL(IndependentSuspensionComponent_SetAxleVelocity);
		S3D_ICALL(IndependentSuspensionComponent_SetAxleAcceleration);

		S3D_ICALL(HUDLayer_Display);
		S3D_ICALL(HUDLayer_Close);
		S3D_ICALL(HUDLayer_CreateElement);
		S3D_ICALL(HUDLayer_GetElement);
		S3D_ICALL(HUDLayer_EraseElement);
		S3D_ICALL(HUDElement_GetVisible);
		S3D_ICALL(HUDElement_SetVisible);
		S3D_ICALL(HUDElement_Line_GetPointA);
		S3D_ICALL(HUDElement_Line_SetPointA);
		S3D_ICALL(HUDElement_Line_GetPointB);
		S3D_ICALL(HUDElement_Line_SetPointB);
		S3D_ICALL(HUDElement_Line_GetWidth);
		S3D_ICALL(HUDElement_Line_SetWidth);
		S3D_ICALL(HUDElement_Line_GetColor);
		S3D_ICALL(HUDElement_Line_SetColor);
		S3D_ICALL(HUDElement_Rect_GetPosition);
		S3D_ICALL(HUDElement_Rect_SetPosition);
		S3D_ICALL(HUDElement_Rect_GetScale);
		S3D_ICALL(HUDElement_Rect_SetScale);
		S3D_ICALL(HUDElement_Rect_GetRotation);
		S3D_ICALL(HUDElement_Rect_SetRotation);
		S3D_ICALL(HUDElement_Rect_GetColor);
		S3D_ICALL(HUDElement_Rect_SetColor);
		S3D_ICALL(HUDElement_Image_GetPosition);
		S3D_ICALL(HUDElement_Image_SetPosition);
		S3D_ICALL(HUDElement_Image_GetScale);
		S3D_ICALL(HUDElement_Image_SetScale);
		S3D_ICALL(HUDElement_Image_GetRotation);
		S3D_ICALL(HUDElement_Image_SetRotation);
		S3D_ICALL(HUDElement_Image_GetColor);
		S3D_ICALL(HUDElement_Image_SetColor);
		S3D_ICALL(HUDElement_Image_GetTexture);
		S3D_ICALL(HUDElement_Image_SetTexture);
		S3D_ICALL(HUDElement_Text_GetPosition);
		S3D_ICALL(HUDElement_Text_SetPosition);
		S3D_ICALL(HUDElement_Text_GetScale);
		S3D_ICALL(HUDElement_Text_SetScale);
		S3D_ICALL(HUDElement_Text_GetRotation);
		S3D_ICALL(HUDElement_Text_SetRotation);
		S3D_ICALL(HUDElement_Text_GetColor);
		S3D_ICALL(HUDElement_Text_SetColor);
		S3D_ICALL(HUDElement_Text_GetText);
		S3D_ICALL(HUDElement_Text_SetText);
		S3D_ICALL(HUDElement_Text_FormatTextParam);



		S3D_ICALL(Material_SurfaceMaterial_Property_GetEmissiveStrength);
		S3D_ICALL(Material_SurfaceMaterial_Property_SetEmissiveStrength);
		S3D_ICALL(Material_SurfaceMaterial_Property_GetEmissiveColor);
		S3D_ICALL(Material_SurfaceMaterial_Property_SetEmissiveColor);
		S3D_ICALL(Material_SurfaceMaterial_Property_GetDiffuseColor);
		S3D_ICALL(Material_SurfaceMaterial_Property_SetDiffuseColor);
		S3D_ICALL(Material_SurfaceMaterial_Property_GetSpecular);
		S3D_ICALL(Material_SurfaceMaterial_Property_SetSpecular);
		S3D_ICALL(Material_SurfaceMaterial_Property_GetGlossiness);
		S3D_ICALL(Material_SurfaceMaterial_Property_SetGlossiness);
		S3D_ICALL(Material_SurfaceMaterial_Property_GetReflectivity);
		S3D_ICALL(Material_SurfaceMaterial_Property_SetReflectivity);
	}
	void ScriptEngineLinker::registerComponents() {
		registerComponent<Components::TransformComponent>();
		registerComponent<Components::BillboardComponent>();
		registerComponent<Components::Mesh3DComponent>();
		registerComponent<Components::AudioComponent>();
		registerComponent<Components::CameraComponent>();
		registerComponent<Components::DirectionalLightComponent>();
		registerComponent<Components::PointLightComponent>();
		registerComponent<Components::SpotLightComponent>();
		registerComponent<Components::SkeletonRigComponent>();
		registerComponent<Components::Rigidbody3DComponent>();
		registerComponent<Components::PhysicsConstraintComponent>();
		registerComponent<Components::IndependentSuspensionComponent>();
		registerComponent<Components::BoxVolumeComponent>();
		registerComponent<Components::DeferredDecalComponent>();
	}
}