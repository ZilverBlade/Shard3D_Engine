#pragma once
#include <string>
#include "../s3dstd.h"
#include "../core.h"
#include "../core/misc/UUID.h"
#include "../core/ecs/level.h"
#include "../systems/rendering/hud_system.h"

extern "C" {
	typedef struct _MonoClass MonoClass;
	typedef struct _MonoObject MonoObject;
	typedef struct _MonoMethod MonoMethod;
	typedef struct _MonoAssembly MonoAssembly;
	typedef struct _MonoImage MonoImage;
	typedef struct _MonoDomain MonoDomain;
}

namespace Shard3D {
	class S3DWindow;
	class ResourceSystem;
	class SkeletalAnimationManager;
	inline namespace ECS {
		class Level;
		class Actor;
	}
	class ScriptClass {
	public:
		ScriptClass() = default;
		ScriptClass(const std::string& c_ns, const std::string& c_n, bool global);

		MonoObject* inst();
		MonoMethod* getMethod(const std::string& name, int parameterCount);
		MonoObject* invokeMethod(MonoObject* inst, MonoMethod* method, void** params = nullptr);
		MonoClass* getClass() {
			return monoClass;
		}
	private:
		std::string classNamespace, className;
		MonoClass* monoClass = nullptr;
	};
	class ScriptInstance {
	public:
		ScriptInstance(sPtr<ScriptClass> scriptClass);
		sPtr<ScriptClass> getClass() {
			return scriptClass;
		}
		MonoObject* getInstance() {
			return instance;
		}
	protected:
		sPtr<ScriptClass> scriptClass;

		MonoMethod* constructor{};
		MonoObject* instance{};
	};
	class ActorScriptInstance : public ScriptInstance {
		struct ScriptEvents {
			ScriptEvents() = default;
			ScriptEvents(sPtr<ScriptClass> ptr, MonoObject* i);
			void beginEvent();
			void endEvent();
			void tickEvent(float dt);
			void physicsTickEvent(float ts);
			void spawnEvent();
			void killEvent();

			MonoMethod* beginEventMethod{};
			MonoMethod* endEventMethod{};
			MonoMethod* tickEventMethod{};
			MonoMethod* physicsTickEventMethod{};
			MonoMethod* spawnEventMethod{};
			MonoMethod* killEventMethod{};

			sPtr<ScriptClass> s_c;
			MonoObject* i_{};
		};
		ActorScriptInstance::ScriptEvents scriptEvents;
	public:
		ActorScriptInstance(sPtr<ScriptClass> s_class, ECS::Actor actor);
		ActorScriptInstance::ScriptEvents invokeEvent();
	};
	class HUDElementScriptInstance : public ScriptInstance {
		struct ScriptEvents {
			ScriptEvents() = default;
			ScriptEvents(sPtr<ScriptClass> ptr, MonoObject* i);
			void clickEvent();
			void pressEvent();
			void hoverEvent();

			MonoMethod* clickEventMethod{};
			MonoMethod* pressEventMethod{};
			MonoMethod* hoverEventMethod{};

			sPtr<ScriptClass> s_c;
			MonoObject* i_{};
		};
		HUDElementScriptInstance::ScriptEvents scriptEvents;
	public:
		HUDElementScriptInstance(sPtr<ScriptClass> s_class, HUDElementData* data, std::string elementName);
		HUDElementScriptInstance::ScriptEvents invokeEvent();
	};
	class SurfaceMaterialScriptInstance : public ScriptInstance {
		struct ScriptEvents {
			ScriptEvents() = default;
			ScriptEvents(sPtr<ScriptClass> ptr, MonoObject* i);
			void update(float dt);

			MonoMethod* updateMethod{};

			sPtr<ScriptClass> s_c;
			MonoObject* i_{};
		};
		SurfaceMaterialScriptInstance::ScriptEvents scriptEvents;
	public:
		SurfaceMaterialScriptInstance(sPtr<ScriptClass> s_class, AssetID material);
		SurfaceMaterialScriptInstance::ScriptEvents invokeEvent();
	};


	struct GlobalScriptEngineData {
		MonoDomain* rootDomain = nullptr;
		MonoDomain* appDomain = nullptr;
		MonoAssembly* coreAssembly = nullptr;
		MonoImage* coreAssemblyImage = nullptr;

		ScriptClass actorClass;
		ScriptClass hudElementClass;
		ScriptClass surfaceMaterialClass;

		sPtr<Level> levelContext{};
		ResourceSystem* resourceSystem{};
		SkeletalAnimationManager* animationSystem{};
		std::unordered_map<entt::entity, std::tuple<bool, float, AssetID>> animationRegistry;
	};

	struct ScriptEngineData {
		MonoAssembly* appAssembly = nullptr;
		MonoImage* appAssemblyImage = nullptr;

		std::unordered_map<std::string, sPtr<ScriptClass>> actorClasses;
		std::unordered_map<std::string, sPtr<ScriptClass>> hudElementClasses;
		std::unordered_map<std::string, sPtr<ScriptClass>> surfaceMaterialClasses;
		std::unordered_map<Shard3D::UUID, sPtr<ActorScriptInstance>> actorInstances;
		std::unordered_map<Shard3D::UUID, sPtr<HUDElementScriptInstance>> hudElementInstances;
		std::unordered_map<Shard3D::UUID, sPtr<SurfaceMaterialScriptInstance>> surfaceMaterialInstances;
	};
	class ScriptEngine {
	public:	
		struct VirtualActorCalls {
			void beginEvent();
			void endEvent();
			bool tickEvent(float dt);
			void physicsTickEvent(float ts);
			void spawnEvent(ECS::Actor actor);
			void killEvent(ECS::Actor actor);
		};
		struct VirtualHUDElementCalls {
			void clickEvent(SHUD::Element::Base* element);
			void hoverEvent(SHUD::Element::Base* element);
			void pressEvent(SHUD::Element::Base* element);
		};
		struct VirtualSurfaceMaterialCalls {
			void update(float dt);
		};


		static void instantiateHUDList(std::unordered_map<std::string, HUDElementData*> list);
		static void instantiateActors();

		static void init(S3DWindow* window);
		static void destroy();
		static void reloadAssembly();
		static void _loadCoreAssembly();
		static MonoImage* getCoreAssemblyImage();
		static MonoDomain* getDomain();
		static GlobalScriptEngineData* getGlobalData();
		static ScriptEngineData* getScriptEngineData();
		static std::unordered_map<std::string, sPtr<ScriptClass>> getActorClasses();
		static std::unordered_map<std::string, sPtr<ScriptClass>> getHUDElementClasses();
		static MonoClass* getActorClass();
		static void runtimeStart(sPtr<ECS::Level>& level, ResourceSystem* resourceSystem);
		static void runtimeStop();
		static void updateSkeletonAnimations(float dt);
		static bool doesActorClassExist(const std::string& fullClassName);
		static bool doesHUDElementClassExist(const std::string& fullClassName);
		static bool doesSurfaceMaterialClassExist(const std::string& fullClassName);
		static bool doesAssemblyExist();
		static sPtr<Level>& getContext();
		static void switchLevel(AssetID path);
		static ResourceSystem* getResourceSystem();
		static VirtualActorCalls actorScript() { return VirtualActorCalls(); }
		static VirtualHUDElementCalls hudElementScript() { return VirtualHUDElementCalls(); }
		static VirtualSurfaceMaterialCalls surfaceMaterialScript() { return VirtualSurfaceMaterialCalls(); }
	private:
		static void _reloadAssembly(ScriptEngineData* scriptEngine);
		static void destroyMono(ScriptEngineData* scriptEngine);

		static MonoObject* instClass(MonoClass* monoClass);
		static void loadAssemblyClasses(ScriptEngineData* data);
		friend class ScriptClass;
		friend class ActorScriptEngineLinker;
		friend struct MonoTypeCombo;
	};


}