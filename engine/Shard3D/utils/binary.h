#pragma once
#include "../core.h"
#include "../s3dstd.h"
namespace Shard3D {
	class BinaryStream {
	public:
		BinaryStream() {
		}
		~BinaryStream() {
			free(buffer);
		}
		
		inline void push_back(void* data, size_t data_size) {
			allocate(data_size);
			memcpy(buffer + buffer_seek, data, data_size);
		}
		inline void array_back(void* array, size_t data_size, size_t array_size) {
			size_t size = data_size * array_size;
			allocate(size);
			memcpy(buffer + buffer_seek, array, size);
		}
		template<typename T>
		inline void array_back(std::vector<T>& data) {
			size_t size = sizeof(T) * data.size();
			allocate(size);
			memcpy(buffer + buffer_seek, data.data(), size);
		}
		template<typename T>
		inline void push_back(T& data) {
			T temp = data;
			push_back(&temp, sizeof(T));
		}
		template<typename T>
		inline void push_back(T&& data) {
			T temp = data;
			push_back(&temp, sizeof(T));
		}
		template<typename T>
		inline void operator<<(T& data) {
			push_back(&data, sizeof(T));
		}
		template<typename T>
		inline void operator<<(T&& data) {
			push_back(data);
		}
		inline void merge_back(BinaryStream& other) {
			allocate(other.buffer_size);
			memcpy(buffer + buffer_seek, other.buffer, other.buffer_size);
		}

		inline void operator<<(BinaryStream& other) {
			merge_back(other);
		}

		inline const size_t size() const { return buffer_size; }
		inline const void* get() const { return reinterpret_cast<void*>(buffer); }
	private:
		inline void allocate(size_t size) {
			buffer_seek = buffer_size;
			buffer_size += size;
			void* new_buff{};
			new_buff = realloc(buffer, buffer_size);
			SHARD3D_ENSURE(new_buff, "failed to allocate binary buffer stream memory!");
			buffer = reinterpret_cast<uint8_t*>(new_buff);
		}

		size_t buffer_size{};
		size_t buffer_seek{};
		uint8_t* buffer{};
	};
}