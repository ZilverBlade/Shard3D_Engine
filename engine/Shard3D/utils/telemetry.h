#pragma once
#include "../core.h"
#include "../core/vulkan_api/device.h"
#include <vector>
#include <chrono>
#include <unordered_set>

namespace Shard3D {
	class TelemetryMetric;
	class Telemetry {
	public:
		DELETE_COPY(Telemetry);
		Telemetry(S3DDevice& device);
		~Telemetry();
		void registerTelemetryMetric(TelemetryMetric* metric);
		const auto& getTelemetryData() {
			return telemetryData;
		}
	private:
		VkQueryPool queryPool;
		uint64_t queryCount;
		S3DDevice& engineDevice;
		friend class TelemetryGPUMetric;
		friend class TelemetryCPUMetric;
		std::vector<TelemetryMetric*> telemetryData;
	};

	class TelemetryMetric {
	public:
		DELETE_COPY(TelemetryMetric);
		TelemetryMetric(Telemetry* telemetry, const std::string& category, const std::string& stat)
			: category_(category), stat_name_(stat), time_elapsed(0.0), telemetry_(telemetry){}

		double getTime() {
			return time_elapsed;
		}
		const char* getCategory() {
			return category_.c_str();
		}
		const char* getEntry() {
			return stat_name_.c_str();
		}
	protected:
		friend class Telemetry;
		Telemetry* telemetry_;
		std::string category_, stat_name_;
		double time_elapsed;
	};

	class TelemetryGPUMetric : public TelemetryMetric {
	public:
		TelemetryGPUMetric(Telemetry* telemetry, const std::string& category, const std::string& stat);
		~TelemetryGPUMetric();
		void record(VkCommandBuffer commandBuffer, VkPipelineStageFlagBits pipelineStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT);
		void end(VkCommandBuffer commandBuffer, VkPipelineStageFlagBits pipelineStage = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT);
	protected:
		uint32_t index;
		uint64_t buffer[2];
	};

	class TelemetryCPUMetric : public TelemetryMetric {
	public:
		TelemetryCPUMetric(Telemetry* telemetry, const std::string& category, const std::string& stat);
		~TelemetryCPUMetric();

		void record();
		void end();
	protected:
		std::chrono::steady_clock::time_point timeRecordBegin;
	};


}