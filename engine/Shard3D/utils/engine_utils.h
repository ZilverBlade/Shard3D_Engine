#pragma once
#include <sys/stat.h>
#include <string>
#include <vector>
namespace Shard3D {
	// from: https://stackoverflow.com/a/57595105
	template <typename T, typename... Rest>
	static void hashCombine(std::size_t& seed, const T& v, const Rest&... rest) {
		seed ^= std::hash<T>{}(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
		(hashCombine(seed, rest), ...);
	};

	template <class T>
	static void SafeRelease(T** ppT)
	{
		if (*ppT)
		{
			(*ppT)->Release();
			*ppT = NULL;
		}
	}
	
	class strUtils {
	public:
		static inline bool hasEnding(std::string const& fullString, std::string const& ending) {
			if ((fullString.length() < ending.length())) return false;
			return (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
		}
		static inline bool hasStarting(std::string const& fullString, std::string const& starting) {
			return (fullString.rfind(starting, 0) == 0);
		}
		static inline bool contains(std::string const& fullString, std::string const& sub) {
			return fullString.find(sub) != std::string::npos;
		}
		static void replace(std::string& fullString, std::string const& replacee, std::string const& replacer);
		//static std::string replace_all(std::string const& fullString, std::string const& replacee, std::string const& replacer);
		static std::string removeIllegalPathChars(const std::string & string);
	};

	class VectorUtils {
	public:
		template <typename T>
		static void eraseItemAtIndex(std::vector<T>& vector, size_t index) {
			vector.erase(vector.begin() + index);
		}
		template <typename T>
		static void eraseItem(std::vector<T>& vector, T item) {
			int i = 0;
			while ((vector.at(i) != item)) { i++; };
			vector.erase(vector.begin() + i);
		}
		template <typename T>
		static bool itemExists(std::vector<T>& vector, T item) {
			return std::find(vector.begin(), vector.end(), item) != vector.end();
		}

		template <typename Key, typename Val>
		static Val getItemFromPair(std::vector<std::pair<Key, Val>>& vector, Key key) {
			int i = 0;
			while ((vector.at(i).first != key)) {
				i++;
			};
			return vector.at(i).second;
		}
		template <typename T>
		static void pushBackUnique(std::vector<T>& vector, T item) {
			if (std::find(vector.begin(), vector.end(), item) != vector.end()) return;
			vector.push_back(item);
		}
		template <typename T>
		static void merge(std::vector<T>& vector, const std::vector<T>& other) {
			vector.reserve(vector.size() + other.size());
			for (const T& item : other) vector.push_back(item);
		}
	};

	class IOUtils {
	public:
		static void writeStackBinary(const void* data, size_t object_size, const std::string& path);
		static std::vector<uint8_t> getStackBinary(void* data, size_t object_size);
		static std::vector<char> readBinary(const std::string& path);
		
		static std::string convertWinPathToUnixPath(const std::string& path);
		static std::string readText(const std::string& path, bool binary = false);
		static void clearDirectory(const std::string& path);
		static void writeText(const std::string& text, const std::string& path);
		static void writeText(const char* text, const std::string& path);
		static std::string getFileOnly(const std::string& total);
		static bool doesFileExist(const std::string& assetPath);

		static bool isSubPath(const std::string& sub, const std::string& main);
	};

	class WAITUtils {
	public: static void preciseStandby(float seconds);
	};

	template <typename T, typename MyVal>
	inline T& mem_cast(MyVal myVal) { return *reinterpret_cast<T*>(&myVal); static_assert(false, "dont use this garbage"); }
	template <typename T>
	inline T& mem_cast(uintptr_t myPtr) { return *reinterpret_cast<T*>(myPtr); static_assert(false ,"dont use this garbage"); }
	template <typename T>
	inline T& mem_cast(const void* myVal) { return *reinterpret_cast<T*>(myVal); static_assert(false , "dont use this garbage"); }
} 

#define BIT(x) (1 << x)
#define SHARD3D_WAITFOR(seconds) Shard3D::WAITUtils::preciseStandby(seconds)

#define TOSTRING(myval) #myval


