#pragma once
#define YAML_CPP_STATIC_DEFINE

#include <glm/glm.hpp>
#include <simdjson.h>
#include <nlohmann/json.hpp>
#include "../core/asset/assetid.h"
#include "versioning.h"
#include "../s3dstd.h"

#define SIMDJSON_READ_VEC2(dom) glm::vec2{ dom.get_array().at(0).get_double().value(), dom.get_array().at(1).get_double().value() }
#define SIMDJSON_READ_VEC3(dom) glm::vec3{ dom.get_array().at(0).get_double().value(), dom.get_array().at(1).get_double().value(), dom.get_array().at(2).get_double().value() }
#define SIMDJSON_READ_VEC4(dom) glm::vec4{ dom.get_array().at(0).get_double().value(), dom.get_array().at(1).get_double().value(), dom.get_array().at(2).get_double().value(), dom.get_array().at(3).get_double().value() }
#define SIMDJSON_READ_IVEC2(dom) glm::ivec2{ dom.get_array().at(0).get_int64().value(), dom.get_array().at(1).get_int64().value()}
#define SIMDJSON_READ_IVEC3(dom) glm::ivec3{ dom.get_array().at(0).get_int64().value(), dom.get_array().at(1).get_int64().value(), dom.get_array().at(2).get_int64().value() }
#define SIMDJSON_READ_IVEC4(dom) glm::ivec4{ dom.get_array().at(0).get_int64().value(), dom.get_array().at(1).get_int64().value(), dom.get_array().at(2).get_int64().value(), dom.get_array().at(3).get_int64().value()  }
#define SIMDJSON_READ_S3DVERSION(dom) Shard3D::Version(Shard3D::Version::getVState(dom.get_array().at(3).get_c_str().value()), dom.get_array().at(0).get_int64().value(), dom.get_array().at(1).get_int64().value(), dom.get_array().at(2).get_int64().value())


static Shard3D::Types SIMDJSON_READ_S3DTYPE(simdjson::dom::document_stream::iterator::value_type str) {
		auto domv = str.get_string().value();
		if (domv == "float")	{ return Shard3D::Types::Float;}
		if (domv == "float2")	{ return Shard3D::Types::Float2; }
		if (domv == "float3")	{ return Shard3D::Types::Float3;  }
		if (domv == "float4")	{ return Shard3D::Types::Float4; }
		if (domv == "int8")		{ return Shard3D::Types::Int8;	}
		if (domv == "int16")	{ return Shard3D::Types::Int16;	}
		if (domv == "int32")	{ return Shard3D::Types::Int32;	}
		if (domv == "uint64")	{ return Shard3D::Types::Int64;	 }
		if (domv == "uint8")	{ return Shard3D::Types::UInt8; }
		if (domv == "uint16")	{ return Shard3D::Types::UInt16;   }
		if (domv == "uint32")	{ return Shard3D::Types::UInt32;  }
		if (domv == "uint64")	{ return Shard3D::Types::UInt64;  }
} 

namespace glm {
	static void to_json(nlohmann::ordered_json& j, const glm::vec2& v) {
		j = nlohmann::ordered_json::array();
		j = { v.x, v.y };
	}
	static void from_json(const nlohmann::ordered_json& j, glm::vec2& v) {
		j[0].get_to(v.x);
		j[1].get_to(v.y);
	}
	static void to_json(nlohmann::ordered_json& j, const glm::vec3& v) {
		j = nlohmann::ordered_json::array();
		j = { v.x, v.y, v.z };
	}
	static void from_json(const nlohmann::ordered_json& j, glm::vec3& v) {
		j[0].get_to(v.x);
		j[1].get_to(v.y);
		j[2].get_to(v.z);
	}
	static void to_json(nlohmann::ordered_json& j, const glm::ivec2& v) {
		j = nlohmann::ordered_json::array();
		j = { v.x, v.y };
	}
	static void from_json(const nlohmann::ordered_json& j, glm::ivec2& v) {
		j[0].get_to(v.x);
		j[1].get_to(v.y);
	}
	static void to_json(nlohmann::ordered_json& j, const glm::ivec3& v) {
		j = nlohmann::ordered_json::array();
		j = { v.x, v.y, v.z };
	}
	static void from_json(const nlohmann::ordered_json& j, glm::ivec3& v) {
		j[0].get_to(v.x);
		j[1].get_to(v.y);
		j[2].get_to(v.z);
	}
	static void to_json(nlohmann::ordered_json& j, const glm::ivec4& v) {
		j = nlohmann::ordered_json::array();
		j = { v.x, v.y, v.z, v.w };
	}
	static void from_json(const nlohmann::ordered_json& j, glm::ivec4& v) {
		j[0].get_to(v.x);
		j[1].get_to(v.y);
		j[2].get_to(v.z);
		j[3].get_to(v.w);
	}
	static void to_json(nlohmann::ordered_json& j, const glm::vec4& v) {
		j = nlohmann::ordered_json::array();
		j = { v.x, v.y, v.z, v.w };
	}
	static void from_json(const nlohmann::ordered_json& j, glm::vec4& v) {
		j[0].get_to(v.x);
		j[1].get_to(v.y);
		j[2].get_to(v.z);
		j[3].get_to(v.w);
	}
}
namespace Shard3D {
	static void to_json(nlohmann::ordered_json& j, const std::vector<AssetID> & v) {
		std::vector<std::string> materialAssetStrings{};
		for (const auto& asset : v) {
			materialAssetStrings.push_back(asset.getAsset());
		}
		j = nlohmann::ordered_json::array();
		j = materialAssetStrings;
	}
	static void from_json(const nlohmann::ordered_json& j, std::vector<AssetID>& v) {
		std::vector<std::string> materialAssetStrings;
		j.get_to(materialAssetStrings);
		for (int i = 0; i < materialAssetStrings.size(); i++) {
			v[i] = AssetID(materialAssetStrings[i]);
		}
	}
	static void to_json(nlohmann::ordered_json& j, const Version& v) {
		j = nlohmann::ordered_json::array();
		j = { v.getMajor(), v.getMinor(), v.getPatch(), v.getState() };
	}
	static void from_json(const nlohmann::ordered_json& j, Version& v) {
		v = Version(
			Version::getVState(j[3].get<std::string>().c_str()),
			j[0].get<int>(),
			j[1].get<int>(),
			j[2].get<int>()
		);
	}
}

//static YAML::Emitter& operator<<(YAML::Emitter& out, const Types& v) {
//	switch (v) {
//	case (Types::Float):	return out << "float";
//	case (Types::Float2):	return out << "float2";
//	case (Types::Float3):	return out << "float3";
//	case (Types::Float4):	return out << "float4";
//	case (Types::Int8):		return out << "int8";
//	case (Types::Int16):	return out << "int16";
//	case (Types::Int32):	return out << "int32";
//	case (Types::Int64):	return out << "int64";
//	case (Types::UInt8):	return out << "uint8";
//	case (Types::UInt16):	return out << "uint16";
//	case (Types::UInt32):	return out << "uint32";
//	case (Types::UInt64):	return out << "uint64";
//	}
//}
