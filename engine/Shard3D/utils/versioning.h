#pragma once
#include <string>
namespace Shard3D {
	enum class VersionState {
		PreAlpha, Alpha, Beta, Stable, Experimental,
		NONE = UINT32_MAX
	};
	struct Version {
		Version() = default;
		Version(VersionState _state, int _major, int _minor, int _patch = 0) :
			major(_major), minor(_minor), patch(_patch), state(_state) {
			setBuild();
		}
		Version(int _major, int _minor, int _patch = 0) :
			major(_major), minor(_minor), patch(_patch), state(VersionState::NONE) {
			setBuild();
		}

		Version(const Version& other) : major(other.major), minor(other.minor), patch(other.patch), state(other.state) {
			setBuild();
		};
		Version operator=(const Version& other) { return Version(other); };

		bool potentiallyUnstable() const { return state != VersionState::Stable; }
		std::string toString() const {
			return std::string(
				std::to_string(major) + "." +
				std::to_string(minor) + "." +
				std::to_string(patch) + "b" +
				std::to_string(build) + "-" +
				getState()
			);
		}
		std::wstring toWString() const {
			return std::wstring(
				std::to_wstring(major) + L"." +
				std::to_wstring(minor) + L"." +
				std::to_wstring(patch) + L"b" +
				std::to_wstring(build) + L"-" +
				getStateW()
			);
		}
		std::string getVersion() const {
			return std::string(
				std::to_string(major) + "." +
				std::to_string(minor) + "." +
				std::to_string(patch)
			);
		}
		std::wstring getVersionW() const {
			return std::wstring(
				std::to_wstring(major) + L"." +
				std::to_wstring(minor) + L"." +
				std::to_wstring(patch)
			);
		}
		std::wstring getMajorMinorWP() const {
			return std::wstring(
				std::to_wstring(major) + L"." +
				std::to_wstring(minor)
			);
		}
		bool operator <(const Version& other) const { 
			if (major < other.major) return true;
			if (minor < other.minor) return true;
			return patch < other.patch;
		}
		bool operator >(const Version& other) const {
			if (major > other.major) return true;
			if (minor > other.minor) return true;
			return patch > other.patch;
		}
		bool operator ==(const Version& other) const { return major == other.major && minor == other.minor && patch == other.patch; }
		bool operator !=(const Version& other) const { return !(*this == other); }
		bool operator <=(const Version& other) const {
			return (*this < other) || (*this == other);
		}
		bool operator >=(const Version& other) const {
			return (*this > other) || (*this == other);
		}
		inline operator std::string () const {  
			return toString();
		}
		inline int getMajor() const {
			return major;
		}
		inline int getMinor() const {
			return minor;
		}
		inline int getPatch() const {
			return patch;
		}
		inline int getBuild() const {
			return build;
		}

		inline int getVersionInt() const {
			return (major << 16) | (minor << 8) | (patch);
		}

		inline std::string getState() const {
			switch (state) {
			case VersionState::PreAlpha:
				return "prealpha";
			case VersionState::Alpha:
				return "alpha";
			case VersionState::Beta:
				return "beta";
			case VersionState::Stable:
				return "stable";
			case VersionState::Experimental:
				return "experimental";
			}
		}
		static VersionState getVState(const char* state) {
			if (state == "prealpha") return VersionState::PreAlpha;
			if (state == "alpha") return VersionState::Alpha;
			if (state == "beta") return VersionState::Beta;
			if (state == "stable") return VersionState::Stable;
			if (state == "experimental") return VersionState::Experimental;
		}
		inline std::wstring getStateW() const {
			switch (state) {
			case VersionState::PreAlpha:
				return L"prealpha";
			case VersionState::Alpha:
				return L"alpha";
			case VersionState::Beta:
				return L"beta";
			case VersionState::Stable:
				return L"stable";
			case VersionState::Experimental:
				return L"experimental";
			}
		}
	private:
		inline void setBuild() {
			std::string date = __DATE__;
			int day = std::stoi(date.substr(4, 2));
			int year = std::stoi(date.substr(7, 4));
			int month{};
			std::string month_ = date.substr(0, 3);
			do {
				if (month_ == "Jan") { month = 1; break; }
				if (month_ == "Feb") { month = 2; break; }
				if (month_ == "Mar") { month = 3; break; }
				if (month_ == "Apr") { month = 4; break; }
				if (month_ == "May") { month = 5; break; }
				if (month_ == "Jun") { month = 6; break; }
				if (month_ == "Jul") { month = 7; break; }
				if (month_ == "Aug") { month = 8; break; }
				if (month_ == "Sep") { month = 9; break; }
				if (month_ == "Oct") { month = 10; break; }
				if (month_ == "Nov") { month = 11; break; }
				if (month_ == "Dec") { month = 12; break; }
			} while (false);

			// YYYY MM	DD
			//		MM
			//			DD
			build = year * 10000 + month * 100 + day;
		}

		unsigned int major, minor, patch, build;
		VersionState state = VersionState::PreAlpha;
		// DO NOT CHANGE THE DATA, NOR THE ORDER, .S3DMESH FILES RELY ON THIS BYTE LAYOUT AND SIZE
	};
}