#include "logger.h" 
#include "dialogs.h" 
#include <memory>
#include <spdlog/async.h>
#include <spdlog/fmt/ostr.h>
#include <spdlog/sinks/rotating_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <vector>

namespace Shard3D {

	static std::shared_ptr<spdlog::logger> dllget_logger(
		std::vector<spdlog::sink_ptr> sinks,
		std::string logger_name
	) { // https://github.com/gabime/spdlog/issues/1562
		std::shared_ptr<spdlog::logger> logger = spdlog::get(logger_name);

		if (!logger) {
			if (sinks.size() > 0) {
				logger = std::make_shared<spdlog::logger>(
					logger_name,
					std::begin(sinks),
					std::end(sinks));
				spdlog::register_logger(logger);
			}
			else {
				/*
				auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
				console_sink->set_pattern("[%^%l%$] %v");
				console_sink->set_level(spdlog::level::info);

				auto file_sink = std::make_shared<spdlog::sinks::rotating_file_sink_mt>("var/log/librethfeld", 1048576 * 5, 10, true);
				file_sink->set_pattern("[%^%l%$] %v");
				file_sink->set_level(spdlog::level::trace);

				spdlog::sinks_init_list sink_list = {file_sink, console_sink };

				auto logger = std::make_shared<spdlog::async_logger>(logger_name, sink_list, spdlog::thread_pool(), spdlog::async_overflow_policy::block);
				logger->set_level(spdlog::level::trace);
				*/
				logger = spdlog::stdout_color_mt(logger_name);
			}
		}
		return logger;
	} 


	void LOGGER::init() {
		spdlog::set_pattern("%^[%T] [%l] [%n]: %v%$");

		debugLogger = spdlog::stdout_color_mt("DEBUG");
		debugLogger->set_level(spdlog::level::trace); 
		debugLoggerSinks = debugLogger->sinks();

		devMsgLogger = spdlog::stdout_color_mt("DEVMSG");
		devMsgLogger->set_level(spdlog::level::trace);
		devMsgLoggerSinks = devMsgLogger->sinks();
		
		engineLogger = spdlog::stdout_color_mt("ENGINE");// spdlog::basic_logger_st("ENGINE", "latest_log.txt");
		engineLogger->set_level(spdlog::level::trace);
		engineLoggerSinks = engineLogger->sinks();
		
		editorLogger = spdlog::stdout_color_mt("EDITOR");
		editorLogger->set_level(spdlog::level::trace);
		editorLoggerSinks = editorLogger->sinks();
	}

	void LOGGER::logFatal(const std::string& message) {
		engineLogger->critical(message.c_str());
		MessageDialogs::show(message.c_str(), "Shard3D CORE FATAL", MessageDialogs::OPTABORTRETRYIGNORE | MessageDialogs::OPTICONERROR);
		throw std::error_code();
	}
	std::shared_ptr<spdlog::logger>& LOGGER::getDebugLogger()
	{
		return dllget_logger(debugLoggerSinks, "DEBUG");
	}
	std::shared_ptr<spdlog::logger>& LOGGER::getDeveloperMsgLogger()
	{
		return dllget_logger(devMsgLoggerSinks, "DEVMSG");
	}
	std::shared_ptr<spdlog::logger>& LOGGER::getEngineLogger()
	{
		return dllget_logger(engineLoggerSinks, "ENGINE");
	}
	std::shared_ptr<spdlog::logger>& LOGGER::getEditorLogger()
	{
		return dllget_logger(editorLoggerSinks, "EDITOR");
	}
}