#include "../s3dpch.h"

#include <fstream>
#include <filesystem>
#include <regex>

namespace Shard3D {
	void IOUtils::writeStackBinary(const void* data, size_t object_size, const std::string& path) {
		std::fstream output{ path, std::ios::binary | std::ios::out };

		const uint8_t* raw_data = reinterpret_cast<const uint8_t*>(data);

		for (size_t i = 0; i < object_size; i++) {
			uint8_t destMemory{};
			memcpy(&destMemory, &raw_data[i], sizeof(uint8_t));
			output << destMemory;
		}
		output.flush();
		output.close();
	}
	std::vector<uint8_t> IOUtils::getStackBinary(void* data, size_t object_size) {
		std::vector<uint8_t> output;
		uint8_t* raw_data = reinterpret_cast<uint8_t*>(data);

		for (size_t i = 0; i < object_size; i++) {
			output.push_back(raw_data[i]);
		}

		return output;
	}
	std::vector<char> IOUtils::readBinary(const std::string& path) {
		std::ifstream file{ path, std::ios::ate | std::ios::binary };

		if (!file.is_open()) {
			SHARD3D_FATAL("failed to read file: " + path);
		}

		size_t fileSize = static_cast<size_t>(file.tellg());
		std::vector<char> buffer(fileSize);

		file.seekg(0);
		file.read(buffer.data(), fileSize);

		file.close();
		return buffer;
	}
	std::string IOUtils::convertWinPathToUnixPath(const std::string& path) {
		std::string copy = path;
		std::replace(copy.begin(), copy.end(), '\\', '/');
		return copy;
	}
	std::string IOUtils::readText(const std::string& path, bool binary) {
		std::string code;
		std::ifstream in(path, (binary? std::ios::binary : 0) | std::ios::in);
		if (in) {
			in.seekg(0, std::ios::end);
			size_t size = in.tellg();
			if (size != -1) {
				code.resize(size);
				in.seekg(0, std::ios::beg);
				in.read(&code[0], size);
			}
		}
		in.close();
		return code;
	}
	void IOUtils::clearDirectory(const std::string& path) {
		for (const auto& entry : std::filesystem::directory_iterator(path))
			std::filesystem::remove_all(entry.path());
	}
	void IOUtils::writeText(const std::string& text, const std::string& path) {
		std::ofstream fout(path);
		fout << text.c_str();
		fout.flush();
		fout.close();
	}
	void IOUtils::writeText(const char* text, const std::string& path) {
		std::ofstream fout(path);
		fout << text;
		fout.flush();
		fout.close();
	}

	std::string IOUtils::getFileOnly(const std::string& total) {
		std::string newString = convertWinPathToUnixPath(total);
		SHARD3D_ASSERT(newString.find('/') != 0);
		return newString.substr(newString.find_last_of("/") + 1);
	}


	bool IOUtils::doesFileExist(const std::string& assetPath) {
		std::ifstream ifile(assetPath);
		return ifile.good();
	}

	bool IOUtils::isSubPath(const std::string& sub, const std::string& main) {
		std::string relative = std::filesystem::relative(std::filesystem::path(main), std::filesystem::path(sub)).string();
		// Size check for a "." result.
		// If the path starts with "..", it's not a subdirectory.
		return relative.size() == 1 || relative[0] != '.' && relative[1] != '.';
	}
	void strUtils::replace(std::string& fullString, std::string const& replacee, std::string const& replacer) {
		while (true) {
			size_t start_pos = fullString.find(replacee);
			if (start_pos == std::string::npos) return;
			fullString.replace(start_pos, replacee.length(), replacer);
		}
	}
	std::string strUtils::removeIllegalPathChars(const std::string& string) {
		std::string result_text;
		for (int i = 0; i < string.length(); ++i)
		{
			if ((string[i] >= 'a' && string[i] <= 'z') ||
				(string[i] >= 'A' && string[i] <= 'Z') ||
				(string[i] == ' ') || (string[i] == '-') ||
				(string[i] == '_') ||
				(string[i] >= '0' && string[i] <= '9'))
				result_text.push_back(string[i]);
		}
		return result_text;
	}

	void WAITUtils::preciseStandby(float seconds) {

		// win32 api
		//__int64 timeEllapsed;
		//__int64 timeStart;
		//__int64 timeDelta;
		//
		//QueryPerformanceFrequency((LARGE_INTEGER*)(&timeDelta));
		//
		//__int64 timeToWait = static_cast<double>(timeDelta) * static_cast<double>(seconds);
		//
		//QueryPerformanceCounter((LARGE_INTEGER*)(&timeStart));
		//
		//timeEllapsed = timeStart;
		//
		//while ((timeEllapsed - timeStart) < timeToWait) {
		//	QueryPerformanceCounter((LARGE_INTEGER*)(&timeEllapsed));
		//};
		 
		auto spinStart = std::chrono::high_resolution_clock::now();
		while ((std::chrono::high_resolution_clock::now() - spinStart).count() / 1e9 < seconds);

	}
}