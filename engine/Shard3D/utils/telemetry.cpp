#include "telemetry.h"
namespace Shard3D {
#define T_NOW std::chrono::high_resolution_clock::now()

	Telemetry::Telemetry(S3DDevice& device) : engineDevice(device), queryCount(0) {
		// Set up draw time query creation info
		VkQueryPoolCreateInfo queryPoolCreateInfo{};
		queryPoolCreateInfo.sType = VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO;
		queryPoolCreateInfo.queryCount = 2048;
		queryPoolCreateInfo.queryType = VK_QUERY_TYPE_TIMESTAMP;

		// Create the draw time query pool
		VK_ASSERT(
			vkCreateQueryPool(engineDevice.device(), &queryPoolCreateInfo, nullptr, &queryPool),
			"Failed to create timestamp query pool"
		);
	}

	Telemetry::~Telemetry() {
		vkDestroyQueryPool(engineDevice.device(), queryPool, nullptr);
	}

	TelemetryCPUMetric::TelemetryCPUMetric(Telemetry* telemetry, const std::string& category, const std::string& stat)
		: TelemetryMetric(telemetry, category, stat) {
	
	}
	TelemetryCPUMetric::~TelemetryCPUMetric() {

	}
	void TelemetryCPUMetric::record() {
		timeRecordBegin = T_NOW;
	}
	void TelemetryCPUMetric::end() {
		time_elapsed = std::chrono::duration<double, std::chrono::milliseconds::period>(T_NOW - timeRecordBegin).count();
	}

	TelemetryGPUMetric::TelemetryGPUMetric(Telemetry* telemetry, const std::string& category, const std::string& stat) 
		: TelemetryMetric(telemetry, category, stat), index(telemetry_->queryCount) {
		telemetry_->queryCount += 2;
	}

	TelemetryGPUMetric::~TelemetryGPUMetric() {
		telemetry_->queryCount -= 2;
	}

	void TelemetryGPUMetric::record(VkCommandBuffer commandBuffer, VkPipelineStageFlagBits pipelineStage) {
		vkCmdResetQueryPool(commandBuffer, telemetry_->queryPool, index, 2);
		vkCmdWriteTimestamp(commandBuffer, pipelineStage, telemetry_->queryPool, index);
	}
	void TelemetryGPUMetric::end(VkCommandBuffer commandBuffer, VkPipelineStageFlagBits pipelineStage) {
		vkCmdWriteTimestamp(commandBuffer, pipelineStage, telemetry_->queryPool, index + 1);

		// Get results from query
		VkResult result = vkGetQueryPoolResults(telemetry_->engineDevice.device(), telemetry_->queryPool, index, 2, sizeof(buffer), buffer, sizeof(uint64_t), VK_QUERY_RESULT_64_BIT);

		// Check for errors
		//if (result == VK_NOT_READY) {
		//	time_elapsed = 0.0;
		//} else if (result != VK_SUCCESS) {
		//	time_elapsed = static_cast<double>(float(INFINITY));
		//}
		if (result != VK_SUCCESS) return;

		time_elapsed = (buffer[1] - buffer[0]) * telemetry_->engineDevice.properties.limits.timestampPeriod * 1e-6;
	}
	void Telemetry::registerTelemetryMetric(TelemetryMetric* metric) {
		telemetryData.push_back(metric);
	}
}