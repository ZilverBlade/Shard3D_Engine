#pragma once
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/rotating_file_sink.h>

//#define SHARD3D_ALLOW_LOG_PER_FRAME
//#define SHARD3D_ENABLE_VERBOSE

namespace Shard3D {
	class LOGGER {
	public:
		static void init();
		static void logFatal(const std::string& message);

		static std::shared_ptr<spdlog::logger>& getDebugLogger();
		static std::shared_ptr<spdlog::logger>& getDeveloperMsgLogger();
		static std::shared_ptr<spdlog::logger>& getEngineLogger();
		static std::shared_ptr<spdlog::logger>& getEditorLogger();
	private:
		static inline std::shared_ptr<spdlog::logger> debugLogger;
		static inline std::shared_ptr<spdlog::logger> devMsgLogger;
		static inline std::shared_ptr<spdlog::logger> engineLogger;
		static inline std::shared_ptr<spdlog::logger> editorLogger;
		static inline std::vector<spdlog::sink_ptr> debugLoggerSinks;
		static inline std::vector<spdlog::sink_ptr> devMsgLoggerSinks;
		static inline std::vector<spdlog::sink_ptr> engineLoggerSinks;
		static inline std::vector<spdlog::sink_ptr> editorLoggerSinks;
	};
}
/* Throw a message notifying that a function has not been implemented.
*/
#define SHARD3D_NOIMPL		Shard3D::LOGGER::getDeveloperMsgLogger()->error("function not implemented!")
#define SHARD3D_NOIMPL_C		{Shard3D::LOGGER::getDeveloperMsgLogger()->error("function not implemented! program may not continues"); throw std::exception("Not implemented!"); }
#define SHARD3D_LOG_D(...)		Shard3D::LOGGER::getDeveloperMsgLogger()->trace(__VA_ARGS__)
#ifndef NDEBUG
/* Log for debugging, doesnt log in release/deploy mode, so use conservatively.
*/
#define SHARD3D_LOG(...)	Shard3D::LOGGER::getDebugLogger()->trace(__VA_ARGS__)
#elif NDEBUG
#define SHARD3D_LOG(...)
#endif
#ifdef SHARD3D_ENABLE_VERBOSE
/* [ENGINE] Log verbose information
*/
#define SHARD3D_VERBOSE(...)	Shard3D::LOGGER::getDebugLogger()->debug(__VA_ARGS__)
#else
#define SHARD3D_VERBOSE(...)
#endif
#ifdef SHARD3D_ALLOW_LOG_PER_FRAME
/* [ENGINE] Log per-frame verbose information (warning, will cause serious slowdowns and is only meant for thorough debugging)
*/
#define SHARD3D_VERBOSE_F(...)	Shard3D::LOGGER::getDebugLogger()->debug(__VA_ARGS__)
#else
#define SHARD3D_VERBOSE_F(...)
#endif
/* [ENGINE] Log information, for notifying what may happen, like asset loading.
*/
#define SHARD3D_INFO(...)	Shard3D::LOGGER::getEngineLogger()->trace(__VA_ARGS__)
/* [ENGINE] Log a warning, such as for when something unexpected happens, and might cause undefined behaviour, but might function normally.
*/
#define SHARD3D_WARN(...)	Shard3D::LOGGER::getEngineLogger()->warn(__VA_ARGS__)
/* [ENGINE] Log an error, such as for when something goes wrong, and has a chance of causing the engine to be in an undefined state.
*/
#define SHARD3D_ERROR(...)	Shard3D::LOGGER::getEngineLogger()->error(__VA_ARGS__)


/* [EDITOR] Log information, for notifying what may happen, like asset loading.
*/
#define SHARD3D_INFO_E(...)	Shard3D::LOGGER::getEditorLogger()->trace(__VA_ARGS__)
/* [EDITOR] Log a warning, such as for when something unexpected happens, and might cause undefined behaviour, but might function normally.
*/
#define SHARD3D_WARN_E(...)	Shard3D::LOGGER::getEditorLogger()->warn(__VA_ARGS__)
/* [EDITOR] Log an error, such as for when something goes wrong, and has a chance of causing undefined behaviour.
*/
#define SHARD3D_ERROR_E(...)Shard3D::LOGGER::getEditorLogger()->error(__VA_ARGS__)

/* Log a fatal error, also shows a message box as well as an exception, ending the program
(only use for a critical that will cause undefined behaviour).
Unlike the other logging macros, this only takes a string value.
*/
#define SHARD3D_FATAL(...)	Shard3D::LOGGER::logFatal(__VA_ARGS__)

/*
* Assert a statement, also shows a message box as well as an exception, ending the program
(use for catching potential mistakes in debugging).
Unlike the other logging macros, this only takes a string value.
*/
#ifndef NDEBUG
#define SHARD3D_ASSERT(EXPERESSION) assert(EXPERESSION)
#define SHARD3D_SILENT_ASSERT(EXPERESSION) if (!(EXPERESSION)) Shard3D::LOGGER::getEngineLogger()->error("Assertion failed! '"#EXPERESSION"'")
#endif
// permanent version of assert
#define SHARD3D_ENSURE(EXPERESSION, MESSAGE) if (!(EXPERESSION)) Shard3D::LOGGER::logFatal(MESSAGE)
#ifdef NDEBUG
#define SHARD3D_ASSERT(EXPERESSION)
#define SHARD3D_SILENT_ASSERT(EXPERESSION)
#endif