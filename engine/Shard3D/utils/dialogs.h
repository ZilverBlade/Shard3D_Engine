#pragma once
#include <string>

#ifdef _WIN32	
#include <Windows.h>
#include <commdlg.h>
#include <shlobj_core.h>
#endif

namespace Shard3D {
	class MessageDialogs {
	public:
		enum DialogResult{
			RESERROR = -1,
//#ifdef _WIN32	
			RESOK			=	1,
			RESCANCEL		=	2,
			RESABORT		=	3,
			RESRETRY		=	4,
			RESIGNORE		=	5,
			RESYES			=	6,
			RESNO			=	7,
			 
			RESCLOSE		=	8,
			RESHELP			=	9,
					
			RESTRYAGAIN		=	10,
			RESCONTINUE		=	11,
			
			RESTIMEOUT		=	32000
//#endif // since other OS use different values
			
		};
		enum DialogOptions {
//#ifdef _WIN32
			OPTOK                      = 0x00000000L,
			OPTOKCANCEL                = 0x00000001L, 
			OPTABORTRETRYIGNORE        = 0x00000002L, 
			OPTYESNOCANCEL             = 0x00000003L, 
			OPTYESNO                   = 0x00000004L, 
			OPTRETRYCANCEL             = 0x00000005L, 
				   				 
			OPTCANCELTRYCONTINUE       = 0x00000006L, 
								   				 
			OPTICONERROR               = 0x00000010L, 
			OPTICONQUESTION            = 0x00000020L, 
			OPTICONEXCLAMATION         = 0x00000030L, 
			OPTICONINFO				   = 0x00000040L, 
								   				 
			OPTUSERICON                = 0x00000080L, 
								   				 
			OPTDEFBUTTON1              = 0x00000000L, 
			OPTDEFBUTTON2              = 0x00000100L, 
			OPTDEFBUTTON3              = 0x00000200L, 
								   				 
			OPTDEFBUTTON4              = 0x00000300L, 
								   				 								   				 
			OPTAPPLMODAL               = 0x00000000L, 
			OPTSYSTEMMODAL             = 0x00001000L, 
			OPTTASKMODAL               = 0x00002000L, 
								   				 
			OPTHELP                    = 0x00004000L, // Help Button
								   								   
			OPTNOFOCUS                 = 0x00008000L,
			OPTSETFOREGROUND           = 0x00010000L,
			OPTDEFAULT_DESKTOP_ONLY    = 0x00020000L,
								   							   
			OPTTOPMOST                 = 0x00040000L,
			OPTRIGHT                   = 0x00080000L,
			OPTRTLREADING              = 0x00100000L
//#endif // since other OS use different values
		};
		// wrapper exists to keep code clean when potentially dealing with other OS'es (future proofing)

		static DialogResult show(const char* text, const char* caption = "", uint32_t options = OPTOKCANCEL | OPTDEFBUTTON1) {
#ifdef _WIN32
			return (DialogResult)(uint32_t)(MessageBoxA(
				nullptr,
				text, caption, options
			));
#endif
#ifdef __linux__ 
			SHARD3D_NOIMPL;
#endif
			return RESERROR;
		}	
	};
	class BrowserDialogs {
	public:
		// no string returned if cancelled
		static std::string openFile(const char* filter = "All files (*.*)\0*.*\0") {
#ifdef _WIN32
			OPENFILENAMEA ofn;
			CHAR szFile[256] = { 0 };
			ZeroMemory(&ofn, sizeof(OPENFILENAME));
			ofn.lStructSize = sizeof(OPENFILENAME);
			ofn.hwndOwner = nullptr;
			ofn.lpstrFile = szFile;
			ofn.nMaxFile = sizeof(szFile);
			ofn.lpstrFilter = filter;
			ofn.nFilterIndex = 1;
			ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR;
			if (GetOpenFileNameA(&ofn) == TRUE) {
				return ofn.lpstrFile;
			}
#endif
#ifdef __linux__ 
			SHARD3D_WARN("unsupported function");
#endif
			return std::string();

		}
		// no string returned if cancelled
		static std::string openFolder(const char* title, const char* rootFolder = nullptr) {
#ifdef _WIN32
#if 1
			CHAR szFile[256] = { 0 };
			BROWSEINFOA bi = { 0 };

			bi.lpszTitle = title;
			wchar_t* rf;
			if (rootFolder) {
				rf = reinterpret_cast<wchar_t*>(calloc(strlen(rootFolder), sizeof(wchar_t)));
				mbstowcs(rf, rootFolder, strlen(rootFolder) - 1);
				bi.pidlRoot = SHSimpleIDListFromPath(rf);
			}
			LPITEMIDLIST pidl = SHBrowseForFolderA(&bi);
			if (rootFolder) free(rf);
			if (pidl != NULL) {
				CHAR tszPath[MAX_PATH] = "\0";

				if (SHGetPathFromIDListA(pidl, tszPath) == TRUE)
				{
					// � Free pidl
					CoTaskMemFree(pidl);
					return tszPath;
				}

				// � Free pidl
				CoTaskMemFree(pidl);
			}
#else
			// CoCreate the File Open Dialog object.
			IFileDialog* pfd = NULL;
			HRESULT hr = CoCreateInstance(CLSID_FileOpenDialog,
				NULL,
				CLSCTX_INPROC_SERVER,
				IID_PPV_ARGS(&pfd));
			if (SUCCEEDED(hr))
			{
				// Set the options on the dialog.
				DWORD dwFlags;

				// Before setting, always get the options first in order 
				// not to override existing options.
				hr = pfd->GetOptions(&dwFlags);
				if (SUCCEEDED(hr))
				{
					// In this case, get shell items only for file system items.
					hr = pfd->SetOptions(dwFlags | FOS_PICKFOLDERS);
					if (SUCCEEDED(hr)) {
						// Show the dialog
						hr = pfd->Show(NULL);
						if (SUCCEEDED(hr))
						{
							// Obtain the result once the user clicks 
							// the 'Open' button.
							// The result is an IShellItem object.
							IShellItem* psiResult;
							hr = pfd->GetResult(&psiResult);
							if (SUCCEEDED(hr))
							{
								// We are just going to print out the 
								// name of the file for sample sake.
								PWSTR pszFilePath = NULL;
								hr = psiResult->GetDisplayName(SIGDN_FILESYSPATH,
									&pszFilePath);
								if (SUCCEEDED(hr))
								{
									TaskDialog(NULL,
										NULL,
										L"CommonFileDialogApp",
										pszFilePath,
										NULL,
										TDCBF_OK_BUTTON,
										TD_INFORMATION_ICON,
										NULL);
									CoTaskMemFree(pszFilePath);
								}
								psiResult->Release();
							}

						}
					}
				}
				pfd->Release();
			}
#endif
#endif
#ifdef __linux__ 
			SHARD3D_WARN("unsupported function");
#endif
			return std::string();
		}
		static std::string saveFile(const char* filter = "All files (*.*)\0*.*\0") {
#ifdef _WIN32
			OPENFILENAMEA ofn;
			CHAR szFile[256] = { 0 };
			ZeroMemory(&ofn, sizeof(OPENFILENAME));
			ofn.lStructSize = sizeof(OPENFILENAME);
			ofn.hwndOwner = nullptr;
			ofn.lpstrFile = szFile;
			ofn.nMaxFile = sizeof(szFile);
			ofn.lpstrFilter = filter;
			ofn.nFilterIndex = 1;
			ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR;
			if (GetSaveFileNameA(&ofn) == TRUE) {
				return ofn.lpstrFile;
			}
#endif
#ifdef __linux__ 
			SHARD3D_WARN("unsupported function");
#endif
			return std::string();
		}
	};

	class ProgressDialog : public IProgressDialog {
	public:
		HRESULT StartProgressDialog(_In_opt_ HWND hwndParent, _In_opt_ IUnknown* punkEnableModless, DWORD dwFlags, _Reserved_ LPCVOID pvResevered) override {

		}
		HRESULT StopProgressDialog() override {

		}
		HRESULT SetTitle(PCWSTR pwzTitle) override {

		}

		HRESULT SetAnimation(_In_opt_ HINSTANCE hInstAnimation, UINT idAnimation) override {

		}

		BOOL HasUserCancelled()  override {

		}
		HRESULT SetProgress(DWORD dwCompleted, DWORD dwTotal) override {

		}
		HRESULT SetProgress64(ULONGLONG ullCompleted, ULONGLONG ullTotal) override {

		}
		HRESULT SetLine(DWORD dwLineNum, _In_ PCWSTR pwzString, BOOL fCompactPath, _Reserved_ LPCVOID pvResevered)  override {

		}
		HRESULT SetCancelMsg(_In_ PCWSTR pwzCancelMsg, _Reserved_ LPCVOID pvResevered)  override {

		}
		HRESULT Timer(DWORD dwTimerAction, _Reserved_ LPCVOID pvResevered) override {

		}

	};
}