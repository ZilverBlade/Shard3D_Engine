﻿using Shard3D.Library.Types;

namespace CoreDefault
{
    public class AutoCamPossess : Shard3D.Actor
    {
        override protected void BeginEvent()
        {
            if (this.HasComponent<Shard3D.Components.CameraComponent>())
            {
                this.GetComponent<Shard3D.Components.CameraComponent>().Possess();
            }
        }
        protected override void TickEvent(float dt) { }
        protected override void EndEvent() { }
    }
}