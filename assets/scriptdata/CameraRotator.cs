﻿using Shard3D;
using Shard3D.Library.Types;
using System;
namespace MyTestProject {
    public class CameraRotator : Actor
    {
        private bool hasCam;
        private Shard3D.Components.TransformComponent transform;
        private Shard3D.Library.Animation.Motion.TweenFloat3 cameraMovementA;
        private Shard3D.Library.Animation.Motion.TweenFloat3 cameraMovementB;
        private Shard3D.Library.Animation.Motion.TweenFloat3 cameraMovementC;
        private Shard3D.Library.Animation.Motion.TweenFloat3 cameraMovementD;
        private Shard3D.Library.Animation.Motion.TweenFloat3 cameraMovementE;

        private int currentMovementIndex = 0;
        protected override void BeginEvent()
        {
            hasCam = this.HasComponent<Shard3D.Components.CameraComponent>();
            if (hasCam)
            {
               this.GetComponent<Shard3D.Components.CameraComponent>().Possess();
            }
            transform = this.GetComponent<Shard3D.Components.TransformComponent>();
            cameraMovementA = new Shard3D.Library.Animation.Motion.TweenFloat3.Ease(
                1.0f, transform.Rotation, new Float3(1.5f, 1.0f, 0.0f));
            cameraMovementB = new Shard3D.Library.Animation.Motion.TweenFloat3.Ease(
               2.0f, new Float3(1.5f, 1.0f, 0.0f), new Float3(0.8f, -0.8f, 2.0f));
            cameraMovementC = new Shard3D.Library.Animation.Motion.TweenFloat3.Ease(
               2.0f, new Float3(0.8f, -0.8f, 2.0f), new Float3(0.5f, 0.0f, 0.0f));
            cameraMovementD = new Shard3D.Library.Animation.Motion.TweenFloat3.Ease(
               2.0f, new Float3(0.5f, 0.0f, 0.0f), new Float3(1.5f, 1.0f, 0.0f));
            cameraMovementE = new Shard3D.Library.Animation.Motion.TweenFloat3.Ease(
               25.0f, new Float3(1.5f, 1.0f, 0.0f), new Float3(1.5f, 0.0f, 50.0f));
        }
        protected override void TickEvent(float dt)
        {
            Float3 rotation = new Float3();
            Float3 translation = transform.Translation;
            translation.z += 1.0f * dt;
            if (currentMovementIndex == 0)
            {
                cameraMovementA.Update(dt);
                rotation = cameraMovementA.GetValue();
                if (cameraMovementA.GetAlpha() == 1.0f) currentMovementIndex = 1;
            }
            if (currentMovementIndex == 1)
            {
                cameraMovementB.Update(dt);
                rotation = cameraMovementB.GetValue();
                if (cameraMovementB.GetAlpha() == 1.0f) currentMovementIndex = 2;
            }
            if (currentMovementIndex == 2)
            {
                cameraMovementC.Update(dt);
                rotation = cameraMovementC.GetValue();
                if (cameraMovementC.GetAlpha() == 1.0f) currentMovementIndex = 3;
            }
            if (currentMovementIndex == 3)
            {
                cameraMovementD.Update(dt);
                rotation = cameraMovementD.GetValue();
                if (cameraMovementD.GetAlpha() == 1.0f) currentMovementIndex = 4;
            }
            if (currentMovementIndex == 4)
            {
                cameraMovementE.Update(dt);
                rotation = cameraMovementE.GetValue();
            }
            transform.Rotation = rotation;
            transform.Translation = translation;
        }
        protected override void EndEvent()
        {
            
        }
    }
}