﻿using System;
using Shard3D.Library.Types;
using Shard3D;
using Shard3D.Library;
using Shard3D.Components;
using Shard3D.UI;

namespace MyTestProject
{
    public class MarbleSpawner : Actor
    {
        private Shard3D.Components.TransformComponent transform;
        private bool buttonPressed = false;

        public float linearSpeed = 3.0f;
        public float angularSpeed = 1.5f;

        private HUDElement.Text VelocityTextElement;
        private HUDElement.Rect VelocityNeedleElement;
        private HUD MainHUD;

        readonly private string[] marbleAssets = {
           "content/marbles/marble_actor.s3dprefab"
        };

        override protected void BeginEvent() {
            transform = this.GetComponent<Shard3D.Components.TransformComponent>();
            this.GetComponent<Shard3D.Components.CameraComponent>().Possess();
            MainHUD = new HUD("content/hud/camera_hud_test.s3dasset");
            MainHUD.Display();
            VelocityTextElement = MainHUD.GetElement<HUDElement.Text>("Text VPfvJcrgRYwT");
            VelocityNeedleElement = MainHUD.GetElement<HUDElement.Rect>("Rect iizs2trQF69A");
            
            VelocityTextElement.SetTextFormattingParameter("velocity", 0.ToString());
        }

        private string GetRandomMarble() {
            Random rand = new Random();
            int rnum = rand.Next(0, marbleAssets.Length);
            return marbleAssets[rnum];
        }

        protected override void TickEvent(float dt)
        {
            Float3 translation = transform.Translation;
            Float3 rotation = transform.Rotation;

            if (Input.IsKeyDown(KeyInput.KeyI))
                rotation.x -= angularSpeed * dt;
            if (Input.IsKeyDown(KeyInput.KeyK))
                rotation.x += angularSpeed * dt;
            if (Input.IsKeyDown(KeyInput.KeyJ))
                rotation.z -= angularSpeed * dt;
            if (Input.IsKeyDown(KeyInput.KeyL))
                rotation.z += angularSpeed * dt;

            Float3 forwardDir = new Float3((float)Math.Sin(rotation.z), (float)Math.Cos(rotation.z), -(float)Math.Sin(rotation.x));
            Float3 rightDir = new Float3(forwardDir.y, -forwardDir.x, 0.0f);
            Float3 upDir = new Float3(0.0f, 0.0f, 1.0f);

            Float3 moveDir = new Float3(0.0f) ;

            if (Input.IsKeyDown(KeyInput.KeyW))
                moveDir += forwardDir;
            if (Input.IsKeyDown(KeyInput.KeyS))
                moveDir -= forwardDir;
            if (Input.IsKeyDown(KeyInput.KeyA))
                moveDir -= rightDir;
            if (Input.IsKeyDown(KeyInput.KeyD))
                moveDir += rightDir;
            if (Input.IsKeyDown(KeyInput.KeySpace))
                moveDir += upDir;
            if (Input.IsKeyDown(KeyInput.KeyRightControl))
                moveDir -= upDir;

            if (Input.IsKeyDown(KeyInput.KeyKP_0)) {
                Level.LoadLevel("content/translucent_shadow_test.s3dlevel");
                return;
            }

            if (SMath.Dot(moveDir, moveDir) > 0.00001f) {
                linearSpeed += dt;
                linearSpeed = SMath.Clamp(linearSpeed, 0.0f, 10.0f);
                moveDir /= moveDir.Length();
                transform.Translation = translation + moveDir * dt * linearSpeed;
            } else {
                linearSpeed = 0.0f;
            }
            VelocityTextElement.SetTextFormattingParameter("velocity", ((int)(linearSpeed * 3.6f)).ToString());
            VelocityNeedleElement.Rotation = -linearSpeed / 3.1415f;

            transform.Rotation = rotation;

            if (Input.IsKeyDown(KeyInput.KeyF))
            {
                if (buttonPressed == false)
                {
                    Actor marbleActor = Level.SpawnPrefabActor(GetRandomMarble(), "Marble");
                    //marbleActor.GetComponent<Rigidbody3DComponent>().AddForce(forwardDir * 50.0f);
                    marbleActor.GetComponent<TransformComponent>().Translation = translation;
                    marbleActor.GetComponent<TransformComponent>().Scale = new Float3(0.2f, 0.2f, 0.2f);
                    buttonPressed = true;
                }
            }
            else
            {
                buttonPressed = false;
            }
        }
    }
}
