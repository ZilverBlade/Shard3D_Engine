﻿using Shard3D;
using Shard3D.Components;
using Shard3D.Library;
using Shard3D.Library.Animation.Motion;
using Shard3D.Library.Types;
using System;
using System.Collections.Generic;

namespace MyTestProject {
    class CarActorPhysics : Actor {
		const float TWO_PI = (float)(2.0F * Math.PI);

		const ulong ConstraintFL = 4132154254947563438;
        const ulong ConstraintFR = 11049656913291076209;

		const ulong ConstraintRL = 9684532219153937503;
		const ulong ConstraintRR = 14484216280691560816;

		const ulong Chassis = 2986922447173263237;

		const ulong Camera = 98521095821905289;

		// ACTORS
		Actor constraintActorFL;
		Actor constraintActorFR;
		Actor constraintActorRL;
		Actor constraintActorRR;

		Actor ChassisActor;

		Actor CameraActor;


		Actor triggerVolumeTestActor;

		// BRAKES
		float brakeForceFront = 3000.0f; // N
		float brakeForceRear = 2000.0f; // N

		// ENGINE
		float maxRPM = 7800.0F;
		float rpm = 800.0f;
		float inertia = 24.0f;
		float engineBreakForce = 1000.0F;
		Dictionary<uint, float> torqueCurve = new Dictionary<uint, float> {
			{ 0, 0.0F },
			{ 500, 115.73F },
			{ 1000, 151.78F },
			{ 1100, 154.58F },
			{ 1200, 157.55F },
			{ 1300, 160.33F },
			{ 1400, 162.89F },
			{ 1500, 164.89F },
			{ 1600, 166.51F },
			{ 1700, 167.79F },
			{ 1800, 168.75F },
			{ 1900, 169.30F },
			{ 2000, 169.33F },
			{ 2100, 170.10F },
			{ 2200, 170.74F },
			{ 2300, 171.06F },
			{ 2400, 171.16F },
			{ 2500, 170.88F },
			{ 2600, 169.98F },
			{ 2700, 169.73F },
			{ 2800, 169.68F },
			{ 2900, 169.01F },
			{ 3000, 168.44F },
			{ 3100, 167.56F },
			{ 3200, 166.48F },
			{ 3300, 165.72F },
			{ 3400, 166.63F },
			{ 3500, 167.26F },
			{ 3600, 167.88F },
			{ 3700, 168.24F },
			{ 3800, 168.47F },
			{ 3900, 168.60F },
			{ 4000, 168.53F },
			{ 4100, 168.84F },
			{ 4200, 168.61F },
			{ 4300, 168.29F },
			{ 4400, 167.50F },
			{ 4500, 167.08F },
			{ 4600, 166.38F },
			{ 4700, 165.86F },
			{ 4800, 165.31F },
			{ 4900, 164.78F },
			{ 5000, 165.16F },
			{ 5100, 166.45F },
			{ 5200, 167.38F },
			{ 5300, 168.46F },
			{ 5400, 169.85F },
			{ 5500, 171.36F },
			{ 5600, 172.44F },
			{ 5700, 173.49F },
			{ 5800, 173.93F },
			{ 5900, 174.71F },
			{ 6000, 175.34F },
			{ 6100, 176.54F },
			{ 6200, 177.81F },
			{ 6300, 178.74F },
			{ 6400, 179.61F },
			{ 6500, 180.00F },
			{ 6600, 180.34F },
			{ 6700, 180.94F },
			{ 6800, 180.54F },
			{ 6900, 179.58F },
			{ 7000, 177.71F },
			{ 7100, 175.34F },
			{ 7200, 172.67F },
			{ 7300, 169.68F },
			{ 7400, 166.14F },
			{ 7500, 161.96F },
			{ 7600, 157.15F },
			{ 7700, 151.84F },
			{ 7800, 145.80F },
			{ 7900, 139.13F },
			{ 8000, 131.82F }
		};

		// PEDALS
		float throttle = 0.0f;
		float accelSpeed = 0.3f;
		float brake = 0.0f;

		// STEERING
		float turnAngle = 0.0f;
		const float MAX_TURN_ANGLE = 0.8F; // radians
		float turnSpeed = 2.0f; // rad/s
		TweenFloat normalizeTurn = null;

		// GEARBOX
		bool shifting = false; // avoid throttle input when shifting
		float shiftingTime = 0.2f;
		int gear = 0;
		float[] gr = { -3.40f, 0.00f, 2.70f, 1.80f, 1.42f, 1.11f, 0.98f };
		float fd = 4.2f;
		float shiftingTimeElapsed = 0.0f;

		private float GetGearRatio(int gearIndex) {
			return gr[gearIndex + 1];
        }

		private void ShiftGear(int increment) {
			if (gear == -1 || gear == (gr.Length - 1)) return;

			shifting = true;
			shiftingTimeElapsed = 0.0F;
			gear += increment;
		}

		private float GetTorqueAtRPM(float RPM) {
			const float MIN_RPM_SAMPLE = 500.0F;
			const float MAX_RPM_SAMPLE = 8000.0F;
			const float IDLE_RPM = 1000.0F;
			const float RPM_SAMPLE_STEP = 100.0F;

			if (RPM <= IDLE_RPM) {
				float alpha = (RPM - MIN_RPM_SAMPLE) / (IDLE_RPM - MIN_RPM_SAMPLE);

				float tLow = torqueCurve[(uint)MIN_RPM_SAMPLE];
				float tIdle = torqueCurve[(uint)IDLE_RPM];

				return SMath.Lerp(tLow, tIdle, alpha);
			} else if (RPM <= MAX_RPM_SAMPLE) {
				float floor = (float)Math.Floor(RPM / RPM_SAMPLE_STEP) * RPM_SAMPLE_STEP;
				float ceil = (float)Math.Ceiling(RPM / RPM_SAMPLE_STEP) * RPM_SAMPLE_STEP;

				float alpha = (RPM - floor) / (ceil - floor);

				float tFloor = torqueCurve[(uint)floor];
				float tCeil = torqueCurve[(uint)ceil];

				return SMath.Lerp(tFloor, tCeil, alpha);
			} else {
				return torqueCurve[(uint)MAX_RPM_SAMPLE];
			}
        }

		protected override void BeginEvent() {
			triggerVolumeTestActor = Level.GetActor("Trigger Volume");
			constraintActorFL = this.GetActor(ConstraintFL);
			constraintActorFR = this.GetActor(ConstraintFR);
			constraintActorRL = this.GetActor(ConstraintRL);
			constraintActorRR = this.GetActor(ConstraintRR);
			ChassisActor = this.GetActor(Chassis);

			CameraActor = this.GetActor(Camera);
			CameraActor.GetComponent<CameraComponent>().Possess();

		}
		protected override void TickEvent(float dt) {
			if (ChassisActor.GetComponent<Rigidbody3DComponent>().CollidesWith(triggerVolumeTestActor)) {
				Actor marbleActor = Level.SpawnPrefabActor("content/marbles/marble_actor.s3dprefab", "Marble");
				marbleActor.GetComponent<TransformComponent>().Translation += new Float3(0.0f, 5.0f, 1.0f);
				Console.WriteLine("aaaaa");
			}

			if (Input.IsKeyDown(KeyInput.KeyD)) {
				turnAngle -= turnSpeed * dt;
				normalizeTurn = null;
			} else if (Input.IsKeyDown(KeyInput.KeyA)) {
				turnAngle += turnSpeed * dt;
				normalizeTurn = null;
			} else {
				float absAngle = Math.Abs(turnAngle);
				if (normalizeTurn == null) {
					normalizeTurn = new TweenFloat.Linear(absAngle / turnSpeed, turnAngle, 0.0f);
				}
				normalizeTurn.Update(dt);
				turnAngle = normalizeTurn.GetValue();
			}

			if (!shifting) {
				if (Input.IsKeyDown(KeyInput.KeyEqual)) {
					ShiftGear(1);
				} 
				if(Input.IsKeyDown(KeyInput.KeyMinus)) {
					ShiftGear(-1);
				}
            } else {
				shiftingTimeElapsed += dt;
				shifting = shiftingTimeElapsed < shiftingTime;
			}
		}
		
		protected override void PhysicsTickEvent(float dt) {
			float rotationalVelocityRR = Math.Abs(constraintActorRR.GetComponent<SuspensionComponent>().GetAxleVelocity());
			float rotationalVelocityRL = Math.Abs(constraintActorRL.GetComponent<SuspensionComponent>().GetAxleVelocity());
			float maxWheelVelocity = SMath.Max(rotationalVelocityRR, rotationalVelocityRL); // treat as open differential, it's the easiest for now

			float ratio = GetGearRatio(gear) * fd;
			if (gear != 0) {
				rpm = maxWheelVelocity * ratio * 60.0F / TWO_PI;
			}
			if (Input.IsKeyDown(KeyInput.KeyW) && !shifting) {
				float oldTorque = GetTorqueAtRPM(rpm) * throttle;
				rpm += inertia * oldTorque * dt;
				throttle += (1.0f / accelSpeed) * dt;
			} else {
				rpm -= dt * engineBreakForce / inertia;
				throttle -= (1.0f / accelSpeed) * dt;
			}
			if (rpm > maxRPM)  {
				throttle = 0.0F;
				rpm -= 100.0f;
			}
			rpm = Math.Max(rpm, 800);
			throttle = SMath.Clamp(throttle, 0.0F, 1.0F);
			turnAngle = SMath.Clamp(turnAngle, -MAX_TURN_ANGLE, MAX_TURN_ANGLE);

			constraintActorFR.GetComponent<SuspensionComponent>().hinge = -turnAngle;
			constraintActorFL.GetComponent<SuspensionComponent>().hinge = -turnAngle;

			float torqueNM = GetTorqueAtRPM(rpm) * throttle; // good enough approximation for torque curve variation with throttle
			float wheelTorque = torqueNM * ratio;
			float wheelVelocity = TWO_PI * rpm / (60.0F * ratio);
			//Console.WriteLine($"RPM: {rpm}\nTORQUE:{wheelTorque}\n WHEEL:{wheelVelocity}");

			constraintActorRR.GetComponent<SuspensionComponent>().SetMotorVelocity(-wheelVelocity, wheelTorque);
			constraintActorRL.GetComponent<SuspensionComponent>().SetMotorVelocity(-wheelVelocity, wheelTorque);
																											
			if (Input.IsKeyDown(KeyInput.KeyS)) {
				constraintActorFR.GetComponent<SuspensionComponent>().SetMotorVelocity(0.0f, -brakeForceFront);
				constraintActorFL.GetComponent<SuspensionComponent>().SetMotorVelocity(0.0f, -brakeForceFront);
				//constraintActorRR.GetComponent<SuspensionComponent>().SetMotorVelocity(0.0f, -SMath.Max(brakeForceRear - engineTorque * throttle, 0.0f));
				//constraintActorRL.GetComponent<SuspensionComponent>().SetMotorVelocity(0.0f, -SMath.Max(brakeForceRear - engineTorque * throttle, 0.0f));
			} else {
				constraintActorFR.GetComponent<SuspensionComponent>().SetMotorVelocity(0.0f, 0.0f);
				constraintActorFL.GetComponent<SuspensionComponent>().SetMotorVelocity(0.0f, 0.0f);
			}

		}

		protected override void EndEvent() {

		}
	}
}
