﻿using System;
using Shard3D.Library.Types;
using Shard3D;

namespace MyTestProject
{
    public class Strober : Actor
    {
       // private Shard3D.Components.PointLightComponent light;
        private Shard3D.Components.TransformComponent transform;
        private Shard3D.Library.Animation.Motion.TweenFloat moveUpDown;
        private bool reversing = false;
        private float strobeElapse = 0.0f;
        private bool hasbeensetup = false;
        private Shard3D.Components.TransformComponent cubeTransform;
        private void Setup()
        {
           // light = this.GetComponent<Shard3D.Components.PointLightComponent>();
            transform = this.GetComponent<Shard3D.Components.TransformComponent>();
            moveUpDown = new Shard3D.Library.Animation.Motion.TweenFloat.Ease(2.0f, transform.Translation.z - 1.0f, transform.Translation.z + 1.0f);
            //cubeTransform = this.GetActor("CubeObj").GetComponent<Shard3D.Components.TransformComponent>();
            hasbeensetup = true;
        }
        override protected void BeginEvent()
        {
        }
        override protected void SpawnEvent()
        {
        }
        protected override void TickEvent(float dt)
        {
            if (!hasbeensetup) Setup();
           // Float3 color = light.Color;
           // strobeElapse += dt * 2.0f;
           // color.x = Math.Abs((float)Math.Sin(strobeElapse + 0.5f));
           // color.y = Math.Abs((float)Math.Sin(strobeElapse + 1.0f));
           // color.z = Math.Abs((float)Math.Sin(strobeElapse + 1.5f));
           // light.Color = color;

            Float3 transl = transform.Translation;
            if (!reversing) moveUpDown.Update(dt);
            if (reversing) moveUpDown.UpdateReverse(dt);
            if (moveUpDown.GetAlpha() == 1.0f)
            {
                reversing = true;
            }
            else if (moveUpDown.GetAlpha() == 0.0f)
            {
                reversing = false;
            }
            transl.z = moveUpDown.GetValue();
            transform.Translation = transl;

            Float3 rot = cubeTransform.Rotation;
            rot += dt * 3.0f;
            cubeTransform.Rotation = rot;
        }
        protected override void EndEvent() { }
    }
}
