using Shard3D.Library.Types; // For vector types (f2, f3, f4)
using Shard3D; 
using Shard3D.Components;
using System;
using Shard3D.Library;
using Shard3D.UI;

namespace MyTestProject {
	namespace Controllers {
		public class PlayerActor : Actor {

			public float walkingSpeed = 3.0f; // meters per second

			const ulong CameraActor = 8839328315883139233;
			const ulong CameraPivotActor = 8392524855681231218;


			TransformComponent playerTransform;
			TransformComponent cameraPivotTransform;
			TransformComponent cameraPositionTransform;
			CameraComponent camera;

			HUD debugHUD = new HUD("content/debug_analog_hud.s3dasset");
				
			private Float2 lastMouse = new Float2(0.0F, 0.0F);

			private float sensitivity = 5.0f;

			protected override void BeginEvent() {
				debugHUD.Display();

				playerTransform = this.GetComponent<TransformComponent>();
				cameraPositionTransform = this.GetActor(CameraActor).GetComponent<TransformComponent>();
				cameraPivotTransform = this.GetActor(CameraPivotActor).GetComponent<TransformComponent>();
				camera = this.GetActor(CameraActor).GetComponent<CameraComponent>();
				camera.FOV = 70.0f;
				camera.Possess();
			}
			protected override void EndEvent() {
				debugHUD.Close();
			}

			protected override void TickEvent(float dt) {

				Float3 rotation = playerTransform.Rotation;
				Float3 forward = playerTransform.GetForward();
				Float3 right = playerTransform.GetRight();
				Float3 movement = new Float3();

				float speedMod = 1.0f;

				// KEYBOARD
				if (Input.IsKeyDown(KeyInput.KeyW)) {
					movement += forward;
				}
				if (Input.IsKeyDown(KeyInput.KeyA)) {
					movement -= right;
				}
				if (Input.IsKeyDown(KeyInput.KeyS)) {
					movement -= forward;
				}
				if (Input.IsKeyDown(KeyInput.KeyD)) {
					movement += right;
				}
				if (Input.IsKeyDown(KeyInput.KeyLeftShift)) {
					speedMod = 3.0f;
				}
				if (Input.IsKeyDown(KeyInput.KeyLeftControl)) {
					speedMod = 0.5f;
				}
				

				// MOUSE
				Float2 mouse = Input.GetMousePosition();
				Float2 mouseNow = mouse / Renderer.GetResolution();
				Float2 ori = mouseNow - lastMouse;
				lastMouse = mouseNow;

				Float3 orientation = cameraPivotTransform.Rotation;
				if (Input.IsMouseButtonDown(MouseInput.MouseButton2)) {
					orientation.x += ori.y * sensitivity;
					rotation.z += ori.x * sensitivity;
				}

				// GAMEPAD
				if (Input.IsGamepadPresent()) {
					Float2 axesLeft = new Float2(Input.GetGamepadAnalogAxis(GamepadAnalogInput.GamepadAnalogLeftStickX),
						Input.GetGamepadAnalogAxis(GamepadAnalogInput.GamepadAnalogLeftStickY));
					Float2 axesRight = new Float2(Input.GetGamepadAnalogAxis(GamepadAnalogInput.GamepadAnalogRightStickX),
						Input.GetGamepadAnalogAxis(GamepadAnalogInput.GamepadAnalogRightStickY));

					debugHUD.GetElement<HUDElement.Text>("LeftAnalogStickText").SetTextFormattingParameter("xaxis", axesLeft.x.ToString("N2"));
					debugHUD.GetElement<HUDElement.Text>("LeftAnalogStickText").SetTextFormattingParameter("yaxis", axesLeft.y.ToString("N2"));
					debugHUD.GetElement<HUDElement.Text>("RightAnalogStickText").SetTextFormattingParameter("xaxis", axesRight.x.ToString("N2"));
					debugHUD.GetElement<HUDElement.Text>("RightAnalogStickText").SetTextFormattingParameter("yaxis", axesRight.y.ToString("N2"));

					debugHUD.GetElement<HUDElement.Image>("JoystickImage").Position = 
						new Float2(-128.0F, 256.0F) + axesLeft * new Float2(64.0F, 64.0F);

					movement += forward * -axesLeft.y;
					movement += right * axesLeft.x;

					//speedMod = Input.GetGamepadAnalogAxis(GamepadAnalogInput.GamepadAnalogRT) * 0.5f + 0.5f;
					orientation.x += axesRight.y * 0.1f;
					rotation.z += axesRight.x * 0.1f;
				}
				orientation.x = SMath.Clamp(orientation.x, -1.5f, 1.5f);
				cameraPivotTransform.Rotation = orientation;
				playerTransform.Rotation = rotation;


				Float3 translation = playerTransform.Translation;
				if (SMath.Dot(movement, movement) > float.Epsilon) {
					translation += SMath.Normalize(movement) * dt * walkingSpeed * speedMod;
				}
				playerTransform.Translation = translation;
			}

		}
	}
}