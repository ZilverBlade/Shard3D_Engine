﻿using Shard3D.Library.Types;
using System.Runtime.CompilerServices;

namespace MyTestProject
{
    public class Rotator : Shard3D.Actor
    {
        public float velocity = 5.0f; // radians per second;
        private Shard3D.Components.TransformComponent transform;
        override protected void BeginEvent() {
            transform = this.GetComponent<Shard3D.Components.TransformComponent>();

        }
        protected override void TickEvent(float dt) {
            Float3 rotation = transform.Rotation;
            rotation.z += velocity * 4.0f * dt;
            //rotation.x += velocity * 2.0f * dt;
            //rotation.y += velocity * 8.0f * dt;
            transform.Rotation = rotation;
        }
        protected override void EndEvent() { }
    }
}