﻿using Shard3D.Library.Types; // For vector types (f2, f3, f4)
using Shard3D;
using Shard3D.Components;
using System;
using Shard3D.Library;
using Shard3D.UI;
using Shard3D.Physics;
using Shard3D.Library.Animation.Motion;

namespace MyTestProject {
	namespace Controllers {
		public class PlayerActorFPS : Actor {

			public float walkingSpeed = 3.0f; // meters per second

			const ulong CameraActor = 8392524855681231218;

			TransformComponent playerTransform;
			TransformComponent scopeTransform;
			CameraComponent camera;

			private Float2 lastMouse = new Float2(0.0F, 0.0F);

			private float sensitivity = 5.0f;
			float recoilAmount = 0.2f; // radians of X+ rotation
			float recoilAccum = 0.0f;
			float recoilSubtract = 0.0f;

			MouseInput triggerBullet = MouseInput.MOUSE_BUTTON_LEFT;
			MouseInput toggleScope = MouseInput.MOUSE_BUTTON_RIGHT;
			bool shot = false;
			bool buttonPressed = false;
			bool buttonScopePressed = false;
			Float2 scopeHUDElementSize = new Float2(0.0F);
			HUD scopeUI = new HUD("engine/hud/sniper_scope_hud.s3dasset");
			bool scopeOpen = false;
			const float BULLET_SIZE = 0.10f;

			TweenFloat.Ease fovEase = new TweenFloat.Ease(1.0f, 90.0f, 90.0f);

			void ShowScope() {
				scopeUI.Display();
				scopeHUDElementSize = scopeUI.GetElement<HUDElement.Image>("Scope Overlay").Scale;
				scopeOpen = true;
				fovEase = new TweenFloat.Ease(0.3f, fovEase.GetValue(), 30.0f);
			}
			void CloseScope() {
				scopeUI.Close();
				scopeOpen = false;
				fovEase = new TweenFloat.Ease(0.3f, fovEase.GetValue(), 90.0f);
			}
			protected override void BeginEvent() {
				playerTransform = this.GetComponent<TransformComponent>();
				camera = this.GetActor(CameraActor).GetComponent<CameraComponent>();
				scopeTransform = this.GetActor(CameraActor).GetComponent<TransformComponent>();
				camera.Possess();

			}
			protected override void EndEvent() {
			}
			protected override void PhysicsTickEvent(float ts) {
				if (shot) {
					shot = false;
					recoilAccum += recoilAmount;
					Float3 t = scopeTransform.GetTranslationWorld();
					Float3 f = scopeTransform.GetForward();
					PhysicsContact contact = Physics.RayCast(t, f, 100.0F);
					if (contact.actor.Exists()) {
						Actor decalActor = Level.CreateEmptyActor("Bullet Hole", true);
						decalActor.AddComponent<DeferredDecalComponent>().Material = "engine/materials/decals/bullet.s3dasset";
						decalActor.AddComponent<BoxVolumeComponent>().Bounds = new Float3(BULLET_SIZE, BULLET_SIZE, 0.10f);
						Float3 right = new Float3(0.99999f, 0.00001f, 0.00001f);//scopeTransform.GetRight();
						Float3 front = SMath.Cross(right, contact.normal);
						right = SMath.Cross(contact.normal, front);
						Float3x3 orientation = new Float3x3(SMath.Normalize(right), contact.normal, SMath.Normalize(front));
						decalActor.GetComponent<TransformComponent>().SetRotationMatrix(orientation);
						decalActor.GetComponent<TransformComponent>().Translation = contact.point;
					}
				}
			}
			protected override void TickEvent(float dt) {
				fovEase.Update(dt);
				camera.FOV = fovEase.GetValue();
				if (scopeOpen) {
					scopeUI.GetElement<HUDElement.Image>("Scope Overlay").Scale = scopeHUDElementSize * Renderer.GetResolution() / new Float2(1280, 720);
					if (Input.IsMouseButtonDown(triggerBullet)) {
						if (buttonPressed == false) {
							if (shot == false) {
								shot = true;
							}
							buttonPressed = true;
						}
					} else {
						buttonPressed = false;
					}
				}
				if (Input.IsMouseButtonDown(toggleScope)) {
					if (buttonScopePressed == false) {
						if (!scopeOpen) {
							ShowScope();
                        } else {
							CloseScope();
                        }
						buttonScopePressed = true;
					}
				} else {
					buttonScopePressed = false;
				}
				Float3 orientation = scopeTransform.Rotation;
				orientation.x -= recoilAccum;
				recoilSubtract += recoilAccum;
				recoilAccum = 0.0f;
				Float3 rotation = playerTransform.Rotation;
				Float3 forward = playerTransform.GetForward();
				Float3 right = playerTransform.GetRight();
				Float3 movement = new Float3();

				float speedMod = 1.0f;

				// KEYBOARD
				if (Input.IsKeyDown(KeyInput.KeyW)) {
					movement += forward;
				}
				if (Input.IsKeyDown(KeyInput.KeyA)) {
					movement -= right;
				}
				if (Input.IsKeyDown(KeyInput.KeyS)) {
					movement -= forward;
				}
				if (Input.IsKeyDown(KeyInput.KeyD)) {
					movement += right;
				}
				if (Input.IsKeyDown(KeyInput.KeyLeftShift)) {
					speedMod = 3.0f;
				}
				if (Input.IsKeyDown(KeyInput.KeyLeftControl)) {
					speedMod = 0.5f;
				}


				// MOUSE
				Float2 mouse = Input.GetMousePosition();
				Float2 mouseNow = mouse / Renderer.GetResolution();
				Float2 ori = mouseNow - lastMouse;
				lastMouse = mouseNow;

				orientation.x += ori.y * sensitivity;
				rotation.y += ori.x * sensitivity;
				

				// GAMEPAD
				if (Input.IsGamepadPresent()) {
					Float2 axesLeft = new Float2(Input.GetGamepadAnalogAxis(GamepadAnalogInput.GamepadAnalogLeftStickX),
						Input.GetGamepadAnalogAxis(GamepadAnalogInput.GamepadAnalogLeftStickY));
					Float2 axesRight = new Float2(Input.GetGamepadAnalogAxis(GamepadAnalogInput.GamepadAnalogRightStickX),
						Input.GetGamepadAnalogAxis(GamepadAnalogInput.GamepadAnalogRightStickY));

					movement += forward * -axesLeft.y;
					movement += right * axesLeft.x;

					//speedMod = Input.GetGamepadAnalogAxis(GamepadAnalogInput.GamepadAnalogRT) * 0.5f + 0.5f;
					orientation.x += axesRight.y * 0.02f;
					rotation.y += axesRight.x * 0.02f;
				}
				recoilSubtract = SMath.Max(recoilSubtract - dt * 0.5f, 0.0f);
				float moveDownRecoil = recoilSubtract == 0.0f ? 0.0f : +dt * 0.5f;
				orientation.x = SMath.Clamp(orientation.x + moveDownRecoil, -1.0f, 1.0f);
				playerTransform.Rotation = rotation;
				scopeTransform.Rotation = orientation;

				Float3 translation = playerTransform.Translation;
				if (SMath.Dot(movement, movement) > float.Epsilon) {
					translation += SMath.Normalize(movement) * dt * walkingSpeed * speedMod;
				}
				playerTransform.Translation = translation;
			}

		}
	}
}