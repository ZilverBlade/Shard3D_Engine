﻿using System;
using Shard3D.Library.Types;
using Shard3D;
using Shard3D.Library;
using Shard3D.Components;
using Shard3D.UI;
using Shard3D.Physics;
using Shard3D.Library.Animation.Motion;

namespace MyTestProject {
    public class CinematicPF : Actor {
        Actor meshA;
        Actor cameraActor;
        int state = 0;
        override protected void BeginEvent() {
            meshA = Level.GetActor("SkillActor");
            cameraActor = Level.GetActor("Camera Actor");
            cameraActor.GetComponent<CameraComponent>().FOV = 60.0f;
            cameraActor.GetComponent<CameraComponent>().Possess(); 
        }
        override protected void EndEvent() {

        }
        protected override void TickEvent(float dt) {
            if (Input.IsKeyDown(KeyInput.Key1)) {
                meshA.GetComponent<SkeletonRigComponent>().SetAnimation("content/launchger/missile_launchertest_RocketLauncherBaseTest_animations/Armature_Engage.s3danimation");
                meshA.GetComponent<SkeletonRigComponent>().StartAnimation();
                state = 1;
            }
            if (state == 1 && (meshA.GetComponent<SkeletonRigComponent>().GetAnimationTimeElasped() + dt) > 3.958F) {
                meshA.GetComponent<SkeletonRigComponent>().StopAnimation();
                if (Input.IsKeyDown(KeyInput.Key2)) {
                    meshA.GetComponent<SkeletonRigComponent>().SetAnimation("content/launchger/missile_launchertest_RocketLauncherBaseTest_animations/Armature_Launch.s3danimation");
                    meshA.GetComponent<SkeletonRigComponent>().StartAnimation();
                    state = 2;
                    if (state == 1 && (meshA.GetComponent<SkeletonRigComponent>().GetAnimationTimeElasped() + dt) > 0.417F) {
                        meshA.GetComponent<SkeletonRigComponent>().StopAnimation();
                    }
                }
            }
        }
    }
}
