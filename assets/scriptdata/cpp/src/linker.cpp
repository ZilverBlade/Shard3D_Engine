#include <iostream>
#include "test.h"
constexpr int GENERATED_VERSION = 3328;

/*
	To add an c++ internal call into C#, the steps are as following:

	- add "using System.Runtime.CompilerServices;"
	- declare an extern static void with the same input arguments as the c++ call (Note, use the "out" keyword for pointers)
	- add the [MethodImplAttribute(MethodImplOptions.InternalCall)] attribute on top of your function
	
	Additional notes:
		- Strings MUST be a void*, and be converted to a const char* with the functions provided by the EngineData struct
		- Prefer using pointers to return data, rather than using a return type
		- The function name is not required to be identical to the C++ implementation
		- Struct input parameters are allowed, as long as they are the same size

	Example: (Shard3D Core function)
-> C# side

using System.Runtime.CompilerServices;
using Shard3D.Library.Types;

namespace Shard3D.Core
{
    internal static class InternalCalls
    {
 		[MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void TransformComponent_GetTranslation(ulong bpInvokeUUID, ulong UUID, out Float3 translation);
    }
}

-> C++ side
static void TransformComponent_GetTranslation(uint64_t blueprintParentInvokationID, uint64_t actorID, glm::vec3* v) {
	// ...
}

*/

struct EngineData {
	int version;
	const char* (*getMonoStringFunc)(void*);
	void (*freeMonoStringFunc)(void*);
};
struct CppCustomInternalCallData {
	const char* name;
	const void* func;
};
struct CppCustomInternalCallCreateInfo {
	int numInteralCalls;
	CppCustomInternalCallData* cppUserInternalCalls;
};

// mark extern C to avoid c++ function call decoration
extern "C" __declspec(dllexport) int __stdcall S3DRetrieveInternalCalls(const void* data, void* ptr) {
	const EngineData* engineData = (const EngineData*)data;
	CppCustomInternalCallCreateInfo* create = (CppCustomInternalCallCreateInfo*)ptr;
	
	if (GENERATED_VERSION < engineData->version) {
		std::cout << "Warning! Engine version is newer than generated C++ file.\nCertain functions may be outdated.\n";
	}
	
	create->numInteralCalls = 0;//amount of total internal calls
	create->cppUserInternalCalls = (CppCustomInternalCallData*)calloc(create->numInteralCalls, sizeof(CppCustomInternalCallData));

	// return success to signal success
	return EXIT_SUCCESS;
}