using Shard3D.Library.Types; // For vector types (f2, f3, f4)
public class Example : Shard3D.Actor
{
	protected override void BeginEvent()
	{
		System.Console.WriteLine("Hello World from Shard3D!");
	}

	protected override void TickEvent(float dt)
	{
		// Moves up the +Z axis 1.0 meters per second
		Float3 translation = this.GetComponent<Shard3D.Components.TransformComponent>().Translation;
		translation.z += 1.0f * dt;
		this.GetComponent<Shard3D.Components.TransformComponent>().Translation = translation;
	}

	protected override void EndEvent()
	{
		System.Console.WriteLine("Bye!");
	}

	protected override void SpawnEvent()
	{
		System.Console.WriteLine("I just appeared!");
	}
	protected override void KillEvent()
	{
		System.Console.WriteLine("Ahhhh I was destroyed!");
	}
}