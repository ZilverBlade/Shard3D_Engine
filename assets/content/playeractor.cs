using Shard3D.Library.Types; // For vector types (f2, f3, f4)
using Shard3D; 
using Shard3D.Components;
using System;
namespace MyTestProject {
	namespace Controllers {
		public class PlayerActor : Actor {

			public float walkingSpeed = 3.0f; // meters per second

			const ulong CameraActor = 8839328315883139233;
			const ulong CameraPivotActor = 8392524855681231218;

			TransformComponent playerTransform;
			TransformComponent cameraPivotTransform;
			TransformComponent cameraPositionTransform;
			CameraComponent camera;


			private Float2 lastMouse = new Float2(0.0F, 0.0F);

			private float sensitivity = 5.0f;

			protected override void BeginEvent() {
				playerTransform = this.GetComponent<TransformComponent>();
				cameraPositionTransform = this.GetActor(CameraActor).GetComponent<TransformComponent>();
				cameraPivotTransform = this.GetActor(CameraPivotActor).GetComponent<TransformComponent>();
				camera = this.GetActor(CameraActor).GetComponent<CameraComponent>();
				camera.FOV = 70.0f;
				camera.Possess();
			}

			protected override void TickEvent(float dt) {
				Float3 rotation = playerTransform.Rotation;
				Float3 forward = new Float3((float)Math.Sin(rotation.z), (float)Math.Cos(rotation.z), 0.0F);
				Float3 right = new Float3(forward.y, -forward.x, 0.0F);
				Float3 movement = new Float3();

				float speedMod = 1.0f;

				if (Input.IsKeyDown(KeyInput.KeyW)) {
					movement += forward;
				}
				if (Input.IsKeyDown(KeyInput.KeyA)) {
					movement -= right;
				}
				if (Input.IsKeyDown(KeyInput.KeyS)) {
					movement -= forward;
				}
				if (Input.IsKeyDown(KeyInput.KeyD)) {
					movement += right;
				}
				if (Input.IsKeyDown(KeyInput.KeyLeftShift)) {
					speedMod = 3.0f;
				}
				if (Input.IsKeyDown(KeyInput.KeyLeftControl)) {
					speedMod = 0.5f;
				}

				Float2 mouse = Input.GetMousePosition();
				Float2 mouseNow = mouse / Renderer.GetResolution();
				Float2 ori = mouseNow - lastMouse;
				lastMouse = mouseNow;

				if (Input.IsMouseButtonDown(MouseInput.MouseButton2)) {
					Float3 orientation = cameraPivotTransform.Rotation;
					orientation.x += ori.y * sensitivity;
					orientation.x = Shard3D.Library.Math.Clamp(orientation.x, -1.5f, 1.5f);
					rotation.z += ori.x * sensitivity;
					cameraPivotTransform.Rotation = orientation;
					playerTransform.Rotation = rotation;
				}

				Float3 translation = playerTransform.Translation;
				if (Shard3D.Library.Math.Dot(movement, movement) > float.Epsilon) {
					translation += Shard3D.Library.Math.Normalize(movement) * dt * walkingSpeed * speedMod;
				}
				playerTransform.Translation = translation;
			}

			protected override void EndEvent() {
			}

			protected override void SpawnEvent() {
			}
			protected override void KillEvent() {
			}
		}
	}
}