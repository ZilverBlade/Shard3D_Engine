#include "elements.h"
#include <Shard3DEditorKit/tools/asset_reference_tracker.h>
using namespace Shard3D;

void ImGuiEx::DragIntPow2(const char* label, int* v, int min, int max, const char* format) {
	//int real = sqrt(v);
	ImGui::DragInt(label, v, 1.0f, min, max, format);
}

void ImGuiEx::drawTransformControl(const std::string& label, glm::vec3* values, float resetValue, float stepVal, float columnWidth) {
	ImGui::PushID(label.c_str());

	ImGui::Columns(2);
	ImGui::SetColumnWidth(0, columnWidth);
	ImGui::Text(label.c_str());
	ImGui::NextColumn();

	ImGui::PushMultiItemsWidths(3, ImGui::CalcItemWidth());
	ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0.f, 0.f));
	float lineHeight = ENGINE_FONT_SIZE + GImGui->Style.FramePadding.y * 2.f;
	ImVec2 buttonSize = { lineHeight + 3.f, lineHeight };

	ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.7f, 0.f, 0.f, 1.f));
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4(0.9f, 0.f, 0.f, 1.f));
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(0.9f, 0.2f, 0.2f, 1.f));
	if (ImGui::Button("X", buttonSize)) values->x = resetValue;
	ImGui::PopStyleColor(3);
	ImGui::SameLine(); ImGui::DragFloat("##X", &values->x, stepVal); ImGui::PopItemWidth(); ImGui::SameLine();

	ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.f, 0.5f, 0.f, 1.f));
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4(0.f, 0.7f, 0.f, 1.f));
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(0.3f, 0.7f, 0.3f, 1.f));
	if (ImGui::Button("Y", buttonSize)) values->y = resetValue;
	ImGui::PopStyleColor(3);
	ImGui::SameLine(); ImGui::DragFloat("##Y", &values->y, stepVal); ImGui::PopItemWidth(); ImGui::SameLine();

	ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.f, 0.f, 0.7f, 1.f));
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4(0.f, 0.f, 0.9f, 1.f));
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(0.2f, 0.2f, 0.9f, 1.f));
	if (ImGui::Button("Z", buttonSize)) values->z = resetValue;
	ImGui::PopStyleColor(3);
	ImGui::SameLine(); ImGui::DragFloat("##Z", &values->z, stepVal); ImGui::PopItemWidth();

	ImGui::PopStyleVar();
	ImGui::Columns(1);

	ImGui::PopID();
}
void ImGuiEx::drawTransform2DControl(const std::string& label, glm::vec2& values, float resetValue, float stepVal, const char* format, float columnWidth) {
	ImGui::PushID(label.c_str());

	ImGui::Columns(2);
	ImGui::SetColumnWidth(0, columnWidth);
	ImGui::Text(label.c_str());
	ImGui::NextColumn();

	ImGui::PushMultiItemsWidths(3, ImGui::CalcItemWidth());
	ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0.f, 0.f));
	float lineHeight = ENGINE_FONT_SIZE + GImGui->Style.FramePadding.y * 2.f;
	ImVec2 buttonSize = { lineHeight + 3.f, lineHeight };

	ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.7f, 0.f, 0.f, 1.f));
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4(0.9f, 0.f, 0.f, 1.f));
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(0.9f, 0.2f, 0.2f, 1.f));
	if (ImGui::Button("X", buttonSize)) values.x = resetValue;
	ImGui::PopStyleColor(3);
	ImGui::SameLine(); ImGui::DragFloat("##X", &values.x, stepVal, 0.f, 0.f, format); ImGui::PopItemWidth(); ImGui::SameLine();

	ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.f, 0.5f, 0.f, 1.f));
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4(0.f, 0.7f, 0.f, 1.f));
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(0.3f, 0.7f, 0.3f, 1.f));
	if (ImGui::Button("Y", buttonSize)) values.y = resetValue;
	ImGui::PopStyleColor(3);
	ImGui::SameLine(); ImGui::DragFloat("##Y", &values.y, stepVal, 0.f, 0.f, format); ImGui::PopItemWidth();

	ImGui::PopStyleVar();
	ImGui::Columns(1);

	ImGui::PopID();
}
#if 0
struct ComboFilterState
{
	int  activeIdx;         // Index of currently 'active' item by use of up/down keys
	bool selectionChanged;  // Flag to help focus the correct item when selecting active item
};

static bool ComboFilter__DrawPopup(ComboFilterState& state, std::unordered_set<AssetID> assets, int entryCount, int START, AssetType type)
{
	using namespace ImGui;
	bool clicked = 0;

	// Grab the position for the popup
	ImVec2 pos = GetItemRectMin(); pos.y += GetItemRectSize().y;
	ImVec2 size = ImVec2(GetItemRectSize().x - 60, GetItemsLineHeightWithSpacing() * 4);

	PushStyleVar(ImGuiStyleVar_WindowRounding, 0);

	ImGuiWindowFlags flags =
		ImGuiWindowFlags_NoTitleBar |
		ImGuiWindowFlags_NoResize |
		ImGuiWindowFlags_NoMove |
		ImGuiWindowFlags_HorizontalScrollbar |
		ImGuiWindowFlags_NoSavedSettings |
		0; //ImGuiWindowFlags_ShowBorders;

	SetNextWindowFocus();

	SetNextWindowPos(pos);
	SetNextWindowSize(size);
	Begin("##combo_filter", nullptr, flags);

	PushAllowKeyboardFocus(false);

	for (int i = 0; i < entryCount; i++) {
		// Track if we're drawing the active index so we
		// can scroll to it if it has changed
		bool isIndexActive = state.activeIdx == i;

		if (isIndexActive) {
			// Draw the currently 'active' item differently
			// ( used appropriate colors for your own style )
			PushStyleColor(ImGuiCol_Border, ImVec4(1, 1, 0, 1));
		}

		PushID(i);
		if (Selectable(ENTRIES[i], isIndexActive)) {
			// And item was clicked, notify the input
			// callback so that it can modify the input buffer
			state.activeIdx = i;
			clicked = 1;
		}
		if (IsItemFocused() && IsKeyPressed(GetIO().KeyMap[ImGuiKey_Enter])) {
			// Allow ENTER key to select current highlighted item (w/ keyboard navigation)
			state.activeIdx = i;
			clicked = 1;
		}
		PopID();

		if (isIndexActive) {
			if (state.selectionChanged) {
				// Make sure we bring the currently 'active' item into view.
				SetScrollHere();
				state.selectionChanged = false;
			}

			PopStyleColor(1);
		}
	}

	PopAllowKeyboardFocus();
	End();
	PopStyleVar(1);

	return clicked;
}
#endif


//https://github.com/ocornut/imgui/issues/1658
bool ImGuiEx::drawAssetBox(const char* label, Shard3D::AssetType assetType, Shard3D::AssetID& currentAsset) {
	AssetID currentOption = currentAsset;

	const char* payloadLabel;
	const std::unordered_set<AssetID>* listptr = &AssetReferenceTracker::getAssetList(assetType);
	switch (assetType) {
	case (AssetType::Texture2D):
		payloadLabel = "SHARD3D.ASSEXP.TEX";
		break;
	case (AssetType::TextureCube):
		payloadLabel = "SHARD3D.ASSEXP.TXC";
		break;
	case (AssetType::Model3D):
		payloadLabel = "SHARD3D.ASSEXP.MESH";
		break;
	case (AssetType::SkeletonRig):
		payloadLabel = "SHARD3D.ASSEXP.SKEL";
		break;
	case (AssetType::SkeletonAnimation):
		payloadLabel = "SHARD3D.ASSEXP.SKLA";
		break;
	case (AssetType::Material):
		payloadLabel = "SHARD3D.ASSEXP.MATE";
		break;
	case (AssetType::PhysicsAsset):
		payloadLabel = "SHARD3D.ASSEXP.PHYA";
		break;
	case (AssetType::PhysicsMaterial):
		payloadLabel = "SHARD3D.ASSEXP.PHYM";
		break;
	case (AssetType::PhysicsMaterialCollection):
		payloadLabel = "SHARD3D.ASSEXP.PHMC";
		break;
	case (AssetType::Prefab):
		payloadLabel = "SHARD3D.ASSEXP.BLUP";
		break;
	default:
		SHARD3D_ASSERT(false && "retard");
	}

	//static char filter[256] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 ,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 ,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,00,0,0,0 };

	if (ImGui::BeginCombo(label, currentOption.getAsset().c_str())) {
		for (auto& asset : *listptr) {
			//if (!strUtils::contains(asset.getAsset(), filter)) continue;
			bool is_selected = (currentOption == asset); // You can store your selection however you want, outside or inside your objects
			if (ImGui::Selectable(asset.getAsset().c_str(), is_selected)) {
				currentOption = asset;
				currentAsset = AssetID(asset.getAsset());
				ImGui::EndCombo();
				return true;
			}
			if (is_selected)
				ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
		}
		ImGui::EndCombo();
	}
	if (ImGui::BeginDragDropTarget())
		if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload(payloadLabel)) {
			currentAsset = AssetID(std::string((char*)payload->Data));
			return true;
		}
	return false;
}
