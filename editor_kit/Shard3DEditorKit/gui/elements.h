#pragma once
#include "../core.h"
#include <string>
#include <glm/ext/vector_float3.hpp>
#include <glm/ext/vector_float2.hpp>
#include <imgui.h>
#include <imgui_internal.h>
#include <Shard3D/core.h>
#include <Shard3D/core/asset/assetmgr.h>

namespace ImGuiEx {
	S3D_DLLAPI void DragIntPow2(const char* label, int* v, int min, int max, const char* format = "%d");
		
	S3D_DLLAPI void drawTransformControl(const std::string& label, glm::vec3* values, float resetValue = 0.0f, float stepVal = 0.01f, float columnWidth = 100.f);
	S3D_DLLAPI inline void drawTransformControl(const std::string& label, glm::vec3& values, float resetValue = 0.0f, float stepVal = 0.01f, float columnWidth = 100.f) {
		drawTransformControl(label, &values, resetValue, stepVal, columnWidth);
	}
	S3D_DLLAPI void drawTransform2DControl(const std::string& label, glm::vec2& values, float resetValue = 0.0f, float stepVal = 0.01f, const char* format = "%.3f", float columnWidth = 100.f);
	S3D_DLLAPI bool drawAssetBox(const char* label, Shard3D::AssetType assetType, Shard3D::AssetID& currentAsset);
}