
#include "icons.h"
namespace Shard3D {


	static ImGuiIcons___* ImGuiIcons;

	ImGuiIcons___& getImGuiIcon() {
		return *ImGuiIcons;
	}

	void createImGuiIcons() {
		ImGuiIcons = new ImGuiIcons___();
		{
			auto& img = _special_assets::_get().at("editor.play");
			ImGuiIcons->play = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.pause");
			ImGuiIcons->pause = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.stop");
			ImGuiIcons->stop = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.pref");
			ImGuiIcons->pref = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.save");
			ImGuiIcons->save = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.preview");
			ImGuiIcons->preview = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.layout");
			ImGuiIcons->layout = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.load");
			ImGuiIcons->load = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.level");
			ImGuiIcons->level = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.viewport");
			ImGuiIcons->viewport = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.settings");
			ImGuiIcons->settings = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.launch");
			ImGuiIcons->launchgame = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.edit_position");
			ImGuiIcons->edit_position = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.edit_position.add");
			ImGuiIcons->edit_position_add = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.edit_position.rmv");
			ImGuiIcons->edit_position_rmv = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.edit_node");
			ImGuiIcons->edit_node = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.edit_node.add");
			ImGuiIcons->edit_node_add = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.edit_node.rmv");
			ImGuiIcons->edit_node_rmv = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.edit");
			ImGuiIcons->edit_any = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.gizmo.translate");
			ImGuiIcons->gizmo_translate = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.gizmo.rotate");
			ImGuiIcons->gizmo_rotate = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.gizmo.scale");
			ImGuiIcons->gizmo_scale = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.mirror");
			ImGuiIcons->mirror = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.mirror_left");
			ImGuiIcons->mirror_left = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		{
			auto& img = _special_assets::_get().at("editor.mirror_right");
			ImGuiIcons->mirror_right = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
	}
}