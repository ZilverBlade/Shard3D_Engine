#pragma once

#include "../core.h"
#include <Shard3D/core/vulkan_api/device.h>
#include "imgui_implementation.h"
#include <imgui.h>
struct ImGuiContext;
namespace Shard3D {
	class S3DRenderTarget;
	class ImGuiInitializer {
	public:
		static inline bool isImGUIInit = false;
		static inline VkDescriptorPool imGuiDescriptorPool;

		S3D_DLLAPI static void init(S3DDevice& dvc, S3DWindow& wnd, ImGuiContext* context, VkRenderPass renderPass, VkQueue rendererQueue, const char* iniFile, bool lightTheme);
		S3D_DLLAPI static void setViewportImage(VkDescriptorSet& image, S3DRenderTarget* framebufferAttachment);
	private:
		S3D_DLLAPI static void createGlobal(S3DDevice& dvc, S3DWindow& wnd, VkRenderPass renderPass, bool lightTheme);
	};
}