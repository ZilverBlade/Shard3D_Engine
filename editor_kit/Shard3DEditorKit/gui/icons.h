#pragma once

#include <volk.h>
#include "../tools/editor_assets.h" 
#include <Shard3DEditorKit/gui/imgui_implementation.h>
namespace Shard3D {

	// icons
	struct ImGuiIcons___ {
		VkDescriptorSet play, pause, stop,
			pref, save, load,
			preview, layout, viewport, level, launchgame,
			settings,
			l_save, l_load,
			edit_position, edit_position_add, edit_position_rmv,
			edit_node, edit_node_add, edit_node_rmv, edit_any,
			gizmo_translate, gizmo_rotate, gizmo_scale,
			mirror, mirror_left, mirror_right;

	};
	ImGuiIcons___& getImGuiIcon();

	void createImGuiIcons();
}