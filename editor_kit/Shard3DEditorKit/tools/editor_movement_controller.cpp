#include "editor_movement_controller.h"
#include <limits>

#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <iostream>
namespace Shard3D {
	namespace controller {
		void EditorMovementController::tryPollTranslation(S3DWindow& _window, float _dt, const ECS::Actor& _actor) {
			actor = _actor;

			float yaw = actor.getComponent<Components::TransformComponent>().getRotation().y;
			float pitch = actor.getComponent<Components::TransformComponent>().getRotation().x;
			const glm::vec3 forwardDir{ sin(yaw), -sin(pitch), cos(yaw) };
			const glm::vec3 rightDir{ forwardDir.z, 0.f, -forwardDir.x };
			const glm::vec3 upDir{ 0.f, 1.f, 0.f };

			glm::vec3 moveDir{ 0.f };
			if (_window.isKeyDown(keys.moveForward)) moveDir += forwardDir;
			if (_window.isKeyDown(keys.moveBackward)) moveDir -= forwardDir;
			if (_window.isKeyDown(keys.moveRight)) moveDir += rightDir;
			if (_window.isKeyDown(keys.moveLeft)) moveDir -= rightDir;
			if (_window.isKeyDown(keys.moveUp)) moveDir += upDir;
			if (_window.isKeyDown(keys.moveDown)) moveDir -= upDir;

			if (glm::dot(moveDir, moveDir) > std::numeric_limits<float>::epsilon()) {
				move += glm::normalize(moveDir) * _dt * acceleration;
			}
			if (glm::dot(move, move) > std::numeric_limits<float>::epsilon()) {
				move = glm::sign(move) * glm::clamp(glm::abs(move) - _dt * glm::abs(glm::normalize(move)) * drag, glm::vec3(0.0), glm::abs(glm::normalize(move)));
			}

			speedModifier = 0.25f + 0.75f * (!_window.isKeyDown(keys.slowDown)) + 1.75f * _window.isKeyDown(keys.speedUp);

			actor.getComponent<Components::TransformComponent>().setTranslation(
				actor.getComponent<Components::TransformComponent>().getTranslation() += pow(2.0f, moveSpeedExponent) * move * speedModifier * 0.1f);
		}
		void EditorMovementController::tryPollOrientation(S3DWindow& _window, float _dt, glm::vec4 screencontentArea, const ECS::Actor& _actor) {
			actor = _actor;
			GLFWwindow* window = _window.getGLFWwindow();
			if (_window.isMouseButtonDown(buttons.canRotate)) {
				//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

				double mouseX;
				double mouseY;
				glfwGetCursorPos(window, &mouseX, &mouseY);

				static float lastRotX	= 0.0f;
				static float lastRotY	= 0.0f;
				// Prevents camera from jumping on the first click
				if (firstClick) {
					lastRotX = (float)((mouseY - screencontentArea.y) - (screencontentArea.w / 2.f)) / screencontentArea.w;
					lastRotY = (float)((mouseX - screencontentArea.x) - (screencontentArea.z / 2.f)) / screencontentArea.z;
					firstClick = false;
				}

				float mouseDeltaX = (float)((mouseY - screencontentArea.y) - (screencontentArea.w / 2.f)) / screencontentArea.w;
				float mouseDeltaY = (float)((mouseX - screencontentArea.x) - (screencontentArea.z / 2.f)) / screencontentArea.z;


				float rotX = mouseDeltaX - lastRotX;
				float rotY = mouseDeltaY - lastRotY;

				lastRotX = mouseDeltaX;
				lastRotY = mouseDeltaY;

				// force the roll to be pi*2 radians
				orientation.z = 6.283185482f;

				// up down rotation
				orientation = glm::rotate(orientation, glm::radians(sensitivity * rotX), upVec);
				// to make sure it doesnt over-rotate			
				orientation.x = glm::clamp(orientation.x, -1.5707963f, 1.5707963f);

				// left right rotation
				orientation = glm::rotate(orientation, glm::radians(sensitivity * rotY), glm::normalize(glm::cross(orientation, upVec)));
			}
			else { 
				glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL); 
				firstClick = true; 
			}

			actor.getComponent<Components::TransformComponent>().setRotation({ orientation.x, orientation.y, 0.f });
		}
		void EditorMovementController::scrollMouse(S3DWindow& _window, float deltaY) {
			if (_window.isKeyDown(GLFW_KEY_LEFT_ALT)) {
				float fov = actor.getComponent<Components::CameraComponent>().fov;
				fov -= deltaY * 3.5f;
				actor.getComponent<Components::CameraComponent>().fov = glm::clamp(fov, 10.f, 170.f);
			}
			else {
				moveSpeedExponent = glm::clamp(moveSpeedExponent + deltaY * 0.1f, -2.f, 10.f);
			}
		}
	}
}