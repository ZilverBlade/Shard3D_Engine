#pragma once

#include "../core.h"
#include <Shard3D/vulkan_abstr.h>
#include <Shard3D/systems/systems.h>
#include <Shard3D/core/ecs/level.h>
#include <Shard3D/systems/rendering/simple_processing_system.h>
#include <Shard3D/core/rendering/level_render_instance.h>

namespace Shard3D {
	class StaticReflectionCapturePass {
		struct ReflectionCaptureRendererData {
			uPtr<DeferredRenderSystem> deferredRenderSystem{};
			uPtr<DecalRenderSystem> decalRenderSystem{};
		};
	public:
		StaticReflectionCapturePass(glm::vec3 position, sPtr<ECS::Level>& activeLevel, S3DDevice& device, ResourceSystem* resourceSystem, uPtr<S3DDescriptorSetLayout>& globalSetLayout, uPtr<S3DDescriptorSetLayout>& sceneSetLayout, uPtr<S3DDescriptorSetLayout>& riggedSkeletonLayout, uPtr<S3DDescriptorSetLayout>& terrainSetLayout, float resolution, float supersampling_mult = 1.f);
		~StaticReflectionCapturePass();
		DELETE_COPY(StaticReflectionCapturePass);
		S3D_DLLAPI void capture(const std::string& pathReflection, const std::string& pathIrr);
	private:
		S3D_DLLAPI void process(uPtr<DeferredRenderSystem>& renderer, int ind);
		S3D_DLLAPI void store(const std::string& pathReflection, const std::string& patIrr);
		StaticReflectionCapturePass::ReflectionCaptureRendererData renderer[6];
		uPtr<LevelRenderInstance> levelRenderer;
		uPtr<SimpleProcessingSystem> processors[6];
		uPtr<S3DDescriptorPool> localPool{};
		uPtr<S3DDescriptorSetLayout>& globalSetLayout;
		uPtr<S3DDescriptorSetLayout>& sceneSetLayout;
		uPtr<S3DDescriptorSetLayout>& riggedSkeletonLayout;
		uPtr<S3DDescriptorSetLayout>& terrainSetLayout;

		uPtr<S3DDescriptorSetLayout> uboSetLayout{};
		uPtr<S3DBuffer> globalUBO;
		VkDescriptorSet uboDescriptorSet{};

		ResourceSystem* resourceSystem;

		S3DCamera camera{};
		S3DDevice& device;
		sPtr<ECS::Level> level;
		glm::ivec2 resolution;
		float ssm;
		glm::vec3 position;
	};
}