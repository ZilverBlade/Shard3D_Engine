#include <glm/gtc/type_ptr.hpp>
#include "level_properties_panel.h"

#include <Shard3D/ecs.h>

#include <imgui_internal.h>

#include <Shard3DEditorKit/kit.h>
#include <Shard3D/scripting/script_engine.h>
#include <fstream>
#include <Shard3D/systems/handlers/render_list.h>
#include <Shard3D/systems/handlers/prefab_manager.h>
#include <Shard3DEditorKit/tools/reflection_probe_capture.h>
#include <Shard3D/core/misc/engine_settings.h>
#include <Shard3D/core/misc/graphics_settings.h>
#include <Shard3D/utils/dialogs.h>
#include <Shard3DEditorKit/tools/script_gen.h>
#include <Shard3DEditorKit/gui/icons.h>
#include <Shard3D/core/asset/image_tools.h>

namespace Shard3D {

	LevelPropertiesPanel::~LevelPropertiesPanel() {}

	void LevelPropertiesPanel::setContext(sPtr<Level>& level, LevelRenderInstance* renderInstance, ResourceSystem* rs) {
		context = level; 
		renderContext = renderInstance;
		resourceSystem = rs;
	}
	void LevelPropertiesPanel::destroyContext() { context = nullptr; }

	static void drawShadowDebug(const ShadowMap* shadowMap) { // todo: descriptor leak
		constexpr bool enableShadowMapViewer = false; // buggy
		static std::unordered_map<const ShadowMap*, VkDescriptorSet> shadows = {};

		if (shadowMap) {
			if (enableShadowMapViewer) {
				if (shadows.find(shadowMap) == shadows.end()) {
					shadows[shadowMap] = ImGui_ImplVulkan_AddTexture(shadowMap->getOpaqueAttachment()->getSampler(), shadowMap->getOpaqueAttachment()->getImageView(), shadowMap->getOpaqueAttachment()->getImageLayout());
				}
			}
			ImGui::Text("Resource Size: %i kB", shadowMap->getEstimatedShadowMapSize());
			ImGui::Text("Draw Time CPU: %f ms", shadowMap->getDrawTimeCPU());
			ImGui::Text("Draw Time GPU: %f ms", shadowMap->getDrawTimeGPU());
			if (enableShadowMapViewer) {
				ImGui::Image(shadows[shadowMap], { 128, 128 });
			}
			return;
		}
		ImGui::Text("unavailable...");
		ImGui::Text("unavailable...");
		ImGui::Text("unavailable...");
		if (enableShadowMapViewer) {
			ImGui::Button("", { 128, 128 });
		}
	}

	void LevelPropertiesPanel::render(Actor* selectedActor) {
		SHARD3D_ASSERT(context != nullptr && "Context not provided!");
		ImGui::Begin("Properties"); //ImGui::BeginDisabled(context->simulationState == PlayState::Playing/*&& ini.canEditDuringSimulation*/);
		if (*selectedActor) { 
			drawActorProperties(*selectedActor);
			if (ImGui::Button("Convert to prefab")) { 
				ImGui::OpenPopup("Create Prefab");
			}
			if (ImGui::Button("Add Component")) ImGui::OpenPopup("AddComponent");
			if (ImGui::BeginPopup("AddComponent")) {
				if (!selectedActor->hasComponent<Components::ActorGroupComponent>()) if (ImGui::MenuItem("Group Tag")) {
					selectedActor->addComponent<Components::ActorGroupComponent>();
					ImGui::CloseCurrentPopup();
				}
				if (!selectedActor->hasComponent<Components::PointLightComponent>()) if (ImGui::MenuItem("Point Light")) {
					selectedActor->addComponent<Components::PointLightComponent>();
					ImGui::CloseCurrentPopup();
				}
				if (!selectedActor->hasComponent<Components::SpotLightComponent>()) if (ImGui::MenuItem("Spot Light")) {
					selectedActor->addComponent<Components::SpotLightComponent>();
					ImGui::CloseCurrentPopup();
				}
				if (!selectedActor->hasComponent<Components::DirectionalLightComponent>()) if (ImGui::MenuItem("Directional Light")) {
					selectedActor->addComponent<Components::DirectionalLightComponent>();
					ImGui::CloseCurrentPopup();
				}
				if (!selectedActor->hasComponent<Components::Rigidbody3DComponent>()) if (ImGui::MenuItem("Rigidbody3D")) {
					selectedActor->addComponent<Components::Rigidbody3DComponent>();
					ImGui::CloseCurrentPopup();
				}
				if (!selectedActor->hasComponent<Components::PhysicsConstraintComponent>()) if (ImGui::MenuItem("Physics Constraint")) {
					selectedActor->addComponent<Components::PhysicsConstraintComponent>();
					ImGui::CloseCurrentPopup();
				}
				if (!selectedActor->hasComponent<Components::Mesh3DComponent>()) if (ImGui::MenuItem("Mesh")) {
					selectedActor->addComponent<Components::Mesh3DComponent>(resourceSystem, ResourceSystem::coreAssets.m_defaultModel);
					ImGui::CloseCurrentPopup();
				}
				if (!selectedActor->hasComponent<Components::SkeletonRigComponent>()) if (ImGui::MenuItem("Skeleton")) {
					selectedActor->addComponent<Components::SkeletonRigComponent>();
					ImGui::CloseCurrentPopup();
				}
				if (!selectedActor->hasComponent<Components::CameraComponent>()) if (ImGui::MenuItem("Camera")) {
					selectedActor->addComponent<Components::CameraComponent>();
					resourceSystem->loadMesh(AssetID("engine/meshes/camcorder" ENGINE_ASSET_SUFFIX));
					selectedActor->addComponent<Components::Mesh3DComponent>(resourceSystem, AssetID("engine/meshes/camcorder" ENGINE_ASSET_SUFFIX));
					selectedActor->getComponent<Components::VisibilityComponent>().editorOnly = true;
					ImGui::CloseCurrentPopup();
				}

				ImGui::EndPopup();
			}
		}

		bool true_ = true;
		if (ImGui::BeginPopupModal("Create Prefab", &true_, ImGuiWindowFlags_Popup | ImGuiWindowFlags_NoDocking)) {
			char fileBuffer[256];
			memset(fileBuffer, 0, 256);
			strncpy(fileBuffer, modalCreatePrefabTextInputName.c_str(), 256);

			if (ImGui::InputText("Prefab Name", fileBuffer, 256)) {
				modalCreatePrefabTextInputName = std::string(fileBuffer);
			}
			char fileBuffer2[256];
			memset(fileBuffer2, 0, 256);
			strncpy(fileBuffer2, modalCreatePrefabTextInputLocation.c_str(), 256);

			if (ImGui::InputText("Prefab Location", fileBuffer2, 256)) {
				modalCreatePrefabTextInputLocation = std::string(fileBuffer2);
			}
			if (ImGui::Button("Browse")) {
				std::string folder = BrowserDialogs::openFolder("Select folder for your prefab", ProjectSystem::getAssetLocation().c_str());
				if (std::filesystem::exists(folder)) {
					std::string modalCreatePrefabTextInputLocation = std::filesystem::relative(folder, std::filesystem::path(ProjectSystem::getAssetLocation())).string();
				}
				else {
					MessageDialogs::show("Invalid folder provided", "Asset Error", MessageDialogs::OPTICONERROR);
				}
			}
			if (ImGui::Button("Create")) {
				Actor converted = *selectedActor;
				Level temporary = Level(resourceSystem);
				temporary.duplicateActor(converted, false);
				LevelManager lvlMgr(temporary, resourceSystem);
				if (!strUtils::hasEnding(modalCreatePrefabTextInputName, ".s3dprefab")) modalCreatePrefabTextInputName = modalCreatePrefabTextInputName + ".s3dprefab";

				if (modalCreatePrefabTextInputLocation.length() > 0)
					modalCreatePrefabTextInputLocation += "/";
				AssetID bpAsset = modalCreatePrefabTextInputLocation + modalCreatePrefabTextInputName;
				
				std::function<void(nlohmann::ordered_json&, void*)> extraSaveFunc = [&](nlohmann::ordered_json& out, void* userdata) {
					out["enableScript"] = reinterpret_cast<Prefab*>(userdata)->enableScript;
					out["scriptModule"] = reinterpret_cast<Prefab*>(userdata)->scriptModule;
				};
				Prefab imposter{resourceSystem};
				lvlMgr.save(bpAsset.getAbsolute(), false, extraSaveFunc, &imposter);
				context->getPrefabManager()->cachePrefab(bpAsset);

				UUID scopeID = converted.getScopedID();
				std::string actorName = converted.getTag();
				context->killActor(converted);
				context->runGarbageCollector();
				context->getPrefabManager()->instPrefab(context.get(), 0, scopeID, bpAsset, actorName);

				ImGui::CloseCurrentPopup();
				modalCreatePrefabTextInputLocation.clear();
				modalCreatePrefabTextInputName.clear();
			}
			if (ImGui::Button("Cancel")) {
				ImGui::CloseCurrentPopup();
				modalCreatePrefabTextInputLocation.clear();
				modalCreatePrefabTextInputName = "some_kind_of_prefab";
			}
			ImGui::EndPopup();
		}
		if (ImGui::BeginPopupModal("Create Script Engine", &true_, ImGuiWindowFlags_Popup | ImGuiWindowFlags_NoDocking)) {
			ImGui::Text(std::string("Game name: " + ProjectSystem::getGameName()).c_str());

			static bool openIDE = true;
			ImGui::Checkbox("Open project automatically", &openIDE);

			if (ImGui::Button("Setup")) {
				SHARD3D_INFO("Setting up script engine");
				CSimpleIniA ini;
				ini.LoadFile((ProjectSystem::getProjectLocation() + "/configdata/script_settings.ini").c_str());

				ScriptFileGenerationCreateInfo createInfo;

				std::string value = ini.GetValue("EDITOR", "preferredIDE");

				if		(value == std::string("VisualStudio2005")) createInfo.ide = IDEVersion::VisualStudio2005;
				else if (value == std::string("VisualStudio2008")) createInfo.ide = IDEVersion::VisualStudio2008;
				else if (value == std::string("VisualStudio2010")) createInfo.ide = IDEVersion::VisualStudio2010;
				else if (value == std::string("VisualStudio2012")) createInfo.ide = IDEVersion::VisualStudio2012;
				else if (value == std::string("VisualStudio2013")) createInfo.ide = IDEVersion::VisualStudio2013;
				else if (value == std::string("VisualStudio2015")) createInfo.ide = IDEVersion::VisualStudio2015;
				else if (value == std::string("VisualStudio2017")) createInfo.ide = IDEVersion::VisualStudio2017;
				else if (value == std::string("VisualStudio2019")) createInfo.ide = IDEVersion::VisualStudio2019;
				else if (value == std::string("VisualStudio2022")) createInfo.ide = IDEVersion::VisualStudio2022;

				auto splitIntoVector = [&](const char* in, std::vector<std::string>& out) {
					if (in == nullptr) return;
					std::string str = in;
					int lastNonCommaEntry = 0;
					for (int idx = 0; idx < str.length(); idx++) {
						if (in[idx] == ',') {
							out.push_back(str.substr(lastNonCommaEntry, idx - lastNonCommaEntry));
							lastNonCommaEntry = idx + 1;
							idx++;
						}
						if (idx + 1 == str.length()) {
							out.push_back(str.substr(lastNonCommaEntry, idx - lastNonCommaEntry + 1));
						}
					}
				};

				createInfo.allowUnsafeCode = ini.GetValue("HIGH_LEVEL_CS", "enableUnsafeCode");

				splitIntoVector(ini.GetValue("HIGH_LEVEL_CS", "includeLibraries"), createInfo.csInclude);
				splitIntoVector(ini.GetValue("LOW_LEVEL_CPP", "include"), createInfo.cppInclude);
				splitIntoVector(ini.GetValue("LOW_LEVEL_CPP", "includeLibraries"), createInfo.cppIncludeLibs);
				splitIntoVector(ini.GetValue("LOW_LEVEL_CPP", "linkLibrariesDebug"), createInfo.cppLinkLibsDebug);
				splitIntoVector(ini.GetValue("LOW_LEVEL_CPP", "linkLibrariesDeploy"), createInfo.cppLinkLibsRelease);

				createInfo.cppVersion = ini.GetValue("LOW_LEVEL_CPP", "cppVersion");
				createInfo.enableMPC = ini.GetBoolValue("LOW_LEVEL_CPP", "enableMPC");
				createInfo.gameName = ProjectSystem::getGameName();
				createInfo.projectGenFile = ENGINE_SCRIPT_LIBRARY_PATH"/p5template.txt";
				createInfo.templateFileCPP = ENGINE_SCRIPT_LIBRARY_PATH"/cpp_base_file.txt";
				createInfo.templateFileCS = ENGINE_SCRIPT_LIBRARY_PATH"/cs_template_class.txt";
				createInfo.openProjectAfterDone = openIDE;

				ScriptFileGenerator::generate(ENGINE_SCRIPT_LIBRARY_PATH"/premake5.exe", createInfo, ProjectSystem::getAssetLocation() + "scriptdata");
				SHARD3D_WARN("Finished setting up script engine, requires engine restart...");
				ImGui::CloseCurrentPopup();
			}
			if (ImGui::Button("Cancel")) {
				ImGui::CloseCurrentPopup();
			}
			ImGui::EndPopup();
		}
		ImGui::End();
	}

	void LevelPropertiesPanel::displayPreviewCamera(Actor& actor) {
		ImGui::Begin("PREVIEW (viewport)");
#if ENSET_ALLOW_PREVIEW_CAMERA
		ImGui::Image(Singleton::previewViewportImage, { 800, 600 });
#endif
		ImGui::End();
	}
	void LevelPropertiesPanel::drawActorProperties(Actor& actor) {	
		//if (ImGui::TreeNodeEx((void*)18975819752189, nodeFlags, "Brush")) {
		//	
		//	//auto& agc = renderContext->
		//	//
		//	//int i = 0;
		//	//int deleteIndex = -1;
		//	//for (auto& tag : agc.tags) {
		//	//	float sz = ImGui::GetFrameHeight();
		//	//	if (ImGui::Button("X", { sz, sz })) {
		//	//		deleteIndex = i;
		//	//		break;
		//	//	}
		//	//	ImGui::SameLine();
		//	//	char fileBuffer[256];
		//	//	memset(fileBuffer, 0, 256);
		//	//	strncpy(fileBuffer, tag.c_str(), 256);
		//	//	if (ImGui::InputText("", fileBuffer, 256)) {
		//	//		tag = std::string(fileBuffer);
		//	//	}
		//	//	i++;
		//	//}
		//	//if (deleteIndex != -1) {
		//	//	VectorUtils::eraseItemAtIndex(agc.tags, deleteIndex);
		//	//}
		//	//if (ImGui::Button("Add")) {
		//	//	agc.tags.push_back("");
		//	//}
		//
		//
		//	ImGui::TreePop();
		//}
		if (actor.hasComponent<Components::TagComponent>()) {
			auto& tag = actor.getComponent<Components::TagComponent>().tag;
			char tagBuffer[256];
			memset(tagBuffer, 0, 256);
			strncpy(tagBuffer, tag.c_str(), 256);
			if (ImGui::InputText("Tag", tagBuffer, 256)) {
				tag = std::string(tagBuffer);
			}
		}
		if (actor.hasComponent<Components::MobilityComponent>()) {
			if (ImGui::TreeNodeEx((void*)typeid(Components::MobilityComponent).hash_code(), nodeFlags, "Mobility")) {
				int* mob = reinterpret_cast<int*>(&actor.getComponent<Components::MobilityComponent>().mobility);
				if (ImGui::Combo("Mobility", mob, "Static\0Stationary\0Dynamic")) {
					switch (*reinterpret_cast<Mobility*>(mob)) {
					case (Mobility::Static):	actor.makeStatic();		break;
					case (Mobility::Stationary):actor.makeStationary();	break;
					case (Mobility::Dynamic):	actor.makeDynamic();	break;
					}
				}
				ImGui::TreePop();
			}
		}
		if (actor.hasComponent<Components::VisibilityComponent>()) {
			if (ImGui::TreeNodeEx((void*)typeid(Components::VisibilityComponent).hash_code(), nodeFlags, "Visibility")) {
				auto& vbc = actor.getComponent<Components::VisibilityComponent>();
				if (ImGui::Checkbox("Visible", &vbc.isVisible)) {
					if (actor.isVisible())
						actor.makeVisible();
					else actor.makeInvisible();
					if (vbc.editorOnly && GraphicsSettings2::editorPreview.ONLY_GAME) {
						actor.killComponent<Components::IsVisibileComponent>();
					}
				}
				if (ImGui::Checkbox("Hidden in Game", &vbc.editorOnly)) {
					if (GraphicsSettings2::editorPreview.ONLY_GAME) {
						if (vbc.editorOnly) {
							actor.killComponent<Components::IsVisibileComponent>();
						}
						else {
							actor.addComponent<Components::IsVisibileComponent>();
						}
					}
				}
				ImGui::Checkbox("Exportable", &vbc.exportable);
				ImGui::TreePop();
			}
		}
		if (actor.hasComponent<Components::TransformComponent>()) {
			if (ImGui::TreeNodeEx((void*)typeid(Components::TransformComponent).hash_code(), nodeFlags, "Transform")) {
				auto& tc = actor.getTransform();
				
				glm::vec3 tran = tc.getTranslation();
				ImGuiEx::drawTransformControl("Translation", tran, 0.f);
				tc.setTranslation(tran);

				glm::vec3 rota = glm::degrees(tc.getRotation());
				ImGuiEx::drawTransformControl("Rotation", rota, 0.f, 0.1f);
				tc.setRotation(glm::radians(rota));

				glm::vec3 scal = tc.getScale();
				ImGuiEx::drawTransformControl("Scale", scal, 1.f);
				tc.setScale(scal);

				ImGui::TreePop();
			}
		}
		if (actor.hasComponent<Components::ActorGroupComponent>()) {
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::ActorGroupComponent).hash_code(), nodeFlags, "Actor Group Tags");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {
				auto& agc = actor.getComponent<Components::ActorGroupComponent>();
				std::vector<char[256]> textBuffers(agc.tags.size());
				int i = 0;
				int deleteIndex = -1;
				for (auto& tag : agc.tags) {
					float sz = ImGui::GetFrameHeight();
					if (ImGui::Button(("X###uc89sahciasuyr9wq" + std::to_string(i)).c_str(), { sz, sz })) {
						deleteIndex = i;
						break;
					}
					ImGui::SameLine();
					memset(textBuffers[i], 0, 256);
					strncpy(textBuffers[i], tag.c_str(), 256);
					if (ImGui::InputText(("###u8941uoidjscaoipu890"+std::to_string(i)).c_str(), textBuffers[i], 256)) {
						tag = std::string(textBuffers[i]);
					}
					i++;
				}
if (deleteIndex != -1) {
	VectorUtils::eraseItemAtIndex(agc.tags, deleteIndex);
}
if (ImGui::Button("Add")) {
	agc.tags.push_back("");
}

ImGui::TreePop();
			}
			if (killComponent) actor.killComponent<Components::ActorGroupComponent>();
		}
		if (allowRenderPrefab) if (actor.hasComponent<Components::PrefabComponent>()) {
			bool popupScriptEngine = false;
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::PrefabComponent).hash_code(), nodeFlags, "Prefab");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {
				auto& bpc = actor.getComponent<Components::PrefabComponent>();
				if (ImGuiEx::drawAssetBox("Prefab", AssetType::Prefab, bpc.prefab)) {
					SHARD3D_INFO_E("Reloading prefab '{0}'", bpc.prefab.getAsset());
					actor = context->getPrefabManager()->reloadPrefab(actor, bpc.prefab);
				}
				bool exists = false;
				sPtr<Prefab> prefab = context->getPrefabManager()->getPrefab(bpc.prefab);
				ImGui::Checkbox("Enable Script", &prefab->enableScript);
				if (!prefab->enableScript) ImGui::BeginDisabled();
				if (!ScriptEngine::doesAssemblyExist()) {
					ImGui::TextColored(ImVec4(1.0f, 0.0f, 1.0f, 1.0f), "Script Engine has not been set up!");
					if (ImGui::Button("Setup Script Engine")) {
						popupScriptEngine = true;
					}
					ImGui::BeginDisabled();
				}
				auto& name = prefab->scriptModule;
				const auto& actorClasses = ScriptEngine::getActorClasses();
				exists = ScriptEngine::doesActorClassExist(name);
				ImGui::Text("Script Module");
				if (!exists) ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.9f, 0.4, 0.7f, 1.0f));
				if (ImGui::BeginCombo("##comboscriptclasstypes", name.c_str())) {
					for (auto& class_ : actorClasses) {
						bool is_selected = (name == class_.first);
						if (ImGui::Selectable(class_.first.c_str(), is_selected))
							name = class_.first;
						if (is_selected)
							ImGui::SetItemDefaultFocus();
					}
					ImGui::EndCombo();
				}
				if (!exists) ImGui::PopStyleColor();

				if (!ScriptEngine::doesAssemblyExist())
					ImGui::EndDisabled();
				if (!prefab->enableScript) ImGui::EndDisabled();
				ImGui::TreePop();
			}
			if (killComponent) actor.killComponent<Components::PrefabComponent>();
			if (popupScriptEngine) ImGui::OpenPopup("Create Script Engine");
			return;
		}
		if (actor.hasComponent<Components::TerrainComponent>()) {
			auto& trc = actor.getComponent<Components::TerrainComponent>();
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::TerrainComponent).hash_code(), nodeFlags, "Terrain");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			
			if (open) {
				ImGui::TextColored(ImVec4(1.0, 0.3, 0.5, 1.0), "Warning, terrain position should be set at world origin!\nNot all features have been properly integrated.");
				if (ImGui::TreeNodeEx("Terrain Mesh Config", ImGuiTreeNodeFlags_DefaultOpen)) {
					int subdivisionType = log2(trc.tileSubdivisions) - 5;
					ImGui::Combo("Terrain Divisions (Rendering)", &subdivisionType, "32x32\00064x64\000128x128\000256x256");
					trc.tileSubdivisions = pow(2, subdivisionType + 5);
					int psubdivisionType = log2(trc.tilePhysicsSubdivisions) - 4;
					ImGui::Combo("Terrain Divisions (Physics)", &psubdivisionType, "16x16\00032x32\00064x64\000128x128\000256x256");
					trc.tilePhysicsSubdivisions = std::min(trc.tileSubdivisions, (uint32_t)pow(2, psubdivisionType + 4));
					glm::vec2 tiles = trc.tiles;
					ImGuiEx::drawTransform2DControl("Terrain Tiles", tiles, 4.0f, 0.5f);
					tiles = glm::max(tiles, glm::vec2(1.0f));
					trc.tiles = tiles;
					ImGui::DragFloat("Tile Extent", &trc.tileExtent, 1.0f, 1.0f, 1024.0f, "%.1f", ImGuiSliderFlags_Logarithmic);
					ImGui::Checkbox("Enable 2nd material map", &trc.allowDualMaterialMap);
					ImGui::Checkbox("Force 32 bit height map", &trc.force32BitHeightMap);
					glm::vec2 terrainExtent = { trc.tiles.x * trc.tileExtent , trc.tiles.y * trc.tileExtent };
					float tileDimX = 2.f * terrainExtent.x;
					float tileDimY = 2.f * terrainExtent.y;
					int tileCount = trc.tiles.x * trc.tiles.y;
					int textureFootprint = tileCount * trc.tileSubdivisions * trc.tileSubdivisions;
					int textureFootprintMips = 0;
					for (int i = 0; i < std::log2(trc.tileSubdivisions) - 3; i++) {
						int pow2 = 1 << int(std::log2(trc.tileSubdivisions) - i);
						textureFootprintMips += tileCount * pow2 * pow2;
					}

					int tileResX = trc.tiles.x * trc.tileSubdivisions;
					int tileResY = trc.tiles.y * trc.tileSubdivisions;
					ImGui::Text("Tile Batches (Max: %i)", tileCount);
					ImGui::Text("Texture Resolution: %i x %i", tileResX, tileResY);
					ImGui::Text("Texture Mip Levels: %i", std::log2(trc.tileSubdivisions) - 3);
					ImGui::Text("Terrain Size: %.1f m x %.1f m (%.1f m sq)", tileDimX, tileDimY, tileDimX * tileDimY);
					ImGui::Text("VRAM usage (Editor): Height map (%i kB), Material maps (%i kB)", textureFootprintMips * 4 / 1024, (trc.allowDualMaterialMap ? 2 : 1) * textureFootprintMips * 8 / 1024);
					ImGui::Text("VRAM usage (In Game): Height map (%i kB), Material maps (%i kB)", textureFootprintMips * (trc.force32BitHeightMap ? 4 : 2) / 1024, (trc.allowDualMaterialMap ? 2 : 1) * ((textureFootprint / 16) * 4) / 1024);
					ImGui::Text("RAM usage (Physics): Height map (%i kB), Material maps (%i kB)", (trc.tiles.x* trc.tiles.y* trc.tilePhysicsSubdivisions * trc.tilePhysicsSubdivisions) * 4 / 1024, ((textureFootprint / 16) * 4) / 1024);
					bool disableTerrainRecreate = false;

					if (tileResX > 16384 || tileResY > 16384) {
						disableTerrainRecreate = true;
						ImGui::TextColored(ImVec4(1.0, 0.3, 0.5, 1.0), "Terrain resolution exceeded maximum supported limit of 16384 pixels on at least 1 axis (%i x %i)!", tileResX, tileResY);
					} else if (tileResX > 8192 || tileResY > 8192) {
						ImGui::TextColored(ImVec4(0.8, 0.8, 0.3, 1.0), "Terrain resolution on one axis is larger than 8192 (%i x %i), memory usage may be increased", tileResX, tileResY);
					}
					ImGui::BeginDisabled(disableTerrainRecreate);
					if (ImGui::Button("Recreate Terrain")) {
						if (MessageDialogs::show(
							"This will clear any terrain painting applied. Are you sure you want to recreate the terrain?",
							"Recreate Terrain",
							MessageDialogs::OPTYESNO | MessageDialogs::OPTICONQUESTION
						) == MessageDialogs::RESYES) {
							trc.dirty = true;
						}
					}
					ImGui::EndDisabled();
					ImGui::TreePop();
				}
				if (ImGui::TreeNodeEx("Terrain Material Config", ImGuiTreeNodeFlags_DefaultOpen)) {
					ImGui::SliderInt("Max materials", &trc.maxMaterials, 1, trc.allowDualMaterialMap ? 8 : 4);
					if (trc.materials.size() < trc.maxMaterials) trc.materials.resize(trc.maxMaterials, AssetID::null());
					for (int i = 0; i < trc.maxMaterials; i++) {
						bool invalidMaterial = false;
						if (trc.materials[i]) {
							resourceSystem->loadMaterialRecursive(trc.materials[i], false);
							if (resourceSystem->retrieveMaterial(trc.materials[i])->getClass() != MaterialClass::Terrain) {
								invalidMaterial = true;
							}
						}
						if (invalidMaterial) ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.0, 0.3, 0.5, 1.0));
						ImGuiEx::drawAssetBox(("Terrain Material #" + std::to_string(i)).c_str(), AssetType::Material, trc.materials[i]);
						ImGui::Text("Invalid material asset!\nMaterial must be of class 'Terrain'");
						if (invalidMaterial) ImGui::PopStyleColor(ImGuiCol_Text);
					}
					ImGui::TreePop();
				}

				ImGui::TreePop();
			}
			if (killComponent) actor.killComponent<Components::TerrainComponent>();
		}
		if (actor.hasComponent<Components::Rigidbody3DComponent>()) {
			auto& rbc = actor.getComponent<Components::Rigidbody3DComponent>();
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::Rigidbody3DComponent).hash_code(), nodeFlags, "Rigidbody3D");
			if (ImGui::BeginDragDropTarget())
				if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SHARD3D.ASSEXP.PHYA")) {
					AssetID currentAsset = AssetID(std::string((char*)payload->Data));
					
					rbc.asset = AssetManager::loadPhysicsAsset(currentAsset);
				}

			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {
				if (actor.hasComponent<Components::TerrainComponent>()) {
					ImGui::Text("Terrain Actor");
					ImGuiEx::drawAssetBox("Physics Material", AssetType::PhysicsMaterialCollection, rbc.material);
					rbc.asset.colliderType = PhysicsColliderType::Terrain;
				} else {
					ImGuiEx::drawAssetBox("Physics Material", AssetType::PhysicsMaterial, rbc.material);
					if (actor.getMobility() == Mobility::Dynamic) {
						ImGui::Checkbox("Custom Center of Mass", &rbc.asset.enableCustomCOM);
						if (!rbc.asset.enableCustomCOM) ImGui::BeginDisabled();
						ImGuiEx::drawTransformControl("Center of mass", &rbc.asset.computedCOM, 0.f);
						if (!rbc.asset.enableCustomCOM) ImGui::EndDisabled();
						ImGuiEx::drawTransformControl("Linear Velocity", &rbc.linearVelocity, 0.f);
						ImGuiEx::drawTransformControl("Angular Velocity", &rbc.angularVelocity, 0.f);
						ImGui::DragFloat("Mass (kg)", &rbc.asset.mass, 0.1f, 0.001f, 1000.f, "%.3f", ImGuiSliderFlags_Logarithmic);
					} else {
						ImGui::Text("Actor is a static physics object");
					}
					ImGui::Checkbox("Enable Collision", &rbc.enableCollision);
					if (ImGui::Combo("Shape", (int*)&rbc.asset.colliderType, "Sphere\0Box\0Infinite Plane\0Capsule\0Cylinder\0ConvexHull Asset\0Mesh Asset")) {
					
					}
					switch ((int)rbc.asset.colliderType) {
					case(0):
						ImGui::DragFloat("Radius", &rbc.asset.sphereRadius, 0.05f, 0.0f, 999.f);
						break;
					case(1):
						ImGuiEx::drawTransformControl("Extents", &rbc.asset.boxExtent, 1.f);
						break;
					case(3):
					case(4):
						ImGui::DragFloat("Radius", &rbc.asset.cylindricalRadius, 0.05f, 0.0f, 10.f);
						ImGui::DragFloat("Length", &rbc.asset.cylindricalLength, 0.05f, 0.0f, 10.f);
						break;
					case(5):
						ImGui::Text("Warning: ConvexHull colliders are unstable and (may be) slow.\nConsider using trimesh colliders instead");
						break;
					case(6):
						ImGui::Text("Note: Trimesh colliders must have inverted normals to work");
						break;
					}
				}

				ImGui::TreePop();
			}
			if (killComponent) actor.killComponent<Components::Rigidbody3DComponent>();
		}
		if (actor.hasComponent<Components::PhysicsConstraintComponent>()) {
			auto& pcc = actor.getComponent<Components::PhysicsConstraintComponent>();
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::PhysicsConstraintComponent).hash_code(), nodeFlags, "Physics Constraint");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {
				ImGui::Combo("Constraint Type", (int*)&pcc.constraintType, "Fixed\0Ball\0Hinge\0DualHinge\0Slider\0AngularMotor\0LinearMotor\0Piston");
				glm::mat4 realTransform = actor.getTransform().transformMatrix;
				realTransform = glm::rotate(realTransform, -glm::half_pi<float>(), glm::vec3(1.f, 0.f, 0.f));

				if (pcc.constraintType == PhysicsConstraintType::DualHinge) {
					glm::vec3 deg = glm::degrees(pcc.perpendicularAxisOffsetRotation);
					ImGuiEx::drawTransformControl("Secondary Axis Offset", deg);
					pcc.perpendicularAxisOffsetRotation = deg;

					ImGui::DragFloat("Suspension Softness", &pcc.suspensionSoftness, 0.001, 0.0f, 1.0f, "%.9f");
				}

				std::array<entt::entity*, 2> constraintActors = { &pcc.actorA , &pcc.actorB };
				static std::array<const char*, 2> names = { "Constraint Node Actor A", "Constraint Node Actor B" };
				static std::array<const char*, 2> names2 = { "A", "B" };

				for (int i = 0; i < 2; i++) {
					const char* currentActorName = "NOACTOR";
					if (Actor actor = Actor(*constraintActors[i], context.get()); !actor.isInvalid()) {
						currentActorName = actor.getTag().c_str();
					}
					if (ImGui::BeginCombo(names[i], currentActorName)) {
						for (auto& physicsActor : context->registry.view<Components::Rigidbody3DComponent>()) {
							bool is_selected = (*constraintActors[i] == physicsActor);
							if (ImGui::Selectable(Actor(physicsActor, context.get()).getTag().c_str(), is_selected))
								*constraintActors[i] = physicsActor;
							if (is_selected) {
								ImGui::SetItemDefaultFocus();
							}
						}
						ImGui::EndCombo();
					}
					if (ImGui::BeginDragDropTarget()) {
						if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SHARD3D.ACTOR")) {
							Actor receiveActor = { (entt::entity)(*reinterpret_cast<uint32_t*>(payload->Data)), context.get() };
							*constraintActors[i] = receiveActor;
						}
						ImGui::EndDragDropTarget();
					}
				}
				

				ImGui::TreePop();
			}
			if (killComponent) actor.killComponent<Components::PhysicsConstraintComponent>();
		}
		if (actor.hasComponent<Components::BoxVolumeComponent>()) {
			auto& bvc = actor.getComponent<Components::BoxVolumeComponent>();
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::BoxVolumeComponent).hash_code(), nodeFlags, "Box Volume");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {
				ImGuiEx::drawTransformControl("Bounds", bvc.bounds, 1.f);
				bvc.bounds = glm::max(bvc.bounds, glm::vec3{ 0.01f });
				ImGui::DragFloat("Transition Distance", &bvc.transitionDistance, 0.01f, 0.f, 10.f);
				ImGui::TreePop();
			}
			if (killComponent) actor.killComponent<Components::BoxVolumeComponent>();
		}
		if (actor.hasComponent<Components::SphereVolumeComponent>()) {
			auto& svc = actor.getComponent<Components::SphereVolumeComponent>();
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::SphereVolumeComponent).hash_code(), nodeFlags, "Sphere Volume");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {
				ImGui::DragFloat("Radius", &svc.radius, 0.05f, 0.1f);
				ImGui::DragFloat("Transition Distance", &svc.transitionDistance, 0.01f, 0.f, 10.f);
			}
			if (killComponent) actor.killComponent<Components::BoxVolumeComponent>();
		}
		if (actor.hasComponent<Components::BoxReflectionCaptureComponent>()) {
			auto& brf = actor.getComponent<Components::BoxReflectionCaptureComponent>();
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::BoxReflectionCaptureComponent).hash_code(), nodeFlags, "Box Reflection Capture");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {
				ImGui::ColorEdit3("Tint", glm::value_ptr(brf.tint), ImGuiColorEditFlags_Float);
				ImGui::DragFloat("Intensity", &brf.intensity, 0.01f, 0.f, 100.f);
				{ // combo box code
					int cmbIndex{};
					switch (brf.resolution) {
					case (64): cmbIndex = 0; break;
					case (128): cmbIndex = 1; break;
					case (256): cmbIndex = 2; break;
					case (512): cmbIndex = 3; break;
					default: cmbIndex = 2;
					}
					ImGui::Combo("Resolution", &cmbIndex, "64px\000128px\000256px\000512px (EXPERIMENTAL)");
					switch (cmbIndex) {
					case (0): brf.resolution = 64; break;
					case (1): brf.resolution = 128; break;
					case (2): brf.resolution = 256; break;
					case (3): brf.resolution = 512; break;
					}
				}
				ImGui::Checkbox("Use Custom Cubemap", &brf.useAssetInsteadOfCapture);
				if (brf.useAssetInsteadOfCapture) { 
					if (ImGuiEx::drawAssetBox("Environment", AssetType::TextureCube, brf.reflectionAsset)) {
						SHARD3D_INFO_E("Reloading cubemap '{0}'", brf.reflectionAsset.getAsset());
						resourceSystem->loadTextureCube(brf.reflectionAsset);
					}
				} else {
					if (ImGui::Button("Capture")) {
						if (brf.resolution == 1024) if (MessageDialogs::show("Reflection capture resolution will be capturing at 4x super sampled 1024x1024x6. This will be using approx 6GB+ vram. Are you sure you want to capture?",
							"Warning",
							MessageDialogs::OPTYESNO | MessageDialogs::OPTICONEXCLAMATION
						) == MessageDialogs::RESNO) goto skip;
						{
							LPVOID dialog;
							HRESULT res = CoCreateInstance(CLSID_ProgressDialog, nullptr, CLSCTX_INPROC_SERVER, IID_IProgressDialog, &dialog);
							reinterpret_cast<IProgressDialog*>(dialog)->SetTitle(L"Reflection Capture");
							reinterpret_cast<IProgressDialog*>(dialog)->SetLine(1, L"Building Reflection...", false, nullptr);
							reinterpret_cast<IProgressDialog*>(dialog)->StartProgressDialog(nullptr, nullptr, PROGDLG_NOMINIMIZE | PROGDLG_NOCANCEL | PROGDLG_MARQUEEPROGRESS, nullptr);

							auto newTime = std::chrono::high_resolution_clock::now();
							vkDeviceWaitIdle(device->device());
							StaticReflectionCapturePass capturer = StaticReflectionCapturePass(
								actor.getTransform().getTranslationWorld(), context, *device, resourceSystem, *globalSetLayout, *sceneSetLayout, *riggedSkeletonSetLayout, *terrainSetLayout, static_cast<float>(brf.resolution), 4.f
							);
							if (!std::filesystem::exists(context->bakedDataDirectory.getAbsolute())) {
								std::filesystem::create_directories(context->bakedDataDirectory.getAbsolute());
							}
							AssetID asset = context->bakedDataDirectory.getAsset() + "/brfcpt_" + std::to_string(actor.getScopedID());
							capturer.capture(asset.getAbsolute() + ".s3dbak", asset.getAbsolute() + "_irr.s3dbak");
							PackageInvocationIndex pii{};
							pii.resource = ResourceType::TextureCube;
							AssetID result0{ AssetID::null() };
							result0 = asset.getAsset() + ".s3dbak" + ".s3dasset";
							resourceSystem->loadBakedReflectionCubeAsset(result0, true);
							AssetID result1{ AssetID::null() };
							result1 = asset.getAsset() + "_irr.s3dbak" + ".s3dasset";
							resourceSystem->loadBakedReflectionCubeAsset(result1, true);
							brf.reflectionAsset = result0;
							brf.irradianceAsset = result1;
							SHARD3D_INFO("Duration of capturing scene: {0} ms",
								std::chrono::duration<float, std::chrono::milliseconds::period>
								(std::chrono::high_resolution_clock::now() - newTime).count());
							reinterpret_cast<IProgressDialog*>(dialog)->StopProgressDialog();
						}
					}
				skip:;
				}
				
				ImGui::TreePop();
			}
			if (killComponent) actor.killComponent<Components::BoxReflectionCaptureComponent>();
		}

		if (actor.hasComponent<Components::PlanarReflectionComponent>()) {
			auto& prf = actor.getComponent<Components::PlanarReflectionComponent>();
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::PlanarReflectionComponent).hash_code(), nodeFlags, "Planar Reflection Component");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {
				ImGui::ColorEdit3("Tint", glm::value_ptr(prf.tint), ImGuiColorEditFlags_Float);
				ImGui::DragFloat("Intensity", &prf.intensity, 0.01f, 0.f, 100.f);
				{ // combo box code
					int cmbIndex{};
					switch (prf.reflectionFormat) {
					case(VK_FORMAT_R16G16B16A16_SFLOAT):
						cmbIndex = 0;
						break;
					case(VK_FORMAT_R8G8B8A8_UNORM):
						cmbIndex = 1;
						break;
					case(VK_FORMAT_R5G6B5_UNORM_PACK16):
						cmbIndex = 2;
						break;
					case(VK_FORMAT_B10G11R11_UFLOAT_PACK32):
						cmbIndex = 3;
						break;
					}
					ImGui::Combo("HDR Format", &cmbIndex, "RGBA_F16 (HDR)\000RGBA_U8 (SDR)\000RGB_U5/6/5 (SDR) (EXPERIMENTAL)\000BGR_F10/11/11 (HDR) (EXPERIMENTAL)");
					switch (cmbIndex) {
					case(0):
						prf.reflectionFormat = VK_FORMAT_R16G16B16A16_SFLOAT;
						break;
					case(1):
						prf.reflectionFormat = VK_FORMAT_R8G8B8A8_UNORM;
						break;
					case(2):
						prf.reflectionFormat = VK_FORMAT_R5G6B5_UNORM_PACK16;
						break;
					case(3):
						prf.reflectionFormat = VK_FORMAT_B10G11R11_UFLOAT_PACK32;
						break;
					}
				}
				ImGui::DragFloat("Resolution Scale", &prf.resolutionMultiplier, 0.01f, 0.1f, 2.0f);
				ImGui::DragFloat("Clip Offset", &prf.clipOffset, 0.001f, -1.0f, 1.0f);
				ImGui::DragFloat("Max Height", &prf.maxInfluenceHeight, 0.01f, 0.f, 100.f);
				ImGui::DragFloat("Transition Distance", &prf.transitionDistance, 0.01f, 0.f, 100.f);
				ImGui::DragFloat("Normal Distortion Factor", &prf.distortDuDvFactor, 1.0f, 0.f, 100.f);
				ImGui::DragFloat("Distortion Delta Limit", &prf.distortionDeltaLimit, 0.001f, 0.f, 1.f, "%.4f", ImGuiSliderFlags_Logarithmic);

				ImGui::Checkbox("Render Irradiance", &prf.irradiance);
				ImGui::DragFloat("Irradiance Sample Radius", &prf.irradianceSampleRadius, 0.01, 0.01, 0.5f);
				if (const PlanarReflectionInstance* reflectionInst = renderContext->getPlanarReflectionSystem()->getReflection(actor)) {
					ImGui::Text("Resource Size: %i kB", reflectionInst->getEstimatedResourceSize());
					ImGui::Text("Draw Time CPU: %f ms", reflectionInst->getDrawTimeCPU());
					ImGui::Text("Draw Time GPU: %f ms", reflectionInst->getDrawTimeGPU());
					ImGui::Text("Irradiance Time GPU: %f ms", reflectionInst->getIrradianceTimeGPU());
				} else {
					ImGui::Text("unavailable...");
					ImGui::Text("unavailable...");
					ImGui::Text("unavailable...");
				}
				ImGui::TreePop();
			}
			if (killComponent) actor.killComponent<Components::PlanarReflectionComponent>();
		}

		if (actor.hasComponent<Components::BoxAmbientOcclusionVolumeComponent>()) {
			auto& aov = actor.getComponent<Components::BoxAmbientOcclusionVolumeComponent>();
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::BoxAmbientOcclusionVolumeComponent).hash_code(), nodeFlags, "Ambient Occlusion Volume");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {
				ImGui::ColorEdit3("Tint", glm::value_ptr(aov.tint), ImGuiColorEditFlags_Float);
				ImGui::DragFloat("Min Occlusion", &aov.minOcclusion, 0.00f, 0.f, 1.f);
				ImGui::TreePop();
			}
			if (killComponent) actor.killComponent<Components::BoxAmbientOcclusionVolumeComponent>();
		}
		if (actor.hasComponent<Components::SkyLightComponent>()) {
			auto& skl = actor.getComponent<Components::SkyLightComponent>();
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::SkyLightComponent).hash_code(), nodeFlags, "Sky Light");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {
				ImGui::ColorEdit3("Sky Light Tint", glm::value_ptr(skl.skyTint));
				ImGui::DragFloat("Sky Light Intensity", &skl.skyIntensity, 0.01f, 0.f, 100.f);
				ImGui::Combo("reflectionType", &skl.reflectionType, "Skybox\0Sky Atmosphere");
				ImGui::BeginDisabled();
				ImGui::Checkbox("reflectRayleighScattering", &skl.reflectRayleighScattering);
				{ // combo box code
					int cmbIndex{};
					switch (skl.skyAtmosphereReflectionResolution) {
					case (64): cmbIndex = 0; break;
					case (128): cmbIndex = 1; break;
					case (256): cmbIndex = 2; break;
					case (512): cmbIndex = 3; break;
					case (1024): cmbIndex = 4; break;
					case (2048): cmbIndex = 5; break;
					default: cmbIndex = 3;
					}
					ImGui::Combo("Resolution", &cmbIndex, "64px\000128px\000256px\000512px\0001024px (ULTRA)\0002048px (CINEMATIC)");
					switch (cmbIndex) {
					case (0): skl.skyAtmosphereReflectionResolution = 64; break;
					case (1): skl.skyAtmosphereReflectionResolution = 128; break;
					case (2): skl.skyAtmosphereReflectionResolution = 256; break;
					case (3): skl.skyAtmosphereReflectionResolution = 512; break;
					case (4): skl.skyAtmosphereReflectionResolution = 1024; break;
					case (5): skl.skyAtmosphereReflectionResolution = 2048; break;
					}
				}
				ImGui::EndDisabled();

				if (skl.reflectionType == 0) {
					const char* currentActorName = "NOACTOR";
					if (Actor actor = Actor(skl.skyboxActor, context.get()); !actor.isInvalid()) {
						currentActorName = actor.getTag().c_str();
					}
					if (ImGui::BeginCombo("Skybox Actor", currentActorName)) {
						for (auto& skyboxActor : context->registry.view<Components::SkyboxComponent>()) {
							bool is_selected = (skl.skyAtmosphereActor == skyboxActor);
							if (ImGui::Selectable(Actor(skyboxActor, context.get()).getTag().c_str(), is_selected))
								skl.skyboxActor = skyboxActor;
							if (is_selected) {
								ImGui::SetItemDefaultFocus();
							}
						}
						ImGui::EndCombo();
					}
					if (ImGui::BeginDragDropTarget()) {
						if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SHARD3D.ACTOR")) {
							Actor receiveActor = { (entt::entity)(*reinterpret_cast<uint32_t*>(payload->Data)), context.get() };
							skl.skyboxActor = receiveActor;
						}
						ImGui::EndDragDropTarget();
					}
				} else if (skl.reflectionType == 1) {
					const char* currentActorName = "NOACTOR";
					if (Actor actor = Actor(skl.skyAtmosphereActor, context.get()); !actor.isInvalid()) {
						currentActorName = actor.getTag().c_str();
					}
					if (ImGui::BeginCombo("Sky Atmosphere Actor", currentActorName)) {
						for (auto& skyAtmosphereActor : context->registry.view<Components::SkyAtmosphereComponent>()) {
							bool is_selected = (skl.skyAtmosphereActor == skyAtmosphereActor); 
							if (ImGui::Selectable(Actor(skyAtmosphereActor, context.get()).getTag().c_str(), is_selected))
								skl.skyAtmosphereActor = skyAtmosphereActor;
							if (is_selected) {
								ImGui::SetItemDefaultFocus();
							}
						}
						ImGui::EndCombo();
					}
					if (ImGui::BeginDragDropTarget()) {
						if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SHARD3D.ACTOR")) {
							Actor receiveActor = { (entt::entity)(*reinterpret_cast<uint32_t*>(payload->Data)), context.get() };
							skl.skyAtmosphereActor = receiveActor;
						}
						ImGui::EndDragDropTarget();
					}
				}
				ImGui::TreePop();
			}
			if (killComponent) actor.killComponent<Components::SkyLightComponent>();
		}
		if (actor.hasComponent<Components::SkyboxComponent>()) {
			auto& skb = actor.getComponent<Components::SkyboxComponent>();
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::SkyboxComponent).hash_code(), nodeFlags, "Skybox");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {	
				if (ImGuiEx::drawAssetBox("Environment", AssetType::TextureCube, skb.environment)) {
					SHARD3D_INFO_E("Reloading cubemap '{0}'", skb.environment.getAsset());
					resourceSystem->loadTextureCube(skb.environment);
				}
				if (ImGuiEx::drawAssetBox("Irradiance", AssetType::TextureCube, skb.irradiance)) {
					SHARD3D_INFO_E("Reloading cubemap '{0}'", skb.irradiance.getAsset());
					resourceSystem->loadTextureCube(skb.irradiance);
				}
				if (ImGui::Button("Generate Irradiance from Cubemap")) {

					LPVOID dialog;
					HRESULT res = CoCreateInstance(CLSID_ProgressDialog, nullptr, CLSCTX_INPROC_SERVER, IID_IProgressDialog, &dialog);
					reinterpret_cast<IProgressDialog*>(dialog)->SetTitle(L"Irradiance Baking");
					reinterpret_cast<IProgressDialog*>(dialog)->SetLine(1, L"Baking Irradiance Map...", false, nullptr);
					reinterpret_cast<IProgressDialog*>(dialog)->StartProgressDialog(nullptr, nullptr, PROGDLG_NOMINIMIZE | PROGDLG_NOCANCEL | PROGDLG_MARQUEEPROGRESS, nullptr);

					S3DShaderSpecialization specialization{};
					const float sampleDelta = 0.025f;
					specialization.addConstant(0, sampleDelta);
					PostProcessingEffect* envIrradianceGen = new PostProcessingEffect(
						*device,
						{ 64, 64 },
						S3DShader(ShaderType::Fragment, "resources/shaders/processing/envirrgen.frag", "misc", specialization),
						{ { resourceSystem->retrieveTextureCube(skb.environment)->getSampler(),resourceSystem->retrieveTextureCube(skb.environment)->getImageView(), resourceSystem->retrieveTextureCube(skb.environment)->getImageLayout() } },
						VK_FORMAT_R32_UINT,
						VK_IMAGE_VIEW_TYPE_CUBE,
						6,
						1
					);
					{
						static glm::vec3 orientations2[6]{
							{ 0.f, 0.f, glm::half_pi<float>(), },	// right
							{ 0.f, 0.f, -glm::half_pi<float>() },	// left
							{ glm::half_pi<float>(), 0.f, 0.f },	// down
							{ -glm::half_pi<float>(), 0.f, 0.f },	// up
							{ 0.f, 0.f, 0.f },	// front
							{ 0.f, 0.f, glm::pi<float>() }		// back
						};
						VkCommandBuffer commandBuffer = device->beginSingleTimeCommands();
						for (int i = 0; i < 6; i++) {
							S3DCamera camera{};
							camera.setViewYXZ({}, orientations2[i]);
							glm::mat4 mvp = camera.getView();
							envIrradianceGen->render(commandBuffer, mvp, i);
						}
						device->endSingleTimeCommands(commandBuffer);
					}

					VkBuffer copyBuffer;
					VkDeviceMemory copyBufferMemory;

					size_t imageSize = 64 * 64 * 4 * 6;
					device->createBuffer(
						imageSize,
						VK_BUFFER_USAGE_TRANSFER_DST_BIT,
						VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
						copyBuffer,
						copyBufferMemory
					);
					void* imageData;
					vkMapMemory(device->device(), copyBufferMemory, 0, imageSize, 0, &imageData);
					VkCommandBuffer commandBuffer = device->beginSingleTimeCommands();
					device->copyImageToBuffer(commandBuffer, envIrradianceGen->getAttachment()->getImage(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, copyBuffer, 64, 64, 6, 0);

					device->endSingleTimeCommands(commandBuffer); 
					TextureImportInfo importInfo{};
					importInfo.cubeFormat = TextureCubeFormat::Cube;
					importInfo.isCubemap = true;
					TextureLoadInfo loadInfo{};
					loadInfo.channels = TextureChannels::RGB;
					loadInfo.format = TextureFormat::FloatingEBGR;
					loadInfo.filter = TextureFilter::Linear;
					loadInfo.compression = TextureCompression::Lossless;
					loadInfo.colorSpace = TextureColorSpace::Linear;
					skb.irradiance = AssetManager::importTextureCube(imageData, imageSize, {64,64},
						std::string(skb.environment.getAbsolute().substr(0, skb.environment.getAbsolute().find_last_of(".")) + "_irradiance"),
						resourceSystem, importInfo, loadInfo);
					resourceSystem->loadTextureCube(skb.irradiance);

					vkUnmapMemory(device->device(), copyBufferMemory);
					vkDestroyBuffer(device->device(), copyBuffer, nullptr);
					vkFreeMemory(device->device(), copyBufferMemory, nullptr);
					delete envIrradianceGen;
					reinterpret_cast<IProgressDialog*>(dialog)->StopProgressDialog();
				}
				ImGui::ColorEdit3("Skybox tint", glm::value_ptr(skb.tint));
				ImGui::DragFloat("Skybox intensity", &skb.intensity, 0.01f, 0.f, 100.f);
				ImGui::TreePop();
			}
			if (killComponent) actor.killComponent<Components::SkyboxComponent>();
		}
		if (actor.hasComponent<Components::SkyAtmosphereComponent>()) {
			auto& ska = actor.getComponent<Components::SkyAtmosphereComponent>();
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::SkyAtmosphereComponent).hash_code(), nodeFlags, "Sky Atmosphere");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {
				ImGui::DragFloat("Height (km)", &ska.height, 0.1f, 6371.f, 7000.f);
				//ImGui::DragFloat("Atmosphere Height (km)", &ska.height, 0.1f, 6371.f, 7000.f);
				ImGui::DragFloat("mieScattering", &ska.mieScattering, 0.00001f, 0.f, 0.01f, "%.6f", ImGuiSliderFlags_Logarithmic);
				ImGui::DragFloat("rayleighScattering", &ska.rayleighScattering, 0.00001f, 0.f, 0.05f, "%.6f", ImGuiSliderFlags_Logarithmic);
				ImGui::DragFloat("sunIntensity", &ska.sunIntensity, 0.1f, 1.f, 40.0f);
				ImGui::TreePop();
			}
			if (killComponent) actor.killComponent<Components::SkyAtmosphereComponent>();
		}
		

		if (actor.hasComponent<Components::ExponentialFogComponent>()) {
			auto& efc = actor.getComponent<Components::ExponentialFogComponent>();
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::ExponentialFogComponent).hash_code(), nodeFlags, "Exponential Fog");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {
				ImGui::ColorEdit3("Fog Color", glm::value_ptr(efc.color));
				ImGui::DragFloat("Fog Density", &efc.density, 0.00001f, 0.0001f, 0.100f, "%.5f", ImGuiSliderFlags_Logarithmic);
				//ImGui::DragFloat("Fog Falloff", &efc.falloff, 0.005f, 0.1f, 8.000f, "%.3f", ImGuiSliderFlags_Logarithmic);
				//ImGui::DragFloat("Fog Strength", &efc.strength, 0.01f, 0.f, 1.0f);
				ImGui::DragFloat("Sun Scattering Power", &efc.sunScatteringPow, 0.05f, 2.0f, 16.0f);
				ImGui::Combo("Density Bias", &efc.upDownBias, "Up\000Down\000Omni");
				ImGui::TreePop();
			}
			if (killComponent) actor.killComponent<Components::ExponentialFogComponent>();
		}

		if (actor.hasComponent<Components::Mesh3DComponent>()) {
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::Mesh3DComponent).hash_code(), nodeFlags, "Mesh");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {
				auto& mcm = actor.getComponent<Components::Mesh3DComponent>();
				ImGui::Checkbox("Cast Shadows", &mcm.castShadows);
				ImGui::BeginDisabled(!ProjectSystem::getEngineSettings().renderer.enableDeferredDecals);
				ImGui::Checkbox("Receive Decals", &mcm.receiveDecals);
				ImGui::EndDisabled();
				if (ImGuiEx::drawAssetBox("Mesh File", AssetType::Model3D, mcm.asset)) {
					SHARD3D_INFO_E("Reloading mesh '{0}'", mcm.asset.getAsset());
					resourceSystem->loadMesh(mcm.asset);
					actor.getComponent<Components::Mesh3DComponent>() = Components::Mesh3DComponent(resourceSystem, mcm.asset);
				}
				for (int i = 0; i < actor.getComponent<Components::Mesh3DComponent>().getMaterials().size(); i++) {
					ImGui::Text("%i# %s", i, resourceSystem->retrieveMesh(actor.getComponent<Components::Mesh3DComponent>().asset)->materialSlots[i].c_str());
					AssetID asset = actor.getComponent<Components::Mesh3DComponent>().getMaterials()[i];
					bool invalid = false;
					invalid = Material::discoverMaterialClass(asset) != MaterialClass::Surface;
					if (ImGuiEx::drawAssetBox(("##surfacematerial__NO" + std::to_string(i)).c_str(), AssetType::Material, asset)) {
						invalid = Material::discoverMaterialClass(asset) != MaterialClass::Surface;
						if (!invalid) {
							actor.getComponent<Components::Mesh3DComponent>().setMaterial(asset, i);
							resourceSystem->loadMaterialRecursive(asset);
							actor.getComponent<Components::Mesh3DComponent>().validate(resourceSystem);
						}
					}
					if (invalid) {
						ImGui::TextColored(ImVec4(1.0, 0.3, 0.5, 1.0), "Invalid material! Material %i must be of class 'Surface'", i);
					}
				}
				ImGui::TreePop();
			}
			if (killComponent) {
				actor.killComponent<Components::Mesh3DComponent>();
			}
		}

		if (actor.hasComponent<Components::SkeletonRigComponent>()) {
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::SkeletonRigComponent).hash_code(), nodeFlags, "Skeleton");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {
				auto& skc = actor.getComponent<Components::SkeletonRigComponent>();
				ImGui::Checkbox("Enable Bones", &skc.enableBones);
				ImGui::Checkbox("Enable Morph Targets", &skc.enableMorphTargets);

				if (ImGuiEx::drawAssetBox("Skeleton File", AssetType::SkeletonRig, skc.asset)) {
					SHARD3D_INFO_E("Reloading skeleton '{0}'", skc.asset.getAsset());
					resourceSystem->loadSkeleton(skc.asset);
					actor.getComponent<Components::SkeletonRigComponent>().boneTransforms.clear();
					Skeleton* skeleton = resourceSystem->retrieveSkeleton(skc.asset);
					actor.getComponent<Components::SkeletonRigComponent>().boneTransforms.resize(skeleton->boneNames.size());
					actor.getComponent<Components::SkeletonRigComponent>().morphTargetWeights.resize(skeleton->morphTargetNames.size());
					if (actor.hasComponent<Components::Mesh3DComponent>()) {
						actor.getComponent<Components::Mesh3DComponent>().dirty = true;
					}
				}
				for (int i = 0; i < actor.getComponent<Components::SkeletonRigComponent>().morphTargetWeights.size(); i++) {
					ImGui::DragFloat(resourceSystem->retrieveSkeleton(skc.asset)->morphTargetNames[i].c_str(), 
						&actor.getComponent<Components::SkeletonRigComponent>().morphTargetWeights[i], 0.01f, 0.f, 1.f);
				}
				for (int i = 0; i < actor.getComponent<Components::SkeletonRigComponent>().boneTransforms.size(); i++) {
					if (ImGui::TreeNodeEx(resourceSystem->retrieveSkeleton(skc.asset)->boneNames[i].c_str(), ImGuiTreeNodeFlags_DefaultOpen)) {
						auto& tc = actor.getComponent<Components::SkeletonRigComponent>().boneTransforms[i];
						glm::vec3 tran = tc.getTranslation();
						ImGuiEx::drawTransformControl("Translation", tran, 0.f);
						tc.setTranslation(tran);

						//glm::vec3 rota = glm::degrees(TransformMath::decomposeEulerYXZ(glm::toMat3()));
						//ImGuiEx::drawTransformControl("Rotation", rota, 0.f, 0.1f);
						//tc.setRotation(glm::toQuat(TransformMath::YXZ(glm::radians(rota))));
						//glm::vec3 rota = glm::degrees(tc.getRotation());
						//ImGuiEx::drawTransformControl("Rotation", rota, 0.f, 0.1f);
						//tc.setRotation(glm::radians(rota));


						glm::vec3 scal = tc.getScale();
						ImGuiEx::drawTransformControl("Scale", scal, 1.f);
						tc.setScale(scal);
						ImGui::TreePop();
					}
				}
				ImGui::TreePop();
			}
			if (killComponent) {
				actor.killComponent<Components::SkeletonRigComponent>();
			}
		}
		if (actor.hasComponent<Components::DeferredDecalComponent>()) {
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::DeferredDecalComponent).hash_code(), nodeFlags, "Deferred Decal");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {
				auto& ddc = actor.getComponent<Components::DeferredDecalComponent>();
				ImGui::DragFloat("Opacity", &ddc.opacity, 0.01f, 0.0f, 1.0f);
				ImGui::DragInt("Priority", (int*)&ddc.priority, 0.5f, 0, INT32_MAX);
				if (ImGuiEx::drawAssetBox("Decal Material", AssetType::Material, ddc.material)) {
					AssetID materialAsset = ddc.material;
					SHARD3D_INFO_E("Reloading material '{0}'", materialAsset.getAsset());
					actor.killComponent<Components::DeferredDecalComponent>();
					actor.addComponent<Components::DeferredDecalComponent>().material = materialAsset;
					resourceSystem->loadMaterial(materialAsset);
				}
				if (Material::discoverMaterialClass(ddc.material) != MaterialClass::DeferredDecal) {
					ImGui::TextColored(ImVec4(1.0, 0.3, 0.5, 1.0), "Invalid material! Materials must be of class 'Deferred Decal'");
				}
				if (!actor.hasComponent<Components::BoxVolumeComponent>()) {
					ImGui::TextColored(ImVec4(1.0, 0.3, 0.5, 1.0), "Deferred Decals require a Box Volume Component!");
				}
				if (actor.getMobility() == Mobility::Dynamic) {
					ImGui::TextColored(ImVec4(0.8, 0.8, 0.3, 1.0), "Mobility is set to dynamic, high quantity of decals are not recommended!");
					ImGui::TextColored(ImVec4(0.8, 0.8, 0.3, 1.0), "Mobility is set to dynamic, some features may be buggy");
				}
				ImGui::TreePop();
			}
			if (killComponent) {
				actor.killComponent<Components::DeferredDecalComponent>();
			}
		}
		//if (actor.hasComponent<Components::BillboardComponent>()) {
		//	bool open = ImGui::TreeNodeEx((void*)typeid(Components::BillboardComponent).hash_code(), nodeFlags, "Billboard");
		//	ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
		//	bool killComponent = false;
		//	if (ImGui::BeginPopup("KillComponent")) {
		//		if (ImGui::MenuItem("Remove Component")) {
		//			killComponent = true;
		//			ImGui::CloseCurrentPopup();
		//		}
		//		ImGui::EndPopup();
		//	}
		//	if (open) {
		//		auto& rfile = actor.getComponent<Components::BillboardComponent>().asset.getFileRef();
		//	
		//		char fileBuffer[256];
		//		memset(fileBuffer, 0, 256);
		//		strncpy(fileBuffer, rfile.c_str(), 256);
		//	
		//		if (ImGui::InputText("Texture File", fileBuffer, 256)) {
		//			rfile = std::string(fileBuffer);
		//		}
		//		if (ImGui::BeginDragDropTarget()) {
		//			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SHARD3D.ASSEXP.TEX")) {
		//				rfile = std::string((char*)payload->Data);
		//			}
		//		}
		//		if (ImGui::Button("Load Texture")) {
		//			std::ifstream ifile(ProjectSystem::getAssetLocation() + fileBuffer);
		//			if (ifile.good()) {
		//				SHARD3D_INFO_E("Reloading texture '{0}'", rfile);
		//				actor.getComponent<Components::BillboardComponent>().asset = AssetID(rfile);
		//				resourceSystem->loadTexture(actor.getComponent<Components::BillboardComponent>().asset);
		//			}
		//			else SHARD3D_WARN_E("File '{0}' does not exist!", fileBuffer);
		//		}
		//		ImGui::TreePop();
		//	}
		//	if (killComponent) actor.killComponent<Components::BillboardComponent>();
		//}
		if (actor.hasComponent<Components::AudioComponent>()) {
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::AudioComponent).hash_code(), nodeFlags, "Audio");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {
				auto& rfile = actor.getComponent<Components::AudioComponent>().file;
				char fileBuffer[256];
				memset(fileBuffer, 0, 256);
				strncpy(fileBuffer, rfile.c_str(), 256);
				if (ImGui::InputText("Audio File", fileBuffer, 256)) {
					rfile = std::string(fileBuffer);
				}
				if (ImGui::TreeNode("Audio Properties")) {
					ImGui::DragFloat("Volume", &actor.getComponent<Components::AudioComponent>().properties.volume, 0.01f, 0.f, 10.f);
					ImGui::DragFloat("Pitch", &actor.getComponent<Components::AudioComponent>().properties.pitch, 0.01f, 0.f, 2.f);
					ImGui::TreePop();
				}
				ImGui::TreePop();
			}
			if (killComponent) actor.killComponent<Components::AudioComponent>();
		}
		if (actor.hasComponent<Components::CameraComponent>()) {
			auto& cmc = actor.getComponent<Components::CameraComponent>();
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::CameraComponent).hash_code(), nodeFlags, "Camera");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {
				bool previewing = context->getPossessedCameraActor() == actor;
				if (ImGui::Button(previewing ? "Stop Preview" : "Preview")) {
					if (previewing) {
						context->setPossessedCameraActor(context->getActorFromUUID(0, 0));
					}
					else {
						context->setPossessedCameraActor(actor);
					}
				}
				int projType = (int)cmc.projectionType;
				ImGui::Combo("Projection Type", &projType, "Orthographic\0Perspective");
				cmc.projectionType = (ProjectionType)projType;
				
				bool enableFov = false;
				if (cmc.projectionType == ProjectionType::Perspective) enableFov = true;
				ImGui::BeginDisabled(!enableFov); ImGui::DragFloat("FOV", &cmc.fov, 0.1f, 10.f, 180.f); ImGui::EndDisabled();
				ImGui::DragFloat("Near Clip Plane", &cmc.nearClip, 0.001f, 0.05f, 1.f);
				ImGui::BeginDisabled(ProjectSystem::getEngineSettings().renderer.infiniteFarZ);
				static float inf = INFINITY;
				ImGui::DragFloat("Far Clip Plane", ProjectSystem::getEngineSettings().renderer.infiniteFarZ ? &inf : &cmc.farClip, 1.f, 16.f, 8192.f);
				ImGui::EndDisabled();
				ImGui::TreePop();
			}
			if (killComponent) actor.killComponent<Components::CameraComponent>();
		}
		if (actor.hasComponent<Components::PostFXComponent>()) {
			auto& pxc = actor.getComponent<Components::PostFXComponent>();
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::PostFXComponent).hash_code(), nodeFlags, "PostFX");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {
				ImGui::Text("Post Processing");
				if (ImGui::TreeNode("HDR")) {
					ImGui::DragFloat("Exposure", &pxc.postfx.hdr.exposure, 0.01f, 0.f, 10.f);
					bool noWhiteLim = true;
					switch (pxc.postfx.hdr.toneMappingAlgorithm) {
					case(PostFXData::HDR::ToneMappingAlgorithm::REINHARD_EXTENDED_TONE_MAPPING):
						noWhiteLim = false;
					}
					if (noWhiteLim) ImGui::BeginDisabled();
					ImGui::DragFloat("White Limit", &pxc.postfx.hdr.lim, 0.01f, 0.f, 10.f);
					if (noWhiteLim) ImGui::EndDisabled();

					ImGui::Combo("Tone Mapping Algorithm", reinterpret_cast<int*>(&pxc.postfx.hdr.toneMappingAlgorithm), 
						"None\0Reinhard\0Reinhard Extended\0Exponential\0Reinhard Luma\0Hable\0ACES (Filmic)");
					ImGui::TreePop();
				}
				if (ImGui::TreeNode("Colour Grading")) {
					ImGui::DragFloat("Contrast", &pxc.postfx.grading.contrast, 0.01f, 0.f, 10.f);
					ImGui::DragFloat("Saturation", &pxc.postfx.grading.saturation, 0.01f, 0.f, 10.f);
					ImGui::DragFloat("Gain", &pxc.postfx.grading.gain, 0.01f, -1.0f, 1.0f);
					ImGui::DragFloat("Temperature (K)", &pxc.postfx.grading.temperature, 100.0f, 1000.0f, 12000.0f, "%.0f");
					ImGui::DragFloat("Hue Shift", &pxc.postfx.grading.hueShift, 1.0f, -360.0f, 360.0f, "%.0f");

					ImGui::ColorEdit3("Shadows", (float*)&pxc.postfx.grading.shadows, ImGuiColorEditFlags_Float);
					ImGui::ColorEdit3("Midtones", (float*)&pxc.postfx.grading.midtones, ImGuiColorEditFlags_Float);
					ImGui::ColorEdit3("Highlights", (float*)&pxc.postfx.grading.highlights, ImGuiColorEditFlags_Float);
					ImGui::TreePop();
				}
				if (ImGui::TreeNode("Bloom")) {
					ImGui::DragFloat("Threshold", &pxc.postfx.bloom.threshold, 0.01f, 0.f, 2.f);
					ImGui::DragFloat("Strength", &pxc.postfx.bloom.strength, 0.01f, 0.f, 2.f);
					ImGui::DragFloat("Knee", &pxc.postfx.bloom.knee, 0.01f, 0.f, 1.f);
					ImGui::BeginDisabled();
					ImGui::Checkbox("Enabled", &pxc.postfx.bloom.enable);
					ImGui::EndDisabled();
					ImGui::TreePop();
				}
				if (ImGui::TreeNode("Motion Blur")) {
					ImGui::DragFloat("Shutter Speed (FPS)", &pxc.postfx.motionBlur.targetFramerate, 1.0f, 15.f, 120.f);
					ImGui::BeginDisabled();
					ImGui::Checkbox("Enabled", &pxc.postfx.motionBlur.enable);
					ImGui::EndDisabled();
					ImGui::TreePop();
				}

				ImGui::Text("Renderer Features");
				if (ImGui::TreeNode("SSAO")) {
					ImGui::DragFloat("Radius", &pxc.postfx.ssao.radius, 0.01f, 0.f, 1.f);
					ImGui::DragFloat("Intensity", &pxc.postfx.ssao.intensity, 0.01f, 0.f, 1.0f);
					ImGui::DragFloat("Power", &pxc.postfx.ssao.power, 0.01f, 1.f, 8.0f);
					ImGui::DragFloat("Bias", &pxc.postfx.ssao.bias, 0.001f, 0.f, 0.1f);
					ImGui::BeginDisabled();
					ImGui::Checkbox("Enabled", &pxc.postfx.ssao.enable);
					ImGui::EndDisabled();
					ImGui::TreePop();
				}
				if (ImGui::TreeNode("Light Propagation Volume")) {
					ImGui::ColorEdit3("Tint", glm::value_ptr(pxc.postfx.lightPropagationVolume.boost), ImGuiColorEditFlags_Float);
					ImGui::DragFloat("Boost", &pxc.postfx.lightPropagationVolume.boost.w, 20.0f, 0.f, 4000.f);
					ImGui::DragFloat3("Extent", glm::value_ptr(pxc.postfx.lightPropagationVolume.extent), 0.01f, 0.f, 128.f);
					ImGui::SliderInt("Cascades", (int*)&pxc.postfx.lightPropagationVolume.numCascades, 1, 3);
					ImGui::SliderInt("Propagations", (int*)&pxc.postfx.lightPropagationVolume.numPropagations, 1, 4);
					ImGui::DragFloat("Fade Ratio", &pxc.postfx.lightPropagationVolume.fadeRatio, 0.05f, 1.f, 10.f);
					ImGui::DragFloat("Temporal Blend Speed", &pxc.postfx.lightPropagationVolume.temporalBlend, 0.1f, 1.f, 60.f);
					ImGui::Checkbox("Fixed", &pxc.postfx.lightPropagationVolume.fixed);
					ImGuiEx::drawTransformControl("Fixed Position", &pxc.postfx.lightPropagationVolume.fixedPosition);
					ImGui::BeginDisabled();
					ImGui::Checkbox("Enabled", &pxc.postfx.lightPropagationVolume.enable);
					ImGui::EndDisabled();
					ImGui::TreePop();
				}
				if (ImGui::TreeNode("Mist")) {
					ImGui::ColorEdit3("Tint", glm::value_ptr(pxc.postfx.mist.tint), ImGuiColorEditFlags_Float);
					ImGui::DragFloat("Mie Scattering", &pxc.postfx.mist.mieScattering, 0.001f, 0.f, 1.f);
					ImGui::DragFloat("Constant Scattering", &pxc.postfx.mist.constantScattering, 0.001f, 0.f, 1.f);
					ImGui::Checkbox("Enabled", &pxc.postfx.mist.enable);
					ImGui::TreePop();
				}
				ImGui::TreePop();
			}
			if (killComponent) actor.killComponent<Components::PostFXComponent>();
		}
		
		if (actor.hasComponent<Components::VirtualPointLightComponent>()) {
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::VirtualPointLightComponent).hash_code(), nodeFlags, "Virtual Point Light");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {
				auto& vlc = actor.getComponent<Components::VirtualPointLightComponent>();
				ImGui::ColorEdit3("Color", glm::value_ptr(vlc.color), ImGuiColorEditFlags_Float);
				ImGui::DragFloat("Intensity", &vlc.lightIntensity, 0.01f, 0.f, 100.f);
				ImGui::TreePop();
			}
			if (killComponent) actor.killComponent<Components::VirtualPointLightComponent>();
		}
		if (actor.hasComponent<Components::PointLightComponent>()) {
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::PointLightComponent).hash_code(), nodeFlags, "Point Light");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {		
				auto& plc = actor.getComponent<Components::PointLightComponent>();
				ImGui::ColorEdit3("Color", glm::value_ptr(plc.color), ImGuiColorEditFlags_Float);
				ImGui::DragFloat("Intensity", &plc.lightIntensity, 0.01f, 0.f, 100.f);
				ImGui::DragFloat("Attenuation Radius", &plc.lightRadius, 0.01f, 0.1f, 100.f);
				ImGui::DragFloat("Source Radius", &plc.sourceRadius, 0.01f, 0.f, 10.f);
				ImGui::DragFloat("Source Length", &plc.sourceLength, 0.01f, 0.f, 100.f);
				ImGui::Checkbox("Cast Shadows", &plc.castShadows);
				ImGui::Checkbox("Instanced Shadow Rendering", &plc.shadowMapInstancedDraw);
				ImGui::BeginDisabled();
				ImGui::Checkbox("Cast Translucent Shadows", &plc.castTranslucentShadows);
				ImGui::EndDisabled();

				ImGui::Checkbox("Volumetric", &plc.volumetric);
				ImGui::Checkbox("Volumetric Translucency", &plc.volumetricTranslucency);

				if (ImGui::TreeNodeEx("Shadow Map", ImGuiTreeNodeFlags_DefaultOpen)) {
					const char* shadowMapConvItems[] = { "Cube", "Dual Paraboloid" };
					static const char* currentShadowMapConvItem = "Cube";

					switch (plc.shadowMapConvention) {
					case ((int)ShadowMapConvention::Cube):
						currentShadowMapConvItem = "Cube";
						break;
					case ((int)ShadowMapConvention::DualParaboloid):
						currentShadowMapConvItem = "Dual Paraboloid";
						break;
					}
					if (ImGui::BeginCombo("Shadow Map Mode", currentShadowMapConvItem)) {
						for (int n = 0; n < 2; n++) {
							bool is_selected = (currentShadowMapConvItem == shadowMapConvItems[n]); 
							if (ImGui::Selectable(shadowMapConvItems[n], is_selected))
								currentShadowMapConvItem = shadowMapConvItems[n];
							if (is_selected) {
								ImGui::SetItemDefaultFocus();
							}
						}
						ImGui::EndCombo();
					}
					if (currentShadowMapConvItem == "Cube") plc.shadowMapConvention = (int)ShadowMapConvention::Cube;
					if (currentShadowMapConvItem == "Dual Paraboloid") plc.shadowMapConvention = (int)ShadowMapConvention::DualParaboloid;

					//if (ProjectSystem::getEngineSettings().shading.pointShadowMapType == ESET::ShadowMap::PCF) {
					ImGui::DragFloat("Shadow PCF Bias", &plc.shadowMapBias, 0.001f, 0.f, 0.10f);
					ImGui::DragFloat("Shadow PCF Slope Bias Fac.", &plc.shadowMapSlopeBiasFactor, 0.01f, 1.f, 4.00f);
					ImGui::DragFloat("Shadow Radius", &plc.shadowMapPoissonRadius, 0.01f, 0.f, 4.00f, "%.2f");
					ImGui::DragInt("Shadow Samples", &plc.shadowMapPoissonSamples, 0.5f, 1, 16);
					//} else if (ProjectSystem::getEngineSettings().shading.spotShadowMapType == ESET::ShadowMap::Variance)
					//	ImGui::DragFloat("Shadow Variance Bias", &plc.shadowMapBias, 0.01f, 0.f, 0.99f);
					ImGui::DragFloat("Shadow Sharpness", &plc.shadowMapSharpness, 0.01f, 0.f, 0.99f, "%.2f");
					{ // combo box code
						int cmbIndex{};
						switch (plc.shadowMapResolution) {
						case (32): cmbIndex = 0; break;
						case (64): cmbIndex = 1; break;
						case (128): cmbIndex = 2; break;
						case (256): cmbIndex = 3; break;
						case (512): cmbIndex = 4; break;
						case (1024): cmbIndex = 5; break;
						default: cmbIndex = 2;
						}
						ImGui::Combo("Shadow Map Resolution", &cmbIndex, "32px\00064px\000128px\000256px\000512px (HIGH)\0001024px (ULTRA)");
						switch (cmbIndex) {
						case (0): plc.shadowMapResolution = 32; break;
						case (1): plc.shadowMapResolution = 64; break;
						case (2): plc.shadowMapResolution = 128; break;
						case (3): plc.shadowMapResolution = 256; break;
						case (4): plc.shadowMapResolution = 512; break;
						case (5): plc.shadowMapResolution = 1024; break;
						}
					}
					drawShadowDebug(renderContext->getShadowMapSystem()->getShadowMapForDebug(actor));
					ImGui::TreePop();
				}
				ImGui::TreePop();
			}
			if (killComponent) actor.killComponent<Components::PointLightComponent>();
		}
		if (actor.hasComponent<Components::SpotLightComponent>()) {
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::SpotLightComponent).hash_code(), nodeFlags, "Spot Light");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {
				auto& slc = actor.getComponent<Components::SpotLightComponent>();
				ImGui::ColorEdit3("Color", glm::value_ptr(slc.color), ImGuiColorEditFlags_Float);
				ImGui::DragFloat("Intensity", &slc.lightIntensity, 0.01f, 0.f, 100.f);
				ImGui::DragFloat("Attenuation Radius", &slc.lightRadius, 0.01f, 0.1f, 100.f);
				ImGui::DragFloat("Source Radius", &slc.sourceRadius, 0.01f, 0.f, 10.f);
				float oa = glm::degrees(glm::acos(slc.outerAngle) * 2.f);
				ImGui::DragFloat("Outer Angle", &oa, 0.1f, 5.f, 160.f);
				slc.outerAngle = glm::cos(glm::radians(oa) / 2.f);
				float ia = glm::min(glm::degrees(glm::acos(slc.innerAngle) * 2.f), oa);
				ImGui::DragFloat("Inner Angle", &ia, 0.1f, 5.f, oa);
				slc.innerAngle = glm::cos(glm::radians(ia) / 2.f);

				if (ImGui::TreeNodeEx("Cookie", ImGuiTreeNodeFlags_DefaultOpen)) {
					ImGui::BeginDisabled(slc.castShadows&& slc.shadowMapConvention == (int)ShadowMapConvention::Paraboloid);
					ImGui::Checkbox("Use Cookie", &slc.hasCookie);
					ImGui::BeginDisabled(!slc.hasCookie);
					if (ImGuiEx::drawAssetBox("Cookie Texture", AssetType::Texture2D, slc.cookieTexture)) {
						resourceSystem->loadTexture(slc.cookieTexture);
					}
					ImGui::EndDisabled();
					ImGui::EndDisabled();
					ImGui::TreePop();
				}

				ImGui::Checkbox("Cast Shadows", &slc.castShadows);

				const char* shadowMapConvItems[] = { "Standard", "Paraboloid" };
				static const char* currentShadowMapConvItem = "Standard";

				bool disableTransShadows = currentShadowMapConvItem == "Paraboloid";
				if (disableTransShadows) ImGui::BeginDisabled();
				ImGui::Checkbox("Cast Translucent Shadows", &slc.castTranslucentShadows);
				if (disableTransShadows) ImGui::EndDisabled();

				ImGui::BeginDisabled(!slc.castShadows || slc.shadowMapConvention == (int)ShadowMapConvention::Paraboloid ||
					ProjectSystem::getEngineSettings().shading.spotShadowMapType == ESET::ShadowMap::Variance);
				ImGui::Checkbox("Indirect Lighting", &slc.indirectLighting);
				ImGui::EndDisabled();

				ImGui::Checkbox("Volumetric", &slc.volumetric);
				ImGui::Checkbox("Volumetric Translucency", &slc.volumetricTranslucency);

				if (ImGui::TreeNodeEx("Shadow Map", ImGuiTreeNodeFlags_DefaultOpen)) {
					switch (slc.shadowMapConvention) {
					case ((int)ShadowMapConvention::Conventional):
						currentShadowMapConvItem = "Standard";
						break;
					case ((int)ShadowMapConvention::Paraboloid):
						currentShadowMapConvItem = "Paraboloid";
						break;
					}
					if (ImGui::BeginCombo("Shadow Map Mode", currentShadowMapConvItem)) {
						for (int n = 0; n < 2; n++) {
							bool is_selected = (currentShadowMapConvItem == shadowMapConvItems[n]); 
							if (ImGui::Selectable(shadowMapConvItems[n], is_selected))
								currentShadowMapConvItem = shadowMapConvItems[n];
							if (is_selected) {
								ImGui::SetItemDefaultFocus();
							}
						}
						ImGui::EndCombo();
					}
					if (currentShadowMapConvItem == "Standard") slc.shadowMapConvention = (int)ShadowMapConvention::Conventional;
					if (currentShadowMapConvItem == "Paraboloid") {
						slc.castTranslucentShadows = false; // not currently supported
						slc.shadowMapConvention = (int)ShadowMapConvention::Paraboloid;
					}

					if (ProjectSystem::getEngineSettings().shading.spotShadowMapType == ESET::ShadowMap::PCF) {
						ImGui::DragFloat("Shadow PCF Bias", &slc.shadowMapBias, 0.001f, 0.f, 0.10f);
						ImGui::DragFloat("Shadow PCF Slope Bias Fac.", &slc.shadowMapSlopeBiasFactor, 0.01f, 1.f, 4.00f);
						ImGui::DragFloat("Shadow Radius", &slc.shadowMapPoissonRadius, 0.01f, 0.f, 4.00f, "%.2f");
						ImGui::DragInt("Shadow Samples", &slc.shadowMapPoissonSamples, 0.5f, 1, 16);
					} else if (ProjectSystem::getEngineSettings().shading.spotShadowMapType == ESET::ShadowMap::Variance)
						ImGui::DragFloat("Shadow Variance Bias", &slc.shadowMapBias, 0.01f, 0.f, 0.99f);
					ImGui::DragFloat("Shadow Sharpness", &slc.shadowMapSharpness, 0.01f, 0.f, 0.99f, "%.2f");
					//ImGui::DragInt("Shadow Map Resolution", &slc.shadowmapResolution, 16, 64, 2048);
					{ // combo box code
						int cmbIndex{};
						switch (slc.shadowMapResolution) {
						case (64): cmbIndex = 0; break;
						case (128): cmbIndex = 1; break;
						case (256): cmbIndex = 2; break;
						case (512): cmbIndex = 3; break;
						case (1024): cmbIndex = 4; break;
						case (2048): cmbIndex = 5; break;
						default: cmbIndex = 2;
						}
						ImGui::Combo("Shadow Map Resolution", &cmbIndex, "64px\000128px\000256px\000512px\0001024px (HIGH)\0002048px (ULTRA)");
						switch (cmbIndex) {
						case (0): slc.shadowMapResolution = 64; break;
						case (1): slc.shadowMapResolution = 128; break;
						case (2): slc.shadowMapResolution = 256; break;
						case (3): slc.shadowMapResolution = 512; break;
						case (4): slc.shadowMapResolution = 1024; break;
						case (5): slc.shadowMapResolution = 2048; break;
						}
					}
					drawShadowDebug(renderContext->getShadowMapSystem()->getShadowMapForDebug(actor));
					ImGui::TreePop();
				}
				ImGui::TreePop();
			}
			if (killComponent) actor.killComponent<Components::SpotLightComponent>();
		}
		if (actor.hasComponent<Components::DirectionalLightComponent>()) {
			auto& dlc = actor.getComponent<Components::DirectionalLightComponent>();
			bool open = ImGui::TreeNodeEx((void*)typeid(Components::DirectionalLightComponent).hash_code(), nodeFlags, "Directional Light");
			ImGui::OpenPopupOnItemClick("KillComponent", ImGuiPopupFlags_MouseButtonRight);
			bool killComponent = false;
			if (ImGui::BeginPopup("KillComponent")) {
				if (ImGui::MenuItem("Remove Component")) {
					killComponent = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
			if (open) {
				ImGui::ColorEdit3("Color", glm::value_ptr(dlc.color), ImGuiColorEditFlags_Float);
				ImGui::DragFloat("Intensity", &dlc.lightIntensity, 0.01f, 0.f, 100.f);

				ImGui::Checkbox("Cast Shadows", &dlc.castShadows);
				bool cascaded = dlc.shadowMapConvention == 7;
				ImGui::Checkbox("Cascaded Shadows", &cascaded);
				dlc.shadowMapConvention = cascaded ? 7 : 1;

				ImGui::BeginDisabled(!dlc.castShadows || cascaded);
				ImGui::Checkbox("Cast Translucent Shadows", &dlc.castTranslucentShadows);
				bool disableVolumetrics = ProjectSystem::getEngineSettings().shading.directionalShadowMapType != ESET::ShadowMap::PCF;
				ImGui::BeginDisabled(disableVolumetrics);
				ImGui::Checkbox("Indirect Lighting", &dlc.indirectLighting);

				ImGui::Checkbox("Volumetric", &dlc.volumetric);
				ImGui::Checkbox("Volumetric Translucency", &dlc.volumetricTranslucency);
				ImGui::EndDisabled();
				ImGui::EndDisabled();
				if (ImGui::TreeNodeEx("Shadow Map", ImGuiTreeNodeFlags_DefaultOpen)) {
					if (ProjectSystem::getEngineSettings().shading.directionalShadowMapType == ESET::ShadowMap::PCF) {
						ImGui::DragFloat("Shadow PCF Bias", &dlc.shadowMapBias, 0.001f, 0.f, 0.10f);
						ImGui::DragFloat("Shadow PCF Slope Bias Fac.", &dlc.shadowMapSlopeBiasFactor, 0.01f, 1.f, 4.00f);
						ImGui::DragFloat("Shadow Poisson Radius", &dlc.shadowMapPoissonRadius, 0.01f, 0.f, 8.00f, "%.2f");
						ImGui::DragInt("Shadow Poisson Samples", &dlc.shadowMapPoissonSamples, 0.01f, 1, 16);
					}
					else if (ProjectSystem::getEngineSettings().shading.directionalShadowMapType == ESET::ShadowMap::Variance)
						ImGui::DragFloat("Shadow Variance Bias", &dlc.shadowMapBias, 0.01f, 0.f, 0.99f);
					ImGui::DragFloat("Shadow Sharpness", &dlc.shadowMapSharpness, 0.01f, 0.f, 0.99f, "%.2f");
					ImGui::DragFloat("Shadow (Base) Clip Bounds", &dlc.shadowMapClippingDistance, 1.f, 10.f, 1000.f);
				//	ImGui::DragInt("Shadow Map Resolution", &dlc.shadowmapResolution, 64, 128, 8192);
					{ // combo box code
						int cmbIndex{};
						switch (dlc.shadowMapResolution) {
						case (256): cmbIndex = 0; break;
						case (512): cmbIndex = 1; break;
						case (1024): cmbIndex = 2; break;
						case (2048): cmbIndex = 3; break;
						case (4096): cmbIndex = 4; break;
						case (8192): cmbIndex = 5; break;
						default: cmbIndex = 2;
						}
						ImGui::Combo("Shadow Map Resolution", &cmbIndex, "256px\000512px\0001024px\0002048px\0004096px (ULTRA)\0008192px (CINEMATIC)");
						switch (cmbIndex) {
						case (0): dlc.shadowMapResolution = 256; break;
						case (1): dlc.shadowMapResolution = 512; break;
						case (2): dlc.shadowMapResolution = 1024; break;
						case (3): dlc.shadowMapResolution = 2048; break;
						case (4): dlc.shadowMapResolution = 4096; break;
						case (5): dlc.shadowMapResolution = 8192; break;
						}
					}
					drawShadowDebug(renderContext->getShadowMapSystem()->getShadowMapForDebug(actor));
					ImGui::TreePop();
				}
				ImGui::TreePop();
			}
			if (killComponent) actor.killComponent<Components::DirectionalLightComponent>();
		}
		
	}
}
