#pragma once

#include "../core.h"
#include <Shard3D/core/ecs/level.h>
#include <Shard3D/core/ecs/actor.h>
#include <Shard3D/core/rendering/level_render_instance.h>
#include <imgui.h>

using namespace Shard3D::ECS;
namespace Shard3D {
	class LevelPropertiesPanel {
	public:
		S3D_DLLAPI LevelPropertiesPanel() = default;
		S3D_DLLAPI ~LevelPropertiesPanel();

		S3D_DLLAPI void setContext(sPtr<Level>& level, LevelRenderInstance* renderInstance, ResourceSystem* resourceSystem);
		S3D_DLLAPI void setDevice(S3DDevice& dvc) { device = &dvc; }
		S3D_DLLAPI void setDescriptorSetLayouts(uPtr<S3DDescriptorSetLayout>& globalSetLayout, uPtr<S3DDescriptorSetLayout>& sceneSetLayout, uPtr<S3DDescriptorSetLayout>& riggedSkeletonSetLayout, uPtr<S3DDescriptorSetLayout>& terrainSetLayout){
			this->globalSetLayout = &globalSetLayout;
			this->sceneSetLayout = &sceneSetLayout;
			this->riggedSkeletonSetLayout = &riggedSkeletonSetLayout;
			this->terrainSetLayout = &terrainSetLayout;
		}
		S3D_DLLAPI void destroyContext();
		S3D_DLLAPI void render(Actor* selectedActor);
		bool allowRenderPrefab = true; // disable for prefab editor
	private:
		S3D_DLLAPI void drawActorProperties(Actor& actor);
		S3D_DLLAPI void displayPreviewCamera(Actor& actor);
		sPtr<Level> context;
		LevelRenderInstance* renderContext;
		S3DDevice* device; // is pointer due to default constructor 
		uPtr<S3DDescriptorSetLayout>* globalSetLayout;
		uPtr<S3DDescriptorSetLayout>* sceneSetLayout;
		uPtr<S3DDescriptorSetLayout>* riggedSkeletonSetLayout;
		uPtr<S3DDescriptorSetLayout>* terrainSetLayout;
		bool showPreviewCamera = false;
		std::string modalCreatePrefabTextInputLocation{};
		std::string modalCreatePrefabTextInputName = "some_kind_of_prefab";

		const ImGuiTreeNodeFlags nodeFlags = ImGuiTreeNodeFlags_DefaultOpen | ImGuiTreeNodeFlags_AllowItemOverlap;
		ResourceSystem* resourceSystem;
	};
}
