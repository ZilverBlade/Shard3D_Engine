#include "asset_explorer_panel.h"

#include <Shard3D/core/asset/assetmgr.h>
#include <Shard3D/utils/dialogs.h>
#include <fstream>

#include <imgui.h>

#include <Shard3DEditorKit/kit.h>
#include <Shard3D/core/ecs/levelmgr.h>
#include <Shard3D/core/ecs/prefab.h>

namespace Shard3D {
	
	std::filesystem::path rootPath;

	void AssetExplorerPanel::refreshIterator(std::filesystem::path newPath) {
		directoryEntries.infos.clear();
		for (auto& dirEnt : std::filesystem::directory_iterator(newPath)) {
			AssetEntryInfo info{}; 
			info.entry = dirEnt;
			info.relativePath = std::filesystem::relative(dirEnt.path(), ProjectSystem::getAssetLocation());
			std::string fileStr = info.relativePath.filename().string();
			if (dirEnt.path() == std::filesystem::path(ProjectSystem::getAssetLocation() + "cache")) continue;
			if (!dirEnt.is_directory() && !(strUtils::hasEnding(fileStr, ENGINE_ASSET_SUFFIX) || strUtils::hasEnding(fileStr, ".s3dlevel") || strUtils::hasEnding(fileStr, ".s3dprefab"))) continue;

			if (dirEnt.is_directory()) {
				info.type = AssetType::Unknown;
				info.icon = folderIcon;
			} else {
				AssetType t = ResourceSystem::discoverAssetType(ProjectSystem::getAssetLocation() + info.relativePath.string());
				info.type = t;
				switch (t) {
				case (AssetType::Model3D):
					info.icon = mesh3dIcon;
					break;
				case (AssetType::TextureCube):
					info.icon = fileIcon;
					break;
				case (AssetType::Texture2D):
					info.icon = textureIcon;
					break;
				case (AssetType::Material):
					info.icon = smatIcon;
					break;
				case (AssetType::Level):
					info.icon = levelIcon;
					break;
				case (AssetType::Prefab):
					info.icon = prefbIcon;
					break;
				case (AssetType::SkeletonRig):
					info.icon = skeletonIcon;
					break;
				default:
					info.icon = fileIcon;
				}
			}

			info.currentAsset = AssetID(std::filesystem::relative(currentDir.string() + "/" + fileStr, std::filesystem::path(ProjectSystem::getAssetLocation())).string());
			directoryEntries.infos.push_back(info);
		}
	}

	AssetExplorerPanel::AssetExplorerPanel() {
		rootPath = std::filesystem::path(ProjectSystem::getAssetLocation().substr(0, ProjectSystem::getAssetLocation().length() - 1));
		currentDir = rootPath;

		{
			auto& img = _special_assets::_get().at("editor.browser.navback");
			backIcon = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("editor.browser.refresh");
			refreshIcon = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("editor.browser.folder");
			folderIcon = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("editor.browser.file");
			fileIcon = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("editor.browser.file.lvl");
			levelIcon = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("editor.browser.file.tex");
			textureIcon = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("editor.browser.file.msh");
			mesh3dIcon = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("editor.browser.file.smt");
			smatIcon = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("editor.browser.file.pmt");
			pmatIcon = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("editor.browser.file.blup");
			prefbIcon = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("editor.browser.file.skel");
			skeletonIcon = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		refreshIterator(std::filesystem::path(ProjectSystem::getAssetLocation()));
	}

	AssetExplorerPanel::~AssetExplorerPanel() {}

	void AssetExplorerPanel::setContext(sPtr<Level>& context, ResourceSystem* rs) {
		activelevel = context;
		resourceSystem = rs;
	}

	void AssetExplorerPanel::render() {
		wantsTextureEditor = AssetID::null();
		wantsMaterialEditor = AssetID::null();
		wantsPhysicsEditor = AssetID::null();
		wantsHUDEditor = AssetID::null();
		wantsAnimationPreviewer = AssetID::null();

		bool refresh_it = false;

		ImGui::Begin("Asset Explorer");
		if (ImGui::ImageButtonWithText(refreshIcon, "Refresh", { 16.f, 16.f })) {
			refreshIterator(currentDir);
			ImGui::End();
			return;
		}
		if (currentDir != rootPath) {
			ImGui::SameLine();
			if (ImGui::ImageButtonWithText(backIcon, "Back", { 16.f, 16.f })) {
				currentDir = currentDir.parent_path();
				refreshIterator(currentDir);
				ImGui::End();
				return;
			}
		}
		ImGui::SameLine();
		ImGui::Text("Filters:");
		bool enableFiltering = false;
		for (auto& [type, useFilter] : filters) {
			if (!useFilter) continue;
			ImGui::SameLine();
			const char* label = "null";
			switch (type) {
			case(AssetType::Level):
				label = "Level";
				break;
			case(AssetType::Prefab):
				label = "Actor Prefab";
				break;
			case(AssetType::Texture2D):
				label = "Texture2D";
				break;
			case(AssetType::TextureCube):
				label = "Texture Cube";
				break;
			case(AssetType::Material):
				label = "Surface Material";
				break;
			case(AssetType::Model3D):
				label = "Model 3D";
				break;
			case(AssetType::SkeletonRig):
				label = "Skeleton Rig";
				break;
			case(AssetType::SkeletonAnimation):
				label = "Skeleton Animation";
				break;
			case(AssetType::BakedLighting):
				label = "Baked Lighting";
				break;
			case(AssetType::PhysicsAsset):
				label = "Physics Asset";
				break;
			case(AssetType::PhysicsMaterial):
				label = "Physics Material";
				break;
			case(AssetType::PhysicsMaterialCollection):
				label = "Physics Material Collection";
				break;
			case(AssetType::Video):
				label = "Video";
				break;
			}		
			if (ImGui::Button(label)) {
				useFilter = !useFilter;
			}
			if (useFilter) enableFiltering += 1;
		}

		std::vector<const char*> deferredPopupOpens{};
		
	// panels

		float panelWidth = ImGui::GetContentRegionAvail().x;
		float thumbSize = 96.f;
		int columnCount = std::max((int)(panelWidth / (thumbSize + 16.f)) , 1);// <--- thumbnail size (96px) + padding (16px)
		
		//ImDrawList* drawList = ImGui::GetWindowDrawList();
		//ImVec2 originalContentRegion = ImGui::GetContentRegionAvail();
		//ImVec2 remainingContentRegion = originalContentRegion;

		//ImU32 textColor = IM_COL32(255, 255, 255, 255);

		ImGui::BeginColumns("assetexplorerpnl", columnCount, ImGuiOldColumnFlags_NoBorder);
		for (int i = 0; i < directoryEntries.infos.size(); i++) {
			AssetEntryInfo& entry = directoryEntries.infos[i];
			AssetID assetCurrent = entry.currentAsset;
			AssetType assetType = entry.type;
			std::string fileStr = entry.relativePath.filename().string().substr(strUtils::hasStarting(entry.relativePath.filename().string(), "."));

			if (enableFiltering && !entry.entry.is_directory()) {
				// filtered out	
				if (filters.find(assetType) == filters.end()) continue;
				if (!filters[assetType]) continue; 
			}
			//ImVec2 cursorPosition = ImGui::GetCursorScreenPos();
			//ImVec2 objectNameSize = ImGui::CalcTextSize(fileStr.c_str(), nullptr, false, thumbSize + ImGui::GetStyle().ItemSpacing.x);

			ImGui::PushID(fileStr.data());

			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4());
			ImGui::PushStyleVar(ImGuiStyleVar_FrameBorderSize, 0.f);
			//drawList->AddImage(entry.icon, { cursorPosition.x, cursorPosition.y + 15.0f }, { cursorPosition.x + thumbSize, cursorPosition.y + thumbSize + 15.0f });
			//ImGui::Selectable("", false, 0, { thumbSize, thumbSize + objectNameSize.y });
			ImGui::ImageButton(entry.icon, { thumbSize ,thumbSize });

			if (ImGui::IsItemHovered()) {
				if (ImGui::IsMouseClicked(ImGuiMouseButton_Left)) {
					if (entry.entry.is_directory()) {
						currentDir /= entry.entry.path().filename();
						refresh_it = true;
					}
				}
				if (ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left)) {
					switch (assetType) {
					case(AssetType::Texture2D):
						wantsTextureEditor = assetCurrent;
						break;
					case(AssetType::Material):
						wantsMaterialEditor = assetCurrent;
						break;
					case(AssetType::SkeletonAnimation):
						wantsAnimationPreviewer = assetCurrent;
						break;
					case(AssetType::PhysicsAsset):
						wantsPhysicsEditor = assetCurrent;
						break;
					case(AssetType::HUDLayer):
						wantsHUDEditor = assetCurrent;
						break;
					}
				}
				if (!fileStr.empty())
					currentSelection = fileStr;
			}
			if (!entry.entry.is_directory())
				if (ImGui::BeginDragDropSource()) {
					// memory leak. if any issues in the future, check this
					char* item = (char*)malloc(entry.relativePath.u8string().length() + 1);
					strncpy(item, const_cast<char*>(entry.relativePath.string().c_str()), entry.relativePath.u8string().length() + 1);
					switch (assetType) {
					case(AssetType::Texture2D):
						ImGui::SetDragDropPayload("SHARD3D.ASSEXP.TEX", item, strlen(item) + 1, ImGuiCond_Once);
						break;
					case(AssetType::TextureCube):
						ImGui::SetDragDropPayload("SHARD3D.ASSEXP.TXC", item, strlen(item) + 1, ImGuiCond_Once);
						break;
					case(AssetType::Model3D):
						ImGui::SetDragDropPayload("SHARD3D.ASSEXP.MESH", item, strlen(item) + 1, ImGuiCond_Once);
						break;
					case(AssetType::Level):
						ImGui::SetDragDropPayload("SHARD3D.ASSEXP.LVL", item, strlen(item) + 1, ImGuiCond_Once);
						break;
					case(AssetType::Material):
						ImGui::SetDragDropPayload("SHARD3D.ASSEXP.MATE", item, strlen(item) + 1, ImGuiCond_Once);
						break;
					case(AssetType::Prefab):
						ImGui::SetDragDropPayload("SHARD3D.ASSEXP.BLUP", item, strlen(item) + 1, ImGuiCond_Once);
						break;
					case(AssetType::SkeletonRig):
						ImGui::SetDragDropPayload("SHARD3D.ASSEXP.SKEL", item, strlen(item) + 1, ImGuiCond_Once);
						break;
					case(AssetType::SkeletonAnimation):
						ImGui::SetDragDropPayload("SHARD3D.ASSEXP.SKLA", item, strlen(item) + 1, ImGuiCond_Once);
						break;
					case(AssetType::PhysicsAsset):
						ImGui::SetDragDropPayload("SHARD3D.ASSEXP.PHYA", item, strlen(item) + 1, ImGuiCond_Once);
						break;
					case(AssetType::PhysicsMaterial):
						ImGui::SetDragDropPayload("SHARD3D.ASSEXP.PHYM", item, strlen(item) + 1, ImGuiCond_Once);
						break;
					case(AssetType::PhysicsMaterialCollection):
						ImGui::SetDragDropPayload("SHARD3D.ASSEXP.PHMC", item, strlen(item) + 1, ImGuiCond_Once);
						break;
					case(AssetType::Unknown):
					default:
						ImGui::SetDragDropPayload("SHARD3D.ASSEXP.WHAT", item, strlen(item) + 1, ImGuiCond_Once);
						break;
					}
					ImGui::EndDragDropSource();
				}

			ImGui::PopStyleVar();
			ImGui::PopStyleColor();
			if (!currentSelection.empty() && renamingAssetOriginal == fileStr.substr(0, fileStr.find_last_of(".")).c_str() && renamingAssetOriginal == currentSelection.substr(0, currentSelection.find_last_of("."))) {
				auto& rfile = renamingAsset;
				char fileBuffer[256];
				memset(fileBuffer, 0, 256);
				strncpy(fileBuffer, rfile.c_str(), 256);

				if (ImGui::InputText("##assetrenamingpayload", fileBuffer, 256)) {
					rfile = std::string(fileBuffer);
				}
				if (ImGui::IsKeyPressed(ImGuiKey_Enter)) {
					if (entry.entry.is_directory()) {
						SHARD3D_WARN_E("Directory was renamed! Assets inside will have incorrect references!");
						std::filesystem::rename(currentDir.string() + "/" + currentSelection, currentDir.string() + "/" + renamingAsset);
						currentSelection = renamingAsset;
						refresh_it = true;
					} else { // TODO: NO RENAME IF THE NAME IS THE SAME!!! WILL CAUSE INFINITE LOOP
						std::string relCurrDir = std::filesystem::relative(currentDir.string(), std::filesystem::path(ProjectSystem::getAssetLocation())).string();
						AssetID old = relCurrDir + "/" + currentSelection;
						std::string literal_extension = currentSelection.substr(currentSelection.find_last_of("."), currentSelection.length() - currentSelection.find_last_of(".")).c_str();
						AssetID new_ = relCurrDir + "/" + renamingAsset + literal_extension;

						if (assetType != AssetType::Material && assetType != AssetType::Level) {
							AssetManager::renameVirtualResource(assetType, old, new_);
						}
						std::filesystem::rename(std::filesystem::path(old.getAbsolute()), std::filesystem::path(new_.getAbsolute()));
						currentSelection = renamingAsset + literal_extension;
						AssetReferenceTracker::swapReferenceBruteforce(old, new_.getAsset());
						resourceSystem->swapAsset(assetType, old, new_.getAsset());

						switch (assetType) {
						case(AssetType::Model3D):
						{
							activelevel->registry.view<Components::Mesh3DComponent>().each([&](auto& mesh) {
								if (mesh.asset.getID() == old.getID()) {
									mesh.asset = new_;
								}
							});
							//TODO: FIX!!!
							//activelevel->getRenderList()->swapAsset(assetType, old, new_);
						}break;
						case(AssetType::Texture2D):
						{
							activelevel->registry.view<Components::BillboardComponent>().each([&](auto& bill) {
								if (bill.asset.getID() == old.getID()) {
									bill.asset = new_;
								}
							});
							for (auto& [asset, material] : resourceSystem->getMaterialAssets()) {
								const auto& at = material->getAllTexturesReference();
								for (int i = 0; i < at.size(); i++) {
									if (at[i]->getID() == old.getID()) {
										*at[i] = new_;
									}
								}
							}
						}break;
						case(AssetType::Material):
						{
							activelevel->registry.view<Components::Mesh3DComponent>().each([&](auto& mesh) {
								for (int i = 0; i < mesh.getMaterials().size(); i++) if (mesh.getMaterials()[i].getID() == old.getID()) {
									mesh.setMaterial(new_, i);
								}
							});
							//TODO: FIX!!!
							//activelevel->getRenderList()->swapAsset(assetType, old, new_);
						}break;
						}

						for (const auto& entry : std::filesystem::recursive_directory_iterator(ProjectSystem::getAssetLocation())) {
							if (!entry.is_regular_file()) continue;
							std::string assetPath = entry.path().string();
							AssetType assetType = ResourceSystem::discoverAssetType(assetPath);
							if (assetType == AssetType::Unknown) continue;
							std::string reldp = std::filesystem::relative(assetPath, std::filesystem::path(ProjectSystem::getAssetLocation())).string();
							AssetID asset = AssetID(reldp);

							switch (assetType) {
							case(AssetType::Material):
							case(AssetType::Prefab):
							case(AssetType::Level):
							case(AssetType::Model3D):

								std::ifstream f(assetPath);
								if (!f.good()) {
									continue;
								}
								std::stringstream dat{};
								dat << f.rdbuf();
								f.close();
								std::string file = dat.str();
								if (!strUtils::contains(file, old.getAsset())) continue;
								strUtils::replace(file, old.getAsset(), new_.getAsset());

								std::ofstream o(assetPath);
								o << file;
								o.flush();
								o.close();
							break;
							}
						}

						refresh_it = true;
						renamingAssetOriginal = {};
						renamingAsset = {};
						currentSelection = {};
					}
				}
			} else {
				//drawList->AddText(nullptr, 0.0f, { cursorPosition.x, cursorPosition.y + thumbSize }, textColor,  entry.entry.is_directory() ? fileStr.c_str() : fileStr.substr(0, fileStr.find_last_of(".")).c_str(), nullptr, thumbSize + ImGui::GetStyle().ItemSpacing.x);
				ImGui::TextWrapped(entry.entry.is_directory() ? fileStr.c_str() : fileStr.substr(0, fileStr.find_last_of(".")).c_str());
			}
			
			//remainingContentRegion.x -= (thumbSize + ImGui::GetStyle().ItemSpacing.x);
			//if (remainingContentRegion.x < thumbSize) {
			//	ImGui::NewLine();
			//	remainingContentRegion = originalContentRegion;
			//} else {
			//	ImGui::SameLine();
			//}
			ImGui::NextColumn();
			ImGui::PopID();

			if (refresh_it) { refreshIterator(currentDir); break; }
		}
		ImGui::EndColumns();
		if (!currentSelection.empty()) {
			if (ImGui::BeginPopupContextWindow("AssetMenu", ImGuiPopupFlags_MouseButtonRight)) {
				AssetID asset_ = AssetID(std::filesystem::relative(currentDir.string() + "/" + currentSelection, std::filesystem::path(ProjectSystem::getAssetLocation())).string());
				if (ImGui::MenuItem("Rename")) {
					if (ResourceSystem::discoverAssetType(asset_.getAbsolute()) != AssetType::Prefab) {
						renamingAssetOriginal = renamingAsset = currentSelection.substr(0, currentSelection.find_last_of("."));
						ImGui::CloseCurrentPopup();
					} else {
						SHARD3D_ERROR("Renaming prefabs is disabled until crash is fixed!");
					}
				}
				if (ImGui::MenuItem("Reload into memory")) {
					switch (ResourceSystem::discoverAssetType(asset_.getAbsolute())) {
					case(AssetType::Model3D):
						resourceSystem->loadMesh(asset_, true);
						break;
					case(AssetType::SkeletonRig):
						resourceSystem->loadSkeleton(asset_, true);
						break;
					case(AssetType::Texture2D):
						resourceSystem->loadTexture(asset_, true);
						break;
					case(AssetType::TextureCube):
						resourceSystem->loadTextureCube(asset_, true);
						break;
					case(AssetType::Material):
						resourceSystem->loadMaterialRecursive(asset_, true);
						break;
					default:
						SHARD3D_NOIMPL;
					}
					ImGui::CloseCurrentPopup();
				}
				if (AssetType type = ResourceSystem::discoverAssetType(asset_.getAbsolute()); type == AssetType::Texture2D || type == AssetType::Model3D || type == AssetType::TextureCube) {
					if (ImGui::MenuItem("Replace")) {
						SHARD3D_NOIMPL;

						currentSelection = {};
						ImGui::CloseCurrentPopup();
					}
					if (ImGui::MenuItem("Reimport")) {
						if (AssetManager::reimportAsset(asset_)) {
							SHARD3D_INFO_E("Successfully reimported asset {0}", asset_.getAsset());
						} else {
							SHARD3D_ERROR_E("Error reimporting asset {0}", asset_.getAsset());
						}
						currentSelection = {};
						ImGui::CloseCurrentPopup();
					}
				}
				if (ImGui::MenuItem("Delete")) {
					bool destroy = false;
					bool used = false;
					if (AssetReferenceTracker::isBeingUsed(asset_)) {
						std::stringstream stream{};
						for (AssetID user : AssetReferenceTracker::getReferences(asset_).users) stream << "\n\"" << user.getAsset() << "\"";
						if (MessageDialogs::show(std::string("This asset is used in other contexts, removing this asset may cause issues.\nAre you sure you want to delete this?\nContexts using this asset:" + stream.str()).c_str(), "Asset Explorer", MessageDialogs::DialogOptions::OPTYESNO | MessageDialogs::DialogOptions::OPTICONQUESTION) == MessageDialogs::DialogResult::RESYES) {
							destroy = true;
							used = false;
						}
					} else {
						if (MessageDialogs::show("Are you sure you want to delete this? Asset will be permanently deleted.", "Asset Explorer", MessageDialogs::DialogOptions::OPTYESNO | MessageDialogs::DialogOptions::OPTICONQUESTION) == MessageDialogs::DialogResult::RESYES) {
							destroy = true;
						}
					}
					if (destroy) {
						AssetType aType = ResourceSystem::discoverAssetType(asset_.getAbsolute());
						resourceSystem->unloadAsset(aType, asset_.getAsset());
						if (used) {
							switch (aType) {
							case(AssetType::Model3D):
								AssetReferenceTracker::swapReferenceBruteforce(asset_, resourceSystem->coreAssets.m_errorMesh);
								break;
							case(AssetType::Texture2D):
								AssetReferenceTracker::swapReferenceBruteforce(asset_, resourceSystem->coreAssets.t_errorTexture);
								break;
							case(AssetType::Material):
							{
								MaterialClass matClass = Material::discoverMaterialClass(asset_.getAbsolute());
								AssetReferenceTracker::swapReferenceBruteforce(asset_, resourceSystem->coreAssets.sm_errorMaterial);
							}
								break;
							default: // idk
								SHARD3D_WARN_E("Asset is being improperly handled!");
								AssetReferenceTracker::rmvReferencePurge(asset_.getAsset());
								break;
							}
						} else {
							AssetReferenceTracker::rmvReferencePurge(asset_.getAsset());
						}
						AssetManager::purgeAsset(asset_.getAsset());
						currentSelection = {};
						refreshIterator(currentDir);
					}
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}
		}


		if (ImGui::BeginPopupContextWindow("AssetExplorerContext", ImGuiPopupFlags_MouseButtonRight | ImGuiPopupFlags_NoOpenOverItems)) {
			if (ImGui::MenuItem("Import Texture")) {
				std::string file = BrowserDialogs::openFile();
				std::ifstream check(file);
				if (check.good()) {
					std::string dest = std::string(currentDir.string() + "/" + IOUtils::getFileOnly(file));
					std::replace(dest.begin(), dest.end(), '\\', '/');
					std::transform(dest.begin(), dest.end(), dest.begin(),
						[](unsigned char c) { return std::tolower(c); });
					currentModalFileDest = dest;
					currentModalFileFile = file;
					modalTextureLImport = TextureLoadInfo();
					modalTextureIImport = TextureImportInfo();

					bool possiblyMaterialParam = false;
					if (currentModalFileDest.find("normal") != currentModalFileDest.npos) {
						modalTextureLImport.colorSpace = TextureColorSpace::Linear;
						modalTextureLImport.channels = TextureChannels::RG;
						modalTextureLImport.compression = TextureCompression::BC5;
						modalTextureLImport.isNormalMap = true;
					}
					if (currentModalFileDest.find("specular") != currentModalFileDest.npos) possiblyMaterialParam = true;
					if (currentModalFileDest.find("glossiness") != currentModalFileDest.npos) possiblyMaterialParam = true;
					if (currentModalFileDest.find("shine") != currentModalFileDest.npos) possiblyMaterialParam = true;
					if (currentModalFileDest.find("roughness") != currentModalFileDest.npos) possiblyMaterialParam = true;
					if (currentModalFileDest.find("metallic") != currentModalFileDest.npos) possiblyMaterialParam = true;
					if (currentModalFileDest.find("metalcy") != currentModalFileDest.npos) possiblyMaterialParam = true;
					if (possiblyMaterialParam) {
						modalTextureLImport.colorSpace = TextureColorSpace::Linear;
						modalTextureLImport.channels = TextureChannels::R;
						modalTextureLImport.compression = TextureCompression::BC4;
					}
					deferredPopupOpens.push_back("Import Texture2D");
				} else {
					MessageDialogs::show("Invalid texture file provided", "Asset Error", MessageDialogs::OPTICONERROR);
				}
				ImGui::CloseCurrentPopup();
			}
			if (ImGui::MenuItem("Import Cubemap")) {
				std::string folder = BrowserDialogs::openFolder("Select a folder with cubemap images");
				if (std::filesystem::exists(folder)) {
					std::string dest = std::string(currentDir.string() + "/" + IOUtils::getFileOnly(folder));
					std::replace(dest.begin(), dest.end(), '\\', '/');
					currentModalFileDest = dest;
					currentModalFileFile = folder;
					modalTextureLImport = TextureLoadInfo();
					modalTextureIImport = TextureImportInfo();
					modalTextureIImport.isCubemap = true;
					deferredPopupOpens.push_back("Import TextureC");
				}
				else {
					MessageDialogs::show("Invalid cubemap file provided", "Asset Error", MessageDialogs::OPTICONERROR);
				}
				ImGui::CloseCurrentPopup();
			}
			if (ImGui::MenuItem("Import 3D Model")) {
				std::string file = BrowserDialogs::openFile();
				std::ifstream check(file);
				if (check.good()) {
					std::string dest = std::string(currentDir.string() + "/" + IOUtils::getFileOnly(file));
					std::replace(dest.begin(), dest.end(), '\\', '/');
					currentModalFileDest = dest;
					currentModalFileFile = file;

					modalModelImport = Model3DImportInfo();
					deferredPopupOpens.push_back("Import Model3D");
				}
				else {
					MessageDialogs::show("Invalid model file provided", "Asset Error", MessageDialogs::OPTICONERROR);
				}
				ImGui::CloseCurrentPopup();
			}
			ImGui::Separator();
			if (ImGui::MenuItem("New level")) {
				auto trash = make_sPtr<Level>(resourceSystem);
				LevelManager lman(*trash, resourceSystem);
				lman.save(std::string(currentDir.string() + "/some_kind_of_level"));
				refreshIterator(currentDir);
				ImGui::CloseCurrentPopup();
			}
			if (ImGui::MenuItem("Create Prefab")) {
				auto trash2 = make_uPtr<Level>(resourceSystem);
				SHARD3D_NOIMPL_C;
				//auto trash = trash2->createPrefab(trash2->createActor("Prefab Actor"));
				//trash.serialize(std::string(currentDir.string() + "/some_kind_of_prefab"));
				refreshIterator(currentDir);
				ImGui::CloseCurrentPopup();
			}
			if (ImGui::BeginMenu("Create Material")) {
				if (ImGui::MenuItem("Surface Shaded")) {
					rPtr<SurfaceMaterial> blank = make_rPtr<SurfaceMaterial>(resourceSystem);
					std::string dest = std::string(currentDir.string() + "/some_kind_of_surface_material");
					std::replace(dest.begin(), dest.end(), '\\', '/');
					AssetID asset = AssetManager::createMaterial(dest, blank.get());
					AssetReferenceTracker::allocate(asset);
					for (auto& tex : blank->getAllTextures())
						AssetReferenceTracker::addReference(asset, tex);
					refreshIterator(currentDir);
					ImGui::CloseCurrentPopup();
				}
				if (ImGui::MenuItem("Physics")) {
					std::string dest = std::string(currentDir.string() + "/some_kind_of_physics_material.s3dasset");
					std::replace(dest.begin(), dest.end(), '\\', '/');
					PhysicsMaterial().serialize(dest);
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndMenu();
			}
			if (ImGui::BeginMenu("Create Physics Collider")) {
				
				ImGui::EndMenu();
			}
			if (ImGui::BeginMenu("Filters")) {
				if (ImGui::MenuItem("Level")) {
					filters[AssetType::Level] = true;
					ImGui::CloseCurrentPopup();
				}
				if (ImGui::MenuItem("Actor Prefab")) {
					filters[AssetType::Prefab] = true;
					ImGui::CloseCurrentPopup();
				}
				if (ImGui::MenuItem("Texture2D")) {
					filters[AssetType::Texture2D] = true;
					ImGui::CloseCurrentPopup();
				}
				if (ImGui::MenuItem("Texture Cube")) {
					filters[AssetType::TextureCube] = true;
					ImGui::CloseCurrentPopup();
				}
				if (ImGui::MenuItem("Surface Material")) {
					filters[AssetType::Material] = true;
					ImGui::CloseCurrentPopup();
				}
				if (ImGui::MenuItem("Model3D")) {
					filters[AssetType::Model3D] = true;
					ImGui::CloseCurrentPopup();
				}
				if (ImGui::MenuItem("Baked Lighting")) {
					filters[AssetType::BakedLighting] = true;
					ImGui::CloseCurrentPopup();
				}
				if (ImGui::MenuItem("Physics Asset")) {
					filters[AssetType::PhysicsAsset] = true;
					ImGui::CloseCurrentPopup();
				}
				if (ImGui::MenuItem("Physics Material")) {
					filters[AssetType::PhysicsAsset] = true;
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndMenu();
			}
			ImGui::EndPopup();
		}
		for (const char* dfp : deferredPopupOpens) ImGui::OpenPopup(dfp);
		bool true_ = true;
		if (ImGui::BeginPopupModal("Import Texture2D", &true_, ImGuiWindowFlags_Popup | ImGuiWindowFlags_NoDocking)) {
			ImGui::BeginDisabled();
			ImGui::Combo("Texture Format", reinterpret_cast<int*>(&modalTextureLImport.format), "U8\000U16\000F16\000F32");
			ImGui::EndDisabled();
			ImGui::Combo("Texture Filter Mode", reinterpret_cast<int*>(&modalTextureLImport.filter), "Nearest\000Linear");
			ImGui::Combo("Texture Address Mode", reinterpret_cast<int*>(&modalTextureLImport.addressMode), "Repeat\000Mirror Repeat\000Edge Clamp\000Border Clamp\000Mirrored Edge Clamp");
			ImGui::Combo("Texture Color Space", reinterpret_cast<int*>(&modalTextureLImport.colorSpace), "Linear\000sRGB");
			ImGui::Combo("Texture Channels", reinterpret_cast<int*>(&modalTextureLImport.channels), "R\000RG\000RGB\000RGBA");
			
			const char* compressionItems[] = { "Lossless", "BC1", "BC3", "BC4", "BC5", "BC7" };
			static const char* currentCompressionItem = "BC7";
			bool invalidSettings = false;
			std::vector<const char*> displayMessages{};
			if (modalTextureLImport.format != TextureFormat::Unorm8) {
				displayMessages.push_back("*Non U8 formats do not support block compression");
			}

			switch (modalTextureLImport.compression) {
			case (TextureCompression::Lossless):currentCompressionItem = "Lossless"; break;
			case (TextureCompression::BC1):		currentCompressionItem = "BC1"; break;
			case (TextureCompression::BC3):		currentCompressionItem = "BC3"; break;
			case (TextureCompression::BC4):		currentCompressionItem = "BC4"; break;
			case (TextureCompression::BC5):		currentCompressionItem = "BC5"; break;
			case (TextureCompression::BC7):		currentCompressionItem = "BC7"; break;
			}
			if (ImGui::BeginCombo("Texture Compression Mode", currentCompressionItem)) {
				for (int n = 0; n < 6; n++) {
					bool compressionOptionIsDisabled = false;
					if (n == 1 && modalTextureLImport.channels != TextureChannels::RGB) compressionOptionIsDisabled = true;
					if ((n == 2 || n == 5) && (modalTextureLImport.channels != TextureChannels::RGB && modalTextureLImport.channels != TextureChannels::RGBA)) 
						compressionOptionIsDisabled = true;
					
					if (n == 3 && modalTextureLImport.channels != TextureChannels::R) 
						compressionOptionIsDisabled = true;
					
					if (n == 4 && modalTextureLImport.channels != TextureChannels::RG) 
						compressionOptionIsDisabled = true;
					

					bool is_selected = (currentCompressionItem == compressionItems[n]); // You can store your selection however you want, outside or inside your objects
					if (compressionOptionIsDisabled) ImGui::BeginDisabled();
					if (ImGui::Selectable(compressionItems[n], is_selected))
						currentCompressionItem = compressionItems[n];
					if (is_selected) {
						ImGui::SetItemDefaultFocus();
					}
					if (compressionOptionIsDisabled) ImGui::EndDisabled();
				}
				ImGui::EndCombo();
			}
			if (currentCompressionItem == "Lossless" && modalTextureLImport.channels == TextureChannels::R ||
						modalTextureLImport.channels == TextureChannels::RG ||
						modalTextureLImport.channels == TextureChannels::RGB) displayMessages.push_back("Format used on the GPU will be RGBA");
			if (currentCompressionItem == "BC3" && modalTextureLImport.channels == TextureChannels::RGB) {
				displayMessages.push_back("BC1 is more efficient as it has no alpha channel with same compression.");
			}
			if ((currentCompressionItem == "BC3" || currentCompressionItem == "BC7") && modalTextureLImport.channels == TextureChannels::RGB) {
				displayMessages.push_back("Format used on the GPU will be RGBA");
			}
			if ((currentCompressionItem == "BC3" || currentCompressionItem == "BC7") && (modalTextureLImport.channels == TextureChannels::RG || modalTextureLImport.channels == TextureChannels::R)) {
				invalidSettings = true;
			}
			if ((currentCompressionItem == "BC4") && modalTextureLImport.channels != TextureChannels::R) {
				invalidSettings = true;
			}
			if ((currentCompressionItem == "BC5") && modalTextureLImport.channels != TextureChannels::RG) {
				invalidSettings = true;
			}
			if (currentCompressionItem == "Lossless") modalTextureLImport.compression = TextureCompression::Lossless;
			if (currentCompressionItem == "BC1") modalTextureLImport.compression = TextureCompression::BC1;
			if (currentCompressionItem == "BC3") modalTextureLImport.compression = TextureCompression::BC3;
			if (currentCompressionItem == "BC4") modalTextureLImport.compression = TextureCompression::BC4;
			if (currentCompressionItem == "BC5") modalTextureLImport.compression = TextureCompression::BC5;
			if (currentCompressionItem == "BC7") modalTextureLImport.compression = TextureCompression::BC7;
			
			ImGui::Checkbox("Enable Texture Mip Maps", &modalTextureLImport.enableMipMaps);
			ImGui::Checkbox("(Conversion) Flip Y Normals", &modalTextureIImport.flipNormalMapY);
			for (const char* msg : displayMessages) {
				ImGui::TextColored(ImVec4(1.0, 1.0, 0.0, 1.0), msg);
			}
			if (invalidSettings) {
				ImGui::TextColored(ImVec4(1.0, 0.3, 0.5, 1.0), "Invalid settings detected. Import may not proceed until resolved.");
				ImGui::BeginDisabled();
			}
			if (ImGui::Button("Import")) {
				AssetID asset = AssetManager::importTexture2D(currentModalFileFile, currentModalFileDest, resourceSystem, modalTextureIImport, modalTextureLImport);
				AssetReferenceTracker::allocate(asset);
				refreshIterator(currentDir);
				ImGui::CloseCurrentPopup();
			}
			if (invalidSettings) ImGui::EndDisabled();
			ImGui::EndPopup();
		}
		if (ImGui::BeginPopupModal("Import TextureC", &true_, ImGuiWindowFlags_Popup | ImGuiWindowFlags_NoDocking)) {
			ImGui::Combo("Texture Format", reinterpret_cast<int*>(&modalTextureLImport.format), "U8\000U16\000F16\000F32\000E5BGR9");
			ImGui::Combo("Texture Filter Mode", reinterpret_cast<int*>(&modalTextureCLImport.filter), "Nearest\000Linear");
			ImGui::Combo("Texture Compression Mode", reinterpret_cast<int*>(&modalTextureCLImport.compression),
				"Lossless\000BC1\000BC2\000BC3\000BC4\000BC5\000BC6H\000BC7");
			ImGui::Checkbox("Generate Glossiness Map", &modalTextureCIImport.generateCubemapGlossinessMap);
			ImGui::BeginDisabled();
			ImGui::Combo("Texture Cube Format", reinterpret_cast<int*>(&modalTextureCIImport.cubeFormat),
				"Cube\000Equirectangular\000Paraboloid\000BC3\000BC4\000BC5\000BC6H\000BC7");
			ImGui::EndDisabled();
			if (ImGui::Button("Import")) {
				std::string ext = std::filesystem::directory_iterator(currentModalFileFile)->path().extension().string();
				std::vector<std::string> subfiles{
					currentModalFileFile + "/right." + ext,
					currentModalFileFile + "/left." + ext,
					currentModalFileFile + "/top." + ext,
					currentModalFileFile + "/bottom." + ext,
					currentModalFileFile + "/front." + ext,
					currentModalFileFile + "/back." + ext
				};
				AssetID asset = AssetManager::importTextureCube(subfiles, currentModalFileDest, resourceSystem, modalTextureCIImport, modalTextureCLImport, PackageInvocationIndex());
				AssetReferenceTracker::allocate(asset);
				refreshIterator(currentDir);
				ImGui::CloseCurrentPopup();
			}
			ImGui::EndPopup();
		}
		
		if (ImGui::BeginPopupModal("Import Model3D", &true_, ImGuiWindowFlags_Popup | ImGuiWindowFlags_NoDocking)) {
			ImGui::Checkbox("Combine Meshes", &modalModelImport.combineMeshes);
			ImGui::Dummy({ 1, 16 });
			if (!modalModelImport.combineMeshes) {
				ImGui::TextColored(ImVec4(1.0, 0.3, 0.5, 1.0), "Some formats will not transform their axis to match Shard3D's axis!");
			}
			ImGuiEx::drawTransformControl("Import Rotation", modalModelImport.importEulerRotation);
			ImGuiEx::drawTransformControl("Import Translation", modalModelImport.importTranslation);
			ImGui::Dummy({ 1, 16 });

			ImGui::BeginDisabled();
			ImGui::Checkbox("Calculate Tangent Space", &modalModelImport.calculateTangents);
			ImGui::EndDisabled();
			ImGui::Dummy({ 1, 16 });
			ImGui::Checkbox("Create Materials", &modalModelImport.createMaterials);
			ImGui::Dummy({ 1, 16 });
			ImGui::BeginDisabled(modalModelImport.combineMeshes);
			ImGui::Checkbox("Skeletal Mesh", &modalModelImport.skeletalMesh);
			ImGui::BeginDisabled(!modalModelImport.skeletalMesh);
			ImGui::Checkbox("Import Bones", &modalModelImport.importBones);
			ImGui::Checkbox("Import Morph Targets", &modalModelImport.importMorphTargets);
			ImGui::Checkbox("Import Animations", &modalModelImport.importAnimations);
			ImGui::EndDisabled();
			ImGui::EndDisabled();
			ImGui::Dummy({ 1, 16 });

			ImGui::DragFloat("Model Scale", &modalModelImport.scaleFactor, 0.1f, 0.0f);
			if (ImGui::Button("Import")) {
				std::vector<AssetID> assets = AssetManager::importMeshes(currentModalFileFile, currentModalFileDest, resourceSystem, modalModelImport);
				for (auto& asset : assets) {
					AssetReferenceTracker::allocate(asset);
					for (AssetID& material : resourceSystem->retrieveMesh(asset)->materials) AssetReferenceTracker::addReference(asset, material);
				}
				refreshIterator(currentDir);
				ImGui::CloseCurrentPopup();
			}
			ImGui::EndPopup();
		}

		ImGui::End();
	}
}