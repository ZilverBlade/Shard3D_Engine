#include "editor_pref.h"
#include <Shard3D/core.h>
#include <Shard3D/s3dstd.h>
#include <filesystem>
#include <fstream>
namespace Shard3D {
	void EditorPreferences::load() {
		auto editorCFG = ProjectSystem::getEngineAppDataLoc() + "/editorcfg.ini";
		if (!std::filesystem::exists(editorCFG)) {
			save();
			return;
		}
		CSimpleIniA ini{};
		ini.SetUnicode();
		ini.LoadFile(editorCFG.c_str());
			 if (ini.GetValue("SCRIPTING", "preferredIDE") == "VisualStudio2005") ideVersion = IDEVersion::VisualStudio2005;
		else if (ini.GetValue("SCRIPTING", "preferredIDE") == "VisualStudio2008") ideVersion = IDEVersion::VisualStudio2008;
		else if (ini.GetValue("SCRIPTING", "preferredIDE") == "VisualStudio2010") ideVersion = IDEVersion::VisualStudio2010;
		else if (ini.GetValue("SCRIPTING", "preferredIDE") == "VisualStudio2012") ideVersion = IDEVersion::VisualStudio2012;
		else if (ini.GetValue("SCRIPTING", "preferredIDE") == "VisualStudio2013") ideVersion = IDEVersion::VisualStudio2013;
		else if (ini.GetValue("SCRIPTING", "preferredIDE") == "VisualStudio2015") ideVersion = IDEVersion::VisualStudio2015;
		else if (ini.GetValue("SCRIPTING", "preferredIDE") == "VisualStudio2017") ideVersion = IDEVersion::VisualStudio2017;
		else if (ini.GetValue("SCRIPTING", "preferredIDE") == "VisualStudio2019") ideVersion = IDEVersion::VisualStudio2019;
		else if (ini.GetValue("SCRIPTING", "preferredIDE") == "VisualStudio2022") ideVersion = IDEVersion::VisualStudio2022;
		
			 if (ini.GetValue("GUI", "theme") == "Dark")	theme = EditorTheme::Dark;
		else if (ini.GetValue("GUI", "theme") == "Light")	theme = EditorTheme::Light;
		else if (ini.GetValue("GUI", "theme") == "Abyss")	theme = EditorTheme::Abyss;

			 if (ini.GetValue("GUI", "colorPickFmt") == "Integer")		colorPickFmt = ColorPickerFormat::Integer;
		else if (ini.GetValue("GUI", "colorPickFmt") == "FP32")			colorPickFmt = ColorPickerFormat::FloatingPoint;
		else if (ini.GetValue("GUI", "colorPickFmt") == "Hexadecimal")	colorPickFmt = ColorPickerFormat::Hexadecimal;
	}
	void EditorPreferences::save() {
		auto editorCFG = ProjectSystem::getEngineAppDataLoc() + "/editorcfg.ini";

		CSimpleIniA ini{};
		ini.SetUnicode();
		
		switch (ideVersion) {
		case (IDEVersion::VisualStudio2005): ini.SetValue("SCRIPTING", "preferredIDE", "VisualStudio2005"); break;
		case (IDEVersion::VisualStudio2008): ini.SetValue("SCRIPTING", "preferredIDE", "VisualStudio2008"); break;
		case (IDEVersion::VisualStudio2010): ini.SetValue("SCRIPTING", "preferredIDE", "VisualStudio2010"); break;
		case (IDEVersion::VisualStudio2012): ini.SetValue("SCRIPTING", "preferredIDE", "VisualStudio2012"); break;
		case (IDEVersion::VisualStudio2013): ini.SetValue("SCRIPTING", "preferredIDE", "VisualStudio2013"); break;
		case (IDEVersion::VisualStudio2015): ini.SetValue("SCRIPTING", "preferredIDE", "VisualStudio2015"); break;
		case (IDEVersion::VisualStudio2017): ini.SetValue("SCRIPTING", "preferredIDE", "VisualStudio2017"); break;
		case (IDEVersion::VisualStudio2019): ini.SetValue("SCRIPTING", "preferredIDE", "VisualStudio2019"); break;
		case (IDEVersion::VisualStudio2022): ini.SetValue("SCRIPTING", "preferredIDE", "VisualStudio2022"); break;
		}
		switch (theme) {
		case (EditorTheme::Dark):	ini.SetValue("GUI", "theme", "Dark");	break;
		case (EditorTheme::Light):	ini.SetValue("GUI", "theme", "Light");	break;
		case (EditorTheme::Abyss):	ini.SetValue("GUI", "theme", "Abyss");	break;
		}
		switch (colorPickFmt) {
		case (ColorPickerFormat::Integer):			ini.SetValue("GUI", "colorPickFmt", "Integer");		break;
		case (ColorPickerFormat::FloatingPoint):	ini.SetValue("GUI", "colorPickFmt", "FP32");		break;
		case (ColorPickerFormat::Hexadecimal):		ini.SetValue("GUI", "colorPickFmt", "Hexadecimal");	break;
		}

		ini.SetDoubleValue("GUI", "assetExplorerThumbSize", assetExplorerThumbnailSize);

		ini.SaveFile(editorCFG.c_str());
	}
}