#pragma once
#include <Shard3D/s3dstd.h>
#include "../core.h"
#include <Shard3D/core/vulkan_api/bindless.h>
#include <Shard3D/core/asset/texture2d.h>
namespace Shard3D {

	class _special_assets {
	public:
		// engine only function, do not call this
		S3D_DLLAPI static void _editor_icons_load(S3DDevice& device, uPtr<SmartDescriptorSet>& descriptor);
		// engine only function, do not call this
		S3D_DLLAPI static void _editor_icons_destroy();
		S3D_DLLAPI static std::unordered_map<std::string, Texture2D*>& _get();
	};
}