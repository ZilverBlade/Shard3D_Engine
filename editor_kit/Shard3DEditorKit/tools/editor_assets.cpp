#include "editor_assets.h"
#include <ktx.h>
#include <Shard3D/core/asset/image_tools.h>
#pragma region special
namespace Shard3D {
	static std::unordered_map<std::string, Texture2D*> _editor_icons;

	static const char* _editor_icons_array[][2]{
		{"editor.play",						"resources/editor_icons/icon_play.png"				},
		{"editor.pause",					"resources/editor_icons/icon_pause.png"				},
		{"editor.stop",						"resources/editor_icons/icon_stop.png"				},
		{"editor.save",						"resources/editor_icons/icon_save.png"				},
		{"editor.load",						"resources/editor_icons/icon_load.png"				},
		{"editor.saveas",					"resources/editor_icons/icon_save_as.png"			},
		{"editor.pref",						"resources/editor_icons/icon_pref.png"				},
		{"editor.settings",					"resources/editor_icons/icon_gear.png"				},
		{"editor.preview",					"resources/editor_icons/icon_preview.png"			},
		{"editor.layout",					"resources/editor_icons/icon_layout.png"			},
		{"editor.viewport",					"resources/editor_icons/icon_monitor.png"			},
		{"editor.level",					"resources/editor_icons/icon_level.png"				},
		{"editor.launch",					"resources/editor_icons/icon_launch_game.png"		},
		{"editor.edit_position",			"resources/editor_icons/icon_edit_position.png"		},
		{"editor.edit_position.add",		"resources/editor_icons/icon_edit_position_add.png"	},
		{"editor.edit_position.rmv",		"resources/editor_icons/icon_edit_position_rmv.png"	},
		{"editor.edit_node",				"resources/editor_icons/icon_edit_node.png"			},
		{"editor.edit_node.add",			"resources/editor_icons/icon_edit_node_add.png"		},
		{"editor.edit_node.rmv",			"resources/editor_icons/icon_edit_node_rmv.png"		},
		{"editor.edit",						"resources/editor_icons/icon_pencil.png"			},
		{"editor.mirror",					"resources/editor_icons/icon_mirror.png"			},
		{"editor.mirror_left",				"resources/editor_icons/icon_mirror_left.png"		},
		{"editor.mirror_right",				"resources/editor_icons/icon_mirror_right.png"		},
		{"editor.gizmo.translate",			"resources/editor_icons/icon_gizmo_translate.png"	},
		{"editor.gizmo.rotate",				"resources/editor_icons/icon_gizmo_rotate.png"		},
		{"editor.gizmo.scale",				"resources/editor_icons/icon_gizmo_scale.png"		},
		{"editor.browser.navback",			"resources/editor_icons/icon_back.png"				},
		{"editor.browser.navfwd",			"resources/editor_icons/icon_forward.png"			},
		{"editor.browser.refresh",			"resources/editor_icons/icon_refresh.png"			},
		{"editor.browser.folder",			"resources/editor_icons/icon_folder.png"			},
		{"editor.browser.file",				"resources/editor_icons/icon_whatfile.png"			},
		{"editor.browser.file.tex",			"resources/editor_icons/icon_texture.png"			},
		{"editor.browser.file.msh",			"resources/editor_icons/icon_mesh.png"				},
		{"editor.browser.file.lvl",			"resources/editor_icons/icon_level_old.png"			},
		{"editor.browser.file.smt",			"resources/editor_icons/icon_material.png"			},
		{"editor.browser.file.pmt",			"resources/editor_icons/icon_whatfile.png"			},
		{"editor.browser.file.blup",		"resources/editor_icons/icon_prefab_edit.png"		},
		{"editor.browser.file.skel",		"resources/editor_icons/icon_skeleton.png"			},
		{"editor.browser.file.0",			"resources/editor_icons/icon_whatfile.png"			},
		//{"editor.circle",					"resources/editor_icons/icon_circle.png"			},
		{"editor.cube",						"resources/editor_icons/icon_cube.png"				},
		{"editor.sphere",					"resources/editor_icons/icon_sphere.png"			},
		{"component.light.point",			"resources/editor_icons/icon_lightpoint.png"		},
		{"component.light.virtualpoint",	"resources/editor_icons/icon_lightpointv.png"		},
		{"component.light.spot",			"resources/editor_icons/icon_lightspot.png"			},
		{"component.light.directional",		"resources/editor_icons/icon_lightdir.png"			},
		{"component.audio",					"resources/editor_icons/icon_audio.png"				},
		{"component.camera",				"resources/editor_icons/icon_camera.png"			},
		{"component.expfog",				"resources/editor_icons/icon_exponential_fog.png"	},
		{"component.reflection",			"resources/editor_icons/icon_what.png"				},
		{"component.aovolume",				"resources/editor_icons/icon_what.png"				},
		{"component.skylight",				"resources/editor_icons/icon_skylight.png"			},
		{"component.skybox",				"resources/editor_icons/icon_cube.png"				},
	};

	void _special_assets::_editor_icons_load(S3DDevice& device, uPtr<SmartDescriptorSet>& descriptor) {
		for (int i = 0; i < sizeof(_editor_icons_array) / sizeof(_editor_icons_array[0]); i++) {
			auto& readIco = _editor_icons_array[i][1];
			TextureLoadInfo loadInfo{};
			loadInfo.filter = (readIco == "assets/engine/textures/.editor/icon_null") ? TextureFilter::Nearest : TextureFilter::Linear;
			loadInfo.enableMipMaps = false;
			loadInfo.compression = TextureCompression::Lossless;
			loadInfo.channels = TextureChannels::RGBA;
			int f{};
			uint8_t* pixels = ImageResource::getSTBImage(_editor_icons_array[i][1], &f, &f, &f, 4);
			ktxTextureCreateInfo txCreateInfo{};
			memset(&txCreateInfo, 0, sizeof(ktxTextureCreateInfo));
			txCreateInfo.baseWidth = 256;
			txCreateInfo.baseHeight = 256;
			txCreateInfo.baseDepth = 1;
			txCreateInfo.generateMipmaps = KTX_FALSE; // generate mip maps even if not used
			txCreateInfo.vkFormat = VK_FORMAT_R8G8B8A8_SRGB;
			txCreateInfo.numDimensions = 1;
			txCreateInfo.isArray = KTX_FALSE;
			txCreateInfo.numLevels = 1;
			txCreateInfo.numLayers = 1;
			txCreateInfo.numFaces = 1;
			txCreateInfo.numDimensions = 2;
			ktxTexture2* ktxTex = nullptr;
			ktxResult cErr = ktxTexture2_Create(&txCreateInfo, KTX_TEXTURE_CREATE_ALLOC_STORAGE, &ktxTex);
			ktxResult sifmErr = ktxTexture_SetImageFromMemory(ktxTexture(ktxTex), 0, 0, 0, reinterpret_cast<ktx_uint8_t*>(pixels), 256 * 256 * 4);
			ImageResource::freeSTBImage(pixels);
			Texture2D* tex =
				new Texture2D(device, descriptor, ktxTex,
					loadInfo);
			ktxTexture_Destroy(ktxTexture(ktxTex));
			_editor_icons[_editor_icons_array[i][0]] = tex;
		}
	}

	void Shard3D::_special_assets::_editor_icons_destroy() {
		for (const auto& [str, icon]: _editor_icons) {
			delete icon;
		}
		_editor_icons.clear();
	}
	std::unordered_map<std::string, Texture2D*>& _special_assets::_get()
	{
		return _editor_icons;
	}
}
#pragma endregion