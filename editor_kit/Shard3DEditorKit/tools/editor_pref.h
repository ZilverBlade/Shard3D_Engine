#pragma once

namespace Shard3D {
	enum class IDEVersion {
		VisualStudio2005,
		VisualStudio2008,
		VisualStudio2010,
		VisualStudio2012,
		VisualStudio2013,
		VisualStudio2015,
		VisualStudio2017,
		VisualStudio2019,
		VisualStudio2022
	};
	enum class EditorTheme {
		Dark,
		Light,
		Abyss,
		Default = Dark
	};
	enum class ColorPickerFormat {
		Integer,
		FloatingPoint,
		Hexadecimal
	};
	class EditorPreferences {
	public:
		static void load();
		static void save();
		static inline EditorTheme theme = EditorTheme::Default;
		static inline IDEVersion ideVersion = IDEVersion::VisualStudio2019;
		static inline float assetExplorerThumbnailSize = 96.0f;
		static inline enum class ColorPickerFormat colorPickFmt = ColorPickerFormat::Integer;
	};
}