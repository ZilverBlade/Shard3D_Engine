#include "script_gen.h"
#include <Shard3D/s3dstd.h>
#include <Shard3D/core.h>
#include <shellapi.h>
#include <fstream>
#include <sstream>
#include <filesystem>
#include <ShlObj_core.h>

namespace Shard3D {
	void ScriptFileGenerator::generate(std::string premakeexe, const ScriptFileGenerationCreateInfo& createInfo, std::string scriptDestination) {
		LPVOID dialog;
		HRESULT res = CoCreateInstance(CLSID_ProgressDialog, nullptr, CLSCTX_INPROC_SERVER, IID_IProgressDialog, &dialog);
		reinterpret_cast<IProgressDialog*>(dialog)->SetTitle(L"Script Engine");
		reinterpret_cast<IProgressDialog*>(dialog)->SetLine(1, L"Generating Script Engine Files...", false, nullptr);
		reinterpret_cast<IProgressDialog*>(dialog)->StartProgressDialog(nullptr, nullptr, PROGDLG_NOMINIMIZE | PROGDLG_NOCANCEL | PROGDLG_MARQUEEPROGRESS, nullptr);

		reinterpret_cast<IProgressDialog*>(dialog)->SetLine(2, L"Preprocessing Project Files...", false, nullptr);
		std::ifstream open{ createInfo.projectGenFile };
		std::stringstream textr{};
		textr << open.rdbuf();
		open.close();
		std::string text = textr.str();
		strUtils::replace(text, "\\$MyGameNameTemplate", createInfo.gameName);
		strUtils::replace(text, "\\$MyGameNameLocationFolderName", createInfo.gameName);
		strUtils::replace(text, "\\$MyGameNameLocationFolderName", createInfo.gameName);
		strUtils::replace(text, "\\$CppVersion", createInfo.cppVersion);
		strUtils::replace(text, "\\$EnableMultiProcessorCompile", createInfo.enableMPC ? "\"MultiProcessorCompile\"" : "");
		
		if (createInfo.allowUnsafeCode) strUtils::replace(text, "\\$UnsafeOptionGen", "clr \"Unsafe\"");
		else strUtils::replace(text, "\\$UnsafeOptionGen", "");

		std::string csIncludeText;
		for (int i = 0; i < createInfo.csInclude.size(); i++) {
			if (i + 1 != createInfo.csInclude.size()) {
				csIncludeText += "\"" + createInfo.csInclude[i] + "\",\n";
			} else {
				csIncludeText += "\"" + createInfo.csInclude[i] + "\"";
			}
		}
		if (createInfo.csInclude.size() > 0) {
			strUtils::replace(text, "\\$CsIncludes", csIncludeText);
		} else {
			strUtils::replace(text, "\\$CsIncludes,", "");
		}
		std::string cppIncludeLibsText;
		for (int i = 0; i < createInfo.cppIncludeLibs.size(); i++) {
			if (i + 1 != createInfo.cppIncludeLibs.size()) {
				cppIncludeLibsText += "\"" + createInfo.cppIncludeLibs[i] + "\",\n";
			} else {
				cppIncludeLibsText += "\"" + createInfo.cppIncludeLibs[i] + "\"";
			}
		}
		strUtils::replace(text, "\\$CppIncludesLibs", cppIncludeLibsText);
		std::string cppIncludeText;
		for (int i = 0; i < createInfo.cppInclude.size(); i++) {
			if (i + 1 != createInfo.cppInclude.size()) {
				cppIncludeText += "\"" + createInfo.cppInclude[i] + "\",\n";
			} else {
				cppIncludeText += "\"" + createInfo.cppInclude[i] + "\"";
			}
		}
		strUtils::replace(text, "\\$CppIncludesHeaders", cppIncludeText);
		std::string cppLinkLibsDebugText;
		for (int i = 0; i < createInfo.cppLinkLibsDebug.size(); i++) {
			if (i + 1 != createInfo.cppLinkLibsDebug.size()) {
				cppLinkLibsDebugText += "\"" + createInfo.cppLinkLibsDebug[i] + "\",\n";
			} else {
				cppLinkLibsDebugText += "\"" + createInfo.cppLinkLibsDebug[i] + "\"";
			}
		}
		strUtils::replace(text, "\\$CppLinksDebug", cppLinkLibsDebugText);
		std::string cppLinkLibsReleaseText;
		for (int i = 0; i < createInfo.cppLinkLibsRelease.size(); i++) {
			if (i + 1 != createInfo.cppLinkLibsRelease.size()) {
				cppLinkLibsReleaseText += "\"" + createInfo.cppLinkLibsRelease[i] + "\",\n";
			} else {
				cppLinkLibsReleaseText += "\"" + createInfo.cppLinkLibsRelease[i] + "\"";
			}
		}
		strUtils::replace(text, "\\$CppLinksRelease", cppLinkLibsReleaseText);

		if (!std::filesystem::exists(scriptDestination)) std::filesystem::create_directories(scriptDestination);
		std::ofstream out{ scriptDestination + "/premake5.lua" };
		out << text;
		out.flush();
		out.close();

		if (!std::filesystem::exists(scriptDestination + "/include")) std::filesystem::create_directory(scriptDestination + "/include");
		if (!std::filesystem::exists(scriptDestination + "/cpp")) std::filesystem::create_directory(scriptDestination + "/cpp");
		if (!std::filesystem::exists(scriptDestination + "/cpp/include")) std::filesystem::create_directory(scriptDestination + "/cpp/include");
		if (!std::filesystem::exists(scriptDestination + "/cpp/lib")) std::filesystem::create_directory(scriptDestination + "/cpp/lib");
		if (!std::filesystem::exists(scriptDestination + "/cpp/src")) std::filesystem::create_directory(scriptDestination + "/cpp/src");
		if (!std::filesystem::exists(scriptDestination + "/include/s3dcorelib.dll")) std::filesystem::copy(ENGINE_SCRIPT_LIBRARY_PATH"/s3dcorelib.dll", scriptDestination + "/include/s3dcorelib.dll");

		reinterpret_cast<IProgressDialog*>(dialog)->SetLine(2, L"Copying Files...", false, nullptr);
		if (std::string loc = scriptDestination + "/example.cs"; !std::filesystem::exists(loc))
			std::filesystem::copy(createInfo.templateFileCS, loc);

		std::string cppRead = IOUtils::readText(createInfo.templateFileCPP);
		strUtils::replace(cppRead, "\\$CurrentEngineVersion", std::to_string(ENGINE_VERSION.getVersionInt()));
		if (!std::filesystem::exists(scriptDestination + "/cpp/src/linker.cpp"))
			IOUtils::writeText(cppRead.c_str(), scriptDestination + "/cpp/src/linker.cpp");

		reinterpret_cast<IProgressDialog*>(dialog)->SetLine(2, L"Generating Project...", false, nullptr);
		std::string vsVer{};
		switch (createInfo.ide) {
		case (IDEVersion::VisualStudio2005): vsVer = "vs2005"; break;
		case (IDEVersion::VisualStudio2008): vsVer = "vs2008"; break;
		case (IDEVersion::VisualStudio2010): vsVer = "vs2010"; break;
		case (IDEVersion::VisualStudio2012): vsVer = "vs2012"; break;
		case (IDEVersion::VisualStudio2013): vsVer = "vs2013"; break;
		case (IDEVersion::VisualStudio2015): vsVer = "vs2015"; break;
		case (IDEVersion::VisualStudio2017): vsVer = "vs2017"; break;
		case (IDEVersion::VisualStudio2019): vsVer = "vs2019"; break;
		case (IDEVersion::VisualStudio2022): vsVer = "vs2022"; break;
		default:
			vsVer = "vs2013";
		}
		if (!std::filesystem::exists(scriptDestination + "/premake5.exe")) std::filesystem::copy(premakeexe, scriptDestination + "/premake5.exe");
		SHARD3D_WAITFOR(1);
		HINSTANCE result = ShellExecuteA(nullptr, "open", std::string(scriptDestination + "/premake5.exe").c_str(), vsVer.c_str(), scriptDestination.c_str(), true);
		SHARD3D_WAITFOR(2);
		DeleteFileA(std::string(scriptDestination + "/premake5.exe").c_str());
		if (std::filesystem::exists(scriptDestination + "/premake5.lua.log"))
			DeleteFileA(std::string(scriptDestination + "/premake5.lua.log").c_str());
		std::filesystem::copy(scriptDestination + "/premake5.lua", scriptDestination + "/premake5.lua.log");
		DeleteFileA(std::string(scriptDestination + "/premake5.lua").c_str());
		reinterpret_cast<IProgressDialog*>(dialog)->StopProgressDialog();

		if (createInfo.openProjectAfterDone) {
			HINSTANCE result2 = ShellExecuteA(nullptr, "open", std::string(scriptDestination + "/" + createInfo.gameName + ".sln").c_str(), vsVer.c_str(), scriptDestination.c_str(), SW_SHOW);
		}
	}
}