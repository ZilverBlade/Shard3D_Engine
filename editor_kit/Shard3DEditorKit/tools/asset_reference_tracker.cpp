#include "asset_reference_tracker.h"
#include <filesystem>
#include <fstream>	
#include <Shard3D/utils/json_ext.h>

#include <Shard3D/core.h>
#include <Shard3D/core/misc/UUID.h>
#include <Shard3D/core/asset/assetmgr.h>
#include <Shard3D/utils/json_ext.h>

namespace Shard3D {
	static std::unordered_map<AssetID, AssetReferences> lookupTable;

	static std::unordered_map<AssetType, std::unordered_set<AssetID>> assetList;

	static void load() {
		SHARD3D_INFO("Discovering assets");
		uint32_t assetCount = 0;
		for (const auto& entry : std::filesystem::recursive_directory_iterator(ProjectSystem::getAssetLocation())) {
			if (!entry.is_regular_file()) continue;
			std::string assetPath = entry.path().string();
			AssetType assetType = ResourceSystem::discoverAssetType(assetPath);
			if (assetType == AssetType::Unknown) continue;
			std::string reldp = std::filesystem::relative(assetPath, std::filesystem::path(ProjectSystem::getAssetLocation())).string();
			AssetID asset = AssetID(reldp);

			simdjson::dom::parser parser;
			simdjson::padded_string json = simdjson::padded_string::load(assetPath);
			auto& data = parser.parse(json);
			if (int ec = data.error(); ec != simdjson::error_code::SUCCESS) {
				continue;
			}

			switch (assetType) {
			case(AssetType::Material): {
					auto config = data["material"]["config"];
					if (config["emissiveTex"].error() == simdjson::error_code::SUCCESS)
						AssetReferenceTracker::addReference(asset, AssetID(std::string(config["emissiveTex"].get_string().value())));
					if (config["diffuseTex"].error() == simdjson::error_code::SUCCESS)
						AssetReferenceTracker::addReference(asset, AssetID(std::string(config["diffuseTex"].get_string().value())));
					if (config["normalTex"].error() == simdjson::error_code::SUCCESS)
						AssetReferenceTracker::addReference(asset, AssetID(std::string(config["normalTex"].get_string().value())));
					if (config["specularTex"].error() == simdjson::error_code::SUCCESS)
						AssetReferenceTracker::addReference(asset, AssetID(std::string(config["specularTex"].get_string().value())));
					if (config["glossinessTex"].error() == simdjson::error_code::SUCCESS)
						AssetReferenceTracker::addReference(asset, AssetID(std::string(config["glossinessTex"].get_string().value())));
					if (config["reflectivityTex"].error() == simdjson::error_code::SUCCESS)
						AssetReferenceTracker::addReference(asset, AssetID(std::string(config["reflectivityTex"].get_string().value())));
					if (config["maskTex"].error() == simdjson::error_code::SUCCESS)
						AssetReferenceTracker::addReference(asset, AssetID(std::string(config["maskTex"].get_string().value())));
					if (config["opacityTex"].error() == simdjson::error_code::SUCCESS)
						AssetReferenceTracker::addReference(asset, AssetID(std::string(config["opacityTex"].get_string().value())));
				} assetList[AssetType::Material].insert(asset);
				break;
			case(AssetType::Prefab):
				assetList[AssetType::Prefab].insert(asset);
				// no this is not a mistake, dont break; after prefab insertion since prefabs are essentially levels too
			case(AssetType::Level): {
					if (data["assets"].error() == simdjson::SUCCESS) {
						for (auto& entry : data["assets"].get_array()) {
							AssetReferenceTracker::addReference(asset, AssetID(std::string(entry.get_string().value())));
						}
					}
				}
				break;
			case(AssetType::Model3D): {
					if (data["materials"].error() == simdjson::SUCCESS) {
						for (auto& entry : data["materials"].get_array()) {
							AssetReferenceTracker::addReference(asset, AssetID(std::string(entry.get_string().value())));
						}
					}
				} assetList[AssetType::Model3D].insert(asset);
				break;
			case(AssetType::SkeletonRig):
			case(AssetType::SkeletonAnimation):
			case(AssetType::Texture2D):
			case(AssetType::TextureCube):
			case(AssetType::PhysicsAsset):
			case(AssetType::PhysicsMaterial):
			case(AssetType::PhysicsMaterialCollection):
				assetList[assetType].insert(asset);
				break;
			}

			assetCount++;
		}
		SHARD3D_INFO("Discovered {0} assets", assetCount);
	}

	static void replaceReferences(const AssetID& user, const AssetID& old_usee, const AssetID& new_usee) {
		std::string data = IOUtils::readText(user.getAsset());		
		size_t start_pos = 0;
		while ((start_pos = data.find(old_usee.getAsset(), start_pos)) != std::string::npos) {
			data.replace(start_pos, old_usee.getAsset().length(), new_usee.getAsset());
			start_pos += new_usee.getAsset().length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
		}
		IOUtils::writeText(data, user.getAsset());
	}

	void AssetReferenceTracker::initialize() {
		load();
	}
	void AssetReferenceTracker::destruct() {
	}

	bool AssetReferenceTracker::allocate(const AssetID& asset) {
		if (lookupTable.find(asset) != lookupTable.end()) return true;
		lookupTable[asset] = AssetReferences();
		assetList[ResourceSystem::discoverAssetType(asset.getAbsolute())].insert(asset);
		SHARD3D_INFO_E("Allocating asset '{0}'", asset.getAsset());
		return false;
	}

	void AssetReferenceTracker::addReference(const AssetID& user, const AssetID& usee) {
		lookupTable[user].usees.insert(usee);
		lookupTable[usee].users.insert(user);
		SHARD3D_INFO_E("Adding references from (usee) '{0}' to asset (user) '{1}'", usee.getAsset(), user.getAsset());
	}

	void AssetReferenceTracker::rmvReference(const AssetID& user, const AssetID& usee) {
		lookupTable[user].usees.erase(usee);
		lookupTable[usee].users.erase(user);
		SHARD3D_INFO_E("Removing references from (usee) '{0}' in asset (user) '{1}'", usee.getAsset(), user.getAsset());
	}

	void AssetReferenceTracker::rmvReferencePurge(const AssetID& asset) {
		for (auto& [user, relations] : lookupTable) {
			if (user.getID() == asset.getID()) continue;
			std::vector<AssetID> eraseUsees;
			std::vector<AssetID> eraseUsers;
			for (auto& u_user : relations.users)
				if (u_user == asset) eraseUsers.push_back(asset);
			for (auto& u_usee : relations.usees)
				if (u_usee == asset) eraseUsees.push_back(asset);
			for (const auto& u_usee : eraseUsees) 
				relations.usees.erase(asset);
			for (const auto& u_user : eraseUsers)
				relations.users.erase(asset);
		}
		lookupTable.erase(asset);
		assetList[ResourceSystem::discoverAssetType(asset.getAbsolute())].erase(asset);
	}

	void AssetReferenceTracker::swapUseeReference(const AssetID& user, const AssetID& old_usee, const AssetID& new_usee) {	
		lookupTable[old_usee].users.erase(user);
		lookupTable[new_usee].users.insert(user);
		lookupTable[user].usees.erase(old_usee);
		lookupTable[user].usees.insert(new_usee);
		SHARD3D_INFO_E("Replacing references from '{0}' to '{1}' in asset '{2}'", old_usee.getAsset(), new_usee.getAsset(), user.getAsset());
	}

	void AssetReferenceTracker::swapUserReference(const AssetID& usee, const AssetID& old_user, const AssetID& new_user) {
		lookupTable[old_user].users.erase(usee);
		lookupTable[new_user].users.insert(usee);
		lookupTable[usee].usees.erase(old_user);
		lookupTable[usee].usees.insert(new_user);
		SHARD3D_INFO_E("Replacing references from '{0}' to '{1}' in asset '{2}'", old_user.getAsset(), new_user.getAsset(), usee.getAsset());
	}

	void AssetReferenceTracker::swapReferenceBruteforce(const AssetID& old_asset, const AssetID& new_asset) {
		for (auto& [asset, relations] : lookupTable) {
			if (asset.getID() == old_asset.getID() || asset.getID() == new_asset.getID()) continue;
			if (std::find(relations.usees.begin(), relations.usees.end(), old_asset) != relations.usees.end()) {
				lookupTable[asset].usees.erase(old_asset);
				lookupTable[asset].usees.insert(new_asset);
				replaceReferences(asset, old_asset, new_asset);
			} else if (std::find(relations.users.begin(), relations.users.end(), old_asset) != relations.users.end()) {
				lookupTable[asset].users.erase(old_asset);
				lookupTable[asset].users.insert(new_asset);
			}
		}
		lookupTable[new_asset].users = std::move(lookupTable[old_asset].users);
		lookupTable[new_asset].usees = std::move(lookupTable[old_asset].usees);
		lookupTable.erase(old_asset);
	}

	AssetReferences& AssetReferenceTracker::getReferences(const AssetID& usee) {
		return lookupTable[usee];
	}

	const std::unordered_set<AssetID>& AssetReferenceTracker::getAssetList(AssetType type) {
		return assetList[type];
	}
	std::unordered_map<AssetID, AssetReferences>& AssetReferenceTracker::getLookupTable() {
		return lookupTable;
	}
	bool AssetReferenceTracker::isBeingUsed(const AssetID& asset) {		
		return lookupTable[asset].users.size() != 0;
	}
	bool AssetReferenceTracker::hasUseeReference(const AssetID& user, const AssetID& usee) {
		return std::find(lookupTable[user].usees.begin(), lookupTable[user].usees.end(), usee) != lookupTable[user].usees.end();
	}
}