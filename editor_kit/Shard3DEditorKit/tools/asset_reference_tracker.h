#pragma once
#include "../core.h"
#include <Shard3D/s3dstd.h>
#include <Shard3D/core/asset/AssetID.h>
#include <Shard3D/core/asset/assetmgr.h>
#include <unordered_set>
namespace Shard3D {
	struct AssetReferences {
		std::unordered_set<AssetID> users{};
		std::unordered_set<AssetID> usees{};
	};

	class AssetReferenceTracker {
	public:
		S3D_DLLAPI static void initialize();
		S3D_DLLAPI static void destruct();

/*
	* Allocates an entry in the lookup table
	*
	* @params
	* @param asset: asset to allocate
	*
	* @return (bool) was asset already allocated
	*
*/
		S3D_DLLAPI static bool allocate(const AssetID& asset);

/* 
	* Adds reference to another asset (e.g. is in use)
	* 
	* @params
	* @param user: asset using the other asset
	* @param usee: asset being used
	* 
*/
		S3D_DLLAPI static void addReference(const AssetID& user, const AssetID& usee);

/*
	* Removes reference to another asset (e.g. was destroyed or user was deleted)
	*
	* @params
	* @param user: asset using the other asset
	* @param usee: asset being used
	*
*/
		S3D_DLLAPI static void rmvReference(const AssetID& user, const AssetID& usee);

/*
	* Swaps references from other assets to a new one (e.g. was destroyed or usee was renamed)
	*
	* @params
	* @param user: asset using the other asset
	* @param old_usee: old asset being used, to be replaced
	* @param new_usee: new asset being used, that will replace the old asset
	*
*/
		S3D_DLLAPI static void swapUseeReference(const AssetID& user, const AssetID& old_usee, const AssetID& new_usee);

/*
	* Swaps references from other assets to a new one (e.g. was destroyed or usee was renamed)
	*
	* @params
	* @param usee: asset being used
	* @param old_user: old asset using the usee, being renamed
	* @param new_user: new asset using the usee, that will replace the old asset
	*
*/
		S3D_DLLAPI static void swapUserReference(const AssetID& usee, const AssetID& old_user, const AssetID& new_user);

/*
	* Swaps references from ALL other assets to a new one (e.g. was destroyed or usee was renamed)
	*
	* @params
	* @param old_asset: old asset to be replaced
	* @param new_asset: new asset, that will replace the old asset
	*
*/
		S3D_DLLAPI static void swapReferenceBruteforce(const AssetID& old_asset, const AssetID& new_asset);
/*
	* Removes reference from ALL assets (e.g. was destroyed or user was deleted)
	*
	* @params
	* @param asset: asset to be purged
	*
*/
		S3D_DLLAPI static void rmvReferencePurge(const AssetID& asset);

/*
	* Returns a list of assets being used by this asset, and vice versa, by the provided asset
	*
	* @params
	* @param asset: asset
	*
*/
		S3D_DLLAPI static AssetReferences& getReferences(const AssetID& asset);

		S3D_DLLAPI static const std::unordered_set<AssetID>& getAssetList(AssetType type);
		
/*
	* Returns the lookup table
	*
*/
		S3D_DLLAPI static std::unordered_map<AssetID, AssetReferences>& getLookupTable();

/*
	* Checks is asset is being used by something
	*
	* @params
	* @param asset: asset to check
	*
	* @return (bool) was asset already allocated
	*
*/
		S3D_DLLAPI static bool isBeingUsed(const AssetID& asset);

/*
	* Checks if usee is used by user
	*
	* @params
	* @param user: asset using the other asset
	* @param usee: asset being used
	*
*/
		S3D_DLLAPI static bool hasUseeReference(const AssetID& user, const AssetID& usee);
	};
}