#pragma once

#include "../core.h"

#include <Shard3D/core/ecs/level.h>
#include <Shard3D/core/ecs/actor.h>
#include <Shard3D/systems/handlers/resource_system.h>
#include <filesystem>

namespace Shard3D {
	class ResourceSystem;
	class AssetExplorerPanel {
		struct AssetEntryInfo {
			std::filesystem::directory_entry entry;
			VkDescriptorSet icon;
			AssetType type;
			std::filesystem::path relativePath;
			AssetID currentAsset = AssetID::null();
		};
		struct AssetIteratorCombo {
			std::vector<AssetEntryInfo> infos;
		};
	public:
		S3D_DLLAPI AssetExplorerPanel();
		S3D_DLLAPI ~AssetExplorerPanel();
		S3D_DLLAPI void setContext(sPtr<Level>& context, ResourceSystem* resourceSystem);
		S3D_DLLAPI void render();

		AssetID wantsTextureEditor = AssetID::null();
		AssetID wantsMaterialEditor = AssetID::null();
		AssetID wantsPhysicsEditor = AssetID::null();
		AssetID wantsHUDEditor = AssetID::null();
		AssetID wantsAnimationPreviewer = AssetID::null();
	private:

		// asset type, use filter
		std::unordered_map<AssetType, bool> filters;
		sPtr<Level> activelevel;

		std::string currentSelection{};
		std::string renamingAsset{};
		std::string renamingAssetOriginal{}; 

		VkDescriptorSet refreshIcon;
		VkDescriptorSet backIcon;
		VkDescriptorSet folderIcon;
		VkDescriptorSet fileIcon;
		VkDescriptorSet mesh3dIcon;
		VkDescriptorSet skeletonIcon;
		VkDescriptorSet textureIcon; 
		VkDescriptorSet levelIcon;
		VkDescriptorSet smatIcon;
		VkDescriptorSet pmatIcon;
		VkDescriptorSet prefbIcon;


		void refreshIterator(std::filesystem::path newPath);
		std::filesystem::path currentDir;
		
		std::string currentModalFileDest;
		std::string currentModalFileFile;
		TextureLoadInfo modalTextureLImport;
		TextureImportInfo modalTextureIImport;
		TextureLoadInfo modalTextureCLImport;
		TextureImportInfo modalTextureCIImport;
		Model3DImportInfo modalModelImport;

		AssetIteratorCombo directoryEntries;
		ResourceSystem* resourceSystem;
	};
}