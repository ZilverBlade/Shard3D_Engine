#pragma once

#include "../core.h"
#include <Shard3D/core/ecs/actor.h>
#include <Shard3D/core/ecs/components.h>
#include <Shard3D/core/rendering/window.h>

namespace Shard3D {
    namespace controller {
	    class EditorBallController {
	    public:
            S3D_DLLAPI void tryPollOrientation(GLFWwindow* window, float _dt, glm::vec4 screencontentArea, Actor pivotActor_, Actor cameraActor_);

            S3D_DLLAPI void scrollMouse(float deltaY);
   
            float moveSpeed{ 5.f };
            float speedModifier = 1.f;
            
            float sensitivity{ 15.f };

        private:
            struct ButtonMappings {
                int canRotate = GLFW_MOUSE_BUTTON_RIGHT;
                int canRotate2 = GLFW_MOUSE_BUTTON_LEFT;
            } buttons{};
             glm::vec3 orientation{ .0f, .0f, 6.28318530718f };
             glm::vec3 upVec{ 0.f, 1.f, 0.f };
             bool firstClick = true;
             ECS::Actor pivotActor{};
             ECS::Actor cameraActor{};
	    };

    }
}