#pragma once
#include <string>
#include <vector>
#include "editor_pref.h"
namespace Shard3D {
	struct ScriptFileGenerationCreateInfo {
		std::string projectGenFile;
		std::string templateFileCS;
		std::string templateFileCPP;

		std::string gameName;
		std::string cppVersion;
		bool enableMPC;
		bool allowUnsafeCode;
		IDEVersion ide;
		std::vector<std::string> csInclude;
		std::vector<std::string> cppInclude;
		std::vector<std::string> cppIncludeLibs;
		std::vector<std::string> cppLinkLibsDebug;
		std::vector<std::string> cppLinkLibsRelease;

		bool openProjectAfterDone;
	};
	class ScriptFileGenerator {
	public:
		static void generate(std::string premakeexe, const ScriptFileGenerationCreateInfo& createInfo, std::string scriptDestination);
	};
}