#include "reflection_probe_capture.h"
#include <Shard3D/core/asset/assetmgr.h>
#include <Shard3D/core/ecs/components.h>
#include <Shard3D/core/ecs/actor.h>
#include <glm/gtc/constants.hpp>
#include <filesystem>
#include <Shard3D/core/asset/image_tools.h>
#include <Shard3D/systems/handlers/vertex_arena_allocator.h>
#include <ktx.h>

//#include <renderdoc_app.h>

static glm::vec3 orientations[6]{
	{ 0.f, glm::half_pi<float>(), 0.f },	// right
	{ 0.f, -glm::half_pi<float>(), 0.f },	// left
	{ -glm::half_pi<float>(), 0.f, 0.f },	// up
	{ glm::half_pi<float>(), 0.f, 0.f },	// down
	{ 0.f, 0.f, 0.f },	// front
	{ 0.f, glm::pi<float>(), 0.f }		// back
};

static glm::vec3 orientations2[6]{
	{ 0.f, glm::half_pi<float>(), 0.f },	// right
	{ 0.f, -glm::half_pi<float>(), 0.f },	// left
	{ glm::half_pi<float>(), 0.f, 0.f },	// down
	{ -glm::half_pi<float>(), 0.f, 0.f },	// up
	{ 0.f, 0.f, 0.f },	// front
	{ 0.f, glm::pi<float>(), 0.f }		// back
};
enum orientation {
	o_up, o_down, o_left, o_right, o_front, o_back
};

const char* orienToStr(orientation or_) {
	switch (or_) {
	case (o_up): return "top";
	case (o_down): return "bottom";
	case (o_left): return "left";
	case (o_right): return "right";
	case (o_front): return "front";
	case (o_back): return "back";
	}
}

namespace Shard3D {
	struct ProcessingInfo {
	};
	
	StaticReflectionCapturePass::StaticReflectionCapturePass( glm::vec3 pos, sPtr<ECS::Level>& activeLevel, S3DDevice& dvc, ResourceSystem* resourceSystem, 
		uPtr<S3DDescriptorSetLayout>& globalSetLayout, uPtr<S3DDescriptorSetLayout>& sceneSetLayout, uPtr<S3DDescriptorSetLayout>& riggedSkeletonLayout, uPtr<S3DDescriptorSetLayout>& terrainSetLayout, float res, float supersample)
		: resolution(res), ssm(supersample), position(pos), device(dvc), globalSetLayout(globalSetLayout), sceneSetLayout(sceneSetLayout), resourceSystem(resourceSystem), riggedSkeletonLayout(riggedSkeletonLayout), terrainSetLayout(terrainSetLayout)
	{
		camera.setPerspectiveProjection(glm::half_pi<float>(), 1, 0.1f, 2048.f, ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit);
		localPool = S3DDescriptorPool::Builder(device)
			.setMaxSets(2)
			.addPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1)
			.addPoolSize(VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1)
			.setPoolFlags(VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT)
			.build();
		globalUBO = make_uPtr<S3DBuffer>(
				device,
				sizeof(GlobalUbo),
				1,
				VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT 
				);
		globalUBO->map();

		uboSetLayout = S3DDescriptorSetLayout::Builder(device)
			.addBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_ALL_GRAPHICS)
			.build();

		auto bufferInfo = globalUBO->descriptorInfo();
		S3DDescriptorWriter(*uboSetLayout, *localPool)
			.writeBuffer(0, &bufferInfo)
			.build(uboDescriptorSet);

		level = Level::copy(activeLevel);
	}

	StaticReflectionCapturePass::~StaticReflectionCapturePass() {

	}

	//RENDERDOC_API_1_5_0* rdoc_api = NULL;


	void StaticReflectionCapturePass::capture(const std::string& pathReflection, const std::string& pathIrr) {
		//pRENDERDOC_GetAPI RENDERDOC_GetAPI =
		//	(pRENDERDOC_GetAPI)GetProcAddress(GetModuleHandleA("renderdoc.dll"), "RENDERDOC_GetAPI");
		//int ret = RENDERDOC_GetAPI(eRENDERDOC_API_Version_1_5_0, (void**)&rdoc_api);
		//assert(ret == 1);

		for (auto& obj : level->registry.view<Components::IsVisibileComponent>()) {
			Actor actor = { obj, level.get() };

			if (actor.getComponent<Components::VisibilityComponent>().editorOnly) {
				actor.killComponent<Components::IsVisibileComponent>();
				if (actor.hasComponent<Components::Mesh3DComponent>())
					actor.killComponent<Components::Mesh3DComponent>();
			}
		}
		for (auto& obj : level->registry.view<Components::DynamicActorComponent>()) {
			Actor actor = { obj, level.get() };

			if (actor.hasComponent<Components::Mesh3DComponent>())
				actor.killComponent<Components::Mesh3DComponent>();
		}
		for (auto& obj : level->registry.view<Components::BoxReflectionCaptureComponent>()) {
			Actor actor = { obj, level.get() };
			level->killActor(actor);
		}
		for (auto& obj : level->registry.view<Components::TerrainComponent>()) {
			Actor actor = { obj, level.get() };
			actor.getComponent<Components::TerrainComponent>().loadAsset = true;
		}

		vkDeviceWaitIdle(device.device());

		levelRenderer = make_uPtr<LevelRenderInstance>(device, resourceSystem, globalSetLayout->getDescriptorSetLayout(), sceneSetLayout, riggedSkeletonLayout, terrainSetLayout, 1);
		level->runGarbageCollector();
		VkCommandBuffer levelGenCommandBuffer = device.beginSingleTimeCommands();
		resourceSystem->getVertexAllocator()->bind(levelGenCommandBuffer);
		levelRenderer->getRenderObjects().decalList->forceUpdateDecalsEDITORONLY(level);
		levelRenderer->render(levelGenCommandBuffer, 0, level);
		levelRenderer->runGarbageCollector(0, nullptr, level);
		levelRenderer->render(levelGenCommandBuffer, 0, level);
		device.endSingleTimeCommands(levelGenCommandBuffer);
		DeferredRenderSystem::RendererFeatures rendererFeatures{};
		rendererFeatures.enableSSAO = false;
		rendererFeatures.enableEarlyZ = true;
		rendererFeatures.enableTranslucency = true;
		rendererFeatures.enableNormalDecals = ProjectSystem::getEngineSettings().renderer.enableDeferredDecals;

		//rdoc_api->StartFrameCapture(NULL, NULL);
		for (int i = 0; i < 6; i++) {
			glm::vec2 rendering_resolution = glm::vec2{ resolution.x * ssm, resolution.y * ssm };
			camera.setViewYXZ(position, orientations[i]);
			SHARD3D_INFO("Capturing reflection capture {0} face index {1}", (void*)this, i);

			renderer[i].deferredRenderSystem = make_uPtr<DeferredRenderSystem>(device, resourceSystem, rendererFeatures,
				uboSetLayout->getDescriptorSetLayout(), sceneSetLayout->getDescriptorSetLayout(),
				riggedSkeletonLayout->getDescriptorSetLayout(), terrainSetLayout->getDescriptorSetLayout(), rendering_resolution);
			VolumetricLightingSystem* volumetricLighting = new VolumetricLightingSystem(
				device, resourceSystem, rendering_resolution, renderer[i].deferredRenderSystem->getGBufferAttachments(),
				renderer[i].deferredRenderSystem->getCompositionRenderPass()->getRenderPass(), uboSetLayout->getDescriptorSetLayout(), sceneSetLayout->getDescriptorSetLayout());
			SkyAtmosphereSystem* skyAtmosphere = new SkyAtmosphereSystem(device, resourceSystem, uboSetLayout->getDescriptorSetLayout(), 
				renderer[i].deferredRenderSystem->getCompositionRenderPass()->getRenderPass(), false);
			
			if (ProjectSystem::getEngineSettings().renderer.enableDeferredDecals) {
				renderer[i].decalRenderSystem = make_uPtr<DecalRenderSystem>(device, resourceSystem,
					uboSetLayout->getDescriptorSetLayout(), renderer[i].deferredRenderSystem->getGBufferAttachments());
				renderer[i].deferredRenderSystem->writeDescriptors(renderer[i].decalRenderSystem->getDBufferAttachments().dbuffer2->getDescriptor());
			} else {
				renderer[i].deferredRenderSystem->writeDescriptors();
			}
			
			VkCommandBuffer commandBuffer = device.beginSingleTimeCommands();
			FrameInfo frameInfo{
				0, // no frame index
				commandBuffer,
				camera,
				nullptr, // no telemetry
				uboDescriptorSet,
				levelRenderer->getDescriptorSet(0),
				level,
				levelRenderer->getRenderObjects(),
				resourceSystem
			};
			
			frameInfo.resourceSystem->getVertexAllocator()->bind(commandBuffer);
			GlobalUbo ubo{};
			ubo.projection = camera.getProjection();
			ubo.inverseProjection = camera.getInverseProjection();
			ubo.view = camera.getView();
			ubo.inverseView = camera.getInverseView();
			ubo.projectionView = camera.getProjection() * camera.getView();
			ubo.screenSize = rendering_resolution;
			ubo.globalClipPlane = glm::vec4(0.0, 0.0, 0.0, 999999.0);
			globalUBO->writeToBuffer(&ubo);
			globalUBO->flush();
			if (levelRenderer->getRenderObjects().terrainList->getTerrain()) { // maximum quality
				levelRenderer->getRenderObjects().terrainList->getTerrain()->updateVisibilityCalcs(frameInfo.commandBuffer, camera, 128.0f, -2.0f);
			}

			renderer[i].deferredRenderSystem->beginEarlyZPass(frameInfo);
			renderer[i].deferredRenderSystem->renderEarlyDepthTerrain(frameInfo);
			renderer[i].deferredRenderSystem->renderEarlyDepthMesh3D(frameInfo, MeshMobilityOption_Static);
			renderer[i].deferredRenderSystem->endEarlyZPass(frameInfo);
			renderer[i].deferredRenderSystem->beginGBufferPass(frameInfo);
			renderer[i].deferredRenderSystem->renderGBufferTerrain(frameInfo);
			renderer[i].deferredRenderSystem->renderGBufferMesh3D(frameInfo, MeshMobilityOption_Static);
			renderer[i].deferredRenderSystem->endGBufferPass(frameInfo);
			if (ProjectSystem::getEngineSettings().renderer.enableDeferredDecals) {
				renderer[i].decalRenderSystem->renderDecals(frameInfo);
				renderer[i].decalRenderSystem->applyDBuffer(frameInfo);
			}
			skyAtmosphere->renderSkyLightCubemap(frameInfo);
			renderer[i].deferredRenderSystem->composeLighting(frameInfo);
			renderer[i].deferredRenderSystem->beginCompositionPass(frameInfo);
			renderer[i].deferredRenderSystem->renderSkybox(frameInfo);
			skyAtmosphere->render(frameInfo);
			renderer[i].deferredRenderSystem->endComposition(frameInfo);
			renderer[i].deferredRenderSystem->beginBlendingPass(frameInfo);
			renderer[i].deferredRenderSystem->renderForwardMesh3D(frameInfo, MeshMobilityOption_Static);
			renderer[i].deferredRenderSystem->endBlendingPass(frameInfo);
			volumetricLighting->render(frameInfo);
			volumetricLighting->blur(frameInfo);
			renderer[i].deferredRenderSystem->beginCompositionPass(frameInfo);
			renderer[i].deferredRenderSystem->composeAlpha(frameInfo);
			volumetricLighting->composite(frameInfo);
			renderer[i].deferredRenderSystem->endComposition(frameInfo);
			
			device.endSingleTimeCommands(commandBuffer);
			
			SHARD3D_INFO("Processing reflection capture {0} face index {1}", (void*)this, i);
			process(renderer[i].deferredRenderSystem, i);

			delete skyAtmosphere;
			delete volumetricLighting;
		}
		store(pathReflection, pathIrr);
		//rdoc_api->EndFrameCapture(NULL, NULL);
		SHARD3D_INFO("Finished capturing reflection capture {0}", (void*)this);
	}
	void StaticReflectionCapturePass::process(uPtr<DeferredRenderSystem>& renderer, int ind) {
		ProcessingInfo info = { };
		processors[ind] = make_uPtr<SimpleProcessingSystem>(
			device,
			S3DShader{ShaderType::Fragment, "resources/shaders/editor/reflection_capture_processing.frag", "editor"},
			&renderer->getGBufferAttachments().colorAttachment->getDescriptor(),
			resolution,
			VK_FORMAT_R32_UINT,
			&info,
			sizeof(ProcessingInfo)
		);

		VkCommandBuffer commandBuffer = device.beginSingleTimeCommands();
		processors[ind]->process(commandBuffer);
		device.endSingleTimeCommands(commandBuffer);
	}
	void StaticReflectionCapturePass::store(const std::string& pathReflection, const std::string& patIrr) {
		const uint32_t GLOSSINESS_MIP_LEVELS = 6;

		ktxTexture2* ktxTex{};
		ktxTextureCreateInfo txCreateInfo{};
		memset(&txCreateInfo, 0, sizeof(ktxTextureCreateInfo));
		txCreateInfo.baseWidth = resolution.x;
		txCreateInfo.baseHeight = resolution.y;
		txCreateInfo.baseDepth = 1;
		txCreateInfo.generateMipmaps = KTX_FALSE;
		txCreateInfo.vkFormat = VK_FORMAT_E5B9G9R9_UFLOAT_PACK32;
		txCreateInfo.isArray = KTX_FALSE;
		txCreateInfo.numLevels = GLOSSINESS_MIP_LEVELS; // glossines 1.0, 0.8, 0.6, 0.4, 0.2, 0.0
		txCreateInfo.numLayers = 1;
		txCreateInfo.numFaces = 6;
		txCreateInfo.numDimensions = 2;

		ktxResult cErr = ktxTexture2_Create(&txCreateInfo, KTX_TEXTURE_CREATE_ALLOC_STORAGE, &ktxTex);
		if (cErr != KTX_SUCCESS) {
			SHARD3D_ERROR("Failed to allocate ktx texture!");
			return;
		}

		
		VkImage cubemapImage;
		VkImageView cubemapImageView;
		VkSampler cubemapSampler;
		VkDeviceMemory cubemapImageMemory;
		VkImageCreateInfo cubemapImageInfo{};
		cubemapImageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		cubemapImageInfo.imageType = VK_IMAGE_TYPE_2D;
		cubemapImageInfo.extent = { txCreateInfo.baseWidth, txCreateInfo.baseHeight, 1 };
		cubemapImageInfo.mipLevels = 1;
		cubemapImageInfo.arrayLayers = 6;
		cubemapImageInfo.format = VK_FORMAT_E5B9G9R9_UFLOAT_PACK32;
		cubemapImageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
		cubemapImageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		cubemapImageInfo.usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT |
			VK_IMAGE_USAGE_SAMPLED_BIT;
		cubemapImageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
		cubemapImageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		cubemapImageInfo.flags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;

		device.createImageWithInfo(
			cubemapImageInfo,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
			cubemapImage,
			cubemapImageMemory
		);

		VkImageViewCreateInfo cubemapImageViewInfo{};
		cubemapImageViewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		cubemapImageViewInfo.image = cubemapImage;
		cubemapImageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_CUBE;
		cubemapImageViewInfo.format = VK_FORMAT_E5B9G9R9_UFLOAT_PACK32;
		cubemapImageViewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		cubemapImageViewInfo.subresourceRange.baseMipLevel = 0;
		cubemapImageViewInfo.subresourceRange.levelCount = 1;
		cubemapImageViewInfo.subresourceRange.baseArrayLayer = 0;
		cubemapImageViewInfo.subresourceRange.layerCount = 6;
		VK_ASSERT(vkCreateImageView(device.device(), &cubemapImageViewInfo, nullptr, &cubemapImageView), "failed to create texture image view!");

		VkSamplerCreateInfo cubemapSamplerInfo{};
		cubemapSamplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		cubemapSamplerInfo.magFilter = VK_FILTER_LINEAR;
		cubemapSamplerInfo.minFilter = VK_FILTER_LINEAR;
		cubemapSamplerInfo.anisotropyEnable = VK_FALSE;
		cubemapSamplerInfo.maxAnisotropy = 1.0f; 
		cubemapSamplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
		cubemapSamplerInfo.unnormalizedCoordinates = VK_FALSE;
		cubemapSamplerInfo.compareEnable = VK_FALSE;
		cubemapSamplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
		cubemapSamplerInfo.mipLodBias = 0.0f;
		cubemapSamplerInfo.minLod = 0.0f;
		cubemapSamplerInfo.maxLod = 0.0f;

		VK_ASSERT(vkCreateSampler(device.device(), &cubemapSamplerInfo, nullptr, &cubemapSampler), "failed to create texture sampler!");


		VkCommandBuffer commandBuffer = device.beginSingleTimeCommands();
		device.transitionImageLayout(
			commandBuffer,
			cubemapImage,
			VK_FORMAT_E5B9G9R9_UFLOAT_PACK32,
			VK_IMAGE_LAYOUT_UNDEFINED,
			VK_IMAGE_LAYOUT_GENERAL,
			0,
			1,
			0,
			6
		);
		for (int f = 0; f < 6; f++) {
			VkImageSubresourceLayers subsrc;
			subsrc.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			subsrc.baseArrayLayer = 0;
			subsrc.layerCount = 1;
			subsrc.mipLevel = 0;
			VkImageSubresourceLayers subdst;
			subdst.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			subdst.baseArrayLayer = f;
			subdst.layerCount = 1;
			subdst.mipLevel = 0;
			device.copyImage(commandBuffer, { txCreateInfo.baseWidth, txCreateInfo.baseHeight, 1 },
				 processors[f]->getWrittenAttachment()->getImage(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, subsrc, {},
				cubemapImage, VK_IMAGE_LAYOUT_GENERAL, subdst, {}
			);
		}
		device.endSingleTimeCommands(commandBuffer);

		S3DShaderSpecialization specialization;
		specialization = {};
		const int sampleCount = 1024; // extra large sample count, we can afford extra precision for baking
		specialization.addConstant(0, sampleCount);

		VkDescriptorImageInfo inCubeDescriptorInfo{};
		inCubeDescriptorInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
		inCubeDescriptorInfo.imageView = cubemapImageView;
		inCubeDescriptorInfo.sampler = cubemapSampler;

		PostProcessingEffect* envSpecularGenPrePass = new PostProcessingEffect(
			device,
			{ txCreateInfo.baseWidth, txCreateInfo.baseHeight },
			S3DShader(ShaderType::Fragment, "resources/shaders/processing/envspecgen.frag", "misc", specialization),
			{ inCubeDescriptorInfo },
			VK_FORMAT_R16G16B16A16_SFLOAT,
			VK_IMAGE_VIEW_TYPE_CUBE,
			6,
			GLOSSINESS_MIP_LEVELS
		); 
		PostProcessingEffect* envSpecularGenFinalPass = new PostProcessingEffect(
			device,
			{ txCreateInfo.baseWidth, txCreateInfo.baseHeight },
			S3DShader(ShaderType::Fragment, "resources/shaders/processing/envspecgen_post.frag", "misc", specialization),
			{ envSpecularGenPrePass->getAttachment()->getDescriptor() },
			VK_FORMAT_R32_UINT,
			VK_IMAGE_VIEW_TYPE_CUBE,
			6,
			GLOSSINESS_MIP_LEVELS
		);
		commandBuffer = device.beginSingleTimeCommands();
		// pre pass
		for (int i = 0; i < 6; i++) {
			struct Push {
				glm::mat4 mvp;
				float glossiness;
			} push;
			S3DCamera camera{};
			camera.setViewYXZ({}, orientations2[i]);
			push.mvp = camera.getView();
			for (int j = 0; j < GLOSSINESS_MIP_LEVELS; j++) {
				push.glossiness = 1.0f - static_cast<float>(j) / static_cast<float>(GLOSSINESS_MIP_LEVELS - 1);
				envSpecularGenPrePass->render(commandBuffer, push, i, j);
			}
		}
		// high fidelity pass
		for (int i = 0; i < 6; i++) {
			struct Push {
				glm::mat4 mvp;
				float glossiness;
			} push;
			S3DCamera camera{};
			camera.setViewYXZ({}, orientations2[i]);
			push.mvp = camera.getView();
			for (int j = 0; j < GLOSSINESS_MIP_LEVELS; j++) {
				push.glossiness = 1.0f - static_cast<float>(j) / static_cast<float>(GLOSSINESS_MIP_LEVELS - 1);
				envSpecularGenFinalPass->render(commandBuffer, push, i, j);
			}
		}
		device.transitionImageLayout(commandBuffer, envSpecularGenFinalPass->getAttachment()->getImage(), VK_FORMAT_R32_UINT,
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
			VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
			0,
			GLOSSINESS_MIP_LEVELS,
			0,
			6
		);
		device.endSingleTimeCommands(commandBuffer);

		for (int l = 0; l < GLOSSINESS_MIP_LEVELS; l++) {
			VkBuffer copyBuffer;
			VkDeviceMemory copyBufferMemory;

			glm::ivec2 mipResolution = glm::ivec2(std::pow(2.0f, std::log2(txCreateInfo.baseWidth) - l));
			size_t imageSize = mipResolution.x * mipResolution.y * 4.0f * 6.0f;
			device.createBuffer(
				imageSize,
				VK_BUFFER_USAGE_TRANSFER_DST_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
				copyBuffer,
				copyBufferMemory
			);
			void* pixels{};
			vkMapMemory(device.device(), copyBufferMemory, 0, imageSize, 0, &pixels);
			VkCommandBuffer commandBuffer = device.beginSingleTimeCommands();


			device.copyImageToBuffer(commandBuffer, envSpecularGenFinalPass->getAttachment()->getImage(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
				copyBuffer, mipResolution.x, mipResolution.y, 6, l);
			device.endSingleTimeCommands(commandBuffer);

			for (int f = 0; f < 6; f++) {
				ktxResult sifmErr = ktxTexture_SetImageFromMemory(ktxTexture(ktxTex), l, 0, f, reinterpret_cast<ktx_uint8_t*>(pixels) + (imageSize / 6) * f, imageSize / 6);
				if (sifmErr != KTX_SUCCESS) SHARD3D_ERROR("Failed to save texture! {0}", sifmErr);
			}

			vkUnmapMemory(device.device(), copyBufferMemory);
			vkDestroyBuffer(device.device(), copyBuffer, nullptr);
			vkFreeMemory(device.device(), copyBufferMemory, nullptr);
		}

		AssetManager::exportBakedReflectionCube(ktxTex, pathReflection);

		specialization = {};
		const float sampleDelta = 0.0125f; // extra small steps, we can afford extra precision for baking
		specialization.addConstant(0, sampleDelta);
		PostProcessingEffect* envIrradianceGen = new PostProcessingEffect(
			device,
			{ 64, 64 },
			S3DShader(ShaderType::Fragment, "resources/shaders/processing/envirrgen.frag", "misc", specialization),
			{ inCubeDescriptorInfo },
			VK_FORMAT_R32_UINT,
			VK_IMAGE_VIEW_TYPE_CUBE,
			6,
			1
		);
		{
			VkCommandBuffer commandBuffer = device.beginSingleTimeCommands();
			for (int i = 0; i < 6; i++) {
				S3DCamera camera{};
				camera.setViewYXZ({}, orientations2[i]);
				glm::mat4 mvp = camera.getView();
				envIrradianceGen->render(commandBuffer, mvp, i);
			}
			device.endSingleTimeCommands(commandBuffer);
		}
		ktxTexture2* ktxTex2{};
		txCreateInfo.baseWidth = 64;
		txCreateInfo.baseHeight = 64;
		ktxResult cErr2 = ktxTexture2_Create(&txCreateInfo, KTX_TEXTURE_CREATE_ALLOC_STORAGE, &ktxTex2);
		if (cErr2 != KTX_SUCCESS) {
			SHARD3D_ERROR("Failed to allocate ktx texture!");
			return;
		}

		VkBuffer copyBuffer;
		VkDeviceMemory copyBufferMemory;

		size_t imageSize = 64 * 64 * 4 * 6;
		device.createBuffer(
			imageSize,
			VK_BUFFER_USAGE_TRANSFER_DST_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
			copyBuffer,
			copyBufferMemory
		);
		void* pixels{};
		vkMapMemory(device.device(), copyBufferMemory, 0, imageSize, 0, &pixels);
		{
			VkCommandBuffer commandBuffer = device.beginSingleTimeCommands();
			device.transitionImageLayout(commandBuffer, envIrradianceGen->getAttachment()->getImage(),
				envIrradianceGen->getAttachment()->getAttachmentDescription().format, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, 0, 1, 0, 6);
			device.copyImageToBuffer(commandBuffer, envIrradianceGen->getAttachment()->getImage(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, copyBuffer, 64, 64, 6, 0);
			device.endSingleTimeCommands(commandBuffer);
		}
		for (int i = 0; i < 6; i++) {
			ktxResult sifmErr = ktxTexture_SetImageFromMemory(ktxTexture(ktxTex2), 0, 0, i,
				reinterpret_cast<ktx_uint8_t*>(pixels) + (imageSize / 6) * i, imageSize / 6);
			if (sifmErr != KTX_SUCCESS) SHARD3D_ERROR("Failed to load texture!");
		}
		vkUnmapMemory(device.device(), copyBufferMemory);
		vkDestroyBuffer(device.device(), copyBuffer, nullptr);
		vkFreeMemory(device.device(), copyBufferMemory, nullptr);
		AssetManager::exportBakedReflectionCube(ktxTex2, patIrr);
		TextureImporter::freeKTX(ktxTex);
		TextureImporter::freeKTX(ktxTex2);

		vkDestroyImage(device.device(), cubemapImage, nullptr);
		vkDestroyImageView(device.device(), cubemapImageView, nullptr);
		vkDestroySampler(device.device(), cubemapSampler, nullptr);
		vkFreeMemory(device.device(), cubemapImageMemory, nullptr);

		delete envSpecularGenPrePass;
		delete envSpecularGenFinalPass;
		delete envIrradianceGen;
		// attachments deleted by the processors
	}
}
