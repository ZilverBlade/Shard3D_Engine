#pragma once

#include "../core.h"
#include <Shard3D/core/ecs/actor.h>
#include <Shard3D/core/ecs/components.h>
#include <Shard3D/core/rendering/window.h>

namespace Shard3D {
    namespace controller {
	    class EditorMovementController {
	    public:
            S3D_DLLAPI void tryPollTranslation(S3DWindow& _window, float _dt, const ECS::Actor& _actor);
            S3D_DLLAPI void tryPollOrientation(S3DWindow& _window, float _dt, glm::vec4 screencontentArea, const ECS::Actor& _actor);

            S3D_DLLAPI void scrollMouse(S3DWindow& _window, float deltaY);
   
            float moveSpeedExponent{ 2.f };
            float speedModifier = 1.f;
            
            float sensitivity{ 15.f };


            glm::vec3 move{ 0.f };
            const float drag = 7.0f;
            const float acceleration = 10.0f;
            float currentSpeed = 0.0f;
        private:
            struct KeyMappings {
                int moveLeft = GLFW_KEY_A;
                int moveRight = GLFW_KEY_D;
                int moveForward = GLFW_KEY_W;
                int moveBackward = GLFW_KEY_S;
                int moveUp = GLFW_KEY_SPACE;
                int moveDown = GLFW_KEY_RIGHT_CONTROL;
                int slowDown = GLFW_KEY_LEFT_CONTROL;
                int speedUp = GLFW_KEY_LEFT_SHIFT;
            } keys{};

            struct ButtonMappings {
                int canRotate = GLFW_MOUSE_BUTTON_RIGHT;
            } buttons{};
             glm::vec3 orientation{ .0f, .0f, 6.28318530718f };
             glm::vec3 upVec{ 0.f, 1.f, 0.f };
             bool firstClick = true;
             ECS::Actor actor{};
	    };

    }
}