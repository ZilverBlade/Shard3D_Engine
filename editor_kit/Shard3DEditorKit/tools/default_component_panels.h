#pragma once

#include "../core.h"
#include <Shard3D/ecs.h>
using namespace Shard3D::ECS;
namespace Shard3D {
	class DefaultComponentPanels {
	public:
		S3D_DLLAPI DefaultComponentPanels();
		S3D_DLLAPI ~DefaultComponentPanels();

		S3D_DLLAPI void render();
	private:
		S3D_DLLAPI void createIcons();
		struct _icons {
			VkDescriptorSet pointlight, vpl, spotlight, sun,
				camera, 
				cube, sphere,
				reflection, preflection, expfog;
		} icons;
	};
}