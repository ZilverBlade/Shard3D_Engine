#include <Shard3DEditorKit/kit.h>
#include "default_component_panels.h"
#include <imgui_internal.h>
#include <Shard3DEditorKit/gui/icons.h>
namespace Shard3D {
	static void drawImageButtonWithText(ImTextureID textureID, const char* label, const ImVec2& size, size_t payloadHashCode) {
		ImGui::Image(textureID, size); 
		ImGui::SameLine();
		ImGui::Selectable(label, false, ImGuiSelectableFlags_SelectOnClick);
		if (ImGui::BeginDragDropSource()) {
			ImGui::SetDragDropPayload("SHARD3D.COMPONENTS.DROP", &payloadHashCode, sizeof(size_t), ImGuiCond_Once);
			ImGui::EndDragDropSource();
		}
	}

	DefaultComponentPanels::DefaultComponentPanels() {
		createIcons();
	}
	DefaultComponentPanels::~DefaultComponentPanels() {
	}

	void DefaultComponentPanels::createIcons() {
		{
			auto& img = _special_assets::_get().at("component.light.point");
			icons.pointlight = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("component.light.spot");
			icons.spotlight = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("component.light.virtualpoint");
			icons.vpl = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("component.light.directional");
			icons.sun = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("component.expfog");
			icons.expfog = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("component.camera");
			icons.camera = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("editor.cube");
			icons.cube = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("editor.sphere");
			icons.sphere = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("component.reflection");
			icons.reflection = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("editor.browser.file.tex");
			icons.preflection = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
	}

	void DefaultComponentPanels::render() {
		ImGui::Begin("Quick Actor Menu");
		if (ImGui::BeginTabBar("_quickactormenutabbar")) {
			if (ImGui::BeginTabItem("Generic")) {
				if (ImGui::BeginListBox("##genericactorlistboxpanel", ImGui::GetContentRegionAvail())) {
					drawImageButtonWithText(getImGuiIcon().level, "Terrain Actor (WIP)", { 32.f, 32.f }, typeid(Components::TerrainComponent).hash_code());
					drawImageButtonWithText(icons.sphere, "Sun Sky Actor", { 32.f, 32.f }, typeid(Components::SkyAtmosphereComponent).hash_code());
					drawImageButtonWithText(icons.pointlight, "Point Light Actor", { 32.f, 32.f }, typeid(Components::PointLightComponent).hash_code());
					drawImageButtonWithText(icons.spotlight, "Spot Light Actor", { 32.f, 32.f }, typeid(Components::SpotLightComponent).hash_code());
					drawImageButtonWithText(icons.camera, "Camera Actor", { 32.f, 32.f }, typeid(Components::CameraComponent).hash_code());
					drawImageButtonWithText(icons.cube, "Deferred Decal", { 32.f, 32.f }, typeid(Components::DeferredDecalComponent).hash_code());
					drawImageButtonWithText(icons.cube, "Cube (Static)", { 32.f, 32.f }, 32325235);
					drawImageButtonWithText(icons.sphere, "Sphere (Static)", { 32.f, 32.f }, 623456);
					drawImageButtonWithText(icons.reflection, "Reflection Capture Actor", { 32.f, 32.f }, typeid(Components::BoxReflectionCaptureComponent).hash_code());
					drawImageButtonWithText(icons.cube, "PostFX Volume Actor", { 32.f, 32.f }, typeid(Components::PostFXComponent).hash_code());
					ImGui::EndListBox();
				}
				ImGui::EndTabItem();
			}
			if (ImGui::BeginTabItem("Lights")) {
				if (ImGui::BeginListBox("##lightsactorlistboxpanel", ImGui::GetContentRegionAvail())) {
					drawImageButtonWithText(icons.sun, "Directional Light Actor", { 32.f, 32.f }, typeid(Components::DirectionalLightComponent).hash_code());
					drawImageButtonWithText(icons.pointlight, "Point Light Actor", { 32.f, 32.f }, typeid(Components::PointLightComponent).hash_code());
					drawImageButtonWithText(icons.spotlight, "Spot Light Actor", { 32.f, 32.f }, typeid(Components::SpotLightComponent).hash_code());
					ImGui::EndListBox();
				}
				ImGui::EndTabItem();
			}
			if (ImGui::BeginTabItem("Visual Effects")) {
				if (ImGui::BeginListBox("##visualeffectsactorlistboxpanel", ImGui::GetContentRegionAvail())) {
					drawImageButtonWithText(icons.expfog, "Exponential Fog Actor", { 32.f, 32.f }, typeid(Components::ExponentialFogComponent).hash_code());
					drawImageButtonWithText(icons.vpl, "Virtual Point Light Actor", { 32.f, 32.f }, typeid(Components::VirtualPointLightComponent).hash_code());
					drawImageButtonWithText(icons.reflection, "Reflection Capture Actor", { 32.f, 32.f }, typeid(Components::BoxReflectionCaptureComponent).hash_code());
					drawImageButtonWithText(icons.reflection, "Ambient Occlusion Volume Actor", { 32.f, 32.f }, typeid(Components::BoxAmbientOcclusionVolumeComponent).hash_code());
					drawImageButtonWithText(icons.preflection, "Planar Reflection Actor", { 32.f, 32.f }, typeid(Components::PlanarReflectionComponent).hash_code());
					ImGui::EndListBox();
				}
				ImGui::EndTabItem();
			}
			ImGui::EndTabBar();
		}
		ImGui::End();
	}
}
