#include "editor_ball_controller.h"
#include <limits>
#include <iostream>

#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/vector_angle.hpp>

namespace Shard3D {
	namespace controller {
		void EditorBallController::tryPollOrientation(GLFWwindow* window, float _dt, glm::vec4 screencontentArea, Actor pivotActor_, Actor cameraActor_) {
			pivotActor = pivotActor_;
			cameraActor = cameraActor_;
			if (!window) return;
			if (glfwGetMouseButton(window, buttons.canRotate) == GLFW_PRESS) {
				//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

				double mouseX;
				double mouseY;
				glfwGetCursorPos(window, &mouseX, &mouseY);

				static float lastRotX = 0.0f;
				static float lastRotY = 0.0f;
				// Prevents camera from jumping on the first click
				if (firstClick) {
					lastRotX = (float)((mouseY - screencontentArea.y) - (screencontentArea.w / 2.f)) / screencontentArea.w;
					lastRotY = (float)((mouseX - screencontentArea.x) - (screencontentArea.z / 2.f)) / screencontentArea.z;
					firstClick = false;
				}

				float mouseDeltaX = (float)((mouseY - screencontentArea.y) - (screencontentArea.w / 2.f)) / screencontentArea.w;
				float mouseDeltaY = (float)((mouseX - screencontentArea.x) - (screencontentArea.z / 2.f)) / screencontentArea.z;


				float rotX = mouseDeltaX - lastRotX;
				float rotY = mouseDeltaY - lastRotY;

				lastRotX = mouseDeltaX;
				lastRotY = mouseDeltaY;

				// force the roll to be pi*2 radians
				orientation.z = 6.283185482f;

				// up down rotation
				orientation = glm::rotate(orientation, glm::radians(sensitivity * rotX), upVec);
				// to make sure it doesnt over-rotate			
				orientation.x = glm::clamp(orientation.x, -1.5707963f, 1.5707963f);

				// left right rotation
				orientation = glm::rotate(orientation, glm::radians(sensitivity * rotY), glm::normalize(glm::cross(orientation, upVec)));
			}
			else { glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL); firstClick = true; }

			pivotActor.getComponent<Components::TransformComponent>().setRotation({ orientation.x, orientation.y, 0.f });
		}
		void EditorBallController::scrollMouse(float deltaY) {
			glm::vec3 prevTranslation = cameraActor.getTransform().getTranslation();
			prevTranslation.z += deltaY * 0.2f;
			prevTranslation.z = glm::clamp(prevTranslation.z, -8.0f, -0.5f);
			cameraActor.getTransform().setTranslation(prevTranslation);
		}
	}
}