#include <glm/gtc/constants.hpp>

#include "../tools/editor_assets.h"
#include "editor_billboard_renderer.h"
#include <Shard3D/core/asset/assetmgr.h>
#include <Shard3D/core/ecs/components.h>
#include <Shard3D/systems/computational/convex_hull_generator.h>
#include <Shard3D/systems/handlers/render_list.h>
#include <Shard3D/systems/rendering/terrain_system.h>
namespace Shard3D {

	struct Billboard {
		glm::vec4 position{};
		uint32_t textureID;
	};
	struct BoxPushConstant {
		glm::mat4 transformMatrix{};
		glm::vec4 color{};
		float transition_bounds{};
	};
	struct GenericTransformColorPushConstant {
		glm::mat4 transformMatrix{};
		glm::vec4 color{};
	};
	struct MeshVolumePushConstant {
		glm::mat4 transformMatrix{};
		glm::vec4 color{};
		alignas(16)glm::vec3 maxXYZ{};
		alignas(16)glm::vec3 minXYZ{};
	};
	EditorBillboardRenderer::EditorBillboardRenderer(S3DDevice& device, ResourceSystem* resourceSystem, VkRenderPass opaqueRenderPass, VkRenderPass translucentRenderPass, VkDescriptorSetLayout globalSetLayout) 
		: engineDevice{ device }, resourceSystem(resourceSystem) {
		createPipelineLayout(globalSetLayout);
		createPipeline(opaqueRenderPass, translucentRenderPass);
	}
	EditorBillboardRenderer::~EditorBillboardRenderer() {
		vkDestroyPipelineLayout(engineDevice.device(), pipelineLayout, nullptr);
		vkDestroyPipelineLayout(engineDevice.device(), boxPipelineLayout, nullptr);
		vkDestroyPipelineLayout(engineDevice.device(), meshVolumePipelineLayout, nullptr);
	}
	
	void EditorBillboardRenderer::createPipelineLayout(VkDescriptorSetLayout globalSetLayout) {
		
		{
			VkPushConstantRange pushConstantRange{};
			pushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT;
			pushConstantRange.offset = 0;
			pushConstantRange.size = sizeof(Billboard);

			std::vector<VkDescriptorSetLayout> descriptorSetLayouts{
				globalSetLayout,
				resourceSystem->getDescriptor()->getLayout()
			};

			VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
			pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
			pipelineLayoutInfo.setLayoutCount = (uint32_t)descriptorSetLayouts.size();
			pipelineLayoutInfo.pSetLayouts = descriptorSetLayouts.data();
			pipelineLayoutInfo.pushConstantRangeCount = 1;
			pipelineLayoutInfo.pPushConstantRanges = &pushConstantRange;
			VK_ASSERT(vkCreatePipelineLayout(engineDevice.device(), &pipelineLayoutInfo, nullptr, &pipelineLayout), "failed to create pipeline layout!");
		}
		{
			VkPushConstantRange pushConstantRange{};
			pushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
			pushConstantRange.offset = 0;
			pushConstantRange.size = sizeof(BoxPushConstant);

			std::vector<VkDescriptorSetLayout> descriptorSetLayouts{
				globalSetLayout
			};

			VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
			pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
			pipelineLayoutInfo.setLayoutCount = (uint32_t)descriptorSetLayouts.size();
			pipelineLayoutInfo.pSetLayouts = descriptorSetLayouts.data();
			pipelineLayoutInfo.pushConstantRangeCount = 1;
			pipelineLayoutInfo.pPushConstantRanges = &pushConstantRange;
			VK_ASSERT(vkCreatePipelineLayout(engineDevice.device(), &pipelineLayoutInfo, nullptr, &boxPipelineLayout), "failed to create pipeline layout!");
		}
		{
			VkPushConstantRange pushConstantRange{};
			pushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
			pushConstantRange.offset = 0;
			pushConstantRange.size = sizeof(MeshVolumePushConstant);

			std::vector<VkDescriptorSetLayout> descriptorSetLayouts{
				globalSetLayout
			};

			VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
			pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
			pipelineLayoutInfo.setLayoutCount = (uint32_t)descriptorSetLayouts.size();
			pipelineLayoutInfo.pSetLayouts = descriptorSetLayouts.data();
			pipelineLayoutInfo.pushConstantRangeCount = 1;
			pipelineLayoutInfo.pPushConstantRanges = &pushConstantRange;
			VK_ASSERT(vkCreatePipelineLayout(engineDevice.device(), &pipelineLayoutInfo, nullptr, &meshVolumePipelineLayout), "failed to create pipeline layout!");
		}
	}

	void EditorBillboardRenderer::createPipeline(VkRenderPass opaqueRenderPass, VkRenderPass translucentRenderPass) {
		SHARD3D_ASSERT(pipelineLayout != nullptr && "Cannot create pipeline before pipeline layout");
		{
			S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo(ProjectSystem::getEngineSettings().postfx.enableMotionBlur ? 6 : 5); // 5 gbuffers (or 6 if include velocity buffer)
			if (pipelineConfig.colorBlendAttachments.size() == 6) pipelineConfig.colorBlendAttachments[5].colorWriteMask = 0;
			pipelineConfig.setCullMode(VK_CULL_MODE_BACK_BIT);

			if (ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit) {
				pipelineConfig.reverseDepth();
			}
			if (ProjectSystem::getGraphicsSettings().renderer.msaa) {
				pipelineConfig.setSampleCount((VkSampleCountFlagBits)ProjectSystem::getGraphicsSettings().renderer.msaaSamples);
			}
			pipelineConfig.renderPass = opaqueRenderPass;
			pipelineConfig.pipelineLayout = pipelineLayout;
			graphicsPipeline = make_uPtr<S3DGraphicsPipeline>(
				engineDevice, std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/editor/editor_svabill.vert", "editor" },
				S3DShader{ ShaderType::Pixel, "resources/shaders/editor/editor_svabill.frag", "editor" }
				},
				pipelineConfig
				);
		} {
			S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo(2);
			pipelineConfig.enableWeightedBlending();
			pipelineConfig.lineRasterizer(3.f);
			pipelineConfig.inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_LINE_LIST;

			if (ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit) {
				pipelineConfig.reverseDepth();
			}
			if (ProjectSystem::getGraphicsSettings().renderer.msaa) {
				pipelineConfig.setSampleCount((VkSampleCountFlagBits)ProjectSystem::getGraphicsSettings().renderer.msaaSamples);
			}
			pipelineConfig.renderPass = translucentRenderPass;
			pipelineConfig.pipelineLayout = boxPipelineLayout;
			boxVisualizerPipeline = make_uPtr<S3DGraphicsPipeline>(
				engineDevice,
				std::vector<S3DShader>{
					S3DShader{ ShaderType::Vertex, "resources/shaders/editor/reflection_box_visualizer.vert", "editor" },
					S3DShader{ ShaderType::Pixel, "resources/shaders/fragment_color.frag", "misc" }
				}, 
				pipelineConfig
			);
		} {
			S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo(2);
			pipelineConfig.enableWeightedBlending();
			pipelineConfig.enableVertexDescriptions(VertexDescriptionOptions_Position);

			if (ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit) {
				pipelineConfig.reverseDepth();
			}
			if (ProjectSystem::getGraphicsSettings().renderer.msaa) {
				pipelineConfig.setSampleCount((VkSampleCountFlagBits)ProjectSystem::getGraphicsSettings().renderer.msaaSamples);
			}
			pipelineConfig.renderPass = translucentRenderPass;
			pipelineConfig.pipelineLayout = meshVolumePipelineLayout;
			meshVolumeVisualizerPipeline = make_uPtr<S3DGraphicsPipeline>(
				engineDevice,
				std::vector<S3DShader>{
					S3DShader{ ShaderType::Vertex, "resources/shaders/editor/mesh_volume_visualizer.vert", "editor" },
					S3DShader{ ShaderType::Pixel, "resources/shaders/fragment_color.frag", "misc" }
				},
				pipelineConfig
			);
		} 
	}

	void EditorBillboardRenderer::renderBillboards(FrameInfo &frameInfo) {
		graphicsPipeline->bind(frameInfo.commandBuffer);

		VkDescriptorSet sets[2]{
			frameInfo.globalDescriptorSet,
			frameInfo.resourceSystem->getDescriptor()->getDescriptor()
		};
		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			pipelineLayout,
			0,
			2,
			sets,
			0,
			nullptr
		);
		renderComponent<Components::AudioComponent>("component.audio", frameInfo);
		renderComponent<Components::PointLightComponent>("component.light.point", frameInfo);
		renderComponent<Components::SpotLightComponent>("component.light.spot", frameInfo);
		renderComponent<Components::DirectionalLightComponent>("component.light.directional", frameInfo);
		renderComponent<Components::VirtualPointLightComponent>("component.light.virtualpoint", frameInfo);
		renderComponent<Components::BoxReflectionCaptureComponent>("component.reflection", frameInfo);
		renderComponent<Components::BoxAmbientOcclusionVolumeComponent>("component.aovolume", frameInfo);
		renderComponent<Components::SkyLightComponent>("component.skylight", frameInfo);
		renderComponent<Components::ExponentialFogComponent>("component.expfog", frameInfo);

	}
	void EditorBillboardRenderer::renderBoxVolumes(FrameInfo& frameInfo) {
		boxVisualizerPipeline->bind(frameInfo.commandBuffer);
		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			boxPipelineLayout,
			0,
			1,
			&frameInfo.globalDescriptorSet,
			0,
			nullptr
		);
		renderVolume<Components::BoxVolumeComponent>(frameInfo);
	}

	void EditorBillboardRenderer::visualiseMeshVolumes(FrameInfo& frameInfo) {
		meshVolumeVisualizerPipeline->bind(frameInfo.commandBuffer);
		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			meshVolumePipelineLayout,
			0,
			1,
			&frameInfo.globalDescriptorSet,
			0,
			nullptr
		);
		for (auto [ actor, mri] : frameInfo.renderObjects.renderList->getMeshInfos()) {
			for (auto& aabb : mri.cullInfo.transformedMaterialAABBs) {
				MeshVolumePushConstant push{};

				push.transformMatrix = glm::mat4(0.5f);
				push.color = { 1.f, 0.f, 1.f, 0.6f };
				glm::vec3 ext = (aabb.maxXYZ - aabb.minXYZ) * 0.5f;
				glm::vec3 p = (aabb.maxXYZ + aabb.minXYZ) * 0.5f;
				push.maxXYZ = p + ext;
				push.minXYZ = p - ext;

				vkCmdPushConstants(
				  frameInfo.commandBuffer,
				  meshVolumePipelineLayout,
				  VK_SHADER_STAGE_VERTEX_BIT,
				  0,
				  sizeof(MeshVolumePushConstant),
				  &push
				);
				frameInfo.resourceSystem->retrieveMesh(frameInfo.resourceSystem->coreAssets.m_defaultModel)->draw(frameInfo.commandBuffer, 0);
			}
		}
		if (frameInfo.renderObjects.terrainList->getTerrain()) {
			for (auto& aabb : frameInfo.renderObjects.terrainList->getTerrain()->terrainAABBs) {
				MeshVolumePushConstant push{};

				push.transformMatrix = glm::mat4(0.5f);
				push.color = { 1.f, 0.f, 1.f, 0.6f };
				glm::vec3 ext = (aabb.maxXYZ - aabb.minXYZ) * 0.5f;
				glm::vec3 p = (aabb.maxXYZ + aabb.minXYZ) * 0.5f;
				push.maxXYZ = p + ext;
				push.minXYZ = p - ext;

				vkCmdPushConstants(
				  frameInfo.commandBuffer,
				  meshVolumePipelineLayout,
				  VK_SHADER_STAGE_VERTEX_BIT,
				  0,
				  sizeof(MeshVolumePushConstant),
				  &push
				);
				frameInfo.resourceSystem->retrieveMesh(frameInfo.resourceSystem->coreAssets.m_defaultModel)->draw(frameInfo.commandBuffer, 0);
			}
		}
	}

	template<typename T>
	void EditorBillboardRenderer::renderComponent(const char* iconName, FrameInfo& frameInfo) {
		auto texture = _special_assets::_get().at(iconName)->getResourceIndex();
		frameInfo.level->registry.view<T, Components::TransformComponent>().each([&](T component, Components::TransformComponent  transform) {
			Billboard push{};
		
			push.position = transform.transformMatrix[3];
			push.textureID = texture;
			vkCmdPushConstants(
				frameInfo.commandBuffer,
				pipelineLayout,
				VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
				0,
				sizeof(Billboard),
				&push
			);
			vkCmdDraw(frameInfo.commandBuffer, 6, 1, 0, 0);
		});
	}

	template<typename T>
	inline void EditorBillboardRenderer::renderVolume(FrameInfo& frameInfo) {
		frameInfo.level->registry.view<T, Components::TransformComponent>().each([&](T component, Components::TransformComponent  transform) {
			BoxPushConstant push{};

			push.transformMatrix = glm::scale(transform.transformMatrix, component.bounds);
			push.color = { 1.f, 0.f, 1.f, 0.9f };
			push.transition_bounds = component.transitionDistance;

			vkCmdPushConstants(
				frameInfo.commandBuffer,
				boxPipelineLayout,
				VK_SHADER_STAGE_VERTEX_BIT,
				0,
				sizeof(BoxPushConstant),
				&push
			);
			vkCmdDraw(frameInfo.commandBuffer, 24, 1, 0, 0);

			push.transformMatrix = glm::scale(transform.transformMatrix, component.bounds + component.transitionDistance);

			push.color = { 1.f, 0.f, 1.f, 0.4f };
			vkCmdPushConstants(
				frameInfo.commandBuffer,
				boxPipelineLayout,
				VK_SHADER_STAGE_VERTEX_BIT,
				0,
				sizeof(BoxPushConstant),
				&push
			);
			vkCmdDraw(frameInfo.commandBuffer, 24, 1, 0, 0);
		});
	}
}