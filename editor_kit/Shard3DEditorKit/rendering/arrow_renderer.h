#pragma once

#include "../core.h"
#include <Shard3D/s3dstd.h>
#include <Shard3D/vulkan_abstr.h>
#include <Shard3D/core/ecs/level.h>
#include <Shard3D/core/ecs/components.h>

namespace Shard3D {
	struct ArrowRenderInfo {
		glm::mat4 transform{1.f};
		glm::vec4 color = {1.f, 1.f, 1.f, 0.5f};
		float weight = 1.0f;
	};
	class ArrowRenderer {
	public:
		ArrowRenderer(S3DDevice &device, VkRenderPass translucentRenderPass, VkDescriptorSetLayout globalSetLayout);
		~ArrowRenderer();

		ArrowRenderer(const ArrowRenderer&) = delete;
		ArrowRenderer& operator=(const ArrowRenderer&) = delete;

		void render(FrameInfo &frameInfo, const std::vector<ArrowRenderInfo>& points);
	private:
		void createPipelineLayout(VkDescriptorSetLayout globalSetLayout);
		void createPipeline(VkRenderPass translucentRenderPass);

		S3DDevice& engineDevice;
		uPtr<S3DGraphicsPipeline> graphicsPipeline;
		VkPipelineLayout pipelineLayout;
	};
}