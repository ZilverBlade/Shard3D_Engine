#include "terrain_editor.h"
#include <Shard3D/systems/handlers/resource_system.h>
#include <ShlObj_core.h>
#include <fstream>
#include <Shard3D/utils/binary.h>
#include <zstd.h>
namespace Shard3D {
	struct TerrainPaintHeightMapPush {
		glm::vec4 materials0;
		glm::vec4 materials1;
		alignas(16)glm::vec3 mouseWorld;
		alignas(16)glm::vec3 terrainCenter;
		alignas(16)glm::vec2 terrainExtent;
		glm::vec2 tiles;
		TerrainEditorPaintConfig cfg;
		float tileExtent;
		float dt;
		int paintType;
		int terrainBrushConfig;
		float height;
		float heightCutoffFactor;
	};

	struct TerrainEditPush {
		glm::vec4 translation;
		glm::vec2 tiles;
		float tileExtent;
		float heightMul;
		uint32_t lod;
		TerrainEditorPaintConfig cfg;
		VkBool32 displayHover;
		uint32_t view;
		alignas(16)glm::vec3 mouseWorld;
	};

	TerrainEditSystem::TerrainEditSystem(
		S3DDevice& device, VkRenderPass gbufferPass, ResourceSystem* resourceSystem, VkDescriptorSetLayout globalSetLayout, uPtr<S3DDescriptorSetLayout>& terrainDescriptorSetLayout)
		: engineDevice(device), resourceSystem(resourceSystem), globalSetLayout(globalSetLayout), framesInFlight(framesInFlight), terrainDescriptorSetLayout(terrainDescriptorSetLayout){
		
		static_assert(sizeof(TerrainPaintHeightMapPush) <= 128);
		createPipelineLayouts(resourceSystem->getDescriptor(), globalSetLayout, terrainDescriptorSetLayout);
		createPipelines(gbufferPass);
	}

	TerrainEditSystem::~TerrainEditSystem() {

	}

	void TerrainEditSystem::render(FrameInfo& frameInfo, Actor actor, TerrainEditorVisualizer vis, glm::vec3 posWorld, TerrainEditorBrushSettings brush) {
		if (actor.getComponent<Components::TerrainComponent>().dirty) {
			vkDeviceWaitIdle(engineDevice.device());
			createActor(actor);
			actor.getComponent<Components::TerrainComponent>().dirty = false;
		}
		if (!editingTerrainActor) return;
		sPtr<TerrainActor> terrain = editingTerrainActor;
		if (vis.view == TerrainEditorView::ViewInGame) {
			frameInfo.renderObjects.terrainList->getTerrain() = terrain;
			return;
		} else {
			frameInfo.renderObjects.terrainList->getTerrain() = nullptr;
		}

		(vis.wireframe ? terrainEditWireframePipeline : terrainEditPipeline)->bind(frameInfo.commandBuffer);

		VkDescriptorSet sets[3]{
			frameInfo.globalDescriptorSet,
			frameInfo.resourceSystem->getDescriptor()->getDescriptor(),
			terrain->terrainDescriptor
		};

		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			terrainPipelineLayout->getPipelineLayout(),
			0,
			3,
			sets,
			0,
			nullptr
		);
		
		auto& trc = actor.getComponent<Components::TerrainComponent>();
		TerrainEditPush push;
		push.heightMul = terrain->heightMul;
		push.tiles = { terrain->numTilesX, terrain->numTilesY };
		push.tileExtent = terrain->tileExtent;
		push.translation = actor.getTransform().transformMatrix[3];
		push.cfg = brush.config;
		push.displayHover = vis.displayHover;
		push.view = (uint32_t)vis.view;
		push.mouseWorld = posWorld;

		terrainPush.push(frameInfo.commandBuffer, terrainPipelineLayout->getPipelineLayout(), push);

		if (editingTerrainActor) {
			for (int i = 0; i < editingTerrainActor->getLODCount(); i++) {
				push.lod = i;
				terrainPush.push(frameInfo.commandBuffer, terrainPipelineLayout->getPipelineLayout(), push);
				editingTerrainActor->render(frameInfo, i);
			}
		}
	}

	void TerrainEditSystem::createEditorObjects() {
		if (!editingTerrainActor) return;
		terrainHeightLimitsSSBO = make_uPtr<S3DBuffer>(
			engineDevice,
			editingTerrainActor->numTilesX * editingTerrainActor->numTilesY * 16,
			1,
			VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
			true
		);
		terrainHeightLimitsSSBO->map();

		for (int x = 0; x < editingTerrainActor->numTilesX; x++) {
			for (int y = 0; y < editingTerrainActor->numTilesY; y++) {
				int tileIndex = y * editingTerrainActor->numTilesX + x;
				reinterpret_cast<AlignedType<glm::ivec2, 16>*>(terrainHeightLimitsSSBO->getMappedMemory())[tileIndex]
					= { editingTerrainActor->terrainAABBs[tileIndex].minXYZ.y * 1000000.f,
					editingTerrainActor->terrainAABBs[tileIndex].maxXYZ.y * 1000000.f };
			}
		}
		VkDescriptorBufferInfo storageBufferInfo = terrainHeightLimitsSSBO->descriptorInfo();
		VkDescriptorImageInfo terrainHeightMapStorageImageInfo{ VK_NULL_HANDLE, editingTerrainActor->heightMap->tileImageView, VK_IMAGE_LAYOUT_GENERAL };
		VkDescriptorImageInfo terrainMaterialMapStorageImageInfo{ VK_NULL_HANDLE, editingTerrainActor->materialMap->tileImageView, VK_IMAGE_LAYOUT_GENERAL };
		S3DDescriptorWriter(*terrainStorageDescriptorSetLayout, *engineDevice.staticMaterialPool)
			.writeBuffer(0, &storageBufferInfo)
			.writeImage(1, &terrainHeightMapStorageImageInfo)
			.writeImage(2, &terrainMaterialMapStorageImageInfo)
			.build(terrainStorageDescriptor);
	}

	AssetID TerrainEditSystem::exportTerrain(const std::string& sceneLocation, const std::string& terrainName) {
		if (!editingTerrainActor) return AssetID::null();
		ktxTexture2* heightMapTexture = editingTerrainActor->heightMap->getKTXTexture();
		ktxTexture2* materialMap0Texture = editingTerrainActor->materialMap->getKTXTexture(0.25f);

		AssetID heightMapLoc = sceneLocation + "/" + terrainName + "_heightmap-perface.s3dterrain";
		AssetID materialMap0Loc = sceneLocation + "/" + terrainName + "_materialmap0-perface.s3dterrain";
		AssetID physicsHeightMapLoc = AssetID::null();
		AssetID physicsMaterialMap0Loc = AssetID::null();
		AssetID physicsDataLoc = AssetID::null();
		AssetID aabbLoc = sceneLocation + "/" + terrainName + "_aabbdata.s3dbin";
		TextureImporter::exportCompressedTextureKTX(heightMapTexture, heightMapLoc.getAbsolute().c_str());
		TextureImporter::exportCompressedTextureKTX(materialMap0Texture, materialMap0Loc.getAbsolute().c_str());		
		IOUtils::writeStackBinary(editingTerrainActor->terrainAABBs.data(), editingTerrainActor->terrainAABBs.size() * sizeof(VolumeBounds), aabbLoc.getAbsolute());
		
		auto& trc = editingActor.getComponent<Components::TerrainComponent>();
		if (auto* pd = trc.physData; pd != nullptr) {
			physicsHeightMapLoc = sceneLocation + "/" + terrainName + "_physicsheightmap-pervert.s3dterrain";
			physicsMaterialMap0Loc = sceneLocation + "/" + terrainName + "_physicsmaterialmap-perface.s3dterrain";
			physicsDataLoc = sceneLocation + "/" + terrainName + "_physicsdata.s3dbin";

			BinaryStream streamPhysData{};
			BinaryStream streamHeight{};
			BinaryStream streamMaterial{};
			streamPhysData << trc.tiles.x;
			streamPhysData << trc.tiles.y;
			for (int y = 0; y < trc.tiles.y; y++) {
				for (int x = 0; x < trc.tiles.x; x++) {
					auto tile = pd->tiles[y * trc.tiles.x + x];
					streamPhysData << tile.offsetX;
					streamPhysData << tile.offsetY;
					streamPhysData << tile.verticesX;
					streamPhysData << tile.verticesY;
					streamPhysData << tile.widthX;
					streamPhysData << tile.widthY;
					streamPhysData << tile.materialDataResX;
					streamPhysData << tile.materialDataResY;
				}
			}
			for (int y = 0; y < trc.tiles.y; y++) {
				for (int x = 0; x < trc.tiles.x; x++) {
					auto tile = pd->tiles[y * trc.tiles.x + x];
					streamHeight.array_back(tile.heights);
					streamMaterial.array_back(tile.materialData);
				}
			}
			IOUtils::writeStackBinary(streamPhysData.get(), streamPhysData.size(), physicsDataLoc.getAbsolute());



			ZSTD_CCtx* compressionContext = ZSTD_createCCtx();
			{
				size_t dstSize = ZSTD_compressBound(streamHeight.size());
				uint8_t* dstBytes = new uint8_t[dstSize];
				size_t writtenSize = ZSTD_compressCCtx(compressionContext, dstBytes, dstSize, streamHeight.get(), streamHeight.size(), 14);

				std::ofstream out(physicsHeightMapLoc.getAbsolute(), std::ios::binary | std::ios::out);
				out.write(reinterpret_cast<const char*>(dstBytes), writtenSize);
				out.flush();
				out.close();
				delete[] dstBytes;
			}
			{
				size_t dstSize = ZSTD_compressBound(streamMaterial.size());
				uint8_t* dstBytes = new uint8_t[dstSize];
				size_t writtenSize = ZSTD_compressCCtx(compressionContext, dstBytes, dstSize, streamMaterial.get(), streamMaterial.size(), 14);

				std::ofstream out(physicsMaterialMap0Loc.getAbsolute(), std::ios::binary | std::ios::out);
				out.write(reinterpret_cast<const char*>(dstBytes), writtenSize);
				out.flush();
				out.close();
				delete[] dstBytes;
			}
			ZSTD_freeCCtx(compressionContext);
		}
		TextureImporter::freeKTX(heightMapTexture);
		TextureImporter::freeKTX(materialMap0Texture);

		AssetID exportAsset = sceneLocation + "/" + terrainName + ".s3dasset";
		nlohmann::ordered_json out{};
		std::ofstream fout(exportAsset.getAbsolute());

		out["version"] = ENGINE_VERSION;
		out["assetType"] = "terrain";
		out["assetFiles"]["heightMap"] = heightMapLoc.getAsset();
		out["assetFiles"]["materialMap0"] = materialMap0Loc.getAsset();
		out["assetFiles"]["materialMap1"] = "";
		out["assetFiles"]["physicsTileHeightField"] = physicsHeightMapLoc.getAsset();
		out["assetFiles"]["physicsTileMaterialMap"] = physicsMaterialMap0Loc.getAsset();
		out["assetFiles"]["physicsData"] = physicsDataLoc.getAsset();
		out["assetFiles"]["aabbData"] = aabbLoc.getAsset();
		out["config"]["editorFormatHeightMap"] = string_VkFormat(editingTerrainActor->heightMap->editorFormat);
		out["config"]["editorFormatMaterialMap0"] = string_VkFormat(editingTerrainActor->materialMap->editorFormat);
		out["config"]["editorResolutionHeightMap"] = editingTerrainActor->heightMap->resolution;
		out["config"]["editorResolutionMaterialMap0"] = editingTerrainActor->heightMap->resolution;
		//out["config"]["divisions"] = (uint64_t)editingTerrainActor->divisions;

		fout << out.dump(4);
		fout.flush();
		fout.close();
		return exportAsset;
	}

	void TerrainEditSystem::paintTerrain(FrameInfo& frameInfo, glm::vec3 posWorld, glm::vec3 delta, 
		float dt, TerrainEditorBrushSettings brush) {
		terrainPaintHeightMapPipeline->bind(frameInfo.commandBuffer);
		if (!editingTerrainActor) return;
		sPtr<TerrainActor> terrain = editingTerrainActor;
		if (!editingActor.valid()) return;
		if (!editingActor.hasComponent<Components::TerrainComponent>()) {
			auto view = frameInfo.level->registry.view<Components::TerrainComponent>();
			editingActor = { view.size() ? view[0] : entt::null, frameInfo.level.get() };
		}
		auto& terrainComponent = editingActor.getComponent<Components::TerrainComponent>();

		TerrainPaintHeightMapPush push;
		push.terrainExtent = terrainComponent.tileExtent * glm::vec2(terrainComponent.tiles);
		push.terrainCenter = editingActor.getTransform().transformMatrix[3];
		push.mouseWorld = posWorld;
		push.dt = dt;
		push.tiles = terrainComponent.tiles;
		push.tileExtent = terrainComponent.tileExtent;
		push.cfg = brush.config;
		push.paintType = brush.paintingType;
		push.terrainBrushConfig = (int)brush.brushConfig;
		push.heightCutoffFactor = brush.heightCutoffFactor;
		if (brush.brushConfig == TerrainBrushConfig::Descend) {
			push.cfg.weight *= -1.f;
		}
		push.height = brush.brushHeight;
		switch (brush.currentMaterial) {
		case(0):
			push.materials0 = glm::vec4(1.0, 0.0, 0.0, 0.0);
			break;
		case(1):
			push.materials0 = glm::vec4(0.0, 1.0, 0.0, 0.0);
			break;
		case(2):
			push.materials0 = glm::vec4(0.0, 0.0, 1.0, 0.0);
			break;
		case(3):
			push.materials0 = glm::vec4(0.0, 0.0, 0.0, 1.0);
			break;
		}

		VkDescriptorSet sets[2]{
			frameInfo.globalDescriptorSet,
			terrainStorageDescriptor
		};
		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_COMPUTE,
			terrainPaintHeightMapPipelineLayout->getPipelineLayout(),
			0,
			2,
			sets,
			0,
			nullptr
		);

		int dispatches = (int)ceil((2.0f * ceil(push.cfg.radius / push.tileExtent) * (float)terrainComponent.tileSubdivisions) / 4.0f);

		//const float BRUSH_STEP_REL_SIZE = 0.1;
		//const float MAX_STEPS = 10.0;
		//
		//glm::vec3 mousePrev = push.mouseWorld - delta;
		//float deltaDistance = length(delta);
		//float steps = glm::clamp(ceil(deltaDistance / glm::max(push.cfg.radius * BRUSH_STEP_REL_SIZE, 0.001f)), 1.f, MAX_STEPS);
		//float stepLength = deltaDistance / steps;

		//for (int b = 0; b < 4; b++) {
		//	push.linearBlendIndex = b;
		//for (int i = 1; i <= steps; i++) {
		//	push.mouseWorld = mousePrev + i * stepLength;
		//	push.dt = dt / steps;
		terrainPaintHeightMapPush.push(frameInfo.commandBuffer, terrainPaintHeightMapPipelineLayout->getPipelineLayout(), push);
		vkCmdDispatch(frameInfo.commandBuffer, dispatches, dispatches, 1);
		//}
		//}
	}

	TerrainPhysicsData* TerrainEditSystem::generatePhysicsTiles() {
		
		LPVOID dialog;
		HRESULT res = CoCreateInstance(CLSID_ProgressDialog, nullptr, CLSCTX_INPROC_SERVER, IID_IProgressDialog, &dialog);
		reinterpret_cast<IProgressDialog*>(dialog)->SetTitle(L"Terrain Physics");
		reinterpret_cast<IProgressDialog*>(dialog)->SetLine(1, L"Copying Height Map Data...", false, nullptr);
		reinterpret_cast<IProgressDialog*>(dialog)->StartProgressDialog(nullptr, nullptr, PROGDLG_NOMINIMIZE | PROGDLG_NOCANCEL, nullptr);

		TerrainPhysicsData* terrainData = new TerrainPhysicsData();
		std::vector<TerrainPhysicsTile>& tiles = terrainData->tiles;
		sPtr<TerrainActor> terrain = editingTerrainActor;
		tiles.resize(terrain->numTilesX * terrain->numTilesY);
		auto& trc = editingActor.getComponent<Components::TerrainComponent>();
		glm::vec2 tileBounds = { trc.tileExtent, trc.tileExtent };
		glm::vec2 tileCenterOffset = {-(terrain->numTilesX - 1)* trc.tileExtent, -(terrain->numTilesY - 1) * trc.tileExtent};

		terrainData->position = editingActor.getTransform().transformMatrix[3];

		int shrinkFactor = trc.tileSubdivisions / trc.tilePhysicsSubdivisions;

		glm::ivec2 heightFieldRes = terrain->heightMap->resolution / shrinkFactor;
		glm::ivec2 materialDataRes = terrain->materialMap->resolution / 4;
		glm::ivec2 heightFieldTileResolution = heightFieldRes / glm::ivec2(terrain->numTilesX, terrain->numTilesY);
		glm::ivec2 materialDataTileResolution = materialDataRes / glm::ivec2(terrain->numTilesX, terrain->numTilesY);
		S3DBuffer heightMapData = S3DBuffer(
			engineDevice,
			heightFieldRes.x * heightFieldRes.y * sizeof(float),
			1,
			VK_BUFFER_USAGE_TRANSFER_DST_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
			true
		);
		S3DBuffer materialMapData = S3DBuffer(
			engineDevice,
			materialDataRes.x * materialDataRes.y * sizeof(glm::u16vec4),
			1,
			VK_BUFFER_USAGE_TRANSFER_DST_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
			true
		);
		heightMapData.map();
		materialMapData.map();
		VkCommandBuffer commandBuffer = engineDevice.beginSingleTimeCommands();
		engineDevice.copyImageToBuffer(
			commandBuffer,
			terrain->heightMap->tileImage,
			VK_IMAGE_LAYOUT_GENERAL,
			heightMapData.getBuffer(),
			heightFieldRes.x,
			heightFieldRes.y,
			1,
			std::log2(shrinkFactor)
		);
		engineDevice.copyImageToBuffer(
			commandBuffer,
			terrain->materialMap->tileImage,
			VK_IMAGE_LAYOUT_GENERAL,
			materialMapData.getBuffer(),
			materialDataRes.x,
			materialDataRes.y,
			1,
			2
		);
		engineDevice.endSingleTimeCommands(commandBuffer);

		reinterpret_cast<IProgressDialog*>(dialog)->SetLine(1, L"Generating Physics Data...", false, nullptr);

		reinterpret_cast<IProgressDialog*>(dialog)->SetProgress(1, terrain->numTilesX * terrain->numTilesY + 1);

		int maxPixel = heightFieldRes.x * heightFieldRes.y - 1;
		for (int y = 0; y < terrain->numTilesY; y++) {
			for (int x = 0; x < terrain->numTilesX; x++) {
				int tileIndex = y * terrain->numTilesX + x;


				glm::vec2 tileOffset = { tileBounds.x * x * 2.0f, tileBounds.y * y * 2.0f };
				tiles[tileIndex].offsetX = tileOffset.x + tileCenterOffset.x;
				tiles[tileIndex].offsetY = tileOffset.y + tileCenterOffset.y;
				tiles[tileIndex].widthX = tileBounds.x * 2.0f;
				tiles[tileIndex].widthY = tileBounds.y * 2.0f;
				tiles[tileIndex].aabb = terrain->terrainAABBs[tileIndex];

				// HEIGHT
				{
					// add 1 due to pixels on heightmap corresponding to each face rather than vertex
					tiles[tileIndex].verticesX = (int)terrain->divisions / shrinkFactor + 1;
					tiles[tileIndex].verticesY = (int)terrain->divisions / shrinkFactor + 1;

					int maxPixelOffLeft = -x * heightFieldTileResolution.x;
					int maxPixelOffRight = heightFieldRes.x + maxPixelOffLeft;
					int maxPixelOffUp = -y * heightFieldTileResolution.y;
					int maxPixelOffDown = heightFieldRes.y + maxPixelOffUp;

					int tileArrayOffset = terrain->numTilesX * heightFieldTileResolution.x * heightFieldTileResolution.y * y + x * heightFieldTileResolution.x;
					int remainingPixels = terrain->numTilesX * heightFieldTileResolution.x; // wrap around back to the tile to prevent accidentally sampling the next tile
					std::function<int(int w, int h)> getOffset = [&](int h, int w) {
						return std::clamp(tileArrayOffset + remainingPixels * h + w, 0, maxPixel);
					};
					for (int h = 0; h < tiles[tileIndex].verticesY; h++) {
						for (int w = 0; w < tiles[tileIndex].verticesX; w++) {
							float* heights = reinterpret_cast<float*>(heightMapData.getMappedMemory());

							float nw = heights[getOffset(std::clamp(h - 1, maxPixelOffUp, maxPixelOffDown), std::clamp(w - 1, maxPixelOffLeft, maxPixelOffRight))];
							float sw = heights[getOffset(std::clamp(h + 0, maxPixelOffUp, maxPixelOffDown), std::clamp(w - 1, maxPixelOffLeft, maxPixelOffRight))];
							float ne = heights[getOffset(std::clamp(h - 1, maxPixelOffUp, maxPixelOffDown), std::clamp(w + 0, maxPixelOffLeft, maxPixelOffRight))];
							float se = heights[getOffset(std::clamp(h + 0, maxPixelOffUp, maxPixelOffDown), std::clamp(w + 0, maxPixelOffLeft, maxPixelOffRight))];

							tiles[tileIndex].heights.push_back((nw + sw + ne + se) * 0.25f);
						}
					}
				}
				// MATERIAL
				{
					tiles[tileIndex].materialDataResX = (int)terrain->divisions / 4;
					tiles[tileIndex].materialDataResY = (int)terrain->divisions / 4;

					int maxPixelOffLeft = -x * materialDataTileResolution.x;
					int maxPixelOffRight = materialDataRes.x + maxPixelOffLeft;
					int maxPixelOffUp = -y * materialDataTileResolution.y;
					int maxPixelOffDown = materialDataRes.y + maxPixelOffUp;

					int tileArrayOffset = terrain->numTilesX * materialDataTileResolution.x * materialDataTileResolution.y * y + x * materialDataTileResolution.x;
					int remainingPixels = terrain->numTilesX * materialDataTileResolution.x; // wrap around back to the tile to prevent accidentally sampling the next tile
					std::function<int(int w, int h)> getOffset = [&](int h, int w) {
						return std::clamp(tileArrayOffset + remainingPixels * h + w, 0, maxPixel);
					};
					for (int h = 0; h < tiles[tileIndex].materialDataResX; h++) {
						for (int w = 0; w < tiles[tileIndex].materialDataResY; w++) {
							glm::u16vec4 materials = reinterpret_cast<glm::u16vec4*>(materialMapData.getMappedMemory())[getOffset(h, w)];
							float weights[4] = {
								static_cast<float>(materials[0]) / static_cast<float>(0xFFFF),
								static_cast<float>(materials[1]) / static_cast<float>(0xFFFF),
								static_cast<float>(materials[2]) / static_cast<float>(0xFFFF),
								static_cast<float>(materials[3]) / static_cast<float>(0xFFFF)
							};
							uint8_t materialBits = 0x00;
							for (int i = 0; i < 4; i++) {
								materialBits |= (int(weights[i] > 0.20f) << i);
							}
							tiles[tileIndex].materialData.push_back(materialBits);
						}
					}
				}
				reinterpret_cast<IProgressDialog*>(dialog)->SetLine(2, std::wstring(L"Tile " + std::to_wstring(tileIndex)).c_str(), false, nullptr);
				reinterpret_cast<IProgressDialog*>(dialog)->SetProgress(1 + tileIndex, terrain->numTilesX* terrain->numTilesY + 1);
			}
		}
		reinterpret_cast<IProgressDialog*>(dialog)->StopProgressDialog();

		return terrainData;
	}

	void TerrainEditSystem::updateTerrainData(Actor actor) {
		updateActorAABBs(actor);
		generateMipChain(actor);
	}

	void TerrainEditSystem::createActor(Actor actor) {
		editingActor = actor;
		auto& trc = actor.getComponent<Components::TerrainComponent>();
		glm::ivec2 res = glm::ivec2(static_cast<int>(trc.tileSubdivisions));
		res.x *= trc.tiles.x;
		res.y *= trc.tiles.y;

		editingTerrainActor = make_sPtr<TerrainActor>(engineDevice);
		editingTerrainActor->numTilesX = trc.tiles.x;
		editingTerrainActor->numTilesY = trc.tiles.y;
		editingTerrainActor->tileExtent = trc.tileExtent;

		glm::ivec2 tileRes = res / trc.tiles;
		int mipLevels = std::log2(std::min(tileRes.x, tileRes.y)) - 3; // lowest tile resolution 17x17 vertices, no lower
		editingTerrainActor->heightMap = make_uPtr<TerrainTexture>(engineDevice);
		editingTerrainActor->heightMap->createTexture(res, mipLevels, VK_FORMAT_R32_SFLOAT, VK_FORMAT_R16_SFLOAT);
		editingTerrainActor->materialMap = make_uPtr<TerrainTexture>(engineDevice);
		editingTerrainActor->materialMap->createTexture(res, 3, VK_FORMAT_R16G16B16A16_UNORM, VK_FORMAT_R8G8B8A8_UNORM);

		editingTerrainActor->divisions = (TerrainTileDivisions)trc.tileSubdivisions;
		editingTerrainActor->terrainAABBs.resize(trc.tiles.x * trc.tiles.y);

		editingTerrainActor->createBufferData();
		editingTerrainActor->createTerrainUpdateObjects();
		editingTerrainActor->createDescriptorData(terrainDescriptorSetLayout);
		editingTerrainActor->setMeshData(resourceSystem);
		createEditorObjects();
		updateActorAABBs(actor);
	}

	void TerrainEditSystem::updateActorAABBs(Actor actor) {
		auto& trc = actor.getComponent<Components::TerrainComponent>();
		glm::vec3 tileCenterOffset = -glm::vec3((editingTerrainActor->numTilesX - 1) * trc.tileExtent, 0.0f, (editingTerrainActor->numTilesY - 1) * trc.tileExtent);

		for (int y = 0; y < editingTerrainActor->numTilesY; y++) {
			for (int x = 0; x < editingTerrainActor->numTilesX; x++) {
				int tileIndex = y * editingTerrainActor->numTilesX + x;
				reinterpret_cast<glm::ivec2*>(terrainHeightLimitsSSBO->getMappedMemory())[tileIndex] = glm::ivec2{};
			}
		}
		terrainHeightLimitsSSBO->flush();
		VkCommandBuffer commandBuffer = engineDevice.beginSingleTimeCommands();
		terrainUpdateAABBHeightsPipeline->bind(commandBuffer);
		vkCmdBindDescriptorSets(
			commandBuffer,
			VK_PIPELINE_BIND_POINT_COMPUTE,
			 terrainUpdateAABBHeightsPipelineLayout->getPipelineLayout(),
			0,
			1,
			&terrainStorageDescriptor,
			0,
			nullptr
		);
		terrainUpdateAABBHeightsPush.push(commandBuffer, terrainUpdateAABBHeightsPipelineLayout->getPipelineLayout(), trc.tiles);
		glm::ivec2 dispatches = editingTerrainActor->heightMap->resolution / 8;
		vkCmdDispatch(commandBuffer, dispatches.x, dispatches.y, 1);
		engineDevice.endSingleTimeCommands(commandBuffer);

		for (int y = 0; y < editingTerrainActor->numTilesY; y++) {
			for (int x = 0; x < editingTerrainActor->numTilesX; x++) {
				int tileIndex = y * editingTerrainActor->numTilesX + x;
				glm::ivec2 heightLimits = reinterpret_cast<glm::ivec2*>(terrainHeightLimitsSSBO->getMappedMemory())[tileIndex];
				float boundsPosition = (heightLimits[1] / 1e6f + heightLimits[0] / 1e6f) / 2.0f;
				float boundsExtent = (heightLimits[1] / 1e6f - heightLimits[0] / 1e6f) / 2.0f;
		
				glm::vec3 tileBounds = glm::vec3(trc.tileExtent, boundsExtent + 0.01f, trc.tileExtent);
				glm::vec3 tileOffset = { tileBounds.x * x * 2.0f, boundsPosition, tileBounds.z * y * 2.0f };
				editingTerrainActor->terrainAABBs[tileIndex] = VolumeBounds(glm::vec3(actor.getTransform().transformMatrix[3]) + tileCenterOffset + tileOffset, tileBounds);
				struct VolumeBoundsTerrainAligned {
					alignas(16)glm::vec3 min;
					alignas(16)glm::vec3 max;
				};
				static_cast<VolumeBoundsTerrainAligned&>(reinterpret_cast<VolumeBoundsTerrainAligned*>(editingTerrainActor->terrainAABBBuffer->getMappedMemory())[tileIndex]).min = editingTerrainActor->terrainAABBs[tileIndex].minXYZ;
				static_cast<VolumeBoundsTerrainAligned&>(reinterpret_cast<VolumeBoundsTerrainAligned*>(editingTerrainActor->terrainAABBBuffer->getMappedMemory())[tileIndex]).max = editingTerrainActor->terrainAABBs[tileIndex].maxXYZ;
			}
		}
		editingTerrainActor->terrainAABBBuffer->flush();
	}

	void TerrainEditSystem::generateMipChain(Actor actor) {
		VkCommandBuffer commandBuffer = engineDevice.beginSingleTimeCommands();
		engineDevice.transitionImageLayout(commandBuffer,
			editingTerrainActor->heightMap->tileImage, editingTerrainActor->heightMap->editorFormat,
			editingTerrainActor->heightMap->imageLayout, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 0, editingTerrainActor->heightMap->mipLevels, 0, 1
		);
		engineDevice.transitionImageLayout(commandBuffer,
			editingTerrainActor->materialMap->tileImage, editingTerrainActor->materialMap->editorFormat,
			editingTerrainActor->materialMap->imageLayout, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 0, editingTerrainActor->materialMap->mipLevels, 0, 1
		);
		engineDevice.generateMipMaps(commandBuffer,
			editingTerrainActor->heightMap->tileImage, editingTerrainActor->heightMap->editorFormat,
			editingTerrainActor->heightMap->resolution.x, editingTerrainActor->heightMap->resolution.y, 1,
			editingTerrainActor->heightMap->mipLevels, 1, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
		engineDevice.generateMipMaps(commandBuffer,
			editingTerrainActor->materialMap->tileImage, editingTerrainActor->materialMap->editorFormat,
			editingTerrainActor->materialMap->resolution.x, editingTerrainActor->materialMap->resolution.y, 1,
			editingTerrainActor->materialMap->mipLevels, 1, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
		engineDevice.transitionImageLayout(commandBuffer,
			editingTerrainActor->heightMap->tileImage, editingTerrainActor->heightMap->editorFormat,
			VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, editingTerrainActor->heightMap->imageLayout, 0, editingTerrainActor->heightMap->mipLevels, 0, 1
		);
		engineDevice.transitionImageLayout(commandBuffer,
			editingTerrainActor->materialMap->tileImage, editingTerrainActor->materialMap->editorFormat,
			VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, editingTerrainActor->materialMap->imageLayout, 0, editingTerrainActor->materialMap->mipLevels, 0, 1
		);
		engineDevice.endSingleTimeCommands(commandBuffer);
	}

	void TerrainEditSystem::createPipelineLayouts(uPtr<SmartDescriptorSet>& bindless, VkDescriptorSetLayout globalSetLayout, uPtr<S3DDescriptorSetLayout>& terrainDescriptorSetLayout) {
		terrainStorageDescriptorSetLayout = S3DDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, VK_SHADER_STAGE_COMPUTE_BIT)
			.addBinding(1, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_COMPUTE_BIT)
			.addBinding(2, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_COMPUTE_BIT)
			.build();

		terrainPaintHeightMapPush = S3DPushConstant(sizeof(TerrainPaintHeightMapPush), VK_SHADER_STAGE_COMPUTE_BIT);
		terrainPaintHeightMapPipelineLayout = make_uPtr<S3DPipelineLayout>(
			engineDevice,
			std::vector<VkPushConstantRange>{ terrainPaintHeightMapPush.getRange() },
			std::vector<VkDescriptorSetLayout>{ globalSetLayout, terrainStorageDescriptorSetLayout->getDescriptorSetLayout() }
		);

		terrainUpdateAABBHeightsPush = S3DPushConstant(sizeof(glm::ivec2), VK_SHADER_STAGE_COMPUTE_BIT);
		terrainUpdateAABBHeightsPipelineLayout = make_uPtr<S3DPipelineLayout>(
			engineDevice,
			std::vector<VkPushConstantRange>{ terrainUpdateAABBHeightsPush.getRange() },
			std::vector<VkDescriptorSetLayout>{ terrainStorageDescriptorSetLayout->getDescriptorSetLayout() }
		);

		terrainPush = S3DPushConstant(sizeof(TerrainEditPush), VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT);
		terrainPipelineLayout = make_uPtr<S3DPipelineLayout>(
			engineDevice,
			std::vector<VkPushConstantRange>{ terrainPush.getRange() },
			std::vector<VkDescriptorSetLayout>{ globalSetLayout, bindless->getLayout(), terrainDescriptorSetLayout->getDescriptorSetLayout() }
		);
	}
	void TerrainEditSystem::createPipelines(VkRenderPass gbufferPass) {
		S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo(ProjectSystem::getEngineSettings().postfx.enableMotionBlur ? 6 : 5);
		if (pipelineConfig.colorBlendAttachments.size() == 6) pipelineConfig.colorBlendAttachments[5].colorWriteMask = 0;
		pipelineConfig.enableVertexDescriptions(VertexDescriptionOptions_Position | VertexDescriptionOptions_UV);
		pipelineConfig.setCullMode(VK_CULL_MODE_BACK_BIT);

		if (ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit) {
			pipelineConfig.reverseDepth();
		}

		S3DShaderSpecialization specializationVert;
		specializationVert.addConstant(0, VK_FALSE);

		pipelineConfig.pipelineLayout = terrainPipelineLayout->getPipelineLayout();
		pipelineConfig.renderPass = gbufferPass;
		if (ProjectSystem::getGraphicsSettings().renderer.msaa) {
			pipelineConfig.setSampleCount((VkSampleCountFlagBits)ProjectSystem::getGraphicsSettings().renderer.msaaSamples);
		}
		terrainEditPipeline = make_uPtr<S3DGraphicsPipeline>(
			engineDevice, std::vector<S3DShader>{
			S3DShader{ ShaderType::Vertex, "resources/shaders/rendering/terrain.vert", "rendering", specializationVert },
				S3DShader{ ShaderType::Fragment, "resources/shaders/editor/terrain_debug.frag", "editor" },
		}, pipelineConfig
		);

		pipelineConfig.lineRasterizer(2.0f);
		terrainEditWireframePipeline = make_uPtr<S3DGraphicsPipeline>(
			engineDevice, std::vector<S3DShader>{
			S3DShader{ ShaderType::Vertex, "resources/shaders/rendering/terrain.vert", "rendering", specializationVert },
				S3DShader{ ShaderType::Fragment, "resources/shaders/editor/terrain_debug.frag", "editor" },
		}, pipelineConfig
		);

		terrainPaintHeightMapPipeline = make_uPtr<S3DComputePipeline>(
			engineDevice, terrainPaintHeightMapPipelineLayout->getPipelineLayout(),
			S3DShader{ ShaderType::Compute, "resources/shaders/editor/terrain_paint.comp", "editor" }
		); 
		terrainUpdateAABBHeightsPipeline= make_uPtr<S3DComputePipeline>(
			engineDevice, terrainUpdateAABBHeightsPipelineLayout->getPipelineLayout(),
			S3DShader{ ShaderType::Compute, "resources/shaders/editor/terrain_gen_bounds.comp", "editor" }
		);
	}
}