#pragma once

#include <Shard3D/s3dstd.h>
#include <Shard3D/vulkan_abstr.h>
#include <Shard3D/core/misc/frame_info.h>	  
#include <Shard3D/core/ecs/level.h>
#include <Shard3D/core/ecs/actor.h>
#include <Shard3D/core/asset/importer_model.h>
#include <Shard3D/core/asset/mesh3d.h>
#include <Shard3D/core/rendering/render_target.h>
#include <Shard3D/systems/rendering/terrain_system.h>

namespace Shard3D {
	enum class TerrainBrushConfig {
		Ascend = 0,
		Descend = 1,
		Average = 2
	};
	enum class TerrainPaintWeightFalloff {
		Linear = 0,
		Quadratic = 1,
		Cubic = 2,
		Exponential2 = 3
	};
	enum class TerrainEditorView {
		ViewDefault = 0,
		ViewTiles = 1,
		ViewHeight = 2,
		ViewNormals = 3,
		ViewPhysMat = 4,
		ViewLOD = 5,
		ViewInGame = 6
	};
	struct TerrainEditorVisualizer {
		bool displayHover = true;
		bool wireframe = false;
		TerrainEditorView view = TerrainEditorView::ViewTiles;
	};
	struct TerrainEditorPaintConfig {
		float radius = 20.0f;
		float weight = 10.0f;
		TerrainPaintWeightFalloff falloff = TerrainPaintWeightFalloff::Quadratic;
		float perlinIntensity = 0.0;
		float perlinScale = 1.0;
	};
	struct TerrainEditorBrushSettings {
		TerrainEditorPaintConfig config{};
		uint32_t paintingType = 0;
		uint32_t currentMaterial = 0;
		TerrainBrushConfig brushConfig = TerrainBrushConfig::Ascend;
		float brushHeight = 1.0f;
		float heightCutoffFactor = 0.0f;
	};
	
	class TerrainEditSystem {
	public:
		TerrainEditSystem(S3DDevice& device, VkRenderPass gbufferPass, ResourceSystem* resourceSystem, VkDescriptorSetLayout globalSetLayout, uPtr<S3DDescriptorSetLayout>& terrainDescriptorSetLayout);
		~TerrainEditSystem();

		TerrainEditSystem(const TerrainEditSystem&) = delete;
		TerrainEditSystem& operator=(const TerrainEditSystem&) = delete;

		void render(FrameInfo& frameInfo, Actor actor, TerrainEditorVisualizer vis, glm::vec3 posWorld, TerrainEditorBrushSettings brush);
		void createEditorObjects();

		AssetID exportTerrain(const std::string& sceneLocation, const std::string& terrainName);

		void paintTerrain(FrameInfo& frameInfo, glm::vec3 posWorld, glm::vec3 delta, 
			float dt, TerrainEditorBrushSettings brush);
		TerrainPhysicsData* generatePhysicsTiles();
		void updateTerrainData(Actor actor);

		sPtr<TerrainActor> editingTerrainActor;
		Actor editingActor;
	private:
		void createActor(Actor actor);
		void createPipelineLayouts(uPtr<SmartDescriptorSet>& bindless, VkDescriptorSetLayout globalSetLayout, uPtr<S3DDescriptorSetLayout>& terrainDescriptorSetLayout);
		void createPipelines(VkRenderPass gbufferPass);

		void updateActorAABBs(Actor actor);
		void generateMipChain(Actor actor);

		VkDescriptorSet terrainStorageDescriptor{};
		S3DDevice& engineDevice;
		ResourceSystem* resourceSystem;
		VkDescriptorSetLayout globalSetLayout;

		uint32_t framesInFlight;

		uPtr<S3DBuffer> terrainHeightLimitsSSBO;

		uPtr<S3DComputePipeline> terrainPaintHeightMapPipeline;
		uPtr<S3DPipelineLayout> terrainPaintHeightMapPipelineLayout;
		S3DPushConstant terrainPaintHeightMapPush;

		uPtr<S3DComputePipeline> terrainUpdateAABBHeightsPipeline;
		uPtr<S3DPipelineLayout> terrainUpdateAABBHeightsPipelineLayout;
		S3DPushConstant terrainUpdateAABBHeightsPush;

		uPtr<S3DGraphicsPipeline> terrainEditPipeline;
		uPtr<S3DGraphicsPipeline> terrainEditWireframePipeline;
		uPtr<S3DPipelineLayout> terrainPipelineLayout;
		S3DPushConstant terrainPush;

		uPtr<S3DDescriptorSetLayout>& terrainDescriptorSetLayout;
		uPtr<S3DDescriptorSetLayout> terrainStorageDescriptorSetLayout;
	};
}