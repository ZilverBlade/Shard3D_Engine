#include <glm/gtc/constants.hpp>

#include "../tools/editor_assets.h"
#include "object_picking_system.h"
#include <Shard3D/core/asset/assetmgr.h>
#include <Shard3D/core/ecs/components.h>
#include <Shard3D/systems/handlers/render_list.h>
#include <Shard3D/core/misc/engine_settings.h>
#include <Shard3D/core/misc/graphics_settings.h>
#include <Shard3D/ecs.h>
#include <Shard3D/systems/rendering/terrain_system.h>

namespace Shard3D {

	struct TerrainPush {
		glm::vec4 translation;
		glm::vec2 tiles;
		float tileExtent;
		float heightMul;
		uint32_t lod;
		uint32_t objectID;
	};
	struct Billboard {
		glm::vec4 position{};
		uint32_t textureID;
		uint32_t objectID;
	};
	struct Mesh {
		glm::mat4 modelMatrix{};
		uint32_t materialID;
		uint32_t objectID;
	};

	struct MeshDynamic {
		uint32_t actorBuffer;
		uint32_t materialID;
		uint32_t objectID;
	};
	struct IDReadCompute {
		glm::vec2 mouseRelativeCoord;
	};
	ObjectPickingSystem::ObjectPickingSystem(S3DDevice& device, VkDescriptorSetLayout globalSetLayout, VkDescriptorSetLayout riggingDescriptorSetLayout, VkDescriptorSetLayout terrainDescriptorSetLayout, ResourceSystem* resourceSystem_, glm::vec2 resolution) : engineDevice(device), resourceSystem(resourceSystem_){
		createPickingDeviceObjects(resolution);

		pickingDescriptorLayout = S3DDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, VK_SHADER_STAGE_COMPUTE_BIT)
			.addBinding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_COMPUTE_BIT)
			.addBinding(2, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_COMPUTE_BIT)
			.build();
		pickingBuffer = make_uPtr<S3DBuffer>(
			device,
			sizeof(PickingStruct),
			1,
			VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
			);
		pickingBuffer->map();
		VkDescriptorImageInfo pickingAttachment = pickingRenderTarget->getDescriptor();
		VkDescriptorImageInfo pickingDepth = pickingDepthAttachment->getDescriptor();
		VkDescriptorBufferInfo pickingBufferInfo = pickingBuffer->descriptorInfo();
		S3DDescriptorWriter(*pickingDescriptorLayout, *device.staticMaterialPool)
			.writeBuffer(0, &pickingBufferInfo)
			.writeImage(1, &pickingAttachment)
			.writeImage(2, &pickingDepth)
			.build(pickingDescriptor);
		createPipelineLayout(globalSetLayout, riggingDescriptorSetLayout, terrainDescriptorSetLayout);
		createPipeline(pickingRenderPass->getRenderPass());
		readSync.addImageBarrier(SynchronizationAttachment::Color, pickingRenderTarget->getImage(), pickingRenderTarget->getImageSubresourceRange(), VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL);
		depthSync.addImageBarrier(SynchronizationAttachment::Depth, pickingDepthAttachment->getImage(), pickingDepthAttachment->getImageSubresourceRange(), VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL);
	}
	ObjectPickingSystem::~ObjectPickingSystem() {
		delete pickingRenderPass;
		delete pickingRenderTarget;
		delete pickingDepthAttachment;
		delete pickingFramebuffer;
		delete billboardPipelineLayout;
		delete meshPipelineLayout;
		delete meshPipelineLayoutDynamic;
		delete terrainPipelineLayout;
		delete pickingReaderPipelineLayout;
	}

	void ObjectPickingSystem::createPickingDeviceObjects(glm::vec2 resolution) {
		pickingRenderTarget = new S3DRenderTarget(engineDevice, {
			VK_FORMAT_R32_UINT,
			VK_IMAGE_ASPECT_COLOR_BIT,
			VK_IMAGE_VIEW_TYPE_2D,
			{resolution, 1},
			VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
			VK_IMAGE_LAYOUT_GENERAL,
			S3DRenderTargetType::Color,
			false
			}
		);
		pickingDepthAttachment = new S3DRenderTarget(engineDevice, {
			VK_FORMAT_D16_UNORM,
			VK_IMAGE_ASPECT_DEPTH_BIT,
			VK_IMAGE_VIEW_TYPE_2D,
			{resolution, 1},
			VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
			VK_IMAGE_LAYOUT_GENERAL,
			S3DRenderTargetType::Depth,
			false
			}
		);

		AttachmentInfo pickInfo{};
		pickInfo.framebufferAttachment = pickingRenderTarget;
		pickInfo.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		pickInfo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

		AttachmentInfo depthInfo{};
		depthInfo.framebufferAttachment = pickingDepthAttachment;
		depthInfo.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		depthInfo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

		if (ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit) {
			depthInfo.clear.depth = { 0.f, 0 };
		}
		pickingRenderPass = new S3DRenderPass(engineDevice, { pickInfo, depthInfo } );
		pickingFramebuffer = new S3DFramebuffer(engineDevice, pickingRenderPass, {pickingRenderTarget, pickingDepthAttachment});
	}

	void ObjectPickingSystem::createPipelineLayout(VkDescriptorSetLayout globalSetLayout, VkDescriptorSetLayout riggingDescriptorSetLayout, VkDescriptorSetLayout terrainDescriptorSetLayout) {
		billboardPush = S3DPushConstant(sizeof(Billboard), VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT);

		billboardPipelineLayout = new S3DPipelineLayout(engineDevice, { billboardPush.getRange() }, { globalSetLayout, resourceSystem->getDescriptor()->getLayout()} );
		
		meshPush = S3DPushConstant(sizeof(Mesh), VK_SHADER_STAGE_VERTEX_BIT);
		//meshPushDynamic = S3DPushConstant(sizeof(MeshDynamic), VK_SHADER_STAGE_VERTEX_BIT);
		meshPipelineLayout = new S3DPipelineLayout(engineDevice, { meshPush.getRange() }, { globalSetLayout, resourceSystem->getDescriptor()->getLayout() });
		terrainPush = S3DPushConstant(sizeof(TerrainPush), VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT);
		terrainPipelineLayout = new S3DPipelineLayout(engineDevice, { terrainPush.getRange() },
			{ globalSetLayout,  resourceSystem->getDescriptor()->getLayout() , terrainDescriptorSetLayout });

		//meshPipelineLayoutDynamic = new S3DPipelineLayout(engineDevice, { meshPushDynamic.getRange() }, { globalSetLayout, bindless->getLayout() });

		pickingReaderPush = S3DPushConstant(sizeof(IDReadCompute), VK_SHADER_STAGE_COMPUTE_BIT);
		pickingReaderPipelineLayout = new S3DPipelineLayout(engineDevice, { pickingReaderPush.getRange() }, { globalSetLayout, pickingDescriptorLayout->getDescriptorSetLayout() });
	}

	void ObjectPickingSystem::createPipeline(VkRenderPass renderPass) {
		SHARD3D_ASSERT(meshPipelineLayout != nullptr && "Cannot create pipeline before pipeline layout");
		SHARD3D_ASSERT(billboardPipelineLayout != nullptr && "Cannot create pipeline before pipeline layout");
		{
			S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo();
			pipelineConfig.enableVertexDescriptions(VertexDescriptionOptions_Position | VertexDescriptionOptions_UV);
			if (ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit) {
				pipelineConfig.reverseDepth();
			}
			pipelineConfig.renderPass = renderPass;
			pipelineConfig.pipelineLayout = meshPipelineLayout->getPipelineLayout();
			meshPipeline = make_uPtr<S3DGraphicsPipeline>(
				engineDevice, std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/editor/id_pick_mesh.vert", "editor" },
				S3DShader{ ShaderType::Pixel, "resources/shaders/editor/id_pick_mesh.frag", "editor" }
			}, pipelineConfig
			);
			//pipelineConfig.pipelineLayout = meshPipelineLayoutDynamic->getPipelineLayout();
			//meshPipelineDynamic = make_uPtr<S3DGraphicsPipeline>(
			//	engineDevice, std::vector<S3DShader>{
			//	S3DShader{ ShaderType::Vertex, "resources/shaders/editor/id_pick_mesh.vert", "editor" },
			//	S3DShader{ ShaderType::Pixel, "resources/shaders/editor/id_pick_mesh.frag", "editor", {}, { "ACTOR_DYNAMIC" }, "id_pick_mesh_dynamic.frag" }
			//}, pipelineConfig
			//);
		} {
			S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo();
			pipelineConfig.setCullMode(VK_CULL_MODE_BACK_BIT);
			if (ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit) {
				pipelineConfig.reverseDepth();
			}
			pipelineConfig.renderPass = renderPass;
			pipelineConfig.pipelineLayout = billboardPipelineLayout->getPipelineLayout();
			billboardPipeline = make_uPtr<S3DGraphicsPipeline>(
				engineDevice, std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/editor/id_pick_billboard.vert", "editor" },
				S3DShader{ ShaderType::Pixel, "resources/shaders/editor/id_pick_billboard.frag", "editor" }
			}, pipelineConfig
			);
		}{

			S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo(); 
			pipelineConfig.enableVertexDescriptions(VertexDescriptionOptions_Position | VertexDescriptionOptions_UV);
			pipelineConfig.setCullMode(VK_CULL_MODE_BACK_BIT);
			if (ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit) {
				pipelineConfig.reverseDepth();
			}

			pipelineConfig.pipelineLayout = terrainPipelineLayout->getPipelineLayout();
			pipelineConfig.renderPass = renderPass;

			terrainPipeline = make_uPtr<S3DGraphicsPipeline>(
				engineDevice, std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/rendering/terrain.vert", "rendering", {} },
					S3DShader{ ShaderType::Fragment, "resources/shaders/editor/terrain_pick.frag", "rendering" },
			}, pipelineConfig
			);
		}

		pickingReaderPipeline = make_uPtr<S3DComputePipeline>(
			engineDevice, pickingReaderPipelineLayout->getPipelineLayout(), S3DShader{ ShaderType::Compute, "resources/shaders/editor/id_pick_read.comp", "editor" }
		);
	}

	void ObjectPickingSystem::resize(glm::ivec3 newSize) {
		pickingFramebuffer->resize(newSize, pickingRenderPass);		
		readSync.clearBarriers();
		depthSync.clearBarriers();
		readSync.addImageBarrier(SynchronizationAttachment::Color, pickingRenderTarget->getImage(), pickingRenderTarget->getImageSubresourceRange(), VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL);
		depthSync.addImageBarrier(SynchronizationAttachment::Depth, pickingDepthAttachment->getImage(), pickingDepthAttachment->getImageSubresourceRange(), VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL);

		VkDescriptorImageInfo pickingAttachment = pickingRenderTarget->getDescriptor();
		VkDescriptorImageInfo pickingDepth = pickingDepthAttachment->getDescriptor();
		VkDescriptorBufferInfo pickingBufferObject = pickingBuffer->descriptorInfo();
		S3DDescriptorWriter(*pickingDescriptorLayout, *engineDevice.staticMaterialPool)
			.writeBuffer(0, &pickingBufferObject)
			.writeImage(1, &pickingAttachment)
			.writeImage(2, &pickingDepth)
			.build(pickingDescriptor);
	}

	void ObjectPickingSystem::renderObjects(FrameInfo& frameInfo, TerrainActor* terrainActor) {
		if (terrainActor) {
			terrainPipeline->bind(frameInfo.commandBuffer);
			VkDescriptorSet sets[3]{
				frameInfo.globalDescriptorSet,
				frameInfo.resourceSystem->getDescriptor()->getDescriptor(),
				terrainActor->terrainDescriptor
			};
			vkCmdBindDescriptorSets(
				frameInfo.commandBuffer,
				VK_PIPELINE_BIND_POINT_GRAPHICS,
				terrainPipelineLayout->getPipelineLayout(),
				0,
				3,
				sets,
				0,
				nullptr
			);
			TerrainPush push;
			push.heightMul = 1.0f;
			push.tileExtent = terrainActor->tileExtent;
			push.tiles = { terrainActor->numTilesX, terrainActor->numTilesY };
			push.translation = glm::vec4(0.f);
			for (int i = 0; i < terrainActor->getLODCount(); i++) {
				push.lod = i;
				terrainPush.push(frameInfo.commandBuffer, terrainPipelineLayout->getPipelineLayout(), push);
				terrainActor->render(frameInfo, i);
			}
		}
		meshPipeline->bind(frameInfo.commandBuffer);
		{
			VkDescriptorSet sets[2]{
				frameInfo.globalDescriptorSet,
				resourceSystem->getDescriptor()->getDescriptor()
			};
			vkCmdBindDescriptorSets(
				frameInfo.commandBuffer,
				VK_PIPELINE_BIND_POINT_GRAPHICS,
				meshPipelineLayout->getPipelineLayout(),
				0,
				2,
				sets,
				0,
				nullptr
			);
		}
		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::DRAW, "EDITOR_PICKING_MESHES", frameInfo.commandBuffer);
		for (auto& iter : frameInfo.renderObjects.renderList->getIterator(ProjectSystem::getEngineSettings().allowedMaterialPermutations, MeshMobilityOption_All)) {
			for (auto& [actor_e, mesh] : iter.renderList) {
				Mesh3D* mesh_r = frameInfo.resourceSystem->retrieveMesh(mesh.mesh->mesh);
				Actor actor = { actor_e, frameInfo.level.get() };
				Mesh push{};
				push.modelMatrix = mesh.mesh->transform;
				if (actor.isPrefabChild()) { // check if owned by prefab 
					UUID scope = actor.getInstancerUUID();
					while (scope != 0) {
						actor = frameInfo.level->getActorFromUUID(0, actor.getInstancerUUID());
						scope = actor.getInstancerUUID();
					}
					push.objectID = static_cast<uint32_t>(actor.actorHandle);
				} else {
					push.objectID = static_cast<uint32_t>(actor.actorHandle);
				}
				for (int index : mesh.material_indices) {
					push.materialID = frameInfo.resourceSystem->retrieveMaterial(mesh.mesh->materials[index])->getShaderMaterialID();
					meshPush.push(frameInfo.commandBuffer, meshPipelineLayout->getPipelineLayout(), push);
					mesh_r->draw(frameInfo.commandBuffer, index);
				}
			}
		}

		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::DRAW, "EDITOR_PICKING_BILLBOARDS", frameInfo.commandBuffer);
		billboardPipeline->bind(frameInfo.commandBuffer);

		{
			VkDescriptorSet sets[2]{
				frameInfo.globalDescriptorSet,
				frameInfo.resourceSystem->getDescriptor()->getDescriptor()
			};
			vkCmdBindDescriptorSets(
				frameInfo.commandBuffer,
				VK_PIPELINE_BIND_POINT_GRAPHICS,
				billboardPipelineLayout->getPipelineLayout(),
				0,  // first set
				2,  // set count
				sets,
				0,
				nullptr
			);
		}
		renderComponent<Components::AudioComponent>("component.audio", frameInfo);
		renderComponent<Components::PointLightComponent>("component.light.point", frameInfo);
		renderComponent<Components::SpotLightComponent>("component.light.spot", frameInfo);
		renderComponent<Components::DirectionalLightComponent>("component.light.directional", frameInfo);
		renderComponent<Components::VirtualPointLightComponent>("component.light.virtualpoint", frameInfo);
		renderComponent<Components::BoxReflectionCaptureComponent>("component.reflection", frameInfo);
		renderComponent<Components::BoxAmbientOcclusionVolumeComponent>("component.aovolume", frameInfo);
		renderComponent<Components::SkyLightComponent>("component.skylight", frameInfo);
		renderComponent<Components::ExponentialFogComponent>("component.expfog", frameInfo);
	}
	
	void ObjectPickingSystem::pickObjects(FrameInfo& frameInfo, glm::vec2 mousePosition) {

		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::SYNC_BARRIER, "EDITOR_PICKING_IDENTIFIERS_SYNC", frameInfo.commandBuffer);
		readSync.syncBarrier(frameInfo.commandBuffer);
		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::SYNC_BARRIER, "EDITOR_PICKING_DEPTH_SYNC", frameInfo.commandBuffer);
		depthSync.syncBarrier(frameInfo.commandBuffer);

		float mX = GraphicsSettings2::getRuntimeInfo().localScreen.x;
		float mY = GraphicsSettings2::getRuntimeInfo().localScreen.y;
		float mWidth = GraphicsSettings2::getRuntimeInfo().localScreen.z;
		float mHeight = GraphicsSettings2::getRuntimeInfo().localScreen.w;

		float xClipped = (mousePosition.x - mX) / (mWidth);
		float yClipped = (mousePosition.y - mY) / (mHeight);
		pickingReaderPipeline->bind(frameInfo.commandBuffer);
		IDReadCompute idreadPush{};
		idreadPush.mouseRelativeCoord = { xClipped, yClipped };
		pickingReaderPush.push(frameInfo.commandBuffer, pickingReaderPipelineLayout->getPipelineLayout(), idreadPush);
		VkDescriptorSet computePickingSets[2]{
			frameInfo.globalDescriptorSet,
			pickingDescriptor
		};
		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_COMPUTE,
			pickingReaderPipelineLayout->getPipelineLayout(),
			0,
			2,
			computePickingSets,
			0,
			nullptr
		);
		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::COMPUTE_DISPATCH, "EDITOR_PICKING_READ_DATA", frameInfo.commandBuffer);
		vkCmdDispatch(frameInfo.commandBuffer, 1, 1, 1);
	}

	void ObjectPickingSystem::beginPickingPass(FrameInfo& frameInfo) {
		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::BEGIN_RENDER_PASS, "EDITOR_PICKING", frameInfo.commandBuffer);
		pickingRenderPass->beginRenderPass(frameInfo.commandBuffer, pickingFramebuffer);
	}

	void ObjectPickingSystem::endPickingPass(FrameInfo& frameInfo) {
		MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::END_RENDER_PASS, "EDITOR_PICKING", frameInfo.commandBuffer);
		pickingRenderPass->endRenderPass(frameInfo.commandBuffer);
	}

	template<typename T>
	void ObjectPickingSystem::renderComponent(const char* iconName, FrameInfo& frameInfo) {
		auto texture = _special_assets::_get().at(iconName)->getResourceIndex();
		auto view = frameInfo.level->registry.view<T, Components::TransformComponent>();
		for (auto& obj : view) {
			Actor actor = { obj, frameInfo.level.get() };
			Billboard push{};

			push.position = actor.getTransform().transformMatrix[3];
			push.textureID = texture;
			if (actor.isPrefabChild()) { // check if owned by prefab 
				UUID scope = actor.getInstancerUUID();
				while (scope != 0) {
					actor = frameInfo.level->getActorFromUUID(0, actor.getInstancerUUID());
					scope = actor.getInstancerUUID();
				}
				push.objectID = static_cast<uint32_t>(actor.actorHandle);
			} else {
				push.objectID = static_cast<uint32_t>(obj);
			}

			billboardPush.push(frameInfo.commandBuffer, billboardPipelineLayout->getPipelineLayout(), push);
			vkCmdDraw(frameInfo.commandBuffer, 6, 1, 0, 0);
		}
	}
}