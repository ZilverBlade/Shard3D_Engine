#include <glm/gtc/constants.hpp>

#include "arrow_renderer.h"
#include <Shard3D/core/asset/assetmgr.h>
#include <Shard3D/core/ecs/actor.h>
namespace Shard3D {
	ArrowRenderer::ArrowRenderer(S3DDevice& device, VkRenderPass translucentRenderPass, VkDescriptorSetLayout globalSetLayout) : engineDevice{ device } {
		createPipelineLayout(globalSetLayout);
		createPipeline(translucentRenderPass);
	}
	ArrowRenderer::~ArrowRenderer() {
		vkDestroyPipelineLayout(engineDevice.device(), pipelineLayout, nullptr);
	}
	void ArrowRenderer::render(FrameInfo& frameInfo, const std::vector<ArrowRenderInfo>& points) {
		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			pipelineLayout, 
			0,
			1,
			&frameInfo.globalDescriptorSet, 
			0, 
			nullptr
		);
		graphicsPipeline->bind(frameInfo.commandBuffer);

		for (auto& point : points) {
			vkCmdPushConstants(frameInfo.commandBuffer, pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(ArrowRenderInfo), &point);
			frameInfo.resourceSystem->retrieveMesh(ResourceSystem::coreAssets.m_arrow)->drawAll(frameInfo.commandBuffer);
		}
	}

	void ArrowRenderer::createPipelineLayout(VkDescriptorSetLayout globalSetLayout) {
		{
			VkPushConstantRange pushConstantRange{};
			pushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
			pushConstantRange.offset = 0;
			pushConstantRange.size = sizeof(ArrowRenderInfo);

			std::vector<VkDescriptorSetLayout> descriptorSetLayouts{
				globalSetLayout
			};

			VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
			pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
			pipelineLayoutInfo.setLayoutCount = (uint32_t)descriptorSetLayouts.size();
			pipelineLayoutInfo.pSetLayouts = descriptorSetLayouts.data();
			pipelineLayoutInfo.pushConstantRangeCount = 1;
			pipelineLayoutInfo.pPushConstantRanges = &pushConstantRange;
			VK_ASSERT(vkCreatePipelineLayout(engineDevice.device(), &pipelineLayoutInfo, nullptr, &pipelineLayout), "failed to create pipeline layout!");
		}
	}

	void ArrowRenderer::createPipeline(VkRenderPass translucentRenderPass) {
		{
			S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo(2);
			pipelineConfig.enableWeightedBlending();
			pipelineConfig.enableVertexDescriptions(VertexDescriptionOptions_Position);

			if (ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit) {
				pipelineConfig.reverseDepth();
			}
			if (ProjectSystem::getGraphicsSettings().renderer.msaa) {
				pipelineConfig.setSampleCount((VkSampleCountFlagBits)ProjectSystem::getGraphicsSettings().renderer.msaaSamples);
			}
			pipelineConfig.renderPass = translucentRenderPass;
			pipelineConfig.pipelineLayout = pipelineLayout;
			graphicsPipeline = make_uPtr<S3DGraphicsPipeline>(
				engineDevice,
				std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/editor/arrow.vert", "editor" },
					S3DShader{ ShaderType::Pixel, "resources/shaders/fragment_color.frag", "misc" }
			},
				pipelineConfig
					);
		}
	}
}