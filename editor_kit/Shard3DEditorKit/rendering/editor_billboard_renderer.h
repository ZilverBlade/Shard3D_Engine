#pragma once

#include "../core.h"
#include <Shard3D/s3dstd.h>
#include <Shard3D/vulkan_abstr.h>
#include <Shard3D/core/ecs/level.h>

namespace Shard3D {
	class EditorBillboardRenderer {
	public:
		S3D_DLLAPI EditorBillboardRenderer(S3DDevice &device, ResourceSystem* resourceSystem, VkRenderPass opaqueRenderPass, VkRenderPass translucentRenderPass, VkDescriptorSetLayout globalSetLayout);
		S3D_DLLAPI ~EditorBillboardRenderer();

		S3D_DLLAPI EditorBillboardRenderer(const EditorBillboardRenderer&) = delete;
		S3D_DLLAPI EditorBillboardRenderer& operator=(const EditorBillboardRenderer&) = delete;

		S3D_DLLAPI void renderBillboards(FrameInfo &frameInfo);
		S3D_DLLAPI void renderBoxVolumes(FrameInfo& frameInfo);
		S3D_DLLAPI void visualiseMeshVolumes(FrameInfo& frameInfo);
	private:

		S3D_DLLAPI void createPipelineLayout(VkDescriptorSetLayout globalSetLayout);
		S3D_DLLAPI void createPipeline(VkRenderPass opaqueRenderPass, VkRenderPass translucentRenderPass);

		template <typename T>
		S3D_DLLAPI void renderComponent(const char* iconName, FrameInfo& frameInfo);
		template <typename T>
		S3D_DLLAPI void renderVolume(FrameInfo& frameInfo);

		S3DDevice& engineDevice;
		ResourceSystem* resourceSystem;

		uPtr<S3DGraphicsPipeline> graphicsPipeline;
		VkPipelineLayout pipelineLayout;

		uPtr<S3DGraphicsPipeline> boxVisualizerPipeline;
		VkPipelineLayout boxPipelineLayout;

		uPtr<S3DGraphicsPipeline> meshVolumeVisualizerPipeline;
		VkPipelineLayout meshVolumePipelineLayout;
	};
}