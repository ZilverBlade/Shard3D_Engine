#include "highlight_renderer.h"
#include "../tools/editor_assets.h"
#include "object_picking_system.h"
#include <Shard3D/core/asset/assetmgr.h>
#include <Shard3D/core/ecs/components.h>
#include <Shard3D/core/misc/engine_settings.h>
#include <Shard3D/core/misc/graphics_settings.h>
#include <Shard3D/ecs.h>
namespace Shard3D {
	struct Billboard {
		glm::vec4 position{};
		uint32_t textureID;
	};
	struct Mesh {
		glm::mat4 modelMatrix{};
		uint32_t materialID;
		uint32_t objectID;
	};
	HighlightRenderer::HighlightRenderer(
		S3DDevice& device, VkRenderPass renderPass, VkDescriptorSetLayout globalSetLayout, uPtr<SmartDescriptorSet>& bindless_
	) : engineDevice(device), bindless(bindless_) {
		createPipelineLayout(globalSetLayout);
		createPipeline(renderPass);
	}

	HighlightRenderer::~HighlightRenderer() {
		delete billboardPipelineLayout;
		delete meshPipelineLayout;
	}

	void HighlightRenderer::renderHighlight(FrameInfo& frameInfo, const std::vector<Actor>& actors) {
		for (auto& actor : actors) {
			uint32_t m = static_cast<uint32_t>(actor.getMobility());
			uint32_t mob = m * 2 | uint32_t(m == 0x0) * 1;
			bool rig = actor.hasComponent<Components::SkeletonRigComponent>();
			for (auto& class_ : frameInfo.renderObjects.renderList->getRenderUsingClasses(actor, rig, mob)) {
				meshPipeline->bind(frameInfo.commandBuffer);
				VkDescriptorSet maskedSets[2]{
					frameInfo.globalDescriptorSet,
					bindless->getDescriptor()
				};
				vkCmdBindDescriptorSets(
					frameInfo.commandBuffer,
					VK_PIPELINE_BIND_POINT_GRAPHICS,
					meshPipelineLayout->getPipelineLayout(),
					0,
					2,
					maskedSets,
					0,
					nullptr
				);
				auto& renderInfo = frameInfo.renderObjects.renderList->getSurfaceMaterialRenderingList()[rig][mob][class_][actor];
				Mesh3D* mesh = frameInfo.resourceSystem->retrieveMesh(renderInfo.mesh->mesh);
				for (auto& index : renderInfo.material_indices) {
					Mesh push{};
					push.modelMatrix = renderInfo.mesh->transform;
					push.materialID = frameInfo.resourceSystem->retrieveMaterial(renderInfo.mesh->materials[index])->getShaderMaterialID();
					meshPush.push(frameInfo.commandBuffer, meshPipelineLayout->getPipelineLayout(), push);
					mesh->draw(frameInfo.commandBuffer, index);
				}
			}

			VkDescriptorSet sets[2]{ frameInfo.globalDescriptorSet, bindless->getDescriptor() };
			vkCmdBindDescriptorSets(
				frameInfo.commandBuffer,
				VK_PIPELINE_BIND_POINT_GRAPHICS,
				billboardPipelineLayout->getPipelineLayout(),
				0,  // first set
				2,  // set count
				sets,
				0,
				nullptr);
			if (actor.hasComponent<Components::AudioComponent>()) renderComponentHighlight<Components::AudioComponent>("component.audio", frameInfo, actor);
			if (actor.hasComponent<Components::PointLightComponent>()) renderComponentHighlight<Components::PointLightComponent>("component.light.point", frameInfo, actor);
			if (actor.hasComponent<Components::SpotLightComponent>()) renderComponentHighlight<Components::SpotLightComponent>("component.light.spot", frameInfo, actor);
			if (actor.hasComponent<Components::DirectionalLightComponent>()) renderComponentHighlight<Components::DirectionalLightComponent>("component.light.directional", frameInfo, actor);
			if (actor.hasComponent<Components::VirtualPointLightComponent>()) renderComponentHighlight<Components::DirectionalLightComponent>("component.light.virtualpoint", frameInfo, actor);
			if (actor.hasComponent<Components::BoxReflectionCaptureComponent>()) renderComponentHighlight<Components::BoxReflectionCaptureComponent>("component.reflection", frameInfo, actor);
			if (actor.hasComponent<Components::SkyLightComponent>()) renderComponentHighlight<Components::SkyLightComponent>("component.skylight", frameInfo, actor);
			if (actor.hasComponent<Components::ExponentialFogComponent>()) renderComponentHighlight<Components::ExponentialFogComponent>("component.expfog", frameInfo, actor);
		}
	}

	void HighlightRenderer::createPipelineLayout(VkDescriptorSetLayout globalSetLayout) {
		billboardPush = S3DPushConstant(sizeof(Billboard), VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT);

		billboardPipelineLayout = new S3DPipelineLayout(engineDevice, { billboardPush.getRange() }, { globalSetLayout, bindless->getLayout() });

		meshPush = S3DPushConstant(sizeof(Mesh), VK_SHADER_STAGE_VERTEX_BIT);
		meshPipelineLayout = new S3DPipelineLayout(engineDevice, { meshPush.getRange() }, { globalSetLayout, bindless->getLayout() });
	}

	void HighlightRenderer::createPipeline(VkRenderPass renderPass) {
		SHARD3D_ASSERT(meshPipelineLayout != nullptr && "Cannot create pipeline before pipeline layout");
		SHARD3D_ASSERT(billboardPipelineLayout != nullptr && "Cannot create pipeline before pipeline layout");
		//SHARD3D_NOIMPL_C("Highligher renderer pipeline needs to be rewritten!");
		{
			S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo(2);
			pipelineConfig.enableVertexDescriptions(VertexDescriptionOptions_Position | VertexDescriptionOptions_UV);
			pipelineConfig.enableWeightedBlending();
			//pipelineConfig.disableDepthTest();
			//pipelineConfig.lineRasterizer(5.f);
			if (ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit) {
				pipelineConfig.reverseDepth();
			}
			if (ProjectSystem::getGraphicsSettings().renderer.msaa) {
				pipelineConfig.setSampleCount((VkSampleCountFlagBits)ProjectSystem::getGraphicsSettings().renderer.msaaSamples);
			}
			pipelineConfig.renderPass = renderPass;
			pipelineConfig.pipelineLayout = meshPipelineLayout->getPipelineLayout();
			meshPipeline = make_uPtr<S3DGraphicsPipeline>(
				engineDevice, std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/editor/id_pick_mesh.vert", "editor" },
					S3DShader{ ShaderType::Pixel, "resources/shaders/editor/id_pick_mesh_hl.frag", "editor" }
			}, pipelineConfig
				);
		} {
			S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo(2);
			pipelineConfig.setCullMode(VK_CULL_MODE_BACK_BIT);
			pipelineConfig.enableWeightedBlending();
			//pipelineConfig.disableDepthTest();
			if (ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit) {
				pipelineConfig.reverseDepth();
			}
			if (ProjectSystem::getGraphicsSettings().renderer.msaa) {
				pipelineConfig.setSampleCount((VkSampleCountFlagBits)ProjectSystem::getGraphicsSettings().renderer.msaaSamples);
			}
			pipelineConfig.renderPass = renderPass;
			pipelineConfig.pipelineLayout = billboardPipelineLayout->getPipelineLayout();
			billboardPipeline = make_uPtr<S3DGraphicsPipeline>(
				engineDevice, std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/editor/id_pick_billboard.vert", "editor" },
					S3DShader{ ShaderType::Pixel, "resources/shaders/editor/id_pick_billboard_hl.frag", "editor" }
			}, pipelineConfig
				);
		}

	}

	template<typename T>
	void HighlightRenderer::renderComponentHighlight(const char* iconName, FrameInfo& frameInfo, Actor actor) {
		billboardPipeline->bind(frameInfo.commandBuffer);
		auto texture = _special_assets::_get().at(iconName)->getResourceIndex();

		Billboard push{};

		push.textureID = texture;
		push.position = actor.getTransform().transformMatrix[3];

		billboardPush.push(frameInfo.commandBuffer, billboardPipelineLayout->getPipelineLayout(), push);
		vkCmdDraw(frameInfo.commandBuffer, 6, 1, 0, 0);	
	}
}