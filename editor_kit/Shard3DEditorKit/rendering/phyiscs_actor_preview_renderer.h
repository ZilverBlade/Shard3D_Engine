#pragma once

#include "../core.h"
#include <Shard3D/s3dstd.h>
#include <Shard3D/vulkan_abstr.h>
#include <Shard3D/core/ecs/level.h>
#include <Shard3D/core/ecs/components.h>

namespace Shard3D {
	class PhysicsAsset;
	struct TerrainPhysicsData;
	class VertexArenaAllocator;
	class PhysicsActorPreviewRenderer {
	public:
		PhysicsActorPreviewRenderer(S3DDevice &device, VkRenderPass translucentRenderPass, VkDescriptorSetLayout globalSetLayout);
		~PhysicsActorPreviewRenderer();

		PhysicsActorPreviewRenderer(const PhysicsActorPreviewRenderer&) = delete;
		PhysicsActorPreviewRenderer& operator=(const PhysicsActorPreviewRenderer&) = delete;

		void renderWireframe(FrameInfo &frameInfo);
	private:
		struct PhysicsActorRenderInfo {
			PhysicsAsset asset;
			std::vector<sPtr<S3DBuffer>> meshesInFlight;
			std::vector<VkDescriptorSet> setsInFlight;
		};

		void createPipelineLayout(VkDescriptorSetLayout globalSetLayout);
		void createPipeline(VkRenderPass translucentRenderPass);

		sPtr<S3DBuffer> createConvexActorMesh(PhysicsAsset asset);
		uint32_t createTerrainActorMesh(TerrainPhysicsData* actor, VertexArenaAllocator* allocator);
		void updateConvexActorMesh(PhysicsActorRenderInfo& actor, uint32_t frameIndex);

		S3DDevice& engineDevice;
		uPtr<S3DGraphicsPipeline> graphicsPipeline;
		uPtr<S3DGraphicsPipeline> graphicsPipelineTerrain;
		VkPipelineLayout pipelineLayout;

		uPtr<S3DDescriptorSetLayout> convexHullDataDescriptorLayout;
		std::unordered_map<entt::entity, PhysicsActorRenderInfo> physicsActorVertexVisualizationBuffers;

		uint32_t terrainMeshID = 0;

		VkDescriptorSet dummyset{};
		uPtr<S3DBuffer> dummybuffer;
	};
}