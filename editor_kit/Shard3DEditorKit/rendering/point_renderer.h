#pragma once

#include "../core.h"
#include <Shard3D/s3dstd.h>
#include <Shard3D/vulkan_abstr.h>
#include <Shard3D/core/ecs/level.h>
#include <Shard3D/core/ecs/components.h>

namespace Shard3D {
	struct PointRenderInfo {
		glm::vec3 position = { 0.f, 0.f, 0.f };
		glm::vec4 color = {1.f, 1.f, 1.f, 0.5f};
		float size = 8.0f;
		float weight = 1.0f;
	};
	class PointRenderer {
	public:
		PointRenderer(S3DDevice &device, VkRenderPass translucentRenderPass, VkDescriptorSetLayout globalSetLayout);
		~PointRenderer();

		PointRenderer(const PointRenderer&) = delete;
		PointRenderer& operator=(const PointRenderer&) = delete;

		void render(FrameInfo &frameInfo, const std::vector<PointRenderInfo>& points);
	private:
		void createPipelineLayout(VkDescriptorSetLayout globalSetLayout);
		void createPipeline(VkRenderPass translucentRenderPass);

		S3DDevice& engineDevice;
		uPtr<S3DGraphicsPipeline> graphicsPipeline;
		VkPipelineLayout pipelineLayout;
	};
}