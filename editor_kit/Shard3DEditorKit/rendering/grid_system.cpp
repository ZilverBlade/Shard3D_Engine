#include <glm/gtc/constants.hpp>

#include "grid_system.h"
#include <Shard3D/utils/logger.h>

namespace Shard3D {

	GridSystem::GridSystem(S3DDevice& device, VkRenderPass renderPass, VkDescriptorSetLayout globalSetLayout) : engineDevice{ device } {
		createPipelineLayout(globalSetLayout);
		createPipeline(renderPass);
	}

	GridSystem::~GridSystem() {
		vkDestroyPipelineLayout(engineDevice.device(), pipelineLayout, nullptr);
	}

	void GridSystem::createPipelineLayout(VkDescriptorSetLayout globalSetLayout) {
		std::vector<VkDescriptorSetLayout> descriptorSetLayouts{ globalSetLayout };

		VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
		pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		pipelineLayoutInfo.setLayoutCount = static_cast<uint32_t>(descriptorSetLayouts.size());
		pipelineLayoutInfo.pSetLayouts = descriptorSetLayouts.data();
		pipelineLayoutInfo.pushConstantRangeCount = 0;
		pipelineLayoutInfo.pPushConstantRanges = nullptr;
		VK_ASSERT(vkCreatePipelineLayout(engineDevice.device(), &pipelineLayoutInfo, nullptr, &pipelineLayout),"failed to create pipeline layout!");
	}

	void GridSystem::createPipeline(VkRenderPass renderPass) {
		SHARD3D_ASSERT(pipelineLayout != nullptr && "Cannot create pipeline before pipeline layout");

		S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo(2);
		pipelineConfig.enableWeightedBlending();
		pipelineConfig.disableDepthWrite();
		if (ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit) {
			pipelineConfig.reverseDepth();
		}
		if (ProjectSystem::getGraphicsSettings().renderer.msaa) {
			pipelineConfig.setSampleCount((VkSampleCountFlagBits)ProjectSystem::getGraphicsSettings().renderer.msaaSamples);
		}
		pipelineConfig.renderPass = renderPass;
		pipelineConfig.pipelineLayout = pipelineLayout;
		graphicsPipeline = make_uPtr<S3DGraphicsPipeline>(
			engineDevice, std::vector<S3DShader>{
			S3DShader{ ShaderType::Vertex, "resources/shaders/editor/grid.vert", "editor" },
				S3DShader{ ShaderType::Pixel, "resources/shaders/editor/grid.frag", "editor" }
		}, pipelineConfig
			);
	}

	void GridSystem::render(FrameInfo& frameInfo) {
		graphicsPipeline->bind(frameInfo.commandBuffer);
		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			pipelineLayout,
			0,
			1,
			&frameInfo.globalDescriptorSet,
			0,
			nullptr
		);
		vkCmdDraw(frameInfo.commandBuffer, 6, 1, 0, 0);
	}
}