#pragma once

#include "../core.h"
#include <Shard3D/s3dstd.h>
#include <Shard3D/vulkan_abstr.h>
#include <Shard3D/core/ecs/level.h>
#include <Shard3D/core/rendering/framebuffer.h>
#include <Shard3D/core/rendering/render_pass.h>
#include <Shard3D/systems/handlers/material_system.h>

namespace Shard3D {
	class TerrainActor;
	struct PickingStruct {
		glm::vec4 mouseWorldPosition;
		uint32_t objectID;
	};
	class ObjectPickingSystem {
	public:
		S3D_DLLAPI ObjectPickingSystem(S3DDevice& device, VkDescriptorSetLayout globalSetLayout, VkDescriptorSetLayout riggingDescriptorSetLayout, VkDescriptorSetLayout terrainDescriptorSetLayout, ResourceSystem* resourceSystem_, glm::vec2 resolution);
		S3D_DLLAPI ~ObjectPickingSystem();

		S3D_DLLAPI ObjectPickingSystem(const ObjectPickingSystem&) = delete;
		S3D_DLLAPI ObjectPickingSystem& operator=(const ObjectPickingSystem&) = delete;

		S3D_DLLAPI void resize(glm::ivec3 newSize);
		S3D_DLLAPI void renderObjects(FrameInfo& frameInfo, TerrainActor* terrainActor);
		void pickObjects(FrameInfo& frameInfo, glm::vec2 mousePosition);
		void beginPickingPass(FrameInfo& frameInfo);
		void endPickingPass(FrameInfo& frameInfo);

		S3D_DLLAPI uint32_t getActiveObject() {
			uint32_t selectedHandle = reinterpret_cast<PickingStruct*>(pickingBuffer->getMappedMemory())->objectID;
			return selectedHandle;
		}
		S3D_DLLAPI glm::vec4 getWorldPosition() {
			glm::vec4 position = reinterpret_cast<PickingStruct*>(pickingBuffer->getMappedMemory())->mouseWorldPosition;
			//SHARD3D_INFO("worldPos = ({0},{1},{2})", position.x, position.y, position.z);
			return position;
		}
		S3DRenderPass* getRenderPass() {
			return pickingRenderPass;
		}
	private:
		S3D_DLLAPI void createPickingDeviceObjects(glm::vec2 resolution);
		S3D_DLLAPI void createPipelineLayout(VkDescriptorSetLayout globalSetLayout, VkDescriptorSetLayout riggingDescriptorSetLayout, VkDescriptorSetLayout terrainDescriptorSetLayout);
		S3D_DLLAPI void createPipeline(VkRenderPass renderPass);
		template <typename T>
		S3D_DLLAPI void renderComponent(const char* iconName, FrameInfo& frameInfo);

		S3DDevice& engineDevice;
		uPtr<S3DGraphicsPipeline> billboardPipeline;
		S3DPushConstant billboardPush{};
		S3DPipelineLayout* billboardPipelineLayout{};

		uPtr<S3DGraphicsPipeline> meshPipeline;
		uPtr<S3DGraphicsPipeline> meshPipelineDynamic;
		uPtr<S3DGraphicsPipeline> terrainPipeline;
		S3DPushConstant meshPush{};
		S3DPushConstant meshPushDynamic{};
		S3DPipelineLayout* meshPipelineLayout{};
		S3DPipelineLayout* meshPipelineLayoutDynamic{};
		S3DPipelineLayout* terrainPipelineLayout{};

		S3DPushConstant terrainPush;

		uPtr<S3DComputePipeline> pickingReaderPipeline;
		S3DPushConstant pickingReaderPush{};
		S3DPipelineLayout* pickingReaderPipelineLayout;

		S3DSynchronization readSync{ SynchronizationType::GraphicsToCompute, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
		S3DSynchronization depthSync{ SynchronizationType::GraphicsToCompute, VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT };

		ResourceSystem* resourceSystem;
		uPtr<S3DDescriptorSetLayout> pickingDescriptorLayout{};
		VkDescriptorSet pickingDescriptor{};
		uPtr<S3DBuffer> pickingBuffer{};
		uPtr<S3DDescriptorSetLayout> pickingBufferDescriptorLayout{};
		VkDescriptorSet pickingBufferDescriptor{};

		S3DRenderPass* pickingRenderPass{};
		S3DRenderTarget* pickingRenderTarget{};
		S3DRenderTarget* pickingDepthAttachment{};
		S3DFramebuffer* pickingFramebuffer{};
	};
}