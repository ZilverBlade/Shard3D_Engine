#include <glm/gtc/constants.hpp>

#include "../tools/editor_assets.h"
#include "phyiscs_actor_preview_renderer.h"
#include <Shard3D/core/asset/assetmgr.h>
#include <Shard3D/core/ecs/actor.h>
#include <Shard3D/systems/rendering/terrain_system.h>
#include <Shard3D/systems/handlers/vertex_arena_allocator.h>
#include <ReactorPhysicsEngine/colliders/convex_hull.h>
#include <ReactorPhysicsEngine/convex_shape.h>
namespace Shard3D {
	struct ColliderVisualizerPush {
		glm::mat4 transformMatrix{};
		glm::vec4 color{};
		PhysicsColliderType type;
	};

	PhysicsActorPreviewRenderer::PhysicsActorPreviewRenderer(S3DDevice& device, VkRenderPass translucentRenderPass, VkDescriptorSetLayout globalSetLayout) : engineDevice{ device } {
		createPipelineLayout(globalSetLayout);
		createPipeline(translucentRenderPass);
		
		convexHullDataDescriptorLayout = S3DDescriptorSetLayout::Builder(engineDevice).addBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT).build();

		dummybuffer = make_uPtr<S3DBuffer>(engineDevice, sizeof(glm::vec4), 1, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
		S3DDescriptorWriter(*convexHullDataDescriptorLayout, *device.staticMaterialPool)
			.writeBuffer(0, &dummybuffer->descriptorInfo())
			.build(dummyset);
	}
	PhysicsActorPreviewRenderer::~PhysicsActorPreviewRenderer() {
		vkDestroyPipelineLayout(engineDevice.device(), pipelineLayout, nullptr);
		engineDevice.staticMaterialPool->freeDescriptors({ dummyset });
	}

	void PhysicsActorPreviewRenderer::renderWireframe(FrameInfo& frameInfo) {
		auto& view = frameInfo.level->registry.view<Components::TransformComponent, Components::Rigidbody3DComponent>();
		for (auto& a : view) {
			Actor actor = { a, frameInfo.level.get() };
			auto& rbc = actor.getComponent<Components::Rigidbody3DComponent>();
			auto& tfc = actor.getTransform();
			if (rbc.asset.colliderType == PhysicsColliderType::Terrain) {
				if (terrainMeshID == 0) {
					terrainMeshID = createTerrainActorMesh(actor.getComponent<Components::TerrainComponent>().physData, frameInfo.resourceSystem->getVertexAllocator());
				}
			} else {
				bool requireRebuild = physicsActorVertexVisualizationBuffers.find(actor) == physicsActorVertexVisualizationBuffers.end();
				if (!requireRebuild) {
					if (physicsActorVertexVisualizationBuffers[actor].asset.colliderType == PhysicsColliderType::ConvexHullAsset || rbc.asset.colliderType == PhysicsColliderType::ConvexHullAsset) {
						requireRebuild = physicsActorVertexVisualizationBuffers[actor].asset.colliderType != rbc.asset.colliderType;
					}
				}
				if (requireRebuild) {
					if (rbc.asset.colliderType == PhysicsColliderType::ConvexHullAsset) {
						if (rbc.asset.convexPoints.size() < 4) continue;
						physicsActorVertexVisualizationBuffers[actor] = {
							rbc.asset,
							std::vector<sPtr<S3DBuffer>>(ProjectSystem::getGraphicsSettings().renderer.framesInFlight),
							std::vector<VkDescriptorSet>(ProjectSystem::getGraphicsSettings().renderer.framesInFlight)
						};
						for (int i = 0; i < ProjectSystem::getGraphicsSettings().renderer.framesInFlight; i++) {
							physicsActorVertexVisualizationBuffers[actor].meshesInFlight[i] = createConvexActorMesh(rbc.asset);
							S3DDescriptorWriter(*convexHullDataDescriptorLayout, *engineDevice.staticMaterialPool)
								.writeBuffer(0, &physicsActorVertexVisualizationBuffers[actor].meshesInFlight[i]->descriptorInfo())
								.build(physicsActorVertexVisualizationBuffers[actor].setsInFlight[i]);
						}
					} else {
						physicsActorVertexVisualizationBuffers[actor] = {
							rbc.asset,
							{},
							{}
						};
					}
				} else {
					physicsActorVertexVisualizationBuffers[actor].asset = rbc.asset;
				}
			}
		}

		VkDescriptorSet sets[]{
			frameInfo.globalDescriptorSet,
			dummyset
		};
		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			pipelineLayout,
			0,
			2,
			sets,
			0,
			nullptr
		);

		std::vector<Actor> eraseActors;

		graphicsPipeline->bind(frameInfo.commandBuffer);
		for (auto& [a, ph] : physicsActorVertexVisualizationBuffers) {
			Actor actor = { a, frameInfo.level.get() };
			if (actor.isInvalid()) {
				eraseActors.push_back(actor);
				continue;
			} else if (!actor.hasComponent<Components::Rigidbody3DComponent>()) {
				eraseActors.push_back(actor);
				continue;
			}
			auto& tfc = actor.getTransform();

			ColliderVisualizerPush push{};
			push.type = ph.asset.colliderType;
			push.color = { 1.0f, 0.4f, 0.0f, 0.8f };
			glm::mat4 rotationTransform = glm::mat4(tfc.getRotationMatrix());
			if (ph.asset.colliderType == PhysicsColliderType::ConvexHullAsset) {
				updateConvexActorMesh(ph, frameInfo.frameIndex);
				vkCmdBindDescriptorSets(
					frameInfo.commandBuffer,
					VK_PIPELINE_BIND_POINT_GRAPHICS,
					pipelineLayout,
					1,
					1,
					&ph.setsInFlight[frameInfo.frameIndex],
					0,
					nullptr
				);


				//auto x = Components::TransformComponent();
				//x.setRotation(tfc.getScale());
				//glm::mat4 rotOff = x.calculateMat4();
				//glm::mat4 transform = tfc.transformMatrix;
				//transform[0] /= glm::length(transform[0]);
				//transform[1] /= glm::length(transform[1]);
				//transform[2] /= glm::length(transform[2]);

				//rotOff = glm::mat4{
				//	{ 0.0, 1.0, 0.0, 0.0 },
				//	{ 0.0, 0.0, 1.0, 0.0 },
				//	{ 1.0, 0.0, 0.0, 0.0 },
				//	{ 0.0, 0.0, 0.0, 1.0 }
				//};
				push.transformMatrix = rotationTransform;
				push.transformMatrix[3] = tfc.transformMatrix[3];
					
				vkCmdPushConstants(frameInfo.commandBuffer, pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(ColliderVisualizerPush), &push);
				vkCmdDraw(frameInfo.commandBuffer, ph.meshesInFlight[0]->getInstanceCount(), 1, 0, 0);
			} else {
				switch (push.type) {
				case(PhysicsColliderType::Sphere):
					push.transformMatrix = glm::scale(glm::mat4(1.0f), glm::vec3(ph.asset.sphereRadius)); 
					push.transformMatrix = rotationTransform * push.transformMatrix;
					push.transformMatrix[3] = tfc.transformMatrix[3];
					vkCmdPushConstants(frameInfo.commandBuffer, pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(ColliderVisualizerPush), &push);
					frameInfo.resourceSystem->retrieveMesh(ResourceSystem::coreAssets.m_sphere)->drawAll(frameInfo.commandBuffer);
					break;
				case(PhysicsColliderType::Box):
					push.transformMatrix = glm::scale(glm::mat4(1.0f), ph.asset.boxExtent);
					push.transformMatrix = rotationTransform * push.transformMatrix;
					push.transformMatrix[3] = tfc.transformMatrix[3];
					vkCmdPushConstants(frameInfo.commandBuffer, pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(ColliderVisualizerPush), &push);
					frameInfo.resourceSystem->retrieveMesh(ResourceSystem::coreAssets.m_defaultModel)->drawAll(frameInfo.commandBuffer);
					break;
				case(PhysicsColliderType::Capsule):
					push.transformMatrix = glm::scale(rotationTransform, glm::vec3(ph.asset.cylindricalRadius, ph.asset.cylindricalRadius, ph.asset.cylindricalRadius));
					push.transformMatrix[3].y += ph.asset.cylindricalLength / 2.0f - ph.asset.cylindricalRadius;
					vkCmdPushConstants(frameInfo.commandBuffer, pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(ColliderVisualizerPush), &push);
					frameInfo.resourceSystem->retrieveMesh(ResourceSystem::coreAssets.m_sphere)->drawAll(frameInfo.commandBuffer);

					push.transformMatrix = glm::scale(rotationTransform, glm::vec3(ph.asset.cylindricalRadius, ph.asset.cylindricalRadius, ph.asset.cylindricalRadius));
					push.transformMatrix[3].y -= ph.asset.cylindricalLength / 2.0f - ph.asset.cylindricalRadius; 
					vkCmdPushConstants(frameInfo.commandBuffer, pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(ColliderVisualizerPush), &push);
					frameInfo.resourceSystem->retrieveMesh(ResourceSystem::coreAssets.m_sphere)->drawAll(frameInfo.commandBuffer); 

					push.transformMatrix = glm::scale(rotationTransform, glm::vec3(ph.asset.cylindricalRadius, ph.asset.cylindricalLength / 2.0f - ph.asset.cylindricalRadius, ph.asset.cylindricalRadius));
					push.transformMatrix[3] = tfc.transformMatrix[3];
					vkCmdPushConstants(frameInfo.commandBuffer, pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(ColliderVisualizerPush), &push);
					frameInfo.resourceSystem->retrieveMesh(ResourceSystem::coreAssets.m_cylinder)->drawAll(frameInfo.commandBuffer);
					break;
				case(PhysicsColliderType::Cylinder):
					push.transformMatrix = rotationTransform * 
							glm::scale(glm::mat4(1.0f), glm::vec3(ph.asset.cylindricalRadius, ph.asset.cylindricalLength / 2.0f, ph.asset.cylindricalRadius));
		
					push.transformMatrix[3] = tfc.transformMatrix[3]; 
					vkCmdPushConstants(frameInfo.commandBuffer, pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(ColliderVisualizerPush), &push);
					frameInfo.resourceSystem->retrieveMesh(ResourceSystem::coreAssets.m_cylinder)->drawAll(frameInfo.commandBuffer);
					break;
				case(PhysicsColliderType::MeshAsset):
					push.transformMatrix = rotationTransform;
					push.transformMatrix[3] = tfc.transformMatrix[3];

					vkCmdPushConstants(frameInfo.commandBuffer, pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(ColliderVisualizerPush), &push);
					if (ph.asset.trimeshAsset == AssetID::null()) {
						break;
					}
					frameInfo.resourceSystem->loadMesh(ph.asset.trimeshAsset);
					frameInfo.resourceSystem->retrieveMesh(ph.asset.trimeshAsset)->drawAll(frameInfo.commandBuffer);
					break;
				}
				
			}
		}

		for (auto actor : eraseActors) {
			physicsActorVertexVisualizationBuffers.erase(actor);
		}
		if (terrainMeshID) {
			ColliderVisualizerPush push{};
			push.type = PhysicsColliderType::Terrain;
			push.color = { 0.8f, 0.8f, 0.3f, 0.8f };
			graphicsPipelineTerrain->bind(frameInfo.commandBuffer);
			push.transformMatrix = glm::mat4(1.0f);
			vkCmdPushConstants(frameInfo.commandBuffer, pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(ColliderVisualizerPush), &push);
			auto offsetData = frameInfo.resourceSystem->getVertexAllocator()->getOffsetData(terrainMeshID);
			vkCmdDraw(frameInfo.commandBuffer, offsetData.vertexCount, 1, offsetData.vertexOffset, 0);
		}
	}
	
	void PhysicsActorPreviewRenderer::createPipelineLayout(VkDescriptorSetLayout globalSetLayout) {
		{
			convexHullDataDescriptorLayout = S3DDescriptorSetLayout::Builder(engineDevice).addBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT).build();
			VkPushConstantRange pushConstantRange{};
			pushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
			pushConstantRange.offset = 0;
			pushConstantRange.size = sizeof(ColliderVisualizerPush);

			std::vector<VkDescriptorSetLayout> descriptorSetLayouts{
				globalSetLayout,
				convexHullDataDescriptorLayout->getDescriptorSetLayout()
			};

			VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
			pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
			pipelineLayoutInfo.setLayoutCount = (uint32_t)descriptorSetLayouts.size();
			pipelineLayoutInfo.pSetLayouts = descriptorSetLayouts.data();
			pipelineLayoutInfo.pushConstantRangeCount = 1;
			pipelineLayoutInfo.pPushConstantRanges = &pushConstantRange;
			VK_ASSERT(vkCreatePipelineLayout(engineDevice.device(), &pipelineLayoutInfo, nullptr, &pipelineLayout), "failed to create pipeline layout!");
		}
	}

	void PhysicsActorPreviewRenderer::createPipeline(VkRenderPass translucentRenderPass) {
		{
			S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo(2);
			pipelineConfig.enableWeightedBlending();
			pipelineConfig.lineRasterizer(2.0f);
			pipelineConfig.enableVertexDescriptions(VertexDescriptionOptions_Position);

			if (ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit) {
				pipelineConfig.reverseDepth();
			}
			pipelineConfig.renderPass = translucentRenderPass;
			pipelineConfig.pipelineLayout = pipelineLayout;
			if (ProjectSystem::getGraphicsSettings().renderer.msaa) {
				pipelineConfig.setSampleCount((VkSampleCountFlagBits)ProjectSystem::getGraphicsSettings().renderer.msaaSamples);
			}
			graphicsPipeline = make_uPtr<S3DGraphicsPipeline>(
				engineDevice,
				std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/editor/physics_actor_visualizer.vert", "editor" },
					S3DShader{ ShaderType::Pixel, "resources/shaders/fragment_color.frag", "misc" }
			},
				pipelineConfig
			);
		}
		{
			S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo(2);
			pipelineConfig.enableWeightedBlending();
			pipelineConfig.pointRasterizer();
			pipelineConfig.enableVertexDescriptions(VertexDescriptionOptions_Position);
			pipelineConfig.inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
			if (ProjectSystem::getGraphicsSettings().renderer.msaa) {
				pipelineConfig.setSampleCount((VkSampleCountFlagBits)ProjectSystem::getGraphicsSettings().renderer.msaaSamples);
			}
			if (ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit) {
				pipelineConfig.reverseDepth();
			}
			pipelineConfig.renderPass = translucentRenderPass;
			pipelineConfig.pipelineLayout = pipelineLayout;
			graphicsPipelineTerrain = make_uPtr<S3DGraphicsPipeline>(
				engineDevice,
				std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/editor/physics_actor_visualizer.vert", "editor" },
				S3DShader{ ShaderType::Pixel, "resources/shaders/fragment_color.frag", "misc" }
			},
			pipelineConfig
				);
		}
	}

	sPtr<S3DBuffer> PhysicsActorPreviewRenderer::createConvexActorMesh(PhysicsAsset asset) {
		RPhys::ConvexHull hull = RPhys::buildConvexHull(asset.convexPoints);
		std::vector<glm::vec4> points;
		points.reserve(hull.triangles.size() * 3u);
		for (auto p : hull.triangles) {
			points.push_back(glm::vec4(hull.points[p.a]  * 2.0f, 1));
			points.push_back(glm::vec4(hull.points[p.b]  * 2.0f, 1));
			points.push_back(glm::vec4(hull.points[p.c]  * 2.0f, 1));
		}
		sPtr<S3DBuffer> buffer = make_sPtr<S3DBuffer>(engineDevice, sizeof(glm::vec4), points.size(), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

		buffer->map();
		buffer->writeToBuffer(points.data());
		buffer->flush();
		buffer->unmap();

		return buffer;
	}

	uint32_t PhysicsActorPreviewRenderer::createTerrainActorMesh(TerrainPhysicsData* actor, VertexArenaAllocator* allocator) {
		if (!actor) return 0;
		std::vector<glm::vec3> vertices;

		for (auto& tile : actor->tiles) {
			for (int y = 0; y < tile.verticesY; y++) {
				for (int x = 0; x < tile.verticesX; x++) {
					float px = (static_cast<float>(x) / (tile.verticesX - 1)) * tile.widthX - tile.widthX / 2.0f + tile.offsetX;
					float py = (static_cast<float>(y) / (tile.verticesY - 1)) * tile.widthY - tile.widthY / 2.0f + tile.offsetY;

					int off = y * tile.verticesX + x;
					vertices.push_back({ px, tile.heights[off], py });
				}
			}
		}

		uint32_t meshid = allocator->allocate(vertices.size(), 1);

		VertexDataInfo data;
		data.position = vertices.data();
		data.normal = vertices.data();
		data.tangent = vertices.data();
		data.uv = vertices.data();

		int i = 0;
		allocator->write(meshid, data, &i);
		return meshid;
	}

	void PhysicsActorPreviewRenderer::updateConvexActorMesh(PhysicsActorRenderInfo& actor, uint32_t frameIndex) {
		RPhys::ConvexHull hull = RPhys::buildConvexHull(actor.asset.convexPoints);
		if (hull.triangles.size() * 3 > actor.meshesInFlight[frameIndex]->getInstanceCount()) {
			actor.meshesInFlight[frameIndex] = createConvexActorMesh(actor.asset);
			S3DDescriptorWriter(*convexHullDataDescriptorLayout, *engineDevice.staticMaterialPool)
				.writeBuffer(0, &actor.meshesInFlight[frameIndex]->descriptorInfo())
				.build(actor.setsInFlight[frameIndex]);
		} else {
			std::vector<glm::vec4> points;
			points.reserve(hull.triangles.size() * 3u);
			for (auto p : hull.triangles) {
				points.push_back(glm::vec4(hull.points[p.a], 1));
				points.push_back(glm::vec4(hull.points[p.b], 1));
				points.push_back(glm::vec4(hull.points[p.c], 1));
			}
			actor.meshesInFlight[frameIndex]->map();
			actor.meshesInFlight[frameIndex]->writeToBuffer(points.data());
			actor.meshesInFlight[frameIndex]->flush();
			actor.meshesInFlight[frameIndex]->unmap();
		}
	}
}