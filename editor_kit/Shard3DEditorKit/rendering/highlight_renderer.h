#pragma once

#include "../core.h"
#include <Shard3D/s3dstd.h>
#include <Shard3D/vulkan_abstr.h>
#include <Shard3D/core/ecs/level.h>
#include <Shard3D/core/rendering/framebuffer.h>
#include <Shard3D/core/rendering/render_pass.h>
#include <Shard3D/systems/handlers/material_system.h>
#include <Shard3D/systems/handlers/render_list.h>

namespace Shard3D {
	class HighlightRenderer {
	public:
		S3D_DLLAPI HighlightRenderer(S3DDevice& device, VkRenderPass renderPass, VkDescriptorSetLayout globalSetLayout, uPtr<SmartDescriptorSet>& bindless);
		S3D_DLLAPI ~HighlightRenderer();

		S3D_DLLAPI DELETE_COPY(HighlightRenderer);

		S3D_DLLAPI void renderHighlight(FrameInfo& frameInfo, const std::vector<Actor>& actors);
	private:
		S3D_DLLAPI void createPipelineLayout(VkDescriptorSetLayout globalSetLayout);
		S3D_DLLAPI void createPipeline(VkRenderPass renderPass);
		template <typename T>
		S3D_DLLAPI void renderComponentHighlight(const char* iconName, FrameInfo& frameInfo, Actor actor);

		S3DDevice& engineDevice;
		uPtr<S3DGraphicsPipeline> billboardPipeline;
		S3DPipelineLayout* billboardPipelineLayout;
		S3DPushConstant billboardPush;

		uPtr<S3DGraphicsPipeline> meshPipeline;
		S3DPipelineLayout* meshPipelineLayout;
		S3DPushConstant meshPush;

		uPtr<SmartDescriptorSet>& bindless;
	};
}