#include <glm/gtc/constants.hpp>

#include "point_renderer.h"
#include <Shard3D/core/asset/assetmgr.h>
#include <Shard3D/core/ecs/actor.h>
namespace Shard3D {
	struct PointPush {
		glm::vec4 position;
		glm::vec4 color;
		float size;
		float weight;
	};

	PointRenderer::PointRenderer(S3DDevice& device, VkRenderPass translucentRenderPass, VkDescriptorSetLayout globalSetLayout) : engineDevice{ device } {
		createPipelineLayout(globalSetLayout);
		createPipeline(translucentRenderPass);
	}
	PointRenderer::~PointRenderer() {
		vkDestroyPipelineLayout(engineDevice.device(), pipelineLayout, nullptr);
	}
	void PointRenderer::render(FrameInfo& frameInfo, const std::vector<PointRenderInfo>& points) {
		vkCmdBindDescriptorSets(
			frameInfo.commandBuffer,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			pipelineLayout, 
			0,
			1,
			&frameInfo.globalDescriptorSet, 
			0, 
			nullptr
		);
		graphicsPipeline->bind(frameInfo.commandBuffer);

		for (auto& point : points) {
			PointPush push;
			push.position = { point.position, 1.0f };
			push.color = point.color;
			push.size = point.size;
			push.weight = point.weight;
			vkCmdPushConstants(frameInfo.commandBuffer, pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(PointPush), &push);
			vkCmdDraw(frameInfo.commandBuffer, 1, 1, 0, 0);
		}
	}

	void PointRenderer::createPipelineLayout(VkDescriptorSetLayout globalSetLayout) {
		{
			VkPushConstantRange pushConstantRange{};
			pushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
			pushConstantRange.offset = 0;
			pushConstantRange.size = sizeof(PointPush);

			std::vector<VkDescriptorSetLayout> descriptorSetLayouts{
				globalSetLayout
			};

			VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
			pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
			pipelineLayoutInfo.setLayoutCount = (uint32_t)descriptorSetLayouts.size();
			pipelineLayoutInfo.pSetLayouts = descriptorSetLayouts.data();
			pipelineLayoutInfo.pushConstantRangeCount = 1;
			pipelineLayoutInfo.pPushConstantRanges = &pushConstantRange;
			VK_ASSERT(vkCreatePipelineLayout(engineDevice.device(), &pipelineLayoutInfo, nullptr, &pipelineLayout), "failed to create pipeline layout!");
		}
	}

	void PointRenderer::createPipeline(VkRenderPass translucentRenderPass) {
		{
			S3DGraphicsPipelineConfigInfo pipelineConfig = S3DGraphicsPipelineConfigInfo(2);
			pipelineConfig.enableWeightedBlending();
			pipelineConfig.pointRasterizer();
			pipelineConfig.inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
			pipelineConfig.enableVertexDescriptions(VertexDescriptionOptions_Position);

			if (ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit) {
				pipelineConfig.reverseDepth();
			}
			if (ProjectSystem::getGraphicsSettings().renderer.msaa) {
				pipelineConfig.setSampleCount((VkSampleCountFlagBits)ProjectSystem::getGraphicsSettings().renderer.msaaSamples);
			}
			pipelineConfig.renderPass = translucentRenderPass;
			pipelineConfig.pipelineLayout = pipelineLayout;
			graphicsPipeline = make_uPtr<S3DGraphicsPipeline>(
				engineDevice,
				std::vector<S3DShader>{
				S3DShader{ ShaderType::Vertex, "resources/shaders/editor/point.vert", "editor" },
					S3DShader{ ShaderType::Pixel, "resources/shaders/fragment_color.frag", "misc" }
			},
				pipelineConfig
					);
		}
	}
}