#pragma once

#include "../core.h"
#include <Shard3D/s3dstd.h>
#include <Shard3D/vulkan_abstr.h>

namespace Shard3D {
	class GridSystem {
	public:
		S3D_DLLAPI GridSystem(S3DDevice& device, VkRenderPass renderPass, VkDescriptorSetLayout globalSetLayout);
		S3D_DLLAPI ~GridSystem();

		S3D_DLLAPI GridSystem(const GridSystem&) = delete;
		S3D_DLLAPI GridSystem& operator=(const GridSystem&) = delete;

		S3D_DLLAPI void render(FrameInfo& frameInfo);
	private:
		S3D_DLLAPI void createPipelineLayout(VkDescriptorSetLayout globalSetLayout);
		S3D_DLLAPI void createPipeline(VkRenderPass renderPass);

		S3DDevice& engineDevice;

		uPtr<S3DGraphicsPipeline> graphicsPipeline;
		VkPipelineLayout pipelineLayout;
	};
}