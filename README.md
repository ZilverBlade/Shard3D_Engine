![](https://cdn.discordapp.com/attachments/763044823342776340/1002705748121354302/unknown.png)  

# DISCLAIMER!

### This is an extremely tech debt filled engine as this stems all the way back to when I first started C++/Game Engine/Graphics/Vulkan programming, which means the codebase can be extremely frustrating to navigate through.
For that matter, this engine will no longer be maintained as open source as this is mainly an in house engine.
Current engine may be usable for demos however its not recommended to use for any final products as numerous issues still need to be addressed.


# Shard3D

Shard3D Engine, a 2000s styled engine running on the Vulkan API (for Windows)

The goal is to make a full on editor based engine with 2000s styled graphics with modern technology, with tons of customisation.
Read more on the website: https://www.shard3d.com

### Requirements

Requires at least version **1.2** of Vulkan SDK: https://vulkan.lunarg.com/sdk/home#windows
Make sure to install GLM, and that the vulkan environment variable states VULKAN_SDK.

**Use premake_build.bat to generate your project files** 

### Contributors

- [@ZilverBlade](https://www.github.com/ZilverBlade)
- [@DhiBaid](https://www.github.com/DHIBAID) (editor icons)


### Graphics Examples
Graphics examples can be seen in the screenshots directory.


### Editor Camera Controls

- Look Around = Right MB
- Move Left = A
- Move Right = D
- Move Forward = W
- Move Backward = S
- Move Up = SPACE
- Move Down = RIGHT CONTROL
- Slow Down Movement = LEFT CONTROL
- Speed Up Movement = LEFT SHIFT
- Change Movement Speed = SCROLL
- Change FOV = ALT + SCROLL

# Shard3D Feature Map
### Rendering
- [x] Deferred rendering
- [x] MSAA (EXPERIMENTAL!)
- [x] Point, Spot, Directional Lights
- [x] Dynamic Spot, Directional Shadows
- [x] Translucent Shadows
- [x] Cascaded Directional Shadows
- [x] Point Light Shadows
- [ ] Partial Baked Shadows
- [x] Skybox
- [x] Baked reflection probes
- [x] Planar reflections
- [ ] Dynamic reflection probes (likely paraboloid type)
- [x] Dynamic sky support (Only atmoshphere, no clouds yet)
- [x] SSAO, and AOV (ambient occlusion volumes)
- [x] OIT (Weighted blending)
- [x] Global Illumination (Light propagation volumes, WIP)
- [x] Deferred Decals
- [ ] Particle system
- [ ] Video Playback (CPU playback due to compatibility reasons)

### Post processing
- [x] Tonemapping
- [x] Colour Correction
- [x] Bloom (Require overhaul)
- [x] FXAA
- [ ] MLAA (SMAA)
- [ ] Super Sampling (SSAA)
- [ ] Lens flare
- [x] Motion Blur

### HUD
- [x] Interactive Elements
- [x] Text
- [x] Formattable Text
- [ ] Localizable Text

### Functionality
- [x] Editor Object picking
- [x] Prefabs (Note: may be unstable and might not support prefabs inside prefabs. Tools for editing prefabs are currently limited, only way to create prefabs are through converting actors or renaming level files)
- [x] C# Scripting
- [x] C++ Scripting
- [x] Asset Reference Tracker
- [ ] Multiple camera support
- [x] 3D Physics (WIP: implementing Open Dynamics Engine)
- [x] Rigging skeletons (WIP)
- [ ] Morph Targets (WIP)
- [x] Animations

### Miscellaneous
- [x] Custom mesh format
- [x] KTX format
- [x] BCC texture support (BC1, BC3, BC4, BC5, BC7)
- [ ] Game Packager

# Known issues
### Performance
- 1 point light shadow face is slower to render than 1 spot light shadow?

### Usability
- Editor camera not working when outside of main window
- Actor parenting not working when to parent actor is above the future child actor
- Actor properties blowing up when actor with children is collapsed

### Bugs
- SIMJSON crashing editor when a file error occurs?
- Dropdown viewer for assets buggy
- Nasty crash in certain scenarios due to bad sync (only may occur if window has been resized)

### Missing features
- Unloading assets from GPU memory (was removed due to being unreliable)
- Moving assets properly
- Reimporting assets
- Texture cube config viewer
- Mesh config viewer
- Prefab editor
- Bring back billboards
- Estimated level intensity indicator (cost of models, shadows, actors, lights, scripts, physics)

### Misc problems
- Baked assets getting "lost" when their respective actors are removed
- Asynchronously cache textures, to avoid long waiting times

# TODO:
- Proper drop down viewer + input text for assets
- Fix baked assets losing their assets
- Fix GPU memory allocator unloading system (as well as improve the seeking algorithm)
- Nasty device lost bug due to bad sync (happens when framerate is too high for the gpu or cpu??)
- Fix volumetric lighting shadow calcs for point and directional lights, as well as add volumetric transparency for directional and spot lights
- Fix velocity buffer tile max and neighbour max generation being incorrect
- Add LUT, more colour correction effects (low, mid, high tone corrections)

# 0.13 TODO:
- Time of day script for level (not necessary)
- Timeline actor (not in scope)
- Gamepad/controller input (done)
- Skeletal meshes (done)
- Script variables, script engine reloading, and other script engine stuff that's missing (not in scope)
- Switch levels through script (done)
- Physics (top priority) (done)
- HUD (use SpearHUD) (done)
- Advanced post processing (e.g. lens flare, dynamic exposure, LUT?) (not in scope)
- Change graphics settings in editor/game
- Overhaul colour correction effect (sufficient)
- Add prefab, mesh, texture cube viewer/editors (not in scope)
- Documentation? (not necessary, in house engine)
- Deferred decals (done)

# 0.14 TODO:
- Multiplayer (support steamworks SDK?)
- Water (lakes, rivers, puddles)
- Billboards
- Multiple camera support
- Video decoder? (ffmpeg)
- Dynamic sky (& clouds?) (PRIORITY)
- Dynamic reflections
- Mesh LODs
- Particle system
- Frustum culling (with BVH) (PRIORITY)
- Misc culling (e.g. by distance, tag groups, should be able to be applied to cameras, reflections and shadows)
- Light culling (PRIORITY)

# 1.0 BETA TODO:
- Occlusion culling (HiZ?)
- Foliage & Grass
- Advanced shading models (car paint, refractive glass, double sided foliage)
- SMAA, SSAA & FXAA 3.11 investigation (maybe defered MSAA? resolve gbuffer attachments?)
- Shadow map baking (Partial for stationary, and Semi static Moment (R16G16B16A16) Shadows for static shadows)
- Shadow map streaming based off light culling
- Reflection probe streaming based off light culling
- Linux support (runtime only)

# 1.1 BETA and beyond:
- Enhanced motion blur
- Enhanced post processing effects
- Tutorial levels
- ????
