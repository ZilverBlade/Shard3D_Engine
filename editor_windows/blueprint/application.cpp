#include "application.h"

#include <thread>
#include <future>
#include <fstream>
#include <vector>
#include <Shard3D/systems/systems.h>
#include <Shard3D/core/asset/assetmgr.h>

#include <Shard3DEditorKit/kit.h>

#include <Shard3D/core/misc/graphics_settings.h>
#include <Shard3D/core/misc/engine_settings.h>

#include "imgui/editor.h"
#include <Shard3D/workarounds.h>

// c++ scripts
#include <Shard3D/global.h>
#include <Shard3D/core/vulkan_api/bindless.h>
#include <Shard3DEditorKit/tools/editor_pref.h>
#include <Shard3D/scripting/script_engine.h>
#include <Shard3D/systems/handlers/system_instantiator.h>
#include <Shard3DEditorKit/rendering/object_picking_system.h>
#include <Shard3DEditorKit/rendering/highlight_renderer.h>
namespace Shard3D {
	Application::Application(AssetID bp) : blueprint(bp) {
		setupEngineFeatures();
		createRenderPasses();
	}
	Application::~Application() {
	
	}

	void Application::createRenderPasses() {
		{ // Post processing
			ppoColorFramebufferAttachment = new FramebufferAttachment(engineDevice, {
				VK_FORMAT_R16G16B16A16_SFLOAT,
				VK_IMAGE_ASPECT_COLOR_BIT,
				{ wndWidth, wndHeight, 1 },
				VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
				VK_SAMPLE_COUNT_1_BIT
				}, FramebufferAttachmentOptions_Color);

			ppoRenderpass = new RenderPass(
				engineDevice, {
					AttachmentInfo{ ppoColorFramebufferAttachment, VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE }
				});

			ppoFramebuffer = new Framebuffer(engineDevice, ppoRenderpass->getRenderPass(), { ppoColorFramebufferAttachment });
		}
		
	}

	void Application::destroyRenderPasses() {
	}

	void Application::setupEngineFeatures() {
		ScriptEngine::init(&engineWindow);

		setWindowCallbacks();
		
		SharedPools::constructPools(engineDevice);
		
		engineDevice.resourceSystem = new ResourceSystem(engineDevice);

		_special_assets::_editor_icons_load(engineDevice, engineDevice.resourceSystem->getDescriptor());
		EditorPreferences::load();

		ShaderSystem::init();
	}

	void Application::setWindowCallbacks() {
		SHARD3D_EVENT_BIND_HANDLER_RFC(engineWindow, Application::eventEvent);
	}

	void Application::windowResizeEvent(Events::WindowResizeEvent& e) {
		createRenderPasses();
	}

	void Application::eventEvent(Events::Event& e) {
	}

	void Application::resizeFramebuffers(uint32_t newWidth, uint32_t newHeight, void* editor, void* ppoSystem, void* gbuffer) {
		//ppoFramebuffer->resize(glm::ivec3(newWidth, newHeight, 1), ppoRenderpass->getRenderPass());

		reinterpret_cast<PostProcessingSystem*>(ppoSystem)->resize(glm::vec2(newWidth, newHeight), *reinterpret_cast<SceneBufferInputData*>(gbuffer));
		ImGuiInitializer::setViewportImage(reinterpret_cast<Editor*>(editor)->viewportImage, reinterpret_cast<PostProcessingSystem*>(ppoSystem)->getFinalImage());
	}

	void Application::run() {
		// Global pool
		uPtr<BoundDescriptorPool> globalPool{};
		globalPool = BoundDescriptorPool::Builder(engineDevice)
			.setMaxSets( ProjectSystem::getGraphicsSettings().renderer.framesInFlight)
			.addPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,  ProjectSystem::getGraphicsSettings().renderer.framesInFlight)
			.build();
		std::vector<uPtr<EngineBuffer>> uboBuffers( ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		for (int i = 0; i < uboBuffers.size(); i++) {
			uboBuffers[i] = make_uPtr<EngineBuffer>(
				engineDevice,
				sizeof(GlobalUbo),
				1,
				VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
				);
			uboBuffers[i]->map();
		}
		auto globalSetLayout = BoundDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_ALL_GRAPHICS | VK_SHADER_STAGE_COMPUTE_BIT)
			.build();
		std::vector<VkDescriptorSet> globalDescriptorSets( ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		for (int i = 0; i < globalDescriptorSets.size(); i++) {
			auto bufferInfo = uboBuffers[i]->descriptorInfo();
			BoundDescriptorWriter(*globalSetLayout, *globalPool)
				.writeBuffer(0, &bufferInfo)
				.build(globalDescriptorSets[i]);
		}


		std::vector<uPtr<EngineBuffer>> sceneSSBO( ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		for (int i = 0; i < sceneSSBO.size(); i++) {
			sceneSSBO[i] = make_uPtr<EngineBuffer>(
				engineDevice,
				sizeof(SceneSSBO),
				1,
				VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
				);
			sceneSSBO[i]->map();
		}
		uPtr<BoundDescriptorSetLayout> sceneSetLayout = BoundDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, VK_SHADER_STAGE_ALL_GRAPHICS)
			.build();
		std::vector<VkDescriptorSet> sceneDescriptorSets( ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		std::vector<uPtr<SceneSSBO>> sceneSSBOStruct( ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		for (int i = 0; i < sceneDescriptorSets.size(); i++) {
			auto bufferInfo = sceneSSBO[i]->descriptorInfo();
			BoundDescriptorWriter(*sceneSetLayout, *SharedPools::staticMaterialPool)
				.writeBuffer(0, &bufferInfo)
				.build(sceneDescriptorSets[i]);
			sceneSSBOStruct[i] = make_uPtr<SceneSSBO>();
		}
		SystemInstancer::init(engineDevice, globalSetLayout->getDescriptorSetLayout(), sceneSetLayout->getDescriptorSetLayout());

		DeferredRenderSystem* deferredRenderSystem = SystemInstancer::createDeferredRenderer({ 1280, 720 });
		SceneBufferInputData gbuffer = deferredRenderSystem->getGBufferAttachments();
		//BillboardRenderSystem billboardRenderSystem{ engineDevice, deferredRenderSystem->getDeferredRenderPass()->getRenderPass(), globalSetLayout->getDescriptorSetLayout() };
		GridSystem gridSystem{ engineDevice, deferredRenderSystem->getTransparencyRenderPass()->getRenderPass(), globalSetLayout->getDescriptorSetLayout() };
		EditorBillboardRenderer editorBillboardRenderer{ engineDevice, deferredRenderSystem->getDeferredRenderPass()->getRenderPass(),  deferredRenderSystem->getTransparencyRenderPass()->getRenderPass(), globalSetLayout->getDescriptorSetLayout() };

		PostProcessingSystem ppoSystem{ engineDevice, VkRenderPass(),  globalSetLayout->getDescriptorSetLayout(), sceneSetLayout->getDescriptorSetLayout(), { 1280, 720 }, gbuffer };

		ShadowMappingSystem* shadowSystem = SystemInstancer::createShadowMapper();


		ObjectPickingSystem pickingSystem{ engineDevice, globalSetLayout->getDescriptorSetLayout(), engineDevice.resourceSystem->getDescriptor(), {1280, 720} };
		//HighlightRenderer highlightRenderer{ engineDevice,  deferredRenderSystem->getTransparencyRenderPass()->getRenderPass(), globalSetLayout->getDescriptorSetLayout(), engineDevice.resourceSystem->getDescriptor() };

		PhysicsSystem physicsSystem{};

		ExponentialFogSystem exponentialFogSystem{};
		LightSystem lightSystem{};
		ReflectionSystem reflectionSystem{};
		SkySystem skySystem{};
		LightingVolumeSystem volumeSystem{};

		engineDevice.resourceSystem->loadCoreAssets();
		engineDevice.resourceSystem->loadTextureCube(AssetID("engine/textures/cubemaps/sky1.s3dasset"));

		// level must be created after resource handler has been initialised due to the fact that the editor camera is created with post processing materials
		SHARD3D_INFO_E("Constructing Level Pointer");
		level = make_sPtr<ECS::Level>(engineDevice.resourceSystem);
		level->createSystem();
		level->setPhysicsSystem(&physicsSystem);
		ImGuiContext* context = ImGui::CreateContext();
		{
			//CSimpleIniA ini;
			//ini.SetUnicode();
			//ini.LoadFile(EDITOR_SETTINGS_PATH);

			ImGuiInitializer::init(engineDevice, engineWindow, context, engineRenderer.getSwapChainRenderPass(), "pref/editor_layout.ini", false);
		}
		Editor editor = { engineDevice, engineWindow, engineRenderer.getSwapChainRenderPass(),level, blueprint, context };

		ImGuiInitializer::setViewportImage(editor.viewportImage, ppoSystem.getFinalImage());

		auto editor_cameraActor = level->getActorFromUUID(0);

		loadStaticObjects();


		auto currentTime = std::chrono::high_resolution_clock::now();
	beginWhileLoop:
		while (!engineWindow.shouldClose()) {
			StatsTimer::dumpIf();
			StatsTimer::clear();
			auto newTime = std::chrono::high_resolution_clock::now();
			float frameTime = std::chrono::duration<float, std::chrono::seconds::period>(newTime - currentTime).count();
			currentTime = newTime;
			SHARD3D_STAT_RECORD();
			glfwPollEvents();
			SHARD3D_STAT_RECORD_END({ "Window", "Polling" });
			double x, y;
			glfwGetCursorPos(engineWindow.getGLFWwindow(), &x, &y);

			glm::vec2 mousePos{ x,y };

			auto possessedCameraActor = level->getPossessedCameraActor();
			auto& possessedCamera = level->getPossessedCamera();

			if (level->simulationState == PlayState::Playing) {
				//	SHARD3D_STAT_RECORD(); 
				//	physicsSystem.simulate(level.get(), frameTime);
				//	SHARD3D_STAT_RECORD_END({ "Level", "Physics" });
				SHARD3D_STAT_RECORD();
				level->tick(frameTime);
				SHARD3D_STAT_RECORD_END({ "Level", "Tick" });
			}
			SHARD3D_STAT_RECORD();
			level->runGarbageCollector();
			engineDevice.resourceSystem->runGarbageCollector();
			//EngineAudio::globalUpdate(possessedCameraActor.getTransform().getTranslation(), possessedCameraActor.getTransform().getRotation());
			SHARD3D_STAT_RECORD_END({ "Engine", "Garbage Collection" });
			possessedCameraActor.getComponent<Components::CameraComponent>().ar = GraphicsSettings2::getRuntimeInfo().aspectRatio;
			possessedCamera.setViewYXZ(possessedCameraActor.getTransform().transformMatrix);
			possessedCameraActor.getComponent<Components::CameraComponent>().setProjection();

			shadowSystem->runGarbageCollector(level); // clean up broken shadows

			while (GlobalFrameState::lockFrame) { glfwPollEvents(); }

			if (auto commandBuffer = engineRenderer.beginFrame()) {
				SHARD3D_ASSERT(!GlobalFrameState::lockFrame && "Frame must be unlocked before can start!");
				GlobalFrameState::isFrameInProgress = true;
				int frameIndex = engineRenderer.getFrameIndex();
				SharedPools::drawPools[frameIndex]->resetPool();
				FrameInfo frameInfo{
					frameIndex,
					frameTime,
					commandBuffer,
					possessedCamera,
					&engineWindow,
					globalDescriptorSets[frameIndex],
					sceneDescriptorSets[frameIndex],
					*SharedPools::drawPools[frameIndex],
					level,
					engineDevice.resourceSystem
				};
				//	render
				SHARD3D_STAT_RECORD();
				shadowSystem->render(frameInfo);
				SHARD3D_STAT_RECORD_END({ "Rendering", "Shadow Mapping" });

				//SHARD3D_STAT_RECORD();
				//vmaoSystem.render(frameInfo);
				//SHARD3D_STAT_RECORD_END({ "Rendering", "Shadow Mapping" });


				SHARD3D_STAT_RECORD();
				//	update
				GlobalUbo ubo{};
				ubo.projection = possessedCamera.getProjection();
				ubo.inverseProjection = possessedCamera.getInverseProjection();
				ubo.view = possessedCamera.getView();
				ubo.inverseView = possessedCamera.getInverseView();
				ubo.screenSize = { engineWindow.getWindowWidth(), engineWindow.getWindowHeight() };
				uboBuffers[frameIndex]->writeToBuffer(&ubo);
				uboBuffers[frameIndex]->flush();
				lightSystem.update(frameInfo, sceneSSBOStruct[frameIndex].get());
				reflectionSystem.update(frameInfo, sceneSSBOStruct[frameIndex].get());
				volumeSystem.update(frameInfo, sceneSSBOStruct[frameIndex].get());
				skySystem.update(frameInfo, sceneSSBOStruct[frameIndex].get());
				exponentialFogSystem.update(frameInfo, sceneSSBOStruct[frameIndex].get());
				sceneSSBO[frameIndex]->writeToBuffer(sceneSSBOStruct[frameIndex].get());
				sceneSSBO[frameIndex]->flush();
				SHARD3D_STAT_RECORD_END({ "UBO", "Update" });

				pickingSystem.renderObjects(frameInfo, mousePos);
				deferredRenderSystem->beginDeferredPass(frameInfo);
				SHARD3D_STAT_RECORD();
				deferredRenderSystem->renderDeferred(frameInfo);
				SHARD3D_STAT_RECORD_END({ "Deferred Pass", "Geometry, Lighting" });
				if (GraphicsSettings2::editorPreview.ONLY_GAME == false) {
					if (GraphicsSettings2::editorPreview.V_EDITOR_BILLBOARDS == true)
						editorBillboardRenderer.renderBillboards(frameInfo); // editor billboards
				}
				deferredRenderSystem->beginBlendingPass(frameInfo);
				//billboardRenderSystem.render(frameInfo);
				SHARD3D_STAT_RECORD();
				deferredRenderSystem->renderForward(frameInfo);
				SHARD3D_STAT_RECORD_END({ "Forward Pass", "Transparency" });

				SHARD3D_STAT_RECORD();
				if (GraphicsSettings2::editorPreview.ONLY_GAME == false) {
					if (GraphicsSettings2::editorPreview.V_GRID == true)
						gridSystem.render(frameInfo);
					if (GraphicsSettings2::editorPreview.V_EDITOR_BILLBOARDS == true)
						editorBillboardRenderer.renderBoxVolumes(frameInfo);
					if (GraphicsSettings2::editorPreview.V_CULLVOLUMES == true)
						editorBillboardRenderer.visualiseMeshVolumes(frameInfo); // visualise culling volumes
				}

				SHARD3D_STAT_RECORD_END({ "Forward Pass", "Editor" });
				//if (Actor activeObj = Actor((entt::entity)pickingSystem.getActiveObject(), level.get()); activeObj.isInvalid() == false) 
				//	highlightRenderer.renderHighlight(frameInfo, { activeObj });

				deferredRenderSystem->beginCompositionPass(frameInfo);
				SHARD3D_STAT_RECORD();
				deferredRenderSystem->renderSkybox(frameInfo);
				SHARD3D_STAT_RECORD_END({ "Forward Pass", "Skybox" });
				deferredRenderSystem->endComposition(frameInfo);

				SHARD3D_STAT_RECORD();
				ppoSystem.renderCore(frameInfo);
				SHARD3D_STAT_RECORD_END({ "Post Processing", "Main" });

				editor.render(frameInfo);
				//editor.setHoveredObject(pickingSystem.getActiveObject(), level.get());
				//editor.setMouseHoveringCoord(pickingSystem.getWorldPosition());
				engineRenderer.beginSwapChainRenderPass(commandBuffer);
				editor.draw(frameInfo);
				SHARD3D_STAT_RECORD();
				engineRenderer.endSwapChainRenderPass(commandBuffer);
				SHARD3D_STAT_RECORD_END({ "Swapchain", "End" });

				// Command buffer ends
				SHARD3D_STAT_RECORD();
				engineRenderer.endFrame(newTime);
				SHARD3D_STAT_RECORD_END({ "Command Buffer", "Submit" });

				SHARD3D_STAT_RECORD();
				if (engineWindow.wasWindowResized()) {
					glm::ivec3 newsize = { engineWindow.getExtent().width, engineWindow.getExtent().height, 1 };
					vkDeviceWaitIdle(engineDevice.device()); // wait for all in flight frames to finish
					deferredRenderSystem->resize(newsize);
					resizeFramebuffers(newsize.x, newsize.y, &editor, &ppoSystem, &gbuffer);
					pickingSystem.resize(newsize);
					engineWindow.resetWindowResizedFlag();
				}
				SHARD3D_STAT_RECORD_END({ "Window", "Resize check & FBuffer" });
				GlobalFrameState::isFrameInProgress = false;
			}
		}

		if (MessageDialogs::show("Any unsaved changes will be lost! Are you sure you want to exit?",
			"Shard3D Torque", MessageDialogs::OPTYESNO | MessageDialogs::OPTDEFBUTTON2 | MessageDialogs::OPTICONEXCLAMATION) == MessageDialogs::RESNO) {
			glfwSetWindowShouldClose(engineWindow.getGLFWwindow(), GLFW_FALSE);
			goto beginWhileLoop;
		}

		if (level->simulationState != PlayState::Stopped) level->end();
		vkDeviceWaitIdle(engineDevice.device());

		editor.detach();

		ShaderSystem::destroy();
		ScriptEngine::destroy();
		SystemInstancer::destroy(deferredRenderSystem);
		SystemInstancer::destroy(shadowSystem);
		_special_assets::_editor_icons_destroy();

		destroyRenderPasses();
		SharedPools::destructPools();

		delete engineDevice.resourceSystem;
		level = nullptr;
	}

	void Application::loadStaticObjects() {
		LevelManager levelMan(level, engineDevice.resourceSystem);
		levelMan.load("assets/engine/levels/blueprint_builder_level.s3dlevel", true);
		auto editor_cameraActor = level->getActorFromUUID(0);
		editor_cameraActor.getTransform().setTranslation({ 0.f, -2.f, 1.f });
	}
}