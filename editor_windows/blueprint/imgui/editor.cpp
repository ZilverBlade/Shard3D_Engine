#include "editor.h"

#include <imgui.h>

#include <imgui_internal.h>

#include <miniaudio.h>

#include <Shard3D/ecs.h>
#include <Shard3D/core/misc/graphics_settings.h>
#include <Shard3D/core/asset/assetmgr.h>
#include <Shard3D/core/ui/hud_layer.h>
#include <Shard3D/core/audio/audio.h>
#include <Shard3D/workarounds.h>
#include <Shard3D/utils/dialogs.h>

#include <shellapi.h>
#include <glm/gtc/type_ptr.hpp>

#include <ImGuizmo.h>
#include <Shard3D/systems/handlers/material_system.h>
#include <Shard3D/core/ecs/levelmgr.h>
#include <Shard3D/systems/handlers/render_list.h>
#include <Shard3D/systems/handlers/material_system.h>

namespace Shard3D {
	Editor::Editor(EngineDevice& dvc, EngineWindow& wnd, VkRenderPass renderPass, sPtr<Level>& level, AssetID bp, ImGuiContext* ctx) : engineDevice(dvc), engineWindow(wnd), context(ctx), blueprint(make_sPtr<Blueprint>(bp, engineDevice.resourceSystem)){
		std::string title = "Shard3D Blueprint Editor";
		glfwSetWindowTitle(engineWindow.getGLFWwindow(), title.c_str());

		createIcons();

		AssetReferenceTracker::initialize();
		bpActor = level->instBlueprint(blueprint.get());
	}

	Editor::~Editor() {
		AssetReferenceTracker::destruct();
	}

	void Editor::createIcons() {
		{
			auto& img = _special_assets::_get().at("editor.play");
			icons.play = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("editor.pause");
			icons.pause = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("editor.stop");
			icons.stop = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("editor.pref");
			icons.pref = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);       
		} {
			auto& img = _special_assets::_get().at("editor.save");
			icons.save = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("editor.preview");
			icons.preview = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);       
		} {
			auto& img = _special_assets::_get().at("editor.layout");
			icons.layout = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("editor.load");
			icons.load = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("editor.level");
			icons.level = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("editor.viewport");
			icons.viewport = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("editor.settings");
			icons.settings = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		} {
			auto& img = _special_assets::_get().at("editor.launch");
			icons.launchgame = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
	
	}

	void Editor::detach() {
		vkDeviceWaitIdle(engineDevice.device());
		vkDestroyDescriptorPool(engineDevice.device(), ImGuiInitializer::imGuiDescriptorPool, nullptr);
		ImGui_ImplVulkan_Shutdown();
		ImGui_ImplGlfw_Shutdown();
		ImGui::DestroyContext();
	}

	void Editor::render(FrameInfo& frameInfo) {
		assetExplorerPanel.setContext(frameInfo.activeLevel, frameInfo.resourceSystem);
		levelPropertiesPanel.setContext(frameInfo.activeLevel, frameInfo.resourceSystem);
		bpTreePanel.setContext(frameInfo.activeLevel, blueprint.get(), frameInfo.resourceSystem);
		levelPropertiesPanel.allowRenderBlueprint = false;
		SHARD3D_EVENT_BIND_HANDLER_RFC(engineWindow, Editor::eventEvent);

		ImGui::SetCurrentContext(context);

		ImGuiIO& io = ImGui::GetIO();
		io.DeltaTime = frameInfo.frameTime;

		glfwGetWindowSize(engineWindow.getGLFWwindow(), &width, &height);
		io.DisplaySize = ImVec2(width, height);

		//ImGui_ImplVulkan_NewFrame();
		//ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		//ax::NodeEditor::SetCurrentEditor(nodeEditorContext);
		static bool visible = true;

		//ImGui::ShowDemoWindow(&visible);
		
#pragma region boilerplate dockspace code    
		static bool opt_fullscreen = true;
		static bool opt_padding = true;
		ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.f, 0.f, 0.f, 0.f));
		static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;
		// We are using the ImGuiWindowFlags_NoDocking flag to make the parent window not dockable into,
		// because it would be confusing to have two docking targets within each others.
		ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
		if (opt_fullscreen) {
			const ImGuiViewport* viewport = ImGui::GetMainViewport();
			ImGui::SetNextWindowPos(viewport->WorkPos);
			ImGui::SetNextWindowSize(viewport->WorkSize);
			ImGui::SetNextWindowViewport(viewport->ID);
			ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
			ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
			window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
			window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
		}
		else { dockspace_flags &= ~ImGuiDockNodeFlags_PassthruCentralNode; }
		if (dockspace_flags & ImGuiDockNodeFlags_PassthruCentralNode)
			window_flags |= ImGuiWindowFlags_NoBackground;
		if (!opt_padding)
			ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
		ImGui::Begin("MyDockSpace", &visible, window_flags);
		if (!opt_padding)
			ImGui::PopStyleVar();
		if (opt_fullscreen)
			ImGui::PopStyleVar(2);
#pragma endregion
		// Submit the DockSpace
		if (io.ConfigFlags & ImGuiConfigFlags_DockingEnable) {
			ImGuiID dockspace_id = ImGui::GetID("MyDockSpace");
			ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);
		}

		if (ImGui::BeginMenuBar()) {
			renderMenuBar(frameInfo);
			ImGui::EndMenuBar();
		}
		ImGui::PopStyleColor();
		renderQuickBar(frameInfo);
		renderViewport(frameInfo);

		// start rendering stuff here
		assetExplorerPanel.render();
		bpTreePanel.render(bpActor);
		levelPropertiesPanel.render(&bpTreePanel.getSelectedActor());
		componentActorPanels.render();

		ImGui::End();
		ImGui::Render();
	}

	void Editor::draw(FrameInfo& frameInfo) {
		ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(), frameInfo.commandBuffer);
		ImGui::UpdatePlatformWindows();
		ImGui::RenderPlatformWindowsDefault();
	}

	void Editor::renderViewport(FrameInfo& frameInfo) {
		ImGui::Begin("Viewport");
		ImVec2 vSize = ImGui::GetContentRegionAvail();
		GraphicsSettings2::getRuntimeInfo().aspectRatio = vSize.x / vSize.y;
		GraphicsSettings2::getRuntimeInfo().localScreen = { ImGui::GetWindowPos().x, ImGui::GetWindowPos().y, ImGui::GetWindowSize().x ,ImGui::GetWindowSize().y};
		
		ImGui::Image(viewportImage, vSize);
		if (ImGui::IsWindowHovered()) {
			isViewportHovered = true;
			Actor cameraActor = frameInfo.activeLevel->getActorFromUUID(0);
			editorCameraController.tryPollOrientation(engineWindow, frameInfo.frameTime, cameraActor);
			if (ImGui::IsMouseClicked(ImGuiMouseButton_Right)) ImGui::FocusWindow(ImGui::GetCurrentWindow());
		} else isViewportHovered = false;
		if (ImGui::IsWindowFocused()) {
			Actor cameraActor = frameInfo.activeLevel->getActorFromUUID(0);
			editorCameraController.tryPollTranslation(engineWindow, frameInfo.frameTime, cameraActor);
		}
		if (ImGui::BeginDragDropTarget()) {
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SHARD3D.ASSEXP.BLUP")) {
				if (MessageDialogs::show("This will overwrite the current blueprint, and unsaved changes will be lost! Are you sure you want to continue?", "WARNING!", MessageDialogs::OPTYESNO | MessageDialogs::OPTICONEXCLAMATION | MessageDialogs::OPTDEFBUTTON2) == MessageDialogs::RESYES) {
					blueprint = make_sPtr<Blueprint>(AssetID((char*)payload->Data), frameInfo.resourceSystem);
					bpTreePanel.clearSelectedActor();
					frameInfo.activeLevel->killActor(bpActor);
					bpActor = frameInfo.activeLevel->instBlueprint(blueprint.get());
				}
			}
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SHARD3D.COMPONENTS.DROP")) {
				size_t h_code = *reinterpret_cast<size_t*>(payload->Data);
				if (h_code == typeid(Components::PointLightComponent).hash_code()) {
					Actor actor = frameInfo.activeLevel->createActor("Point Light");
					actor.addComponent<Components::PointLightComponent>();
					actor.makeStationary();
					frameInfo.activeLevel->parentActor(actor, bpActor);
				}
				else if (h_code == typeid(Components::SpotLightComponent).hash_code()) {
					Actor actor = frameInfo.activeLevel->createActor("Spot Light");
					actor.addComponent<Components::SpotLightComponent>();
					actor.makeStationary();
					frameInfo.activeLevel->parentActor(actor, bpActor);
				}
				else if (h_code == typeid(Components::BoxReflectionCaptureComponent).hash_code()) {
					Actor actor = frameInfo.activeLevel->createActor("Reflection Capture");
					actor.addComponent<Components::BoxReflectionCaptureComponent>();
					frameInfo.activeLevel->parentActor(actor, bpActor);
				}
				else if (h_code == typeid(Components::BoxAmbientOcclusionVolumeComponent).hash_code()) {
					Actor actor = frameInfo.activeLevel->createActor("Ambient Occlusion Volume");
					actor.addComponent<Components::BoxAmbientOcclusionVolumeComponent>();
					frameInfo.activeLevel->parentActor(actor, bpActor);
				}
				else if (h_code == 32325235) {
					Actor actor = frameInfo.activeLevel->createActor("Cube");
					actor.addComponent<Components::Mesh3DComponent>(frameInfo.resourceSystem, ResourceSystem::coreAssets.m_defaultModel);
					frameInfo.activeLevel->parentActor(actor, bpActor);
				}
				else if (h_code == typeid(Components::CameraComponent).hash_code()) {
					Actor actor = frameInfo.activeLevel->createActor("Camera Actor");
					actor.addComponent<Components::Mesh3DComponent>(frameInfo.resourceSystem, AssetID("engine/meshes/camcorder" ENGINE_ASSET_SUFFIX));
					frameInfo.resourceSystem->loadMesh(AssetID("engine/meshes/camcorder" ENGINE_ASSET_SUFFIX));
					actor.addComponent<Components::CameraComponent>();
					actor.getComponent<Components::VisibilityComponent>().editorOnly = true;
					frameInfo.activeLevel->parentActor(actor, bpActor);
				}
			}
		}

		{
			Actor actor = bpTreePanel.getSelectedActor();
			if (actor) {
				ImGuizmo::SetOrthographic(false);
				ImGuizmo::SetDrawlist();
				ImGuizmo::SetRect(ImGui::GetWindowPos().x, ImGui::GetWindowPos().y, ImGui::GetWindowWidth(), ImGui::GetWindowHeight());
				Actor cameraActor = frameInfo.activeLevel->getPossessedCameraActor();
				EngineCamera& camera = cameraActor.getComponent<Components::CameraComponent>().camera;
				glm::mat4 proj = camera.getProjection();
				glm::mat4 invView = camera.getView();

				auto& tc = actor.getComponent<Components::TransformComponent>();
				glm::mat4 actorTransform = tc.transformMatrix;
				bool hasParent = actor.hasRelationship() && actor.getComponent<Components::RelationshipComponent>().parentActor != entt::null;
				if (hasParent) {
					glm::mat4 parentTransform = Actor(actor.getComponent<Components::RelationshipComponent>().parentActor, frameInfo.activeLevel.get()).getTransform().transformMatrix;
					glm::vec3 parentScale{
						 glm::length(parentTransform[0]),
						 glm::length(parentTransform[1]),
						 glm::length(parentTransform[2])
					};

					actorTransform = glm::mat4(
						{ glm::vec3(actorTransform[0]) / parentScale.x, 0.f },
						{ glm::vec3(actorTransform[1]) / parentScale.y, 0.f },
						{ glm::vec3(actorTransform[2]) / parentScale.z, 0.f },
						actorTransform[3]
					);
				}
				ImGuizmo::Manipulate(glm::value_ptr(invView), glm::value_ptr(proj), (ImGuizmo::OPERATION)gizmoType, ImGuizmo::WORLD, glm::value_ptr(actorTransform));
				if (ImGuizmo::IsUsing() && gizmoType != -1) {

					if (hasParent) {
						glm::mat4 parentTransform = Actor(actor.getComponent<Components::RelationshipComponent>().parentActor, frameInfo.activeLevel.get()).getTransform().transformMatrix;
						glm::vec3 parentScale{
						 glm::length(parentTransform[0]),
						 glm::length(parentTransform[1]),
						 glm::length(parentTransform[2])
						};

						glm::mat4 noScaleParentTransform = glm::mat4(
							{ glm::vec3(parentTransform[0]) / parentScale.x, 0.f },
							{ glm::vec3(parentTransform[1]) / parentScale.y, 0.f },
							{ glm::vec3(parentTransform[2]) / parentScale.z, 0.f },
							parentTransform[3]
						);
						//actorTransform[3] = {
						//	actorTransform[3].x / noScaleParentTransform[0] +
						//	actorTransform[3].y / noScaleParentTransform[1] +
						//	actorTransform[3].z / noScaleParentTransform[2] +
						//	parentTransform[3]
						//};
						actorTransform = glm::inverse(noScaleParentTransform) * actorTransform;
					}
					glm::vec3 transl{};
					glm::vec3 rotat{};
					glm::vec3 scal{};

					Components::TransformComponent::decompose(actorTransform, &transl, &rotat, &scal);
					glm::vec3 realTranslation = glm::vec3(transl.x, transl.z, transl.y);
					glm::vec3 realRotation = glm::vec3(rotat.x, rotat.z, rotat.y);
					glm::vec3 realScale = glm::vec3(scal.x, scal.z, scal.y);

					// not sure if needed
					//glm::vec3 original_rotation = tc.getRotation();
					//glm::vec3 deltaRotation = glm::vec3(realRotation) - original_rotation; 

					tc.setTranslation(glm::vec3(realTranslation));
					tc.setScale(glm::vec3(realScale));
					tc.setRotation(realRotation); // tc.setRotation(deltaRotation)
				}
			}
		}


		ImGui::End();
	}

	void Editor::renderQuickBar(FrameInfo& frameInfo) {

		ImGui::Begin("_editor_quickbar", nullptr, ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoTitleBar);
		ImVec2 btnSize = { 48.f, 48.f };
		float panelWidth = ImGui::GetContentRegionAvail().x;
		int columnCount = std::max((int)(panelWidth / (btnSize.x + 16.f))// <--- thumbnail size (96px) + padding (16px)
			, 1);
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4());
		ImGui::PushStyleVar(ImGuiStyleVar_FrameBorderSize, 0.f);
		ImGui::Columns(columnCount, 0, false);                
		// begin

		// Save/Load
		if (ImGui::ImageButton(icons.save, btnSize)) {
			AssetID asset_ = blueprint->getAsset();
			blueprint = make_sPtr<Blueprint>(bpActor, frameInfo.resourceSystem);
			blueprint->serialize(asset_);
		}
		ImGui::TextWrapped("Save");
		ImGui::NextColumn();
		if (ImGui::ImageButton(icons.load, btnSize)) {
			
		}
		ImGui::TextWrapped("Load");
		ImGui::NextColumn();
		// Editor settings
		if (ImGui::ImageButton(icons.settings, btnSize)) {
		}
		ImGui::TextWrapped("Rebuild");
		ImGui::NextColumn();
		
		// end
		ImGui::Columns(1);
		ImGui::PopStyleVar();
		ImGui::PopStyleColor();
		ImGui::End();
	}

	void Editor::renderMenuBar(FrameInfo& frameInfo) {
		if (ImGui::BeginMenu("File")) {
			ImGui::TextDisabled("SHARD3D 0.1");
			ImGui::Separator();
			ImGui::BeginDisabled(frameInfo.activeLevel->simulationState != PlayState::Stopped);
			if (ImGui::MenuItem("New Blueprint", "Ctrl+N")) {
				SHARD3D_NOIMPL;
			}
			if (ImGui::MenuItem("Load Blueprint...", "Ctrl+O")) {
				SHARD3D_NOIMPL;
			}
			if (ImGui::MenuItem("Save Blueprint...", "Ctrl+S")) {
				SHARD3D_NOIMPL;
			}
			if (ImGui::MenuItem("Save Blueprint As...", "Ctrl+Shift+S")) {
				SHARD3D_NOIMPL;	
			}
			ImGui::Separator();
			ImGui::EndDisabled();

			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("View")) {
			if (ImGui::BeginMenu("Rendering")) {
				ImGui::Separator();
				//static bool yes = true;
				//ImGui::Checkbox("Skybox", &yes);
				//ImGui::Checkbox("Sun", &yes);
				ImGui::EndMenu();
			}
			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Help")) {
#ifdef WIN32
			if (ImGui::MenuItem("Main Website")) { ShellExecuteA(nullptr, "open", "https://www.shard3d.com", nullptr, nullptr, false); }
			if (ImGui::MenuItem("Documentation")) { ShellExecuteA(nullptr, "open", "https://docs.shard3d.com", nullptr, nullptr, false); }
			if (ImGui::MenuItem("SHARD3D")) { ShellExecuteA(nullptr, "open", "https://docs.shard3d.com/SHARD3D.html", nullptr, nullptr, false); }
#endif  
#ifdef __linux__ 
			ImGui::MenuItem("Unsupported");
#endif
			ImGui::EndMenu();
		}
	}


	void Editor::eventEvent(Events::Event& e) {
		SHARD3D_EVENT_CREATE_DISPATCHER(e);
		dispatcher.dispatch<Events::MouseButtonDownEvent>(SHARD3D_EVENT_BIND_VOID(Editor::mouseButtonDownEvent));
		dispatcher.dispatch<Events::MouseButtonReleaseEvent>(SHARD3D_EVENT_BIND_VOID(Editor::mouseButtonReleaseEvent));
		dispatcher.dispatch<Events::MouseHoverEvent>(SHARD3D_EVENT_BIND_VOID(Editor::mouseHoverEvent));
		dispatcher.dispatch<Events::MouseScrollEvent>(SHARD3D_EVENT_BIND_VOID(Editor::mouseScrollEvent));
		dispatcher.dispatch<Events::KeyDownEvent>(SHARD3D_EVENT_BIND_VOID(Editor::keyDownEvent));
		dispatcher.dispatch<Events::KeyReleaseEvent>(SHARD3D_EVENT_BIND_VOID(Editor::keyReleaseEvent));
		dispatcher.dispatch<Events::KeyPressEvent>(SHARD3D_EVENT_BIND_VOID(Editor::keyPressEvent));
	}

	bool Editor::mouseButtonDownEvent(Events::MouseButtonDownEvent& e) {
		ImGuiIO& io = ImGui::GetIO();
		io.MouseDown[e.getButtonCode()] = true;

		return false;
	}

	bool Editor::mouseButtonReleaseEvent(Events::MouseButtonReleaseEvent& e)  {
		ImGuiIO& io = ImGui::GetIO();
		io.MouseDown[e.getButtonCode()] = false;

		return false;
	}

	bool Editor::mouseHoverEvent(Events::MouseHoverEvent& e) {
		ImGuiIO& io = ImGui::GetIO();
		io.MousePos = ImVec2(e.getXPos(), e.getYPos());

		return false;
	}

	bool Editor::mouseScrollEvent(Events::MouseScrollEvent& e) {
		ImGuiIO& io = ImGui::GetIO();
		io.MouseWheel += e.getYOffset();
		io.MouseWheelH += e.getXOffset();

		if (isViewportHovered) {
			editorCameraController.mouseScrollEvent(e);
		}

		return false;
	}
	
	bool Editor::keyDownEvent(Events::KeyDownEvent& e) {
		ImGuiIO& io = ImGui::GetIO();
		int kc = e.getKeyCode();
		io.KeysDown[kc] = true;
		io.KeyShift = io.KeysDown[GLFW_KEY_LEFT_SHIFT] || io.KeysDown[GLFW_KEY_RIGHT_SHIFT];
		io.KeyAlt = io.KeysDown[GLFW_KEY_LEFT_ALT] || io.KeysDown[GLFW_KEY_RIGHT_ALT];
		io.KeyCtrl = io.KeysDown[GLFW_KEY_LEFT_CONTROL] || io.KeysDown[GLFW_KEY_RIGHT_CONTROL];
		io.KeySuper = io.KeysDown[GLFW_KEY_LEFT_SUPER] || io.KeysDown[GLFW_KEY_RIGHT_SUPER];
		return false;
	}

	bool Editor::keyReleaseEvent(Events::KeyReleaseEvent& e)  {
		ImGuiIO& io = ImGui::GetIO();
		io.KeysDown[e.getKeyCode()] = false;
		io.KeyShift = io.KeysDown[GLFW_KEY_LEFT_SHIFT] || io.KeysDown[GLFW_KEY_RIGHT_SHIFT];
		io.KeyAlt = io.KeysDown[GLFW_KEY_LEFT_ALT] || io.KeysDown[GLFW_KEY_RIGHT_ALT];
		io.KeyCtrl = io.KeysDown[GLFW_KEY_LEFT_CONTROL] || io.KeysDown[GLFW_KEY_RIGHT_CONTROL];
		io.KeySuper = io.KeysDown[GLFW_KEY_LEFT_SUPER] || io.KeysDown[GLFW_KEY_RIGHT_SUPER];
		return false;
	}

	bool Editor::keyPressEvent(Events::KeyPressEvent& e)  {
		ImGuiIO& io = ImGui::GetIO();
		io.AddInputCharacter((unsigned short)e.getKeyCode());
		return false;
	}




}