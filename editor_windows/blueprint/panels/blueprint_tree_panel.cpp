#include "blueprint_tree_panel.h"

#include <imgui.h>
#include <Shard3DEditorKit/kit.h>
#include <Shard3D/core/asset/assetmgr.h>
#include <Shard3D/utils/dialogs.h>
#include <Shard3D/systems/handlers/render_list.h>
#include <glm/gtc/type_ptr.inl>
#include <fstream>
namespace Shard3D {
	BlueprintTreePanel::BlueprintTreePanel() {

	}
	BlueprintTreePanel::~BlueprintTreePanel() { context = nullptr; }

	void BlueprintTreePanel::setContext(const sPtr<Level>& levelContext, Blueprint* blueprintContext, ResourceSystem* rs) {
		context = levelContext;
		blueprint = blueprintContext;
		resourceSystem = rs;
	}
	void BlueprintTreePanel::destroyContext() { context = {}; blueprint = {}; }
	void BlueprintTreePanel::clearSelectedActor() { selectedActor = {}; }

	void BlueprintTreePanel::render(Actor bpActor) {
		SHARD3D_ASSERT(context != nullptr && "Context not provided!");
		ImGui::Begin("Blueprint Tree");

		drawActorEntry(bpActor);

		if (ImGui::IsMouseDown(0) && ImGui::IsWindowHovered()) selectedActor = {};

		// blank space RCLICK
		if (ImGui::BeginPopupContextWindow(0, 1, false)) {
			if (ImGui::MenuItem("New Blank Actor")) selectedActor = context->createActor();
			ImGui::EndPopup();
		}
		if (ImGui::BeginPopupContextWindow(0, 1, false)) {
			if (ImGui::MenuItem("New Camera Actor")) {
				auto actor = context->createActor("Camera Actor");
				actor.addComponent<Components::CameraComponent>();
				if (!actor.hasComponent<Components::Mesh3DComponent>()) {
					resourceSystem->loadMesh(AssetID("engine/meshes/camcorder" ENGINE_ASSET_SUFFIX));
					actor.addComponent<Components::Mesh3DComponent>(resourceSystem, AssetID("engine/meshes/camcorder" ENGINE_ASSET_SUFFIX));
					actor.getComponent<Components::VisibilityComponent>().editorOnly = true;
				}
				selectedActor = actor;
			}
			ImGui::EndPopup();
		}
		
		if (ImGui::BeginPopupContextWindow(0, 1, false)) {
			if (ImGui::MenuItem("New Static Mesh")) { auto actor = context->createActor("Cube"); 	context->parentActor(actor, bpActor); actor.addComponent<Components::Mesh3DComponent>(resourceSystem, ResourceSystem::coreAssets.m_defaultModel); }
			ImGui::EndPopup();
		}
		if (ImGui::BeginPopupContextWindow(0, 1, false)) {
			if (ImGui::MenuItem("New Pointlight")) {
				auto actor = context->createActor("Pointlight"); actor.addComponent<Components::PointLightComponent>(); context->parentActor(actor, bpActor); selectedActor = actor; actor.makeStationary();
			}
			ImGui::EndPopup();
		}
		if (ImGui::BeginPopupContextWindow(0, 1, false)) {
			if (ImGui::MenuItem("New Spotlight")) {
				auto actor = context->createActor("Spotlight"); actor.addComponent<Components::SpotLightComponent>();context->parentActor(actor, bpActor); selectedActor = actor; actor.makeStationary();
			}
			ImGui::EndPopup();
		}
		if (ImGui::BeginPopupContextWindow(0, 1, false)) {
			if (ImGui::MenuItem("New Directional Light")) {
				auto actor = context->createActor("Directional Light"); actor.addComponent<Components::DirectionalLightComponent>();context->parentActor(actor, bpActor); selectedActor = actor; actor.makeStationary();
			}
			ImGui::EndPopup();
		}

		ImGui::End();
	}
	void BlueprintTreePanel::drawActorEntry(Actor actor) {
		auto& tag = actor.getTag();
		ImGuiTreeNodeFlags flags = ((selectedActor == actor) ? ImGuiTreeNodeFlags_Selected : 0) | ImGuiTreeNodeFlags_OpenOnArrow;

		bool expanded = ImGui::TreeNodeEx((void*)(uint64_t)(uint32_t)actor, flags, tag.c_str());	

		if (ImGui::IsItemClicked()) selectedActor = actor;
		// remove stuff
		bool actorExists = true;
		if (selectedActor)
			if (ImGui::BeginPopupContextItem()) {
				if (ImGui::MenuItem("Remove Actor")) actorExists = false;
				if (ImGui::MenuItem("Duplicate Actor")) {
					Actor actor = context->duplicateActor(actor);
					actor.getComponent<Components::BlueprintTagComponent>().tag = actor.getTag();
				}

				if (ImGui::MenuItem("Copy UUID to clipboard")) ImGui::SetClipboardText(std::to_string(selectedActor.getUUID()).c_str());
				if (ImGui::MenuItem("Add Child")) {
					Actor child = context->createActor();
					context->parentActor(child, selectedActor);
					child.addComponent<Components::BlueprintTagComponent>().tag = child.getTag() + "x" + std::to_string(child.getUUID());
				}

				ImGui::EndPopup();
			}

		if (expanded) {
			if (actor.hasComponent<Components::RelationshipComponent>())
				for (auto& child : actor.getComponent<Components::RelationshipComponent>().childActors)
					drawActorEntry({ child, context.get() });
			ImGui::TreePop();
		}
		if (!actorExists) {
			context->killActor(actor);
			if (selectedActor == actor) selectedActor = {};
		}
	}
}