#pragma once

#include <Shard3D/ecs.h>
namespace Shard3D {
	class ResourceSystem;
	class BlueprintTreePanel {
	public:
		BlueprintTreePanel();
		~BlueprintTreePanel();

		void setContext(const sPtr<Level>& levelContext, Blueprint* blueprintContext, ResourceSystem* resourceSystem);
		void destroyContext();
		void clearSelectedActor();

		void render(Actor actor);

		inline Actor& getSelectedActor() { return selectedActor; }
		Actor selectedActor;
		Actor hoveredActor;
		Actor draggingActor;
	private:
		void drawActorEntry(Actor actor);
		sPtr<Level> context;
		VkDescriptorSet circleIcon;
		Blueprint* blueprint;
		ResourceSystem* resourceSystem;
	};
}