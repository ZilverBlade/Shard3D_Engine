#include "application.h"
#include <Shard3D/utils/logger.h>
/*
	Shard3D 1.0 (2022) created by ZilverBlade
*/

void initEngineFeatures() {
	Shard3D::LOGGER::init();
}
int main(int argc, char* argv[], char* env[]) {
	initEngineFeatures();
	
	Shard3D::AssetID blueprint{ "test.s3dblueprint" };
	char wkd[256];
	getcwd(wkd, 256);
	Shard3D::AssetID project{ std::string(wkd) + "\\testproject.s3dproj" };
	if (argc == 3) {
		project = Shard3D::AssetID(argv[1]);
		blueprint = Shard3D::AssetID(argv[2]);
	}
	Shard3D::ProjectSystem::init(project.getAsset());
	Shard3D::Application app{ blueprint };
	app.run();
	return EXIT_SUCCESS;
}
