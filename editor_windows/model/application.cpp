#include "application.h"

#include <thread>
#include <future>
#include <fstream>
#include <vector>
#include <Shard3D/systems/systems.h>
#include <Shard3D/core/asset/assetmgr.h>

#include <Shard3DEditorKit/kit.h>

#include <Shard3D/core/misc/graphics_settings.h>
#include <Shard3D/core/misc/engine_settings.h>

#include "imgui/editor.h"
#include <Shard3D/workarounds.h>

// c++ scripts
#include <Shard3D/global.h>
#include <Shard3D/core/vulkan_api/bindless.h>
namespace Shard3D {
	Application::Application(AssetID mdl) : model(mdl) {

		setupEngineFeatures();
		createRenderPasses();

	}
	Application::~Application() {
	
	}

	void Application::createRenderPasses() {
		
		
	}

	void Application::destroyRenderPasses() {
		PTR_DELETE(ppoFramebuffer);
		PTR_DELETE(ppoColorFramebufferAttachment);
		PTR_DELETE(ppoRenderpass);
	}

	void Application::setupEngineFeatures() {
		EngineAudio::init();
		EngineSettings::init();
		GraphicsSettings2::init(nullptr);

		setWindowCallbacks();
		
		SharedPools::constructPools(engineDevice);
		
		engineDevice.resourceSystem = new ResourceSystem(engineDevice);

		_special_assets::_editor_icons_load(engineDevice, engineDevice.resourceSystem->getDescriptor());
		
		GraphicsSettings2::init(&engineWindow);

		ShaderSystem::init();
	}

	void Application::setWindowCallbacks() {
		SHARD3D_EVENT_BIND_HANDLER_RFC(engineWindow, Application::eventEvent);
	}

	void Application::windowResizeEvent(Events::WindowResizeEvent& e) {
		createRenderPasses();
	}

	void Application::eventEvent(Events::Event& e) {
	}


	void Application::resizeFramebuffers(uint32_t newWidth, uint32_t newHeight, void* editor, void* ppoSystem, void* gbuffer) {
		reinterpret_cast<PostProcessingSystem*>(ppoSystem)->resize(glm::vec2(newWidth, newHeight), *reinterpret_cast<SceneBufferInputData*>(gbuffer));
		ImGuiInitializer::setViewportImage(&reinterpret_cast<Editor*>(editor)->viewportImage, reinterpret_cast<PostProcessingSystem*>(ppoSystem)->getFinalImage());
	}

	void Application::run() {
		// Global pool
		uPtr<BoundDescriptorPool> globalPool{};
		globalPool = BoundDescriptorPool::Builder(engineDevice)
			.setMaxSets( ProjectSystem::getGraphicsSettings().renderer.framesInFlight)
			.addPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,  ProjectSystem::getGraphicsSettings().renderer.framesInFlight)
			.build();
		std::vector<uPtr<EngineBuffer>> uboBuffers( ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		for (int i = 0; i < uboBuffers.size(); i++) {
			uboBuffers[i] = make_uPtr<EngineBuffer>(
				engineDevice,
				sizeof(GlobalUbo),
				1,
				VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
				);
			uboBuffers[i]->map();
		}
		auto globalSetLayout = BoundDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_ALL_GRAPHICS)
			.build();
		std::vector<VkDescriptorSet> globalDescriptorSets( ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		for (int i = 0; i < globalDescriptorSets.size(); i++) {
			auto bufferInfo = uboBuffers[i]->descriptorInfo();
			BoundDescriptorWriter(*globalSetLayout, *globalPool)
				.writeBuffer(0, &bufferInfo)
				.build(globalDescriptorSets[i]);
		}


		std::vector<uPtr<EngineBuffer>> sceneSSBO( ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		for (int i = 0; i < sceneSSBO.size(); i++) {
			sceneSSBO[i] = make_uPtr<EngineBuffer>(
				engineDevice,
				sizeof(SceneSSBO),
				1,
				VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
				);
			sceneSSBO[i]->map();
		}
		uPtr<BoundDescriptorSetLayout> sceneSetLayout = BoundDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, VK_SHADER_STAGE_ALL_GRAPHICS)
			.build();
		std::vector<VkDescriptorSet> sceneDescriptorSets( ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		std::vector<uPtr<SceneSSBO>> sceneSSBOStruct( ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		for (int i = 0; i < sceneDescriptorSets.size(); i++) {
			auto bufferInfo = sceneSSBO[i]->descriptorInfo();
			BoundDescriptorWriter(*sceneSetLayout, *SharedPools::staticMaterialPool)
				.writeBuffer(0, &bufferInfo)
				.build(sceneDescriptorSets[i]);
			sceneSSBOStruct[i] = make_uPtr<SceneSSBO>();
		}

		DeferredRenderSystem deferredRenderSystem{ engineDevice, globalSetLayout->getDescriptorSetLayout(), sceneSetLayout->getDescriptorSetLayout(), { 1280, 720 } };

		SceneBufferInputData gbuffer = deferredRenderSystem.getGBufferAttachments();
		PostProcessingSystem ppoSystem{ engineDevice, VkRenderPass(),  globalSetLayout->getDescriptorSetLayout(), sceneSetLayout->getDescriptorSetLayout(), { 1280, 720 }, gbuffer };

		ShadowMappingSystem shadowSystem { engineDevice };
		
		LightSystem lightSystem{};
		ReflectionSystem reflectionSystem{};
		LightingVolumeSystem volumeSystem{};

		// level must be created after resource handler has been initialised due to the fact that the editor camera is created with post processing materials
		SHARD3D_INFO_E("Constructing Level Pointer");
		level = make_sPtr<ECS::Level>(engineDevice.resourceSystem);
		level->createSystem();
		
		auto editor_cameraActor = level->getActorFromUUID(0);

		ImGuiContext* context = ImGui::CreateContext();
		{
			CSimpleIniA ini;
			ini.SetUnicode();
			ini.LoadFile(EDITOR_SETTINGS_PATH);

			ImGui::SetCurrentContext(context);
			ImGuiInitializer::init(engineDevice, engineWindow, context, engineRenderer.getSwapChainRenderPass(), "configdata/editor_layout_mdlwnd.ini", ini.GetBoolValue("THEME", "useLightMode"));
		}
		Editor editor = { engineDevice, engineWindow, engineRenderer.getSwapChainRenderPass(),level, model, context };

		ImGuiInitializer::setViewportImage(&editor.viewportImage, ppoSystem.getFinalImage());

		loadStaticObjects();

		{
			CSimpleIniA ini;
			ini.SetUnicode();
			ini.LoadFile(EDITOR_SETTINGS_PATH);

			float fov = ini.GetDoubleValue("CAMERA", "FOV");
			SHARD3D_INFO_E("Default FOV set to {0} degrees", fov);
			editor_cameraActor.getComponent<Components::CameraComponent>().setFOV(ini.GetDoubleValue("CAMERA", "FOV"));

			if ((std::string)ini.GetValue("CAMERA", "View") == "Perspective") {
				editor_cameraActor.getComponent<Components::CameraComponent>().setProjectionType(ProjectionType::Perspective);
			}
			else if ((std::string)ini.GetValue("CAMERA", "View") == "Orthographic") {
				editor_cameraActor.getComponent<Components::CameraComponent>().setProjectionType(ProjectionType::Orthographic);  //Ortho perspective (not needed 99.99% of the time)
			}
		}

		auto currentTime = std::chrono::high_resolution_clock::now();
		while (!engineWindow.shouldClose()) {
			auto newTime = std::chrono::high_resolution_clock::now();
			float frameTime = std::chrono::duration<float, std::chrono::seconds::period>(newTime - currentTime).count();
			currentTime = newTime;
			glfwPollEvents();
		
			auto possessedCameraActor = level->getPossessedCameraActor();
			auto& possessedCamera = level->getPossessedCamera();

			level->runGarbageCollector(engineDevice);
			engineDevice.resourceSystem->runGarbageCollector();
			possessedCameraActor.getComponent<Components::CameraComponent>().ar = GraphicsSettings2::getRuntimeInfo().aspectRatio;
			possessedCamera.setViewYXZ(possessedCameraActor.getTransform().transformMatrix);
			possessedCameraActor.getComponent<Components::CameraComponent>().setProjection();

			shadowSystem.runGarbageCollector(); // clean up broken shadows
			
			if (auto commandBuffer = engineRenderer.beginFrame()) {
				int frameIndex = engineRenderer.getFrameIndex();
				SharedPools::drawPools[frameIndex]->resetPool();
				FrameInfo frameInfo{
					frameIndex,
					frameTime,
					commandBuffer,
					possessedCamera,
					&engineWindow,
					globalDescriptorSets[frameIndex],
					sceneDescriptorSets[frameIndex],
					*SharedPools::drawPools[frameIndex],
					level,
					engineDevice.resourceSystem
				};
				//	render
				shadowSystem.render(frameInfo);
				//	update
				GlobalUbo ubo{};
				ubo.projection = possessedCamera.getProjection();
				ubo.inverseProjection = possessedCamera.getInverseProjection();
				ubo.view = possessedCamera.getView();
				ubo.inverseView = possessedCamera.getInverseView();
				ubo.screenSize = { GraphicsSettings2::get().WindowWidth, GraphicsSettings2::get().WindowHeight };
				uboBuffers[frameIndex]->writeToBuffer(&ubo);
				uboBuffers[frameIndex]->flush();
				lightSystem.update(frameInfo, sceneSSBOStruct[frameIndex].get());
				reflectionSystem.update(frameInfo, sceneSSBOStruct[frameIndex].get());
				volumeSystem.update(frameInfo, sceneSSBOStruct[frameIndex].get());
				auto* wsf = level->getWorldSceneInfo();
				sceneSSBOStruct[frameIndex]->globalSceneInfo.skybox_id = engineDevice.resourceSystem->retrieveTextureCube(wsf->environment)->getResourceIndex();
				sceneSSBOStruct[frameIndex]->globalSceneInfo.ambientColor = wsf->skyInfo;
				sceneSSBOStruct[frameIndex]->globalSceneInfo.tint = wsf->environmentTint;
				sceneSSBOStruct[frameIndex]->globalSceneInfo.intensity = wsf->environmentTint.w;
				sceneSSBO[frameIndex]->writeToBuffer(sceneSSBOStruct[frameIndex].get());
				sceneSSBO[frameIndex]->flush();
				
				deferredRenderSystem.beginRenderPass(frameInfo);
				deferredRenderSystem.renderDeferred(frameInfo);
				deferredRenderSystem.renderSkybox(frameInfo);
				deferredRenderSystem.renderForward(frameInfo);

				deferredRenderSystem.endRenderPass(frameInfo);

				ppoSystem.renderCore(frameInfo);

				editor.render(frameInfo);
				engineRenderer.beginSwapChainRenderPass(commandBuffer);
				editor.draw(frameInfo);
				engineRenderer.endSwapChainRenderPass(commandBuffer);

				// Command buffer ends
				engineRenderer.endFrame(newTime);

				if (engineWindow.wasWindowResized()) {
					glm::ivec3 newsize = { engineWindow.getExtent().width, engineWindow.getExtent().height, 1};
					vkDeviceWaitIdle(engineDevice.device()); // wait for all in flight frames to finish
					deferredRenderSystem.resize(newsize);
					resizeFramebuffers(newsize.x, newsize.y, &editor, &ppoSystem, &gbuffer);
					engineWindow.resetWindowResizedFlag();
				}
			}
		}

		if (level->simulationState != PlayState::Stopped) level->end();
		vkDeviceWaitIdle(engineDevice.device());

		editor.detach();

		ShaderSystem::destroy();
		
		_special_assets::_editor_icons_destroy();
		
		destroyRenderPasses();
		SharedPools::destructPools();

		delete engineDevice.resourceSystem;

		level = nullptr;
	}

	void Application::loadStaticObjects() {
		LevelManager levelMan(level, engineDevice.resourceSystem);
		levelMan.load("assets/engine/levels/model3d_viewer_level.s3dlevel", true);
		Actor modelActor = level->getActorFromTag("Model3D Viewer Actor");
		modelActor.getComponent<Components::Mesh3DComponent>() = Components::Mesh3DComponent(engineDevice.resourceSystem, model);
		level->getRenderList()->updateActor(&modelActor);
		auto editor_cameraActor = level->getActorFromUUID(0);
		editor_cameraActor.getTransform().setTranslation({ 0.f, -2.f, 1.f });
	}
}