#pragma once
#include <Shard3D/core/asset/material.h>
#include <Shard3D/core/asset/model.h>
namespace Shard3D {
	class ResourceSystem;
	class ModelViewerPanel {
	public:
		ModelViewerPanel() = default;
		ModelViewerPanel(ResourceSystem* resourceSystem, sPtr<Level>& level, AssetID meshAsset);
		~ModelViewerPanel();

		void render(FrameInfo& frameInfo);
		void save();
		void destroy();
		void update();
	private:
		sPtr<Level> preview_builder;
		rPtr<Mesh3D> currentItem{};
		std::vector<AssetID> currentItem_oldmaterials;
		AssetID currentAsset{AssetID::null()};
	};
}