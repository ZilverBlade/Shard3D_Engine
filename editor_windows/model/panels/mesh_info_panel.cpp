#include "mesh_info_panel.h"

#include <imgui.h>
#include <TextEditor.h>
#include <fstream>
#include <Shard3D/core/asset/assetmgr.h>
#include <glm/gtc/type_ptr.hpp>
#include <Shard3D/systems/handlers/render_list.h>
#include <Shard3DEditorKit/kit.h>
namespace Shard3D {
	ModelViewerPanel::ModelViewerPanel(ResourceSystem* resourceSystem, sPtr<Level>& level, AssetID meshAsset) : currentAsset(meshAsset), preview_builder(level) {
		resourceSystem->loadSurfaceMaterial(currentAsset);
		currentItem = resourceSystem->retrieveMesh(currentAsset);
		currentItem_oldmaterials = currentItem->materials;
	}
	ModelViewerPanel::~ModelViewerPanel() {}

	void ModelViewerPanel::render(FrameInfo& frameInfo) {
		ImGui::Begin("Model3D viewer");
		
		if (ImGui::Button("Save")) {
			
		}
		ImGui::End();
	}
	void ModelViewerPanel::save() {
		AssetManager::modifyMesh(currentAsset.getAsset(), currentItem);
		auto& allmat = currentItem->materials;
		for (int i = 0; i < currentItem_oldmaterials.size(); i++)
			if (AssetReferenceTracker::hasUseeReference(currentAsset, currentItem_oldmaterials[i]))
				AssetReferenceTracker::rmvReference(currentAsset, currentItem_oldmaterials[i]);
		for (int i = 0; i < allmat.size(); i++)
			AssetReferenceTracker::addReference(currentAsset, allmat[i]);

		currentItem_oldmaterials = currentItem->materials;
	}
	void ModelViewerPanel::destroy() {
	}
	void ModelViewerPanel::update() {
		Actor materialActor = preview_builder->getActorFromTag("Model3D Viewer Actor");
		materialActor.getComponent<Components::Mesh3DComponent>().materials = std::vector<AssetID>{ currentAsset };
		preview_builder->getRenderList()->updateActor(&materialActor);
	}
}