#pragma once

#include <Shard3DEditorKit/kit.h>
#include <Shard3D/core/vulkan_api/graphics_pipeline.h>

// panels
#include "../panels/mesh_info_panel.h" 

#include <Shard3D/events/mouse_event.h>
#include <Shard3D/events/key_event.h>
namespace Shard3D {
	class HUDLayer;
	class HUD;
	class Editor {
	public:
		Editor(EngineDevice& dvc, EngineWindow& wnd, VkRenderPass renderPass, sPtr<Level>& level, AssetID model, ImGuiContext* context);
		~Editor();

		void detach();
		void render(FrameInfo& frameInfo);
		void draw(FrameInfo& frameInfo);
	
		void renderViewport(FrameInfo& frameInfo);

		VkDescriptorSet viewportImage{};
	private:
		void eventEvent(Events::Event& e);
		bool mouseButtonDownEvent(Events::MouseButtonDownEvent& e);
		bool mouseButtonReleaseEvent(Events::MouseButtonReleaseEvent& e);
		bool mouseHoverEvent(Events::MouseHoverEvent& e);
		bool mouseScrollEvent(Events::MouseScrollEvent& e);
		bool keyDownEvent(Events::KeyDownEvent& e);
		bool keyReleaseEvent(Events::KeyReleaseEvent& e);
		bool keyPressEvent(Events::KeyPressEvent& e);
	private:
		void createIcons();
		void renderQuickBar(FrameInfo& frameInfo);
		void renderMenuBar(FrameInfo& frameInfo);
		int width;
		int height;
		bool isViewportHovered = false;

		EngineWindow& engineWindow;
		EngineDevice& engineDevice;
		controller::EditorMovementController editorCameraController{};

		ImGuiContext* context;
		
		ModelViewerPanel modelViewerPanel;
		AssetExplorerPanel assetExplorerPanel{};

		// icons

		struct {
			VkDescriptorSet play, pause, stop, 
				pref, save, load,
				preview, layout, viewport, level, launchgame,
				settings,
				l_save, l_load;
			
		} icons{};
	};

}