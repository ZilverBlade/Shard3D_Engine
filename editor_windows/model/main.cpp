#include "application.h"
#include <Shard3D/utils/logger.h>
/*
	Shard3D 1.0 (2022) created by ZilverBlade
*/

void initEngineFeatures() {
	Shard3D::LOGGER::init();
}
int main(int argc, char* argv[], char* env[]) {
	initEngineFeatures();

	Shard3D::AssetID material{ "assets/metal.s3dasset" };
	if (argc == 2) {
		material = Shard3D::AssetID(argv[1]);
	}
	Shard3D::Application app{ material };
	app.run();
	return EXIT_SUCCESS;
}
