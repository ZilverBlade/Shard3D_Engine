#include "application.h"
#include <Shard3D/utils/logger.h>
#include <Shard3D/systems/handlers/project_system.h>
/*
	Shard3D 1.0 (2022) created by ZilverBlade
*/

void initEngineFeatures() {
	Shard3D::LOGGER::init();
}
int main(int argc, char* argv[], char* env[]) {
	initEngineFeatures();
	Shard3D::AssetID material{ "sponzanious_scene/sponzabricks1.s3dasset" };
	char wkd[256];
	getcwd(wkd, 256);
	Shard3D::AssetID project{ std::string(wkd) + "\\testproject.s3dproj" };
	if (argc == 2) {
		project = Shard3D::AssetID(argv[1]);
		material = Shard3D::AssetID(argv[2]);
	}
	Shard3D::ProjectSystem::init(project.getAsset());
	Shard3D::Application app{ material };
	app.run();
	return EXIT_SUCCESS;
}
