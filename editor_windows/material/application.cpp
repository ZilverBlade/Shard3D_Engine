#include "application.h"

#include <thread>
#include <future>
#include <fstream>
#include <vector>
#include <Shard3D/systems/systems.h>
#include <Shard3D/core/asset/assetmgr.h>

#include <Shard3DEditorKit/kit.h>

#include <Shard3D/core/misc/graphics_settings.h>
#include <Shard3D/core/misc/engine_settings.h>
#include <Shard3D/systems/handlers/system_instantiator.h>
#include <Shard3D/systems/rendering/variance_filtered_ao_system.h>
#include <Shard3DEditorKit/rendering/object_picking_system.h>
#include <Shard3DEditorKit/rendering/highlight_renderer.h>
#include <Shard3DEditorKit/tools/editor_pref.h>

#include "imgui/editor.h"
#include <Shard3D/workarounds.h>

// c++ scripts
#include <Shard3D/global.h>
#include <Shard3D/core/vulkan_api/bindless.h>
namespace Shard3D {
	Application::Application(AssetID mat) : material(mat ) {

		setupEngineFeatures();
		createRenderPasses();

	}
	Application::~Application() {
	
	}

	void Application::createRenderPasses() {
		
		
	}

	void Application::destroyRenderPasses() {
		PTR_DELETE(ppoFrameBuffer);
		PTR_DELETE(ppoColorFramebufferAttachment);
		PTR_DELETE(ppoRenderpass);
	}

	void Application::setupEngineFeatures() {
		setWindowCallbacks();

		SharedPools::constructPools(engineDevice);

		engineDevice.resourceSystem = new ResourceSystem(engineDevice);

		_special_assets::_editor_icons_load(engineDevice, engineDevice.resourceSystem->getDescriptor());
		EditorPreferences::load();

		ShaderSystem::init();
	}

	void Application::setWindowCallbacks() {
		SHARD3D_EVENT_BIND_HANDLER_RFC(engineWindow, Application::eventEvent);
	}

	void Application::windowResizeEvent(Events::WindowResizeEvent& e) {
		createRenderPasses();
	}

	void Application::eventEvent(Events::Event& e) {
	}

	void Application::resizeFrameBuffers(uint32_t newWidth, uint32_t newHeight, void* editor, void* ppoSystem, void* gbuffer) {
		reinterpret_cast<PostProcessingSystem*>(ppoSystem)->resize(glm::vec2(newWidth, newHeight), *reinterpret_cast<SceneBufferInputData*>(gbuffer));
		ImGuiInitializer::setViewportImage(reinterpret_cast<Editor*>(editor)->viewportImage, reinterpret_cast<PostProcessingSystem*>(ppoSystem)->getFinalImage());
	}

	void Application::run() {
		// Global pool
		uPtr<BoundDescriptorPool> globalPool{};
		globalPool = BoundDescriptorPool::Builder(engineDevice)
			.setMaxSets( ProjectSystem::getGraphicsSettings().renderer.framesInFlight)
			.addPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,  ProjectSystem::getGraphicsSettings().renderer.framesInFlight)
			.build();
		std::vector<uPtr<EngineBuffer>> uboBuffers( ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		for (int i = 0; i < uboBuffers.size(); i++) {
			uboBuffers[i] = make_uPtr<EngineBuffer>(
				engineDevice,
				sizeof(GlobalUbo),
				1,
				VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
				);
			uboBuffers[i]->map();
		}
		auto globalSetLayout = BoundDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_ALL_GRAPHICS)
			.build();
		std::vector<VkDescriptorSet> globalDescriptorSets( ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		for (int i = 0; i < globalDescriptorSets.size(); i++) {
			auto bufferInfo = uboBuffers[i]->descriptorInfo();
			BoundDescriptorWriter(*globalSetLayout, *globalPool)
				.writeBuffer(0, &bufferInfo)
				.build(globalDescriptorSets[i]);
		}


		std::vector<uPtr<EngineBuffer>> sceneSSBO( ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		for (int i = 0; i < sceneSSBO.size(); i++) {
			sceneSSBO[i] = make_uPtr<EngineBuffer>(
				engineDevice,
				sizeof(SceneSSBO),
				1,
				VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
				);
			sceneSSBO[i]->map();
		}
		uPtr<BoundDescriptorSetLayout> sceneSetLayout = BoundDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, VK_SHADER_STAGE_ALL_GRAPHICS)
			.build();
		std::vector<VkDescriptorSet> sceneDescriptorSets( ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		std::vector<uPtr<SceneSSBO>> sceneSSBOStruct( ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		for (int i = 0; i < sceneDescriptorSets.size(); i++) {
			auto bufferInfo = sceneSSBO[i]->descriptorInfo();
			BoundDescriptorWriter(*sceneSetLayout, *SharedPools::staticMaterialPool)
				.writeBuffer(0, &bufferInfo)
				.build(sceneDescriptorSets[i]);
			sceneSSBOStruct[i] = make_uPtr<SceneSSBO>();
		}

		SystemInstancer::init(engineDevice, globalSetLayout->getDescriptorSetLayout(), sceneSetLayout->getDescriptorSetLayout());

		DeferredRenderSystem* deferredRenderSystem = SystemInstancer::createDeferredRenderer({ 1280, 720 });
		SceneBufferInputData gbuffer = deferredRenderSystem->getGBufferAttachments();
		//BillboardRenderSystem billboardRenderSystem{ engineDevice, deferredRenderSystem->getLightingRenderPass()->getRenderPass(), globalSetLayout->getDescriptorSetLayout() };
		
		PostProcessingSystem ppoSystem{ engineDevice, VkRenderPass(),  globalSetLayout->getDescriptorSetLayout(), sceneSetLayout->getDescriptorSetLayout(), { 1280, 720 }, gbuffer };

		ShadowMappingSystem* shadowSystem = SystemInstancer::createShadowMapper();

		ExponentialFogSystem exponentialFogSystem{};
		LightSystem lightSystem{};
		ReflectionSystem reflectionSystem{};
		SkySystem skySystem{};
		LightingVolumeSystem volumeSystem{};

		engineDevice.resourceSystem->loadCoreAssets();
		engineDevice.resourceSystem->loadTextureCube(AssetID("engine/textures/cubemaps/sky1.s3dasset"));
		

		// level must be created after resource handler has been initialised due to the fact that the editor camera is created with post processing materials
		SHARD3D_INFO_E("Constructing Level Pointer");
		level = make_sPtr<ECS::Level>(engineDevice.resourceSystem);
		level->createSystem();
		
		auto editor_cameraActor = level->getActorFromUUID(0);

		ImGuiContext* context = ImGui::CreateContext();
		{
			CSimpleIniA ini;
			ini.SetUnicode();
			ini.LoadFile(EDITOR_SETTINGS_PATH);

			ImGui::SetCurrentContext(context);
			ImGuiInitializer::init(engineDevice, engineWindow, context, engineRenderer.getSwapChainRenderPass(), "configdata/editor_layout_matwnd.ini", ini.GetBoolValue("THEME", "useLightMode"));
		}
		Editor editor = { engineDevice, engineWindow, engineRenderer.getSwapChainRenderPass(),level, material, context };

		ImGuiInitializer::setViewportImage(editor.viewportImage, ppoSystem.getFinalImage());

		loadStaticObjects();

		auto oldTime = std::chrono::high_resolution_clock::now();
		while (!engineWindow.shouldClose()) {
			StatsTimer::dumpIf();
			StatsTimer::clear();
			auto newTime = std::chrono::high_resolution_clock::now();
			float frameTime = std::chrono::duration<float, std::chrono::seconds::period>(newTime - oldTime).count();
			oldTime = newTime;
			SHARD3D_STAT_RECORD();
			glfwPollEvents();
			SHARD3D_STAT_RECORD_END({ "Window", "Polling" });
			double x, y;
			glfwGetCursorPos(engineWindow.getGLFWwindow(), &x, &y);

			glm::vec2 mousePos{ x,y };

			auto possessedCameraActor = level->getPossessedCameraActor();
			auto& possessedCamera = level->getPossessedCamera();

			if (level->simulationState == PlayState::Playing) {
				//	SHARD3D_STAT_RECORD(); 
				//	physicsSystem.simulate(level.get(), frameTime);
				//	SHARD3D_STAT_RECORD_END({ "Level", "Physics" });
				SHARD3D_STAT_RECORD();
				level->tick(frameTime);
				SHARD3D_STAT_RECORD_END({ "Level", "Tick" });
			}
			SHARD3D_STAT_RECORD();
			level->runGarbageCollector();
			engineDevice.resourceSystem->runGarbageCollector();
			SHARD3D_STAT_RECORD_END({ "Engine", "Garbage Collection" });
			possessedCameraActor.getComponent<Components::CameraComponent>().ar = GraphicsSettings2::getRuntimeInfo().aspectRatio;
			possessedCamera.setViewYXZ(possessedCameraActor.getTransform().transformMatrix);
			possessedCameraActor.getComponent<Components::CameraComponent>().setProjection();

			shadowSystem->runGarbageCollector(level); // clean up broken shadows

			while (GlobalFrameState::lockFrame) { glfwPollEvents(); }

			if (auto commandBuffer = engineRenderer.beginFrame()) {
				SHARD3D_ASSERT(!GlobalFrameState::lockFrame && "Frame must be unlocked before can start!");
				GlobalFrameState::isFrameInProgress = true;
				int frameIndex = engineRenderer.getFrameIndex();
				SharedPools::drawPools[frameIndex]->resetPool();
				FrameInfo frameInfo{
					frameIndex,
					frameTime,
					commandBuffer,
					possessedCamera,
					&engineWindow,
					globalDescriptorSets[frameIndex],
					sceneDescriptorSets[frameIndex],
					*SharedPools::drawPools[frameIndex],
					level,
					engineDevice.resourceSystem
				};
				//	render
				SHARD3D_STAT_RECORD();
				shadowSystem->render(frameInfo);
				SHARD3D_STAT_RECORD_END({ "Rendering", "Shadow Mapping" });

				//SHARD3D_STAT_RECORD();
				//vmaoSystem.render(frameInfo);
				//SHARD3D_STAT_RECORD_END({ "Rendering", "Shadow Mapping" });

				SHARD3D_STAT_RECORD();
				//	update
				GlobalUbo ubo{};
				ubo.projection = possessedCamera.getProjection();
				ubo.inverseProjection = possessedCamera.getInverseProjection();
				ubo.view = possessedCamera.getView();
				ubo.inverseView = possessedCamera.getInverseView();
				ubo.screenSize = { engineWindow.getWindowWidth(), engineWindow.getWindowHeight() };
				uboBuffers[frameIndex]->writeToBuffer(&ubo);
				uboBuffers[frameIndex]->flush();
				lightSystem.update(frameInfo, sceneSSBOStruct[frameIndex].get());
				reflectionSystem.update(frameInfo, sceneSSBOStruct[frameIndex].get());
				volumeSystem.update(frameInfo, sceneSSBOStruct[frameIndex].get());
				skySystem.update(frameInfo, sceneSSBOStruct[frameIndex].get());
				exponentialFogSystem.update(frameInfo, sceneSSBOStruct[frameIndex].get());
				sceneSSBO[frameIndex]->writeToBuffer(sceneSSBOStruct[frameIndex].get());
				sceneSSBO[frameIndex]->flush();
				SHARD3D_STAT_RECORD_END({ "UBO", "Update" });

				deferredRenderSystem->beginDeferredPass(frameInfo);
				SHARD3D_STAT_RECORD();
				deferredRenderSystem->renderDeferred(frameInfo);
				SHARD3D_STAT_RECORD_END({ "Deferred Pass", "Geometry, Lighting" });
				deferredRenderSystem->beginBlendingPass(frameInfo);
				//billboardRenderSystem.render(frameInfo);
				SHARD3D_STAT_RECORD();
				deferredRenderSystem->renderForward(frameInfo);
				SHARD3D_STAT_RECORD_END({ "Forward Pass", "Transparency" });

				SHARD3D_STAT_RECORD();

				SHARD3D_STAT_RECORD_END({ "Forward Pass", "Editor" });
				//if (Actor activeObj = Actor((entt::entity)pickingSystem.getActiveObject(), level.get()); activeObj.isInvalid() == false) 
				//	highlightRenderer.renderHighlight(frameInfo, { activeObj });

				deferredRenderSystem->beginCompositionPass(frameInfo);
				SHARD3D_STAT_RECORD();
				deferredRenderSystem->renderSkybox(frameInfo);
				SHARD3D_STAT_RECORD_END({ "Forward Pass", "Skybox" });
				deferredRenderSystem->endComposition(frameInfo);

				SHARD3D_STAT_RECORD();
				ppoSystem.renderCore(frameInfo);
				SHARD3D_STAT_RECORD_END({ "Post Processing", "Main" });
				editor.render(frameInfo);
				engineRenderer.beginSwapChainRenderPass(commandBuffer);
				editor.draw(frameInfo);
				SHARD3D_STAT_RECORD();
				engineRenderer.endSwapChainRenderPass(commandBuffer);
				SHARD3D_STAT_RECORD_END({ "Swapchain", "End" });

				// Command buffer ends
				SHARD3D_STAT_RECORD();
				engineRenderer.endFrame(newTime);
				SHARD3D_STAT_RECORD_END({ "Command Buffer", "Submit" });

				SHARD3D_STAT_RECORD();
				if (engineWindow.wasWindowResized()) {
					glm::ivec3 newsize = { engineWindow.getExtent().width, engineWindow.getExtent().height, 1 };
					vkDeviceWaitIdle(engineDevice.device()); // wait for all in flight frames to finish
					deferredRenderSystem->resize(newsize);
					resizeFrameBuffers(newsize.x, newsize.y, &editor, &ppoSystem, &gbuffer);
					engineWindow.resetWindowResizedFlag();
				}
				SHARD3D_STAT_RECORD_END({ "Window", "Resize check & FBuffer" });
				GlobalFrameState::isFrameInProgress = false;
			}
		}

		if (level->simulationState != PlayState::Stopped) level->end();
		vkDeviceWaitIdle(engineDevice.device());

		editor.detach();

		ShaderSystem::destroy();
		
		_special_assets::_editor_icons_destroy();
		
		destroyRenderPasses();
		SharedPools::destructPools();

		delete engineDevice.resourceSystem;

		level = nullptr;
	}

	void Application::loadStaticObjects() {
		LevelManager levelMan(level, engineDevice.resourceSystem);
		levelMan.load("assets/engine/levels/material_builder_level.s3dlevel", true);
		Actor materialActor = level->getActorFromTag("Material Ball Preview Actor");
		materialActor.getComponent<Components::Mesh3DComponent>().materials = std::vector<AssetID>{ material };
		level->getRenderList()->updateActor(&materialActor);
		auto editor_cameraActor = level->getActorFromUUID(0);
		editor_cameraActor.getTransform().setTranslation({ 0.f, -2.f, 1.f });
	}
}