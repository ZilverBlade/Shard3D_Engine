#include "application.h"
#include <Shard3D/utils/logger.h>
#include <Shard3D/systems/handlers/project_system.h>
/*
	Shard3D 1.0 (2022) created by ZilverBlade
*/

void initEngineFeatures() {
	Shard3D::LOGGER::init();
	VkResult result = volkInitialize();
	if (result != VK_SUCCESS) {
		SHARD3D_FATAL("Failed to intialize vulkan! (volk.h) Does your system support vulkan?");
	}
}

int main(int argc, char* argv[], char* env[]) {
	initEngineFeatures();
	char wkd[256];
	getcwd(wkd, 256);
	Shard3D::AssetID project{ std::string(wkd) + "\\testproject.s3dproj" };
	Shard3D::AssetID level{ Shard3D::AssetID("content/sponza/sponza.s3dlevel") };
	if (argc == 3) {
		project = Shard3D::AssetID(argv[1]);
		level = Shard3D::AssetID(argv[2]);
	} else if (argc == 2){
		SHARD3D_FATAL("Not enough arguments provided! (Provide: 'project' 'level')");
	}
	Shard3D::ProjectSystem::init(project.getAsset(), false);
	Shard3D::Application app{  };
	app.run(level);
	return EXIT_SUCCESS;
}
