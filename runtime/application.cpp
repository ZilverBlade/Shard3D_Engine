#include "application.h"

#include <thread>
#include <future>
#include <fstream>
#include <vector>
#include <Shard3D/systems/systems.h>
#include <Shard3D/systems/handlers/vertex_arena_allocator.h>
#include <Shard3D/systems/rendering/variance_filtered_ao_system.h>
#include <Shard3D/core/asset/assetmgr.h>

#include <Shard3D/core/misc/graphics_settings.h>
#include <Shard3D/core/misc/engine_settings.h>

#include <Shard3D/scripting/script_engine.h>

// c++ scripts
#include <Shard3D/global.h>
#include <Shard3D/core/vulkan_api/bindless.h>
#include <Shard3D/systems/handlers/project_system.h>
#include <Shard3D/core/rendering/camera_render_instance.h>
#include <Shard3D/core/ecs/level_simulation.h>

namespace Shard3D {
	Application::Application() {
		createRenderPasses();
		setupEngineFeatures();
		SHARD3D_INFO("Constructing Level Pointer");
	}
	Application::~Application() {

	}

	void Application::createRenderPasses() {

	}

	void Application::destroyRenderPasses() {

	}

	void Application::setupEngineFeatures() {
		EngineAudio::init();

		ScriptEngine::init(&engineWindow);

		setWindowCallbacks();

		resourceSystem = new ResourceSystem(engineDevice, ProjectSystem::getGraphicsSettings().renderer.framesInFlight);

		ShaderSystem::init();
	}

	void Application::setWindowCallbacks() {
		engineWindow.setEventCallback(std::bind(&Application::eventEvent, this, std::placeholders::_1));
	}

	void Application::eventEvent(Events::Event& e) {
		//SHARD3D_LOG("{0}", e.toString());
	}


	void Application::run(AssetID levelpath) {
		resourceSystem->loadCoreAssets();
		resourceSystem->loadTextureCube(AssetID("engine/textures/cubemaps/sky1.s3dasset"));

		level = make_sPtr<ECS::Level>(resourceSystem);
		level->createSystem();
		loadStaticObjects();


		globalSetLayout = S3DDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_COMPUTE_BIT)
			.build();
		sceneSetLayout = S3DDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_COMPUTE_BIT)
			.addBinding(1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_COMPUTE_BIT)
			.addBinding(2, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_COMPUTE_BIT)
			.addBinding(3, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_COMPUTE_BIT)
			.addBinding(4, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_COMPUTE_BIT)
			.build();
		skeletonDescriptorSetLayout = S3DDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT)
			.build();
		terrainDescriptorSetLayout = S3DDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT)
			.addBinding(1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT)
			.addBinding(2, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT)
			.addBinding(3, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT)
			.build();


		SHARD3D_ASSERT(IOUtils::doesFileExist(levelpath.getAbsolute()) && "Level does not exist!");
		SHARD3D_ASSERT(ResourceSystem::discoverAssetType(levelpath.getAbsolute()) == AssetType::Level && "Item provided is not a level!");

		ECS::LevelManager levelMan(*level, resourceSystem);
		ECS::LevelMgrResults result = levelMan.load(levelpath.getAbsolute(), true);

		glm::vec2 resolution = { engineWindow.getWindowWidth(), engineWindow.getWindowHeight() };

		uPtr<PostFX::GammaCorrect> fxSwapChain;

		std::vector<LevelRenderInstance*> levelRenderInstances{};
		std::vector<CameraRenderInstance*> renderTargetInstances{};
		LevelRenderInstance* mainLevelRender = new LevelRenderInstance(engineDevice, resourceSystem, globalSetLayout->getDescriptorSetLayout(), sceneSetLayout,
			skeletonDescriptorSetLayout, terrainDescriptorSetLayout, ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		mainLevelRender->resizePlanarReflections(resolution, {});
		levelRenderInstances.push_back(mainLevelRender);

		DeferredRenderSystem::RendererFeatures rendererFeatures{};
		rendererFeatures.enableSSAO = ProjectSystem::getEngineSettings().postfx.enableSSAO;
		rendererFeatures.enableEarlyZ = ProjectSystem::getEngineSettings().renderer.earlyZMode != ESET::EarlyDepthMode::Off;
		rendererFeatures.enableTranslucency = true;
		rendererFeatures.enableMSAA = ProjectSystem::getGraphicsSettings().renderer.msaa;
		rendererFeatures.msaaSamples = (VkSampleCountFlagBits)ProjectSystem::getGraphicsSettings().renderer.msaaSamples;
		rendererFeatures.enableStencilBuffer = false;
		rendererFeatures.enableNormalDecals = ProjectSystem::getEngineSettings().renderer.enableDeferredDecals;

		CameraRenderInstance::RendererFeatures rtiRendererFeatures{};
		rtiRendererFeatures.deferredRendererFeatures = rendererFeatures;
		rtiRendererFeatures.enableMotionBlur = ProjectSystem::getEngineSettings().postfx.enableMotionBlur;
		rtiRendererFeatures.enablePostFX = true;
		rtiRendererFeatures.enableDecals = ProjectSystem::getEngineSettings().renderer.enableDeferredDecals;

		CameraRenderInstance* mainCameraRenderer = new CameraRenderInstance(engineDevice, resourceSystem, rtiRendererFeatures, resolution, 
			globalSetLayout, sceneSetLayout->getDescriptorSetLayout(), skeletonDescriptorSetLayout->getDescriptorSetLayout(), terrainDescriptorSetLayout->getDescriptorSetLayout(), ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		renderTargetInstances.push_back(mainCameraRenderer);

		fxSwapChain = make_uPtr<PostFX::GammaCorrect>(engineDevice,
			resolution,
			mainCameraRenderer->getFinalImage(),
			engineSwapChain->getRenderPass()
		);

		auto editor_cameraActor = level->getActorFromUUID(0, 0);
		bool reverseDepth = ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit;

		std::vector<uPtr<S3DCommandBuffer>> commandBuffers{};
		commandBuffers.resize(engineSwapChain->getImageCount());
		for (auto& cb : commandBuffers) {
			cb = make_uPtr<S3DCommandBuffer>(engineDevice);
		}

		sPtr<HUDRenderSystem> hudRenderSystem = make_sPtr<HUDRenderSystem>(engineDevice, engineWindow, resourceSystem, mainCameraRenderer->getFinalImage(), resolution);
		sPtr<PhysicsSystem> physicsSystem = make_sPtr<PhysicsSystem>(resourceSystem, static_cast<float>(ProjectSystem::getEngineSettings().physics.updateRate), ProjectSystem::getEngineSettings().physics.useQuickStep);
		LevelSimulationState::setData(physicsSystem, hudRenderSystem, resourceSystem);
		level->hideAllEditorOnlyActors();
		level->runGarbageCollector();
		level->rebuildTransforms();
		LevelSimulationState::instantiate(level, PlayState::Playing);
		LevelSimulationState::eventBegin();

		float ar = 1.0f;
		auto oldTime = std::chrono::high_resolution_clock::now();

		uint32_t swapChainImageIndex;
		uint32_t frameIndex = 0;
		while (!engineWindow.shouldClose()) {
			auto newTime = std::chrono::high_resolution_clock::now();
			float frameTime = std::chrono::duration<float, std::chrono::seconds::period>(newTime - oldTime).count();
			oldTime = newTime;
			frameTime = std::min(frameTime, static_cast<float>(ProjectSystem::getEngineSettings().cpu.maximumAllowedFrameTimeMilliseconds) / 1000.f);
			glfwPollEvents();
			double x, y;
			int wx, wy;
			glfwGetCursorPos(engineWindow.getGLFWwindow(), &x, &y);
			glfwGetWindowPos(engineWindow.getGLFWwindow(), &wx, &wy);
		
			ar = static_cast<float>(engineSwapChain->getSwapChainExtent().width) / static_cast<float>(engineSwapChain->getSwapChainExtent().height);
		
			auto possessedCameraActor = level->getPossessedCameraActor();
			auto& possessedCamera = level->getPossessedCamera();
			possessedCameraActor.getComponent<Components::CameraComponent>().ar = ar;
		
			level->runGarbageCollector();
		
		
			LevelSimulationState::eventTick(frameTime);
			if (level->simulationState == PlayState::Playing) {
				level = ScriptEngine::getContext();
			}
			level->rebuildTransforms();
		
			for (auto obj : level->registry.view<Components::TransformComponent>()) {
				ECS::Actor actor = { obj, level.get() };
				auto& tc = actor.getTransform();
				mainLevelRender->getRenderObjects().renderList->updateActorTransform(actor, tc.transformMatrix, tc.normalMatrix);
			}
		
			if (VkResult result = engineSwapChain->acquireNextImage(&swapChainImageIndex); result == VK_SUCCESS) {
				GlobalFrameState::isFrameInProgress = true;
		
				hudRenderSystem->setCursorPosition(
					{ 0, 0 },
					{ engineWindow.getExtent().width, engineWindow.getExtent().height },
					{ x, y }
				);
		
				hudRenderSystem->newFrame();
				hudRenderSystem->getDrawList()->FrameStart();
				for (auto& layer : hudRenderSystem->getAllLayers()) {
					hudRenderSystem->renderHUDLayer(hudRenderSystem->getDrawList(), layer);
				}
				hudRenderSystem->getDrawList()->FrameEnd();
		
				commandBuffers[frameIndex]->begin();
				VkCommandBuffer commandBuffer = commandBuffers[frameIndex]->getCommandBuffer();
				VkFence fence = engineSwapChain->getFence(frameIndex);
		
				resourceSystem->runGarbageCollector(frameIndex, fence);
				resourceSystem->getVertexAllocator()->runGarbageCollector(frameIndex, fence);
				for (LevelRenderInstance* lri : levelRenderInstances) {
					lri->runGarbageCollector(frameIndex, fence, level);
				}
				resourceSystem->getVertexAllocator()->bind(commandBuffer);
				resourceSystem->updateSurfaceMaterials(commandBuffer);
		
				renderTargetInstances[0]->updateCamera(level, level->getPossessedCameraActor(), ar);
				renderTargetInstances[0]->renderEarlyZ( commandBuffer, mainLevelRender->getDescriptorSet(frameIndex), fence, frameIndex, frameTime, 
					level->getPossessedCameraActor(), level, mainLevelRender->getRenderObjects());
		
				levelRenderInstances[0]->render(commandBuffer, frameIndex, level);
				renderTargetInstances[0]->render(commandBuffer, mainLevelRender->getDescriptorSet(frameIndex), fence, frameIndex, frameTime,
					level->getPossessedCameraActor(), level, mainLevelRender->getRenderObjects(), *mainLevelRender->getBuffer(frameIndex));
		
				possessedCameraActor.getComponent<Components::CameraComponent>().setProjection(ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit, ProjectSystem::getEngineSettings().renderer.infiniteFarZ);
				possessedCameraActor.getComponent<Components::CameraComponent>().camera.setViewYXZ(possessedCameraActor.getTransform().transformMatrix);
				hudRenderSystem->render(commandBuffer, frameIndex);
				engineSwapChain->beginRenderPass(commandBuffer);
				fxSwapChain->render(commandBuffer, level->getPossessedCamera().postfx);
				engineSwapChain->endRenderPass(commandBuffer);
				commandBuffers[frameIndex]->end();
				mainLevelRender->getRenderObjects().renderList->updateDynamicTransformBufferData(frameIndex);
				mainLevelRender->getRenderObjects().renderList->updateDynamicRiggingBufferData(level, frameIndex);
		
				engineRenderer.submitCommandBuffer(commandBuffers[frameIndex].get());
				engineRenderer.submitQueue(engineSwapChain->getSubmitInfo(&swapChainImageIndex), engineSwapChain->getFence(frameIndex));
				engineSwapChain->present(engineRenderer.getQueue(), &swapChainImageIndex);
		
				const float frameTime = std::chrono::duration<double, std::chrono::seconds::period>(std::chrono::high_resolution_clock::now() - newTime).count();
				int fpsLimit = engineWindow.isWindowFocussed() ? ProjectSystem::getGraphicsSettings().renderer.frameRateLimit : 30;
				const float waitTime = std::max(1.f / fpsLimit - frameTime, 0.f);
		
				if (waitTime > 0.f) SHARD3D_WAITFOR(waitTime);
		
				frameIndex = (frameIndex + 1) % ProjectSystem::getGraphicsSettings().renderer.framesInFlight;
			} else if (result == VK_SUBOPTIMAL_KHR || result == VK_ERROR_OUT_OF_DATE_KHR) {
				VkExtent2D extent = engineWindow.getExtent();
		
				while (extent.width == 0 || extent.height == 0) {
					extent = engineWindow.getExtent();
					glfwWaitEvents();
				}
				glm::vec2 newsize = { extent.width, extent.height };
				vkDeviceWaitIdle(engineDevice.device()); // wait for all in flight frames to finish
				resourceSystem->runGarbageCollector(frameIndex, engineSwapChain->getFence(frameIndex));
				resourceSystem->getVertexAllocator()->runGarbageCollector(frameIndex, engineSwapChain->getFence(frameIndex));
				for (LevelRenderInstance* lri : levelRenderInstances) {
					lri->runGarbageCollector(frameIndex, engineSwapChain->getFence(frameIndex), level);
				}
				S3DSwapChain* oldSwapChain = engineSwapChain;
				engineSwapChain = new S3DSwapChain(engineDevice, engineWindow, extent, oldSwapChain);
				delete oldSwapChain;
				renderTargetInstances[0]->resize(newsize);
				levelRenderInstances[0]->resizePlanarReflections(newsize, {});
				hudRenderSystem->resize(mainCameraRenderer->getFinalImage(), newsize);
				fxSwapChain->resize(newsize, { renderTargetInstances[0]->getFinalImage()->getDescriptor() });
				engineWindow.resetWindowResizedFlag();
				frameIndex = 0; // match up with the swap chain
			}
			level->cleanup();
		}


		LevelSimulationState::eventEnd();
		LevelSimulationState::release();
		LevelSimulationState::removeData();
		vkDeviceWaitIdle(engineDevice.device());
		delete engineSwapChain;
		hudRenderSystem = nullptr;
		ShaderSystem::destroy();
		ScriptEngine::destroy();

		for (LevelRenderInstance* lri : levelRenderInstances) {
			delete lri;
		}
		for (CameraRenderInstance* rti : renderTargetInstances) {
			delete rti;
		}
		delete resourceSystem;

		level = nullptr;
	}

	void Application::loadStaticObjects() {

	}
}