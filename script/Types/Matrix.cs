﻿using System;

namespace Shard3D.Library.Types {
    public struct Float3x3 {
        public Float3 x;
        public Float3 y;
        public Float3 z;
        public Float3x3(Float3 right, Float3 up, Float3 front) {
            x = right;
            y = up;
            z = front;
        }
        public Float3x3(float identity) {
            x = new Float3(identity, 0.0f, 0.0f);
            y = new Float3(0.0f, identity, 0.0f);
            z = new Float3(0.0f, 0.0f, identity);
        }

        public static Float3x3 operator *(Float3x3 lhs, Float3x3 rhs) {
            Float3 SrcA0 = lhs[0];
            Float3 SrcA1 = lhs[1];
            Float3 SrcA2 = lhs[2];

            Float3 SrcB0 = rhs[0];
            Float3 SrcB1 = rhs[1];
            Float3 SrcB2 = rhs[2];

            Float3x3 Result = new Float3x3();
            Result[0] = SrcA0 * SrcB0[0] + SrcA1 * SrcB0[1] + SrcA2 * SrcB0[2];
            Result[1] = SrcA0 * SrcB1[0] + SrcA1 * SrcB1[1] + SrcA2 * SrcB1[2];
            Result[2] = SrcA0 * SrcB2[0] + SrcA1 * SrcB2[1] + SrcA2 * SrcB2[2];
            return Result;
        }

        public unsafe Float3 this[int index] { // we use a memory hack to avoid branching
            get {
                return Core.Unsafe.Pointer.ReinterpretCast<Float3x3, Float3>(this, index * sizeof(Float3));
            }
            set {
                var sourceRef = __makeref(this);
                *(Float3*)(IntPtr*)(&sourceRef + index * sizeof(Float3)) = value;
            }
        }
    }
    public struct Float4x4 {
        public Float4 x;
        public Float4 y;
        public Float4 z;
        public Float4 w;
        public Float4x4(Float4 right, Float4 up, Float4 front, Float4 w) {
            x = right;
            y = up;
            z = front;
            this.w = w;
        }
        public Float4x4(float identity) {
            x = new Float4(identity, 0.0f, 0.0f, 0.0f);
            y = new Float4(0.0f, identity, 0.0f, 0.0f);
            z = new Float4(0.0f, 0.0f, identity, 0.0f);
            w = new Float4(0.0f, 0.0f, 0.0f, identity);
        }

        public static Float4x4 operator *(Float4x4 lhs, Float4x4 rhs) {
            Float4 SrcA0 = lhs[0];
            Float4 SrcA1 = lhs[1];
            Float4 SrcA2 = lhs[2];
            Float4 SrcA3 = lhs[3];

            Float4 SrcB0 = rhs[0];
            Float4 SrcB1 = rhs[1];
            Float4 SrcB2 = rhs[2];
            Float4 SrcB3 = rhs[3];

            Float4x4 Result = new Float4x4();
            Result[0] = SrcA0 * SrcB0[0] + SrcA1 * SrcB0[1] + SrcA2 * SrcB0[2] + SrcA3 * SrcB0[3];
            Result[1] = SrcA0 * SrcB1[0] + SrcA1 * SrcB1[1] + SrcA2 * SrcB1[2] + SrcA3 * SrcB1[3];
            Result[2] = SrcA0 * SrcB2[0] + SrcA1 * SrcB2[1] + SrcA2 * SrcB2[2] + SrcA3 * SrcB2[3];
            Result[3] = SrcA0 * SrcB3[0] + SrcA1 * SrcB3[1] + SrcA2 * SrcB3[2] + SrcA3 * SrcB3[3];
            return Result;
        }

        public unsafe Float4 this[int index] { // we use a memory hack to avoid branching
            get {
                return Core.Unsafe.Pointer.ReinterpretCast<Float4x4, Float4>(this, index * sizeof(Float4));
            }
            set {
                var sourceRef = __makeref(this);
                *(Float4*)(IntPtr*)(&sourceRef + index * sizeof(Float4)) = value;
            }
        }
    }
}
