﻿using System;

namespace Shard3D.Library.Types
{
    public struct Float2
    {
        public float x;
        public float y;
        public Float2(float xVal, float yVal)
        {
            x = xVal;
            y = yVal;
        }
        public Float2(float uVal)
        {
            x = uVal;
            y = uVal;
        }
        public Float2(Float3 vec3)
        {
            x = vec3.x;
            y = vec3.y;
        }
        public Float2(Float4 vec4)
        {
            x = vec4.x;
            y = vec4.y; 
        }

        public static implicit operator Float2(Float4 v)
        {
            return new Float2(v);
        }
        public static implicit operator Float2(Float3 v)
        {
            return new Float2(v);
        }

        public static implicit operator Float2(float[] value)
        {
            return new Float2(value[0], value[1]);
        }
        public static Float2 operator +(Float2 first, Float2 second)
        {
            return new Float2(first.x + second.x, first.y + second.y);
        }
        public static Float2 operator -(Float2 first, Float2 second)
        {
            return new Float2(first.x - second.x, first.y - second.y);
        }
        public static Float2 operator *(Float2 first, Float2 second)
        {
            return new Float2(first.x * second.x, first.y * second.y);
        }
        public static Float2 operator /(Float2 first, Float2 second)
        {
            return new Float2(first.x / second.x, first.y / second.y);
        }
        public static Float2 operator *(Float2 first, float second)
        {
            return new Float2(first.x * second, first.y * second);
        }
        public static Float2 operator *(float first, Float2 second) {
            return new Float2(first * second.x, first * second.x);
        }
        public static Float2 operator /(Float2  first, float second) {
            return new Float2(first.x / second, first.x / second);
        }
        public static Float2 operator /(float first, Float2 second) {
            return new Float2(first / second.x, first / second.x);
        }

        public static Float2 operator +(Float2 first, float second)
        {
            return new Float2(first.x + second, first.y + second);
        }
        public static Float2 operator +(float first, Float2 second) {
            return new Float2(first + second.x, first + second.x);
        }
        public static Float2 operator -(Float2 first, float second)
        {
            return new Float2(first.x - second, first.y - second);
        }
        public static Float2 operator -(float first, Float2 second) {
            return new Float2(first - second.x, first - second.x);
        }

        public static bool operator ==(Float2 first, Float2 second)
        {
            return first.x == second.x && first.y == second.y;
        }
        public static bool operator !=(Float2 first, Float2 second)
        {
            return !(first == second);
        }
        public unsafe float this[int index]
        { // we use a memory hack to avoid branching
            get
            {
                return Core.Unsafe.Pointer.ReinterpretCast<Float2, float>(this, index * sizeof(float));
            }

            set
            {
                var sourceRef = __makeref(this);
                *(float*)(IntPtr*)(&sourceRef + index * sizeof(float)) = value;
            }
        }
        public float Length()
        {
            return (float)System.Math.Sqrt(x * x + y * y);
        }
    }
    public struct Float3
    {
        public float x;
        public float y;
        public float z;
        public Float3(float xVal, float yVal, float zVal)
        {
            x = xVal;
            y = yVal;
            z = zVal;
        }
        public Float3(Float2 vec2, float zVal)
        {
            x = vec2.x;
            y = vec2.y;
            z = zVal;
        }  
        public Float3(float uVal)
        {
            x = uVal;
            y = uVal;
            z = uVal;         
        }
        public Float3(Float4 vec4)
        {
            x = vec4.x;
            y = vec4.y;
            z = vec4.z;
        }
        public static implicit operator Float3(Float4 v)
        {
            return new Float3(v);
        }

        public static implicit operator Float3(float[] value)
        {
            return new Float3(value[0], value[1], value[2]);
        }

        public static Float3 operator +(Float3 first, Float3 second)
        {
            return new Float3(first.x + second.x, first.y + second.y, first.z + second.z);
        }
        public static Float3 operator -(Float3 first, Float3 second)
        {
            return new Float3(first.x - second.x, first.y - second.y, first.z - second.z);
        }
        public static Float3 operator *(Float3 first, Float3 second)
        {
            return new Float3(first.x * second.x, first.y * second.y, first.z * second.z);
        }
        public static Float3 operator /(Float3 first, Float3 second)
        {
            return new Float3(first.x / second.x, first.y / second.y, first.z / second.z);
        }

        public static Float3 operator +(Float3 first, Float2 second)
        {
            return new Float3(first.x + second.x, first.y + second.y, first.z);
        }
        public static Float3 operator -(Float3 first, Float2 second)
        {
            return new Float3(first.x - second.x, first.y - second.y, first.z);
        }
        public static Float3 operator *(Float3 first, Float2 second)
        {
            return new Float3(first.x * second.x, first.y * second.y, first.z);
        }
        public static Float3 operator /(Float3 first, Float2 second)
        {
            return new Float3(first.x / second.x, first.y / second.y, first.z);
        }
        public static Float3 operator *(Float3 first, float second)
        {
            return new Float3(first.x * second, first.y * second, first.z * second);
        }
        public static Float3 operator *(float first, Float3 second) {
            return new Float3(first * second.x, first * second.y, first * second.z);
        }
        public static Float3 operator /(Float3 first, float second)
        {
            return new Float3(first.x / second, first.y / second, first.z / second);
        }
        public static Float3 operator /(float first, Float3 second) {
            return new Float3(first / second.x, first / second.y, first / second.z);
        }

        public static Float3 operator +(Float3 first, float second)
        {
            return new Float3(first.x + second, first.y + second, first.z + second);
        }
        public static Float3 operator +(float first, Float3 second) {
            return new Float3(first + second.x, first + second.y, first + second.z);
        }
        public static Float3 operator -(Float3 first, float second)
        {
            return new Float3(first.x - second, first.y - second, first.z - second);
        }
        public static Float3 operator -(float first, Float3 second) {
            return new Float3(first - second.x, first - second.y, first - second.z);
        }

        public static implicit operator Float3(Float2 value)
        {
            return new Float3(value.x, value.y, 0.0F);
        }
        public static bool operator ==(Float3 first, Float3 second)
        {
            return first.x == second.x && first.y == second.y && first.z == second.z;
        }
        public static bool operator !=(Float3 first, Float3 second)
        {
            return !(first == second);
        }
        public unsafe float this[int index]
        { // we use a memory hack to avoid branching
            get
            {
                return Core.Unsafe.Pointer.ReinterpretCast<Float3, float>(this, index * sizeof(float));
            }

            set
            {
                var sourceRef = __makeref(this);
                *(float*)(IntPtr*)(&sourceRef + index * sizeof(float)) = value;
            }
        }
        public float Length()
        {
            return (float)System.Math.Sqrt(x * x + y * y + z * z);
        }
    }
    public struct Float4
    {
        public float x;
        public float y;
        public float z;
        public float w;
        public Float4(float xVal, float yVal, float zVal, float wVal)
        {
            x = xVal;
            y = yVal;
            z = zVal;
            w = wVal;
        }
        public Float4(Float2 vec2x, Float2 vec2y)
        {
            x = vec2x.x;
            y = vec2x.y;
            z = vec2y.x;
            w = vec2y.y;
        }
        public Float4(Float2 vec2, float zVal, float wVal)
        {
            x = vec2.x;
            y = vec2.y;
            z = zVal;
            w = wVal;
        }
        public Float4(Float3 vec3, float wVal)
        {
            x = vec3.x;
            y = vec3.y;
            z = vec3.z;
            w = wVal;
        }
        public Float4(float uVal)
        {
            x = uVal;
            y = uVal;
            z = uVal;
            w = uVal;
        }
        public static implicit operator Float4(float[] value)
        {
            return new Float4(value[0], value[1], value[2], value[3]);
        }

        public static Float4 operator +(Float4 first, Float4 second)
        {
            return new Float4(first.x + second.x, first.y + second.y, first.z + second.z, first.w + second.w);
        }
        public static Float4 operator -(Float4 first, Float4 second)
        {
            return new Float4(first.x - second.x, first.y - second.y, first.z - second.z, first.w - second.w);
        }
        public static Float4 operator *(Float4 first, Float4 second)
        {
            return new Float4(first.x * second.x, first.y * second.y, first.z * second.z, first.w * second.w);
        }
        public static Float4 operator /(Float4 first, Float4 second)
        {
            return new Float4(first.x / second.x, first.y / second.y, first.z / second.z, first.w / second.w);
        }

        public static Float4 operator +(Float4 first, Float3 second)
        {
            return new Float4(first.x + second.x, first.y + second.y, first.z + second.z, first.w);
        }
        public static Float4 operator -(Float4 first, Float3 second)
        {
            return new Float4(first.x - second.x, first.y - second.y, first.z - second.z, first.w);
        }
        public static Float4 operator *(Float4 first, Float3 second)
        {
            return new Float4(first.x * second.x, first.y * second.y, first.z * second.z, first.w);
        }
        public static Float4 operator /(Float4 first, Float3 second)
        {
            return new Float4(first.x / second.x, first.y / second.y, first.z / second.z, first.w);
        }

        public static Float4 operator +(Float4 first, Float2 second)
        {
            return new Float4(first.x + second.x, first.y + second.y, first.z, first.w);
        }
        public static Float4 operator -(Float4 first, Float2 second)
        {
            return new Float4(first.x - second.x, first.y - second.y, first.z, first.w);
        }
        public static Float4 operator *(Float4 first, Float2 second)
        {
            return new Float4(first.x * second.x, first.y * second.y, first.z, first.w);
        }
        public static Float4 operator /(Float4 first, Float2 second)
        {
            return new Float4(first.x / second.x, first.y / second.y, first.z, first.w);
        }
        public static Float4 operator *(Float4 first, float second) {
            return new Float4(first.x * second, first.y * second, first.z * second, first.w * second);
        }
        public static Float4 operator *(float first, Float4 second) {
            return new Float4(first * second.x, first * second.y, first * second.z, first * second.w);
        }
        public static Float4 operator /(Float4 first, float second) {
            return new Float4(first.x / second, first.y / second, first.z / second, first.w / second);
        }
        public static Float4 operator /(float first, Float4 second) {
            return new Float4(first / second.x, first / second.y, first / second.z, first / second.w);
        }

        public static Float4 operator +(Float4 first, float second) {
            return new Float4(first.x + second, first.y + second, first.z + second, first.w + second);
        }
        public static Float4 operator +(float first, Float4 second) {
            return new Float4(first + second.x, first + second.y, first + second.z, first + second.w);
        }
        public static Float4 operator -(Float4 first, float second) {
            return new Float4(first.x - second, first.y - second, first.z - second, first.w - second);
        }
        public static Float4 operator -(float first, Float4 second) {
            return new Float4(first - second.x, first - second.y, first - second.z, first - second.w);
        }

        public static implicit operator Float4(Float2 value)
        {
            return new Float4(value.x, value.y, 0.0F, 0.0F);
        }

        public static implicit operator Float4(Float3 value)
        {
            return new Float4(value.x, value.y, value.z, 0.0F);
        }

        public static bool operator ==(Float4 first, Float4 second)
        {
            return first.x == second.x && first.y == second.y && first.z == second.z && first.w == second.w;
        }
        public static bool operator !=(Float4 first, Float4 second)
        {
            return !(first == second);
        }
        public static Bool4 operator >(Float4 first, Float4 second)
        {
            return new Bool4 (first.x > second.x, first.y > second.y, first.z > second.z, first.w > second.w);
        }
        public static Bool4 operator <(Float4 first, Float4 second)
        {
            return !(first > second);
        }
        public static Bool4 operator >=(Float4 first, Float4 second)
        {
            return new Bool4(first.x >= second.x, first.y >= second.y, first.z >= second.z, first.w >= second.w);
        }
        public static Bool4 operator <=(Float4 first, Float4 second)
        {
            return !(first >= second);
        }
        public unsafe float this[int index] 
        { // we use a memory hack to avoid branching
            get
            {
                return Core.Unsafe.Pointer.ReinterpretCast<Float4, float>(this, index * sizeof(float));
            }
            set
            {
                var sourceRef = __makeref(this);
                *(float*)(IntPtr*)(&sourceRef + index * sizeof(float)) = value;
            }
        }
        public float Length()
        {
            return (float)System.Math.Sqrt(x * x + y * y + z * z + w * w);
        }
    }
    
    public struct Bool2
    {
        public bool x;
        public bool y;
        public Bool2(bool xVal, bool yVal)
        {
            x = xVal;
            y = yVal;
        }
     
        public Bool2(bool uVal)
        {
            x = uVal;
            y = uVal;
        }
        public static implicit operator Bool2(bool[] value)
        {
            return new Bool2(value[0], value[1]);
        }
        public static implicit operator Float2(Bool2 value)
        {
            return new Float2(System.Convert.ToSingle(value.x), System.Convert.ToSingle(value.y));
        }
        public static bool operator ==(Bool2 first, Bool2 second)
        {
            return first.x == second.x && first.y == second.y;
        }
        public static bool operator !=(Bool2 first, Bool2 second)
        {
            return !(first == second);
        }
        public static Bool2 operator !(Bool2 inb)
        {
            return new Bool2(!inb.x, !inb.y);
        }
        public bool All()
        {
            return x && y;
        }
        public bool Any() {
            return x || y;
        }
    }
    public struct Bool3 {
        public bool x;
        public bool y;
        public bool z;
        public Bool3(bool xVal, bool yVal, bool zVal) {
            x = xVal;
            y = yVal;
            z = zVal;
        }
        public Bool3(Bool2 vec2, bool zVal) {
            x = vec2.x;
            y = vec2.y;
            z = zVal;
        }
        public Bool3(bool uVal) {
            x = uVal;
            y = uVal;
            z = uVal;
        }
        public static implicit operator Bool3(bool[] value) {
            return new Bool3(value[0], value[1], value[2]);
        }
        public static implicit operator Float3(Bool3 value) {
            return new Float3(System.Convert.ToSingle(value.x), System.Convert.ToSingle(value.y), System.Convert.ToSingle(value.z));
        }
        public static bool operator ==(Bool3 first, Bool3 second) {
            return first.x == second.x && first.y == second.y && first.z == second.z;
        }
        public static bool operator !=(Bool3 first, Bool3 second) {
            return !(first == second);
        }
        public static Bool3 operator !(Bool3 inb) {
            return new Bool3(!inb.x, !inb.y, !inb.z);
        }
        public bool All() {
            return x && y && z;
        }
        public bool Any() {
            return x || y || z;
        }
    }
    public struct Bool4
    {
        public bool x;
        public bool y;
        public bool z;
        public bool w;
        public Bool4(bool xVal, bool yVal, bool zVal, bool wVal)
        {
            x = xVal;
            y = yVal;
            z = zVal;
            w = wVal;
        }
        public Bool4(Bool2 vec2x, Bool2 vec2y)
        {
            x = vec2x.x;
            y = vec2x.y;
            z = vec2y.x;
            w = vec2y.y;
        }
        public Bool4(Bool2 vec2, bool zVal, bool wVal)
        {
            x = vec2.x;
            y = vec2.y;
            z = zVal;
            w = wVal;
        }
        public Bool4(Bool3 vec3, bool wVal)
        {
            x = vec3.x;
            y = vec3.y;
            z = vec3.z;
            w = wVal;
        }
        public Bool4(bool uVal)
        {
            x = uVal;
            y = uVal;
            z = uVal;
            w = uVal;
        }
        public static implicit operator Bool4(bool[] value)
        {
            return new Bool4(value[0], value[1], value[2], value[3]);
        }
        public static implicit operator Float4(Bool4 value)
        {
            return new Float4(System.Convert.ToSingle(value.x), System.Convert.ToSingle(value.y), System.Convert.ToSingle(value.z), System.Convert.ToSingle(value.w));
        }
        public static bool operator ==(Bool4 first, Bool4 second)
        {
            return first.x == second.x && first.y == second.y && first.z == second.z && first.w == second.w;
        }
        public static bool operator !=(Bool4 first, Bool4 second)
        {
            return !(first == second);
        }
        public static Bool4 operator !(Bool4 inb)
        {
            return new Bool4(!inb.x, !inb.y, !inb.z, !inb.w);
        }
        public bool All()
        {
            return x && y && z && w;
        }
        public bool Any() {
            return x || y || z || w;
        }
    }
    
}
