﻿using Shard3D.Library.Types;

namespace Shard3D.Library {
	public static partial class SMath {
		public static Float4x4 Transpose(Float4x4 f4x4) {
			Float4x4 Result = f4x4;

			Result.x[1] = f4x4.y[0];
			Result.x[2] = f4x4.z[0];
			Result.x[3] = f4x4.w[0];

			Result.y[0] = f4x4.x[1];
			Result.y[2] = f4x4.z[1];
			Result.y[3] = f4x4.w[1];

			Result.z[0] = f4x4.x[2];
			Result.z[1] = f4x4.y[2];
			Result.z[3] = f4x4.w[2];

			Result.w[0] = f4x4.x[3];
			Result.w[1] = f4x4.y[3];
			Result.w[2] = f4x4.z[3];

			return Result;
		}
		public static Float3x3 Transpose(Float3x3 f3x3) {
			Float3x3 Result = f3x3;
			Result.x[1] = f3x3.y[0];
			Result.x[2] = f3x3.z[0];

			Result.y[0] = f3x3.x[1];
			Result.y[2] = f3x3.z[1];

			Result.z[0] = f3x3.x[2];
			Result.z[1] = f3x3.y[2];
			return Result;
		}
	}
}
