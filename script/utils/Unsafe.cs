﻿using System;
namespace Shard3D.Core.Unsafe
{
    class Pointer
    {
        public static unsafe TDest ReinterpretCast<TSource, TDest>(TSource source, long offset = 0)
        {
            var sourceRef = __makeref(source);
            var dest = default(TDest);
            var destRef = __makeref(dest);
            *(IntPtr*)&destRef = *(IntPtr*)(&sourceRef + offset);
            return __refvalue(destRef, TDest);
        }
    }
}
