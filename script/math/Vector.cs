﻿using Shard3D.Library.Types;

namespace Shard3D.Library
{
	public static partial class SMath
	{
		public static Float2 Normalize(Float2 f2)
        {
			return f2 / f2.Length();
        }
		public static Float3 Normalize(Float3 f3)
		{
			return f3 / f3.Length();
		}
		public static Float4 Normalize(Float4 f4)
		{
			return f4 / f4.Length();
		}

		public static float Dot(Float2 fa, Float2 fb)
		{
			return fa.x * fb.x + fa.y * fb.y;
		}								
		public static float Dot(Float3 fa, Float3 fb)
		{
			return fa.x * fb.x + fa.y * fb.y + fa.z * fb.z;
		}							
		public static float Dot(Float4 fa, Float4 fb)
		{
			return fa.x * fb.x + fa.y * fb.y + fa.z * fb.z + fa.w * fb.w;
		}
		
		public static Float3 Cross(Float3 fa, Float3 fb)
        {
			return new Float3(fa.y * fb.z - fb.y * fa.z, fa.z * fb.x - fb.z * fa.x, fa.x * fb.y - fb.x * fa.y);
		}

		public static float Distance(Float2 fa, Float2 fb)
        {
			Float2 lenVec = fb - fa;
			return lenVec.Length();
		}
		public static float Distance(Float3 fa, Float3 fb)
		{
			Float3 lenVec = fb - fa;
			return lenVec.Length();
		}
		public static float Distance(Float4 fa, Float4 fb)
		{
			Float4 lenVec = fb - fa;
			return lenVec.Length();
		}
	}
}
