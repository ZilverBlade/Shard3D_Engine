﻿using Shard3D.Library.Types;

namespace Shard3D.Library
{
    partial class SMath
    {
        public class Bezier
        {
            public Float2 curveOrigin = new Float2(0, 1);
            public Float2 curveDest = new Float2(1, 0);
        }
    }
}
