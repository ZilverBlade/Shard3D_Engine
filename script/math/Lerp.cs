﻿using Shard3D.Library.Types;

namespace Shard3D.Library
{
	public static partial class SMath {
		public static float Lerp(float begin, float end, float alpha)
		{
			return (1 - alpha) * begin + alpha * end;
		}
		public static Float2 Lerp(Float2 begin, Float2 end, float alpha)
		{
			Float2 result;
			float oneMinAlpha = 1.0f - alpha;
			result.x = oneMinAlpha * begin.x + alpha * end.x;
			result.y = oneMinAlpha * begin.y + alpha * end.y;
			return result;
		}
		public static Float3 Lerp(Float3 begin, Float3 end, float alpha)
		{
			Float3 result;
			float oneMinAlpha = 1.0f - alpha;
			result.x = oneMinAlpha * begin.x + alpha * end.x;
			result.y = oneMinAlpha * begin.y + alpha * end.y;
			result.z = oneMinAlpha * begin.z + alpha * end.z;
			return result;
		}
		public static Float4 Lerp(Float4 begin, Float4 end, float alpha)
		{
			Float4 result;
			float oneMinAlpha = 1.0f - alpha;
			result.x = oneMinAlpha * begin.x + alpha * end.x;
			result.y = oneMinAlpha * begin.y + alpha * end.y;
			result.z = oneMinAlpha * begin.z + alpha * end.z;
			result.w = oneMinAlpha * begin.w + alpha * end.w;
			return result;
		}
	  
	}
}
