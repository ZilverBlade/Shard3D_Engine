﻿namespace Shard3D.Library
{
    public static partial class SMath {
        public static float Step(float edge, float x)
        {
            return System.Convert.ToSingle(x > edge);
        }
        public static float LinearStep(float edgeMin, float edgeMax, float x)
        {
            return SMath.Saturate((x - edgeMin) / (edgeMax - edgeMin));
        }
        public static float SmoothStep(float edgeMin, float edgeMax, float x)
        {
            float t = LinearStep(edgeMin, edgeMax, x);
            return t * t * (3.0f - 2.0f * t);
        }
        public static float HardStep(float edgeMin, float edgeMax, float x)
        {
            return SMath.Max((x - edgeMin) * (edgeMax / (1.0f - edgeMin)), 0.0f);
        }
    }
}
