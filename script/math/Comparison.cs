﻿namespace Shard3D.Library
{
    public static partial class SMath {
        public static bool IsBetween(float value, float min, float max)
        {
            return (value < max) && (value > min);
        }
        public static bool IsBetweenOrEqual(float value, float min, float max)
        {
            return (value <= max) && (value >= min);
        }

        public static float Clamp(float value, float min, float max) 
        {
            bool vlessmin = value < min;
            bool vmoremax = value > max;
            return System.Convert.ToSingle(vlessmin) * min 
                + System.Convert.ToSingle(vmoremax) * max 
                + System.Convert.ToSingle(!vmoremax && !vlessmin) * value;
        }

        public static float Min(float x, float y)
        {
            bool xlessy = x < y;
            return System.Convert.ToSingle(xlessy) * x + System.Convert.ToSingle(!xlessy) * y;
        }
        public static float Max(float x, float y)
        {
            bool xmorey = x > y;
            return System.Convert.ToSingle(xmorey) * x + System.Convert.ToSingle(!xmorey) * y;
        }

        public static float Saturate(float value) {
            return SMath.Clamp(value, 0.0f, 1.0f);
        }

    }
}