﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shard3D.Graphics {
    public class Material {
        public Material(string iAsset) {
            Asset = iAsset;
        }


        public readonly string Asset;
    }
}
