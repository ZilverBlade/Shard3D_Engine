﻿using Shard3D.Core;
using Shard3D.Library.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shard3D.Graphics {

    public class SurfaceMaterial : Material {
        public SurfaceMaterial(string iAsset) : base(iAsset) {
            Properties = new SFProperties(iAsset);
        }
        public SurfaceMaterialInstance CreateInstance() {
            InternalCalls.Material_SurfaceMaterial_CreateInstance(Asset, out string _inst);
            return new SurfaceMaterialInstance(_inst);
        }

        virtual protected void Update(float dt) { }

        public struct SFProperties {
            internal SFProperties(string iAsset) {
                Asset = iAsset;
            }
            public float EmissiveStrength {
                get {
                    InternalCalls.Material_SurfaceMaterial_Property_GetEmissiveStrength(Asset, out float _v);
                    return _v;
                }
                set {
                    InternalCalls.Material_SurfaceMaterial_Property_SetEmissiveStrength(Asset, value);
                }
            }
            public Float3 EmissiveColor {
                get {
                    InternalCalls.Material_SurfaceMaterial_Property_GetEmissiveColor(Asset, out Float3 _v);
                    return _v;
                }
                set {
                    InternalCalls.Material_SurfaceMaterial_Property_SetEmissiveColor(Asset, ref value);
                }
            }
            public Float3 DiffuseColor {
                get {
                    InternalCalls.Material_SurfaceMaterial_Property_GetDiffuseColor(Asset, out Float3 _v);
                    return _v;
                }
                set {
                    InternalCalls.Material_SurfaceMaterial_Property_SetDiffuseColor(Asset, ref value);
                }
            }
            public float Specular {
                get {
                    InternalCalls.Material_SurfaceMaterial_Property_GetSpecular(Asset, out float _v);
                    return _v;
                }
                set {
                    InternalCalls.Material_SurfaceMaterial_Property_SetSpecular(Asset, value);
                }
            }
            public float Glossiness {
                get {
                    InternalCalls.Material_SurfaceMaterial_Property_GetGlossiness(Asset, out float _v);
                    return _v;
                }
                set {
                    InternalCalls.Material_SurfaceMaterial_Property_SetGlossiness(Asset, value);
                }
            }
            public float Reflectivity {
                get {
                    InternalCalls.Material_SurfaceMaterial_Property_GetReflectivity(Asset, out float _v);
                    return _v;
                }
                set {
                    InternalCalls.Material_SurfaceMaterial_Property_SetReflectivity(Asset, value);
                }
            }

            private string Asset;
        };

        public SFProperties Properties;
    }
    public class SurfaceMaterialInstance : SurfaceMaterial {
        internal SurfaceMaterialInstance(string instAsset) : base(instAsset) {

        }
    }


    public class SurfaceMaterialMeshOwnedArray {
        private IList<SurfaceMaterial> _source;
        private Actor _meshActor;

        public SurfaceMaterialMeshOwnedArray(IList<SurfaceMaterial> source, Actor meshActor) {
            _source = source;
            _meshActor = meshActor;
        }
        public SurfaceMaterialMeshOwnedArray(string[] sourceAssets, Actor meshActor) {
            foreach (string i in sourceAssets) {
                _source.Add(new SurfaceMaterial(i));
            }
            _meshActor = meshActor;
        }

        public SurfaceMaterial this[int index] {
            get {
                return _source[index]; 
            }
            set {
                InternalCalls.Mesh3DComponent_SetSurfaceMaterialListEntry(_meshActor.ScopeID, _meshActor.ID, value.Asset, (ulong)index); 
            }
        }
    }
}
