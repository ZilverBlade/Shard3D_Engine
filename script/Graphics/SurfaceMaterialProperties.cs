﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shard3D.Graphics {
    public enum SurfaceMaterialShadingModel {
		Unshaded = 0,
		Shaded = 1,
		Emissive = 2,
	//	DeferredFoliage = 3,
	//	DeferredDualLayerClearCoat = 4,
		ForwardPureRefractiveGlass = 5,
		ForwardPureScreenspaceRefractiveGlass = 6
	}
}
