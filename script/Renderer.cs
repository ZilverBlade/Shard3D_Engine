﻿namespace Shard3D
{
    public static class Renderer
    {
        public static Library.Types.Float2 GetResolution() 
        {
            Core.InternalCalls.GetWindowResolution(out Library.Types.Float2 res);
            return res;
        }
    }

}
