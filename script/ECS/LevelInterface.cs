﻿using System;
using Shard3D.Core;

namespace Shard3D {
    public class LevelInterface {
        internal LevelInterface() {

        }

        virtual protected void TimeOfDayModifiedEvent(float newTimeOfDay) { }

        virtual protected void BeginEvent() { }
        virtual protected void TickEventPre(float dt) { }
        virtual protected void TickEventPost(float dt) { }
        virtual protected void EndEvent() { }
    }
}

