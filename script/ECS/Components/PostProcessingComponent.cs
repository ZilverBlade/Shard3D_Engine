﻿using Shard3D.Library.Types;
using Shard3D.Core;

namespace Shard3D {
    public struct PostProcessingEffect {
        public struct ColorGrading {
            public float Contrast;
            public float Saturation;
            public float Gain;
            public float Temperature; // Kelvin
            public float HueShift;
        }
        public struct HDR {
            public float Exposure;
        }
    }
    namespace Components {
        public class PostProcessingComponent : Core.Component {
            public PostProcessingEffect.HDR HDR {
                get {
                    InternalCalls.PostProcessingComponent_GetProperty_HDR(_Actor.ScopeID, _Actor.ID, out PostProcessingEffect.HDR v);
                    return v;
                }
                set {
                    InternalCalls.PostProcessingComponent_SetProperty_HDR(_Actor.ScopeID, _Actor.ID, ref value);
                }
            }
            public PostProcessingEffect.ColorGrading ColorGrading {
                get {
                    InternalCalls.PostProcessingComponent_GetProperty_ColorGrading(_Actor.ScopeID, _Actor.ID, out PostProcessingEffect.ColorGrading v);
                    return v;
                }
                set {
                    InternalCalls.PostProcessingComponent_SetProperty_ColorGrading(_Actor.ScopeID, _Actor.ID, ref value);
                }
            }
        }
    }
}
