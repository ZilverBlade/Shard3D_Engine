﻿using Shard3D.Library.Types;
using Shard3D.Core;

namespace Shard3D {
    
    namespace Components {
        public class IndependentSuspensionComponent : Core.Component {
            public float hinge {
                get {
                    InternalCalls.IndependentSuspensionComponent_GetSteeringAngle(_Actor.ScopeID, _Actor.ID, out float val);
                    return val;
                }
                set {
                    InternalCalls.IndependentSuspensionComponent_SetSteeringAngle(_Actor.ScopeID, _Actor.ID, ref value);
                }
            }
            public float GetAxleVelocity() {
                InternalCalls.IndependentSuspensionComponent_GetAxleVelocity(_Actor.ScopeID, _Actor.ID, out float velocity);
                return velocity;
            }
            public void SetMotorVelocity(float velocity, float forceMAX){
                 InternalCalls.IndependentSuspensionComponent_SetAxleVelocity(_Actor.ScopeID, _Actor.ID, ref velocity, ref forceMAX);
            }
            public void SetMotorAcceleration(float acceleration, float forceMAX) {
                InternalCalls.IndependentSuspensionComponent_SetAxleAcceleration(_Actor.ScopeID, _Actor.ID, ref acceleration, ref forceMAX);;
            }
        }
    }
}
