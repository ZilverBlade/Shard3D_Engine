﻿using Shard3D.Library.Types;
using Shard3D.Core;
using Shard3D.Graphics;

namespace Shard3D.Components {
    public class DeferredDecalComponent : Core.Component {
        public string Material {
            get {
                InternalCalls.DeferredDecalComponent_GetMaterialAsset(_Actor.ScopeID, _Actor.ID, out string _f);
                return _f;
            }
            set {
                InternalCalls.DeferredDecalComponent_SetMaterialAsset(_Actor.ScopeID, _Actor.ID, value);
            }
        }
        public float Opacity {
            get {
                InternalCalls.DeferredDecalComponent_GetOpacity(_Actor.ScopeID, _Actor.ID, out float _f);
                return _f;
            }
            set {
                InternalCalls.DeferredDecalComponent_SetOpacity(_Actor.ScopeID, _Actor.ID, value);
            }
        }
        public uint Priority {
            get {
                InternalCalls.DeferredDecalComponent_GetPriority(_Actor.ScopeID, _Actor.ID, out uint _f);
                return _f;
            }
            set {
                InternalCalls.DeferredDecalComponent_SetPriority(_Actor.ScopeID, _Actor.ID, value);
            }
        }
    }
}
