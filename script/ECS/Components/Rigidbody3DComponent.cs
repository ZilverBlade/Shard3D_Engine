﻿using Shard3D.Library.Types;
using Shard3D.Core;
using Shard3D.Graphics;
using System.Collections.Generic;

namespace Shard3D.Components {
    public class Rigidbody3DComponent : Core.Component {
        public Float3 LinearVelocity {
            get {
                InternalCalls.Rigidbody3DComponent_GetLinearVelocity(_Actor.ScopeID, _Actor.ID, out Float3 velocity);
                return velocity;
            }
            set {
                InternalCalls.Rigidbody3DComponent_SetLinearVelocity(_Actor.ScopeID, _Actor.ID, ref value);
            }
        }
        public Float3 AngularVelocity {
            get {
                InternalCalls.Rigidbody3DComponent_GetAngularVelocity(_Actor.ScopeID, _Actor.ID, out Float3 velocity);
                return velocity;
            }
            set {
                InternalCalls.Rigidbody3DComponent_SetAngularVelocity(_Actor.ScopeID, _Actor.ID, ref value);
            }
        }
        public void AddForce(Float3 force) {
            InternalCalls.Rigidbody3DComponent_AddForceImpulse(_Actor.ScopeID, _Actor.ID, ref force);
        }
        public void AddForce(Float3 torque, Float3 relativePoint) {
            InternalCalls.Rigidbody3DComponent_AddForceImpulseAtPoint(_Actor.ScopeID, _Actor.ID, ref torque, ref relativePoint);
        }
        public void AddTorque(Float3 torque) {
            InternalCalls.Rigidbody3DComponent_AddTorqueImpulse(_Actor.ScopeID, _Actor.ID, ref torque);
        }
        public bool CollidesWith(Actor actor) {
            return InternalCalls.Rigidbody3DComponent_CollidesWith(_Actor.ScopeID, _Actor.ID, actor.ScopeID, actor.ID);
        }
        public Physics.PhysicsContact[] GetCollidingWith() {
            ulong size;
            InternalCalls.Rigidbody3DComponent_GetCollidingWithArraySize(_Actor.ScopeID, _Actor.ID, out size);
            InternalCalls.internalPhyicsContact_s[] internalContacts = new InternalCalls.internalPhyicsContact_s[size];
            InternalCalls.Rigidbody3DComponent_GetCollidingWithArrayData(_Actor.ScopeID, _Actor.ID, out internalContacts);
            Physics.PhysicsContact[] contacts = new Physics.PhysicsContact[size];
            for (ulong i = 0; i < size; i++) {
                contacts[i].actor = new Actor(internalContacts[i].scope, internalContacts[i].id);
                contacts[i].point = internalContacts[i].point;
                contacts[i].normal = internalContacts[i].normal;
            }
            return contacts;
        }
    }
}
