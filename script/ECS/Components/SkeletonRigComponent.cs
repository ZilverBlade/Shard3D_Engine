﻿using Shard3D.Library.Types;
using Shard3D.Core;
using Shard3D.Graphics;
using System.Collections.Generic;

namespace Shard3D.Components {
    public class SkeletonBoneTransform {
        internal SkeletonBoneTransform(ulong scopeuuid, ulong actorid, uint boneid_) {
            bpid = scopeuuid;
            id = actorid;
            boneid = boneid_;
        }
        public Float3 Translation {
            get {
                InternalCalls.SkeletonRigComponent_BoneTransform_GetTranslation(bpid, id, boneid, out Float3 t);
                return t;
            }
            set {
                InternalCalls.SkeletonRigComponent_BoneTransform_SetTranslation(bpid, id, boneid, ref value);
            }
        }
        public Float3 Rotation {
            get {
                InternalCalls.SkeletonRigComponent_BoneTransform_GetRotation(bpid, id, boneid, out Float3 r);
                return r;
            }
            set {
                InternalCalls.SkeletonRigComponent_BoneTransform_SetRotation(bpid, id, boneid, ref value);
            }
        }
        public Float3 Scale {
            get {
                InternalCalls.SkeletonRigComponent_BoneTransform_GetScale(bpid, id, boneid, out Float3 s);
                return s;
            }
            set {
                InternalCalls.SkeletonRigComponent_BoneTransform_SetScale(bpid, id, boneid, ref value);
            }
        }

        internal uint boneid;
        internal ulong bpid;
        internal ulong id;
    }
    public class SkeletonRigComponent : Core.Component {
        public void SetAnimation(string asset) {
            InternalCalls.SkeletonRigComponent_SetAnimation(_Actor.ScopeID, _Actor.ID, asset);
        }
        public void StartAnimation() {
            InternalCalls.SkeletonRigComponent_StartAnimation(_Actor.ScopeID, _Actor.ID);
        }
        public void StopAnimation() {
            InternalCalls.SkeletonRigComponent_StopAnimation(_Actor.ScopeID, _Actor.ID);
        }
        public void SeekAnimation(float time) {
            InternalCalls.SkeletonRigComponent_SeekAnimation(_Actor.ScopeID, _Actor.ID, time);
        }
        public float GetAnimationTimeElasped() {
            InternalCalls.SkeletonRigComponent_GetAnimationTimeElasped(_Actor.ScopeID, _Actor.ID, out float time);
            return time;
        }
        public SkeletonBoneTransform GetBone(uint boneID) {
            return new SkeletonBoneTransform(_Actor.ScopeID, _Actor.ID, boneID);
        }
    }
}
