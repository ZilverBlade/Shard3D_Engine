﻿using Shard3D.Library.Types;

namespace Shard3D.Physics {
    public class Physics {
        public static void SetGravity(Float3 force) {
            Core.InternalCalls.Physics_SetGravity(ref force);
        }
        public static PhysicsContact RayCast(Float3 origin, Float3 direction, float maxDistance) {
            Core.InternalCalls.Physics_RayCast(out ulong auuid, out ulong buuid, out Float3 position, out Float3 normal, ref origin, ref direction, maxDistance);
            PhysicsContact contact;
            contact.actor = new Actor(buuid, auuid);
            contact.point = position;
            contact.normal = normal;
            return contact;
        }
    }
}
