﻿using Shard3D.Library.Types;

namespace Shard3D.Physics {
    public struct PhysicsContact {
        public Actor actor;
        public Float3 point;
        public Float3 normal;
    }
}
