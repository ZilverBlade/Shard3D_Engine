﻿using System;
using System.Runtime.CompilerServices;
using Shard3D.Library.Types;
using Shard3D.UI;

namespace Shard3D.Core
{
    internal static class InternalCalls
    {
        #region Logging

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Log(string message, LogSeverity severity = LogSeverity.Debug);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void LogNoImpl();

        #endregion

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Utils_UUIDCombine(ulong inheritID, ulong thisID, out ulong nextInheritID);

        #region Level

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void CreateEmptyActor(ulong scopeUUID, out ulong ID, string name, bool stationary);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void SpawnPrefabActor(ulong scopeUUID, out ulong ID, string asset, string name);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void GetActorByTag(ulong scopeUUID, out ulong ID, string name);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void KillActor(ulong scopeUUID, ulong ID);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void CreateEmptyActorUnderActor(ulong scopeUUID, ulong ID, out ulong scope, out ulong id, string name, bool stationary);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void SpawnPrefabActorUnderActor(ulong scopeUUID, ulong ID, out ulong scope, out ulong id, string asset, string name);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Actor_GetPrefabInterface(ulong scopeUUID, ulong ID, string interfaceGet, out object instance);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static bool Actor_HasComponent(ulong scopeUUID, ulong ID, System.Type componentType);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Actor_AddComponent(ulong scopeUUID, ulong ID, System.Type componentType);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Actor_RmvComponent(ulong scopeUUID, ulong ID, System.Type componentType);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Actor_Reparent(ulong scopeUUID, ulong ID, ulong childScopeUUID, ulong childID);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Level_PossessCameraActor(ulong scopeUUID, ulong ID);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Level_Load(string levelPath);

        #endregion

        #region Physics
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Physics_SetGravity(ref Float3 force);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Physics_RayCast(out ulong auuid, out ulong buuid, out Float3 position, out Float3 normal, ref Float3 p, ref Float3 d, float md);
        #endregion

        #region Input
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static bool IsKeyDown(KeyInput keyCode);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static bool IsMouseButtonDown(MouseInput mouseButton);
        #endregion

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static bool IsGamepadPresent(int gamepadIndex);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static bool IsGamepadButtonDown(GamepadButtonInput button, int gamepadIndex);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static float GetGamepadAnalogAxis(GamepadAnalogInput analog, int gamepadIndex, float deadzoneRange);
        
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void GetMousePosition(out Float2 position);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void GetWindowResolution(out Float2 resolution);



        #region TransformComponent

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void TransformComponent_GetTranslation(ulong scopeUUID, ulong ID, out Float3 translation);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void TransformComponent_SetTranslation(ulong scopeUUID, ulong ID, ref Float3 translation);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void TransformComponent_GetRotation(ulong scopeUUID, ulong ID, out Float3 rotation);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void TransformComponent_SetRotation(ulong scopeUUID, ulong ID, ref Float3 rotation);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void TransformComponent_SetRotationWithMatrix(ulong scopeUUID, ulong ID, ref Float3x3 rotationMatrix);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void TransformComponent_GetScale(ulong scopeUUID, ulong ID, out Float3 scale);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void TransformComponent_SetScale(ulong scopeUUID, ulong ID, ref Float3 scale);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void TransformComponent_GetTransformVectorForward(ulong scopeUUID, ulong ID, out Float3 vector);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void TransformComponent_GetTransformVectorRight(ulong scopeUUID, ulong ID, out Float3 vector);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void TransformComponent_GetTransformVectorUp(ulong scopeUUID, ulong ID, out Float3 vector);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void TransformComponent_GetTransformTranslationWorld(ulong scopeUUID, ulong ID, out Float3 position);


        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void TransformComponent_SetRotationWithMatrixWorld(ulong scopeUUID, ulong ID, ref Float3x3 rotationMatrix);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void TransformComponent_SetTransformTranslationWorld(ulong scopeUUID, ulong ID, ref Float3 translation);

        #endregion

        #region CameraComponent

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void CameraComponent_GetFOV(ulong scopeUUID, ulong ID, out float _f);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void CameraComponent_SetFOV(ulong scopeUUID, ulong ID, ref float _f);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void CameraComponent_GetProjectionType(ulong scopeUUID, ulong ID, out int _c);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void CameraComponent_SetProjectionType(ulong scopeUUID, ulong ID, ref int _c);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void CameraComponent_GetNearClip(ulong scopeUUID, ulong ID, out float _nc);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void CameraComponent_SetNearClip(ulong scopeUUID, ulong ID, ref float _nc);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void CameraComponent_GetFarClip(ulong scopeUUID, ulong ID, out float _fc);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void CameraComponent_SetFarClip(ulong scopeUUID, ulong ID, ref float _fc);

        #endregion

        #region PostProcessingComponent

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void PostProcessingComponent_GetProperty_HDR(ulong scopeUUID, ulong ID, out PostProcessingEffect.HDR _f);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void PostProcessingComponent_SetProperty_HDR(ulong scopeUUID, ulong ID, ref PostProcessingEffect.HDR _f);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void PostProcessingComponent_GetProperty_ColorGrading(ulong scopeUUID, ulong ID, out PostProcessingEffect.ColorGrading _c);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void PostProcessingComponent_SetProperty_ColorGrading(ulong scopeUUID, ulong ID, ref PostProcessingEffect.ColorGrading _c);

        #endregion

        #region AudioComponent

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void AudioComponent_GetFile(ulong scopeUUID, ulong ID, out string str);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void AudioComponent_SetFile(ulong scopeUUID, ulong ID, string str);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void AudioComponent_GetPropertiesVolume(ulong scopeUUID, ulong ID, out float v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void AudioComponent_SetPropertiesVolume(ulong scopeUUID, ulong ID, ref float v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void AudioComponent_GetPropertiesPitch(ulong scopeUUID, ulong ID, out float v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void AudioComponent_SetPropertiesPitch(ulong scopeUUID, ulong ID, ref float v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void AudioComponent_Play(ulong scopeUUID, ulong ID);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void AudioComponent_Pause(ulong scopeUUID, ulong ID);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void AudioComponent_Stop(ulong scopeUUID, ulong ID);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void AudioComponent_Resume(ulong scopeUUID, ulong ID);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void AudioComponent_Update(ulong scopeUUID, ulong ID);

        #endregion

        #region BoxVolume

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void BoxVolumeComponent_CalculateInfluence(ulong scopeUUID, ulong ID, ref Float3 point_, out float v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void BoxVolumeComponent_GetBounds(ulong scopeUUID, ulong ID, out Float3 b);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void BoxVolumeComponent_SetBounds(ulong scopeUUID, ulong ID, ref Float3 b);
        #endregion

        #region Mesh3DComponent

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Mesh3DComponent_GetAsset(ulong scopeUUID, ulong ID, out string str);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Mesh3DComponent_SetAsset(ulong scopeUUID, ulong ID, string str);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Mesh3DComponent_GetCastShadows(ulong scopeUUID, ulong ID, out bool vis);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Mesh3DComponent_SetCastShadows(ulong scopeUUID, ulong ID, bool vis);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Mesh3DComponent_GetSurfaceMaterialList(ulong scopeUUID, ulong ID, out string[] assets);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Mesh3DComponent_SetSurfaceMaterialListEntry(ulong scopeUUID, ulong ID, string newAsset, ulong index);

        #endregion


        #region DeferredDecalComponent

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void DeferredDecalComponent_GetMaterialAsset(ulong scopeUUID, ulong ID, out string str);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void DeferredDecalComponent_SetMaterialAsset(ulong scopeUUID, ulong ID, string str);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void DeferredDecalComponent_GetOpacity(ulong scopeUUID, ulong ID, out float o);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void DeferredDecalComponent_SetOpacity(ulong scopeUUID, ulong ID, float o);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void DeferredDecalComponent_GetPriority(ulong scopeUUID, ulong ID, out uint o);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void DeferredDecalComponent_SetPriority(ulong scopeUUID, ulong ID, uint o);

        #endregion

        #region BillboardComponent

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void BillboardComponent_GetAsset(ulong scopeUUID, ulong ID, out string str);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void BillboardComponent_SetAsset(ulong scopeUUID, ulong ID, string str);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void BillboardComponent_Load(ulong scopeUUID, ulong ID);

        #endregion

        #region PointLightComponent

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void PointLightComponent_GetColor(ulong scopeUUID, ulong ID, out Float3 _c);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void PointLightComponent_SetColor(ulong scopeUUID, ulong ID, ref Float3 _c);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void PointLightComponent_GetIntensity(ulong scopeUUID, ulong ID, out float i_);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void PointLightComponent_SetIntensity(ulong scopeUUID, ulong ID, ref float i_);
        #endregion

        #region SpotLightComponent

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void SpotLightComponent_GetColor(ulong scopeUUID, ulong ID, out Float3 _c);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void SpotLightComponent_SetColor(ulong scopeUUID, ulong ID, ref Float3 _c);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void SpotLightComponent_GetIntensity(ulong scopeUUID, ulong ID, out float i_);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void SpotLightComponent_SetIntensity(ulong scopeUUID, ulong ID, ref float i_);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void SpotLightComponent_GetInnerAngle(ulong scopeUUID, ulong ID, out float _r);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void SpotLightComponent_SetInnerAngle(ulong scopeUUID, ulong ID, ref float _r);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void SpotLightComponent_GetOuterAngle(ulong scopeUUID, ulong ID, out float _r);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void SpotLightComponent_SetOuterAngle(ulong scopeUUID, ulong ID, ref float _r);

        #endregion

        #region DirectionalLightComponent

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void DirectionalLightComponent_GetColor(ulong scopeUUID, ulong ID, out Float3 _c);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void DirectionalLightComponent_SetColor(ulong scopeUUID, ulong ID, ref Float3 _c);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void DirectionalLightComponent_GetIntensity(ulong scopeUUID, ulong ID, out float i_);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void DirectionalLightComponent_SetIntensity(ulong scopeUUID, ulong ID, ref float i_);

        #endregion

        #region SkeletonRigComponent
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void SkeletonRigComponent_BoneTransform_GetTranslation(ulong scopeUUID, ulong ID, uint boneID, out Float3 t);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void SkeletonRigComponent_BoneTransform_SetTranslation(ulong scopeUUID, ulong ID, uint boneID, ref Float3 t);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void SkeletonRigComponent_BoneTransform_GetRotation(ulong scopeUUID, ulong ID, uint boneID, out Float3 r);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void SkeletonRigComponent_BoneTransform_SetRotation(ulong scopeUUID, ulong ID, uint boneID, ref Float3 r);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void SkeletonRigComponent_BoneTransform_GetScale(ulong scopeUUID, ulong ID, uint boneID, out Float3 s);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void SkeletonRigComponent_BoneTransform_SetScale(ulong scopeUUID, ulong ID, uint boneID, ref Float3 s);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void SkeletonRigComponent_SetAnimation(ulong scopeUUID, ulong ID, string asset);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void SkeletonRigComponent_StartAnimation(ulong scopeUUID, ulong ID);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void SkeletonRigComponent_StopAnimation(ulong scopeUUID, ulong ID);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void SkeletonRigComponent_SeekAnimation(ulong scopeUUID, ulong ID, float time);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void SkeletonRigComponent_GetAnimationTimeElasped(ulong scopeUUID, ulong ID, out float time);
        

        #endregion

        #region Rigidbody3DComponent
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Rigidbody3DComponent_GetLinearVelocity(ulong scopeUUID, ulong ID, out Float3 _i);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Rigidbody3DComponent_SetLinearVelocity(ulong scopeUUID, ulong ID, ref Float3 _i);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Rigidbody3DComponent_GetAngularVelocity(ulong scopeUUID, ulong ID, out Float3 _i);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Rigidbody3DComponent_SetAngularVelocity(ulong scopeUUID, ulong ID, ref Float3 _i);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Rigidbody3DComponent_AddForceImpulse(ulong scopeUUID, ulong ID, ref Float3 _i);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Rigidbody3DComponent_AddForceImpulseAtPoint(ulong scopeUUID, ulong ID, ref Float3 _i, ref Float3 _rp);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Rigidbody3DComponent_AddTorqueImpulse(ulong scopeUUID, ulong ID, ref Float3 _i);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static bool Rigidbody3DComponent_CollidesWith(ulong scopeUUID, ulong ID, ulong check_bpInvokeUUID, ulong check_UUID);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Rigidbody3DComponent_GetCollidingWithArraySize(ulong scopeUUID, ulong ID, out ulong size);
        internal struct internalPhyicsContact_s {
            internal ulong scope;
            internal ulong id;
            internal Float3 point;
            internal Float3 normal;
        }
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Rigidbody3DComponent_GetCollidingWithArrayData(ulong scopeUUID, ulong ID, out internalPhyicsContact_s[] contacts);
        #endregion

        #region PhysicsConstraintComponent
        #endregion

        #region IndependentSuspensionComponent

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void IndependentSuspensionComponent_GetSteeringAngle(ulong scopeUUID, ulong ID, out float angle);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void IndependentSuspensionComponent_SetSteeringAngle(ulong scopeUUID, ulong ID, ref float angle);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void IndependentSuspensionComponent_GetAxleVelocity(ulong scopeUUID, ulong ID, out float velocity);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void IndependentSuspensionComponent_SetAxleVelocity(ulong scopeUUID, ulong ID, ref float velocity, ref float maxForce);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void IndependentSuspensionComponent_SetAxleAcceleration(ulong scopeUUID, ulong ID, ref float acceleration, ref float maxForce);
        #endregion




        // HUD

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDLayer_Display(string asset);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDLayer_Close(string asset);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDLayer_CreateElement(string layer, string type, out HUDElementData outHandle, out string outElementName);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDLayer_GetElement(string layer, string elementName, out HUDElementData outHandle);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDLayer_EraseElement(string layer, string element);


        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_GetVisible(HUDElementData data, out bool v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_SetVisible(HUDElementData data, bool v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Line_GetPointA(HUDElementData data, out Float2 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Line_SetPointA(HUDElementData data, ref Float2 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Line_GetPointB(HUDElementData data, out Float2 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Line_SetPointB(HUDElementData data, ref Float2 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Line_GetWidth(HUDElementData data, out float v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Line_SetWidth(HUDElementData data, ref float v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Line_GetColor(HUDElementData data, out Float4 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Line_SetColor(HUDElementData data, ref Float4 v);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Rect_GetPosition(HUDElementData data, out Float2 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Rect_SetPosition(HUDElementData data, ref Float2 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Rect_GetScale(HUDElementData data, out Float2 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Rect_SetScale(HUDElementData data, ref Float2 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Rect_GetRotation(HUDElementData data, out float v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Rect_SetRotation(HUDElementData data, ref float v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Rect_GetColor(HUDElementData data, out Float4 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Rect_SetColor(HUDElementData data, ref Float4 v);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Image_GetPosition(HUDElementData data, out Float2 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Image_SetPosition(HUDElementData data, ref Float2 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Image_GetScale(HUDElementData data, out Float2 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Image_SetScale(HUDElementData data, ref Float2 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Image_GetRotation(HUDElementData data, out float v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Image_SetRotation(HUDElementData data, ref float v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Image_GetColor(HUDElementData data, out Float4 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Image_SetColor(HUDElementData data, ref Float4 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Image_GetTexture(HUDElementData data, out string v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Image_SetTexture(HUDElementData data, string v);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Text_GetPosition(HUDElementData data, out Float2 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Text_SetPosition(HUDElementData data, ref Float2 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Text_GetScale(HUDElementData data, out Float2 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Text_SetScale(HUDElementData data, ref Float2 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Text_GetRotation(HUDElementData data, out float v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Text_SetRotation(HUDElementData data, ref float v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Text_GetColor(HUDElementData data, out Float4 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Text_SetColor(HUDElementData data, ref Float4 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Text_GetText(HUDElementData data, out string v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Text_SetText(HUDElementData data, string v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void HUDElement_Text_FormatTextParam(HUDElementData data, string p, string v);

        // MATERIAL

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Material_SurfaceMaterial_CreateInstance(string asset, out string inst);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Material_SurfaceMaterial_Property_GetEmissiveStrength(string asset, out float v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Material_SurfaceMaterial_Property_SetEmissiveStrength(string asset, float v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Material_SurfaceMaterial_Property_GetEmissiveColor(string asset, out Float3 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Material_SurfaceMaterial_Property_SetEmissiveColor(string asset, ref Float3 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Material_SurfaceMaterial_Property_GetDiffuseColor(string asset, out Float3 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Material_SurfaceMaterial_Property_SetDiffuseColor(string asset, ref Float3 v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Material_SurfaceMaterial_Property_GetSpecular(string asset, out float v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Material_SurfaceMaterial_Property_SetSpecular(string asset, float v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Material_SurfaceMaterial_Property_GetGlossiness(string asset, out float v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Material_SurfaceMaterial_Property_SetGlossiness(string asset, float v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Material_SurfaceMaterial_Property_GetReflectivity(string asset, out float v);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        internal extern static void Material_SurfaceMaterial_Property_SetReflectivity(string asset, float v);
    }
}
