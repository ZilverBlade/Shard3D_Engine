﻿using Shard3D.Library.Types;

namespace Shard3D.Library
{
    namespace Animation
    {
        namespace Motion
        {
            public abstract class Tween
            {
                protected float alpha = 0.0f;
                protected float duration;

                public abstract bool Update(float deltaTime);
                public bool UpdateReverse(float deltaTime)
                {
                    return Update(-deltaTime);
                }
                public virtual void Reset()
                {
                    alpha = 0.0f;
                }
                public void Jump(float newTimePos)
                {
                    duration = newTimePos;
                }
                public float GetAlpha()
                {
                    return alpha;
                }
                public float GetElapsed()
                {
                    return alpha * duration;
                }
            }

            public abstract partial class TweenFloat : Tween
            {
                protected float origin;
                protected float dest;
                protected float value;

                public float GetValue()
                {
                    return value;
                }

                public override void Reset()
                {
                    value = 0;
                }

                internal TweenFloat(float durationSeconds, float begin, float final)
                {
                    duration = durationSeconds;
                    origin = begin;
                    dest = final;
                    value = begin;
                }
            }

            public abstract partial class TweenFloat2 : Tween
            {
                protected Float2 origin;
                protected Float2 dest;
                protected Float2 value;

                public Float2 GetValue()
                {
                    return value;
                }

                public override void Reset()
                {
                    value = new Float2();
                }

                internal TweenFloat2(float durationSeconds, Float2 begin, Float2 final)
                {
                    duration = durationSeconds;
                    origin = begin;
                    dest = final;
                    value = begin;
                }
            }

            public abstract partial class TweenFloat3 : Tween
            {
                protected Float3 origin;
                protected Float3 dest;
                protected Float3 value;

                public Float3 GetValue()
                {
                    return value;
                }

                public override void Reset()
                {
                    value = new Float3();
                }

                internal TweenFloat3(float durationSeconds, Float3 begin, Float3 final)
                {
                    duration = durationSeconds;
                    origin = begin;
                    dest = final;
                    value = begin;
                }
            }

            public abstract partial class TweenFloat4 : Tween
            {
                protected Float4 origin;
                protected Float4 dest;
                protected Float4 value;

                public Float4 GetValue()
                {
                    return value;
                }

                public override void Reset()
                {
                    value = new Float4();
                }

                internal TweenFloat4(float durationSeconds, Float4 begin, Float4 final)
                {
                    duration = durationSeconds;
                    origin = begin;
                    dest = final;
                    value = begin;
                }
            }
            public abstract partial class TweenFloat
            {
                public class Linear : TweenFloat
                {
                    public Linear(float durationSeconds, float begin, float final) : base(durationSeconds, begin, final)
                    {
                    }

                    // Updates the interpolation, returns true when the Tween has ended
                    public override bool Update(float deltaTime)
                    {
                        alpha += deltaTime / duration;
                        if (!SMath.IsBetween(alpha, 0f, 1f))
                        {
                            alpha = SMath.Clamp(alpha, 0, 1f);
                            return true;
                        }

                        value = (1 - alpha) * origin + alpha * dest;

                        return false;
                    }
                }
            }
            public abstract partial class TweenFloat2
            {
                public class Linear : TweenFloat2
                {
                    public Linear(float durationSeconds, Float2 begin, Float2 final) : base(durationSeconds, begin, final)
                    {
                    }

                    // Updates the interpolation, returns true when the Tween has ended
                    public override bool Update(float deltaTime)
                    {
                        alpha += deltaTime / duration;
                        if (!SMath.IsBetween(alpha, 0f, 1f))
                        {

                            alpha = SMath.Clamp(alpha, 0, 1f);
                            return true;
                        }

                        value.x = (1 - alpha) * origin.x + alpha * dest.x;
                        value.y = (1 - alpha) * origin.y + alpha * dest.y;

                        return false;
                    }
                }
            }
            public abstract partial class TweenFloat3
            {
                public class Linear : TweenFloat3
                {
                    public Linear(float durationSeconds, Float3 begin, Float3 final) : base(durationSeconds, begin, final)
                    {
                    }
                    // Updates the interpolation, returns true when the Tween has ended
                    public override bool Update(float deltaTime)
                    {
                        alpha += deltaTime / duration;
                        if (!SMath.IsBetween(alpha, 0f, 1f))
                        {

                            alpha = SMath.Clamp(alpha, 0, 1f);
                            return true;
                        }

                        value.x = (1 - alpha) * origin.x + alpha * dest.x;
                        value.y = (1 - alpha) * origin.y + alpha * dest.y;
                        value.z = (1 - alpha) * origin.z + alpha * dest.z;

                        return false;
                    }
                }
            }
            public abstract partial class TweenFloat4
            {
                public class Linear : TweenFloat4
                {
                    public Linear(float durationSeconds, Float4 begin, Float4 final) : base(durationSeconds, begin, final)
                    {
                    }
                    // Updates the interpolation, returns true when the Tween has ended
                    public override bool Update(float deltaTime)
                    {
                        alpha += deltaTime / duration;
                        if (!SMath.IsBetween(alpha, 0f, 1f))
                        {

                            alpha = SMath.Clamp(alpha, 0, 1f);
                            return true;
                        }

                        value.x = (1 - alpha) * origin.x + alpha * dest.x;
                        value.y = (1 - alpha) * origin.y + alpha * dest.y;
                        value.z = (1 - alpha) * origin.z + alpha * dest.z;
                        value.w = (1 - alpha) * origin.w + alpha * dest.w;

                        return false;
                    }
                }
            }

            public abstract partial class TweenFloat
            {
                public class Ease : TweenFloat
                {
                    private float pow;
                    private float destMinOrigin;

                    public Ease(float durationSeconds, float begin, float final, float power = 2.0F) : base(durationSeconds, begin, final)
                    {
                        destMinOrigin = final - begin;
                        pow = power;
                    }

                    // Updates the interpolation, returns true when the Tween has ended
                    public override bool Update(float deltaTime)
                    {
                        alpha += deltaTime / duration;
                        if (!SMath.IsBetween(alpha, 0f, 1f))
                        {

                            alpha = SMath.Clamp(alpha, 0, 1f);
                            return true;
                        }

                        float sqt = alpha * alpha;
                        value = origin + sqt / (2.0F * (sqt - alpha) + 1.0f) * destMinOrigin;

                        return false;
                    }
                }
            }
            public abstract partial class TweenFloat2
            {
                public class Ease : TweenFloat2
                {
                    private float pow;
                    private Float2 destMinOrigin;

                    public Ease(float durationSeconds, Float2 begin, Float2 final, float power = 2.0F) : base(durationSeconds, begin, final)
                    {
                        destMinOrigin = final - begin;
                        pow = power;
                    }

                    // Updates the interpolation, returns true when the Tween has ended
                    public override bool Update(float deltaTime)
                    {
                        alpha += deltaTime / duration;
                        if (!SMath.IsBetween(alpha, 0f, 1f))
                        {

                            alpha = SMath.Clamp(alpha, 0, 1f);
                            return true;
                        }

                        float sqt = alpha * alpha;
                        value.x = origin.x + sqt / (2.0F * (sqt - alpha) + 1.0f) * destMinOrigin.x;
                        value.y = origin.y + sqt / (2.0F * (sqt - alpha) + 1.0f) * destMinOrigin.y;

                        return false;
                    }
                }
            }
            public abstract partial class TweenFloat3
            {
                public class Ease : TweenFloat3
                {
                    private float pow;
                    private Float3 destMinOrigin;

                    public Ease(float durationSeconds, Float3 begin, Float3 final, float power = 2.0F) : base(durationSeconds, begin, final)
                    {
                        duration = durationSeconds;
                        origin = begin;
                        dest = final; value = begin;

                        destMinOrigin = final - begin;
                        pow = power;
                    }

                    // Updates the interpolation, returns true when the Tween has ended
                    public override bool Update(float deltaTime)
                    {
                        alpha += deltaTime / duration;
                        if (!SMath.IsBetween(alpha, 0f, 1f))
                        {

                            alpha = SMath.Clamp(alpha, 0, 1f);
                            return true;
                        }

                        float sqt = alpha * alpha;
                        value.x = origin.x + sqt / (2.0F * (sqt - alpha) + 1.0f) * destMinOrigin.x;
                        value.y = origin.y + sqt / (2.0F * (sqt - alpha) + 1.0f) * destMinOrigin.y;
                        value.z = origin.z + sqt / (2.0F * (sqt - alpha) + 1.0f) * destMinOrigin.z;

                        return false;
                    }
                }
            }

            public abstract partial class TweenFloat4
            {
                public class Ease : TweenFloat4
                {
                    private float pow;
                    private Float4 destMinOrigin;

                    public Ease(float durationSeconds, Float4 begin, Float4 final, float power = 2.0F) : base(durationSeconds, begin, final)
                    {
                        destMinOrigin = final - begin;
                        pow = power;
                    }

                    // Updates the interpolation, returns true when the Tween has ended
                    public override bool Update(float deltaTime)
                    {
                        alpha += deltaTime / duration;
                        if (!SMath.IsBetween(alpha, 0f, 1f))
                        {

                            alpha = SMath.Clamp(alpha, 0, 1f);
                            return true;
                        }

                        float sqt = alpha * alpha;
                        value.x = origin.x + sqt / (2.0F * (sqt - alpha) + 1.0f) * destMinOrigin.x;
                        value.y = origin.y + sqt / (2.0F * (sqt - alpha) + 1.0f) * destMinOrigin.y;
                        value.z = origin.z + sqt / (2.0F * (sqt - alpha) + 1.0f) * destMinOrigin.z;
                        value.w = origin.w + sqt / (2.0F * (sqt - alpha) + 1.0f) * destMinOrigin.w;

                        return false;
                    }
                }
            }

            public abstract partial class TweenFloat
            {
                public class Bezier : TweenFloat
                {
                    private float destMinOrigin;
                    private SMath.Bezier curve;

                    public float p1x = 0;
                    public float p1y = 0;

                    public float p2x = 0;
                    public float p2y = 1;

                    public float p3x = 1;
                    public float p3y = 0;

                    public float p4x = 1;
                    public float p4y = 1;

                    public Bezier(float durationSeconds, float begin, float final, SMath.Bezier curve_) : base(durationSeconds, begin, final)
                    {
                        destMinOrigin = final - begin;
                        curve = curve_;
                    }

                    public override bool Update(float deltaTime)
                    {

                        alpha += deltaTime / duration;
                        if (!SMath.IsBetween(alpha, 0f, 1f))
                        {

                            alpha = SMath.Clamp(alpha, 0, 1f);
                            return true;
                        }

                        double xfunc = (p1x * (System.Math.Pow(1 - alpha, 3))) + (3 * p2x * alpha * System.Math.Pow(1 - alpha, 2)) + (3 * p3x * System.Math.Pow(alpha, 2) * (1 - alpha)) + (System.Math.Pow(alpha, 3) * p4x);
                        double yfunc = (p1y * (System.Math.Pow(1 - alpha, 3))) + (3 * p2y * alpha * System.Math.Pow(1 - alpha, 2)) + (3 * p3y * System.Math.Pow(alpha, 2) * (1 - alpha)) + (System.Math.Pow(alpha, 3) * p4y);
                        alpha += 0.01f;
                        double hyp = System.Math.Sqrt(System.Math.Pow(xfunc, 2) + System.Math.Pow(yfunc, 2));
                        double resultant = System.Math.Sqrt(System.Math.Pow(xfunc, 2) + System.Math.Pow(yfunc, 2) + (2 * xfunc * yfunc * (xfunc / hyp)));

                        value = (float)(origin + resultant * destMinOrigin);

                        return false;
                    }
                }
            }
        }
    }
}