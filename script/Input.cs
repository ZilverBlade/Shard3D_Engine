﻿namespace Shard3D
{
    public enum MouseInput
    {
        MouseButton1 = 0,
        MouseButton2 = 1,
        MouseButton3 = 2,
        MouseButton4 = 3,
        MouseButton5 = 4,
        MouseButton6 = 5,
        MouseButton7 = 6,
        MouseButton8 = 7,
        MOUSE_BUTTON_LAST = MouseButton8,
        MOUSE_BUTTON_LEFT = MouseButton1,
        MOUSE_BUTTON_RIGHT = MouseButton2,
        MOUSE_BUTTON_MIDDLE = MouseButton3,
    }

    public enum KeyInput
    {
        KeySpace             = 32,
        KeyApostrophe        = 39, /* ' */
        KeyComma             = 44, /* , */
        KeyMinus             = 45, /* - */
        KeyPeriod            = 46, /* . */
        KeySlash             = 47, /* / */
        Key0                 = 48,
        Key1                 = 49,
        Key2                 = 50,
        Key3                 = 51,
        Key4                 = 52,
        Key5                 = 53,
        Key6                 = 54,
        Key7                 = 55,
        Key8                 = 56,
        Key9                 = 57,
        KeySemicolon         = 59, /* ; */
        KeyEqual             = 61, /* = */
        KeyA                 = 65,
        KeyB                 = 66,
        KeyC                 = 67,
        KeyD                 = 68,
        KeyE                 = 69,
        KeyF                 = 70,
        KeyG                 = 71,
        KeyH                 = 72,
        KeyI                 = 73,
        KeyJ                 = 74,
        KeyK                 = 75,
        KeyL                 = 76,
        KeyM                 = 77,
        KeyN                 = 78,
        KeyO                 = 79,
        KeyP                 = 80,
        KeyQ                 = 81,
        KeyR                 = 82,
        KeyS                 = 83,
        KeyT                 = 84,
        KeyU                 = 85,
        KeyV                 = 86,
        KeyW                 = 87,
        KeyX                 = 88,
        KeyY                 = 89,
        KeyZ                 = 90,
        KeyLeft_bracket      = 91, /* [ */
        KeyBackslash         = 92, /* \ */
        KeyRight_bracket     = 93, /* ] */
        KeyGrave_accent      = 96, /* ` */
        KeyWorld_1           = 161, /* non-US #1 */
        KeyWorld_2           = 162, /* non-US #2 */
        
        KeyEscape            = 256,
        KeyEnter             = 257,
        KeyTab               = 258,
        KeyBackspace         = 259,
        KeyInsert            = 260,
        KeyDelete            = 261,
        KeyRight             = 262,
        KeyLeft              = 263,
        KeyDown              = 264,
        KeyUp                = 265,
        KeyPage_up           = 266,
        KeyPage_down         = 267,
        KeyHome              = 268,
        KeyEnd               = 269,
        KeyCaps_lock         = 280,
        KeyScroll_lock       = 281,
        KeyNum_lock          = 282,
        KeyPrint_screen      = 283,
        KeyPause             = 284,
        KeyF1                = 290,
        KeyF2                = 291,
        KeyF3                = 292,
        KeyF4                = 293,
        KeyF5                = 294,
        KeyF6                = 295,
        KeyF7                = 296,
        KeyF8                = 297,
        KeyF9                = 298,
        KeyF10               = 299,
        KeyF11               = 300,
        KeyF12               = 301,
        KeyF13               = 302,
        KeyF14               = 303,
        KeyF15               = 304,
        KeyF16               = 305,
        KeyF17               = 306,
        KeyF18               = 307,
        KeyF19               = 308,
        KeyF20               = 309,
        KeyF21               = 310,
        KeyF22               = 311,
        KeyF23               = 312,
        KeyF24               = 313,
        KeyF25               = 314,
        KeyKP_0              = 320,
        KeyKP_1              = 321,
        KeyKP_2              = 322,
        KeyKP_3              = 323,
        KeyKP_4              = 324,
        KeyKP_5              = 325,
        KeyKP_6              = 326,
        KeyKP_7              = 327,
        KeyKP_8              = 328,
        KeyKP_9              = 329,
        KeyKPDecimal        = 330,
        KeyKPDivide         = 331,
        KeyKPMultiply       = 332,
        KeyKPSubtract       = 333,
        KeyKPAdd            = 334,
        KeyKPEnter          = 335,
        KeyKPEqual          = 336,
        KeyLeftShift        = 340,
        KeyLeftControl      = 341,
        KeyLeftAlt          = 342,
        KeyLeftSuper        = 343,
        KeyRightShift       = 344,
        KeyRightControl     = 345,
        KeyRightAlt         = 346,
        KeyRightSuper       = 347,
        KeyMenu             = 348                       
    }
    public enum GamepadButtonInput {
        GamepadButtonA      = 0,
        GamepadButtonB      = 1,
        GamepadButtonX      = 2,
        GamepadButtonY      = 3,
        GamepadButtonLB     = 4,
        GamepadButtonRB     = 5,
        GamepadButtonBack   = 6,
        GamepadButtonStart  = 7,
        GamepadButtonGuide  = 8,
        GamepadButtonLT     = 9, 
        GamepadButtonRT     = 10,
        GamepadButtonUp     = 11,
        GamepadButtonRight  = 12,
        GamepadButtonDown   = 13,
        GamepadButtonLeft   = 14
    }
    public enum GamepadAnalogInput {
        GamepadAnalogLeftStickX = 0,
        GamepadAnalogLeftStickY = 1,
        GamepadAnalogRightStickX = 2,
        GamepadAnalogRightStickY = 3,
        GamepadAnalogLT = 4,
        GamepadAnalogRT = 5
    }
    public static class Input
    {
        public static bool IsKeyDown(KeyInput key)
        {
            return Core.InternalCalls.IsKeyDown(key);
        }
        public static bool IsMouseButtonDown(MouseInput mouseBtn)
        {
            return Core.InternalCalls.IsMouseButtonDown(mouseBtn);
        }

        public static bool IsGamepadPresent(int gamepadIndex = 0) {
            return Core.InternalCalls.IsGamepadPresent(gamepadIndex);
        }
        public static bool IsGamepadButtonDown(GamepadButtonInput button, int gamepadIndex = 0) {
            return Core.InternalCalls.IsGamepadButtonDown(button, gamepadIndex);
        }
        public static float GetGamepadAnalogAxis(GamepadAnalogInput analog, int gamepadIndex = 0, float deadzoneRange = 0.1F) {
            return Core.InternalCalls.GetGamepadAnalogAxis(analog, gamepadIndex, deadzoneRange);
        }

        public static Library.Types.Float2 GetMousePosition() 
        {
            Core.InternalCalls.GetMousePosition(out Library.Types.Float2 pos);
            return pos;
        }
    }

}
