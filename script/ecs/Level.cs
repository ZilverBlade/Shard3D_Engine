﻿namespace Shard3D
{
    public class Level 
    {
        public static Actor CreateEmptyActor(string name = "Some kind of Actor", bool stationary = false) {
            Core.InternalCalls.CreateEmptyActor(0, out ulong id, name, stationary);
            return new Actor(0, id);
        }
        public static Actor SpawnPrefabActor(string prefab_asset, string name = "Some kind of Actor") {
            Core.InternalCalls.SpawnPrefabActor(0, out ulong id, prefab_asset, name);
            return new Actor(0, id);
        }
        public static Actor GetActor(ulong id) {
            return new Actor(0, id);
        }
        public static Actor GetActor(string tag) {
            Core.InternalCalls.GetActorByTag(0, out ulong id, tag);
            return new Actor(0, id);
        }
        public static T GetActorPrefab<T>(Actor actor) where T : Actor {
            System.Type type = typeof(T);
            Core.InternalCalls.Actor_GetPrefabInterface(actor.ScopeID, actor.ID, type.ToString(), out object actorInstance);
            if (actorInstance.GetType() == type || actorInstance.GetType().IsSubclassOf(type)) {
                return actorInstance as T;
            }
            return null;
        }
        public static T GetActorPrefab<T>(string tag) where T : Actor {
            System.Type type = typeof(T);
            Core.InternalCalls.GetActorByTag(0, out ulong id, tag);
            Core.InternalCalls.Actor_GetPrefabInterface(0, id, type.ToString(), out object actorInstance);
            if (actorInstance.GetType() == type || actorInstance.GetType().IsSubclassOf(type)) {
                return actorInstance as T;
            }
            return null;
        }
        public static void KillActor(ulong id) {
            Core.InternalCalls.KillActor(0, id);
        }

        public static void LoadLevel(string levelPath) {
            Core.InternalCalls.Level_Load(levelPath);
        }

        protected static float simulationSpeed = 1.0f;

        virtual protected void BeginEvent() {

        }
        virtual protected void PreTickEvent(float dt) {

        }
        virtual protected void PostTickEvent(float dt) {

        }
        virtual protected void EndEvent() {

        }
    }
}
