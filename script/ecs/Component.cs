﻿using Shard3D.Library.Types;

namespace Shard3D.Core
{
    public abstract class Component
    {
        public Actor _Actor { get; internal set; }
    }
}