﻿using System;
using Shard3D.Core;

namespace Shard3D
{
    public class Actor
    {
        protected Actor() { 
            ID = 1; 
        }
        internal Actor(ulong _bpInst, ulong _id) {
            ScopeID = _bpInst;
            ID = _id;
        }

        public readonly ulong ID;
        public readonly ulong ScopeID;

        public bool HasComponent<T>() where T : Core.Component, new()
        {
            Type componentType = typeof(T);
            return InternalCalls.Actor_HasComponent(ScopeID, ID, componentType);
        }
        public T GetComponent<T>() where T : Core.Component, new()
        {
            if (!HasComponent<T>()) { InternalCalls.Log($"Tried to get component { typeof(T).Name.ToString() } that does not exist!", LogSeverity.Error); return null; }
            T component = new T() { _Actor = this };
            return component;
        }
        public T AddComponent<T>() where T : Component, new()
        {
            if (HasComponent<T>()) { InternalCalls.Log("Tried to add existing component!", LogSeverity.Warn); return null; }
            Type componentType = typeof(T);
            InternalCalls.Actor_AddComponent(ScopeID, ID, componentType);
            T component = new T() { _Actor = this };
            return component;
        }
        public void KillComponent<T>() where T : Component, new()
        {
            if (!HasComponent<T>()) { InternalCalls.Log("Tried to remove component that does not exist!", LogSeverity.Warn); return; }
            Type componentType = typeof(T);
            InternalCalls.Actor_RmvComponent(ScopeID, ID, componentType);
        }

        public Actor GetActor(ulong uuid) {
            InternalCalls.Utils_UUIDCombine(ScopeID, ID, out ulong thisHash);
            return new Actor(thisHash, uuid);
        }
        public T GetPrefab<T>(ulong uuid) where T : Actor, new() {
            System.Type type = typeof(T); 
            InternalCalls.Utils_UUIDCombine(ScopeID, ID, out ulong thisHash);
            InternalCalls.Actor_GetPrefabInterface(thisHash, uuid, type.ToString(), out object actorInstance);
            if (actorInstance.GetType() == type || actorInstance.GetType().IsSubclassOf(type)) {
                return actorInstance as T;
            }
            return null;
        }
        public void Reparent(Actor child) {
            InternalCalls.Actor_Reparent(ScopeID, ID, child.ScopeID, child.ID);
        }

        public Actor CreateEmptyActor(string name = "Some kind of Actor", bool stationary = false) {
            Core.InternalCalls.CreateEmptyActorUnderActor(ScopeID, ID, out ulong scope, out ulong id, name, stationary);
            return new Actor(scope, id);
        }
        public Actor SpawnPrefabActor(string prefab_asset, string name = "Some kind of Actor") {
            Core.InternalCalls.SpawnPrefabActorUnderActor(ScopeID, ID, out ulong scope, out ulong id, prefab_asset, name);
            return new Actor(scope, id);
        }


        public bool Exists() {
            return ID != 1;
        }

        virtual protected void BeginEvent() { }
        virtual protected void TickEvent(float dt) { }
        virtual protected void PhysicsTickEvent(float ts) {  }
        virtual protected void EndEvent() { }
        virtual protected void SpawnEvent() { }
        virtual protected void KillEvent() { }
    }
}

