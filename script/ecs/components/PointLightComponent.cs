﻿using Shard3D.Library.Types;

namespace Shard3D.Components {
    public class PointLightComponent : Core.Component {
        public Float3 Color {
            get {
                Core.InternalCalls.PointLightComponent_GetColor(_Actor.ScopeID, _Actor.ID, out Float3 _color);
                return _color;
            }
            set {
                Core.InternalCalls.PointLightComponent_SetColor(_Actor.ScopeID, _Actor.ID, ref value);
            }
        }
        public float Intensity {
            get {
                Core.InternalCalls.PointLightComponent_GetIntensity(_Actor.ScopeID, _Actor.ID, out float _intensity);
                return _intensity;
            }
            set {
                Core.InternalCalls.PointLightComponent_SetIntensity(_Actor.ScopeID, _Actor.ID, ref value);
            }
        }
    }
}
