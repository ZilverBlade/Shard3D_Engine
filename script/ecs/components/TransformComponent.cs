﻿using Shard3D.Library.Types;
using Shard3D.Core;

namespace Shard3D.Components {
    public class TransformComponent : Core.Component {
        public Float3 GetForward() {
            InternalCalls.TransformComponent_GetTransformVectorForward(_Actor.ScopeID, _Actor.ID, out Float3 _vec);
            return _vec;
        }
        public Float3 GetRight() {
            InternalCalls.TransformComponent_GetTransformVectorRight(_Actor.ScopeID, _Actor.ID, out Float3 _vec);
            return _vec;
        }
        public Float3 GetUp() {
            InternalCalls.TransformComponent_GetTransformVectorUp(_Actor.ScopeID, _Actor.ID, out Float3 _vec);
            return _vec;
        }
        public Float3 GetTranslationWorld() {
            InternalCalls.TransformComponent_GetTransformTranslationWorld(_Actor.ScopeID, _Actor.ID, out Float3 _pos);
            return _pos;
        }
        public Float3 Translation {
            get {
                InternalCalls.TransformComponent_GetTranslation(_Actor.ScopeID, _Actor.ID, out Float3 _translation);
                return _translation;
            }
            set {
                InternalCalls.TransformComponent_SetTranslation(_Actor.ScopeID, _Actor.ID, ref value);
            }
        }
        public Float3 Rotation {
            get {
                InternalCalls.TransformComponent_GetRotation(_Actor.ScopeID, _Actor.ID, out Float3 _rotation);
                return _rotation;
            }
            set {
                InternalCalls.TransformComponent_SetRotation(_Actor.ScopeID, _Actor.ID, ref value);
            }
        }
        public void SetRotationMatrix(Float3x3 matrix) {
            InternalCalls.TransformComponent_SetRotationWithMatrix(_Actor.ScopeID, _Actor.ID, ref matrix);
        }

        public void SetRotationMatrixWorld(Float3x3 matrix) {
            InternalCalls.TransformComponent_SetRotationWithMatrixWorld(_Actor.ScopeID, _Actor.ID, ref matrix);
        }
        public void SetTranslationWorld(Float3 translation) {
            InternalCalls.TransformComponent_SetTransformTranslationWorld(_Actor.ScopeID, _Actor.ID, ref translation);
        }
        //public void SetRotationWorld(Float3 rotation) {
        //    InternalCalls.TransformComponent_SetTransformRotationWorld(_Actor.ScopeID, _Actor.ID, ref rotation);
        //}
        //public void SetScaleWorld(Float3 scale) {
        //    InternalCalls.TransformComponent_SetTransformScaleWorld(_Actor.ScopeID, _Actor.ID, ref scale);
        //}

        public Float3 Scale {
            get {
                InternalCalls.TransformComponent_GetScale(_Actor.ScopeID, _Actor.ID, out Float3 _scale);
                return _scale;
            }
            set {
                InternalCalls.TransformComponent_SetScale(_Actor.ScopeID, _Actor.ID, ref value);
            }
        }
    }
}