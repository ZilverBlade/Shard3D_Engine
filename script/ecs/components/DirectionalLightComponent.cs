﻿using Shard3D.Library.Types;
using Shard3D.Core;

namespace Shard3D.Components {
    public class DirectionalLightComponent : Core.Component {
        public Float3 Color {
            get {
                InternalCalls.DirectionalLightComponent_GetColor(_Actor.ScopeID, _Actor.ID, out Float3 _color);
                return _color;
            }
            set {
                InternalCalls.DirectionalLightComponent_SetColor(_Actor.ScopeID, _Actor.ID, ref value);
            }
        }
        public float Intensity {
            get {
                InternalCalls.DirectionalLightComponent_GetIntensity(_Actor.ScopeID, _Actor.ID, out float _intensity);
                return _intensity;
            }
            set {
                InternalCalls.DirectionalLightComponent_SetIntensity(_Actor.ScopeID, _Actor.ID, ref value);
            }
        }
    }
}
