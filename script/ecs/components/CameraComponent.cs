﻿using Shard3D.Library.Types;
using Shard3D.Core;

namespace Shard3D.Components {

    public class CameraComponent : Core.Component {
        public CameraProjectionType ProjectionType {
            get {
                InternalCalls.CameraComponent_GetProjectionType(_Actor.ScopeID, _Actor.ID, out int _pt);
                return (CameraProjectionType)_pt;
            }
            set {
                int val = (int)value;
                InternalCalls.CameraComponent_SetProjectionType(_Actor.ScopeID, _Actor.ID, ref val);
            }
        }
        public float FOV {
            get {
                InternalCalls.CameraComponent_GetFOV(_Actor.ScopeID, _Actor.ID, out float _fov);
                return _fov;
            }
            set {
                InternalCalls.CameraComponent_SetFOV(_Actor.ScopeID, _Actor.ID, ref value);
            }
        }
        public float NearClipPlane {
            get {
                InternalCalls.CameraComponent_GetNearClip(_Actor.ScopeID, _Actor.ID, out float _nc);
                return _nc;
            }
            set {
                InternalCalls.CameraComponent_SetNearClip(_Actor.ScopeID, _Actor.ID, ref value);
            }
        }

        public float FarClipPlane {
            get {
                InternalCalls.CameraComponent_GetFarClip(_Actor.ScopeID, _Actor.ID, out float _fc);
                return _fc;
            }
            set {
                InternalCalls.CameraComponent_SetFarClip(_Actor.ScopeID, _Actor.ID, ref value);
            }
        }

        public void Possess() {
            InternalCalls.Level_PossessCameraActor(_Actor.ScopeID, _Actor.ID);
        }
    }
}
