﻿using Shard3D.Library.Types;
using Shard3D.Core;

namespace Shard3D.Components {
    public class BoxVolumeComponent : Core.Component {
        public float CalculateInfluence(Float3 point) {
            InternalCalls.BoxVolumeComponent_CalculateInfluence(_Actor.ScopeID, _Actor.ID, ref point, out float val);
            return val;
        }
        public Float3 Bounds {
            get {
                InternalCalls.BoxVolumeComponent_GetBounds(_Actor.ScopeID, _Actor.ID, out Float3 _b);
                return _b;
            }
            set {
                InternalCalls.BoxVolumeComponent_SetBounds(_Actor.ScopeID, _Actor.ID, ref value);
            }
        }
    }
}
