﻿using Shard3D.Library.Types;
using Shard3D.Core;

// either deprecated or needs revision
namespace Shard3D.Components {
    public class AudioComponent : Core.Component {
        public string File {
            get {
                InternalCalls.AudioComponent_GetFile(_Actor.ScopeID, _Actor.ID, out string _f);
                return _f;
            }
            set {
                InternalCalls.AudioComponent_SetFile(_Actor.ScopeID, _Actor.ID, value);
            }
        }
        public AudioProperties Properties {
            get {
                AudioProperties _ap = new AudioProperties();
                InternalCalls.AudioComponent_GetPropertiesVolume(_Actor.ScopeID, _Actor.ID, out _ap.volume);
                InternalCalls.AudioComponent_GetPropertiesPitch(_Actor.ScopeID, _Actor.ID, out _ap.pitch);
                return _ap;
            }
            set {
                InternalCalls.AudioComponent_SetPropertiesVolume(_Actor.ScopeID, _Actor.ID, ref value.volume);
                InternalCalls.AudioComponent_SetPropertiesPitch(_Actor.ScopeID, _Actor.ID, ref value.pitch);
            }
        }
        public void Play() {
            InternalCalls.AudioComponent_Play(_Actor.ScopeID, _Actor.ID);
        }
        public void Stop() {
            InternalCalls.AudioComponent_Stop(_Actor.ScopeID, _Actor.ID);
        }
        public void Pause() {
            InternalCalls.AudioComponent_Pause(_Actor.ScopeID, _Actor.ID);
        }
        public void Resume() {
            InternalCalls.AudioComponent_Resume(_Actor.ScopeID, _Actor.ID);
        }
        public void Update() {
            InternalCalls.AudioComponent_Update(_Actor.ScopeID, _Actor.ID);
        }
    }





    public class BillboardComponent : Core.Component {
        public string File {
            get {
                InternalCalls.BillboardComponent_GetAsset(_Actor.ScopeID, _Actor.ID, out string _f);
                return _f;
            }
            set {
                InternalCalls.BillboardComponent_SetAsset(_Actor.ScopeID, _Actor.ID, value);
            }
        }
    }
}