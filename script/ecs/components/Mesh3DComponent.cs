﻿using Shard3D.Library.Types;
using Shard3D.Core;
using Shard3D.Graphics;

namespace Shard3D.Components {
    public class Mesh3DComponent : Core.Component {
        public string Asset {
            get {
                InternalCalls.Mesh3DComponent_GetAsset(_Actor.ScopeID, _Actor.ID, out string _f);
                return _f;
            }
            set {
                InternalCalls.Mesh3DComponent_SetAsset(_Actor.ScopeID, _Actor.ID, value);
            }
        }
        public bool CastShadows {
            get {
                InternalCalls.Mesh3DComponent_GetCastShadows(_Actor.ScopeID, _Actor.ID, out bool _b);
                return _b;
            }
            set {
                InternalCalls.Mesh3DComponent_SetCastShadows(_Actor.ScopeID, _Actor.ID, value);
            }
        }
        public SurfaceMaterialMeshOwnedArray SurfaceMaterials {
            get {
                InternalCalls.Mesh3DComponent_GetSurfaceMaterialList(_Actor.ScopeID, _Actor.ID, out string[] _arr);
                return new SurfaceMaterialMeshOwnedArray(_arr, _Actor);
            }
        }
    }
}
