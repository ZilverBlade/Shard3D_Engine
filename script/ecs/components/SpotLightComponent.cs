﻿using Shard3D.Library.Types;
using Shard3D.Core;

namespace Shard3D.Components {
    public class SpotLightComponent : Core.Component {
        public Float3 Color {
            get {
                InternalCalls.SpotLightComponent_GetColor(_Actor.ScopeID, _Actor.ID, out Float3 _color);
                return _color;
            }
            set {
                InternalCalls.SpotLightComponent_SetColor(_Actor.ScopeID, _Actor.ID, ref value);
            }
        }
        public float Intensity {
            get {
                InternalCalls.SpotLightComponent_GetIntensity(_Actor.ScopeID, _Actor.ID, out float _intensity);
                return _intensity;
            }
            set {
                InternalCalls.SpotLightComponent_SetIntensity(_Actor.ScopeID, _Actor.ID, ref value);
            }
        }
        public float OuterAngle {
            get {
                InternalCalls.SpotLightComponent_GetOuterAngle(_Actor.ScopeID, _Actor.ID, out float _angle);
                return _angle;
            }
            set {
                InternalCalls.SpotLightComponent_SetOuterAngle(_Actor.ScopeID, _Actor.ID, ref value);
            }
        }
        public float InnerAngle {
            get {
                InternalCalls.SpotLightComponent_GetInnerAngle(_Actor.ScopeID, _Actor.ID, out float _angle);
                return _angle;
            }
            set {
                InternalCalls.SpotLightComponent_SetInnerAngle(_Actor.ScopeID, _Actor.ID, ref value);
            }
        }
    }
}
