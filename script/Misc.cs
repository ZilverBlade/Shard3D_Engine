﻿using System.Runtime.CompilerServices;
using Shard3D.Library.Types;

[assembly: InternalsVisibleTo("Shard3D.Core.InternalCalls")]
namespace Shard3D
{
    public enum LogSeverity
    {
        Debug = 0,
        Info = 1,
        Warn = 2,
        Error = 3
    }
    public enum CameraProjectionType
    {
        Orthographic = 0,
        Perspective = 1
    }
    public struct AudioProperties
    {
        public float volume;
        public float pitch;
        public Float3 relativePos;
    }
}