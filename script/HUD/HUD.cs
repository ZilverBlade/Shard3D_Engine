﻿using Shard3D.Core;

namespace Shard3D.UI {
    public class HUD {
        public HUD(string widget) {
            currentLayer = widget;
        }

        public void Display() {
            InternalCalls.HUDLayer_Display(currentLayer);
        }
        public void Close() {
            InternalCalls.HUDLayer_Close(currentLayer);
        }
        // public void DisplayOnRenderTarget() {
        //
        // }

        public T CreateElement<T>() where T : HUDElement, new() {
            System.Type type = typeof(T);
            InternalCalls.HUDLayer_CreateElement(currentLayer, type.ToString(), out HUDElementData element, out string elementName);
            T ret = new T();
            ret.OverrideConstructor(element, elementName);
            return ret;
        }
        public T GetElement<T>(string elementName) where T : HUDElement, new() {
            InternalCalls.HUDLayer_GetElement(currentLayer, elementName, out HUDElementData element);
            T ret = new T();
            ret.OverrideConstructor(element, elementName);
            return ret;
        }
        public void EraseElement(string elementName) {
            InternalCalls.HUDLayer_EraseElement(currentLayer, elementName);
        }

        private readonly string currentLayer;
    }
}
