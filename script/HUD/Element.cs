﻿using Shard3D.Core;
using Shard3D.Library.Types;

namespace Shard3D.UI {
    public struct HUDElementData {
        public bool Valid() {
            return handle != 0;
        }
        static internal HUDElementData Empty() {
            return new HUDElementData();
        }
        private ulong handle;
    }
    public class HUDElement {
        protected HUDElement() {
            Data = HUDElementData.Empty();
        }
        internal HUDElement(HUDElementData _id, string _name) {
            Data = _id;
            Name = _name;
        }
        internal void OverrideConstructor(HUDElementData _id, string _name) {
            Data = _id;
            Name = _name;
        }

        public string GetName() {
            return Name;
        }

        private HUDElementData Data;
        private string Name;

        virtual protected void ClickEvent() { }
        virtual protected void PressEvent() { }
        virtual protected void HoverEvent() { }

        public bool Visible {
            get {
                InternalCalls.HUDElement_GetVisible(Data, out bool visible);
                return visible;
            }
            set {
                InternalCalls.HUDElement_SetVisible(Data, value);
            }
        }

        public class Line : HUDElement {
            public Float2 PointA {
                get {
                    InternalCalls.HUDElement_Line_GetPointA(Data, out Float2 v);
                    return v;
                }
                set {
                    InternalCalls.HUDElement_Line_SetPointA(Data, ref value);
                }
            }
            public Float2 PointB {
                get {
                    InternalCalls.HUDElement_Line_GetPointB(Data, out Float2 v);
                    return v;
                }
                set {
                    InternalCalls.HUDElement_Line_SetPointB(Data, ref value);
                }
            }
            public float Width {
                get {
                    InternalCalls.HUDElement_Line_GetWidth(Data, out float v);
                    return v;
                }
                set {
                    InternalCalls.HUDElement_Line_SetWidth(Data, ref value);
                }
            }
            public Float4 Color {
                get {
                    InternalCalls.HUDElement_Line_GetColor(Data, out Float4 v);
                    return v;
                }
                set {
                    InternalCalls.HUDElement_Line_SetColor(Data, ref value);
                }
            }
        }
        public class Rect : HUDElement {
            public Float2 Position {
                get {
                    InternalCalls.HUDElement_Rect_GetPosition(Data, out Float2 v);
                    return v;
                }
                set {
                    InternalCalls.HUDElement_Rect_SetPosition(Data, ref value);
                }
            }
            public Float2 Scale {
                get {
                    InternalCalls.HUDElement_Rect_GetScale(Data, out Float2 v);
                    return v;
                }
                set {
                    InternalCalls.HUDElement_Rect_SetScale(Data, ref value);
                }
            }
            public float Rotation {
                get {
                    InternalCalls.HUDElement_Rect_GetRotation(Data, out float v);
                    return v;
                }
                set {
                    InternalCalls.HUDElement_Rect_SetRotation(Data, ref value);
                }
            }
            public Float4 Color {
                get {
                    InternalCalls.HUDElement_Rect_GetColor(Data, out Float4 v);
                    return v;
                }
                set {
                    InternalCalls.HUDElement_Rect_SetColor(Data, ref value);
                }
            }
        }
        public class Image : HUDElement {
            public Float2 Position {
                get {
                    InternalCalls.HUDElement_Image_GetPosition(Data, out Float2 v);
                    return v;
                }
                set {
                    InternalCalls.HUDElement_Image_SetPosition(Data, ref value);
                }
            }
            public Float2 Scale {
                get {
                    InternalCalls.HUDElement_Image_GetScale(Data, out Float2 v);
                    return v;
                }
                set {
                    InternalCalls.HUDElement_Image_SetScale(Data, ref value);
                }
            }
            public float Rotation {
                get {
                    InternalCalls.HUDElement_Image_GetRotation(Data, out float v);
                    return v;
                }
                set {
                    InternalCalls.HUDElement_Image_SetRotation(Data, ref value);
                }
            }
            public Float4 Color {
                get {
                    InternalCalls.HUDElement_Image_GetColor(Data, out Float4 v);
                    return v;
                }
                set {
                    InternalCalls.HUDElement_Image_SetColor(Data, ref value);
                }
            }
            public string Texture {
                get {
                    InternalCalls.HUDElement_Image_GetTexture(Data, out string v);
                    return v;
                }
                set {
                    InternalCalls.HUDElement_Image_SetTexture(Data, value);
                }
            }
        }
        public class Text : HUDElement {
            public Float2 Position {
                get {
                    InternalCalls.HUDElement_Text_GetPosition(Data, out Float2 v);
                    return v;
                }
                set {
                    InternalCalls.HUDElement_Text_SetPosition(Data, ref value);
                }
            }
            public Float2 Scale {
                get {
                    InternalCalls.HUDElement_Text_GetScale(Data, out Float2 v);
                    return v;
                }
                set {
                    InternalCalls.HUDElement_Text_SetScale(Data, ref value);
                }
            }
            public float Rotation {
                get {
                    InternalCalls.HUDElement_Text_GetRotation(Data, out float v);
                    return v;
                }
                set {
                    InternalCalls.HUDElement_Text_SetRotation(Data, ref value);
                }
            }
            public Float4 Color {
                get {
                    InternalCalls.HUDElement_Text_GetColor(Data, out Float4 v);
                    return v;
                }
                set {
                    InternalCalls.HUDElement_Text_SetColor(Data, ref value);
                }
            }
            public string FormatableText {
                get {
                    InternalCalls.HUDElement_Text_GetText(Data, out string v);
                    return v;
                }
                set {
                    InternalCalls.HUDElement_Text_SetText(Data, value);
                }
            }
            public void SetTextFormattingParameter(string param, string value) {
                InternalCalls.HUDElement_Text_FormatTextParam(Data, param, value);
            }
        }

    }


}
