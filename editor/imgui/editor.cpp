#include "editor.h"

#include <imgui.h>
#include <imgui_internal.h>

#include <imgui_internal.h>

#include <miniaudio.h>

#include <Shard3D/ecs.h>
#include <Shard3D/core/misc/graphics_settings.h>
#include <Shard3D/core/asset/assetmgr.h>
#include <Shard3D/core/audio/audio.h>
#include <Shard3D/utils/dialogs.h>

#include <shellapi.h>
#include <glm/gtc/type_ptr.hpp>

#include <ImGuizmo.h>
#include <Shard3D/global.h>
#include <Shard3D/systems/rendering/deferred_render_system.h>
#include <Shard3D/core/ecs/levelmgr.h>
#include <Shard3D/systems/handlers/render_list.h>
#include <Shard3D/systems/handlers/prefab_manager.h>
#include <Shard3D/systems/handlers/material_system.h>
#include <Shard3DEditorKit/tools/reflection_probe_capture.h>
#include <Shard3D/core/ecs/level_simulation.h>
#include <Shard3DEditorKit/gui/icons.h>

namespace Shard3D {

	static void setWindowTitle(GLFWwindow* window, const std::string& text) {
		glfwSetWindowTitle(window,
			(text
#ifndef NDEBUG
				+ " !! DEBUG BUILD !!"
#endif
				).c_str());
	}


	Editor::Editor(S3DDevice& dvc, S3DWindow& wnd, ResourceSystem* resourceSystem, TerrainEditSystem* terrainEditSystem, uPtr<S3DDescriptorSetLayout>& globalSetLayout, uPtr<S3DDescriptorSetLayout>& sceneSetLayout, uPtr<S3DDescriptorSetLayout>& skeletonRigSetLayout, uPtr<S3DDescriptorSetLayout>&  terrainSetLayout, VkRenderPass renderPass, ImGuiContext* ctx)
		: engineDevice(dvc), engineWindow(wnd), context(ctx), resourceSystem(resourceSystem), globalSetLayout(globalSetLayout), sceneSetLayout(sceneSetLayout), skeletonRigSetLayout(skeletonRigSetLayout), terrainSetLayout(terrainSetLayout), terrainEditSystem(terrainEditSystem) {
		std::string title = "Shard3D Engine " + ENGINE_VERSION.toString() + " (Playstate: Stopped)";
		setWindowTitle(engineWindow.getGLFWwindow(), title);

		refreshContext = true;

		// set the default values for the structure from ini file so that you can actually modify them
		
		createIcons();

		AssetReferenceTracker::initialize();

		levelPropertiesPanel.setDevice(engineDevice);
		levelPropertiesPanel.setDescriptorSetLayouts(globalSetLayout, sceneSetLayout, skeletonRigSetLayout, terrainSetLayout);

		materialEditorLevel = make_sPtr<Level>(resourceSystem);
		materialEditorLevel->createSystem();
		{
			LevelManager levelMan(*materialEditorLevel, resourceSystem);
			levelMan.load(AssetID("engine/levels/material_builder_level.s3dlevel").getAbsolute(), true);
		}
		Actor materialActor = materialEditorLevel->getActorFromTag("Material Ball Preview Actor");
		materialActor.getComponent<Components::Mesh3DComponent>().setMaterials(std::vector<AssetID>{ resourceSystem->coreAssets.sm_errorMaterial });
		auto editor_cameraActor = materialEditorLevel->getActorFromUUID(0, 0);
		editor_cameraActor.getTransform().setTranslation({ 0.f, -2.f, 1.f });
		materialEditorLevel->setPossessedCameraActor(materialEditorLevel->getActorFromTag("Material Camera Actor"));

		physicsEditorLevel = make_sPtr<Level>(resourceSystem);
		physicsEditorLevel->createSystem();
		{
			LevelManager levelMan = LevelManager(*physicsEditorLevel, resourceSystem);
			levelMan.load(AssetID("engine/levels/physics_builder_level.s3dlevel").getAbsolute(), true);
		}
		physicsEditorLevel->setPossessedCameraActor(physicsEditorLevel->getActorFromTag("CameraActor"));

		animationPreviewerLevel = make_sPtr<Level>(resourceSystem);
		animationPreviewerLevel->createSystem();
		{
			LevelManager levelMan = LevelManager(*animationPreviewerLevel, resourceSystem);
			levelMan.load(AssetID("engine/levels/animation_viewer_level.s3dlevel").getAbsolute(), true);
		}
		animationPreviewerLevel->setPossessedCameraActor(animationPreviewerLevel->getActorFromTag("Camera Actor"));

		uiPreviewBuilderRenderTarget = new S3DRenderTarget(engineDevice, {
			VK_FORMAT_R8G8B8A8_UNORM,
			VK_IMAGE_ASPECT_COLOR_BIT,
			VK_IMAGE_VIEW_TYPE_2D,
			{ 1280, 720, 1 },
			VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
			S3DRenderTargetType::Color,
			true,
			VK_SAMPLE_COUNT_1_BIT
			}
		);

		AttachmentInfo uiPreviewBdrRTInfo = {};
		uiPreviewBdrRTInfo.framebufferAttachment = uiPreviewBuilderRenderTarget;
		uiPreviewBdrRTInfo.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		uiPreviewBdrRTInfo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		uiPreviewBdrRTInfo.clear.color = { 0.0, 0.0, 0.0, 0.0 };
		uiPreviewBuilderRenderPass = new S3DRenderPass(engineDevice, { uiPreviewBdrRTInfo });
		uiPreviewBuilderFramebuffer = new S3DFramebuffer(engineDevice, uiPreviewBuilderRenderPass, { uiPreviewBuilderRenderTarget });

		SHUD::VulkanContextCreateInfo shudCreateInfo{};
		shudCreateInfo.mDevice = engineDevice.device();
		shudCreateInfo.mPhysicalDevice = engineDevice.physicalDevice();
		shudCreateInfo.mFramebufferFormat = uiPreviewBuilderRenderTarget->getAttachmentDescription().format;
		shudCreateInfo.mMSAASamples = VK_SAMPLE_COUNT_1_BIT;
		shudCreateInfo.mRenderPass = uiPreviewBuilderRenderPass->getRenderPass();
		shudCreateInfo.mSubPass = 0;
		shudCreateInfo.mSwapChainImageCount = ProjectSystem::getGraphicsSettings().renderer.framesInFlight;
		shudCreateInfo.mDrawCullMode = VK_CULL_MODE_NONE;

		uiPreviewBuilderContext = new SHUD::VulkanContext(shudCreateInfo);
		VkCommandBuffer cmd = engineDevice.beginSingleTimeCommands();
		uiPreviewBuilderContext->CreateResources(cmd);
		engineDevice.endSingleTimeCommands(cmd);

		uiPreviewBuilderContext->SetResolution({ 1280, 720 });
		uiPreviewBuilderContext->SetGamma(2.2F);

		AssetID dftx = AssetID("engine/textures/hud/default_btn.png.s3dasset");
		resourceSystem->loadTexture(dftx);
		auto tx = resourceSystem->retrieveTexture(dftx);
		VkDescriptorImageInfo imageInfo;
		imageInfo.sampler = tx->getSampler();
		imageInfo.imageView = tx->getImageView();
		imageInfo.imageLayout = tx->getImageLayout();
		defaultUIPrevTexture = uiPreviewBuilderContext->CreateTexture(imageInfo);

		hudLayerEditorViewport = ImGui_ImplVulkan_AddTexture(uiPreviewBuilderRenderTarget->getSampler(), uiPreviewBuilderRenderTarget->getImageView(), uiPreviewBuilderRenderTarget->getImageLayout());
	}

	Editor::~Editor() {
		AssetReferenceTracker::destruct();

		delete uiPreviewBuilderContext;
		delete uiPreviewBuilderRenderTarget;
		delete uiPreviewBuilderRenderPass;
		delete uiPreviewBuilderFramebuffer;
	}

	static void loadLevel(sPtr<ECS::Level>& level, ResourceSystem* resourceSystem, const AssetID& path) {
		if (level->simulationState != PlayState::Stopped) {
			LevelSimulationState::eventEnd();
			LevelSimulationState::release();
		}
		sPtr<ECS::Level> newlevel = make_sPtr<ECS::Level>(resourceSystem);
		Level::shallowCopy(newlevel,level);
		newlevel->createSystem();
		LevelManager levelMan(*newlevel, resourceSystem);
		LevelMgrResults result = levelMan.load(path.getAbsolute(), false);
		if (result == LevelMgrResults::OldEngineVersionResult) {
			if (MessageDialogs::show("This level was created in an different version of Shard3D, results may be unexpected.\nWould you like to try and load the level anyway?", "WARNING!", MB_YESNO | MB_ICONWARNING | MB_DEFBUTTON2) == IDYES) {
				levelMan.load(path.getAbsolute(), true);
			}
			else {
				levelMan.load(level->levelAsset.getAbsolute(), true);
			}
		}
		level.swap(newlevel);
	}
	static void saveLevel(sPtr<ECS::Level>& level, ResourceSystem* resourceSystem, const std::string& path) {
		if (level->simulationState != PlayState::Stopped) { SHARD3D_ERROR_E("Cannot save level that is running!"); return; }
		ECS::LevelManager levelMan(*level, resourceSystem);
		levelMan.save(path, true);
		level->registry.each([&](auto actorGUID) { Actor actor = { actorGUID, level.get() };
		if (actor.hasComponent<Components::Mesh3DComponent>()) {
			auto& mc = actor.getComponent<Components::Mesh3DComponent>();
			AssetReferenceTracker::addReference(level->levelAsset, mc.asset);
			for (auto& material : mc.getMaterials()) AssetReferenceTracker::addReference(level->levelAsset, material);
		}
		if (actor.hasComponent<Components::BillboardComponent>()) {
			AssetReferenceTracker::addReference(level->levelAsset, actor.getComponent<Components::BillboardComponent>().asset);
		}
		});
	}

	static void playLevel(sPtr<ECS::Level>& level, sPtr<ECS::Level>& capturedLevel, GLFWwindow* window) {
		for (auto actor_ : level->registry.view<Components::TerrainComponent>()) {
			Actor actor = { actor_, level.get() };
			if (actor.hasComponent<Components::Rigidbody3DComponent>()) {
				if (!actor.getComponent<Components::TerrainComponent>().physData) {
					MessageDialogs::show("Terrain physics data must be rebuilt", "Error", MessageDialogs::OPTOK | MessageDialogs::OPTICONERROR);
					return;
				}
			}
		}
		SHARD3D_INFO_E("Capturing level");
		bool success = LevelSimulationState::instantiate(level, PlayState::Playing);
		if (!success) {
			MessageDialogs::show("Failed to instantiate play state for level. Please stop any other simulations before beginning a new one.", "Play error", MessageDialogs::OPTICONERROR);
			return;
		}
		capturedLevel = Level::copy(level);
		LevelSimulationState::eventBegin();

		std::string title = "Shard3D Engine " + ENGINE_VERSION.toString() + " (Playstate: PLAYING) | " + IOUtils::getFileOnly(level->levelAsset.getAsset());
		setWindowTitle(window, title);
		
		level->registry.view<Components::TerrainComponent>().each([&](Components::TerrainComponent terr){
			if (!terr.physData) {
				MessageDialogs::show("Terrain Actor does not have generated physics heightfield.", "Physics Warning", MessageDialogs::OPTOK | MessageDialogs::OPTICONEXCLAMATION);
			}
		});

	}
	static void pauseLevel(sPtr<ECS::Level>& level, GLFWwindow* window) {
		level->simulationState = PlayState::Paused;
		level->simulationStateCallback();
		std::string title = "Shard3D Engine " + ENGINE_VERSION.toString() + " (Playstate: Paused) | " + IOUtils::getFileOnly(level->levelAsset.getAsset());
		setWindowTitle(window, title);
	}
	static void resumeLevel(sPtr<ECS::Level>& level, GLFWwindow* window) {
		level->simulationState = PlayState::Playing;
		level->simulationStateCallback();
		std::string title = "Shard3D Engine " + ENGINE_VERSION.toString() + " (Playstate: PLAYING) | " + IOUtils::getFileOnly(level->levelAsset.getAsset());
		setWindowTitle(window, title);
	}
	static void stopLevel(sPtr<ECS::Level>& level, sPtr<ECS::Level>& capturedLevel, GLFWwindow* window, TerrainEditSystem* terrainEditSystem) {
		LevelSimulationState::eventEnd();
		LevelSimulationState::release();
		SHARD3D_INFO_E("Loading back Captured level");
		level.swap(capturedLevel);
		capturedLevel = nullptr;
		level->setPossessedCameraActor(level->getActorFromUUID(0, 0));
		for (auto actor_ : level->registry.view<Components::TerrainComponent>()) {
			Actor actor = { actor_, level.get() };
			//delete actor.getComponent<Components::TerrainComponent>().physData;
			//actor.getComponent<Components::TerrainComponent>().physData = nullptr;
			terrainEditSystem->editingActor = actor;
		}
		std::string title = "Shard3D Engine " + ENGINE_VERSION.toString() + " (Playstate: Stopped) | " + IOUtils::getFileOnly(level->levelAsset.getAsset());
		setWindowTitle(window, title);
	}

	static void previewToggleCall(sPtr<ECS::Level>& level, bool preview) {
		if (preview) {
			level->hideAllEditorOnlyActors();
		}
		else {
			level->showAllEditorOnlyActors();
		}
	}

	void Editor::createIcons() {
		
	}

	void Editor::setRenderTargetData(CameraRenderInstance* tgInst) {
		vkDeviceWaitIdle(engineDevice.device());
		if (gbuffer0Dat.tex)
			ImGui_ImplVulkan_RemoveTexture(gbuffer0Dat.tex);
		if (gbuffer1Dat.tex)
			ImGui_ImplVulkan_RemoveTexture(gbuffer1Dat.tex);
		if (gbuffer2Dat.tex)
			ImGui_ImplVulkan_RemoveTexture(gbuffer2Dat.tex);
		if (gbuffer3Dat.tex)
			ImGui_ImplVulkan_RemoveTexture(gbuffer3Dat.tex);
		if (depthDat.tex)
			ImGui_ImplVulkan_RemoveTexture(depthDat.tex);
		if (ssaoDat.tex)
			ImGui_ImplVulkan_RemoveTexture(ssaoDat.tex);
		if (wblendcolaccumDat.tex)
			ImGui_ImplVulkan_RemoveTexture(wblendcolaccumDat.tex);
		if (wblendcolrevealDat.tex)
			ImGui_ImplVulkan_RemoveTexture(wblendcolrevealDat.tex);
		if (lbuffer0Dat.tex)
			ImGui_ImplVulkan_RemoveTexture(lbuffer0Dat.tex);
		if (lbuffer1Dat.tex)
			ImGui_ImplVulkan_RemoveTexture(lbuffer1Dat.tex);
		auto* dfrs = tgInst->getRenderer();

		SceneBufferInputData sbid = tgInst->getGBufferImages();
		DecalBufferInputData dbid = tgInst->getDBufferImages();
		LightingBufferInputData lbid = tgInst->getLBufferImages();
		gbuffer0Dat = sbid.gbuffer0;
		gbuffer1Dat = sbid.gbuffer1;						   
		gbuffer2Dat = sbid.gbuffer2;						   
		gbuffer3Dat = sbid.gbuffer3;				   
		depthDat = sbid.depthSceneInfo;
		if (dfrs->getRendererFeatures().enableSSAO) {
			ssaoDat = dfrs->getSSAOImageAttachment();
		}
		wblendcolaccumDat = dfrs->getWBColAccumAttachment();
		wblendcolrevealDat = dfrs->getWBColRevealAttachment();
		velocityDat = tgInst->getVelocityBuffer();

		dbuffer0Dat = dbid.dbuffer0;
		dbuffer1Dat = dbid.dbuffer1;
		dbuffer2Dat = dbid.dbuffer2;

		lbuffer0Dat = lbid.lbuffer0;
		lbuffer1Dat = lbid.lbuffer1;

		gbuffer0Dat.what = "Diffuse RGB + Reflectivity";
		gbuffer1Dat.what = "Specular + Glossiness/256";
		gbuffer2Dat.what = "Emission RGB";
		gbuffer3Dat.what = "Normal XYZ";
		depthDat.what = "Depth Buffer";
		ssaoDat.what = "SSAO";
		wblendcolaccumDat.what = "Transparency Accumulation";
		wblendcolrevealDat.what = "Transparency Revealage";
		velocityDat.what = "Velocity Buffer";

		dbuffer0Dat.what = "Diffuse RGB + Opacity";
		dbuffer1Dat.what = "Spec + Shin/256 + Chro + Opacity";
		dbuffer2Dat.what = "Normal XYZ (Tg. Sp.) + Opacity";

		lbuffer0Dat.what = "Diffuse Lighting RGB";
		lbuffer1Dat.what = "Specular Lighting RGB";

		ssaoDat.resourceSizeKB *= 4; // 4 SSAO images due to 1: gen, 2: blurH, 3: blurV, 4: process
	}

	void Editor::detach() {
		vkDeviceWaitIdle(engineDevice.device());
		vkDestroyDescriptorPool(engineDevice.device(), ImGuiInitializer::imGuiDescriptorPool, nullptr);
		ImGui_ImplVulkan_Shutdown();
		ImGui_ImplGlfw_Shutdown();
		ImGui::DestroyContext();
		levelTreePanel.destroyContext();
		levelPropertiesPanel.destroyContext();
		levelPeekPanel.destroyContext();
	}

	void Editor::renderCameras(VkCommandBuffer commandBuffer, uint32_t frameIndex, VkFence fence, FrameInfo2& frameInfo) {
		if (isOpenMaterialEditor) {
			materialEditorLevel->runGarbageCollector();
			materialEditorLevel->rebuildTransforms();
			Actor cameraActor = materialEditorLevel->getActorFromTag("Material Camera Actor");
			materialEditorRenderViewport->updateCamera(materialEditorLevel, cameraActor, materialViewportAR);
			materialEditorRenderViewport->renderEarlyZ(commandBuffer, materialEditorLevelRenderInstance->getDescriptorSet(frameIndex), fence, frameIndex, frameInfo.frameTime, cameraActor, materialEditorLevel, materialEditorLevelRenderInstance->getRenderObjects());
			materialEditorLevelRenderInstance->runGarbageCollector(frameIndex, fence, materialEditorLevel);
			materialEditorLevelRenderInstance->render(commandBuffer, frameIndex, materialEditorLevel);
			materialEditorRenderViewport->render(commandBuffer, materialEditorLevelRenderInstance->getDescriptorSet(frameIndex), fence, frameIndex, frameInfo.frameTime, cameraActor, materialEditorLevel, materialEditorLevelRenderInstance->getRenderObjects(), *materialEditorLevelRenderInstance->getBuffer(frameIndex));
			materialEditorLevelRenderInstance->getRenderObjects().renderList->updateDynamicTransformBufferData(frameIndex);
		}
		if (isOpenPhysicsEditor) {
			physicsEditorLevel->runGarbageCollector();
			physicsEditorLevel->rebuildTransforms();
			Actor cameraActor = physicsEditorLevel->getActorFromTag("CameraActor");
			physicsEditorRenderViewport->updateCamera(physicsEditorLevel, cameraActor, physicsViewportAR);
			physicsEditorRenderViewport->renderEarlyZ(commandBuffer, physicsEditorLevelRenderInstance->getDescriptorSet(frameIndex), fence, frameIndex, frameInfo.frameTime, cameraActor, physicsEditorLevel, physicsEditorLevelRenderInstance->getRenderObjects());
			physicsEditorLevelRenderInstance->runGarbageCollector(frameIndex, fence, physicsEditorLevel);
			physicsEditorLevelRenderInstance->render(commandBuffer, frameIndex, physicsEditorLevel);
			physicsEditorRenderViewport->render(commandBuffer, physicsEditorLevelRenderInstance->getDescriptorSet(frameIndex), fence, frameIndex, frameInfo.frameTime, cameraActor, physicsEditorLevel, physicsEditorLevelRenderInstance->getRenderObjects(), *physicsEditorLevelRenderInstance->getBuffer(frameIndex));
			physicsEditorLevelRenderInstance->getRenderObjects().renderList->updateDynamicTransformBufferData(frameIndex);
		}
		if (isOpenAnimationPreviewer) {
			animationPreviewerLevel->runGarbageCollector();
			animationPreviewerLevel->rebuildTransforms();
			Actor cameraActor = animationPreviewerLevel->getActorFromTag("Camera Actor");
			animationPreviewerRenderViewport->updateCamera(animationPreviewerLevel, cameraActor, animationPreviewerAR);
			animationPreviewerRenderViewport->renderEarlyZ(commandBuffer, animationPreviewerLevelRenderInstance->getDescriptorSet(frameIndex), fence, frameIndex, frameInfo.frameTime, cameraActor, animationPreviewerLevel, animationPreviewerLevelRenderInstance->getRenderObjects());
			animationPreviewerLevelRenderInstance->runGarbageCollector(frameIndex, fence, animationPreviewerLevel);
			animationPreviewerLevelRenderInstance->render(commandBuffer, frameIndex, animationPreviewerLevel);
			animationPreviewerRenderViewport->render(commandBuffer, animationPreviewerLevelRenderInstance->getDescriptorSet(frameIndex), fence, frameIndex, frameInfo.frameTime, cameraActor, animationPreviewerLevel, animationPreviewerLevelRenderInstance->getRenderObjects(), *animationPreviewerLevelRenderInstance->getBuffer(frameIndex));
			animationPreviewerLevelRenderInstance->getRenderObjects().renderList->updateDynamicTransformBufferData(frameIndex);
			animationPreviewerLevelRenderInstance->getRenderObjects().renderList->updateDynamicRiggingBufferData(animationPreviewerLevel, frameIndex);
		}
	}

	void Editor::render(FrameInfo2& frameInfo, LevelRenderInstance* levelRenderInstance) {
		if (refreshContext) {
			levelTreePanel.setContext(frameInfo.level, frameInfo.resourceSystem);
			levelPropertiesPanel.setContext(frameInfo.level, mainLevelRenderInstance, frameInfo.resourceSystem);
			levelPeekPanel.setContext(frameInfo.level, frameInfo.resourceSystem);
			assetExplorerPanel.setContext(frameInfo.level, frameInfo.resourceSystem);
			refreshContext = false;
		}
		engineWindow.setEventCallback(std::bind(&Editor::eventEvent, this, std::placeholders::_1));

		ImGuiIO& io = ImGui::GetIO();
		io.DeltaTime = frameInfo.frameTime;

		glfwGetWindowSize(engineWindow.getGLFWwindow(), &width, &height);
		ImGui::GetIO().DisplaySize = ImVec2(width, height);
		ImGui::SetCurrentContext(context);

		static bool visible = true;

		//ImGui::ShowDemoWindow(&visible);
		
#pragma region boilerplate dockspace code    
		static bool opt_fullscreen = true;
		static bool opt_padding = true;
		ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.f, 0.f, 0.f, 0.f));
		static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;
		// We are using the ImGuiWindowFlags_NoDocking flag to make the parent window not dockable into,
		// because it would be confusing to have two docking targets within each others.
		ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
		if (opt_fullscreen) {
			const ImGuiViewport* viewport = ImGui::GetMainViewport();
			ImGui::SetNextWindowPos(viewport->WorkPos);
			ImGui::SetNextWindowSize(viewport->WorkSize);
			ImGui::SetNextWindowViewport(viewport->ID);
			ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
			ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
			window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
			window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
		}
		else { dockspace_flags &= ~ImGuiDockNodeFlags_PassthruCentralNode; }
		if (dockspace_flags & ImGuiDockNodeFlags_PassthruCentralNode)
			window_flags |= ImGuiWindowFlags_NoBackground;
		if (!opt_padding)
			ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
		ImGui::Begin("MyDockSpace", &visible, window_flags);
		if (!opt_padding)
			ImGui::PopStyleVar();
		if (opt_fullscreen)
			ImGui::PopStyleVar(2);
#pragma endregion
		// Submit the DockSpace
		if (io.ConfigFlags & ImGuiConfigFlags_DockingEnable) {
			ImGuiID dockspace_id = ImGui::GetID("MyDockSpace");
			ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);
		}

		if (ImGui::BeginMenuBar()) {
			renderMenuBar(frameInfo);
			ImGui::EndMenuBar();
		}
		ImGui::PopStyleColor();
		renderQuickBar(frameInfo, levelRenderInstance);

		renderViewport(frameInfo);
		if (isOpenMaterialEditor) {
			renderMaterialEditorViewport(frameInfo);
		}

		if (isOpenPhysicsEditor) {
			renderPhysicsEditorViewport(frameInfo);
		}
		if (isOpenAnimationPreviewer) {
			renderAnimationPreviewerViewport(frameInfo);
		}
		if (isOpenTextureEditor) {
			textureConfigPanel.render(frameInfo, isOpenTextureEditor);
			if (!isOpenTextureEditor) {
				closeTextureConfigEditor();
			}
		}
		if (isOpenHUDEditor) {
			renderHUDEditorViewport(frameInfo);
		}

		// start rendering stuff here
		componentActorPanels.render();
		//materialBuilder.render(frameInfo);
		levelTreePanel.render();
		levelPropertiesPanel.render(&levelTreePanel.getSelectedActor());
		levelPeekPanel.render(frameInfo);
		assetExplorerPanel.render();
		settingsPanel.render();


		if (assetExplorerPanel.wantsTextureEditor) {
			showTextureConfigEditor(assetExplorerPanel.wantsTextureEditor);
		}
		if (assetExplorerPanel.wantsMaterialEditor) {
			showMaterialEditor(assetExplorerPanel.wantsMaterialEditor);
		}
		if (assetExplorerPanel.wantsPhysicsEditor) {
			showPhysicsEditor(assetExplorerPanel.wantsPhysicsEditor);
		}
		if (assetExplorerPanel.wantsHUDEditor) {
			showHUDEditor(assetExplorerPanel.wantsHUDEditor);
		}
		if (assetExplorerPanel.wantsAnimationPreviewer) {
			showSkeletalAnimationPreviewer(assetExplorerPanel.wantsAnimationPreviewer);
		}

		if (showCredits) {
			ImGui::Begin("Credits", &showCredits);
			if (ImGui::TreeNodeEx("Programming", ImGuiTreeNodeFlags_DefaultOpen)) {
				ImGui::Text("ZilverBlade (Lead)");
				ImGui::TreePop();
			}
			if (ImGui::TreeNodeEx("Graphics", ImGuiTreeNodeFlags_DefaultOpen)) {
				ImGui::Text("ZilverBlade (Lead)");
				ImGui::TreePop();
			}
			if (ImGui::TreeNodeEx("Editor Design", ImGuiTreeNodeFlags_DefaultOpen)) {
				ImGui::Text("ZilverBlade (Layout and Colours)");
				ImGui::Text("Dhibaid (Icons)");
				ImGui::TreePop();
			}
			if (ImGui::TreeNodeEx("Special thanks", ImGuiTreeNodeFlags_DefaultOpen)) {
				ImGui::Text("Brendan Galea (Vulkan tutorial series)");
				// bye cherno [deprecated :(] ImGui::Text("The Cherno (Game engine series)");
				ImGui::Text("All of the creators of the used libraries");
				ImGui::TreePop();
			}
			ImGui::End();
		}

		if (showStylizersWindow) {
			ImGui::Begin("Stylizer");
			ImGui::ShowStyleEditor();
			ImGui::End();
		}
		if (showDemoWindow) ImGui::ShowDemoWindow();

		ImGui::End();

	}

	void Editor::draw(VkCommandBuffer commandBuffer) {
		ImGui::UpdatePlatformWindows();
		ImGui::RenderPlatformWindowsDefault();
		ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(), commandBuffer);
	}

	void Editor::drawHUDPreview(VkCommandBuffer commandBuffer, uint32_t frameIndex) {
		uiPreviewBuilderRenderPass->beginRenderPass(commandBuffer, uiPreviewBuilderFramebuffer);
		SHUD::VulkanFrameInfo vkData{};
		vkData.mCommandBuffer = commandBuffer;
		vkData.mFrameIndex = frameIndex;
		uiPreviewBuilderContext->Render(&vkData);
		uiPreviewBuilderRenderPass->endRenderPass(commandBuffer);
	}
	void Editor::setHoveredObject(uint32_t handle, Level* level) {
		hoveredActor = { (entt::entity)handle, level };
	}

	void Editor::resize(glm::vec2 newSize) {
		materialEditorRenderViewport->resize(newSize);
	}

	void Editor::showMaterialEditor(AssetID asset) {
		if (isOpenMaterialEditor) {
			materialBuilderPanel.setMaterialAsset(resourceSystem, asset);
			return;
		}
		isOpenMaterialEditor = true;
		materialEditorLevelRenderInstance = make_uPtr<LevelRenderInstance>(engineDevice, resourceSystem, globalSetLayout->getDescriptorSetLayout(), sceneSetLayout, skeletonRigSetLayout, terrainSetLayout, ProjectSystem::getGraphicsSettings().renderer.framesInFlight);

		DeferredRenderSystem::RendererFeatures rendererFeatures{};
		rendererFeatures.enableSSAO = ProjectSystem::getEngineSettings().postfx.enableSSAO;
		rendererFeatures.enableEarlyZ = ProjectSystem::getEngineSettings().renderer.earlyZMode != ESET::EarlyDepthMode::Off;
		rendererFeatures.enableTranslucency = true;
		rendererFeatures.enableNormalDecals = true;

		CameraRenderInstance::RendererFeatures rtiRendererFeatures{};
		rtiRendererFeatures.deferredRendererFeatures = rendererFeatures;
		rtiRendererFeatures.enableMotionBlur = ProjectSystem::getEngineSettings().postfx.enableMotionBlur;
		rtiRendererFeatures.enablePostFX = true;
		rtiRendererFeatures.enableDecals = true;
		materialEditorRenderViewport = make_uPtr<CameraRenderInstance>(engineDevice, resourceSystem, rtiRendererFeatures, glm::vec2{ 640, 480 }, globalSetLayout, sceneSetLayout->getDescriptorSetLayout(), skeletonRigSetLayout->getDescriptorSetLayout(), terrainSetLayout->getDescriptorSetLayout(), ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		materialEditorViewport = ImGui_ImplVulkan_AddTexture(materialEditorRenderViewport->getFinalImage()->getDescriptor().sampler, materialEditorRenderViewport->getFinalImage()->getDescriptor().imageView, materialEditorRenderViewport->getFinalImage()->getDescriptor().imageLayout);
		materialBuilderPanel = MaterialBuilderPanel(resourceSystem, materialEditorLevel, asset);
	}

	void Editor::closeMaterialEditor() {
		vkDeviceWaitIdle(engineDevice.device());
		isOpenMaterialEditor = false;
		materialEditorLevelRenderInstance = nullptr;
		materialEditorRenderViewport = nullptr;
		materialEditorViewport = nullptr;
		materialBuilderPanel.~MaterialBuilderPanel();
	}

	void Editor::showPhysicsEditor(AssetID asset) {
		if (isOpenPhysicsEditor) return;
		isOpenPhysicsEditor = true;
	
		physicsEditorLevelRenderInstance = make_uPtr<LevelRenderInstance>(engineDevice, resourceSystem, globalSetLayout->getDescriptorSetLayout(), sceneSetLayout, skeletonRigSetLayout, terrainSetLayout, ProjectSystem::getGraphicsSettings().renderer.framesInFlight);

		DeferredRenderSystem::RendererFeatures rendererFeatures{};
		rendererFeatures.enableSSAO = ProjectSystem::getEngineSettings().postfx.enableSSAO;
		rendererFeatures.enableEarlyZ = ProjectSystem::getEngineSettings().renderer.earlyZMode != ESET::EarlyDepthMode::Off;
		rendererFeatures.enableTranslucency = true;
		rendererFeatures.enableNormalDecals = false;

		CameraRenderInstance::RendererFeatures rtiRendererFeatures{};
		rtiRendererFeatures.deferredRendererFeatures = rendererFeatures;
		rtiRendererFeatures.enableMotionBlur = ProjectSystem::getEngineSettings().postfx.enableMotionBlur;
		rtiRendererFeatures.enablePostFX = true;
		rtiRendererFeatures.enableDecals = false;
		physicsEditorRenderViewport = make_uPtr<CameraRenderInstance>(engineDevice, resourceSystem, rtiRendererFeatures, glm::vec2{ 640, 480 }, globalSetLayout, sceneSetLayout->getDescriptorSetLayout(), skeletonRigSetLayout->getDescriptorSetLayout(), terrainSetLayout->getDescriptorSetLayout(), ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		physicsEditorViewport = ImGui_ImplVulkan_AddTexture(physicsEditorRenderViewport->getFinalImage()->getDescriptor().sampler, physicsEditorRenderViewport->getFinalImage()->getDescriptor().imageView, physicsEditorRenderViewport->getFinalImage()->getDescriptor().imageLayout);
		physicsBuilderPanel = PhysicsBuilderPanel(physicsEditorLevel, asset, AssetID::null());

		physicsActorDisplayRenderer = make_uPtr<PhysicsActorPreviewRenderer>(engineDevice, physicsEditorRenderViewport->getRenderer()->getTransparencyRenderPass()->getRenderPass(), globalSetLayout->getDescriptorSetLayout());
		physicsActorHullPointRenderer = make_uPtr<PointRenderer>(engineDevice, physicsEditorRenderViewport->getRenderer()->getTransparencyRenderPass()->getRenderPass(), globalSetLayout->getDescriptorSetLayout());
		physicsEditorRenderViewport->addForwardRenderCall([&](FrameInfo& frameInfo, void*) {
			physicsActorDisplayRenderer->renderWireframe(frameInfo);
			auto& asset = frameInfo.level->getActorFromTag("PhysicsActor").getComponent<Components::Rigidbody3DComponent>().asset;
			glm::vec3 translation = frameInfo.level->getActorFromTag("PhysicsActor").getTransform().transformMatrix[3];
			if (frameInfo.level->getActorFromTag("FLAG_EDITOR.DisplayPoints").isVisible()) {
				std::vector<PointRenderInfo> pts{};
				glm::vec3 selectedPoint = frameInfo.level->getActorFromTag("FLAG_EDITOR.GizmoActivePoint").getTransform().transformMatrix[3];
				if (frameInfo.level->getActorFromTag("FLAG_EDITOR.DisplayPoints").isVisible()) {
					for (auto& point : asset.convexPoints) {
						PointRenderInfo prf;
						prf.position = point + translation;

						glm::vec4 proj = frameInfo.camera.getProjectionView() * glm::vec4(prf.position, 1.0f);
						proj = proj / proj.w;
						proj.y *= -1.0f;
						glm::vec2 real = (glm::vec2(proj) * 0.5f + 0.5f) * glm::vec2(640.f, 480.f);
						glm::vec2 cursor = glm::vec2(frameInfo.level->getActorFromTag("FLAG_EDITOR.CursorPositionABS").getTransform().transformMatrix[3]);

						if (glm::distance(prf.position, selectedPoint) < 0.001f) {
							prf.color = { 0.5f, 1.0f, 0.0f, 1.0f };
							prf.size = 32.0f;
							prf.weight = 30.0f;
						} else if (glm::distance(cursor, real) < 12.0f) {
							prf.color = { 1.0f, 0.9f, 0.0f, 0.85f };
							prf.size = 16.0f;
							prf.weight = 10.0f;
						} else {
							prf.color = { 1.0f, 0.0f, 0.5f, 0.75f };
							prf.size = 16.0f;
							prf.weight = 1.0f;
						}
						pts.push_back(prf);
					}
				}
				physicsActorHullPointRenderer->render(frameInfo, pts);
			}
		}, nullptr);
	}

	void Editor::closePhysicsEditor() {
		vkDeviceWaitIdle(engineDevice.device());
		isOpenPhysicsEditor = false;
		physicsEditorLevelRenderInstance = nullptr;
		physicsEditorRenderViewport = nullptr;
		physicsEditorViewport = nullptr;
		physicsActorDisplayRenderer = nullptr;
		physicsActorHullPointRenderer = nullptr;
		physicsBuilderPanel.~PhysicsBuilderPanel();
	}

	void Editor::showHUDEditor(AssetID asset) {
		hudBuilderPanel = HUDBuilderPanel(resourceSystem, asset);
		isOpenHUDEditor = true;
	}

	void Editor::closeHUDEditor() {
		isOpenHUDEditor = false;
		hudBuilderPanel.~HUDBuilderPanel();
	}

	void Editor::showSkeletalAnimationPreviewer(AssetID asset) {
		AssetID skeleton = ResourceSystem::discoverAttachedSkeletalMeshAsset(asset);
		AssetID mesh = ResourceSystem::discoverAttachedMeshAsset(skeleton);
		if (isOpenAnimationPreviewer) {
			if (animationPreviewPanel.getCurrentMeshAsset() != mesh) {
				animationPreviewPanel.setMeshAsset(resourceSystem, mesh, skeleton);
			}
			animationPreviewPanel.setAnimationAsset(resourceSystem, asset);
			return;
		}
		isOpenAnimationPreviewer = true;
		animationPreviewerLevelRenderInstance = make_uPtr<LevelRenderInstance>(engineDevice, resourceSystem, globalSetLayout->getDescriptorSetLayout(), sceneSetLayout, skeletonRigSetLayout, terrainSetLayout, ProjectSystem::getGraphicsSettings().renderer.framesInFlight);

		DeferredRenderSystem::RendererFeatures rendererFeatures{};
		rendererFeatures.enableSSAO = ProjectSystem::getEngineSettings().postfx.enableSSAO;
		rendererFeatures.enableEarlyZ = ProjectSystem::getEngineSettings().renderer.earlyZMode != ESET::EarlyDepthMode::Off;
		rendererFeatures.enableTranslucency = true;
		rendererFeatures.enableNormalDecals = false;

		CameraRenderInstance::RendererFeatures rtiRendererFeatures{};
		rtiRendererFeatures.deferredRendererFeatures = rendererFeatures;
		rtiRendererFeatures.enableMotionBlur = ProjectSystem::getEngineSettings().postfx.enableMotionBlur;
		rtiRendererFeatures.enablePostFX = true;
		rtiRendererFeatures.enableDecals = false;
		animationPreviewerRenderViewport = make_uPtr<CameraRenderInstance>(engineDevice, resourceSystem, rtiRendererFeatures, glm::vec2{ 640, 480 }, globalSetLayout, sceneSetLayout->getDescriptorSetLayout(), skeletonRigSetLayout->getDescriptorSetLayout(), terrainSetLayout->getDescriptorSetLayout(), ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		animationPreviewerViewport = ImGui_ImplVulkan_AddTexture(animationPreviewerRenderViewport->getFinalImage()->getDescriptor().sampler, animationPreviewerRenderViewport->getFinalImage()->getDescriptor().imageView, animationPreviewerRenderViewport->getFinalImage()->getDescriptor().imageLayout);
		animationPreviewPanel = MeshAnimationPreviewPanel(resourceSystem, animationPreviewerLevel, mesh, skeleton, asset);
	}

	void Editor::closeSkeletalAnimationPreviewer() {
		vkDeviceWaitIdle(engineDevice.device());
		isOpenAnimationPreviewer = false;
		animationPreviewerLevelRenderInstance = nullptr;
		animationPreviewerRenderViewport = nullptr;
		animationPreviewerViewport = nullptr;
		animationPreviewPanel.~MeshAnimationPreviewPanel();
	}

	void Editor::showTextureConfigEditor(AssetID asset) {
		isOpenTextureEditor = true;
		textureConfigPanel = TextureConfigPanel(resourceSystem, asset);
	}

	void Editor::closeTextureConfigEditor() {
		textureConfigPanel.~TextureConfigPanel();
		isOpenTextureEditor = false;
	}

	void Editor::renderViewport(FrameInfo2& frameInfo) {
		ImGui::Begin("Viewport");

		int wposx, wposy;
		glfwGetWindowPos(engineWindow.getGLFWwindow(), &wposx, &wposy);
		ImVec2 vSize = ImGui::GetContentRegionAvail();
		GraphicsSettings2::getRuntimeInfo().aspectRatio = vSize.x / vSize.y;
		GraphicsSettings2::getRuntimeInfo().localScreen = { ImGui::GetWindowPos().x - wposx, ImGui::GetWindowPos().y - wposy, ImGui::GetContentRegionAvail().x,ImGui::GetContentRegionAvail().y};
		
		RenderTargetMetadataInfo vpImage = {};
		vpImage.tex = viewportImage;
		ImVec2 vpUV0 = ImVec2(0.0, 1.0);
		ImVec2 vpUV1 = ImVec2(1.0, 0.0);
		switch (renderpassRadioButton) {
		case (0): vpUV0 = ImVec2(0.0, 0.0);
			vpUV1  = ImVec2(1.0, 1.0);
			break;
		case (1): vpImage = depthDat; break;
		case (2): vpImage = gbuffer0Dat; break;
		case (3): vpImage = gbuffer1Dat; break;
		case (4): vpImage = gbuffer2Dat; break;
		case (5): vpImage = gbuffer3Dat; break;
		case (6): vpImage = velocityDat; break;

		case (8): vpImage = dbuffer0Dat; break;
		case (9): vpImage = dbuffer1Dat; break;
		case (10): vpImage = dbuffer2Dat; break;
		case (11): vpImage = wblendcolaccumDat; break;
		case (12): vpImage = wblendcolrevealDat; break;
		case (13): vpImage = ssaoDat; break;
		case (14): vpImage = lbuffer0Dat; break;
		case (15): vpImage = lbuffer1Dat; break;
		}
		ImGui::Image(vpImage.tex, vSize, vpUV0, vpUV1);

		if (renderpassRadioButton != 0) {
			
			ImGuiIO& io = ImGui::GetIO();
			ImGui::GetWindowDrawList()->AddText(ImGui::GetIO().Fonts->Fonts[1], 28.f, ImVec2(GraphicsSettings2::getRuntimeInfo().localScreen.x + 8, GraphicsSettings2::getRuntimeInfo().localScreen.y + GraphicsSettings2::getRuntimeInfo().localScreen.w - (28 * 4 + 8)),
				ImColor(1.f, 1.0f, 1.f),
				fmt::format("{}:\n\tFormat: {}\n\tDimensions: {}x{}x{}\n\tMemory Usage: {} kB",
					vpImage.what, vpImage.format, vpImage.dim.x, vpImage.dim.y, vpImage.dim.z, vpImage.resourceSizeKB
					).c_str(),
				(const char*)0, 0.0f);
		}
		
		if (ImGui::BeginDragDropTarget()) {
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SHARD3D.ASSEXP.LVL")) {
				if (MessageDialogs::show("This will overwrite the current level, and unsaved changes will be lost! Are you sure you want to continue?", "WARNING!", MessageDialogs::OPTYESNO | MessageDialogs::OPTICONEXCLAMATION | MessageDialogs::OPTDEFBUTTON2) == MessageDialogs::RESYES) {
					levelTreePanel.clearSelectedActor();
					loadLevel(frameInfo.level, frameInfo.resourceSystem, std::string((char*)payload->Data));
					refreshContext = true;
				}
			}
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SHARD3D.ASSEXP.BLUP")) {
				AssetID asset = AssetID(std::string((char*)payload->Data));
				frameInfo.level->getPrefabManager()->cachePrefab(asset);
				
				Actor actor = frameInfo.level->getPrefabManager()->instPrefab(frameInfo.level.get(), 0, UUID(), asset, asset.getAsset().substr(0, asset.getAsset().find_last_of(".")));
				actor.getTransform().setTranslation(hoveredCoord);
			}
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SHARD3D.ASSEXP.MESH")) {
				Actor actor = frameInfo.level->createActor(0, "Some kind of mesh");
				AssetID asset = AssetID(std::string((char*)payload->Data));
				frameInfo.resourceSystem->loadMesh(asset);
				actor.addComponent<Components::Mesh3DComponent>(frameInfo.resourceSystem, asset);
				actor.makeStatic();
				actor.getTransform().setTranslation(hoveredCoord);
			}
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SHARD3D.COMPONENTS.DROP")) {
				size_t h_code = *reinterpret_cast<size_t*>(payload->Data);

				if (h_code == typeid(Components::TerrainComponent).hash_code()) {
					Actor actor = frameInfo.level->createActor(0, "Terrain Actor");
					actor.addComponent<Components::TerrainComponent>();
					actor.makeStatic();
					actor.getTransform().setTranslation(hoveredCoord);
				}
				if (h_code == typeid(Components::SkyAtmosphereComponent).hash_code()) {
					Actor parent = frameInfo.level->createActor(0, "Sun Sky Actor");
					Actor sun = frameInfo.level->createActor(0, "Directional Light Actor");
					sun.addComponent<Components::DirectionalLightComponent>().lightIntensity = 8.0f;
					Actor atmosphere = frameInfo.level->createActor(0, "Sky Atmosphere Actor");
					atmosphere.addComponent<Components::SkyAtmosphereComponent>().directionalLightActor = sun;
					atmosphere.getTransform().setTranslation({ 0.0, 1.0, 0.0 });
					Actor skylight = frameInfo.level->createActor(0, "Sky Light Actor");
					skylight.addComponent<Components::SkyLightComponent>().skyAtmosphereActor = atmosphere;
					skylight.getTransform().setTranslation({0.0, 2.0, 0.0});

					frameInfo.level->parentActor(sun, parent);
					frameInfo.level->parentActor(atmosphere, parent);
					frameInfo.level->parentActor(skylight, parent);
					parent.getTransform().setTranslation(hoveredCoord);
				}
				if (h_code == typeid(Components::DirectionalLightComponent).hash_code()) {
					Actor actor = frameInfo.level->createActor(0, "Directional Light Actor");
					actor.addComponent<Components::DirectionalLightComponent>();
					actor.makeStationary();
					actor.getTransform().setTranslation(hoveredCoord);
				}
				if (h_code == typeid(Components::ExponentialFogComponent).hash_code()) {
					Actor actor = frameInfo.level->createActor(0, "Exponential Fog Actor");
					actor.addComponent<Components::ExponentialFogComponent>();
					actor.makeStationary();
					actor.getTransform().setTranslation(hoveredCoord);
				}
				if (h_code == typeid(Components::VirtualPointLightComponent).hash_code()) {
					Actor actor = frameInfo.level->createActor(0, "Virtual Point Light Actor");
					actor.addComponent<Components::VirtualPointLightComponent>();
					actor.makeStatic();
					actor.getTransform().setTranslation(hoveredCoord);
				}

				if (h_code == typeid(Components::PointLightComponent).hash_code()) {
					Actor actor = frameInfo.level->createActor(0, "Point Light Actor");
					actor.addComponent<Components::PointLightComponent>();
					actor.makeStationary();
					actor.getTransform().setTranslation(hoveredCoord);
				}
				else if (h_code == typeid(Components::SpotLightComponent).hash_code()) {
					Actor actor = frameInfo.level->createActor(0, "Spot Light Actor");
					actor.addComponent<Components::SpotLightComponent>();
					actor.makeStationary();
					actor.getTransform().setTranslation(hoveredCoord);
				} else if (h_code == typeid(Components::DeferredDecalComponent).hash_code()) {
					Actor actor = frameInfo.level->createActor(0, "Decal Actor");
					actor.addComponent<Components::DeferredDecalComponent>().material = AssetID("engine/materials/decals/bullet.s3dasset");
					actor.addComponent<Components::BoxVolumeComponent>().bounds = { 1.0, 2.0, 1.0};
					actor.getComponent<Components::BoxVolumeComponent>().transitionDistance = 0.0f;
					actor.getTransform().setTranslation(hoveredCoord);
				}
				else if (h_code == typeid(Components::BoxReflectionCaptureComponent).hash_code()) {
					Actor actor = frameInfo.level->createActor(0, "Box Reflection Capture Actor");
					actor.addComponent<Components::BoxReflectionCaptureComponent>();
					actor.addComponent<Components::BoxVolumeComponent>();
					actor.getTransform().setTranslation(hoveredCoord);
				}
				else if (h_code == typeid(Components::BoxAmbientOcclusionVolumeComponent).hash_code()) {
					Actor actor = frameInfo.level->createActor(0, "Ambient Occlusion Volume Actor");
					actor.addComponent<Components::BoxAmbientOcclusionVolumeComponent>();
					actor.addComponent<Components::BoxVolumeComponent>();
					actor.getTransform().setTranslation(hoveredCoord);
				} 
				else if (h_code == typeid(Components::PlanarReflectionComponent).hash_code()) {
					Actor actor = frameInfo.level->createActor(0, "Planar Reflection Actor");
					actor.makeDynamic();
					actor.addComponent<Components::PlanarReflectionComponent>();
					actor.getTransform().setTranslation(hoveredCoord);
				}
				else if (h_code == typeid(Components::PostFXComponent).hash_code()) {
					Actor actor = frameInfo.level->createActor(0, "PostFX Volume Actor");
					actor.addComponent<Components::PostFXComponent>();
					actor.addComponent<Components::BoxVolumeComponent>();
					actor.getTransform().setTranslation(hoveredCoord);
				}
				else if (h_code == 32325235) {
					Actor actor = frameInfo.level->createActor(0, "Cube");
					actor.addComponent<Components::Mesh3DComponent>(frameInfo.resourceSystem, ResourceSystem::coreAssets.m_defaultModel);
					actor.getTransform().setTranslation(hoveredCoord);
				}
				else if (h_code == 623456) {
					Actor actor = frameInfo.level->createActor(0, "Sphere");
					actor.addComponent<Components::Mesh3DComponent>(frameInfo.resourceSystem, ResourceSystem::coreAssets.m_sphere);
					actor.getTransform().setTranslation(hoveredCoord);
				}
				else if (h_code == typeid(Components::CameraComponent).hash_code()) {
					Actor actor = frameInfo.level->createActor(0, "Camera Actor");
					frameInfo.resourceSystem->loadMesh(AssetID("engine/meshes/camcorder" ENGINE_ASSET_SUFFIX));
					actor.addComponent<Components::Mesh3DComponent>(frameInfo.resourceSystem, AssetID("engine/meshes/camcorder" ENGINE_ASSET_SUFFIX));
					actor.addComponent<Components::CameraComponent>();
					actor.addComponent<Components::PostFXComponent>();
					actor.getComponent<Components::VisibilityComponent>().editorOnly = true;
					actor.getTransform().setTranslation(hoveredCoord);
				}
			}
		}
		
		if (frameInfo.level->simulationState != PlayState::Playing) {	
			if (ImGui::IsWindowHovered()) {
				isViewportHovered = true;
				Actor cameraActor = frameInfo.level->getActorFromUUID(0, 0);
				editorCameraController.tryPollOrientation(engineWindow, frameInfo.frameTime, { ImGui::GetWindowPos().x, ImGui::GetWindowPos().y, ImGui::GetWindowSize().x ,ImGui::GetWindowSize().y }, cameraActor);
				if (ImGui::IsMouseClicked(ImGuiMouseButton_Right)) ImGui::FocusWindow(ImGui::GetCurrentWindow());
				editorCameraController.scrollMouse(engineWindow, ImGui::GetIO().MouseWheel);
			} else {
				isViewportHovered = false;
			}
			if (ImGui::IsWindowFocused()) {
				Actor cameraActor = frameInfo.level->getActorFromUUID(0, 0);
				editorCameraController.tryPollTranslation(engineWindow, frameInfo.frameTime, cameraActor);
			}
		}

		{
			Actor actor = levelTreePanel.getSelectedActor();
			if (actor) {
				ImGuizmo::SetOrthographic(false);
				ImGuizmo::SetDrawlist();
				ImGuizmo::SetRect(ImGui::GetWindowPos().x, ImGui::GetWindowPos().y, ImGui::GetWindowWidth(), ImGui::GetWindowHeight());
				S3DCamera& camera = frameInfo.camera;
				glm::mat4 proj = camera.getProjection();
				glm::mat4 view = camera.getView();

				auto& tc = actor.getComponent<Components::TransformComponent>();
				glm::mat4 actorTransform = tc.transformMatrix;			
				bool hasParent = actor.hasRelationship() && actor.getComponent<Components::RelationshipComponent>().parentActor != entt::null;

				glm::vec3 snap = glm::vec3(1.0f);
				ImGuizmo::Manipulate(
					glm::value_ptr(view), glm::value_ptr(proj), (ImGuizmo::OPERATION)gizmoType, ImGuizmo::WORLD, glm::value_ptr(actorTransform),
					nullptr,
					false ? glm::value_ptr(snap) : nullptr
				);
				gizmoActive = ImGuizmo::IsUsing();
				if (ImGuizmo::IsOver() && gizmoType != -1) {

					if (hasParent) {
						auto& ptc = Actor(actor.getComponent<Components::RelationshipComponent>().parentActor, frameInfo.level.get()).getTransform();
						glm::mat4 parentTransform = ptc.transformMatrix;

						glm::vec3 invParentScale = glm::vec3(1.0) / glm::vec3{ glm::length(parentTransform[0]),glm::length(parentTransform[1]), glm::length(parentTransform[2]) };

						 // this technically works but TODO: see if this code can be made nicer
						glm::mat4 bullshit = glm::inverse(parentTransform) * actorTransform;

						actorTransform[0] *= invParentScale.x;
						actorTransform[1] *= invParentScale.y;
						actorTransform[2] *= invParentScale.z;

						//glm::vec3 actorTranslation =
						//	(actorTransform[3].x - parentTransform[3].x) * glm::vec3(parentTransform[0]) +
						//	(actorTransform[3].y - parentTransform[3].y) * glm::vec3(parentTransform[1]) +
						//	(actorTransform[3].z - parentTransform[3].z) * glm::vec3(parentTransform[2]);

						glm::mat3 parentRotation = glm::mat3(
							 parentTransform[0] * invParentScale.x,
							 parentTransform[1] * invParentScale.y,
							 parentTransform[2] * invParentScale.z
						);

						glm::mat3 actorRotation = glm::inverse(parentRotation) * glm::mat3(actorTransform);

						actorTransform[0] = glm::vec4(actorRotation[0], 0.0);
						actorTransform[1] = glm::vec4(actorRotation[1], 0.0);
						actorTransform[2] = glm::vec4(actorRotation[2], 0.0);
						actorTransform[3] = bullshit[3];
					}
					glm::vec3 realTranslation;
					glm::vec3 realRotation;
					glm::vec3 realScale;

					TransformMath::decompose(actorTransform, &realTranslation, &realRotation, &realScale);

					// not sure if needed
					//glm::vec3 original_rotation = tc.getRotation();
					//glm::vec3 deltaRotation = glm::vec3(realRotation) - original_rotation; 

					static bool lock = false;

					if (engineWindow.isKeyDown(GLFW_KEY_RIGHT_SHIFT)) {
						if (engineWindow.isMouseButtonDown(GLFW_MOUSE_BUTTON_LEFT)) {
							if (!lock) {
								levelTreePanel.getSelectedActor() = frameInfo.level->duplicateActor(actor, true);
								tc = levelTreePanel.getSelectedActor().getTransform();
								lock = true;
							}
						} else {
							lock = false;
						}
					} else {
						lock = false;
					}

					tc.setTranslation(glm::vec3(realTranslation));
					tc.setScale(glm::vec3(realScale));
					tc.setRotation(realRotation); // tc.setRotation(deltaRotation)
				}
			}
		}
		{
			ImVec2 vMin = ImGui::GetWindowContentRegionMin();
			vMin.x += ImGui::GetWindowPos().x + 8;
			vMin.y += ImGui::GetWindowPos().y + 8;
			SYSTEM_INFO siSysInfo;
			GetSystemInfo(&siSysInfo);
			MEMORYSTATUSEX statex{};
			statex.dwLength = sizeof(statex);
			GlobalMemoryStatusEx(&statex);
			int CPUInfo[4] = { -1 };
			unsigned   nExIds, i = 0;
			char CPUBrandString[0x40];
			// Get the information associated with each extended ID.
			__cpuid(CPUInfo, 0x80000000);
			nExIds = CPUInfo[0];
			for (i = 0x80000000; i <= nExIds; ++i)
			{
				__cpuid(CPUInfo, i);
				// Interpret CPU brand string
				if (i == 0x80000002)
					memcpy(CPUBrandString, CPUInfo, sizeof(CPUInfo));
				else if (i == 0x80000003)
					memcpy(CPUBrandString + 16, CPUInfo, sizeof(CPUInfo));
				else if (i == 0x80000004)
					memcpy(CPUBrandString + 32, CPUInfo, sizeof(CPUInfo));
			}

			timeSinceLastSecond += frameInfo.frameTime;
			if (timeSinceLastSecond > 0.25f) { deltaTimeFromLastSecond = frameInfo.frameTime; timeSinceLastSecond = 0; }

			VkDeviceSize usingVMemoryUsage = 0;
			VkDeviceSize availableVMemoryUsage = 0;
			if (engineDevice.supportedExtensions.ext_memory_budget) {
				VkPhysicalDeviceMemoryBudgetPropertiesEXT memoryProperties;
				memoryProperties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_BUDGET_PROPERTIES_EXT;
				memoryProperties.pNext = nullptr;

				VkPhysicalDeviceMemoryProperties2 deviceMemoryProperties;
				deviceMemoryProperties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2;
				deviceMemoryProperties.pNext = &memoryProperties;
				vkGetPhysicalDeviceMemoryProperties2(engineDevice.physicalDevice(), &deviceMemoryProperties);

				uint32_t heapIndex = 0;
				for (uint32_t i = 0; i < deviceMemoryProperties.memoryProperties.memoryHeapCount; i++) {
					if (deviceMemoryProperties.memoryProperties.memoryHeaps[i].flags & VK_MEMORY_HEAP_DEVICE_LOCAL_BIT) {
						heapIndex = i;
						break;
					}
				}

				usingVMemoryUsage = (memoryProperties.heapUsage[heapIndex] / 1008576i64);
				availableVMemoryUsage = (memoryProperties.heapBudget[heapIndex] / 1048576i64);
			}

			ImGui::GetWindowDrawList()->AddText(ImGui::GetIO().Fonts->Fonts[1], 18.f, ImVec2(vMin.x, vMin.y), ImColor(1.f, 0.0f, 1.f), 
				fmt::format("Shard3D Version {} (alpha) build {}\nFramerate: {:.1f}\nFrametime: {:.4f} ms\nHardware:\n\tProcessor: {}\n\tLogical Processors: {}\n\tSystem Memory (MB): {}\n\tGraphics Card: {} ({})\n\tVideo Memory (MB): {}\n\tVRAM Usage (MB): {}/{}", 
					ENGINE_VERSION.getVersion(), ENGINE_VERSION.getBuild(), 1.f / deltaTimeFromLastSecond, deltaTimeFromLastSecond * 1000.f,
					CPUBrandString, siSysInfo.dwNumberOfProcessors, (statex.ullTotalPhys / 1048576i64),
					engineDevice.getGPUInformation().name, engineDevice.getGPUInformation().type, (engineDevice.getGPUInformation().memory / 1048576i64),
					usingVMemoryUsage, availableVMemoryUsage 
				).c_str());
		}
		const static int ICONSIZE = 64;
		if (frameInfo.level->simulationState == PlayState::Paused) {	
			ImVec2 vMin = ImGui::GetWindowContentRegionMin();
			vMin.x += ImGui::GetWindowPos().x + ImGui::GetContentRegionMax().x - 88 - ICONSIZE;
			vMin.y += ImGui::GetWindowPos().y + 16;
			ImGui::GetWindowDrawList()->AddImage(getImGuiIcon().pause, vMin, ImVec2(vMin.x + ICONSIZE, vMin.y + ICONSIZE), ImVec2(0, 0), ImVec2(1, 1));
		}
		if (GraphicsSettings2::editorPreview.ONLY_GAME) {
			ImVec2 vMin = ImGui::GetWindowContentRegionMin();
			vMin.x += ImGui::GetWindowPos().x + ImGui::GetContentRegionMax().x - 16 - ICONSIZE;
			vMin.y += ImGui::GetWindowPos().y + 16;
			ImGui::GetWindowDrawList()->AddImage(getImGuiIcon().preview, vMin, ImVec2(vMin.x + ICONSIZE, vMin.y + ICONSIZE), ImVec2(0, 0), ImVec2(1, 1));
		}

		ImGui::End();

		ImGui::Begin("Telemetry Data");
		for (const auto& telem : mainCameraTelem->getTelemetryData()) {
			ImGui::Text("[%s] [%s] : %f ms", telem->getCategory(), telem->getEntry(), telem->getTime());
		}
		ImGui::End();
	}

	void Editor::renderMaterialEditorViewport(FrameInfo2& frameInfo) {
		if (!isOpenMaterialEditor) {
			closeMaterialEditor();
		}
#pragma region boilerplate dockspace code    
		static bool opt_fullscreen = false;
		static bool opt_padding = true;
		static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;
		// We are using the ImGuiWindowFlags_NoDocking flag to make the parent window not dockable into,
		// because it would be confusing to have two docking targets within each others.
		ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDocking;
		dockspace_flags &= ~ImGuiDockNodeFlags_PassthruCentralNode;
		if (!opt_padding)
			ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
		ImGui::Begin("MaterialEditor", &isOpenMaterialEditor, window_flags);
		if (!opt_padding)
			ImGui::PopStyleVar();
		if (opt_fullscreen)
			ImGui::PopStyleVar(2);
#pragma endregion
		// Submit the DockSpace
		ImGuiID dockspace_id = ImGui::GetID("MaterialEditor");
		ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);

		//if (ImGui::BeginTabBar("material tabs", ImGuiTabBarFlags_Reorderable | ImGuiTabBarFlags_FittingPolicyResizeDown)) {
		//	for (int i = 0; i < 2; i++) {
		//		if (ImGui::BeginTabItem("maymate233234124545" + i)) {
		ImGui::Begin("MaterialEditorViewport");
		ImVec2 vSize = ImGui::GetContentRegionAvail();
		materialViewportAR = vSize.x / vSize.y;
		VkDescriptorSet vpImage = materialEditorViewport;
		ImVec2 vpUV0 = ImVec2(0.0, 0.0);
		ImVec2 vpUV1 = ImVec2(1.0, 1.0);
		ImGui::Image(vpImage, vSize, vpUV0, vpUV1);
		double mouseX, mouseY;
		glfwGetCursorPos(engineWindow.getGLFWwindow(), &mouseX, &mouseY);
		glm::vec2 mousePos(mouseX, mouseY);
		if (ImGui::IsWindowHovered()) {
			isEditorMaterialViewportHovered = true;
			int wposx, wposy;
			glfwGetWindowPos(engineWindow.getGLFWwindow(), &wposx, &wposy); editorMaterialCameraController.tryPollOrientation(reinterpret_cast<GLFWwindow*>(ImGui::GetWindowViewport()->PlatformHandle), frameInfo.frameTime, { ImGui::GetWindowPos().x - wposx, ImGui::GetWindowPos().y - wposy, vSize.x ,vSize.y }, materialEditorLevel->getActorFromTag("Camera Pivot Actor"), materialEditorLevel->getActorFromTag("Material Camera Actor"));
			if (ImGui::IsMouseClicked(ImGuiMouseButton_Right)) ImGui::FocusWindow(ImGui::GetCurrentWindow());
			editorMaterialCameraController.scrollMouse(ImGui::GetIO().MouseWheel);
		} else isEditorMaterialViewportHovered = false;

		if (ImGui::BeginDragDropTarget()) {
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SHARD3D.ASSEXP.MATE")) {
				if (MessageDialogs::show("This will overwrite the current surface material, and unsaved changes will be lost! Are you sure you want to continue?", "WARNING!", MessageDialogs::OPTYESNO | MessageDialogs::OPTICONEXCLAMATION | MessageDialogs::OPTDEFBUTTON2) == MessageDialogs::RESYES) {
					materialBuilderPanel = MaterialBuilderPanel(frameInfo.resourceSystem, materialEditorLevel, AssetID(std::string((char*)payload->Data)));
				}
			}
		}

		ImGui::End();
		materialBuilderPanel.render(frameInfo);
		//			ImGui::EndTabItem();
		//		}
		//	}
		//	ImGui::EndTabBar();
		//}
		ImGui::End();
		if (materialBuilderPanel.requireMaterialRebuild)
			materialEditorLevelRenderInstance->getRenderObjects().renderList->modifyMaterialType(materialBuilderPanel.getCurrentMaterialAsset(), materialEditorLevel, materialBuilderPanel.oldMaterialFlags, materialBuilderPanel.getCurrentMaterial()->getPipelineClass());
		materialBuilderPanel.requireMaterialRebuild = false;
	}

	void Editor::renderPhysicsEditorViewport(FrameInfo2& frameInfo) {
		if (!isOpenPhysicsEditor) {
			closePhysicsEditor();
		}
#pragma region boilerplate dockspace code    
		static bool opt_fullscreen = false;
		static bool opt_padding = true;
		static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;
		// We are using the ImGuiWindowFlags_NoDocking flag to make the parent window not dockable into,
		// because it would be confusing to have two docking targets within each others.
		ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDocking ;
		dockspace_flags &= ~ImGuiDockNodeFlags_PassthruCentralNode;
		if (!opt_padding)
			ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
		ImGui::Begin("PhysicsEditor", &isOpenPhysicsEditor, window_flags);
		if (!opt_padding)
			ImGui::PopStyleVar();
		if (opt_fullscreen)
			ImGui::PopStyleVar(2);
#pragma endregion
		// Submit the DockSpace
		ImGuiID dockspace_id = ImGui::GetID("PhysicsEditor");
		ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);

		ImGui::Begin("PhysicsEditorViewport");
		ImVec2 vSize = ImGui::GetContentRegionAvail();
		physicsViewportAR = vSize.x / vSize.y;
		VkDescriptorSet vpImage = physicsEditorViewport;
		ImVec2 vpUV0 = ImVec2(0.0, 0.0);
		ImVec2 vpUV1 = ImVec2(1.0, 1.0);
		ImGui::Image(vpImage, vSize, vpUV0, vpUV1);
		double mouseX, mouseY;
		glfwGetCursorPos(engineWindow.getGLFWwindow(), &mouseX, &mouseY);

		int wposx, wposy;
		glfwGetWindowPos(engineWindow.getGLFWwindow(), &wposx, &wposy);
		glm::vec2 mousePos(mouseX, mouseY);
		if (ImGui::IsWindowHovered()) {
			isEditorPhysicsViewportHovered = true; 
			editorPhysicsCameraController.tryPollOrientation(reinterpret_cast<GLFWwindow*>(ImGui::GetWindowViewport()->PlatformHandle), frameInfo.frameTime, { ImGui::GetWindowPos().x - wposx, ImGui::GetWindowPos().y - wposy, vSize.x ,vSize.y }, physicsEditorLevel->getActorFromTag("CameraPivot"), physicsEditorLevel->getActorFromTag("CameraActor"));
			if (ImGui::IsMouseClicked(ImGuiMouseButton_Right)) ImGui::FocusWindow(ImGui::GetCurrentWindow());
			editorPhysicsCameraController.scrollMouse(ImGui::GetIO().MouseWheel);
		} else isEditorPhysicsViewportHovered = false;

		if (ImGui::BeginDragDropTarget()) {
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SHARD3D.ASSEXP.PHYA")) {
				if (MessageDialogs::show("This will overwrite the current physics asset, and unsaved changes will be lost! Are you sure you want to continue?", "WARNING!", MessageDialogs::OPTYESNO | MessageDialogs::OPTICONEXCLAMATION | MessageDialogs::OPTDEFBUTTON2) == MessageDialogs::RESYES) {
					physicsBuilderPanel = PhysicsBuilderPanel(physicsEditorLevel, AssetID(std::string((char*)payload->Data)), AssetID::null());
				}
			}
		}


		auto& cmc = physicsEditorLevel->getPossessedCameraActor().getComponent<Components::CameraComponent>();
		//cmc.setProjection(true);
		//cmc.camera.setViewYXZ(physicsEditorLevel->getPossessedCameraActor().getTransform().transformMatrix);
		glm::mat4 proj = cmc.camera.getProjection();
		glm::mat4 view = cmc.camera.getView();
		glm::vec3 transform = glm::vec3(physicsEditorLevel->getActorFromTag("PhysicsActor").getTransform().transformMatrix[3]);

		bool manipulating = false;
		if (physicsBuilderPanel.isEditingConvexShape()) {
			if (physicsEditorLevel->getActorFromTag("FLAG_EDITOR.GizmoActivePoint").isVisible()) {
				// can only edit either convex object or constraint nodes, not both
				glm::vec3& editing = physicsBuilderPanel.getModifyingConvexPoint();
				transform += editing;
				auto& tc = physicsEditorLevel->getActorFromTag("FLAG_EDITOR.GizmoActivePoint").getTransform();
				tc.setTranslation(glm::vec3{ transform.x, transform.z, transform.y });
				tc.transformMatrix = tc.calculateMat4();
				tc.dirty = false;

				ImGuizmo::SetOrthographic(false);
				ImGuizmo::SetDrawlist();
				ImGuizmo::SetRect(ImGui::GetWindowPos().x, ImGui::GetWindowPos().y, ImGui::GetWindowWidth(), ImGui::GetWindowHeight());

				glm::mat4 actorTransform = tc.transformMatrix;
				bool s = ImGuizmo::Manipulate(glm::value_ptr(view), glm::value_ptr(proj), ImGuizmo::TRANSLATE, ImGuizmo::WORLD, glm::value_ptr(actorTransform));
				if (manipulating = ImGuizmo::IsOver(); manipulating ) {
					tc.setTranslation({ actorTransform[3].x, actorTransform[3].z, actorTransform[3].y });
					glm::vec3 transl = -glm::vec3(physicsEditorLevel->getActorFromTag("PhysicsActor").getTransform().transformMatrix[3]);
					transl += glm::vec3(actorTransform[3]);
					editing = transl;
				}
			}
		}

		if (physicsEditorLevel->getActorFromTag("FLAG_EDITOR.GizmoActivePoint").isVisible()) {
			glm::vec2 bds = glm::vec2(ImGui::GetWindowPos().x - wposx, ImGui::GetWindowPos().y - wposy);
			glm::vec2 mouseUV = (glm::vec2(mouseX, mouseY) - bds) / (glm::vec2&)vSize;
			glm::vec2 resWorld = mouseUV * glm::vec2(640, 480);
			physicsEditorLevel->getActorFromTag("FLAG_EDITOR.CursorPositionABS").getTransform().setTranslation({ resWorld.x, 0.0f, resWorld.y });

			if (engineWindow.isMouseButtonDown(GLFW_MOUSE_BUTTON_LEFT) && !manipulating && glm::all(glm::lessThan(mouseUV, glm::vec2(1.0f))) && glm::all(glm::greaterThan(mouseUV, glm::vec2(0.0f)))) {
				bool found = false;
				if (physicsEditorLevel->getActorFromTag("FLAG_EDITOR.DisplayPoints").isVisible()) {
					for (int i = 0; i < physicsBuilderPanel.getCurrentPhysicsObject().convexPoints.size(); i++) {
						glm::vec4 projP = proj * view * glm::vec4(physicsBuilderPanel.getCurrentPhysicsObject().convexPoints[i] + transform, 1.0f);
						projP = projP / projP.w;
						projP.y *= -1.0f;
						glm::vec2 real = (glm::vec2(projP) * 0.5f + 0.5f) * glm::vec2(640.f, 480.f);

						if (glm::distance(real, resWorld) < 12.0f) {
							physicsBuilderPanel.setModifyingConvexPoint(i);
							found = true;
							break;
						}
					}
					if (!found) {
						physicsBuilderPanel.setModifyingConvexPoint(-1);
						physicsEditorLevel->getActorFromTag("FLAG_EDITOR.GizmoActivePoint").getTransform().setTranslation(glm::vec3(999.0f));
					}
				}
			}
			
			
		}

		ImGui::End();
		physicsBuilderPanel.render(frameInfo);

		ImGui::End();
	}

	void Editor::renderAnimationPreviewerViewport(FrameInfo2& frameInfo) {
		if (!isOpenAnimationPreviewer) {
			closeSkeletalAnimationPreviewer();
		}
#pragma region boilerplate dockspace code    
		static bool opt_fullscreen = false;
		static bool opt_padding = true;
		static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;
		// We are using the ImGuiWindowFlags_NoDocking flag to make the parent window not dockable into,
		// because it would be confusing to have two docking targets within each others.
		ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDocking;
		dockspace_flags &= ~ImGuiDockNodeFlags_PassthruCentralNode;
		if (!opt_padding)
			ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
		ImGui::Begin("SkeletalAnimationPreviewer", &isOpenAnimationPreviewer, window_flags);
		if (!opt_padding)
			ImGui::PopStyleVar();
		if (opt_fullscreen)
			ImGui::PopStyleVar(2);
#pragma endregion
		// Submit the DockSpace
		ImGuiID dockspace_id = ImGui::GetID("SkeletalAnimationPreviewer");
		ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);

		//if (ImGui::BeginTabBar("material tabs", ImGuiTabBarFlags_Reorderable | ImGuiTabBarFlags_FittingPolicyResizeDown)) {
		//	for (int i = 0; i < 2; i++) {
		//		if (ImGui::BeginTabItem("maymate233234124545" + i)) {
		ImGui::Begin("SkeletalAnimationPreviewerViewport");
		ImVec2 vSize = ImGui::GetContentRegionAvail();
		animationPreviewerAR = vSize.x / vSize.y;
		VkDescriptorSet vpImage = animationPreviewerViewport;
		ImVec2 vpUV0 = ImVec2(0.0, 0.0);
		ImVec2 vpUV1 = ImVec2(1.0, 1.0);
		ImGui::Image(vpImage, vSize, vpUV0, vpUV1);
		double mouseX, mouseY;
		glfwGetCursorPos(engineWindow.getGLFWwindow(), &mouseX, &mouseY);
		glm::vec2 mousePos(mouseX, mouseY);
		if (ImGui::IsWindowHovered()) {
			isAnimationPreviewerViewportHovered = true;
			int wposx, wposy;
			glfwGetWindowPos(engineWindow.getGLFWwindow(), &wposx, &wposy);
			previewerAnimationCameraController.tryPollOrientation(reinterpret_cast<GLFWwindow*>(ImGui::GetWindowViewport()->PlatformHandle), frameInfo.frameTime, { ImGui::GetWindowPos().x - wposx, ImGui::GetWindowPos().y - wposy, vSize.x ,vSize.y }, animationPreviewerLevel->getActorFromTag("Camera Pivot Actor"), animationPreviewerLevel->getActorFromTag("Camera Actor"));
			if (ImGui::IsMouseClicked(ImGuiMouseButton_Right)) ImGui::FocusWindow(ImGui::GetCurrentWindow());
			previewerAnimationCameraController.scrollMouse(ImGui::GetIO().MouseWheel);
		} else isAnimationPreviewerViewportHovered = false;

		if (ImGui::BeginDragDropTarget()) {
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SHARD3D.ASSEXP.SANM")) {
				AssetID animation = AssetID(std::string((char*)payload->Data));
				AssetID skeleton = ResourceSystem::discoverAttachedSkeletalMeshAsset(animation);
				AssetID mesh = ResourceSystem::discoverAttachedMeshAsset(skeleton);
				animationPreviewPanel = MeshAnimationPreviewPanel(frameInfo.resourceSystem, animationPreviewerLevel, mesh, skeleton, animation);	
			}
		}

		ImGui::End();
		animationPreviewPanel.render(frameInfo);
		ImGui::End();
	}

	void Editor::renderHUDEditorViewport(FrameInfo2& frameInfo) {
		if (!isOpenHUDEditor) {
			closeHUDEditor();
		}
#pragma region boilerplate dockspace code    
		static bool opt_fullscreen = false;
		static bool opt_padding = true;
		static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;
		// We are using the ImGuiWindowFlags_NoDocking flag to make the parent window not dockable into,
		// because it would be confusing to have two docking targets within each others.
		ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDocking;
		dockspace_flags &= ~ImGuiDockNodeFlags_PassthruCentralNode;
		if (!opt_padding)
			ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
		ImGui::Begin("HUDLayerEditor", &isOpenHUDEditor, window_flags);
		if (!opt_padding)
			ImGui::PopStyleVar();
		if (opt_fullscreen)
			ImGui::PopStyleVar(2);
#pragma endregion
		// Submit the DockSpace
		ImGuiID dockspace_id = ImGui::GetID("HUDLayerEditor");
		ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);

		ImGui::Begin("HUDLayerEditorViewport");
		ImVec2 vSize = ImGui::GetContentRegionAvail();
		hudLayerViewportAR = vSize.x / vSize.y;
		VkDescriptorSet vpImage = hudLayerEditorViewport;
		ImVec2 vpUV0 = ImVec2(0.0, 0.0);
		ImVec2 vpUV1 = ImVec2(1.0, 1.0);
		ImGui::Image(vpImage, vSize, vpUV0, vpUV1);
		double mouseX, mouseY;
		glfwGetCursorPos(engineWindow.getGLFWwindow(), &mouseX, &mouseY);
		glm::vec2 mousePos(mouseX, mouseY);

		int wposx, wposy;
		glfwGetWindowPos(engineWindow.getGLFWwindow(), &wposx, &wposy);
		glm::vec2 windowPos = (glm::vec2&)ImGui::GetWindowPos();
		windowPos.x -= wposx;
		windowPos.y -= wposy;
		if (ImGui::IsWindowHovered()) {
			isEditorHUDViewportHovered = true;
		} else isEditorHUDViewportHovered = false;

		if (ImGui::BeginDragDropTarget()) {
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SHARD3D.ASSEXP.HUDL")) {
				if (MessageDialogs::show("This will overwrite the current hud layer, and unsaved changes will be lost! Are you sure you want to continue?", "WARNING!", MessageDialogs::OPTYESNO | MessageDialogs::OPTICONEXCLAMATION | MessageDialogs::OPTDEFBUTTON2) == MessageDialogs::RESYES) {
					hudBuilderPanel = HUDBuilderPanel(frameInfo.resourceSystem, AssetID(std::string((char*)payload->Data)));
				}
			}
		}

		ImGui::End();
		hudBuilderPanel.render(frameInfo, frameInfo.hudRenderSystem->getHUDLayer("_editorreserved"));
		hudBuilderPanel.renderElementPanel(frameInfo, frameInfo.hudRenderSystem->getHUDLayer("_editorreserved"), resourceSystem);
		ImGui::End();
		uiPreviewBuilderContext->UpdateIOFrame();

		glm::vec2 uv = glm::vec2((mousePos - windowPos) / glm::vec2(vSize.x, vSize.y));
		uiPreviewBuilderContext->GetDrawList()->Clear();
		uiPreviewBuilderContext->GetDrawList()->FrameStart();
		frameInfo.hudRenderSystem->renderHUDLayer(uiPreviewBuilderContext->GetDrawList(), "_editorreserved");
		uiPreviewBuilderContext->DrawDebugInfo();
		uiPreviewBuilderContext->GetDrawList()->FrameEnd();
		uiPreviewBuilderContext->GetIO().mCursorPosition = (SHUD::fvec2&)(uv * glm::vec2(1280, 720));
		uiPreviewBuilderContext->GetIO().mButtonState[GLFW_MOUSE_BUTTON_LEFT] = engineWindow.isMouseButtonDown(GLFW_MOUSE_BUTTON_LEFT);
		uiPreviewBuilderContext->Pick();
		uiPreviewBuilderContext->Cleanup();
	}
	void Editor::renderQuickBar(FrameInfo2& frameInfo, LevelRenderInstance* levelRenderInstance) {
		ImGui::Begin("_editor_quickbar", nullptr, ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoTitleBar);
		if (ImGui::BeginTabBar("_editor_quickbar_tabbar")) {
			if (ImGui::BeginTabItem("Level")) {
				renderQuickBarLevel(frameInfo);
				ImGui::EndTabItem();
			}
			if (ImGui::BeginTabItem("Terrain")) {
				renderQuickBarTerrain(frameInfo, levelRenderInstance);
				ImGui::EndTabItem();
			}
			ImGui::EndTabBar();
		}
		ImGui::End();
	}

	void Editor::renderQuickBarLevel(FrameInfo2& frameInfo) {

		//ImGui::Begin("_editor_quickbar_level", nullptr, ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoTitleBar);
		ImVec2 btnSize = { 48.f, 48.f };
		float panelWidth = ImGui::GetContentRegionAvail().x;
		int columnCount = std::max((int)(panelWidth / (btnSize.x + 16.f))// <--- thumbnail size (96px) + padding (16px)
			, 1);
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4());
		ImGui::PushStyleVar(ImGuiStyleVar_FrameBorderSize, 0.f);
		ImGui::Columns(columnCount, 0, false);                
		// begin

		// Save/Load
		if (ImGui::ImageButton(getImGuiIcon().save, btnSize)) {
			saveLevel(frameInfo.level, frameInfo.resourceSystem, frameInfo.level->levelAsset.getAbsolute());
		}
		ImGui::TextWrapped("Save");
		ImGui::NextColumn();
		if (ImGui::ImageButton(getImGuiIcon().load, btnSize)) {
			if (MessageDialogs::show("This will overwrite the current level, and unsaved changes will be lost! Are you sure you want to continue?", "WARNING!", MessageDialogs::OPTYESNO | MessageDialogs::OPTICONEXCLAMATION | MessageDialogs::OPTDEFBUTTON2) == MessageDialogs::RESYES) {
				std::string filepath = BrowserDialogs::openFile(ENGINE_SHARD3D_LEVELFILE_OPTIONS);
				if (!filepath.empty()) {
					levelTreePanel.clearSelectedActor();
					loadLevel(frameInfo.level, frameInfo.resourceSystem, filepath);
					refreshContext = true;
				}
			}
		}
		ImGui::TextWrapped("Load");
		ImGui::NextColumn();
		ImGui::NextColumn();

		// PLAY MODE
		if (ImGui::ImageButton(
			(frameInfo.level->simulationState == PlayState::Stopped) ? getImGuiIcon().play : 
				((frameInfo.level->simulationState == PlayState::Paused) ? getImGuiIcon().play : getImGuiIcon().pause), btnSize))
			if (frameInfo.level->simulationState == PlayState::Stopped) { // Play
				playLevel(frameInfo.level, capturedLevel, engineWindow.getGLFWwindow());
			} else if (frameInfo.level->simulationState == PlayState::Paused) {  // Resume
				resumeLevel(frameInfo.level, engineWindow.getGLFWwindow());
			} else if (frameInfo.level->simulationState == PlayState::Playing) {  // Pause
				pauseLevel(frameInfo.level, engineWindow.getGLFWwindow());
			}
		ImGui::TextWrapped((frameInfo.level->simulationState == PlayState::Stopped) ? "Play" :
			((frameInfo.level->simulationState == PlayState::Paused) ? "Resume" : "Pause"));
		ImGui::NextColumn();
		ImGui::BeginDisabled(frameInfo.level->simulationState == PlayState::Stopped);
		if (ImGui::ImageButton(getImGuiIcon().stop, btnSize)) {        // Stop
			levelTreePanel.clearSelectedActor();
			stopLevel(frameInfo.level, capturedLevel, engineWindow.getGLFWwindow(), terrainEditSystem);
			refreshContext = true;
		}
		ImGui::TextWrapped("Stop");
		ImGui::EndDisabled();

		ImGui::NextColumn();
		// Editor settings
		if (ImGui::ImageButton(getImGuiIcon().settings, btnSize)) {
			if (!std::filesystem::exists(frameInfo.level->bakedDataDirectory.getAbsolute())) {
				std::filesystem::create_directories(frameInfo.level->bakedDataDirectory.getAbsolute());
			}
			LPVOID dialog;
			HRESULT res = CoCreateInstance(CLSID_ProgressDialog, nullptr, CLSCTX_INPROC_SERVER, IID_IProgressDialog, &dialog);
			reinterpret_cast<IProgressDialog*>(dialog)->SetTitle(L"Level Baking");
			reinterpret_cast<IProgressDialog*>(dialog)->SetLine(1, L"Building Lighting...", false, nullptr);
			reinterpret_cast<IProgressDialog*>(dialog)->StartProgressDialog(nullptr, nullptr, PROGDLG_NOMINIMIZE | PROGDLG_NOCANCEL | PROGDLG_MARQUEEPROGRESS, nullptr);

			auto newTime = std::chrono::high_resolution_clock::now();
			vkDeviceWaitIdle(engineDevice.device());

			frameInfo.level->registry.view<Components::BoxReflectionCaptureComponent, Components::UUIDComponent, RENDER_ITER>().each([&](auto& brfcc, auto& uuid, auto& transform, auto& visibility) {
				if (!brfcc.useAssetInsteadOfCapture) {
					StaticReflectionCapturePass capturer = StaticReflectionCapturePass(
						transform.getTranslationWorld(), frameInfo.level, engineDevice, resourceSystem, globalSetLayout, sceneSetLayout, 
						skeletonRigSetLayout, terrainSetLayout, static_cast<float>(brfcc.resolution), 4.f
					);

					AssetID asset = frameInfo.level->bakedDataDirectory.getAsset() + "/brfcpt_" + std::to_string(uuid.getScopedID());
					capturer.capture(asset.getAbsolute() + ".s3dbak", asset.getAbsolute() + "_irr.s3dbak");
					PackageInvocationIndex pii{};
					pii.resource = ResourceType::TextureCube;
					AssetID result0{ AssetID::null() };
					result0 = asset.getAsset() + ".s3dbak.s3dasset";
					AssetID result1{ AssetID::null() };
					result1 = asset.getAsset() + "_irr.s3dbak.s3dasset";
					resourceSystem->loadBakedReflectionCubeAsset(result0, true);
					resourceSystem->loadBakedReflectionCubeAsset(result1, true);
					brfcc.reflectionAsset = result0;
					brfcc.irradianceAsset = result1;
				}
			});
			
			SHARD3D_INFO("Duration of building lighting: {0} ms",
				std::chrono::duration<float, std::chrono::milliseconds::period>
				(std::chrono::high_resolution_clock::now() - newTime).count());
			reinterpret_cast<IProgressDialog*>(dialog)->StopProgressDialog();
		}
		ImGui::TextWrapped("Bake");
		ImGui::NextColumn();
		ImGui::NextColumn();
		// Editor settings
		if (ImGui::ImageButton(getImGuiIcon().pref, btnSize)) {
			SHARD3D_NOIMPL;
		}
		ImGui::TextWrapped("Pref");
		ImGui::NextColumn();
		// Editor settings
		if (ImGui::ImageButton(getImGuiIcon().settings, btnSize)) {
			SHARD3D_NOIMPL;
		}
		ImGui::TextWrapped("Config");
		ImGui::NextColumn();
		if (ImGui::ImageButton(getImGuiIcon().layout, btnSize)) {
			SHARD3D_NOIMPL;
		}
		ImGui::TextWrapped("Layout");
		ImGui::NextColumn();
		if (ImGui::ImageButton(getImGuiIcon().launchgame, btnSize)) {
			ShellExecuteA(nullptr, "open", "Shard3DRuntime.exe", std::string("\"" + ProjectSystem::getProjectAsset() + "\" " + "\"" + frameInfo.level->levelAsset.getAsset() + "\"").c_str(), "", true);
		}
		ImGui::TextWrapped("Launch");
		ImGui::NextColumn();
		if (ImGui::ImageButton(getImGuiIcon().viewport, btnSize)) {
			SHARD3D_NOIMPL;
		}
		ImGui::TextWrapped("View");
		ImGui::NextColumn();
		
		// end
		ImGui::Columns(1);
		ImGui::PopStyleVar();
		ImGui::PopStyleColor();
		//ImGui::End();
	}
	void Editor::renderQuickBarTerrain(FrameInfo2& frameInfo, LevelRenderInstance* levelRenderInstance) {
		//ImGui::Begin("_editor_quickbar_terrain", nullptr, ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoTitleBar);
		ImVec2 btnSize = { 48.f, 48.f };
		float panelWidth = ImGui::GetContentRegionAvail().x;
		int columnCount = std::max((int)(panelWidth / (btnSize.x + 16.f))// <--- thumbnail size (96px) + padding (16px)
			, 1);
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4());
		ImGui::PushStyleVar(ImGuiStyleVar_FrameBorderSize, 0.f);
		ImGui::Columns(columnCount, 0, false);
		// begin

		if (!terrainEditSystem->editingTerrainActor) {
			terrainEditSystem->editingTerrainActor = levelRenderInstance->getRenderObjects().terrainList->getTerrain();
			terrainEditSystem->editingActor = levelRenderInstance->getRenderObjects().terrainList->getActor();
			terrainEditSystem->createEditorObjects();
		}

		// Save/Load
		if (ImGui::ImageButton(getImGuiIcon().save, btnSize)) {
			if (frameInfo.level->simulationState != PlayState::Stopped) {
				SHARD3D_ERROR_E("Cannot save level that is running!"); return;
			}
			AssetID terr = terrainEditSystem->exportTerrain(
				frameInfo.level->levelAsset.getAsset().substr(0, frameInfo.level->levelAsset.getAsset().find_last_of("/")),
				frameInfo.level->levelAsset.getAsset().substr(frameInfo.level->levelAsset.getAsset().find_last_of("/") + 1) + "_terrain");
			if (terr) {
				terrainEditSystem->editingActor.getComponent<Components::TerrainComponent>().terrainAsset = terr;
			}
		}
		ImGui::TextWrapped("Save");
		ImGui::NextColumn();
		if (ImGui::ImageButton(getImGuiIcon().load, btnSize)) {
			if (MessageDialogs::show("This will overwrite the current terrain, and unsaved changes will be lost! Are you sure you want to continue?", "WARNING!", MessageDialogs::OPTYESNO | MessageDialogs::OPTICONEXCLAMATION | MessageDialogs::OPTDEFBUTTON2) == MessageDialogs::RESYES) {
				SHARD3D_NOIMPL;
			}
		}
		ImGui::TextWrapped("Load");
		ImGui::NextColumn();
		ImGui::NextColumn();
		if (ImGui::ImageButton(enabledTerrainPainting == 0 ? getImGuiIcon().play : getImGuiIcon().stop, btnSize)) {
			enabledTerrainPainting = !enabledTerrainPainting;
		}
		ImGui::TextWrapped("Enable Painting");
		ImGui::NextColumn();
		if (ImGui::ImageButton(terrainBrush.paintingType == 0 ? getImGuiIcon().edit_position_add : getImGuiIcon().edit_position_rmv, btnSize)) {
			terrainBrush.paintingType = 1 - terrainBrush.paintingType;
		}
		if (ImGui::IsItemHovered())
			ImGui::SetTooltip(terrainBrush.paintingType == 0 ? "Height Map" : "Material Map");
		ImGui::TextWrapped("Paint Mode");
		ImGui::NextColumn();
		ImGui::NextColumn();
		if (terrainBrush.paintingType == 1) {
			if (ImGui::Button(("#" + std::to_string(terrainBrush.currentMaterial)).c_str(), btnSize)) {
				terrainBrush.currentMaterial = (terrainBrush.currentMaterial + 1) % 4;
			}
			ImGui::TextWrapped("Material Index");
			ImGui::NextColumn();
		}
		if (ImGui::ImageButton(getImGuiIcon().settings, btnSize)) {
			ImGui::OpenPopup("dropdownTerrainBrushOpenPOPUPMENU");
		}
		ImGui::TextWrapped("Brush");
		if (ImGui::BeginPopup("dropdownTerrainBrushOpenPOPUPMENU", ImGuiPopupFlags_None)) {
			ImGui::Combo("Brush Action", (int*)&terrainBrush.brushConfig, "Ascend\0Descend\0Level");
			ImGui::DragFloat("Brush Radius", &terrainBrush.config.radius, 0.1f, 0.1f, 100.0f);
			ImGui::DragFloat("Brush Strength", &terrainBrush.config.weight, 0.1f, 0.f, 100.0f, "%.3f", ImGuiSliderFlags_Logarithmic);
			ImGui::DragFloat("Perlin Intensity", &terrainBrush.config.perlinIntensity, 0.1f, 0.0f, 10.0f);
			ImGui::DragFloat("Perlin Scale", &terrainBrush.config.perlinScale, 1.0f, 1.0f, 1000.0f);
			ImGui::BeginDisabled(terrainBrush.brushConfig != TerrainBrushConfig::Average);
			ImGui::DragFloat("Brush Height", &terrainBrush.brushHeight, 0.1f, -1000.f, 1000.0f, "%.3f", ImGuiSliderFlags_Logarithmic);
			ImGui::DragFloat("Height Cutoff Fac.", &terrainBrush.heightCutoffFactor, 0.01f, 0.0f, 1.0f);
			ImGui::EndDisabled();
			ImGui::EndPopup();
		}
		ImGui::NextColumn();
		if (ImGui::ImageButton(getImGuiIcon().viewport, btnSize)) {
			ImGui::OpenPopup("dropdownTerrainBrushFalloffOpenPOPUPMENU");
		}
		ImGui::TextWrapped("Brush Falloff");

		if (ImGui::BeginPopup("dropdownTerrainBrushFalloffOpenPOPUPMENU", ImGuiPopupFlags_None)) {
			if (ImGui::Button("Linear")) {
				terrainBrush.config.falloff = TerrainPaintWeightFalloff::Linear;
				ImGui::CloseCurrentPopup();
			}
			if (ImGui::Button("Quadratic")) {
				terrainBrush.config.falloff = TerrainPaintWeightFalloff::Quadratic;
				ImGui::CloseCurrentPopup();
			}
			if (ImGui::Button("Cubic")) {
				terrainBrush.config.falloff = TerrainPaintWeightFalloff::Cubic;
				ImGui::CloseCurrentPopup();
			}
			if (ImGui::Button("Exponential 2")) {
				terrainBrush.config.falloff = TerrainPaintWeightFalloff::Exponential2;
				ImGui::CloseCurrentPopup();
			}
			ImGui::EndPopup();
		}
		ImGui::NextColumn();
		if (ImGui::ImageButton(getImGuiIcon().preview, btnSize)) {
			ImGui::OpenPopup("dropdownTerrainViewOpenPOPUPMENU");
		}
		ImGui::TextWrapped("Terrain View");
		ImGui::NextColumn();

		if (ImGui::BeginPopup("dropdownTerrainViewOpenPOPUPMENU", ImGuiPopupFlags_None)) {
			ImGui::Checkbox("Wireframe", &terrainView.wireframe);
			ImGui::Checkbox("Brush Cursor", &terrainView.displayHover);
			ImGui::Separator();
			if (ImGui::Button("Default")) {
				terrainView.view = TerrainEditorView::ViewDefault;
			}
			if (ImGui::Button("Tiles")) {
				terrainView.view = TerrainEditorView::ViewTiles;
			}
			if (ImGui::Button("Height")) {
				terrainView.view = TerrainEditorView::ViewHeight;
			}
			if (ImGui::Button("Normals")) {
				terrainView.view = TerrainEditorView::ViewNormals;
			}
			if (ImGui::Button("Material Complexity")) {
				//terrainView.view = TerrainEditorView::ViewNormals;
			}
			if (ImGui::Button("LOD")) {
				terrainView.view = TerrainEditorView::ViewLOD;
			}
			if (ImGui::Button("In Game")) {
				terrainView.view = TerrainEditorView::ViewInGame;
			}

			ImGui::EndPopup();
		}
		ImGui::NextColumn();
		ImGui::NextColumn();
		if (ImGui::ImageButton(getImGuiIcon().gizmo_rotate, btnSize)) {
			terrainEditSystem->editingActor.getComponent<Components::TerrainComponent>().physData = terrainEditSystem->generatePhysicsTiles();
		}
		ImGui::TextWrapped("Rebuild Physics");
		ImGui::NextColumn();
		if (ImGui::ImageButton(getImGuiIcon().edit_position, btnSize)) {
			std::vector<TerrainMaterial*> materials;
			for (int i = 0; i < terrainEditSystem->editingActor.getComponent<Components::TerrainComponent>().maxMaterials; i++) {
				materials.push_back(dynamic_cast<TerrainMaterial*>(resourceSystem->retrieveMaterial(terrainEditSystem->editingActor.getComponent<Components::TerrainComponent>().materials[i])));
			}
			terrainEditSystem->editingTerrainActor->populateMaterialBuffer(resourceSystem, materials);
		}
		ImGui::TextWrapped("Rebuild Terrain Material");
		ImGui::NextColumn();
		// end
		ImGui::Columns(1);
		ImGui::PopStyleVar();
		ImGui::PopStyleColor();
		//ImGui::End();
	}
	
	void Editor::renderMenuBar(FrameInfo2& frameInfo) {
		if (ImGui::BeginMenu("File")) {
			ImGui::TextDisabled("SHARD3D %s", ENGINE_VERSION.toString().c_str());
			ImGui::Separator();
			ImGui::BeginDisabled(frameInfo.level->simulationState != PlayState::Stopped);
			if (ImGui::MenuItem("New Level", "Ctrl+N")) {
				if (MessageDialogs::show("This will destroy the current level, and unsaved changes will be lost! Are you sure you want to continue?", "WARNING!", MessageDialogs::OPTYESNO | MessageDialogs::OPTICONEXCLAMATION | MessageDialogs::OPTDEFBUTTON2) == MessageDialogs::RESYES) {
					levelTreePanel.clearSelectedActor();
					frameInfo.level->killEverything();
				}
			}
			if (ImGui::MenuItem("Load Level...", "Ctrl+O")) {
				if (MessageDialogs::show("This will overwrite the current level, and unsaved changes will be lost! Are you sure you want to continue?", "WARNING!", MessageDialogs::OPTYESNO | MessageDialogs::OPTICONEXCLAMATION | MessageDialogs::OPTDEFBUTTON2) == MessageDialogs::RESYES) {
					std::string filepath = BrowserDialogs::openFile(ENGINE_SHARD3D_LEVELFILE_OPTIONS);
					if (!filepath.empty()) {
						levelTreePanel.clearSelectedActor();
						loadLevel(frameInfo.level, frameInfo.resourceSystem, filepath);
						refreshContext = true;	
					}
				}
			}
			if (ImGui::MenuItem("Save Level...", "Ctrl+S")) {
				saveLevel(frameInfo.level, frameInfo.resourceSystem, frameInfo.level->levelAsset.getAbsolute());
			}
			if (ImGui::MenuItem("Save Level As...", "Ctrl+Shift+S")) {
				std::string filepath = BrowserDialogs::saveFile(ENGINE_SHARD3D_LEVELFILE_OPTIONS);
				if (!filepath.empty()) {
					saveLevel(frameInfo.level, frameInfo.resourceSystem, filepath);
				}
			}
			ImGui::Separator();
			ImGui::EndDisabled();

			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("Edit")) {
			ImGui::TextDisabled("SHARD3D %s", ENGINE_VERSION.toString().c_str());
			ImGui::Separator();
			if (ImGui::BeginMenu("Level Simulation")) {
				ImGui::BeginDisabled(frameInfo.level->simulationState != PlayState::Stopped || frameInfo.level->simulationState == PlayState::Paused);
				if (ImGui::MenuItem("Begin")) {
					playLevel(frameInfo.level, capturedLevel, engineWindow.getGLFWwindow());		
				} ImGui::EndDisabled();
				if (frameInfo.level->simulationState != PlayState::Paused) {
					ImGui::BeginDisabled(frameInfo.level->simulationState != PlayState::Playing); if (ImGui::MenuItem("Pause")) {
						pauseLevel(frameInfo.level, engineWindow.getGLFWwindow());
					} ImGui::EndDisabled();
				}
				else {
					ImGui::BeginDisabled(frameInfo.level->simulationState == PlayState::Playing); if (ImGui::MenuItem("Resume")) {
						resumeLevel(frameInfo.level, engineWindow.getGLFWwindow());
					} ImGui::EndDisabled();
				}

				ImGui::BeginDisabled(frameInfo.level->simulationState == PlayState::Stopped);
				if (ImGui::MenuItem("End")) {
					levelTreePanel.clearSelectedActor();
					stopLevel(frameInfo.level, capturedLevel, engineWindow.getGLFWwindow(), terrainEditSystem);
					refreshContext = true;
				} ImGui::EndDisabled();
				ImGui::EndMenu();
			}

			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("View")) {
			if (ImGui::BeginMenu("Rendering")) {
				if (ImGui::Checkbox("Preview Game", &GraphicsSettings2::editorPreview.ONLY_GAME)) previewToggleCall(frameInfo.level, GraphicsSettings2::editorPreview.ONLY_GAME);
				ImGui::Separator();
				ImGui::Checkbox("Grid", &GraphicsSettings2::editorPreview.V_GRID);
				ImGui::Checkbox("Billboards", &GraphicsSettings2::editorPreview.V_EDITOR_BILLBOARDS);
				ImGui::BeginDisabled();
				ImGui::Checkbox("HUD", &GraphicsSettings2::editorPreview.V_GUI);
				ImGui::EndDisabled();
				ImGui::Checkbox("Physics Actors", &GraphicsSettings2::editorPreview.V_PHYSICS);
				ImGui::Checkbox("Mesh Cull Volumes", &GraphicsSettings2::editorPreview.V_CULLVOLUMES);
				ImGui::EndMenu();
			}
			if (ImGui::BeginMenu("Renderpasses")) {
				ImGui::RadioButton("PPFX Output", &renderpassRadioButton, 0);
				ImGui::RadioButton("Depth Buffer", &renderpassRadioButton, 1);
				if (ImGui::BeginMenu("G-Buffer")) {
					ImGui::RadioButton("Diffuse (RGBC)", &renderpassRadioButton, 2);
					ImGui::RadioButton("Material (SS)", &renderpassRadioButton, 3);
					ImGui::RadioButton("Emission", &renderpassRadioButton, 4);
					ImGui::BeginDisabled();
					ImGui::RadioButton("Compressed Normal", &renderpassRadioButton, 5); // doesnt work since it's a R32_UINT attachment
					ImGui::EndDisabled();
					ImGui::RadioButton("Velocity Buffer (XYZ)", &renderpassRadioButton, 6);
					ImGui::EndMenu();
				}
				if (dbuffer0Dat.tex == nullptr) ImGui::BeginDisabled();
				if (ImGui::BeginMenu("D-Buffer")) {
					ImGui::RadioButton("Diffuse (RGBA)", &renderpassRadioButton, 8);
					ImGui::RadioButton("Material (SSCA)", &renderpassRadioButton, 9);
					ImGui::RadioButton("Normal Tg Space (XYZA)", &renderpassRadioButton, 10);
					ImGui::EndMenu();
				}
				if (dbuffer0Dat.tex == nullptr) ImGui::EndDisabled();
				if (lbuffer0Dat.tex == nullptr) ImGui::BeginDisabled();
				if (ImGui::BeginMenu("L-Buffer")) {
					ImGui::RadioButton("Diffuse (RGB)", &renderpassRadioButton, 14);
					ImGui::RadioButton("Specular (RGB)", &renderpassRadioButton, 15);
					ImGui::EndMenu();
				}
				if (lbuffer0Dat.tex == nullptr) ImGui::EndDisabled();
				if (ImGui::BeginMenu("Transparency (Weighted Blending)")) {
					ImGui::RadioButton("Accumulative Buffer", &renderpassRadioButton, 11);
					ImGui::RadioButton("Revealage Buffer", &renderpassRadioButton, 12);
					ImGui::EndMenu();
				}
				if (ssaoDat.tex == nullptr) ImGui::BeginDisabled();
				ImGui::RadioButton("SSAO", &renderpassRadioButton, 13);
				if (ssaoDat.tex == nullptr) ImGui::EndDisabled();
				ImGui::EndMenu();
			}
			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Debug")) {
			ImGui::TextDisabled("Shard3D Debug menu");
			ImGui::Checkbox("Stylizer", &showStylizersWindow);
			ImGui::Checkbox("ImGuiDemo", &showDemoWindow);
			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Actions")) {
			if (ImGui::MenuItem("Recompile Surface Materials", NULL /*make sure to add some sort of shardcut */)) {
				std::thread capture([&]() { // solve this with queue and command buffer sync
					GlobalFrameState::lockFrame = true; // prevent new frames from starting because we're destroying resources that are also being used on the main render thread
					while (true) {
						SHARD3D_VERBOSE("Waiting for frame to finish...");  if (!GlobalFrameState::isFrameInProgress) break;
					} // wait for frame to finish
					SHARD3D_ASSERT(!GlobalFrameState::isFrameInProgress);
					vkDeviceWaitIdle(engineDevice.device());

					DeferredRenderSystem::recompileShaders();
					GlobalFrameState::lockFrame = false; // unlock since we're done
				});
				capture.detach();
			}
			if (ImGui::MenuItem("Recompile Post Processing Materials", NULL /*make sure to add some sort of shardcut */)) {
				//MaterialSystem::recompilePPO();
			}
			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("Window")) {
			// Disabling fullscreen would allow the window to be moved to the front of other windows,
			// which we can't undo at the moment without finer window depth/z control.

			ImGui::TextDisabled("SHARD3D 0.1");
			ImGui::Separator();
			if (ImGui::MenuItem("Engine Settings", NULL /*make sure to add some sort of shardcut */)) { showEngineSettingsWindow = true; }
			if (ImGui::MenuItem("SHARD3D Settings", NULL /*make sure to add some sort of shardcut */)) { /*show editor win*/ }
			ImGui::Separator();
			if (ImGui::MenuItem("Game Graphics Settings", NULL /*make sure to add some sort of shardcut */)) { showGraphicsSettingsWindow = true; }
			ImGui::Separator();
			if (ImGui::MenuItem("Material Builder", NULL /*make sure to add some sort of shardcut */)) {
				showTest = true;
			}
			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("Help")) {
#ifdef WIN32
			if (ImGui::MenuItem("Main Website")) { ShellExecuteA(nullptr, "open", "https://www.shard3d.com", nullptr, nullptr, false); }
			if (ImGui::MenuItem("Documentation")) { ShellExecuteA(nullptr, "open", "https://docs.shard3d.com", nullptr, nullptr, false); }
			if (ImGui::MenuItem("SHARD3D")) { ShellExecuteA(nullptr, "open", "https://docs.shard3d.com/SHARD3D.html", nullptr, nullptr, false); }
#endif  
#ifdef __linux__ 
			ImGui::MenuItem("Unsupported");
#endif
			ImGui::EndMenu();
		}
		if (ImGui::MenuItem("Credits")) showCredits = true;
	}


	void Editor::eventEvent(Events::Event& e) {
		SHARD3D_EVENT_CREATE_DISPATCHER(e);
		dispatcher.dispatch<Events::MouseButtonDownEvent>(SHARD3D_EVENT_BIND_VOID(Editor::mouseButtonDownEvent));
		dispatcher.dispatch<Events::MouseButtonReleaseEvent>(SHARD3D_EVENT_BIND_VOID(Editor::mouseButtonReleaseEvent));
		dispatcher.dispatch<Events::MouseHoverEvent>(SHARD3D_EVENT_BIND_VOID(Editor::mouseHoverEvent));
		dispatcher.dispatch<Events::MouseScrollEvent>(SHARD3D_EVENT_BIND_VOID(Editor::mouseScrollEvent));
		dispatcher.dispatch<Events::KeyDownEvent>(SHARD3D_EVENT_BIND_VOID(Editor::keyDownEvent));
		dispatcher.dispatch<Events::KeyReleaseEvent>(SHARD3D_EVENT_BIND_VOID(Editor::keyReleaseEvent));
		dispatcher.dispatch<Events::KeyPressEvent>(SHARD3D_EVENT_BIND_VOID(Editor::keyPressEvent));
	}

	bool Editor::mouseButtonDownEvent(Events::MouseButtonDownEvent& e) {
		ImGuiIO& io = ImGui::GetIO();
		int bc = e.getButtonCode();
		//io.AddMouseButtonEvent(bc, true);
		if (e.getButtonCode() == GLFW_MOUSE_BUTTON_LEFT && (!hoveredActor.isInvalid()) && !ImGuizmo::IsOver())
			levelTreePanel.selectedActor = hoveredActor;
		return false;
	}

	bool Editor::mouseButtonReleaseEvent(Events::MouseButtonReleaseEvent& e)  {
		ImGuiIO& io = ImGui::GetIO();
		int bc = e.getButtonCode();
		//io.AddMouseButtonEvent(bc, false);

		return false;
	}

	bool Editor::mouseHoverEvent(Events::MouseHoverEvent& e) {
		ImGuiIO& io = ImGui::GetIO();
		//io.AddMousePosEvent(e.getXPos(), e.getYPos());
		return false;
	}

	bool Editor::mouseScrollEvent(Events::MouseScrollEvent& e) {
		ImGuiIO& io = ImGui::GetIO();
		//io.AddMouseWheelEvent(e.getXOffset(), e.getYOffset());


		return false;
	}
	
	bool Editor::keyDownEvent(Events::KeyDownEvent& e) {
		ImGuiIO& io = ImGui::GetIO();
		int kc = e.getKeyCode();
		//io.AddKeyEvent((ImGuiKey)kc, true);

		{
			switch (kc) {
			case(GLFW_KEY_Q):
				gizmoType = ImGuizmo::UNIVERSAL;
				break;
			case(GLFW_KEY_E):
				gizmoType = ImGuizmo::SCALE;
				break;
			case(GLFW_KEY_R):
				gizmoType = ImGuizmo::ROTATE;
				break;
			case(GLFW_KEY_T):
				gizmoType = ImGuizmo::TRANSLATE;
				break;
			}
		}

		return false;
	}

	bool Editor::keyReleaseEvent(Events::KeyReleaseEvent& e)  {
		ImGuiIO& io = ImGui::GetIO();
		int kc = e.getKeyCode();
		//io.AddKeyEvent((ImGuiKey)kc, true);

		return false;
	}

	bool Editor::keyPressEvent(Events::KeyPressEvent& e)  {
		ImGuiIO& io = ImGui::GetIO();
		//io.AddInputCharacter((unsigned int)e.getKeyCode());
		return false;
	}
}