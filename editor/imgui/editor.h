#pragma once

#include <Shard3DEditorKit/kit.h>
#include <Shard3D/core/vulkan_api/graphics_pipeline.h>
#include <Shard3D/systems/rendering/velocity_buffer_system.h>

// panels
#include <Shard3DEditorKit/tools/level_properties_panel.h>
#include <Shard3DEditorKit/tools/editor_ball_controller.h>
#include <Shard3DEditorKit/tools/default_component_panels.h>
#include <Shard3DEditorKit/rendering/phyiscs_actor_preview_renderer.h>

#include "../panels/level_tree_panel.h"
#include "../panels/level_peekers.h"
#include "../panels/material_builder_panel.h"
#include "../panels/physics_builder_panel.h"
#include "../panels/mesh_animation_preview_panel.h"
#include "../panels/texture_config_panel.h"
#include "../panels/hud_builder_panel.h"

#include <Shard3D/events/mouse_event.h>
#include <Shard3D/events/key_event.h>
#include "../panels/settings_panel.h"
#include <Shard3D/core/rendering/camera_render_instance.h>
#include <Shard3DEditorKit/rendering/point_renderer.h>
#include <Shard3DEditorKit/rendering/terrain_editor.h>
namespace Shard3D {
	class HUDLayer;
	class HUD;
	class Editor {
	public:
		Editor(S3DDevice& dvc, S3DWindow& wnd, ResourceSystem* resourceSystem, TerrainEditSystem* terrainEditSystem, uPtr<S3DDescriptorSetLayout>& globalSetLayout, uPtr<S3DDescriptorSetLayout>& sceneSetLayout, uPtr<S3DDescriptorSetLayout>& skeletonRigSetLayout, uPtr<S3DDescriptorSetLayout>& terrainSetLayout, VkRenderPass renderPass, ImGuiContext* context);
		~Editor();

		void setRenderTargetData(CameraRenderInstance* tgInst);
		void detach();
		void renderCameras(VkCommandBuffer commandBuffer, uint32_t frameIndex, VkFence fence, FrameInfo2& frameInfo);
		void render(FrameInfo2& frameInfo, LevelRenderInstance* levelRenderInstance);
		void draw(VkCommandBuffer commandBuffer);
		void drawHUDPreview(VkCommandBuffer commandBuffer, uint32_t frameIndex);
		void setHoveredObject(uint32_t handle, Level* level);
		void setMouseHoveringCoord(glm::vec4 position) { hoveredCoord = position; }
		void resize(glm::vec2 newSize);

		void showMaterialEditor(AssetID asset);
		void closeMaterialEditor();
		void showPhysicsEditor(AssetID asset);
		void closePhysicsEditor();
		void showHUDEditor(AssetID asset);
		void closeHUDEditor();
		void showSkeletalAnimationPreviewer(AssetID asset);
		void closeSkeletalAnimationPreviewer();
		void showTextureConfigEditor(AssetID asset);
		void closeTextureConfigEditor();

		VkDescriptorSet viewportImage{};

		struct RenderTargetMetadataInfo {
			RenderTargetMetadataInfo(S3DRenderTarget* attachment) {
				if (attachment) {
					format = string_VkFormat(attachment->getAttachmentDescription().format);
					resourceSizeKB = attachment->getResourceSize() / 1000.0f;
					dim = attachment->getDimensions();
					VkDescriptorImageInfo imageInfo = attachment->getDescriptor();
					tex = ImGui_ImplVulkan_AddTexture(imageInfo.sampler, imageInfo.imageView, imageInfo.imageLayout);
				}
			}
			RenderTargetMetadataInfo() = default;
			VkDescriptorSet tex = nullptr;
			glm::ivec3 dim;
			size_t resourceSizeKB;
			const char* format;
			const char* what;
		};
		RenderTargetMetadataInfo gbuffer0Dat, gbuffer1Dat, gbuffer2Dat, gbuffer3Dat,  depthDat, wblendcolaccumDat, wblendcolrevealDat,
			ssaoDat, velocityDat, dbuffer0Dat, dbuffer1Dat, dbuffer2Dat, lbuffer0Dat, lbuffer1Dat;
		bool isViewportHovered = false;

		float materialViewportAR = 1.0;
		float animationPreviewerAR = 1.0;
		float physicsViewportAR = 1.0;
		float hudLayerViewportAR = 1.0;
		bool isEditorMaterialViewportHovered = false;
		bool isEditorPhysicsViewportHovered = false;
		bool isAnimationPreviewerViewportHovered = false;
		bool isEditorHUDViewportHovered = false;
		controller::EditorBallController editorMaterialCameraController{};
		controller::EditorBallController editorPhysicsCameraController{};
		controller::EditorBallController previewerAnimationCameraController{};

		uPtr<PhysicsActorPreviewRenderer> physicsActorDisplayRenderer;
		uPtr<PointRenderer> physicsActorHullPointRenderer;

		Telemetry* mainCameraTelem;
		LevelRenderInstance* mainLevelRenderInstance{};

		TerrainEditorBrushSettings terrainBrush{};
		bool enabledTerrainPainting = false;
		TerrainEditorVisualizer terrainView{};

		bool isOpenMaterialEditor = false;
		bool isOpenPhysicsEditor = false;
		bool isOpenAnimationPreviewer = false;
		bool isOpenHUDEditor = false;
		bool isOpenTextureEditor = false;
	private:
		void eventEvent(Events::Event& e);
		bool mouseButtonDownEvent(Events::MouseButtonDownEvent& e);
		bool mouseButtonReleaseEvent(Events::MouseButtonReleaseEvent& e);
		bool mouseHoverEvent(Events::MouseHoverEvent& e);
		bool mouseScrollEvent(Events::MouseScrollEvent& e);
		bool keyDownEvent(Events::KeyDownEvent& e);
		bool keyReleaseEvent(Events::KeyReleaseEvent& e);
		bool keyPressEvent(Events::KeyPressEvent& e);
	private:
		void renderViewport(FrameInfo2& frameInfo);
		void renderMaterialEditorViewport(FrameInfo2& frameInfo);
		void renderPhysicsEditorViewport(FrameInfo2& frameInfo);
		void renderAnimationPreviewerViewport(FrameInfo2& frameInfo);
		void renderHUDEditorViewport(FrameInfo2& frameInfo);
	private:

		void detachTag() { }
		void createIcons();
		void renderQuickBar(FrameInfo2& frameInfo, LevelRenderInstance* levelRenderInstance);
		void renderQuickBarLevel(FrameInfo2& frameInfo);
		void renderQuickBarTerrain(FrameInfo2& frameInfo, LevelRenderInstance* levelRenderInstance);
		void renderMenuBar(FrameInfo2& frameInfo);
		int width;
		int height;

		sPtr<ECS::Level> capturedLevel{};

		S3DWindow& engineWindow;
		S3DDevice& engineDevice;
		ResourceSystem* resourceSystem;

		controller::EditorMovementController editorCameraController{};
		Actor hoveredActor{};
		glm::vec4 hoveredCoord{};
		ImGuiContext* context;
		uPtr<S3DDescriptorSetLayout>& globalSetLayout;
		uPtr<S3DDescriptorSetLayout>& sceneSetLayout;
		uPtr<S3DDescriptorSetLayout>& skeletonRigSetLayout;
		uPtr<S3DDescriptorSetLayout>& terrainSetLayout;

		float timeSinceLastSecond{};
		float deltaTimeFromLastSecond{};

		bool showTest = false;
		bool showEngineSettingsWindow = false;
		bool showGraphicsSettingsWindow = false;
		bool showStylizersWindow = false;
		bool showCredits = false;
		bool showDemoWindow = false;
		struct EngineSettings {	
			// WINDOW
			int DEFAULT_WIDTH{};
			int DEFAULT_HEIGHT{};
			bool Resizable{};
			char WindowName[64]{};

			// RENDERING
			int ViewCombo{};
			float NearClipDistance{};
			float FarClipDistance{};
			float FOV{};
			float defaultBGColor[3] = {0.f, 0.f, 0.f};
			bool pbr{};
		};
		struct EditorPreferences {
			// WINDOW
			bool antiAliasedUI = true;
			ImGuiColorEditFlags displayFloatOr255 = ImGuiColorEditFlags_Float;
		};

		PlanarReflectionSystem* planarReflectionSystem;
		EditorPreferences edpref{};
		EngineSettings enset{};

		LevelTreePanel levelTreePanel{};
		LevelPropertiesPanel levelPropertiesPanel{};
		LevelPeekingPanel levelPeekPanel{};
		DefaultComponentPanels componentActorPanels{};
		AssetExplorerPanel assetExplorerPanel{};
		SettingsPanel settingsPanel{};

		TerrainEditSystem* terrainEditSystem;

		// material editor
		VkDescriptorSet materialEditorViewport{};
		sPtr<Level> materialEditorLevel{};
		uPtr<LevelRenderInstance> materialEditorLevelRenderInstance{};
		uPtr<CameraRenderInstance> materialEditorRenderViewport{};

		// physics editor
		VkDescriptorSet physicsEditorViewport{};
		sPtr<Level> physicsEditorLevel{};
		uPtr<LevelRenderInstance> physicsEditorLevelRenderInstance{};
		uPtr<CameraRenderInstance> physicsEditorRenderViewport{};

		// animation previewer
		VkDescriptorSet animationPreviewerViewport{};
		sPtr<Level> animationPreviewerLevel{};
		uPtr<LevelRenderInstance> animationPreviewerLevelRenderInstance{};
		uPtr<CameraRenderInstance> animationPreviewerRenderViewport{};

		// hud editor
		VkDescriptorSet hudLayerEditorViewport{};
		SHUD::ResourceObject defaultUIPrevTexture;
		SHUD::VulkanContext* uiPreviewBuilderContext;
		S3DRenderTarget* uiPreviewBuilderRenderTarget;
		S3DRenderPass* uiPreviewBuilderRenderPass;
		S3DFramebuffer* uiPreviewBuilderFramebuffer;

		MaterialBuilderPanel materialBuilderPanel{};
		PhysicsBuilderPanel physicsBuilderPanel{};
		MeshAnimationPreviewPanel animationPreviewPanel{};
		TextureConfigPanel textureConfigPanel{};
		HUDBuilderPanel hudBuilderPanel{};

		int gizmoType = 14463;
		bool gizmoActive = false;

		bool refreshContext = true;
//Gizmo levelGizmo;


		int renderpassRadioButton = 0;
	};

}