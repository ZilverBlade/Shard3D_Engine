#include "application.h"
#include <Shard3D/utils/logger.h>
#include <Shard3D/systems/handlers/project_system.h>
/*
	Shard3D 1.0 (2022-2023) created by ZilverBlade
*/


void initEngineFeatures() {
	Shard3D::LOGGER::init(); 
	VkResult result = volkInitialize();
	if (result != VK_SUCCESS) {
		SHARD3D_FATAL("Failed to intialize vulkan! (volk.h) Does your system support vulkan?");
	}
}

int main(int argc, char* argv[], char* env[]) {

	initEngineFeatures();

	//std::vector<glm::vec4> oooffsets;
	//oooffsets.resize(128);
	//std::vector<glm::vec4> oooffsets2;
	//oooffsets2.resize(128);
	//simdjson::dom::parser parser;
	//simdjson::padded_string json = simdjson::padded_string::load("D:\\Directories\\Downloads\\msdf-atlas-gen-1.2.2-win32\\msdf-atlas-gen\\s3ddefaultfont_metadata.json");
	//auto& data = parser.parse(json);
	//
	//for (auto& entry : data["glyphs"].get_array().value()) {
	//	if (entry["atlasBounds"].error() != simdjson::SUCCESS) continue;
	//	oooffsets[entry["unicode"].get_int64().value()] = glm::vec4{
	//		entry["atlasBounds"]["left"].get_double().value(),
	//		entry["atlasBounds"]["bottom"].get_double().value(),
	//		entry["atlasBounds"]["right"].get_double().value(),
	//		entry["atlasBounds"]["top"].get_double().value()
	//	} / glm::vec4(256.0f);
	//	oooffsets2[entry["unicode"].get_int64().value()] = glm::vec4{
	//		entry["planeBounds"]["left"].get_double().value(),
	//		entry["planeBounds"]["bottom"].get_double().value(),
	//		entry["planeBounds"]["right"].get_double().value(),
	//		entry["planeBounds"]["top"].get_double().value()
	//	};
	//}
	//
	//for (int i = 0; i < 128; i++) {
	//	auto x = oooffsets[i];
	//	auto y = oooffsets2[i];
	//	SHARD3D_LOG("{8} fvec4( {0}, {1}, {2}, {3} ), fvec4( {4}, {5}, {6}, {7} ) {9},", x.x, x.y, x.z, x.w, y.x, y.y, y.z, y.w, "{", "}");
	//}




	char wkd[256];
	getcwd(wkd, 256);
	Shard3D::AssetID project{ std::string(wkd) + "\\testproject.s3dproj" };
	if (argc == 2) {
		project = Shard3D::AssetID(argv[1]);
	}
	Shard3D::ProjectSystem::init(project.getAsset(), true);
	Shard3D::Application app{ project };
	app.run();
	return EXIT_SUCCESS;
}
