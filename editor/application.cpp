#include "application.h"

#include <thread>
#include <future>
#include <fstream>
#include <vector>
#include <Shard3D/systems/systems.h>
#include <Shard3D/systems/handlers/vertex_arena_allocator.h>
#include <Shard3D/systems/rendering/variance_filtered_ao_system.h>
#include <Shard3D/core/asset/assetmgr.h>
#include <Shard3D/core/ecs/level_simulation.h>

#include "imgui/editor.h"

#include <Shard3D/core/misc/graphics_settings.h>
#include <Shard3D/core/misc/engine_settings.h>

#include <Shard3D/scripting/script_engine.h>
#include <Shard3DEditorKit/rendering/object_picking_system.h>
#include <Shard3DEditorKit/rendering/highlight_renderer.h>
#include <Shard3DEditorKit/rendering/arrow_renderer.h>
#include <Shard3DEditorKit/tools/editor_pref.h>

#include <Shard3D/global.h>
#include <Shard3D/core/vulkan_api/bindless.h>
#include <Shard3D/systems/handlers/project_system.h>
#include <Shard3D/core/rendering/camera_render_instance.h>
#include <ReactorPhysicsEngine/colliders/convex_hull.h>
#include <Shard3DEditorKit/gui/icons.h>
namespace Shard3D {
	Application::Application(AssetID project_) : project(project_) {
		setupEngineFeatures();
		createRenderPasses();
	}
	Application::~Application() {
	}

	void Application::createRenderPasses() {
		
	}

	void Application::destroyRenderPasses() {

	}

	void Application::setupEngineFeatures() {
	//	EngineAudio::init();
		ScriptEngine::init(&engineWindow);

		setWindowCallbacks();
		
		resourceSystem = new ResourceSystem(engineDevice,  ProjectSystem::getGraphicsSettings().renderer.framesInFlight);

		_special_assets::_editor_icons_load(engineDevice, resourceSystem->getDescriptor());
		EditorPreferences::load();

		ShaderSystem::init();
	}

	void Application::setWindowCallbacks() {
		engineWindow.setEventCallback(std::bind(&Application::eventEvent, this, std::placeholders::_1));
	}

	void Application::windowResizeEvent(Events::WindowResizeEvent& e) {
		createRenderPasses();
	}

	void Application::eventEvent(Events::Event& e) {
	}

	static void createEngineAssets(ResourceSystem* resourceSystem) {
		TextureImportInfo importInfo{};
		TextureLoadInfo info{};
		info.filter = TextureFilter::Nearest;
		info.compression = TextureCompression::Lossless; // compression on textures this small is pointless and can cause colour artifacts
		info.channels = TextureChannels::RGB;
		PackageInvocationIndex id{};
		id.index = 0;
		id.resource = ResourceType::Texture2D;
		info.enableMipMaps = true;
		AssetManager::importTexture2D("assets/engine/textures/null_tex.png", "assets/engine/textures/null_tex", resourceSystem, importInfo, info, id);		// Purple checkerboard texture
		AssetManager::importTexture2D("assets/engine/textures/null_mem.png", "assets/engine/textures/null_mem", resourceSystem, importInfo, info, id);		// Orange checkerboard texture

		info.colorSpace = TextureColorSpace::Linear;
		AssetManager::importTexture2D("assets/engine/textures/null_mat.png", "assets/engine/textures/null_mat", resourceSystem, importInfo, info, id);		// Blank checkerboard texture
		info.enableMipMaps = false;
		AssetManager::importTexture2D("assets/engine/textures/0x000000.png", "assets/engine/textures/0x000000", resourceSystem, importInfo, info, id);		// Black texture
		AssetManager::importTexture2D("assets/engine/textures/0xffffff.png", "assets/engine/textures/0xffffff", resourceSystem, importInfo, info, id);		// White texture
		AssetManager::importTexture2D("assets/engine/textures/blue_noise.png", "assets/engine/textures/blue_noise", resourceSystem, importInfo, info, id);		// Blue noise texture
		

		info.channels = TextureChannels::RGB;
		AssetManager::importTexture2D("assets/engine/textures/0x8080ff.png", "assets/engine/textures/0x8080ff", resourceSystem, importInfo, info, id);
		id.resource = ResourceType::TextureCube;
		/*
			ORDER:
				RIGHT,
				LEFT,
				TOP,
				BOTTOM,
				FRONT,
				BACK
		*/
		std::vector<std::string> bcTextures{
				"assets/engine/textures/cubemaps/blank_cube/right.png",
				"assets/engine/textures/cubemaps/blank_cube/left.png",
				"assets/engine/textures/cubemaps/blank_cube/top.png",
				"assets/engine/textures/cubemaps/blank_cube/bottom.png",
				"assets/engine/textures/cubemaps/blank_cube/front.png",
				"assets/engine/textures/cubemaps/blank_cube/back.png"
		};
		std::vector<std::string> sky0Textures{
			"assets/engine/textures/cubemaps/sky0/right.png",
			"assets/engine/textures/cubemaps/sky0/left.png",
			"assets/engine/textures/cubemaps/sky0/top.png",
			"assets/engine/textures/cubemaps/sky0/bottom.png",
			"assets/engine/textures/cubemaps/sky0/front.png",
			"assets/engine/textures/cubemaps/sky0/back.png"
		};
		std::vector<std::string> sky1Textures{
			"assets/engine/textures/cubemaps/sky1/right.png",
			"assets/engine/textures/cubemaps/sky1/left.png",
			"assets/engine/textures/cubemaps/sky1/top.png",
			"assets/engine/textures/cubemaps/sky1/bottom.png",
			"assets/engine/textures/cubemaps/sky1/front.png",
			"assets/engine/textures/cubemaps/sky1/back.png"
		};
		importInfo.isCubemap = true;
		info.channels = TextureChannels::R;
		info.compression = TextureCompression::BC4;
		info.colorSpace = TextureColorSpace::Linear;

		AssetManager::importTextureCube(bcTextures, "assets/engine/textures/cubemaps/blank_cube", resourceSystem, importInfo, info, id);	// white cubemap
		info.channels = TextureChannels::RGB;
		info.colorSpace = TextureColorSpace::sRGB;
		info.compression = TextureCompression::Lossless; // compression is not really good for skyboxes												   
		AssetManager::importTextureCube(sky0Textures, "assets/engine/textures/cubemaps/sky0", resourceSystem, importInfo, info, id);	// sky cubemap
		AssetManager::importTextureCube(sky1Textures, "assets/engine/textures/cubemaps/sky1", resourceSystem, importInfo, info, id);	// sky cubemap


		id.resource = ResourceType::Model3D;
		Model3DImportInfo m3dImpInfo{};
		m3dImpInfo.combineMeshes = true;
		m3dImpInfo.createMaterials = false;
		m3dImpInfo.calculateTangents = true;
		AssetManager::importMeshes("assets/engine/meshes/null_mdl.fbx", "assets/engine/meshes/null_mdl", resourceSystem, m3dImpInfo, id);		// No Mesh model
		AssetManager::importMeshes("assets/engine/meshes/cube.obj", "assets/engine/meshes/cube", resourceSystem, m3dImpInfo, id);
		auto wgrid = make_rPtr<SurfaceMaterial>(resourceSystem);
		wgrid->diffuseColor = { 0.7f,0.7f,0.7f };
		wgrid->diffuseTex = ResourceSystem::coreAssets.t_errorMaterialTexture;
		wgrid->specular = 0.5f;
		wgrid->glossiness = 0.2f;
		AssetManager::createMaterial("assets/engine/materials/world_grid.s3dasset", wgrid.get());		// Opaque surface material (world grid)
		auto ntgrid = make_rPtr<SurfaceMaterial>(resourceSystem);
		ntgrid->diffuseColor = { 0.7f,0.7f,0.7f };
		ntgrid->diffuseTex = ResourceSystem::coreAssets.t_errorTexture;
		ntgrid->specular = 0.5f;
		ntgrid->glossiness = 0.4f;
		AssetManager::createMaterial("assets/engine/materials/missing_texture.s3dasset", ntgrid.get());
	}

	void Application::run() {
		{
#if 0
			createEngineAssets(resourceSystem);
#endif
			PhysicsAsset pyramideDefault = {};
			RPhys::ConvexHullCollider pyramideCollider = RPhys::ConvexHullCollider({ {1.0, -1.0, 1.0},{1.0, -1.0, -1.0},{-1.0, -1.0, 1.0},{-1.0, -1.0, -1.0},{0.0, 1.0, 0.0} });
			pyramideDefault.convexPoints = pyramideCollider.getPoints();
			pyramideDefault.computedCOM = pyramideCollider.getCenterOfMass();
			pyramideDefault.computedInteriaTensor = pyramideCollider.getIntertiaTensor();
			pyramideDefault.colliderType = PhysicsColliderType::ConvexHullAsset;
			//AssetManager::createPhysicsAsset("assets/engine/physics/pyramide.s3dasset", pyramideDefault);

		}
		resourceSystem->loadCoreAssets();
		resourceSystem->loadTextureCube(AssetID("engine/textures/cubemaps/sky1.s3dasset"));
		
		// level must be created after resource handler has been initialised due to the fact that the editor camera is created with post processing materials
		SHARD3D_INFO_E("Constructing Level Pointer");
		level = make_sPtr<ECS::Level>(resourceSystem);
		level->createSystem();
		ImGuiContext* context = ImGui::CreateContext();
		{
			//CSimpleIniA ini;
			//ini.SetUnicode();
			//ini.LoadFile(EDITOR_SETTINGS_PATH);

			ImGuiInitializer::init(engineDevice, engineWindow, context, engineSwapChain->getRenderPass(), engineRenderer.getQueue(), "pref/editor_layout.ini", false);
		}
		createImGuiIcons();

		globalSetLayout = S3DDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_COMPUTE_BIT)
			.build();
		sceneSetLayout = S3DDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_COMPUTE_BIT)
			.addBinding(1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_COMPUTE_BIT)
			.addBinding(2, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_COMPUTE_BIT)
			.addBinding(3, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_COMPUTE_BIT)
			.addBinding(4, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_COMPUTE_BIT)
			.build();
		skeletonDescriptorSetLayout = S3DDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT)
			.build(); 
		terrainDescriptorSetLayout = S3DDescriptorSetLayout::Builder(engineDevice)
			.addBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT)
			.addBinding(1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT)
			.addBinding(2, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT)
			.addBinding(3, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT)
			.build();

		glm::vec2 resolution = { engineWindow.getWindowWidth(), engineWindow.getWindowHeight() };

		std::vector<LevelRenderInstance*> levelRenderInstances{};
		std::vector<CameraRenderInstance*> renderTargetInstances{};
		LevelRenderInstance* mainLevelRender = new LevelRenderInstance(engineDevice, resourceSystem, globalSetLayout->getDescriptorSetLayout(), sceneSetLayout, 
			skeletonDescriptorSetLayout, terrainDescriptorSetLayout, ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		levelRenderInstances.push_back(mainLevelRender);

		DeferredRenderSystem::RendererFeatures rendererFeatures{};
		rendererFeatures.enableSSAO = ProjectSystem::getEngineSettings().postfx.enableSSAO;
		rendererFeatures.enableEarlyZ = ProjectSystem::getEngineSettings().renderer.earlyZMode != ESET::EarlyDepthMode::Off;
		rendererFeatures.enableTranslucency = true;
		rendererFeatures.enableMSAA = ProjectSystem::getGraphicsSettings().renderer.msaa;
		rendererFeatures.msaaSamples = (VkSampleCountFlagBits)ProjectSystem::getGraphicsSettings().renderer.msaaSamples;
		rendererFeatures.enableStencilBuffer = false;
		rendererFeatures.enableNormalDecals = ProjectSystem::getEngineSettings().renderer.enableDeferredDecals;
		rendererFeatures.enableDeferredLighting = ProjectSystem::getEngineSettings().renderer.rendererMode == ESET::RendererMode::FullyDeferredLighting;

		CameraRenderInstance::RendererFeatures rtiRendererFeatures{};
		rtiRendererFeatures.deferredRendererFeatures = rendererFeatures;
		rtiRendererFeatures.enableMotionBlur = ProjectSystem::getEngineSettings().postfx.enableMotionBlur;
		rtiRendererFeatures.enablePostFX = true;
		rtiRendererFeatures.enableDecals = ProjectSystem::getEngineSettings().renderer.enableDeferredDecals;

		CameraRenderInstance* mainCameraRenderer = new CameraRenderInstance(engineDevice, resourceSystem, rtiRendererFeatures, resolution, globalSetLayout, sceneSetLayout->getDescriptorSetLayout(), skeletonDescriptorSetLayout->getDescriptorSetLayout(), terrainDescriptorSetLayout->getDescriptorSetLayout(), ProjectSystem::getGraphicsSettings().renderer.framesInFlight);
		renderTargetInstances.push_back(mainCameraRenderer);
	
		mainLevelRender->resizePlanarReflections(resolution, mainCameraRenderer->getGBufferImages().depthSceneInfo->getDescriptor());

		sPtr<HUDRenderSystem> hudRenderSystem = make_sPtr<HUDRenderSystem>(engineDevice, engineWindow, resourceSystem, mainCameraRenderer->getFinalImage(), resolution);

		sPtr<PhysicsSystem> physicsSystem = make_sPtr<PhysicsSystem>(resourceSystem, static_cast<float>(ProjectSystem::getEngineSettings().physics.updateRate), ProjectSystem::getEngineSettings().physics.useQuickStep);
		LevelSimulationState::setData(physicsSystem, hudRenderSystem, resourceSystem);

		
		struct EditorRendererStuff {
			GridSystem gridSystem;
			EditorBillboardRenderer editorBillboardRenderer;
			ObjectPickingSystem pickingSystem;
			HighlightRenderer highlightRenderer;
			ArrowRenderer arrowRenderer;
			PhysicsActorPreviewRenderer physicsActorDisplayRenderer;
			TerrainEditSystem terrainEditSystem;
			Editor* editor;
			glm::vec2 mousePos;
			glm::vec3 mousePrev;
			float dt;
			bool mouseDownLeft;
			bool mouseDownRight;
		};
		EditorRendererStuff editorRenderers{
			{ engineDevice, mainCameraRenderer->getRenderer()->getTransparencyRenderPass()->getRenderPass(), globalSetLayout->getDescriptorSetLayout() },
			{ engineDevice, resourceSystem, mainCameraRenderer->getRenderer()->getDeferredRenderPass()->getRenderPass(), mainCameraRenderer->getRenderer()->getTransparencyRenderPass()->getRenderPass(), globalSetLayout->getDescriptorSetLayout() },
			{ engineDevice, globalSetLayout->getDescriptorSetLayout(), skeletonDescriptorSetLayout->getDescriptorSetLayout(), terrainDescriptorSetLayout->getDescriptorSetLayout(), resourceSystem, resolution },
			{ engineDevice, mainCameraRenderer->getRenderer()->getTransparencyRenderPass()->getRenderPass(), globalSetLayout->getDescriptorSetLayout(), resourceSystem->getDescriptor() },
			{ engineDevice, mainCameraRenderer->getRenderer()->getTransparencyRenderPass()->getRenderPass(), globalSetLayout->getDescriptorSetLayout() },
			{ engineDevice, mainCameraRenderer->getRenderer()->getTransparencyRenderPass()->getRenderPass(), globalSetLayout->getDescriptorSetLayout() },
			{ engineDevice, mainCameraRenderer->getRenderer()->getDeferredRenderPass()->getRenderPass(), resourceSystem, globalSetLayout->getDescriptorSetLayout(), terrainDescriptorSetLayout }
		};
		Editor* editor = new Editor(engineDevice, engineWindow, resourceSystem, &editorRenderers.terrainEditSystem, globalSetLayout, sceneSetLayout, skeletonDescriptorSetLayout, terrainDescriptorSetLayout, engineSwapChain->getRenderPass(), context);
		editor->setRenderTargetData(mainCameraRenderer);
		editor->mainCameraTelem = mainCameraRenderer->getTelemetry();
		editor->mainLevelRenderInstance = mainLevelRender;
		editorRenderers.editor = editor;

		mainCameraRenderer->addArbitraryRenderCall([&](FrameInfo& frameInfo, void* userData) {
			reinterpret_cast<EditorRendererStuff*>(userData)->pickingSystem.beginPickingPass(frameInfo);
			reinterpret_cast<EditorRendererStuff*>(userData)->pickingSystem.renderObjects(frameInfo, reinterpret_cast<EditorRendererStuff*>(userData)->terrainEditSystem.editingTerrainActor.get());
			reinterpret_cast<EditorRendererStuff*>(userData)->pickingSystem.endPickingPass(frameInfo);
			reinterpret_cast<EditorRendererStuff*>(userData)->pickingSystem.pickObjects(frameInfo, reinterpret_cast<EditorRendererStuff*>(userData)->mousePos);
		}, & editorRenderers);


		mainCameraRenderer->addGBufferRenderCall([&](FrameInfo& frameInfo, void* userData) {
			if (GraphicsSettings2::editorPreview.ONLY_GAME == false && GraphicsSettings2::editorPreview.V_EDITOR_BILLBOARDS == true) 
				reinterpret_cast<EditorRendererStuff*>(userData)->editorBillboardRenderer.renderBillboards(frameInfo); // editor billboards	
		}, & editorRenderers);

		mainCameraRenderer->addArbitraryRenderCall([&](FrameInfo& frameInfo, void* userData) {
			static bool painting = false;
			if (reinterpret_cast<EditorRendererStuff*>(userData)->mouseDownLeft && reinterpret_cast<EditorRendererStuff*>(userData)->editor->isViewportHovered && reinterpret_cast<EditorRendererStuff*>(userData)->editor->enabledTerrainPainting) {
				for (auto actor_ : frameInfo.level->registry.view<Components::TerrainComponent>()) {
					Actor actor = Actor(actor_, level.get());
					auto& trc = actor.getComponent<Components::TerrainComponent>();
					painting = true;
					reinterpret_cast<EditorRendererStuff*>(userData)->terrainEditSystem.paintTerrain(
						frameInfo,
						reinterpret_cast<EditorRendererStuff*>(userData)->pickingSystem.getWorldPosition(),
						glm::vec3(reinterpret_cast<EditorRendererStuff*>(userData)->pickingSystem.getWorldPosition()) - reinterpret_cast<EditorRendererStuff*>(userData)->mousePrev,
						reinterpret_cast<EditorRendererStuff*>(userData)->dt,
						reinterpret_cast<EditorRendererStuff*>(userData)->editor->terrainBrush
					);
				}
			} else if (painting == true) {
				for (auto actor_ : frameInfo.level->registry.view<Components::TerrainComponent>()) {
					Actor actor = Actor(actor_, level.get());
					reinterpret_cast<EditorRendererStuff*>(userData)->terrainEditSystem.updateTerrainData(actor);
				}
				painting = false;
			}
			reinterpret_cast<EditorRendererStuff*>(userData)->mousePrev = reinterpret_cast<EditorRendererStuff*>(userData)->pickingSystem.getWorldPosition();
			
		}, & editorRenderers);

		mainCameraRenderer->addGBufferRenderCall([&](FrameInfo& frameInfo, void* userData) {
			for (auto actor_ : frameInfo.level->registry.view<Components::TerrainComponent>()) {
				Actor actor = Actor(actor_, level.get());
				reinterpret_cast<EditorRendererStuff*>(userData)->terrainEditSystem.render(frameInfo,
					actor,
					reinterpret_cast<EditorRendererStuff*>(userData)->editor->terrainView,
					reinterpret_cast<EditorRendererStuff*>(userData)->pickingSystem.getWorldPosition(),
					reinterpret_cast<EditorRendererStuff*>(userData)->editor->terrainBrush);
			}
		}, & editorRenderers);



		mainCameraRenderer->addForwardRenderCall([&](FrameInfo& frameInfo, void* userData) {
			if (GraphicsSettings2::editorPreview.ONLY_GAME == false) {
				if (GraphicsSettings2::editorPreview.V_GRID == true)
					reinterpret_cast<EditorRendererStuff*>(userData)->gridSystem.render(frameInfo);
				if (GraphicsSettings2::editorPreview.V_EDITOR_BILLBOARDS == true) {
					reinterpret_cast<EditorRendererStuff*>(userData)->editorBillboardRenderer.renderBoxVolumes(frameInfo);
					std::vector<ArrowRenderInfo> arrows{};
					for (auto& a : frameInfo.level->registry.view<Components::PhysicsConstraintComponent, Components::TransformComponent>()) {
						Actor actor = { a, frameInfo.level.get() };
						auto& cnstr = actor.getComponent<Components::PhysicsConstraintComponent>();
						auto& trans = actor.getTransform();

						ArrowRenderInfo info{};
						info.color = { 1.0, 0.3f, 1.0, 0.8f };
						info.weight = 2.f;
						info.transform = {
							trans.getRotationMatrix()
						};
						info.transform[3] = trans.transformMatrix[3];
						info.transform[0] *= 0.1f;
						info.transform[1] *= 0.1f;
						info.transform[2] *= 0.1f;
						arrows.push_back(info);

						if (cnstr.constraintType == PhysicsConstraintType::DualHinge) {
							Components::TransformComponent tf{};
							tf.setRotation(cnstr.perpendicularAxisOffsetRotation);

							info.transform = tf.calculateMat4() * glm::mat4(trans.getRotationMatrix());
							info.transform[3] = trans.transformMatrix[3];

							info.color = { 1.0, 1.f, 0.3, 0.8f };
							
							info.transform[0] *= 0.1f;
							info.transform[1] *= 0.1f;
							info.transform[2] *= 0.1f;
							arrows.push_back(info);
						}

					}
					for (auto& a : frameInfo.level->registry.view<Components::IndependentSuspensionComponent, Components::TransformComponent>()) {
						Actor actor = { a, frameInfo.level.get() };
						auto& cnstr = actor.getComponent<Components::IndependentSuspensionComponent>();
						auto& trans = actor.getTransform();

						ArrowRenderInfo info{};
						info.color = { 1.0, 0.3f, 1.0, 0.8f };
						info.weight = 2.f;
						info.transform = {
							trans.getRotationMatrix()
						};
						if (cnstr.suspensionActor) {
							glm::vec3 anchor;
							dJointGetPistonAnchor((dJointID)cnstr.suspensionActor, (dVector3&)anchor);
							trans.setTranslation({ anchor.x, anchor.z, anchor.y });
						}
						info.transform[3] = trans.transformMatrix[3];
						info.transform[0] *= 0.1f;
						info.transform[1] *= 0.1f;
						info.transform[2] *= 0.1f;
						arrows.push_back(info);

						if (cnstr.hasAxle) {
							Components::TransformComponent tf{};
							tf.setRotation(cnstr.perpendicularAxisOffsetRotation);

							info.transform = tf.calculateMat4() * glm::mat4(trans.getRotationMatrix());
							info.transform[3] = trans.transformMatrix[3];

							info.color = { 1.0, 1.f, 0.3, 0.8f };

							info.transform[0] *= 0.1f;
							info.transform[1] *= 0.1f;
							info.transform[2] *= 0.1f;
							arrows.push_back(info);

							if (cnstr.axleBodyActor) {
								info.color = { 1.0, 1.f, 1.0, 1.0f };
								info.weight = 10.0f;
								info.transform[3] = glm::vec4(*(glm::vec3*)dBodyGetPosition((dBodyID)cnstr.axleBodyActor), 1.0f);
								arrows.push_back(info);
							}
						}

					}
					reinterpret_cast<EditorRendererStuff*>(userData)->arrowRenderer.render(frameInfo, arrows);
				}
				if (GraphicsSettings2::editorPreview.V_CULLVOLUMES == true)
					reinterpret_cast<EditorRendererStuff*>(userData)->editorBillboardRenderer.visualiseMeshVolumes(frameInfo); // visualise culling volumes
				if (GraphicsSettings2::editorPreview.V_PHYSICS == true)
					reinterpret_cast<EditorRendererStuff*>(userData)->physicsActorDisplayRenderer.renderWireframe(frameInfo);


				Actor hlActor = { (entt::entity)reinterpret_cast<EditorRendererStuff*>(userData)->pickingSystem.getActiveObject(), frameInfo.level.get() };
				if (!hlActor.isInvalid()) {
					std::vector<Actor> actors{ hlActor };
					if (hlActor.hasComponent<Components::PrefabComponent>()) {
						VectorUtils::merge(actors, frameInfo.level->getAllChildrenBelongingToActor(hlActor));
					}
					reinterpret_cast<EditorRendererStuff*>(userData)->highlightRenderer.renderHighlight(frameInfo, actors); // editor billboards	
				}
			}
		}, &editorRenderers);
		
		ImGuiInitializer::setViewportImage(editor->viewportImage, mainCameraRenderer->getFinalImage());
		auto editor_cameraActor = level->getActorFromUUID(0, 0);
		bool reverseDepth = ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit;

		std::vector<uPtr<S3DCommandBuffer>> commandBuffers{};
		commandBuffers.resize(engineSwapChain->getImageCount());
		for (auto& cb : commandBuffers) {
			cb = make_uPtr<S3DCommandBuffer>(engineDevice);
		}

		uPtr<TelemetryCPUMetric> levelTickMetric = make_uPtr<TelemetryCPUMetric>(renderTargetInstances[0]->getTelemetry(), "Level", "Tick");
		uPtr<TelemetryCPUMetric> levelRenderMetricCPU = make_uPtr<TelemetryCPUMetric>(renderTargetInstances[0]->getTelemetry(), "Level", "NonCameraRender CPU");
		uPtr<TelemetryGPUMetric> levelRenderMetricGPU = make_uPtr<TelemetryGPUMetric>(renderTargetInstances[0]->getTelemetry(), "Level", "NonCameraRender GPU");
		uPtr<TelemetryCPUMetric> hudUpdateMetric = make_uPtr<TelemetryCPUMetric>(renderTargetInstances[0]->getTelemetry(), "HUD", "Update");
		uPtr<TelemetryGPUMetric> hudRenderMetric = make_uPtr<TelemetryGPUMetric>(renderTargetInstances[0]->getTelemetry(), "HUD", "Render GPU");
		renderTargetInstances[0]->getTelemetry()->registerTelemetryMetric(levelTickMetric.get());
		renderTargetInstances[0]->getTelemetry()->registerTelemetryMetric(levelRenderMetricCPU.get());
		renderTargetInstances[0]->getTelemetry()->registerTelemetryMetric(levelRenderMetricGPU.get());
		renderTargetInstances[0]->getTelemetry()->registerTelemetryMetric(hudUpdateMetric.get());
		renderTargetInstances[0]->getTelemetry()->registerTelemetryMetric(hudRenderMetric.get());

		loadStaticObjects();

		uint32_t swapChainImageIndex;
		uint32_t frameIndex = 0;
		//GHUD::DrawList* hudDraw = hudRenderSystem->getDrawList();
		auto oldTime = std::chrono::high_resolution_clock::now();
beginWhileLoop:
		while (!engineWindow.shouldClose()) {
			auto newTime = std::chrono::high_resolution_clock::now();
			float frameTime = std::chrono::duration<float, std::chrono::seconds::period>(newTime - oldTime).count();
			oldTime = newTime;
			frameTime = std::min(frameTime, ProjectSystem::getEngineSettings().cpu.maximumAllowedFrameTimeMilliseconds / 1000.f);
			glfwPollEvents();
			double x, y;
			glfwGetCursorPos(engineWindow.getGLFWwindow(), &x, &y);

			editorRenderers.mousePos = {x,y};
			editorRenderers.mouseDownLeft = glfwGetMouseButton(engineWindow.getGLFWwindow(), GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS;
			editorRenderers.mouseDownRight = glfwGetMouseButton(engineWindow.getGLFWwindow(), GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS;
			editorRenderers.dt = frameTime;
			hudRenderSystem->setCursorPosition(
				{ GraphicsSettings2::getRuntimeInfo().localScreen.x, GraphicsSettings2::getRuntimeInfo().localScreen.y },
				{ GraphicsSettings2::getRuntimeInfo().localScreen.z, GraphicsSettings2::getRuntimeInfo().localScreen.w },
				editorRenderers.mousePos
			);

			auto possessedCameraActor = level->getPossessedCameraActor();
			auto& possessedCamera = level->getPossessedCamera();

			level->runGarbageCollector();

			hudUpdateMetric->record();
			hudRenderSystem->newFrame();
			for (auto& layer : hudRenderSystem->getAllLayers()) {
				if (layer == "_editorreserved") continue;
				hudRenderSystem->renderHUDLayer(hudRenderSystem->getDrawList(), layer);
			}
			hudUpdateMetric->end();

			levelTickMetric->record();
			LevelSimulationState::eventTick(frameTime);
			if (level->simulationState == PlayState::Playing) {
				level = ScriptEngine::getContext();
			}
			level->rebuildTransforms();
			for (auto obj : level->registry.view<Components::TransformComponent>()) {
				ECS::Actor actor = { obj, level.get() };
				auto& tc = actor.getTransform();
				mainLevelRender->getRenderObjects().renderList->updateActorTransform(actor, tc.transformMatrix, tc.normalMatrix);
			}
			levelTickMetric->end();
			possessedCameraActor.getComponent<Components::CameraComponent>().ar = GraphicsSettings2::getRuntimeInfo().aspectRatio;

			while (GlobalFrameState::lockFrame) { glfwPollEvents(); }

			if (VkResult result = engineSwapChain->acquireNextImage(&swapChainImageIndex); result == VK_SUCCESS) {
				SHARD3D_ASSERT(!GlobalFrameState::lockFrame && "Frame must be unlocked before can start!");
				GlobalFrameState::isFrameInProgress = true;

				commandBuffers[frameIndex]->begin();
				VkCommandBuffer commandBuffer = commandBuffers[frameIndex]->getCommandBuffer();
				VkFence fence = engineSwapChain->getFence(frameIndex);

				FrameInfo2 frameInfo{
					frameIndex,
					frameTime,
					level->getPossessedCamera(),
					level,
					hudRenderSystem.get(),
					resourceSystem
				};
				ImGui_ImplVulkan_NewFrame();
				ImGui_ImplGlfw_NewFrame();
				ImGui::NewFrame();

				editor->render(frameInfo, levelRenderInstances[0]);
				//ImGui::Begin("Suspension Debug");
				//static float suspensionFrequency = 1.0f;
				//static float suspensionDamping = 1.0f;
				//ImGui::DragFloat("Frequency", &suspensionFrequency, 0.05f, 0.0f, 5.0f);
				//ImGui::DragFloat("Damping", &suspensionDamping, 0.05f, 0.0f, 100.0f);
				//ImGui::End();
				//
				//for (auto& a : level->registry.view<Components::IndependentSuspensionComponent>()) {
				//	Actor sus = { a, level.get() };
				//	sus.getComponent<Components::IndependentSuspensionComponent>().frequency = suspensionFrequency;
				//	sus.getComponent<Components::IndependentSuspensionComponent>().damping = suspensionDamping;
				//}

				ImGui::Render();

				resourceSystem->runGarbageCollector(frameIndex, fence);
				resourceSystem->getVertexAllocator()->runGarbageCollector(frameIndex, fence);
				for (LevelRenderInstance* lri : levelRenderInstances) {
					lri->runGarbageCollector(frameIndex, fence, level);
				}
				resourceSystem->getVertexAllocator()->bind(commandBuffer);
				resourceSystem->updateSurfaceMaterials(commandBuffer);

				for (CameraRenderInstance* rti : renderTargetInstances) {
					rti->updateCamera(level, level->getPossessedCameraActor(), GraphicsSettings2::getRuntimeInfo().aspectRatio);
					rti->renderEarlyZ(commandBuffer, mainLevelRender->getDescriptorSet(frameIndex), fence, frameIndex,
						frameTime, level->getPossessedCameraActor(), level, mainLevelRender->getRenderObjects());
				}
				levelRenderMetricCPU->record();
				levelRenderMetricGPU->record(commandBuffer);
				for (LevelRenderInstance* lri : levelRenderInstances) {
					lri->render(commandBuffer, frameIndex, level);
				}
				levelRenderMetricCPU->end();
				levelRenderMetricGPU->end(commandBuffer);

				for (CameraRenderInstance* rti : renderTargetInstances) {
					if (editorRenderers.terrainEditSystem.editingTerrainActor) {
						editorRenderers.terrainEditSystem.editingTerrainActor->updateVisibilityCalcs(commandBuffer, 
							level->getPossessedCameraActor().getComponent<Components::CameraComponent>().camera, 32.0f);
					}
					rti->render(commandBuffer, mainLevelRender->getDescriptorSet(frameIndex), fence, frameIndex, frameTime,
						level->getPossessedCameraActor(), level, mainLevelRender->getRenderObjects(), 
						*mainLevelRender->getBuffer(frameInfo.frameIndex));
				}
				possessedCameraActor.getComponent<Components::CameraComponent>().setProjection(ProjectSystem::getEngineSettings().renderer.depthBufferFormat == ESET::DepthBufferFormat::ReverseF32Bit, ProjectSystem::getEngineSettings().renderer.infiniteFarZ);
				possessedCameraActor.getComponent<Components::CameraComponent>().camera.setViewYXZ(possessedCameraActor.getTransform().transformMatrix);
				editor->renderCameras(commandBuffer, frameIndex, fence, frameInfo);

				editor->setHoveredObject(editorRenderers.pickingSystem.getActiveObject(), level.get());
				editor->setMouseHoveringCoord(editorRenderers.pickingSystem.getWorldPosition());

				hudRenderMetric->record(commandBuffer);
				hudRenderSystem->render(commandBuffer, frameIndex);
				hudRenderMetric->end(commandBuffer);

				editor->drawHUDPreview(commandBuffer, frameIndex);
				
				engineSwapChain->beginRenderPass(commandBuffer);

				MACRO_CHECKPOINT_NVIDIA(engineDevice, S3DCheckpointType::DRAW, "SWAP_CHAIN_RENDER", commandBuffer);
				editor->draw(commandBuffer);

				engineSwapChain->endRenderPass(commandBuffer);

				commandBuffers[frameIndex]->end();
				//SHARD3D_INFO("submit cb {0} with fence {1}", (void*)commandBuffers[frameIndex]->getCommandBuffer(), (void*)fence);

				mainLevelRender->getRenderObjects().renderList->updateDynamicTransformBufferData(frameIndex);
				mainLevelRender->getRenderObjects().renderList->updateDynamicRiggingBufferData(level, frameIndex);
				engineRenderer.submitCommandBuffer(commandBuffers[frameIndex].get());
				engineRenderer.submitQueue(engineSwapChain->getSubmitInfo(&swapChainImageIndex), fence);

				engineSwapChain->present(engineRenderer.getQueue(), &swapChainImageIndex);

				const float frameTime = std::chrono::duration<double, std::chrono::seconds::period>(std::chrono::high_resolution_clock::now() - newTime).count();
				int fpsLimit = engineWindow.isWindowFocussed() ? ProjectSystem::getGraphicsSettings().renderer.frameRateLimit : 30;
				const float waitTime = std::max(1.f / fpsLimit - frameTime, 0.f);

				if (waitTime > 0.f) Shard3D::WAITUtils::preciseStandby(waitTime);

				frameIndex = (frameIndex + 1) % ProjectSystem::getGraphicsSettings().renderer.framesInFlight;

				GlobalFrameState::isFrameInProgress = false;
			}
			else if (result == VK_SUBOPTIMAL_KHR || result == VK_ERROR_OUT_OF_DATE_KHR) {
				VkExtent2D extent = engineWindow.getExtent();

				while (extent.width == 0 || extent.height == 0) {
					extent = engineWindow.getExtent();
					glfwWaitEvents();
				}
				vkDeviceWaitIdle(engineDevice.device()); // wait for all in flight frames to finish
				glm::vec2 newsize = { extent.width, extent.height };
				S3DSwapChain* oldSwapChain = engineSwapChain;
				engineSwapChain = new S3DSwapChain(engineDevice, engineWindow, extent, oldSwapChain);
				delete oldSwapChain;
				for (CameraRenderInstance* rti : renderTargetInstances) {
					rti->resize(newsize);
				}
				mainLevelRender->resizePlanarReflections(newsize, mainCameraRenderer->getGBufferImages().depthSceneInfo->getDescriptor());
				hudRenderSystem->resize(mainCameraRenderer->getFinalImage(), newsize);
				editorRenderers.pickingSystem.resize(glm::ivec3(newsize, 1));
				ImGuiInitializer::setViewportImage(editor->viewportImage, mainCameraRenderer->getFinalImage());
				editor->setRenderTargetData(mainCameraRenderer);
				engineWindow.resetWindowResizedFlag();
				frameIndex = 0; // reset frame index to match the swap chain index
			} else {		
				SHARD3D_FATAL(fmt::format("failed to acquire swap chain image! {0} ({1})", string_VkResult(result), (int)result));
			}
			level->cleanup();
		}

		if (MessageDialogs::show("Any unsaved changes will be lost! Are you sure you want to exit?",
			"Shard3D Torque", MessageDialogs::OPTYESNO | MessageDialogs::OPTDEFBUTTON2 | MessageDialogs::OPTICONEXCLAMATION) == MessageDialogs::RESNO) {
			glfwSetWindowShouldClose(engineWindow.getGLFWwindow(), GLFW_FALSE);
			goto beginWhileLoop;
		}

		if (level->simulationState != PlayState::Stopped)
			LevelSimulationState::eventEnd();
		vkDeviceWaitIdle(engineDevice.device());
		LevelSimulationState::release();
		LevelSimulationState::removeData();

		editor->detach();
		delete editor;
		vkDestroyDescriptorPool(engineDevice.device(), ImGuiInitializer::imGuiDescriptorPool, nullptr);
		ShaderSystem::destroy();
		ScriptEngine::destroy();
		_special_assets::_editor_icons_destroy();
		
		for (LevelRenderInstance* lri : levelRenderInstances) {
			delete lri;
		}
		for (CameraRenderInstance* rti : renderTargetInstances) {
			delete rti;
		}
		level = nullptr;
		delete engineSwapChain;
		delete resourceSystem;
	}

	void Application::loadStaticObjects() {
		resourceSystem->loadMesh(AssetID("engine/meshes/sphere.obj" ENGINE_ASSET_SUFFIX));

		//for (int x = 0; x < 300; x++) {
		//	for (int z = 0; z < 30; z++) {
		//		Actor actor = level->createActor("Decal Actor");
		//		actor.makeDynamic();
		//		actor.addComponent<Components::DeferredDecalComponent>().material = AssetID("engine/materials/decals/bullet.s3dasset");
		//		actor.addComponent<Components::BoxVolumeComponent>().bounds = { 0.05, 0.05, 0.1 };
		//		actor.getComponent<Components::BoxVolumeComponent>().transitionDistance = 0.0f;
		//		actor.getTransform().setTranslation({x * 0.1f, 0.0f, z * 0.1f});
		//		actor.getTransform().setRotation({ glm::half_pi<float>(), 0.0f, 0.0f });
		//	}
		//}
	}
}