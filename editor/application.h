#pragma once
#define SHARD3D_EDITOR

#include <Shard3D/core.h>
#include <Shard3D/s3dpch.h>
#include <Shard3D/ecs.h>
#include <Shard3D/vulkan_abstr.h>

#include <Shard3D/events/event.h>
#include <Shard3D/events/wnd_event.h>
#include <Shard3D/core/rendering/window.h>
#include <Shard3D/core/rendering/swap_chain.h>
#include <Shard3D/core/rendering/render_pass.h>
#include <Shard3D/core/rendering/renderer.h>

namespace Shard3D {
	class Application {
	public:
		Application(AssetID project_);
		~Application();

		Application(const Application&) = delete;
		Application& operator=(const Application&) = delete;

		void run();
	private:
		// Functions
		void setupEngineFeatures();
		void setWindowCallbacks();
		void createRenderPasses();
		void destroyRenderPasses();
		void loadStaticObjects();

		void windowResizeEvent(Events::WindowResizeEvent& e);
		void eventEvent(Events::Event& e);

		// Engine components

		S3DDevice engineDevice{};

		S3DWindow engineWindow{ engineDevice, 
			ProjectSystem::getEngineSettings().window.defaultWidth,
			ProjectSystem::getEngineSettings().window.defaultHeight, "Shard3D Engine" };
		S3DSwapChain* engineSwapChain = new S3DSwapChain(engineDevice, engineWindow, engineWindow.getExtent(), nullptr);
		S3DRenderer engineRenderer{ engineDevice };
		ResourceSystem* resourceSystem;

		uPtr<S3DDescriptorSetLayout> globalSetLayout{};
		uPtr<S3DDescriptorSetLayout> sceneSetLayout{};
		uPtr<S3DDescriptorSetLayout> skeletonDescriptorSetLayout;
		uPtr<S3DDescriptorSetLayout> terrainDescriptorSetLayout; 
		// ECS
		sPtr<ECS::Level> level{};
		AssetID project;
	};
}