#pragma once
#include <Shard3D/core/asset/material.h>
#include <Shard3D/core/asset/physics_asset.h>

namespace Shard3D {
	class ResourceSystem;
	class PhysicsBuilderPanel {
	public:
		PhysicsBuilderPanel() = default;
		PhysicsBuilderPanel(sPtr<Level>& level, AssetID physicsAsset, AssetID physicsMaterial);
		~PhysicsBuilderPanel();

		void render(FrameInfo2& frameInfo);
		void rebuild();
		void destroy();
		void setPhysicsAsset(const AssetID& asset);

		const PhysicsAsset& getCurrentPhysicsObject() {
			return currentPhysicsItem;
		}
		AssetID getCurrentPhysicsAsset() {
			return currentPhysicsAsset;
		}
		const PhysicsMaterial& getCurrentPhysicsMaterial() {
			return currentMaterialItem;
		}
		AssetID getCurrentMaterialAsset() {
			return currentMaterialAsset;
		}
		bool isEditingConvexShape() {
			return modifyingPointIndex < currentPhysicsItem.convexPoints.size();
		}
		glm::vec3& getModifyingConvexPoint() {
			return currentPhysicsItem.convexPoints[modifyingPointIndex];
		}
		void setModifyingConvexPoint(int index) {
			modifyingPointIndex = index;
		}
	private:
		void mirrorConvexShapeOnDirection(glm::vec3 mirrorTo);

		void beginSimulation();
		void stopSimulation();
		void pauseSimulation();
		void resumeSimulation();

		int convexStat_numConcavePoints = 0;

		uint32_t modifyingPointIndex = -1;
		sPtr<Level> preview_builder;
		PhysicsAsset currentPhysicsItem{};
		AssetID currentPhysicsAsset{AssetID::null()};
		PhysicsMaterial currentMaterialItem{};
		AssetID currentMaterialAsset{ AssetID::null() };
	};
}