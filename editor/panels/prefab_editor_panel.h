#pragma once
#include <Shard3D/ecs.h>
namespace Shard3D {
	class PrefabEditorPanel {
	public:
		PrefabEditorPanel() = default;
		PrefabEditorPanel(Prefab* prefab);
		~PrefabEditorPanel();

		void render(FrameInfo2& frameInfo);
		void destroy();
		Prefab* getCurrentPrefab() {
			return currentItem;
		}
	private:
		sPtr<Level> preview_builder;
		Prefab* currentItem{};
	};
}