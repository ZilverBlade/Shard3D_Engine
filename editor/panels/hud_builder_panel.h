#pragma once
#include <Shard3D/systems/rendering/hud_system.h>
#include <unordered_set>
class TextEditor;
namespace Shard3D {
	class ResourceSystem;
	class HUDBuilderPanel {
	public:
		HUDBuilderPanel() = default;
		HUDBuilderPanel(ResourceSystem* resourceSystem, AssetID hudAsset);
		~HUDBuilderPanel();
		void drawElementEntry(const std::string& elementName, HUDElementData* element, std::unordered_map<std::string, HUDElementData*>& list);

		void render(FrameInfo2& frameInfo, std::unordered_map<std::string, HUDElementData*>& list);
		void renderElementPanel(FrameInfo2& frameInfo, std::unordered_map<std::string, HUDElementData*>& list, ResourceSystem* resourceSystem);
	private:
		void load(AssetID asset, std::unordered_map<std::string, HUDElementData*>& list);
		std::string allocateNewElement(const std::string& baseName, SHUD::Element::Base* genericBase, std::unordered_map<std::string, HUDElementData*>& list);
		std::string copyNewElement(const std::string& baseName, HUDElementData* element, std::unordered_map<std::string, HUDElementData*>& list);
		void eraseElement(const std::string& name);
		bool tryRenameElement(const std::string& oldElementName, const std::string& newElementName, std::unordered_map<std::string, HUDElementData*>& list);

		HUDElementData* findElement(const std::string& element, std::unordered_map<std::string, HUDElementData*>& list);

		std::string selectedElement;
		std::string hoveredElement;
		std::string draggingElement;
		char editingElementNameBuff[256];
		std::unordered_set<std::string> existingElementNames;


		ResourceSystem* resourceSystem;

		std::vector<AssetID> currentItem_oldtextures;
		AssetID currentAsset{AssetID::null()};
	};
}