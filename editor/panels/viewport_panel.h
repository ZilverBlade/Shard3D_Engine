#pragma once
#include <Shard3D/core/misc/frame_info.h>
namespace Shard3D {
	class ViewportPanel {
	public:
		void render(FrameInfo& frameInfo);
	};
}