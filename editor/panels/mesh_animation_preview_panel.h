#pragma once
#include <Shard3D/core/asset/animation.h>
#include <Shard3D/core/asset/importer_model.h>
#include <Shard3D/core/asset/skeleton.h>
namespace Shard3D {
	class ResourceSystem;
	class MeshAnimationPreviewPanel {
	public:
		MeshAnimationPreviewPanel() = default;
		MeshAnimationPreviewPanel(ResourceSystem* resourceSystem, sPtr<Level>& level, AssetID meshAsset, AssetID skeletonAsset, AssetID animationAsset);
		~MeshAnimationPreviewPanel();

		void render(FrameInfo2& frameInfo);
		void destroy();
		void setMeshAsset(ResourceSystem* resourceSystem, const AssetID& meshAsset, const AssetID& skeletonAsset);
		void setAnimationAsset(ResourceSystem* resourceSystem, const AssetID& asset);

		AssetID getCurrentMeshAsset() {
			return currentMeshAsset;
		}	
		AssetID getCurrentSkeletonAsset() {
			return currentSkeletonAsset;
		}	
		AssetID getCurrentAnimationAsset() {
			return currentAnimationAsset;
		}

		float getElapsed() {
			return elapsed;
		}
		void startAnimation(float startPoint) {
			if (!currentAnimation) return;
			if (startPoint > currentAnimation->getMaxDuration()) return;
			elapsed = startPoint;
			playing = true;
		}
		void pauseAnimation() {
			playing = false;
		}
		void resetAnimation() {
			playing = false;
			elapsed = 0.f;
			playing = true;
		}
		bool loops = true;
	private:
		sPtr<Level> preview_level;
		sPtr<SkeletalAnimation> currentAnimation{};
		AssetID currentMeshAsset{ AssetID::null() };
		AssetID currentSkeletonAsset{ AssetID::null() };
		AssetID currentAnimationAsset{ AssetID::null() };

		bool playing = false;
		float elapsed = 0.0f;
	};
}