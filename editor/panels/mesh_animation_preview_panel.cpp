#pragma once
#include <Shard3D/core/ecs/actor.h>
#include <Shard3D/core/ecs/level.h>
#include <Shard3D/systems/handlers/resource_system.h>
#include <Shard3D/core/asset/assetmgr.h>
#include "mesh_animation_preview_panel.h"
#include <imgui.h>

namespace Shard3D {
	MeshAnimationPreviewPanel::MeshAnimationPreviewPanel(
		ResourceSystem* resourceSystem, sPtr<Level>& level, AssetID meshAsset, AssetID skeletonAsset, AssetID animationAsset) : preview_level(level){
		
		setMeshAsset(resourceSystem, meshAsset, skeletonAsset);
		setAnimationAsset(resourceSystem, animationAsset);
	}
	MeshAnimationPreviewPanel::~MeshAnimationPreviewPanel() {}
	void MeshAnimationPreviewPanel::render(FrameInfo2& frameInfo) {
		if (playing) {
			elapsed += frameInfo.frameTime ;
			if (loops) {
				elapsed = std::fmod(elapsed, currentAnimation->getMaxDuration());
			} else {
				elapsed = std::min(elapsed, currentAnimation->getMaxDuration());
			}

			Actor actor = preview_level->getActorFromTag("SkeletalAnimActor");
			auto& rig = actor.getComponent<Components::SkeletonRigComponent>();
			
			for (int i = 0; i < currentAnimation->boneAnimations.size(); i++) {
				rig.boneTransforms[i].setTranslation(currentAnimation->boneAnimations[i].getTranslation(elapsed));
				rig.boneTransforms[i].setRotationQuat(currentAnimation->boneAnimations[i].getQuaternion(elapsed));
				rig.boneTransforms[i].setScale(currentAnimation->boneAnimations[i].getScale(elapsed));
			}
		}

		ImGui::Begin("Skeletal Animation Panel");
		ImGui::Text("Elapsed: %.3f s / %.3f s", elapsed, currentAnimation->getMaxDuration());
		ImGui::Checkbox("Playing", &playing);
		ImGui::Checkbox("Loop", &loops);
		ImGui::End();
	}
	void MeshAnimationPreviewPanel::destroy() {
		
	}
	void MeshAnimationPreviewPanel::setMeshAsset(ResourceSystem* resourceSystem, const AssetID& meshAsset, const AssetID& skeletonAsset) {
		Actor actor = preview_level->getActorFromTag("SkeletalAnimActor");
		auto& mesh = actor.getComponent<Components::Mesh3DComponent>();
		auto& rig = actor.addComponent<Components::SkeletonRigComponent>();
		mesh = Components::Mesh3DComponent(resourceSystem, meshAsset);
		rig.asset = skeletonAsset;
		rig.enableBones = true;
		resourceSystem->loadMesh(meshAsset);
		resourceSystem->loadSkeleton(rig.asset);
		actor.getComponent<Components::SkeletonRigComponent>().boneTransforms.clear();
		Skeleton* skeleton = resourceSystem->retrieveSkeleton(rig.asset);
		actor.getComponent<Components::SkeletonRigComponent>().boneTransforms.resize(skeleton->boneNames.size());
		actor.getComponent<Components::SkeletonRigComponent>().morphTargetWeights.resize(skeleton->morphTargetNames.size());
		if (actor.hasComponent<Components::Mesh3DComponent>()) {
			actor.getComponent<Components::Mesh3DComponent>().dirty = true;
		}
		currentAnimation = nullptr;
	}
	void MeshAnimationPreviewPanel::setAnimationAsset(ResourceSystem* resourceSystem, const AssetID& asset) {
		currentAnimation = AssetManager::loadSkeletalAnimation(asset);
		resetAnimation();
	}
}