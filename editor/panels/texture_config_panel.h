#pragma once
#include <Shard3D/core/asset/texture2D.h>
#include <Shard3D/core/asset/assetid.h>
#include <Shard3D/core/misc/frame_info.h>
namespace Shard3D {
	class ResourceSystem;
	class TextureConfigPanel {
	public:
		TextureConfigPanel() = default;
		TextureConfigPanel(ResourceSystem* resourceSystem, AssetID textureAsset);
		~TextureConfigPanel();

		void render(FrameInfo2& frameInfo, bool& open);
		void refresh(ResourceSystem* resourceSystem);
		void destroy();
		bool requireMaterialRebuild = false;
		Texture2D* getCurrentMaterial() {
			return currentItem;
		}
		AssetID getCurrentMaterialAsset() {
			return currentAsset;
		}
		VkDescriptorSet textureVisualiser;
	private:
		Texture2D* currentItem{};
		AssetID currentAsset{AssetID::null()};
		TextureLoadInfo loadInfo;
	};
}