#include "physics_builder_panel.h"

#include <imgui.h>
#include <fstream>
#include <Shard3D/core/asset/assetmgr.h>
#include <glm/gtc/type_ptr.hpp>
#include <Shard3DEditorKit/kit.h>
#include <Shard3D/core/ecs/level_simulation.h>
#include <Shard3D/systems/handlers/prefab_manager.h>
#include <Shard3D/utils/dialogs.h>
#include <ReactorPhysicsEngine/colliders/convex_hull.h>
#include <Shard3DEditorKit/gui/icons.h>
#include <ReactorPhysicsEngine/convex_shape.h>

namespace Shard3D {
	PhysicsBuilderPanel::PhysicsBuilderPanel( sPtr<Level>& level, AssetID physicsAsset, AssetID physicsMaterial) : currentPhysicsAsset(physicsAsset), preview_builder(level) {
		currentPhysicsItem = AssetManager::loadPhysicsAsset(currentPhysicsAsset);
		rebuild();
	}
	PhysicsBuilderPanel::~PhysicsBuilderPanel() {}

	void PhysicsBuilderPanel::render(FrameInfo2& frameInfo) {
		ImVec2 btnSize = { 48.f, 48.f };
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4());
		ImGui::PushStyleVar(ImGuiStyleVar_FrameBorderSize, 0.f);
		ImGui::Begin("_editor_sidebar_physeditor", nullptr, ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoTitleBar);
		// begin

		ImGui::BeginDisabled(currentPhysicsItem.colliderType != PhysicsColliderType::ConvexHullAsset);
		if (ImGui::ImageButton(getImGuiIcon().edit_position, btnSize)) {
			if (preview_builder->getActorFromTag("FLAG_EDITOR.DisplayPoints").isVisible()) {
				preview_builder->getActorFromTag("FLAG_EDITOR.DisplayPoints").makeInvisible();
			} else {
				preview_builder->getActorFromTag("FLAG_EDITOR.DisplayPoints").makeVisible();
				preview_builder->getActorFromTag("FLAG_EDITOR.DisplayNodes").makeInvisible();
			}
		}
		if (ImGui::IsItemHovered())
			ImGui::SetTooltip("Modify Hull");

		if (ImGui::ImageButton(getImGuiIcon().edit_position_add, btnSize)) {
			if (modifyingPointIndex == -1) {
				currentPhysicsItem.convexPoints.push_back(currentPhysicsItem.convexPoints.back());
			} else {
				currentPhysicsItem.convexPoints.push_back(getModifyingConvexPoint());
			}
			modifyingPointIndex = currentPhysicsItem.convexPoints.size() - 1;
		}
		if (ImGui::IsItemHovered())
			ImGui::SetTooltip("Add Hull Point");


		ImGui::BeginDisabled(currentPhysicsItem.convexPoints.size() == 4 || modifyingPointIndex == -1);
		if (ImGui::ImageButton(getImGuiIcon().edit_position_rmv, btnSize)) {
			currentPhysicsItem.convexPoints.erase(currentPhysicsItem.convexPoints.begin() + modifyingPointIndex);
			modifyingPointIndex -= 1;
		}
		if (ImGui::IsItemHovered())
			ImGui::SetTooltip("Cut Hull Point");
		ImGui::EndDisabled();

		ImGui::Dummy(btnSize);

		if (ImGui::ImageButton(getImGuiIcon().mirror, btnSize)) {
			ImGui::OpenPopup("dropdownMirrorOpenPOPUPMENU");
		}
		if (ImGui::IsItemHovered())
			ImGui::SetTooltip("Mirror Points");

		if (ImGui::BeginPopup("dropdownMirrorOpenPOPUPMENU", ImGuiPopupFlags_None)) {
			if (ImGui::ImageButtonEx(3213211247784, getImGuiIcon().mirror_left, btnSize, ImVec2(0.0, 0.0), ImVec2(1.0, 1.0), ImVec4(0.0, 0.0, 0.0, 0.0), ImVec4(1.0, 0.5, 0.5, 1.0))) {
				mirrorConvexShapeOnDirection({ -1.0, 0.0, 0.0 });
				ImGui::CloseCurrentPopup();
			}
			if (ImGui::IsItemHovered())
				ImGui::SetTooltip("Mirror To X-");

			if (ImGui::ImageButtonEx(82194821048, getImGuiIcon().mirror_right, btnSize, ImVec2(0.0, 0.0), ImVec2(1.0, 1.0), ImVec4(0.0, 0.0, 0.0, 0.0), ImVec4(1.0, 0.5, 0.5, 1.0))) {
				mirrorConvexShapeOnDirection({ 1.0, 0.0, 0.0 });
				ImGui::CloseCurrentPopup();
			}
			if (ImGui::IsItemHovered())
				ImGui::SetTooltip("Mirror To X+");

			if (ImGui::ImageButtonEx(47372383217436, getImGuiIcon().mirror_left, btnSize, ImVec2(0.0, 0.0), ImVec2(1.0, 1.0), ImVec4(0.0, 0.0, 0.0, 0.0), ImVec4(0.5, 1.0, 0.5, 1.0))) {
				mirrorConvexShapeOnDirection({ 0.0, -1.0, 0.0 });
				ImGui::CloseCurrentPopup();
			}
			if (ImGui::IsItemHovered())
				ImGui::SetTooltip("Mirror To Y-");

			if (ImGui::ImageButtonEx(3285327437431812437, getImGuiIcon().mirror_right, btnSize, ImVec2(0.0, 0.0), ImVec2(1.0, 1.0), ImVec4(0.0, 0.0, 0.0, 0.0), ImVec4(0.5, 1.0, 0.5, 1.0))) {
				mirrorConvexShapeOnDirection({ 0.0, 1.0, 0.0 });
				ImGui::CloseCurrentPopup();
			}
			if (ImGui::IsItemHovered())
				ImGui::SetTooltip("Mirror To Y+");

			if (ImGui::ImageButtonEx(7432873452832438, getImGuiIcon().mirror_left, btnSize, ImVec2(0.0, 0.0), ImVec2(1.0, 1.0),  ImVec4(0.0, 0.0, 0.0, 0.0), ImVec4(0.5, 0.5, 1.0, 1.0))) {
				mirrorConvexShapeOnDirection({ 0.0, 0.0, -1.0 });
				ImGui::CloseCurrentPopup();
			}
			if (ImGui::IsItemHovered())
				ImGui::SetTooltip("Mirror To Z-");

			if (ImGui::ImageButtonEx(4623723742372374, getImGuiIcon().mirror_right, btnSize, ImVec2(0.0, 0.0), ImVec2(1.0, 1.0), ImVec4(0.0, 0.0, 0.0, 0.0), ImVec4(0.5, 0.5, 1.0, 1.0))) {
				mirrorConvexShapeOnDirection({ 0.0, 0.0, 1.0 });
				ImGui::CloseCurrentPopup();
			}
			if (ImGui::IsItemHovered())
				ImGui::SetTooltip("Mirror To Z+");

				ImGui::EndPopup();
		}

		if (ImGui::ImageButton(getImGuiIcon().gizmo_scale, btnSize)) {
				SHARD3D_NOIMPL;
		}
		if (ImGui::IsItemHovered())
			ImGui::SetTooltip("Hull From Mesh" );

		ImGui::EndDisabled();
		ImGui::End();


		ImGui::Begin("_editor_quickbar_physeditor", nullptr, ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoTitleBar);
		
		{ 

			float panelWidth = ImGui::GetContentRegionAvail().x;
			int columnCount = std::max((int)(panelWidth / (btnSize.x + 16.f))// <--- thumbnail size (96px) + padding (16px)
				, 1);
			ImGui::Columns(columnCount, 0, false);
		}
		// begin
		
		// Save/Load
		if (ImGui::ImageButton(getImGuiIcon().save, btnSize)) {
			std::string filepath = BrowserDialogs::openFile(ENGINE_SHARD3D_ASSETFILE_OPTIONS);
			if (!filepath.empty()) {


			}
		}
		ImGui::TextWrapped("Save");
		ImGui::NextColumn();
		if (ImGui::ImageButton(getImGuiIcon().load, btnSize)) {
			if (MessageDialogs::show("This will overwrite the current physics asset, and unsaved changes will be lost! Are you sure you want to continue?", "WARNING!", MessageDialogs::OPTYESNO | MessageDialogs::OPTICONEXCLAMATION | MessageDialogs::OPTDEFBUTTON2) == MessageDialogs::RESYES) {
				std::string filepath = BrowserDialogs::openFile(ENGINE_SHARD3D_ASSETFILE_OPTIONS);
				if (!filepath.empty()) {


				}
			}
		}
		ImGui::TextWrapped("Load");
		ImGui::NextColumn();
		ImGui::NextColumn();

		// PLAY MODE
		if (ImGui::ImageButton(
			(preview_builder->simulationState == PlayState::Stopped) ? getImGuiIcon().play :
			((preview_builder->simulationState == PlayState::Paused) ? getImGuiIcon().play : getImGuiIcon().pause), btnSize)) {
			if (preview_builder->simulationState == PlayState::Stopped) { // Play
				beginSimulation();
			} else if (preview_builder->simulationState == PlayState::Paused) {  // Resume
				resumeSimulation();
			} else if (preview_builder->simulationState == PlayState::Simulating) {  // Pause
				pauseSimulation();
			}
		}
		ImGui::TextWrapped((preview_builder->simulationState == PlayState::Stopped) ? "Play" :
			((preview_builder->simulationState == PlayState::Paused) ? "Resume" : "Pause"));
		ImGui::NextColumn();
		ImGui::BeginDisabled(preview_builder->simulationState == PlayState::Stopped);
		if (ImGui::ImageButton(getImGuiIcon().stop, btnSize)) {        // Stop
			stopSimulation();
		}
		ImGui::TextWrapped("Stop");
		ImGui::EndDisabled();

		ImGui::NextColumn();
		ImGui::NextColumn();

		if (ImGui::ImageButton(getImGuiIcon().settings, btnSize)) {
			std::string filepath = BrowserDialogs::openFile(ENGINE_SHARD3D_ASSETFILE_OPTIONS);
			if (!filepath.empty()) {
				AssetID asset = AssetID(std::filesystem::relative(filepath, std::filesystem::path(ProjectSystem::getAssetLocation())).string());
				if (ResourceSystem::discoverAssetType(filepath) == AssetType::Model3D) {
					frameInfo.resourceSystem->loadMesh(asset);
					preview_builder->getActorFromTag("PhysicsActor").addComponent<Components::Mesh3DComponent>(frameInfo.resourceSystem, asset);
				} else {
					MessageDialogs::show("Invalid Mesh3D Asset", "Asset Error", MessageDialogs::OPTICONERROR);
				}
			}
		}
		ImGui::TextWrapped("Load Preview Mesh");
		ImGui::NextColumn();
		if (ImGui::ImageButtonEx(3214712897489, getImGuiIcon().settings, btnSize, { 0.0, 0.0 }, { 1.0, 1.0 }, { 0.0, 0.0, 0.0, 0.0 }, {1.0, 1.0, 1.0, 1.0})) {
			std::string filepath = BrowserDialogs::openFile(ENGINE_SHARD3D_BLUEPRINT_OPTIONS);
			if (!filepath.empty()) {
				AssetID asset = AssetID(std::filesystem::relative(filepath, std::filesystem::path(ProjectSystem::getAssetLocation())).string());
				if (ResourceSystem::discoverAssetType(filepath) == AssetType::Prefab) {
					preview_builder->getPrefabManager()->cachePrefab(asset);

					Actor child = preview_builder->getPrefabManager()->instPrefab(preview_builder.get(), 0, UUID(), asset);
					preview_builder->parentActor(child, preview_builder->getActorFromTag("PhysicsActor"));
				} else {
					MessageDialogs::show("Invalid Prefab Asset", "Asset Error", MessageDialogs::OPTICONERROR);
				}
			}
		}
		
		ImGui::TextWrapped("Load Preview Prefab");
		ImGui::NextColumn();

		ImGui::Columns(1);
		ImGui::End();

		ImGui::PopStyleVar();
		ImGui::PopStyleColor();

		ImGui::Begin("Physics Builder");
		
		if (ImGui::TreeNodeEx("Physics Material", ImGuiTreeNodeFlags_DefaultOpen)) {
			ImGui::DragFloat("Elasticity", &currentMaterialItem.elasticity, 0.01f, 0.0f, 1.f);
			ImGui::DragFloat("Friction Coef.", &currentMaterialItem.frictionCoef, 0.01f, 0.0f, 1.f, "%.3f");
			ImGui::DragFloat("Loose Friction Coef.", &currentMaterialItem.looseFrictionCoef, 0.01f, 0.0f, 1.f, "%.3f");
			ImGui::DragFloat("Softness", &currentMaterialItem.softness, 0.01f, 0.0f, 4.f);
			ImGui::DragFloat("Absorption", &currentMaterialItem.absorption, 0.01f, 0.0f, 4.f);

			ImGui::TreePop();
		}

		ImGui::Checkbox("Custom Center of Mass", &currentPhysicsItem.enableCustomCOM);
		if (!currentPhysicsItem.enableCustomCOM) ImGui::BeginDisabled();
		auto com = currentPhysicsItem.computedCOM;
		com = { com .x, com .z, com .y};
		ImGuiEx::drawTransformControl("Center of mass", &com, 0.f);
		currentPhysicsItem.computedCOM = { com.x, com.z, com.y };
		if (!currentPhysicsItem.enableCustomCOM) ImGui::EndDisabled();
		ImGui::DragFloat("Mass (kg)", &currentPhysicsItem.mass, 0.1f, 0.001f, 1000.f, "%.3f", ImGuiSliderFlags_Logarithmic);
		if (ImGui::Combo("Collider", (int*)&currentPhysicsItem.colliderType, "Sphere\0Box\0Infinite Plane\0Capsule\0Cylinder\0ConvexHull Asset\0Mesh Asset")) {

		}
		preview_builder->getActorFromTag("FLAG_EDITOR.GizmoActivePoint").makeVisible();
		switch ((int)currentPhysicsItem.colliderType) {
		case(0):
			ImGui::DragFloat("Radius", &currentPhysicsItem.sphereRadius, 0.05f, 0.0f, 999.f);
			break;
		case(1):
			ImGuiEx::drawTransformControl("Extents", &currentPhysicsItem.boxExtent, 1.f);
			break;
		case(3):
			ImGui::DragFloat("Radius", &currentPhysicsItem.cylindricalRadius, 0.05f, 0.0f, 5.f);
			ImGui::DragFloat("Length", &currentPhysicsItem.cylindricalLength, 0.05f, 0.0f, 10.f);
			currentPhysicsItem.cylindricalRadius = glm::min(currentPhysicsItem.cylindricalLength / 2.0f, currentPhysicsItem.cylindricalRadius);
			break;
		case(4):
			ImGui::DragFloat("Radius", &currentPhysicsItem.cylindricalRadius, 0.05f, 0.0f, 10.f);
			ImGui::DragFloat("Length", &currentPhysicsItem.cylindricalLength, 0.05f, 0.0f, 10.f);
			break;
		case(5):
			for (int i = 0; i < currentPhysicsItem.convexPoints.size(); i++) {
				glm::vec3 position = currentPhysicsItem.convexPoints[i];
				position = { position.x, position.z, position.y };
				ImGuiEx::drawTransformControl(("Point #" + std::to_string(i) + "(%.2f, %.2f, %.2f)"), position);
				currentPhysicsItem.convexPoints[i] = { position.x, position.z, position.y };
			}
			break;
		case(6):
			if (ImGuiEx::drawAssetBox("Trimesh File", AssetType::Model3D, currentPhysicsItem.trimeshAsset)) {
			}
			ImGui::Text("Note: Trimesh colliders must have inverted normals to work");
			break;
		}
		preview_builder->getActorFromTag("FLAG_EDITOR.GizmoActivePoint").makeVisible();

		if (ImGui::Button("Rebuild")) {
			rebuild();
		}
		if (ImGui::Button("Save Material")) {
			SHARD3D_NOIMPL;
		}
		if (ImGui::Button("Save Asset")) {
			AssetManager::createPhysicsAsset(currentPhysicsAsset.getAbsolute(), currentPhysicsItem);
		}
		if (currentPhysicsItem.colliderType == PhysicsColliderType::ConvexHullAsset) {
			if (convexStat_numConcavePoints > 0) {
				ImGui::TextColored(ImVec4(1.0, 0.8, 0.0, 1.0), "Warning! %i points are concave,\nsuch points will be ignored...", convexStat_numConcavePoints);
			}
		}

		preview_builder->getActorFromTag("PhysicsActor").getComponent<Components::Rigidbody3DComponent>().asset = currentPhysicsItem;

		if (currentPhysicsItem.colliderType == PhysicsColliderType::ConvexHullAsset) {
			RPhys::ConvexHull hull = RPhys::buildConvexHull(currentPhysicsItem.convexPoints);
			convexStat_numConcavePoints = currentPhysicsItem.convexPoints.size() - hull.points.size();
		}
		ImGui::End();
	}
	void PhysicsBuilderPanel::rebuild() {
		if (currentPhysicsItem.colliderType == PhysicsColliderType::ConvexHullAsset) {
			if (currentPhysicsItem.enableCustomCOM) {
				RPhys::ConvexHullCollider c{ currentPhysicsItem.convexPoints, currentPhysicsItem.computedCOM };
				currentPhysicsItem.computedInteriaTensor = c.getIntertiaTensor();
			} else {
				RPhys::ConvexHullCollider c{ currentPhysicsItem.convexPoints };
				currentPhysicsItem.computedCOM = c.getCenterOfMass();
				currentPhysicsItem.computedInteriaTensor = c.getIntertiaTensor();
			}

			RPhys::ConvexHull hull = RPhys::buildConvexHull(currentPhysicsItem.convexPoints);
			convexStat_numConcavePoints = currentPhysicsItem.convexPoints.size() - hull.points.size();
		}
		
		preview_builder->getActorFromTag("PhysicsActor").getComponent<Components::Rigidbody3DComponent>().asset = currentPhysicsItem;
		//preview_builder->getActorFromTag("PhysicsActor").getComponent<Components::Rigidbody3DComponent>().material = ;
	}
	void PhysicsBuilderPanel::destroy() {
	}
	void PhysicsBuilderPanel::setPhysicsAsset(const AssetID& asset) {
		currentPhysicsAsset = asset;
		rebuild();
	}
	void PhysicsBuilderPanel::mirrorConvexShapeOnDirection(glm::vec3 mirrorTo) {
		mirrorTo = { mirrorTo.x, mirrorTo.z, mirrorTo.y };
		// remove
		for (int i = 0; i < currentPhysicsItem.convexPoints.size(); i++) {
			if (currentPhysicsItem.convexPoints.size() <= 2) break; // convex hull requires at least 4 vertices,
			auto& point = currentPhysicsItem.convexPoints[i];
			if (glm::dot(mirrorTo, point) > 0.0f) {
				currentPhysicsItem.convexPoints.erase(currentPhysicsItem.convexPoints.begin() + i);
				i--;
			}
		}
		// mirror
		int size = currentPhysicsItem.convexPoints.size();
		for (int i = 0; i < size; i++) {
			auto& point = currentPhysicsItem.convexPoints[i];
			if (glm::abs(glm::dot(mirrorTo, point)) > glm::epsilon<float>()) { // mirror if not on the dividing axis
				glm::vec3 mirrorVector = -(glm::abs(mirrorTo) - 0.5f) * 2.0f;
				currentPhysicsItem.convexPoints.push_back(point * mirrorVector);
			}
		}

	}

	void PhysicsBuilderPanel::beginSimulation() {
		rebuild();
		bool success = LevelSimulationState::instantiate(preview_builder, PlayState::Simulating);
		if (!success) {
			MessageDialogs::show("Failed to simulate physics. Please stop any other simulations before beginning a new one.", "Play error", MessageDialogs::OPTICONERROR);
		} else {
			LevelSimulationState::eventBegin();
		}
	}
	void PhysicsBuilderPanel::stopSimulation() {
		LevelSimulationState::eventEnd();
		LevelSimulationState::release();
		preview_builder->getActorFromTag("PhysicsActor").getTransform().setTranslation({ 0.f, 0.f, 3.f });
		preview_builder->getActorFromTag("PhysicsActor").getTransform().setRotation({ 0.f, 0.f, 0.f });
		preview_builder->getActorFromTag("PhysicsActor").getComponent<Components::Rigidbody3DComponent>().angularVelocity = {};
		preview_builder->getActorFromTag("PhysicsActor").getComponent<Components::Rigidbody3DComponent>().linearVelocity = {};
	}
	void PhysicsBuilderPanel::pauseSimulation() {
		preview_builder->simulationState = PlayState::Paused;
	}
	void PhysicsBuilderPanel::resumeSimulation() {
		preview_builder->simulationState = PlayState::Simulating;
	}
}