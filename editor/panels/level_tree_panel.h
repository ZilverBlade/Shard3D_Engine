#pragma once

#include <Shard3D/ecs.h>
namespace Shard3D {
	class ResourceSystem;
	class LevelTreePanel {
	public:
		LevelTreePanel();
		~LevelTreePanel();

		void setContext(const sPtr<Level>& levelContext, ResourceSystem* resourceSystem);
		void destroyContext();
		void clearSelectedActor();

		void render();

		inline Actor& getSelectedActor() { return selectedActor; }
		Actor selectedActor;
		Actor hoveredActor;
		Actor draggingActor;
	private:
		void drawActorEntry(Actor actor);
		void drawWSI();
		sPtr<Level> context;
		VkDescriptorSet circleIcon;
		ResourceSystem* resourceSystem;
	};
}