#include <imgui.h>
#include "level_peekers.h"
#include <Shard3D/ecs.h>
#include <Shard3D/core/asset/assetmgr.h>
#include <Shard3D/utils/dialogs.h>
#include <imgui_internal.h>
#include <Shard3DEditorKit/kit.h>
#include <Shard3D/core/vulkan_api/bindless.h>
#include <Shard3D/systems/handlers/vertex_arena_allocator.h>

namespace Shard3D {

	float deltaTimeFromLastSecond{};
	float timeSinceLastSecond{};
	static bool liveRebuild;

	LevelPeekingPanel::LevelPeekingPanel(const sPtr<Level>& levelContext, ResourceSystem* system) {
		setContext(levelContext, system);
	}

	LevelPeekingPanel::~LevelPeekingPanel() { context = nullptr; }

	void LevelPeekingPanel::setContext(const sPtr<Level>& levelContext, ResourceSystem* system) { context = levelContext; resourceSystem = system; }
	void LevelPeekingPanel::destroyContext() { context = {}; }

	void LevelPeekingPanel::render(FrameInfo2& frameInfo) {
		SHARD3D_ASSERT(context != nullptr && "Context not provided!");

		timeSinceLastSecond += frameInfo.frameTime;

		ImGui::Begin("Level Peeker");
		ImGui::Checkbox("Actor Inspector", &actorInspector);
		ImGui::Separator();
		ImGui::Checkbox("LOD Inspector", &lodInspector);
		ImGui::Checkbox("Texture Inspector", &textureInspector);
		ImGui::Checkbox("Cubemap Inspector", &textureCubeInspector);
		ImGui::Separator();
		ImGui::Checkbox("Material Inspector", &materialInspector);
		ImGui::Separator();
		ImGui::Checkbox("Miscellaneous Inspector", &miscInspector);
		ImGui::Separator();
		ImGui::Checkbox("Vulkan API Inspector", &vkInspector);
		ImGui::End();

		if (actorInspector) peekActorInspector();
		if (lodInspector) peekLODInspector();
		if (textureInspector) peekTextureInspector();
		if (textureCubeInspector) peekTextureCubeInspector();
		if (materialInspector) peekMaterialInspector();
		if (miscInspector) peekMisc();
		if (vkInspector) peekVK();
	}

	static void drawColorControl(const std::string& label, glm::vec3& values, float resetValue = 0.0f, float stepVal = 0.01f, float columnWidth = 100.f) {
		ImGui::PushID(label.c_str());

		ImGui::Columns(2);
		ImGui::SetColumnWidth(0, columnWidth);
		ImGui::Text(label.c_str());
		ImGui::NextColumn();

		ImGui::PushMultiItemsWidths(3, ImGui::CalcItemWidth());
		ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0.f, 0.f));
		float lineHeight = ENGINE_FONT_SIZE + GImGui->Style.FramePadding.y * 2.f;
		ImVec2 buttonSize = { lineHeight + 3.f, lineHeight };

		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.7f, 0.f, 0.f, 1.f));
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4(0.9f, 0.f, 0.f, 1.f));
		ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(0.9f, 0.2f, 0.2f, 1.f));
		if (ImGui::Button("R", buttonSize)) values.x = resetValue;
		ImGui::PopStyleColor(3);
		ImGui::SameLine(); ImGui::DragFloat("##R", &values.x, stepVal, 0.f, 1.f); ImGui::PopItemWidth(); ImGui::SameLine();

		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.f, 0.5f, 0.f, 1.f));
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4(0.f, 0.7f, 0.f, 1.f));
		ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(0.3f, 0.7f, 0.3f, 1.f));
		if (ImGui::Button("G", buttonSize)) values.z = resetValue;
		ImGui::PopStyleColor(3);
		ImGui::SameLine(); ImGui::DragFloat("##G", &values.y, stepVal, 0.f, 1.f); ImGui::PopItemWidth(); ImGui::SameLine();

		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.f, 0.f, 0.7f, 1.f));
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4(0.f, 0.f, 0.9f, 1.f));
		ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(0.2f, 0.2f, 0.9f, 1.f));
		if (ImGui::Button("B", buttonSize)) values.y = resetValue;
		ImGui::PopStyleColor(3);
		ImGui::SameLine(); ImGui::DragFloat("##B", &values.z, stepVal, 0.f, 1.f); ImGui::PopItemWidth();

		ImGui::PopStyleVar();
		ImGui::Columns(1);

		ImGui::PopID();
	}


	void LevelPeekingPanel::peekActorInspector() {
		ImGui::Begin("Actor Inspector");
		context->registry.each([&](auto actorGUID) {
			ECS::Actor actor{ actorGUID, context.get() };

			std::string text = actor.getTag() + " (" + std::to_string((uint64_t)actor.getScopedID()) + ", S"+ std::to_string((uint64_t)actor.getInstancerUUID())+")"
				+ " handle (" + std::to_string((uint32_t)actor.actorHandle) + ")";
			if (actor.isInvalid())
				ImGui::TextColored({1.f, 0.f, 1.f, 1.f}, text.c_str());
			else
				ImGui::Text(text.c_str());
		});
		ImGui::End();
	}

	void LevelPeekingPanel::peekTextureInspector() {
		ImGui::Begin("Texture Inspector");
		ImGui::Text((std::string("Texture count: " + std::to_string(resourceSystem->getTextureAssets().size())).c_str()));
		for (auto& asset :resourceSystem->getTextureAssets()) {
			ImGui::Text(fmt::format("{} Index #{}", asset.first.getAsset().c_str(),resourceSystem->textureAssets[asset.first]->getResourceIndex()).c_str());
		}
		if (ImGui::Button("Force delete all textures (dangerous)")) {
			if (MessageDialogs::show(std::string("Deletion of all assets can be dangerous if assets are in use.\nUnexpected crashes may happen if one of these assets is in use.\nAre you sure you want to proceed ?").c_str(), "Caution!", MessageDialogs::OPTICONEXCLAMATION | MessageDialogs::OPTYESNO) == MessageDialogs::RESYES) {
				resourceSystem->clearTextureAssets();
			}
		}
		ImGui::End();
	}

	void LevelPeekingPanel::peekTextureCubeInspector() {
		ImGui::Begin("Cubemap Inspector");
		ImGui::Text((std::string("Cubemap count: " + std::to_string(resourceSystem->getTextureCubeAssets().size())).c_str()));
		for (auto& asset :resourceSystem->getTextureCubeAssets()) {
			ImGui::Text(fmt::format("{} Index #{}", asset.first.getAsset().c_str(),resourceSystem->textureCubeAssets[asset.first]->getResourceIndex()).c_str());
		}
		if (ImGui::Button("Force delete all textures (dangerous)")) {
			if (MessageDialogs::show(std::string("Deletion of all assets can be dangerous if assets are in use.\nUnexpected crashes may happen if one of these assets is in use.\nAre you sure you want to proceed ?").c_str(), "Caution!", MessageDialogs::OPTICONEXCLAMATION | MessageDialogs::OPTYESNO) == MessageDialogs::RESYES) {
				resourceSystem->clearTextureCubeAssets();
			}
		}
		ImGui::End();
	}

	void LevelPeekingPanel::peekLODInspector() {
		ImGui::Begin("Mesh & LOD Inspector");
		ImGui::Text((std::string("Vertex allocator index capacity: " + std::to_string(resourceSystem->getVertexAllocator()->getBuffer().index->getInstanceCount())).c_str()));
		ImGui::Text((std::string("Vertex allocator vertex capacity: " + std::to_string(resourceSystem->getVertexAllocator()->getBuffer().vertexPosition->getInstanceCount())).c_str()));

		size_t totalSize = 0;
		totalSize += resourceSystem->getVertexAllocator()->getBuffer().index->getBufferSize();
		totalSize += resourceSystem->getVertexAllocator()->getBuffer().vertexPosition->getBufferSize();
		totalSize += resourceSystem->getVertexAllocator()->getBuffer().vertexUV->getBufferSize();
		totalSize += resourceSystem->getVertexAllocator()->getBuffer().vertexNormal->getBufferSize();
		if (resourceSystem->getVertexAllocator()->getBuffer().vertexTangent) {
			totalSize += resourceSystem->getVertexAllocator()->getBuffer().vertexTangent->getBufferSize();
		}
		ImGui::Text((std::string("Vertex allocator memory usage (kB): " + std::to_string(totalSize / 1000)).c_str()));

		ImGui::Text((std::string("Mesh count: " + std::to_string(resourceSystem->getMeshAssets().size())).c_str()));
		for (auto& asset :resourceSystem->getMeshAssets()) {
			ImGui::Text(asset.first.getAsset().c_str());
		}
		if (ImGui::Button("Force delete all models (dangerous)")) {
			if (MessageDialogs::show(std::string("Deletion of all assets can be dangerous if assets are in use.\nUnexpected crashes may happen if one of these assets is in use.\nAre you sure you want to proceed ?").c_str(), "Caution!", MessageDialogs::OPTICONEXCLAMATION | MessageDialogs::OPTYESNO) == MessageDialogs::RESYES) {
				resourceSystem->clearMeshAssets();
			}
		}
		ImGui::End();
	}

	void LevelPeekingPanel::peekMaterialInspector() {
		ImGui::Begin("Material Inspector");
		ImGui::Text((std::string("Material count: " + std::to_string(resourceSystem->getMaterialAssets().size())).c_str()));
		for (auto& asset :resourceSystem->getMaterialAssets()) {
			ImGui::Text(asset.first.getAsset().c_str());
		}
		if (ImGui::Button("Force delete all materials (dangerous)")) {
			if (MessageDialogs::show(std::string("Deletion of all assets can be dangerous if assets are in use.\nUnexpected crashes may happen if one of these assets is in use.\nAre you sure you want to proceed ?").c_str(), "Caution!", MessageDialogs::OPTICONEXCLAMATION | MessageDialogs::OPTYESNO) == MessageDialogs::RESYES) {
				resourceSystem->clearMaterialAssets();
			}
		}
		ImGui::End();
	}


	void LevelPeekingPanel::peekMisc() {
		ImGui::Begin("Misc Inspector");
		ImGui::Text(std::string("Active level ("+ context->levelAsset.getAsset()  +") ptr: 0x" + std::to_string((int)context.get())).c_str());

		ImGui::Text(std::string("Possessed camera actor: " + context->getPossessedCameraActor().getTag() + " (0x" + std::to_string((int)&context->getPossessedCamera()) + ")").c_str());
		ImGui::Text("Asset References Debug");

		for (auto& [asset, ref] : AssetReferenceTracker::getLookupTable()) {
			if (ImGui::TreeNode(std::string(asset.getAsset() + "##assetrefdbg").c_str())) {
				if (ImGui::TreeNode(std::string("Users##assetrefdbg" + asset.getAsset()).c_str())) {
					for (auto& user : ref.users) {
						ImGui::Text(user.getAsset().c_str());
					}
					ImGui::TreePop();
				}
				if (ImGui::TreeNode(std::string("Usees##assetrefdbg" + asset.getAsset()).c_str())) {
					for (auto& user : ref.usees) {
						ImGui::Text(user.getAsset().c_str());
					}
					ImGui::TreePop();
				}
				ImGui::TreePop();
			}
		}

		ImGui::End();
	}
	void LevelPeekingPanel::peekVK() {
		ImGui::Begin("Vulkan API Inspector");
		ImGui::Text("Bindless debug");
		for (auto& [binding, indices] : resourceSystem->staticMaterialPool->getAllocated()) {
			if (ImGui::TreeNode(std::string("Binding #" + std::to_string(binding) + "##vkbindlessdbg").c_str())) {
				for (auto& [index, allocated] : indices) {
					if (allocated) {
						ImGui::Text(fmt::format("Index #{}", index).c_str());
					}
				}
				ImGui::TreePop();
			}
		}

		ImGui::End();
	}
}