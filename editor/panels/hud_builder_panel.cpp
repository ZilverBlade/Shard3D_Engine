#include "hud_builder_panel.h"

#include <imgui.h>
#include <fstream>
#include <Shard3D/core/asset/assetmgr.h>
#include <Shard3D/utils/dialogs.h>
#include <glm/gtc/type_ptr.hpp>
#include <Shard3D/systems/handlers/render_list.h>
#include <Shard3D/scripting/script_engine.h>
#include <Shard3DEditorKit/kit.h>
#include <imgui_internal.h>
#include <Shard3DEditorKit/gui/icons.h>
namespace Shard3D {
	const static ImGuiTreeNodeFlags nodeFlags = ImGuiTreeNodeFlags_DefaultOpen | ImGuiTreeNodeFlags_AllowItemOverlap;

	HUDBuilderPanel::HUDBuilderPanel(ResourceSystem* resourceSystem, AssetID hudAsset) : resourceSystem(resourceSystem){
		currentAsset = hudAsset;
	}
	HUDBuilderPanel::~HUDBuilderPanel() {

	}
	void HUDBuilderPanel::drawElementEntry(const std::string& elementName, HUDElementData* element, std::unordered_map<std::string, HUDElementData*>& list) {
		if (!element) return;
		bool allowsChildren = dynamic_cast<SHUD::Element::Panel*>(element->genericBase);

		ImGuiTreeNodeFlags flags = ((selectedElement == elementName) ? ImGuiTreeNodeFlags_Selected : 0) | (!allowsChildren ? ImGuiTreeNodeFlags_Leaf : 0) | ImGuiTreeNodeFlags_OpenOnArrow;
		bool expanded = ImGui::TreeNodeEx(elementName.c_str(), flags);

		if (ImGui::IsItemClicked() && draggingElement != elementName) selectedElement = elementName;
		if (ImGui::IsItemHovered() && draggingElement != elementName) hoveredElement = elementName;

		//if (ImGui::BeginDragDropTarget()) {
		//	if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SHARD3D.ACTOR")) {
		//		std::string child = (*reinterpret_cast<std::string*>(payload->Data));
		//		if (hoveredElement != child)
		//			if (child.hasRelationship())
		//				context->reparentActor(child, hoveredActor);
		//			else
		//				context->parentActor(child, hoveredActor);
		//		draggingActor = Actor();
		//	}
		//	ImGui::EndDragDropTarget();
		//}
			// remove stuff
		//bool actorExists = true;
		//if (selectedActor)
		//	if (ImGui::BeginPopupContextItem("actor properties popup", ImGuiPopupFlags_MouseButtonRight | ImGuiPopupFlags_AnyPopup)) {
		//		if (ImGui::MenuItem("Remove Actor")) actorExists = false;
		//		if (ImGui::MenuItem("Duplicate Actor")) {
		//			context->duplicateActor(actor);
		//		}
		//
		//		if (ImGui::MenuItem("Copy UUID to clipboard")) ImGui::SetClipboardText(std::to_string(selectedActor.getUUID()).c_str());
		//		if (ImGui::MenuItem("Add Child")) {
		//			context->parentActor(context->createActor(), selectedActor);
		//		}
		//
		//		ImGui::EndPopup();
		//	}
		//
		if (expanded) {
			ImGui::TreePop();
		}
		//if (ImGui::BeginDragDropSource()) {
		//	draggingActor = hoveredActor;
		//	uint32_t src = (uint32_t)hoveredActor.actorHandle;
		//	ImGui::SetDragDropPayload("SHARD3D.ACTOR", &src, sizeof(uint32_t), ImGuiCond_Once);
		//	ImGui::Text(draggingActor.getTag().c_str());
		//	ImGui::EndDragDropSource();
		//}
		//
		//if (!actorExists) {
		//	context->killActor(actor);
		//	if (selectedActor == actor) selectedActor = {};
		//}
	}

	static std::string randomString(const int len) {
		static const char alphanum[] =
			"0123456789"
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			"abcdefghijklmnopqrstuvwxyz";
		std::string tmp_s;
		tmp_s.reserve(len);

		for (int i = 0; i < len; ++i) {
			tmp_s += alphanum[UUID() % (sizeof(alphanum) - 1)];
		}
		
		return tmp_s;
	}

	void HUDBuilderPanel::render(FrameInfo2& frameInfo, std::unordered_map<std::string, HUDElementData*>& list) {
		if (currentAsset) {
			load(currentAsset, list);
			currentAsset = AssetID::null();
		}
		ImVec2 btnSize = { 48.f, 48.f };
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4());
		ImGui::PushStyleVar(ImGuiStyleVar_FrameBorderSize, 0.f);
		ImGui::Begin("_editor_quickbar_HUDeditor", nullptr, ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoTitleBar);

		{

			float panelWidth = ImGui::GetContentRegionAvail().x;
			int columnCount = std::max((int)(panelWidth / (btnSize.x + 16.f))// <--- thumbnail size (96px) + padding (16px)
				, 1);
			ImGui::Columns(columnCount, 0, false);
		}
		// begin

		// Save/Load
		if (ImGui::ImageButton(getImGuiIcon().save, btnSize)) {
			std::string filepath = BrowserDialogs::saveFile(ENGINE_SHARD3D_ASSETFILE_OPTIONS);
			if (!filepath.empty()) {
				HUDRenderSystem::createHUDLayerAsset(filepath, list);
			}
		}
		ImGui::TextWrapped("Save");
		ImGui::NextColumn();
		if (ImGui::ImageButton(getImGuiIcon().load, btnSize)) {
			if (MessageDialogs::show("This will overwrite the current hud asset, and unsaved changes will be lost! Are you sure you want to continue?", "WARNING!", MessageDialogs::OPTYESNO | MessageDialogs::OPTICONEXCLAMATION | MessageDialogs::OPTDEFBUTTON2) == MessageDialogs::RESYES) {
				std::string filepath = BrowserDialogs::openFile(ENGINE_SHARD3D_ASSETFILE_OPTIONS);
				if (!filepath.empty()) {
					HUDRenderSystem::eraseHUDLayerData(list);
					HUDRenderSystem::loadHUDLayerAsset(filepath, list, resourceSystem);
				}
			}
		}
		ImGui::TextWrapped("Load");
		ImGui::NextColumn();
		ImGui::NextColumn();

		ImGui::Columns(1);
		ImGui::End();
		ImGui::PopStyleVar();
		ImGui::PopStyleColor();


		ImGui::Begin("Element Tree");
		if (ImGui::IsMouseDown(0) && ImGui::IsWindowHovered()) {
			selectedElement = {};
		}
		for (auto& [str, el] : list) {
			drawElementEntry(str, el, list);
		}

		if (ImGui::BeginPopupContextWindow(0, ImGuiPopupFlags_MouseButtonRight | ImGuiPopupFlags_NoOpenOverItems)) {
			if (ImGui::MenuItem("New Line Element")) {
				auto* line = new SHUD::Element::Line();
				selectedElement = allocateNewElement("Line", line, list);
			}
			if (ImGui::MenuItem("New Rect Element")) {
				auto* rect = new SHUD::Element::Rect();
				selectedElement = allocateNewElement("Rect", rect, list);
			}
			if (ImGui::MenuItem("New Text Element")) {
				auto* txt = new SHUD::Element::Text();
				selectedElement = allocateNewElement("Text", txt, list);
				txt->FormatText("Text");
			}
			if (ImGui::MenuItem("New Button Element")) {
				auto* btn = new SHUD::Element::Button();
				selectedElement = allocateNewElement("Button", btn, list);
				btn->SetUniqueTag(selectedElement);

			}
			if (ImGui::MenuItem("New Image Element")) {
				auto* image = new SHUD::Element::Image();
				selectedElement = allocateNewElement("Image", image, list);
			}
			ImGui::EndPopup();
		}
		ImGui::End();
	}

	static glm::vec4 toVec4Col(SHUD::RGBAColor col) {
		return {
			col.r / 255.f,
			col.g / 255.f,
			col.b / 255.f,
			col.a / 255.f
		};
	}

	void HUDBuilderPanel::renderElementPanel(FrameInfo2& frameInfo, std::unordered_map<std::string, HUDElementData*>& list, ResourceSystem* resourceSystem) {
		ImGui::Begin("HUD Element Properties");
		if (selectedElement.empty()) {
			ImGui::End();
			return;
		}

		HUDElementData* element = findElement(selectedElement, list);

		int layer = element->genericBase->mLayer;
		ImGui::DragInt("Layer Index", &layer, 0.5f, 0, 65535);
		element->genericBase->mLayer = layer;
		if (ImGui::TreeNodeEx("Anchoring", nodeFlags)) {
			for (int y = 0; y < 3; y++) {
				for (int x = 0; x < 3; x++) {
					if (x > 0)
						ImGui::SameLine();
					glm::vec2 coord = { static_cast<float>(x), static_cast<float>(y) };
					glm::vec2 anchor = glm::vec2(coord.x / 2.f - 0.5f, -coord.y / 2.f + 0.5f) * 2.0f;
					if (ImGui::Button(std::string(std::to_string((int)anchor.x) + ", " + std::to_string((int)anchor.y).c_str()).c_str(), ImVec2(50, 50))) {
						element->genericBase->mAnchorOffset = { anchor .x, anchor .y};
					}
				}
			}
			ImGui::TreePop();
		}

		if (SHUD::Element::InteractiveDrawableBase* interactive =
			dynamic_cast<SHUD::Element::InteractiveDrawableBase*>(element->genericBase); interactive) {
			ImGui::DragInt("Tab Index", (int*)&interactive->mTabIndex, 0.5f, -1, 65535);
		}
		memset(editingElementNameBuff, 0, 256);
		strncpy(editingElementNameBuff, selectedElement.c_str(), 256);

		if (ImGui::InputText("Unique Element Name", editingElementNameBuff, 256)) {
			if (ImGui::IsKeyPressed(ImGuiKey::ImGuiKey_Enter)) {
				if (!tryRenameElement(selectedElement, editingElementNameBuff, list)) {
					MessageDialogs::show(std::string("Element named '" + std::string(editingElementNameBuff) + "' already exists!\nTry naming your element something else.").c_str(), "Renaming Error", MessageDialogs::OPTICONERROR);
				} else {
					selectedElement = editingElementNameBuff;
				}
			}
		}
		if (SHUD::Element::Line* line = dynamic_cast<SHUD::Element::Line*>(element->genericBase); line) {
			if (ImGui::TreeNodeEx((void*)typeid(SHUD::Element::Line).hash_code(), nodeFlags, "Line Properties")) {
				ImGuiEx::drawTransform2DControl("Point A", (glm::vec2&)line->mPointA, 0.0f, 1.0f, "%.1f");
				ImGuiEx::drawTransform2DControl("Point B", (glm::vec2&)line->mPointB, 0.0f, 1.0f, "%.1f");
				ImGui::DragFloat("Line Width", (float*)&line->mWidth);

				glm::vec4 color = toVec4Col(line->mColor);
				ImGui::ColorPicker4("Color", glm::value_ptr(color));
				line->mColor = (SHUD::fvec4&)color;

				ImGui::TreePop();
			}
		}
		if (SHUD::Element::Rect* rect = dynamic_cast<SHUD::Element::Rect*>(element->genericBase); rect) {
			if (ImGui::TreeNodeEx((void*)typeid(SHUD::Element::Rect).hash_code(), nodeFlags, "Rect Properties")) {
				ImGuiEx::drawTransform2DControl("Transform UV Offset", (glm::vec2&)rect->mTransform.mTransformOffset, 0.0f, 0.01f, "%.1f");
				ImGuiEx::drawTransform2DControl("Position", (glm::vec2&)rect->mTransform.mPosition, 0.0f, 1.0f, "%.1f");
				ImGuiEx::drawTransform2DControl("Scale", (glm::vec2&)rect->mTransform.mScale, 64.0f, 1.0f, "%.1f");
				float rotDeg = glm::degrees(rect->mTransform.mRotation);
				ImGui::DragFloat("Rotation", &rotDeg, 1.0f, 0.0f, 0.0f, "%.1f");
				rect->mTransform.mRotation = glm::radians(rotDeg);

				glm::vec4 color = toVec4Col(rect->mColor);
				ImGui::ColorPicker4("Color", glm::value_ptr(color));
				rect->mColor = (SHUD::fvec4&)color;

				ImGui::TreePop();
			}
		}
		if (SHUD::Element::Text* text = dynamic_cast<SHUD::Element::Text*>(element->genericBase); text) {
			if (ImGui::TreeNodeEx((void*)typeid(SHUD::Element::Text).hash_code(), nodeFlags, "Text Properties")) {
				if (ImGui::TreeNodeEx((void*)(typeid(SHUD::Element::Text).hash_code() + 1), nodeFlags, "Formatting")) {
					ImGui::Checkbox("Bold", &text->mFormatting.mBold);
					ImGui::Checkbox("Italic", &text->mFormatting.mItalic);
					ImGui::DragFloat("Font Size (px.)", &text->mFormatting.mSizePx, 0.1f, 2.0f, 1024.0f, "%.1f");
					ImGui::DragFloat("Line Spacing (frac.)", &text->mFormatting.mLineSpacing, 0.01f, 0.0f, 4.0f, "%.2f");

					ImGui::Combo("Horizontal Alignment", (int*)&text->mFormatting.mHAlignment, "Left\0Center\0Right\0");
					ImGui::TreePop();
				}
				ImGui::Checkbox("Var. Formattable", &element->formattable);
				ImGui::BeginDisabled();
				ImGui::Checkbox("Localizable", &element->localizable);
				ImGui::EndDisabled();
				ImGuiEx::drawTransform2DControl("Transform UV Offset", (glm::vec2&)text->mTransform.mTransformOffset, 0.0f, 0.01f, "%.1f");
				ImGuiEx::drawTransform2DControl("Position", (glm::vec2&)text->mTransform.mPosition, 0.0f, 1.0f, "%.1f");
				ImGuiEx::drawTransform2DControl("Scale", (glm::vec2&)text->mTransform.mScale, 1.0f, 0.001f, "%.3f");
				float rotDeg = glm::degrees(text->mTransform.mRotation);
				ImGui::DragFloat("Rotation", &rotDeg, 1.0f, 0.0f, 0.0f, "%.1f");
				text->mTransform.mRotation = glm::radians(rotDeg);

				glm::vec4 color = toVec4Col(text->mColor);
				ImGui::ColorPicker4("Color", glm::value_ptr(color));
				text->mColor = (SHUD::fvec4&)color;

				char editingElTextBuff[16000u];
				memset(editingElTextBuff, 0, 16000u);
				strncpy(editingElTextBuff, text->mText.c_str(), 16000u);

				if (ImGui::InputTextMultiline("Text", editingElTextBuff, 16000u)) {
					text->mText = editingElTextBuff;
				}

				ImGui::TreePop();
			}
		}
		if (SHUD::Element::Button* btn = dynamic_cast<SHUD::Element::Button*>(element->genericBase); btn) {
			if (ImGui::TreeNodeEx((void*)typeid(SHUD::Element::Line).hash_code(), nodeFlags, "Button Properties")) {
				ImGuiEx::drawTransform2DControl("Transform UV Offset", (glm::vec2&)btn->mTransform.mTransformOffset, 0.0f, 0.01f, "%.1f");
				ImGuiEx::drawTransform2DControl("Position", (glm::vec2&)btn->mTransform.mPosition, 0.0f, 1.0f, "%.1f");
				ImGuiEx::drawTransform2DControl("Scale", (glm::vec2&)btn->mTransform.mScale, 64.0f, 1.0f, "%.1f");
				float rotDeg = glm::degrees(btn->mTransform.mRotation);
				ImGui::DragFloat("Rotation", &rotDeg, 1.0f, 0.0f, 0.0f, "%.1f");
				btn->mTransform.mRotation = glm::radians(rotDeg);

				glm::vec4 color = toVec4Col(btn->mColor);
				ImGui::ColorPicker4("Color", glm::value_ptr(color));
				btn->mColor = (SHUD::fvec4&)color;

				ImGui::DragFloat("Active Alpha Cutoff", &btn->mAlphaButtonCutoff, 0.01f, 0.0f, 1.0f);

				if (!ScriptEngine::doesAssemblyExist()) {
					ImGui::TextColored(ImVec4(1.0f, 0.0f, 1.0f, 1.0f), "Script Engine has not been set up!");
				}
				ImGui::BeginDisabled(!ScriptEngine::doesAssemblyExist());
				auto& name = element->scriptClass;
				const auto& hudClasses = ScriptEngine::getHUDElementClasses();
				bool exists = ScriptEngine::doesActorClassExist(name);
				ImGui::Text("Script Module");
				if (!exists) ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.9f, 0.4, 0.7f, 1.0f));
				if (ImGui::BeginCombo("##comboscriptclasstypes", name.c_str())) {
					for (auto& class_ : hudClasses) {
						bool is_selected = (name == class_.first);
						if (ImGui::Selectable(class_.first.c_str(), is_selected))
							name = class_.first;
						if (is_selected)
							ImGui::SetItemDefaultFocus();
					}
					ImGui::EndCombo();
				}
				if (!exists) ImGui::PopStyleColor();

				ImGui::EndDisabled();

				if (ImGuiEx::drawAssetBox("Texture Atlas", AssetType::Texture2D, element->textureAtlasAsset)) {
					resourceSystem->loadTexture(element->textureAtlasAsset);
				}

				if (ImGui::TreeNodeEx("Texture Atlas Config", nodeFlags)) {
					if (ImGui::TreeNode("DefaultTextureCoords")) {
						ImGui::DragFloat2("Min", (float*)&btn->mTexture.mDefaultTextureCoords.mUVOffsetMin, 0.01f, 0.0f, 1.0f);
						ImGui::DragFloat2("Max", (float*)&btn->mTexture.mDefaultTextureCoords.mUVOffsetMax, 0.01f, 0.0f, 1.0f);
						ImGui::TreePop();
					}
					if (ImGui::TreeNode("HoverTextureCoords")) {
						ImGui::DragFloat2("Min", (float*)&btn->mTexture.mHoverTextureCoords.mUVOffsetMin, 0.01f, 0.0f, 1.0f);
						ImGui::DragFloat2("Max", (float*)&btn->mTexture.mHoverTextureCoords.mUVOffsetMax, 0.01f, 0.0f, 1.0f);
						ImGui::TreePop();
					}
					if (ImGui::TreeNode("PressTextureCoords")) {
						ImGui::DragFloat2("Min", (float*)&btn->mTexture.mPressTextureCoords.mUVOffsetMin, 0.01f, 0.0f, 1.0f);
						ImGui::DragFloat2("Max", (float*)&btn->mTexture.mPressTextureCoords.mUVOffsetMax, 0.01f, 0.0f, 1.0f);
						ImGui::TreePop();
					}
					if (ImGui::TreeNode("SelectTextureCoords")) {
						ImGui::DragFloat2("Min", (float*)&btn->mTexture.mSelectTextureCoords.mUVOffsetMin, 0.01f, 0.0f, 1.0f);
						ImGui::DragFloat2("Max", (float*)&btn->mTexture.mSelectTextureCoords.mUVOffsetMax, 0.01f, 0.0f, 1.0f);
						ImGui::TreePop();
					}
					if (ImGui::TreeNode("DisabledTextureCoords")) {
						ImGui::DragFloat2("Min", (float*)&btn->mTexture.mDisabledTextureCoords.mUVOffsetMin, 0.01f, 0.0f, 1.0f);
						ImGui::DragFloat2("Max", (float*)&btn->mTexture.mDisabledTextureCoords.mUVOffsetMax, 0.01f, 0.0f, 1.0f);
						ImGui::TreePop();
					}
					ImGui::TreePop();
				}
				ImGui::TreePop();
			}
		}
		if (SHUD::Element::Image* img = dynamic_cast<SHUD::Element::Image*>(element->genericBase); img) {
			if (ImGui::TreeNodeEx((void*)typeid(SHUD::Element::Image).hash_code(), nodeFlags, "Image Properties")) {
				ImGuiEx::drawTransform2DControl("Transform UV Offset", (glm::vec2&)img->mTransform.mTransformOffset, 0.0f, 0.01f, "%.1f");
				ImGuiEx::drawTransform2DControl("Position", (glm::vec2&)img->mTransform.mPosition, 0.0f, 1.0f, "%.1f");
				ImGuiEx::drawTransform2DControl("Scale", (glm::vec2&)img->mTransform.mScale, 64.0f, 1.0f, "%.1f");
				float rotDeg = glm::degrees(img->mTransform.mRotation);
				ImGui::DragFloat("Rotation", &rotDeg, 1.0f, 0.0f, 0.0f, "%.1f");
				img->mTransform.mRotation = glm::radians(rotDeg);

				glm::vec4 color = toVec4Col(img->mColor);
				ImGui::ColorPicker4("Color", glm::value_ptr(color));
				img->mColor = (SHUD::fvec4&)color;

				if (ImGuiEx::drawAssetBox("Texture Atlas", AssetType::Texture2D, element->textureAtlasAsset)) {
					resourceSystem->loadTexture(element->textureAtlasAsset);
				}
				ImGui::TreePop();
			}
		}
		if (ImGui::Button("Duplicate Element")) {
			selectedElement = copyNewElement(selectedElement, element, list);
		}

		if (ImGui::Button("Export Element")) {

		}

		ImGui::End();
	}
	void HUDBuilderPanel::load(AssetID asset, std::unordered_map<std::string, HUDElementData*>& list) {
		existingElementNames.clear();
		HUDRenderSystem::eraseHUDLayerData(list);
		HUDRenderSystem::loadHUDLayerAsset(currentAsset.getAbsolute(), list, resourceSystem);

		std::function<void(const std::string&, const HUDElementData*)> markElementFunction =
			[&](const std::string& name, const HUDElementData* item) {
			existingElementNames.insert(name);
			for (const auto& [namec, childdata] : item->children) {
				markElementFunction(namec, childdata);
			}
		};
		for (const auto& [namec, childdata] : list) {
			markElementFunction(namec, childdata);
		}
	}
	std::string HUDBuilderPanel::allocateNewElement(const std::string& baseName, SHUD::Element::Base* genericBase, std::unordered_map<std::string, HUDElementData*>& list) {
		std::string str = baseName + " " + randomString(12);
		list[str] = new HUDElementData();
		list[str]->genericBase = genericBase;
		return str;
	}
	std::string HUDBuilderPanel::copyNewElement(const std::string& baseName, HUDElementData* element, std::unordered_map<std::string, HUDElementData*>& list) {
		std::string str = baseName + " " + randomString(12);
		list[str] = new HUDElementData();
		list[str]->scriptClass = element->scriptClass;
		list[str]->textureAtlasAsset = element->textureAtlasAsset;
		if (element->genericBase->mType == SHUD::Element::Type::Button) {
			list[str]->genericBase = new SHUD::Element::Button();
			memcpy(list[str]->genericBase, dynamic_cast<SHUD::Element::Button*>(element->genericBase), sizeof(SHUD::Element::Button));
		} else if (element->genericBase->mType == SHUD::Element::Type::Text) {
			list[str]->genericBase = new SHUD::Element::Text();
			memcpy(list[str]->genericBase, dynamic_cast<SHUD::Element::Text*>(element->genericBase), sizeof(SHUD::Element::Text));
			list[str]->formattable = element->formattable;
			list[str]->localizable = element->localizable;
		} else if (element->genericBase->mType == SHUD::Element::Type::Line) {
			list[str]->genericBase = new SHUD::Element::Line();
			memcpy(list[str]->genericBase, dynamic_cast<SHUD::Element::Line*>(element->genericBase), sizeof(SHUD::Element::Line));
		} else if (element->genericBase->mType == SHUD::Element::Type::Rect) {
			list[str]->genericBase = new SHUD::Element::Rect();
			memcpy(list[str]->genericBase, dynamic_cast<SHUD::Element::Rect*>(element->genericBase), sizeof(SHUD::Element::Rect));
		} else if (element->genericBase->mType == SHUD::Element::Type::Image) {
			list[str]->genericBase = new SHUD::Element::Image();
			memcpy(list[str]->genericBase, dynamic_cast<SHUD::Element::Image*>(element->genericBase), sizeof(SHUD::Element::Image));
		} else if (element->genericBase->mType == SHUD::Element::Type::Panel) {
			list[str]->genericBase = new SHUD::Element::Panel();
			memcpy(list[str]->genericBase, dynamic_cast<SHUD::Element::Panel*>(element->genericBase), sizeof(SHUD::Element::Panel));
			for (auto& [cname, cdata] : element->children) {
				copyNewElement(cname, cdata, list[str]->children);
			}
		}
		return str;
	}
	void HUDBuilderPanel::eraseElement(const std::string& name) {}
	bool HUDBuilderPanel::tryRenameElement(const std::string& oldElementName, const std::string& newElementName, std::unordered_map<std::string, HUDElementData*>& list) {
		if (existingElementNames.find(newElementName) == existingElementNames.end()) {
			HUDElementData* data = list.at(oldElementName);
			list.erase(oldElementName);
			list.emplace(newElementName, data);
			existingElementNames.erase(oldElementName);
			existingElementNames.insert(newElementName);
			return true;
		}
		return false;
	}
	HUDElementData* HUDBuilderPanel::findElement(const std::string& element, std::unordered_map<std::string, HUDElementData*>& list) {
		HUDElementData* data = nullptr;
		if (auto iter = list.find(element); iter == list.end()) {
			for (auto& [str, subData] : list) {
				data = findElement(element, subData->children);
			}
		} else {
			data = iter->second;
		}
		return data;
	}
}