#pragma once

#include <Shard3D/core/ecs/level.h>
#include <Shard3D/core/misc/frame_info.h>
using namespace Shard3D::ECS;
namespace Shard3D {
	class LevelPeekingPanel {
	public:
		LevelPeekingPanel() = default;
		LevelPeekingPanel(const sPtr<Level>& levelContext, ResourceSystem* resourceSystem);
		~LevelPeekingPanel();

		void setContext(const sPtr<Level>& levelContext, ResourceSystem* system);
		void destroyContext();

		void render(FrameInfo2& frameInfo);

	private:
		static inline bool textureInspector, textureCubeInspector, materialInspector, actorInspector, lodInspector, miscInspector, vkInspector;
		
		void peekTextureInspector();
		void peekTextureCubeInspector();
		void peekMaterialInspector();
		void peekActorInspector();
		void peekLODInspector();

		void peekMisc();

		void peekVK();

		sPtr<Level> context;
		ResourceSystem* resourceSystem;
	};
}