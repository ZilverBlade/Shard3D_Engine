#include "level_tree_panel.h"

#include <imgui.h>
#include <Shard3DEditorKit/kit.h>
#include <Shard3D/core/asset/assetmgr.h>
#include <Shard3D/utils/dialogs.h>
#include <Shard3D/systems/handlers/render_list.h>
#include <glm/gtc/type_ptr.inl>
#include <fstream>
namespace Shard3D {
	LevelTreePanel::LevelTreePanel() {
		
	}
	LevelTreePanel::~LevelTreePanel() { context = nullptr; }

	void LevelTreePanel::setContext(const sPtr<Level>& levelContext, ResourceSystem* rs) {
		{
			auto& img = _special_assets::_get().at("editor.cube");
			circleIcon = ImGui_ImplVulkan_AddTexture(img->getSampler(), img->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		context = levelContext; 

		resourceSystem = rs;
	}
	void LevelTreePanel::destroyContext() { context = {}; }
	void LevelTreePanel::clearSelectedActor() { selectedActor = {}; }

	void LevelTreePanel::render() {
		SHARD3D_ASSERT(context != nullptr && "Context not provided!");
		drawWSI();
		ImGui::Begin("Level Tree");
		context->registry.each([&](auto actorGUID) {
			ECS::Actor actor{ actorGUID, context.get() };	
			if (actor.isInvalid()) return;
			if (actor.hasComponent<Components::RelationshipComponent>()) if (actor.getComponent<Components::RelationshipComponent>().parentActor != entt::null) return;
			drawActorEntry(actor);
		});
		if (ImGui::IsMouseDown(0) && ImGui::IsWindowHovered()) selectedActor = {};
		
		// blank space RCLICK
		if (ImGui::BeginPopupContextWindow(0, ImGuiPopupFlags_MouseButtonRight | ImGuiPopupFlags_NoOpenOverItems)) {
			if (ImGui::MenuItem("New Blank Actor")) selectedActor = context->createActor(0);
			ImGui::EndPopup();
		}
		if (ImGui::BeginPopupContextWindow(0, ImGuiPopupFlags_MouseButtonRight | ImGuiPopupFlags_NoOpenOverItems)) {
			if (ImGui::MenuItem("New Camera Actor")) { 
				auto actor = context->createActor(0, "Camera Actor"); 
				actor.addComponent<Components::CameraComponent>(); 
				if (!actor.hasComponent<Components::Mesh3DComponent>()) {
					resourceSystem->loadMesh(AssetID("engine/meshes/camcorder" ENGINE_ASSET_SUFFIX));
					actor.addComponent<Components::PostFXComponent>();
					actor.addComponent<Components::Mesh3DComponent>(resourceSystem, AssetID("engine/meshes/camcorder" ENGINE_ASSET_SUFFIX));
					actor.getComponent<Components::VisibilityComponent>().editorOnly = true;
				}
				selectedActor = actor;
			}
			ImGui::EndPopup();
		}
		if (ImGui::BeginPopupContextWindow(0, ImGuiPopupFlags_MouseButtonRight | ImGuiPopupFlags_NoOpenOverItems)) {
			if (ImGui::MenuItem("New Billboard Actor")) { 
				auto actor = context->createActor(0, "Billboard"); actor.addComponent<Components::BillboardComponent>(ResourceSystem::coreAssets.t_errorTexture);
			}

			 ImGui::EndPopup();
		}
		if (ImGui::BeginPopupContextWindow(0, ImGuiPopupFlags_MouseButtonRight | ImGuiPopupFlags_NoOpenOverItems)) {
			if (ImGui::MenuItem("New Static Mesh")) { 
				auto actor = context->createActor(0, "Cube"); actor.addComponent<Components::Mesh3DComponent>(resourceSystem, ResourceSystem::coreAssets.m_defaultModel);
			}
			ImGui::EndPopup();
		}
		if (ImGui::BeginPopupContextWindow(0, ImGuiPopupFlags_MouseButtonRight | ImGuiPopupFlags_NoOpenOverItems)) {
			if (ImGui::MenuItem("New Point Light")) { 
				auto actor = context->createActor(0, "Point Light"); actor.addComponent<Components::PointLightComponent>(); selectedActor = actor; actor.makeStationary();
			}
			ImGui::EndPopup();
		}
		if (ImGui::BeginPopupContextWindow(0, ImGuiPopupFlags_MouseButtonRight | ImGuiPopupFlags_NoOpenOverItems)) {
			if (ImGui::MenuItem("New Spot Light")) { 
				auto actor = context->createActor(0, "Spot Light"); actor.addComponent<Components::SpotLightComponent>(); selectedActor = actor; actor.makeStationary();
			}
			ImGui::EndPopup();
		}
		if (ImGui::BeginPopupContextWindow(0, ImGuiPopupFlags_MouseButtonRight | ImGuiPopupFlags_NoOpenOverItems)) {
			if (ImGui::MenuItem("New Directional Light")) { 
				auto actor = context->createActor(0, "Directional Light"); actor.addComponent<Components::DirectionalLightComponent>(); selectedActor = actor; actor.makeStationary();
			}
			ImGui::EndPopup();
		}
		if (ImGui::BeginPopupContextWindow(0, ImGuiPopupFlags_MouseButtonRight | ImGuiPopupFlags_NoOpenOverItems)) {
			if (ImGui::MenuItem("New Sky Light")) {
				auto actor = context->createActor(0, "Sky Light"); actor.addComponent<Components::SkyLightComponent>(); selectedActor = actor; actor.makeDynamic();
			}
			ImGui::EndPopup();
		}
		if (ImGui::BeginPopupContextWindow(0, ImGuiPopupFlags_MouseButtonRight | ImGuiPopupFlags_NoOpenOverItems)) {
			if (ImGui::MenuItem("New Sky Box")) {
				auto actor = context->createActor(0, "Sky Box"); actor.addComponent<Components::SkyboxComponent>(); selectedActor = actor; actor.makeStatic();
			}
			ImGui::EndPopup();
		}
		if (ImGui::BeginPopupContextWindow(0, ImGuiPopupFlags_MouseButtonRight | ImGuiPopupFlags_NoOpenOverItems)) {
			if (ImGui::MenuItem("New Exponential Fog")) {
				auto actor = context->createActor(0, "Exponential Fog"); actor.addComponent<Components::ExponentialFogComponent>(); selectedActor = actor; actor.makeStationary();
			}
			ImGui::EndPopup();
		}
		ImGui::End();
	}
	void LevelTreePanel::drawActorEntry(Actor actor) {
		if (actor.isPrefabChild()) return;

		auto& tag = actor.getComponent<Components::TagComponent>().tag;
		bool hasChildren = false;
		if (actor.hasComponent<Components::RelationshipComponent>())
			hasChildren = actor.getComponent<Components::RelationshipComponent>().childActors.size();

		ImGuiTreeNodeFlags flags = ((selectedActor == actor) ? ImGuiTreeNodeFlags_Selected : 0) | (!hasChildren ? ImGuiTreeNodeFlags_Leaf : 0) | ImGuiTreeNodeFlags_OpenOnArrow;
		bool expanded = ImGui::TreeNodeEx((void*)(uint64_t)(uint32_t)actor, flags, tag.c_str());

		if (ImGui::IsItemClicked() && draggingActor != actor) selectedActor = actor;
		if (ImGui::IsItemHovered() && draggingActor != actor) hoveredActor = actor;
		if (ImGui::BeginDragDropTarget()) {
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SHARD3D.ACTOR")) {
				Actor child = { (entt::entity)(*reinterpret_cast<uint32_t*>(payload->Data)), context.get() };
				if (hoveredActor != child)
					if (child.hasRelationship())
						context->reparentActor(child, hoveredActor);
					else
						context->parentActor(child, hoveredActor);
				draggingActor = Actor();
			}
			ImGui::EndDragDropTarget();
		}

	//	ImColor color{};
	//	switch (actor.getMobility()) {
	//	case (Mobility::Static):	color = ImColor(1.0f, 0.0f, 0.0f);	break;
	//	case (Mobility::Stationary):color = ImColor(1.0f, 1.0f, 0.0f);	break;
	//	case (Mobility::Dynamic):	color = ImColor(0.0f, 1.0f, 0.5f);	break;
	//	}
	//	ImGui::SameLine(); 
	//	ImGui::Image(circleIcon, { 4.f, 4.f }, {0.f, 0.0f}, { 1.f, 1.0f }, color);

		// remove stuff
		bool actorExists = true;
		if (selectedActor)
			if (ImGui::BeginPopupContextItem("actor properties popup", ImGuiPopupFlags_MouseButtonRight | ImGuiPopupFlags_AnyPopup)) {
				if (ImGui::MenuItem("Remove Actor")) actorExists = false;
				if (ImGui::MenuItem("Duplicate Actor")) {
					context->duplicateActor(actor, true);
				}

				if (ImGui::MenuItem("Copy ID to clipboard")) ImGui::SetClipboardText(std::to_string(selectedActor.getScopedID()).c_str());
				if (ImGui::MenuItem("Add Child")) {
					context->parentActor(context->createActor(0), selectedActor);
				}

				ImGui::EndPopup();
			}

		if (expanded) {
			if (actor.hasComponent<Components::RelationshipComponent>())
				for (auto& child : actor.getComponent<Components::RelationshipComponent>().childActors)
					drawActorEntry({ child, context.get() });
			ImGui::TreePop();
		}
		if (ImGui::BeginDragDropSource()) {
			draggingActor = hoveredActor;
			uint32_t src = (uint32_t)hoveredActor.actorHandle;
			ImGui::SetDragDropPayload("SHARD3D.ACTOR", &src, sizeof(uint32_t), ImGuiCond_Once);
			ImGui::Text(draggingActor.getTag().c_str());
			ImGui::EndDragDropSource();
		}

		if (!actorExists) {
			context->killActor(actor);
			if (selectedActor == actor) selectedActor = {};
		}
	}
	void LevelTreePanel::drawWSI() 	{
		ImGui::Begin("World Scene Info");
		//ImGuiEx::drawAssetBox("Skybox", AssetType::TextureCube, context->getWorldSceneInfo()->environment);
		//auto& rfile = context->getWorldSceneInfo()->environment.getFileRef();
		//
		//char fileBuffer[256];
		//memset(fileBuffer, 0, 256);
		//strncpy(fileBuffer, rfile.c_str(), 256);
		//
		//if (ImGui::InputText("Skybox", fileBuffer, 256)) {
		//	rfile = std::string(fileBuffer);
		//}
		//if (ImGui::BeginDragDropTarget()) {
		//	if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SHARD3D.ASSEXP.TXC")) {
		//		rfile = std::string(ENGINE_ASSETS_PATH + std::string("\\") + (char*)payload->Data);
		//	}
		//}
		//if (ImGui::Button("Load Texture")) {
		//	std::ifstream ifile(fileBuffer);
		//	if (ifile.good()) {
		//		SHARD3D_INFO_E("Reloading cubemap '{0}'", rfile);
		//		context->getWorldSceneInfo()->environment= AssetID(rfile);
		//		resourceSystem->loadTextureCube(context->getWorldSceneInfo()->environment);
		//	}
		//	else SHARD3D_WARN_E("File '{0}' does not exist!", fileBuffer);
		//}
		//ImGui::ColorEdit3("Skybox tint", glm::value_ptr(context->getWorldSceneInfo()->environmentTint));
		//ImGui::DragFloat("Skybox intensity", &context->getWorldSceneInfo()->environmentTint.w, 0.01f, 0.f, 100.f);
		//ImGui::ColorEdit3("Skylight tint", glm::value_ptr(context->getWorldSceneInfo()->skyInfo));
		//ImGui::DragFloat("Skylight intensity", &context->getWorldSceneInfo()->skyInfo.w, 0.01f, 0.f, 100.f);
		ImGui::End();
	}
}