#include "material_builder_panel.h"

#include <imgui.h>
#include <TextEditor.h>
#include <fstream>
#include <Shard3D/core/asset/assetmgr.h>
#include <glm/gtc/type_ptr.hpp>
#include <Shard3D/systems/handlers/render_list.h>
#include <Shard3DEditorKit/kit.h>
namespace Shard3D {


	static std::unordered_set<AssetID> textureList;

	static void reloadTextureList() {
		textureList.clear();
		for (const auto& entry : std::filesystem::recursive_directory_iterator(ProjectSystem::getAssetLocation())) {
			if (!entry.is_regular_file()) continue;
			std::string assetPath = entry.path().string();
			AssetType assetType = ResourceSystem::discoverAssetType(assetPath);
			if (assetType != AssetType::Texture2D) continue;
			std::string reldp = std::filesystem::relative(assetPath, std::filesystem::path(ProjectSystem::getAssetLocation())).string();
			AssetID asset = AssetID(reldp);

			textureList.insert(asset);
		}
	}
	MaterialBuilderPanel::MaterialBuilderPanel(ResourceSystem* resourceSystem, sPtr<Level>& level, AssetID materialAsset) 
		: currentAsset(materialAsset), preview_builder(level) {
		resourceSystem->loadMaterial(currentAsset);
		currentItem = (SurfaceMaterial*)resourceSystem->retrieveMaterial(currentAsset);
		refresh(resourceSystem);
		reloadTextureList();
	}
	MaterialBuilderPanel::~MaterialBuilderPanel() {}

	static const char* blendModeToString(bool blend) {
		if (blend)
			return "Translucent";
		return "Opaque";
	}

	static bool stringToBlendMode(const char* blendMode) {
		if (strstr(blendMode, "Translucent") != nullptr)
			return true;
		return false;
	}

	static bool renderTextureComboBox(AssetID& assetRef, const char* label) {
		float sz = ImGui::GetFrameHeight();
		if (ImGui::Button(("X###uc832434243r9wq" + std::string(label)).c_str(), { sz, sz })) {
			assetRef = AssetID::null();
			return true;
		}
		ImGui::SameLine();
		AssetID currentOption = assetRef;
		if (ImGui::BeginCombo(label, currentOption.getAsset().c_str())) {
			for (auto& asset : textureList) {
				bool is_selected = (currentOption == asset); // You can store your selection however you want, outside or inside your objects
				if (ImGui::Selectable(asset.getAsset().c_str(), is_selected)) {
					currentOption = asset;
					assetRef = AssetID(asset.getAsset());
					ImGui::EndCombo();
					return true;
				}
				if (is_selected)
					ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
			}
			ImGui::EndCombo();
		}
		if (ImGui::BeginDragDropTarget())
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SHARD3D.ASSEXP.TEX")) {
				assetRef = AssetID(std::string((char*)payload->Data));
				return true;
			}
		return false;
	}

	void MaterialBuilderPanel::render(FrameInfo2& frameInfo) {
		ImGui::Begin("Material Builder");
		if (ImGui::BeginCombo("##listofmaterialscombo", currentAsset.getAsset().c_str())) ImGui::EndCombo();	
		if (ImGui::BeginDragDropTarget()) {
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SHARD3D.ASSEXP.MATE")) {
				currentAsset = std::string((char*)payload->Data);
				setMaterialAsset(frameInfo.resourceSystem, currentAsset);
			}
		}
		
		if (currentItem->getClass() == MaterialClass::Surface) {
			SurfaceMaterialPipelineClassPermutationFlags oldSurfaceMaterialFlags = currentItem->getPipelineClass();

			{
				const char* items[] = { "Opaque", "Translucent" };
				const char* current_item = items[currentItem->isTranslucent()];

				if (ImGui::BeginCombo("##blendopcombo", current_item)) { // The second parameter is the label previewed before opening the combo.
					for (int n = 0; n < IM_ARRAYSIZE(items); n++) {
						bool is_selected = (current_item == items[n]); // You can store your selection however you want, outside or inside your objects
						if (ImGui::Selectable(items[n], is_selected)) {
							current_item = items[n];
							currentItem->setTranslucent(n);
							rebuild(frameInfo.resourceSystem);
						}
						if (is_selected)
							ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
					}
					ImGui::EndCombo();
				}
			}
			{
				const char* deferredItems[] = { "Unshaded", "Shaded", "Emissive", "Foliage", "Dual Layer Clear Coat" };
				const char* forwardItems[] = { "Unshaded", "Shaded", "Emissive", "Pure Refractive Glass" };
				const char* current_item;
				if (currentItem->getPipelineClass() & SurfaceMaterialPipelineClassPermutationOptions_Translucent) {
					switch (static_cast<size_t>(currentItem->shadingModel)) {
					case (0):
						current_item = forwardItems[0];
						break;
					case(1):
						current_item = forwardItems[1];
						break;
					case(2):
						current_item = forwardItems[2];
						break;
					case(5):
						current_item = forwardItems[3];
						break;
					case(6):
						current_item = forwardItems[3];
						break;
					}
				} else
					current_item = deferredItems[static_cast<size_t>(currentItem->shadingModel)];

				if (ImGui::BeginCombo("##shadingtypecombo", current_item)) { // The second parameter is the label previewed before opening the combo.
					if (!(currentItem->getPipelineClass() & SurfaceMaterialPipelineClassPermutationOptions_Translucent)) {
						for (int n = 0; n < IM_ARRAYSIZE(deferredItems); n++) {
							if (n > 2) ImGui::BeginDisabled();
							bool is_selected = (current_item == deferredItems[n]); // You can store your selection however you want, outside or inside your objects
							if (ImGui::Selectable(deferredItems[n], is_selected)) {
								current_item = (char*)deferredItems[n];
								switch (n) {
								case (0):
									currentItem->shadingModel = SurfaceShadingModel::Unshaded;
									break;
								case(1):
									currentItem->shadingModel = SurfaceShadingModel::Shaded;
									break;
								case(2):
									currentItem->shadingModel = SurfaceShadingModel::Emissive;
									break;
								case(3):
									currentItem->shadingModel = SurfaceShadingModel::DeferredFoliage;
									break;
								case(4):
									currentItem->shadingModel = SurfaceShadingModel::DeferredDualLayerClearCoat;
									break;
								}
								rebuild(frameInfo.resourceSystem);
							}
							if (is_selected)
								ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
							if (n > 2) ImGui::EndDisabled();
						}
					} else {
						for (int n = 0; n < IM_ARRAYSIZE(forwardItems); n++) {
							//if (n > 2) ImGui::BeginDisabled();
							bool is_selected = (current_item == forwardItems[n]); // You can store your selection however you want, outside or inside your objects
							if (ImGui::Selectable(forwardItems[n], is_selected)) {
								current_item = (char*)forwardItems[n];
								switch (n) {
								case (0):
									currentItem->shadingModel = SurfaceShadingModel::Unshaded;
									break;
								case(1):
									currentItem->shadingModel = SurfaceShadingModel::Shaded;
									break;
								case(2):
									currentItem->shadingModel = SurfaceShadingModel::Emissive;
									break;
								case(3):
									if (currentItem->shadingModel == SurfaceShadingModel::ForwardPureRefractiveGlass) {
										currentItem->shadingModel = SurfaceShadingModel::ForwardPureRefractiveGlass;
									} else if (currentItem->shadingModel == SurfaceShadingModel::ForwardPureScreenspaceRefractiveGlass) {
										currentItem->shadingModel = SurfaceShadingModel::ForwardPureScreenspaceRefractiveGlass;
									} else {
										currentItem->shadingModel = SurfaceShadingModel::ForwardPureRefractiveGlass;
									}
									break;
								}
								rebuild(frameInfo.resourceSystem);
							}
							if (is_selected)
								ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
							//if (n > 2) ImGui::EndDisabled();
						}
					}
					ImGui::EndCombo();
				}
			}
			auto& surfaceMaterial = currentItem;
			ImGui::Text(typeid(*currentItem).name());
			ImGui::DragFloat2("UV Offset", glm::value_ptr(surfaceMaterial->texCoordOffset), 0.01f);
			ImGui::DragFloat2("UV Scale", glm::value_ptr(surfaceMaterial->texCoordMultiplier), 0.01f);

			if (currentItem->shadingModel == SurfaceShadingModel::Unshaded || currentItem->shadingModel == SurfaceShadingModel::Emissive) {
				ImGui::ColorEdit3("Emissive", glm::value_ptr(surfaceMaterial->emissiveColor));
				float emissionIntensity = surfaceMaterial->emissiveColor.w;
				ImGui::DragFloat("Emissive Strength", &emissionIntensity, 0.005f, 0.0f, 32.0f);
				surfaceMaterial->emissiveColor = glm::vec4(glm::clamp(glm::vec3(surfaceMaterial->emissiveColor), glm::vec3(0.0f), glm::vec3(1.0f)), emissionIntensity);
				if (renderTextureComboBox(surfaceMaterial->emissiveTex, "Emissive Texture")) {
					refresh(frameInfo.resourceSystem);
				}
			}
			if (currentItem->shadingModel >= SurfaceShadingModel::Shaded) {
				ImGui::ColorEdit3("Diffuse", glm::value_ptr(surfaceMaterial->diffuseColor));
				surfaceMaterial->diffuseColor = glm::clamp(surfaceMaterial->diffuseColor, glm::vec3(0.0f), glm::vec3(1.0f));
				if (renderTextureComboBox(surfaceMaterial->diffuseTex, "Diffuse Texture")) {
					refresh(frameInfo.resourceSystem);
				}
				if (currentItem->shadingModel != SurfaceShadingModel::ForwardPureRefractiveGlass && currentItem->shadingModel != SurfaceShadingModel::ForwardPureScreenspaceRefractiveGlass) {
					ImGui::DragFloat("Specular", &surfaceMaterial->specular, 0.01f, 0.f, 1.f);
					surfaceMaterial->specular = glm::clamp(surfaceMaterial->specular, 0.0f, 1.0f);
					if (renderTextureComboBox(surfaceMaterial->specularTex, "Specular Map")) {
						refresh(frameInfo.resourceSystem);
					}
					ImGui::DragFloat("Glossiness", &surfaceMaterial->glossiness, 0.01f, 0.f, 1.f);
					surfaceMaterial->specular = glm::clamp(surfaceMaterial->specular, 0.0f, 1.0f);
					if (renderTextureComboBox(surfaceMaterial->glossinessTex, "Glossiness Map")) {
						refresh(frameInfo.resourceSystem);
					}
				}
				ImGui::DragFloat(!surfaceMaterial->isTranslucent() ? "Reflectivity" : "Clarity", &surfaceMaterial->reflectivity, 0.01f, 0.f, 1.f);
				if (renderTextureComboBox(surfaceMaterial->reflectivityTex, !surfaceMaterial->isTranslucent() ? "Reflectivity Map" : "Clarity Map")) {
					refresh(frameInfo.resourceSystem);
				}
				surfaceMaterial->reflectivity = glm::clamp(surfaceMaterial->reflectivity, 0.0f, 1.0f);
				if (renderTextureComboBox(surfaceMaterial->normalTex, "Normal Map")) {
					refresh(frameInfo.resourceSystem);
				}
			}
			if (renderTextureComboBox(surfaceMaterial->maskTex, "Alpha Mask")) {
				refresh(frameInfo.resourceSystem);
			}
			surfaceMaterial->setMasked(surfaceMaterial->maskTex);

			if (surfaceMaterial->isTranslucent()) {
				ImGui::DragFloat("Opacity", &surfaceMaterial->opacity, 0.01f, 0.f, 1.f);
				surfaceMaterial->opacity = glm::clamp(surfaceMaterial->opacity, 0.0f, 1.0f);
				if (renderTextureComboBox(surfaceMaterial->opacityTex, "Opacity Map")) {
					refresh(frameInfo.resourceSystem);
				}
				ImGui::DragFloat("Blend Weight Bias", &surfaceMaterial->forwardBlendingMulBias, 0.01f, 0.01f, 10.f);

				if (currentItem->shadingModel == SurfaceShadingModel::ForwardPureRefractiveGlass || currentItem->shadingModel == SurfaceShadingModel::ForwardPureScreenspaceRefractiveGlass) {
					ImGui::DragFloat("Index of refraction", &surfaceMaterial->sfExt_ForwardRefractive_IOR, 0.01f, 1.f, 1.8f);
					ImGui::DragFloat("Dispersion factor", &surfaceMaterial->sfExt_ForwardRefractive_Dispersion, 0.01f, 0.f, 4.f);
					bool exclScreenspace = currentItem->shadingModel == SurfaceShadingModel::ForwardPureScreenspaceRefractiveGlass;
					ImGui::Checkbox("Only refraction", &exclScreenspace);
					currentItem->shadingModel = exclScreenspace ? SurfaceShadingModel::ForwardPureScreenspaceRefractiveGlass : SurfaceShadingModel::ForwardPureRefractiveGlass;
				}
				if (surfaceMaterial->opacity > 0.8f) {
					ImGui::TextColored(ImVec4(1.0, 0.3, 0.5, 1.0), "High opacity values (> 0.8) yield poor blending.\nConsider using lower opacity values,\nor switch to Opaque blend mode");
				}
			}
			bool doubleSided = surfaceMaterial->isDoubleSided();
			ImGui::Checkbox("Double Sided", &doubleSided);
			surfaceMaterial->setDoubleSided(doubleSided);

			if (oldSurfaceMaterialFlags != currentItem->getPipelineClass()) {
				oldMaterialFlags = oldSurfaceMaterialFlags;
				requireMaterialRebuild = true;
			}
		}

		if (ImGui::Button("Save")) {
			AssetManager::createMaterial(currentAsset.getAbsolute(), currentItem);
			auto alltex = currentItem->getAllTextures();
			for (int i = 0; i < currentItem_oldtextures.size(); i++) 
				if (AssetReferenceTracker::hasUseeReference(currentAsset, currentItem_oldtextures[i]))
					AssetReferenceTracker::rmvReference(currentAsset, currentItem_oldtextures[i]);			
			for (int i = 0; i < alltex.size(); i++) 
				AssetReferenceTracker::addReference(currentAsset, alltex[i]);
			
			currentItem_oldtextures = currentItem->getAllTextures();
		}
		ImGui::End();

		currentItem->updateMaterial();
		Actor materialActor = preview_builder->getActorFromTag("Material Ball Preview Actor");
	}
	void MaterialBuilderPanel::rebuild(ResourceSystem* resourceSystem) {
		resourceSystem->rebuildMaterial(currentItem);
		Actor materialActor = preview_builder->getActorFromTag("Material Ball Preview Actor");
		materialActor.getComponent<Components::Mesh3DComponent>().setMaterials(std::vector<AssetID>{ currentAsset });
	}
	void MaterialBuilderPanel::refresh(ResourceSystem* resourceSystem) {
		rebuild(resourceSystem);
	}
	void MaterialBuilderPanel::destroy() {
		this->currentItem = nullptr;
	}
	void MaterialBuilderPanel::setMaterialAsset(ResourceSystem* resourceSystem, const AssetID& asset) {
		currentAsset = asset;
		resourceSystem->loadMaterial(currentAsset);
		currentItem = (SurfaceMaterial*)resourceSystem->retrieveMaterial(currentAsset);
		currentItem_oldtextures = currentItem->getAllTextures();
		rebuild(resourceSystem);
	}
}