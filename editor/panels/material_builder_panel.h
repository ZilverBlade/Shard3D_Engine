#pragma once
#include <Shard3D/core/asset/material.h>
namespace Shard3D {
	class ResourceSystem;
	class MaterialBuilderPanel {
	public:
		MaterialBuilderPanel() = default;
		MaterialBuilderPanel(ResourceSystem* resourceSystem, sPtr<Level>& level,AssetID materialAsset);
		~MaterialBuilderPanel();

		void render(FrameInfo2& frameInfo);
		void rebuild(ResourceSystem* resourceSystem);
		void refresh(ResourceSystem* resourceSystem);
		void destroy();
		void setMaterialAsset(ResourceSystem* resourceSystem, const AssetID& asset);
		bool requireMaterialRebuild = false;
		SurfaceMaterialPipelineClassPermutationFlags oldMaterialFlags = 0;
		SurfaceMaterial* getCurrentMaterial() {
			return currentItem;
		}
		AssetID getCurrentMaterialAsset() {
			return currentAsset;
		}
	private:
		sPtr<Level> preview_builder;
		SurfaceMaterial* currentItem{};
		std::vector<AssetID> currentItem_oldtextures;
		AssetID currentAsset{AssetID::null()};
	};
}