#include "texture_config_panel.h"

#include <imgui.h>
#include <TextEditor.h>
#include <fstream>
#include <Shard3D/core/asset/assetmgr.h>
#include <glm/gtc/type_ptr.hpp>
#include <Shard3D/systems/handlers/render_list.h>
#include <Shard3DEditorKit/kit.h>
namespace Shard3D {
	TextureConfigPanel::TextureConfigPanel(ResourceSystem* resourceSystem, AssetID textureAsset) : currentAsset(textureAsset) {
		resourceSystem->loadTexture(currentAsset);
		currentItem = resourceSystem->retrieveTexture(currentAsset);
		loadInfo = currentItem->getLoadInfo();
		textureVisualiser = ImGui_ImplVulkan_AddTexture(currentItem->getSampler(), currentItem->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
	}
	TextureConfigPanel::~TextureConfigPanel() {
	}

	void TextureConfigPanel::render(FrameInfo2& frameInfo, bool& open) {
		ImGui::Begin("Texture2D Config", &open);
		if (ImGui::BeginCombo("##listoftexture2dscombo", currentAsset.getAsset().c_str())) ImGui::EndCombo();	
		
		ImGui::Image(textureVisualiser, ImVec2(256, 256));

		if (ImGui::BeginDragDropTarget()) {
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SHARD3D.ASSEXP.TEX")) {
				//ImGui_ImplVulkan_RemoveTexture(textureVisualiser);
				*this = TextureConfigPanel(frameInfo.resourceSystem, AssetID(std::string((char*)payload->Data)));
				ImGui::End();
				return;
			}
		}

		auto& texture2d = currentItem;
		ImGui::Text(typeid(*currentItem).name());
		ImGui::Text("Resource Size: %i kB", currentItem->getResourceSize() / 1000);

		ImGui::BeginDisabled();
		ImGui::Combo("Texture Format", reinterpret_cast<int*>(&loadInfo.format), "U8\000U16\000F16\000F32\000EBGR32");
		ImGui::EndDisabled();
		ImGui::Combo("Texture Filter Mode", reinterpret_cast<int*>(&loadInfo.filter), "Nearest\000Linear");
		ImGui::Combo("Texture Address Mode", reinterpret_cast<int*>(&loadInfo.addressMode), "Repeat\000Mirror Repeat\000Edge Clamp\000Border Clamp\000Mirrored Edge Clamp");
		ImGui::Combo("Texture Color Space", reinterpret_cast<int*>(&loadInfo.colorSpace), "Linear\000sRGB");
		ImGui::Combo("Texture Channels", reinterpret_cast<int*>(&loadInfo.channels), "R\000RG\000RGB\000RGBA");

		const char* compressionItems[] = { "Lossless", "BC1", "BC3", "BC4", "BC5", "BC7" };
		static const char* currentCompressionItem = "BC7";
		switch (loadInfo.compression) {
		case (TextureCompression::Lossless):currentCompressionItem = "Lossless"; break;
		case (TextureCompression::BC1):		currentCompressionItem = "BC1"; break;
		case (TextureCompression::BC3):		currentCompressionItem = "BC3"; break;
		case (TextureCompression::BC4):		currentCompressionItem = "BC4"; break;
		case (TextureCompression::BC5):		currentCompressionItem = "BC5"; break;
		case (TextureCompression::BC7):		currentCompressionItem = "BC7"; break;
		}
		if (ImGui::BeginCombo("Texture Compression Mode", currentCompressionItem)) {
			for (int n = 0; n < 6; n++) {
				bool is_selected = (currentCompressionItem == compressionItems[n]); // You can store your selection however you want, outside or inside your objects
				if (ImGui::Selectable(compressionItems[n], is_selected))
					currentCompressionItem = compressionItems[n];
				if (is_selected) {
					ImGui::SetItemDefaultFocus();
				}
			}
			ImGui::EndCombo();
		}

		if (currentCompressionItem == "Lossless") loadInfo.compression = TextureCompression::Lossless;
		if (currentCompressionItem == "BC1") loadInfo.compression = TextureCompression::BC1;
		if (currentCompressionItem == "BC3") loadInfo.compression = TextureCompression::BC3;
		if (currentCompressionItem == "BC4") loadInfo.compression = TextureCompression::BC4;
		if (currentCompressionItem == "BC5") loadInfo.compression = TextureCompression::BC5;
		if (currentCompressionItem == "BC7") loadInfo.compression = TextureCompression::BC7;

		ImGui::Checkbox("Enable Texture Mip Maps", &loadInfo.enableMipMaps);
		ImGui::Checkbox("Normal Map", &loadInfo.isNormalMap);
		ImGui::DragInt2("True Resolution", (int*)&loadInfo.trueResolutionWidth, 2);
		if (ImGui::Button("Save")) {
			AssetManager::modifyTexture2D(currentAsset, loadInfo);
			refresh(frameInfo.resourceSystem);
			currentItem = frameInfo.resourceSystem->retrieveTexture(currentAsset);
			//ImGui_ImplVulkan_RemoveTexture(textureVisualiser);
			textureVisualiser = ImGui_ImplVulkan_AddTexture(currentItem->getSampler(), currentItem->getImageView(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		}
		ImGui::End();
	}
	void TextureConfigPanel::refresh(ResourceSystem* resourceSystem) {
		resourceSystem->loadTexture(currentAsset, true);
	}
	void TextureConfigPanel::destroy() {
		this->currentItem = nullptr;
	}
}