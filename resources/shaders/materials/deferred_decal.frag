#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_control_flow_attributes : require

#define DESCRIPTOR_BINDLESS_SET 1

#include "material.glsl"
#include "compression.glsl"
#include "depth_to_worldpos.glsl"
#include "depth.glsl"

#include "global_ubo.glsl"

struct DecalInstanceData {
	mat4 transform;
	mat4 invTransform;
};
layout (set = 2, binding = 0) uniform sampler2D depth;
layout (set = 2, binding = 1) uniform usampler2D objectData; // todo, 0x20 == no decal project

layout(std430, set = 3, binding = 0) readonly buffer DecalData {
	DecalInstanceData instances[8];
} decalData;

#ifdef DBUFFER
layout (constant_id = 0) const bool enableDiffuse = false;
layout (constant_id = 1) const bool enableSSC = false;
layout (constant_id = 2) const bool enableNormal = false;
layout (constant_id = 3) const uint blendMode = 0x1000;

const uint BLEND_MODE_NORMAL = 0x1000;
const uint BLEND_MODE_MULTIPLICATIVE = 0x2000;
const uint BLEND_MODE_ADDITIVE = 0x4000;
const uint BLEND_MODE_ERASE = 0x8000;

layout (location = 0) out vec4 DBuffer0; // diffuse + opacity
layout (location = 1) out vec4 DBuffer1; // specular, glossiness, reflectivity + opacity
layout (location = 2) out vec4 DBuffer2; // normal + opacity
#endif
#ifdef GBUFFER
layout (constant_id = 0) const uint model = 0x80;
layout (location = 0) out vec4 GBuffer0;
layout (location = 0) out vec2 GBuffer1;
const uint MODEL_MULT_DIFFUSE = 0x80;
const uint MODEL_MULT_SPECULAR_GLOSSINESS_REFLECTIVITY = 0x100;
const uint MODEL_MULT_DIFFUSE_SPECULAR_GLOSSINESS_REFLECTIVITY = 0x200;
#endif
#ifdef GBUFFER_ADD
layout (location = 0) out vec3 Emission; // emission 
#endif


layout (location = 0) in flat uint instance;
layout (location = 1) in flat vec3 extent;

layout (push_constant) uniform Push {
	uint material;
} push;

void main() {
	vec2 uv = gl_FragCoord.xy / (ubo.screenSize - 1);
	float depth = textureLod(depth, uv, 0.0).x;
	vec3 worldPos = WorldPosFromDepth(uv, depth, ubo.invProjection, ubo.invView);
	
	mat4 it = decalData.instances[instance].invTransform;
	float decOpacity = it[0][3];
	it[0][3] = 0.0;
	vec4 localPos = it * vec4(worldPos, 1.0);
	
	if (all(lessThan(abs(localPos.xyz), extent))) {
		if ((textureLod(objectData, uv, 0.0).r & 0x20) == 0) {
			vec2 decUv = (localPos.xz / extent.xz + 1..xx) * 0.5.xx;
			MaterialData mat = getMaterialData(push.material);
			vec2 txcOff = mat.factorData.texCoordOffset;
			vec2 txcMul = mat.factorData.texCoordMultiplier;
			
			vec2 coord = decUv * txcMul + txcOff;
			
#ifdef DBUFFER
			bool hasOpacityTex = matHasOpacityMap(mat);
			float opacity = getMaterialOpacity(mat, hasOpacityTex, coord) * decOpacity;
			[[flatten]]
			if (blendMode == BLEND_MODE_ERASE) {
				[[flatten]]
				if (enableDiffuse) {
					DBuffer0 = vec4(0..xxx, opacity);
				}
				[[flatten]]
				if (enableSSC) {
					DBuffer1 = vec4(0..xxx, opacity);
				}
				[[flatten]]
				if (enableNormal) {
					DBuffer2 = vec4(0..xxx, opacity);
				}
				return;
			} else {
				if (opacity == 0.0) discard;
			}
			[[flatten]]
			if (enableDiffuse) {
				bool hasDiffuseTex = matHasDiffuseTexture(mat);
				vec3 diffuse = vec3(0.0);
				
				diffuse = getMaterialDiffuse(mat, hasDiffuseTex, coord);
				
				vec4 srcDBuffer0 = vec4(diffuse , opacity);
				[[flatten]]
				if (blendMode == BLEND_MODE_MULTIPLICATIVE) {
					DBuffer0 = vec4(srcDBuffer0.rgb * srcDBuffer0.a, srcDBuffer0.a);
				} else {
					DBuffer0 = srcDBuffer0;
				}
			}
			[[flatten]]
			if (enableSSC) {
				bool hasSpecularTex = matHasSpecularTexture(mat);
				bool hasGlossinessTex = matHasGlossinessTexture(mat);
				bool hasReflectivityTex = matHasReflectivityTexture(mat);
				
				float specular = getMaterialSpecular(mat, hasSpecularTex, coord);
				float glossiness = getMaterialGlossiness(mat, hasGlossinessTex, coord);
				float reflectivity = getMaterialReflectivity(mat, hasReflectivityTex, coord);
				
				vec4 srcDBuffer1 = vec4(specular, glossiness, 1.0 - reflectivity, opacity);
				[[flatten]]
				if (blendMode == BLEND_MODE_MULTIPLICATIVE) {
					DBuffer1 = vec4(srcDBuffer1.rgb * srcDBuffer1.a, srcDBuffer1.a);
				} else {
					DBuffer1 = srcDBuffer1;
				}
			}
			[[flatten]]
			if (enableNormal) {
				bool hasNormalMap = matHasNormalMap(mat); 
				vec3 normal = vec3(0.0, 0.0, 1.0);
				if (hasNormalMap) {
					normal = texture(texID[mat.textureData.normalTex_id], coord).xyz;
				}
				DBuffer2 = vec4(normal, opacity);
			}
#endif
#ifdef GBUFFER_MUL
			[[flatten]]
			if (MODEL_MULT_DIFFUSE || MODEL_MULT_SPECULAR_GLOSSINESS_REFLECTIVITY || 
				MODEL_MULT_DIFFUSE_SPECULAR_GLOSSINESS_REFLECTIVITY) {
				vec3 diffuse = vec3(0.0);
				float reflectivity = 0.0;
				float specular = 0.0; 
				float glossiness = 0.0; 
				[[flatten]]
				if (MODEL_MULT_DIFFUSE || MODEL_MULT_DIFFUSE_SPECULAR_GLOSSINESS_REFLECTIVITY) {
					bool hasDiffuseTex = matHasDiffuseTexture(mat);
					diffuse = getMaterialDiffuse(mat, hasDiffuseTex, coord);
				}
				[[flatten]]
				if (MODEL_MULT_DIFFUSE_SPECULAR_GLOSSINESS_REFLECTIVITY || MODEL_MULT_DIFFUSE_SPECULAR_GLOSSINESS_REFLECTIVITY) {
					bool hasReflectivityTex = matHasReflectivityTexture(mat);
					bool hasSpecularTex = matHasSpecularTexture(mat);
					bool hasGlossinessTex = matHasGlossinessTexture(mat);
					reflectivity = getMaterialReflectivity(mat, hasReflectivityTex, coord);
					specular = getMaterialSpecular(mat, hasSpecularTex, coord);
					glossiness = getMaterialGlossiness(mat, hasGlossinessTex, coord);
				}
				GBuffer0 = vec4(diffuse, 1.0 - reflectivity)
				GBuffer1 = vec4(specular, glossiness);
			}
#endif
#ifdef GBUFFER_ADD
			hasEmissiveTex = matHasEmissiveTexture(sfMaterial);
			Emission = getMaterialEmissive(sfMaterial, hasEmissiveTex, coord);
#endif
		} else {
			discard;
		}
	} else {
		discard;
	}
}