#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_KHR_vulkan_glsl : enable

#define DESCRIPTOR_BINDLESS_SET 1
#define MATERIAL_ID_NO_PUSH
#define SURFACE_UBER_NO_FRAGMENT_INPUTS
#define SURFACE_UBER_NO_TEXTURE_FUNCTIONS

#include "material.glsl"

// this shader should only get used if alpha test is enabled
layout(location = 0) in vec2 fragUV;
layout(location = 4) in flat uint materialIndex;
void main() {
    MaterialData sfMaterial = getMaterialData(materialIndex);
	
	if (matHasAlphaMask(sfMaterial)) {
		vec2 txcOff = sfMaterial.factorData.texCoordOffset;
		vec2 txcMul = sfMaterial.factorData.texCoordMultiplier;
		if (texture(texID[sfMaterial.textureData.maskTex_id], fragUV * txcMul + txcOff).x < 0.5)
			discard;
	}
}