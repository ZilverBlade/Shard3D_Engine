#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_KHR_vulkan_glsl : enable

#pragma optionNV(fastmath on)
#pragma optionNV(fastprecision on)

#define DESCRIPTOR_BINDLESS_SET 2
#define SURFACE_DEFERRED

#define NORMAL_COMPRESSION_INCLUDE_FRONT_FACE_DATA
#include "compression.glsl"
#include "material.glsl"

layout(location = 0) in vec3 fragPosWorld;
layout(location = 1) in vec2 fragUV;
layout(location = 2) in vec3 fragNormalWorld;
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableVertexTangents
layout(location = 3) in vec3 fragTangentWorld;
#endif
layout(location = 4) in flat uint materialIndex;
layout(location = 5) in flat uint receiveDecals;
#ifdef ACTOR_DYNAMIC
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableMotionBlur
#include "global_ubo.glsl"
layout(location = 6) in vec4 projPrevFrame;
#endif
#endif

layout (location = 0) out vec4 GBuffer0; // diffuse + reflectivity R8G8B8A8 unorm
layout (location = 1) out vec2 GBuffer1; // material R8G8 unorm
layout (location = 2) out vec3 GBuffer2; // emission B10G11R11 ufloat
layout (location = 3) out uint GBuffer3; // normal pack uint32 
layout (location = 4) out uint GBuffer4; // shading model, obj data uint44
/*
obj data flags:

0x10 == Dynamic Velocity Buffer
0x20 == No Decal Proj
0x40 == ??
0x80 == ??

*/

#ifdef ACTOR_DYNAMIC
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableMotionBlur
layout (location = 5) out vec2 VelocityBuffer;
#endif
#endif

// random tile orientation

float random1(vec2 coords) {
   return fract(sin(dot(coords.xy, vec2(49.9898,148.233))) * 43758.5453) * 2.0 - 1.0;
}
float random2(vec2 coords) {
   return fract(sin(dot(coords.xy, vec2(-2121.2132,22.56167))) * 122.3623) * 2.0 - 1.0;
}

vec2 getRandomOrientation(vec2 uv, vec2 tile){
	float x = sign(random1(tile));
	float y = sign(random2(tile));
	x = x == 0.0 ? 1.0 : x;
	y = y == 0.0 ? 1.0 : y;
	return vec2(x, y) ;
}

vec3 previewTileColor(vec2 tile) {
	return abs(vec3(random1(tile), random2(tile), random2(tile + 8591.23)));
}

void main() {
    MaterialData sfMaterial = getMaterialData(materialIndex);
	
	vec2 txcOff = sfMaterial.factorData.texCoordOffset;
	vec2 txcMul = sfMaterial.factorData.texCoordMultiplier;
	vec2 coord = getTexCoordValue(fragUV, txcOff, txcMul);
	
	FragmentInput fragInput;
	fragInput.position = fragPosWorld;
	fragInput.uv = fragUV;
	fragInput.normal = fragNormalWorld;
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableVertexTangents
	fragInput.tangent = fragTangentWorld;
#endif
	
// if discard done in the early Z pass, we will simply use EQUAL depth test to avoid using discard twice
#ifndef S3DSDEF_SHADER_PERMUTATION_EarlyZDiscard
#ifdef ENABLE_ALPHA_TEST
	if (getSurfaceMaterialAlphaMaskCutout(sfMaterial, coord)) discard;
#endif
#endif
	vec3 diffuse = vec3(0.0); 
	float specular = 0.0;
	float glossiness = 0.0; 
	float reflectivity = 0.0;
	vec4 emissive = vec4(0.0);
	vec3 normal = vec3(0.0);

	bool hasDiffuseTex = false;
	bool hasSpecularTex = false;
	bool hasGlossinessTex = false;
	bool hasReflectivityTex = false;
	bool hasEmissiveTex = false;
	bool hasNormalMap = false;
	
#ifndef S3DSDEF_DISABLE_MATERIAL_TEXTURES
	hasDiffuseTex = matHasDiffuseTexture(sfMaterial);
	hasSpecularTex = matHasSpecularTexture(sfMaterial);
	hasGlossinessTex = matHasGlossinessTexture(sfMaterial);
	hasReflectivityTex = matHasReflectivityTexture(sfMaterial);
	hasEmissiveTex = matHasEmissiveTexture(sfMaterial);
	hasNormalMap = matHasNormalMap(sfMaterial);
#endif

	emissive = getMaterialEmissive(sfMaterial, hasEmissiveTex, coord);	
	diffuse = getMaterialDiffuse(sfMaterial, hasDiffuseTex, coord);
	specular = getMaterialSpecular(sfMaterial, hasSpecularTex, coord);
	glossiness = getMaterialGlossiness(sfMaterial, hasGlossinessTex, coord);
	reflectivity = getMaterialReflectivity(sfMaterial, hasReflectivityTex, coord);
	normal = getMaterialNormal(fragInput, sfMaterial, hasNormalMap, coord);
	
	uint objectData = receiveDecals == 0 ? 0x20 : 0x00;
#ifdef ACTOR_DYNAMIC
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableMotionBlur
	objectData |= 0x10;
#endif
#endif

	GBuffer0 = vec4(diffuse.r, diffuse.g, diffuse.b, 1.0 - reflectivity);
	GBuffer1 = vec2(specular, glossiness);
	GBuffer2 = emissive.rgb * emissive.a;
	GBuffer3 = cram_32bnormal(normal);
	GBuffer4 = sfMaterial.surfaceShadingModel | objectData;
	
#ifdef ACTOR_DYNAMIC
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableMotionBlur
	const vec2 cc = (vec2(gl_FragCoord.xy) / (ubo.screenSize - vec2(1.0))) * 2.0 - 1.0;
	const vec2 oldCC = projPrevFrame.xy / projPrevFrame.w;
	const vec2 v = 0.5 * (cc - oldCC);
	VelocityBuffer = v * vec2(ubo.velocityBufferTileExtent);
#endif
#endif
}