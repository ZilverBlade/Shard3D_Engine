#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_KHR_vulkan_glsl : enable

#pragma optionNV(fastmath on)
#pragma optionNV(fastprecision on)

#define DESCRIPTOR_BINDLESS_SET 2
#define SURFACE_FORWARD
#include "surface_lighting.glsl"
#include "material.glsl"
#include "weighted_blending.glsl"

layout(location = 0) in vec3 fragPosWorld;
layout(location = 1) in vec2 fragUV;
layout(location = 2) in vec3 fragNormalWorld;
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableVertexTangents
layout(location = 3) in vec3 fragTangentWorld;
#endif
layout(location = 4) in flat uint materialIndex;

layout (set = 3, binding = 0) uniform sampler2D deferredLitTexture;

// first render target which is used to accumulate pre-multiplied color values
layout (location = 0) out vec4 outAccum;
// second render target which is used to store pixel revealage
layout (location = 1) out float outReveal;

// shader code
void main(){
    MaterialData sfMaterial = getMaterialData(materialIndex);
	
	vec2 txcOff = sfMaterial.factorData.texCoordOffset;
	vec2 txcMul = sfMaterial.factorData.texCoordMultiplier;
	vec2 coord = getTexCoordValue(fragUV, txcOff, txcMul);
	
	FragmentInput fragInput;
	fragInput.position = fragPosWorld;
	fragInput.uv = fragUV;
	fragInput.normal = fragNormalWorld;
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableVertexTangents
	fragInput.tangent = fragTangentWorld;
#endif

#ifdef ENABLE_ALPHA_TEST
	if (getMaterialAlphaMaskCutout(sfMaterial, coord)) discard;
#endif

	vec3 diffuse = vec3(0.0); 
	float specular = 0.0;
	float glossiness = 0.0; 
	float reflectivity = 0.0;
	vec4 emissive = vec4(0.0);
	vec3 normal = vec3(0.0);

	bool hasDiffuseTex = false;
	bool hasSpecularTex = false;
	bool hasGlossinessTex = false;
	bool hasReflectivityTex = false;
	bool hasEmissiveTex = false;
	bool hasNormalMap = false;
	bool hasOpacityMap = false;
	
#ifndef S3DSDEF_DISABLE_MATERIAL_TEXTURES
	hasDiffuseTex = matHasDiffuseTexture(sfMaterial);
	hasSpecularTex = matHasSpecularTexture(sfMaterial);
	hasGlossinessTex = matHasGlossinessTexture(sfMaterial);
	hasReflectivityTex = matHasReflectivityTexture(sfMaterial);
	hasEmissiveTex = matHasEmissiveTexture(sfMaterial);
	hasNormalMap = matHasNormalMap(sfMaterial);
	hasOpacityMap = matHasOpacityMap(sfMaterial);
#endif
	
	vec3 lightOutput = vec3(0.0);
	
	const float opacity = getMaterialOpacity(sfMaterial, hasOpacityMap, coord);
	float alphaBlend = opacity;
	
	if (sfMaterial.surfaceShadingModel == 0 || sfMaterial.surfaceShadingModel == 2) {
		emissive = getMaterialEmissive(sfMaterial, hasEmissiveTex, coord);
		lightOutput = emissive.rgb * emissive.a;
	}
	ShadedPixelInfo pixel;	
	pixel.positionWorld = fragPosWorld;
	if (sfMaterial.surfaceShadingModel == 1 ||
		sfMaterial.surfaceShadingModel == 2 ||
		sfMaterial.surfaceShadingModel == 5 ||
		sfMaterial.surfaceShadingModel == 6
	) {
		vec3 Q1 = dFdx(pixel.positionWorld);
		vec3 Q2 = dFdy(pixel.positionWorld);
		pixel.dnormal = normalize(cross(Q1, Q2));
		pixel.normal = getMaterialNormal(fragInput, sfMaterial, hasNormalMap, coord) * float(gl_FrontFacing);
		pixel.diffuse.rgb = getMaterialDiffuse(sfMaterial, hasDiffuseTex, coord);
		pixel.diffuse.w = 1.0 - getMaterialReflectivity(sfMaterial, hasReflectivityTex, coord);
		if (sfMaterial.surfaceShadingModel == 1 ||
			sfMaterial.surfaceShadingModel == 2) {
				pixel.specular = getMaterialSpecular(sfMaterial, hasSpecularTex, coord);
				pixel.glossiness = getMaterialGlossiness(sfMaterial, hasGlossinessTex, coord);
				
				lightOutput += calculateLightShaded(pixel, 1.0);
		} else if (sfMaterial.surfaceShadingModel == 5) {
			lightOutput = mix(vec3(1.0), pixel.diffuse.rgb, pixel.diffuse.w) * 
				calculatePureReflectionRefraction(pixel, 
				sfMaterial.factorData.sfExt_ForwardRefractive_IOR, 
				opacity,
				sfMaterial.factorData.sfExt_ForwardRefractive_Dispersion,
				deferredLitTexture
			);
			alphaBlend = 1.0;
		} else if (sfMaterial.surfaceShadingModel == 6) {
			lightOutput = mix(vec3(1.0), pixel.diffuse.rgb, pixel.diffuse.w) * 
				calculatePureRefraction(pixel,
				sfMaterial.factorData.sfExt_ForwardRefractive_IOR, 
				sfMaterial.factorData.sfExt_ForwardRefractive_Dispersion,
				deferredLitTexture
			);
		}
	}
	
    outAccum = getAlphaAccum(lightOutput, alphaBlend) * sfMaterial.factorData.forwardBlendingMulBias;
	outReveal = alphaBlend;
}