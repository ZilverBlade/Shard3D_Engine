#version 450

// post processing
const vec2 CC[6] = vec2[](
	vec2(1.0, -1.0),
	vec2(1.0, 1.0),
	vec2(-1.0, -1.0),
	vec2(-1.0, -1.0),
	vec2(1.0, 1.0),
	vec2(-1.0, 1.0)
	);
layout(location = 0) out vec2 fragUV;

layout (push_constant) uniform Push {
	vec2 screenSize;
	vec2 maxTiles;
	int tileIndex;
} push;

void main() {
	vec2 baseCC = CC[gl_VertexIndex];
	
	vec2 currTile = vec2(push.tileIndex % int(push.maxTiles.x), floor(float(push.tileIndex) / push.maxTiles.x));
	
	vec2 tileSize = 1..xx / push.maxTiles;
	vec2 tileOffset = currTile / push.maxTiles;
	
	vec2 tiledCC = baseCC * tileSize - (1..xx - tileSize) + tileOffset * 2.0;
	fragUV = tiledCC * 0.5.xx + 0.5.xx;
	gl_Position = vec4(tiledCC, 0.0, 1.0);
}