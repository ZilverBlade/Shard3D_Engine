#version 450
#extension GL_EXT_shader_image_load_formatted : enable

layout (set = 0, binding = 0, rgba16f) uniform image3D LPV_Inout_RedSH;
layout (set = 0, binding = 1, rgba16f) uniform image3D LPV_Inout_GreenSH;
layout (set = 0, binding = 2, rgba16f) uniform image3D LPV_Inout_BlueSH;
layout (set = 0, binding = 3, rgba16f) uniform image3D LPV_Inout2_RedSH;
layout (set = 0, binding = 4, rgba16f) uniform image3D LPV_Inout2_GreenSH;
layout (set = 0, binding = 5, rgba16f) uniform image3D LPV_Inout2_BlueSH;
layout (set = 0, binding = 6, rgba16f) uniform image3D outPropagatedLPV[9]; // 3 cascades MAX

layout (push_constant) uniform Push {
	ivec4 blendOffset;
	int cascade;
	int finalPingPongOutput;
	float temporalBlend;
} push;

void main() {	
	ivec3 cellIndex = ivec3(gl_FragCoord.xyz);
	
	for (int z = 0; z < imageSize(LPV_Inout_RedSH).z; z++) {
		cellIndex.z = z;
		vec4 rCoeffs;
		vec4 gCoeffs;
		vec4 bCoeffs;
		if (push.finalPingPongOutput == 0) {
			rCoeffs = imageLoad(LPV_Inout_RedSH, cellIndex).xyzw;
		} else {
			rCoeffs = imageLoad(LPV_Inout2_RedSH, cellIndex).xyzw;
		}
		if (push.finalPingPongOutput == 0) {
			gCoeffs = imageLoad(LPV_Inout_GreenSH, cellIndex).xyzw;
		} else {
			gCoeffs = imageLoad(LPV_Inout2_GreenSH, cellIndex).xyzw;
		}
		if (push.finalPingPongOutput == 0) {
			bCoeffs = imageLoad(LPV_Inout_BlueSH, cellIndex).xyzw;
		} else {
			bCoeffs = imageLoad(LPV_Inout2_BlueSH, cellIndex).xyzw;
		} 
		int t = int(dot(push.blendOffset.xyz, push.blendOffset.xyz));
		vec4 old_rCoeffs = imageLoad(outPropagatedLPV[push.cascade * 3 + 0], cellIndex.xyz - push.blendOffset.xyz);
		vec4 old_gCoeffs = imageLoad(outPropagatedLPV[push.cascade * 3 + 1], cellIndex.xyz - push.blendOffset.xyz);
		vec4 old_bCoeffs = imageLoad(outPropagatedLPV[push.cascade * 3 + 2], cellIndex.xyz - push.blendOffset.xyz);
		
		float blend = t == 0 ? push.temporalBlend : 1.0;
		imageStore(outPropagatedLPV[push.cascade * 3 + 0], cellIndex.xyz, mix(old_rCoeffs, rCoeffs, blend));
		imageStore(outPropagatedLPV[push.cascade * 3 + 1], cellIndex.xyz, mix(old_gCoeffs, gCoeffs, blend));
		imageStore(outPropagatedLPV[push.cascade * 3 + 2], cellIndex.xyz, mix(old_bCoeffs, bCoeffs, blend));
	}
}
