#version 450
#extension GL_GOOGLE_include_directive : enable

layout (set = 0, binding = 0, rgba16f) uniform image3D LPV_Inout_RedSH;
layout (set = 0, binding = 1, rgba16f) uniform image3D LPV_Inout_GreenSH;
layout (set = 0, binding = 2, rgba16f) uniform image3D LPV_Inout_BlueSH;
layout (set = 0, binding = 3, rgba16f) uniform image3D LPV_Inout2_RedSH;
layout (set = 0, binding = 4, rgba16f) uniform image3D LPV_Inout2_GreenSH;
layout (set = 0, binding = 5, rgba16f) uniform image3D LPV_Inout2_BlueSH;
layout (set = 1, binding = 0, r16f) uniform image3D SH_Sum;

void main() {
	ivec3 coord = ivec3(gl_FragCoord.xyz);
	for (int z = 0; z < imageSize(LPV_Inout_RedSH).z; z++) {
		coord.z = z;
		imageStore(SH_Sum, coord, vec4(0.0));
		imageStore(LPV_Inout_RedSH, coord, vec4(0.0));
		imageStore(LPV_Inout_GreenSH, coord, vec4(0.0));
		imageStore(LPV_Inout_BlueSH, coord, vec4(0.0));
		imageStore(LPV_Inout2_RedSH, coord, vec4(0.0));
		imageStore(LPV_Inout2_GreenSH, coord, vec4(0.0));
		imageStore(LPV_Inout2_BlueSH, coord, vec4(0.0));
	}
}
