#version 450
#define LPV_RESOLUTION 32

layout (location = 0) out flat int layer;
layout (location = 1) out flat vec4 sh;
layout (location = 2) out flat vec3 flux;

layout (push_constant) uniform Push {
	vec3 lpvExtent;
	vec3 lpvCenter;
	int resolution;
} push;
struct VirtualPointLight {
	vec4 position;
	vec4 color;
};

layout (set = 0, binding = 0) uniform VirtualPointLights {
	VirtualPointLight virtualPointLights[2048];
} vplData;

const float SH_C0 = 0.282094792; // 1 / 2sqrt(pi)
const float SH_C1 = 0.488602512; // sqrt(3/pi) / 2

/*Cosine lobe coeff*/
const float SH_cosLobe_C0 = 0.886226925; // sqrt(pi)/2
const float SH_cosLobe_C1 = 1.023326707; // sqrt(pi/3)
const float PI = 3.1415926;

const float SURFEL_WEIGHT = 1.0;

vec4 getCosineLobe() {
	return vec4(SH_cosLobe_C0, 0.0, 0.0, 0.0);
}
vec4 getSH() {
	return vec4(SH_C0, -SH_C1, SH_C1, -SH_C1);
}

vec3 uvToWorld(vec3 uv) {
	return (uv * 2.0 - 1.0) * push.lpvExtent + push.lpvCenter;
}
vec3 worldToUV(vec3 world) {
	return ( (world - push.lpvCenter) / push.lpvExtent + 1.0 ) / 2.0;
}

void main() {
	VirtualPointLight vpl = vplData.virtualPointLights[gl_VertexIndex];
	vec3 pxCoords = floor(worldToUV(vpl.position.xyz) * (LPV_RESOLUTION - 1));

	gl_Position = vec4(pxCoords.xyz / vec3(LPV_RESOLUTION - 1), 1.0);
	gl_Position.xy = gl_Position.xy * 2..xx - 1..xx;
	gl_Position.y = -gl_Position.y;
	layer = int(pxCoords.z);
	if (layer < 0 || layer >= LPV_RESOLUTION) {
		gl_ClipDistance[0] = -1;
		gl_Position = vec4(0..xxx, 1.0);
		sh = 0..xxxx;
		flux = 0..xxx;
		return;
	}

	vec4 coef = (getCosineLobe() / PI) * SURFEL_WEIGHT;
	
	sh = coef;
	flux = vpl.color.rgb * vpl.color.w;
}