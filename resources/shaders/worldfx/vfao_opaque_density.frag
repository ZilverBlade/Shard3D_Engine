#version 450

layout (location = 0) out float density; // R32 float

void main(){
	float x = gl_FragCoord.z;
	float xsq = x*x / sqrt(x);
	density = xsq;
}