#version 450

layout (location = 0) in flat vec4 sh;
layout (location = 1) in flat vec3 flux;

layout (location = 0) out vec4 sh_red;
layout (location = 1) out vec4 sh_green;
layout (location = 2) out vec4 sh_blue;
layout (location = 3) out float sh_sum;

void main() {
	sh_red = sh * flux.r;
	sh_green = sh * flux.g;
	sh_blue = sh * flux.b;
	sh_sum = 1.0;
}