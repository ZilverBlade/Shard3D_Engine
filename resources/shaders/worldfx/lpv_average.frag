#version 450

layout (set = 0, binding = 0, rgba16f) uniform image3D LPV_Inout_RedSH;
layout (set = 0, binding = 1, rgba16f) uniform image3D LPV_Inout_GreenSH;
layout (set = 0, binding = 2, rgba16f) uniform image3D LPV_Inout_BlueSH;
layout (set = 1, binding = 0, r16f) uniform image3D SH_Sum;

void main() {
	ivec3 cellIndex = ivec3(gl_FragCoord.xyz);
	
	for (int z = 0; z < imageSize(LPV_Inout_RedSH).z; z++) {
		cellIndex.z = z;
		float weight = 1.0 / max(imageLoad(SH_Sum, cellIndex).r, 0.00001);
		imageStore(LPV_Inout_RedSH, cellIndex, imageLoad(LPV_Inout_RedSH, cellIndex).xyzw * weight);
		imageStore(LPV_Inout_GreenSH, cellIndex, imageLoad(LPV_Inout_GreenSH, cellIndex).xyzw * weight);
		imageStore(LPV_Inout_BlueSH, cellIndex, imageLoad(LPV_Inout_BlueSH, cellIndex).xyzw * weight);
	}
}