#version 450
#define LPV_RESOLUTION 32
layout (push_constant) uniform Push {
	vec3 lpvExtent;
	vec3 lpvCenter;
	int resolution;
} push;
layout (set = 1, binding = 0) uniform LPVInjectData {
	mat4 rsmVP;
	mat4 rsmInvView;
	mat4 rsmInvProj;
} injection;
layout (set = 1, binding = 1) uniform sampler2D rsmDepth;
layout (set = 1, binding = 2) uniform sampler2D rsmFlux;
layout (set = 1, binding = 3) uniform sampler2D rsmNormal;

layout (location = 0) out flat int layer;
layout (location = 1) out flat vec4 sh;
layout (location = 2) out flat vec3 flux;

const float SH_C0 = 0.282094792; // 1 / 2sqrt(pi)
const float SH_C1 = 0.488602512; // sqrt(3/pi) / 2

/*Cosine lobe coeff*/
const float SH_cosLobe_C0 = 0.886226925; // sqrt(pi)/2
const float SH_cosLobe_C1 = 1.023326707; // sqrt(pi/3)
const float PI = 3.1415926;

const float POS_BIAS_NORMAL = 1.0;
const float POS_BIAS_LIGHT = 1.0;

const float SURFEL_WEIGHT = 1.0;

const int KERNEL_SIZE = 2;
const int STEP_SIZE = 1;

struct RSMData {
	vec4 normal;
	vec3 position;
	vec3 flux;
};

float Luminance(RSMData data) {
	return data.flux.r * 0.299 + data.flux.g * 0.587 + data.flux.b * 0.114;
}

RSMData fetchRSMData(ivec2 coords) {
	int maxLod = textureQueryLevels(rsmFlux) - 1;
	vec2 RSMsize = textureSize(rsmFlux, maxLod).xy;
	vec2 uv = vec2(coords) / vec2(RSMsize - 1);
	RSMData data;
	data.flux = vec3(0.0);
	data.normal = textureLod(rsmNormal, uv, float(maxLod)).xyzw;
	data.normal.xyz = data.normal.xyz * 2.0 - 1.0;
	float depth = textureLod(rsmDepth, uv, 0.0).x;
	vec4 viewSpace = injection.rsmInvProj * vec4(uv * 2.0 - 1.0, depth, 1.0);
	viewSpace.xyz /= viewSpace.w;
	vec4 world = injection.rsmInvView * viewSpace;
	data.position = world.xyz + (data.normal.xyz * POS_BIAS_NORMAL);
	float l = length(data.normal.xyz);
	if (l > 0.0) {
		data.normal /= l;
	} else {
		return data;
	}
	data.flux = textureLod(rsmFlux, uv, float(maxLod)).rgb;
	
	
	return data;
}

vec4 dirToCosineLobe(vec3 dir) {
	return vec4(SH_cosLobe_C0, -SH_cosLobe_C1 * dir.y, SH_cosLobe_C1 * dir.z, -SH_cosLobe_C1 * dir.x);
}

vec4 dirToSH(vec3 dir) {
	return vec4(SH_C0, -SH_C1 * dir.y, SH_C1 * dir.z, -SH_C1 * dir.x);
}

vec3 uvToWorld(vec3 uv) {
	return (uv * 2.0 - 1.0) * push.lpvExtent + push.lpvCenter;
}
vec3 worldToUV(vec3 world) {
	return ( (world - push.lpvCenter) / push.lpvExtent + 1.0 ) / 2.0;
}

void main() {
	ivec2 rsmCoords = ivec2(gl_VertexIndex % push.resolution, int(floor(float(gl_VertexIndex) / float(push.resolution))));
	
	// Pick brightest cell in KERNEL_SIZExKERNEL_SIZE grid
	vec3 brightestCellIndex;
	float maxLuminance = -0.01;
	
	for (uint y = 0; y < KERNEL_SIZE; y += STEP_SIZE) {
		for (uint x = 0; x < KERNEL_SIZE; x += STEP_SIZE) {
			ivec2 txi = rsmCoords.xy + ivec2(x, y);
			RSMData data = fetchRSMData(txi);
			if (data.normal.w == 0.0) continue;
			float texLum = Luminance(data);
			if (texLum > maxLuminance) {
				brightestCellIndex = worldToUV(data.position) * vec3(LPV_RESOLUTION - 1);
				maxLuminance = texLum;
			}
		}
	}
	
	RSMData result;
	result.position = vec3(0.0);
	result.normal = vec4(0.0);
	result.flux = vec3(0.0);
	float numSamples = 0;
	for (uint y = 0; y < KERNEL_SIZE; y += STEP_SIZE) {
		for (uint x = 0; x < KERNEL_SIZE; x += STEP_SIZE) {
			ivec2 txi = rsmCoords.xy + ivec2(x, y);
			RSMData data = fetchRSMData(txi);
			vec3 texelIndex = worldToUV(data.position) * vec3(LPV_RESOLUTION - 1);
			vec3 deltaGrid = texelIndex - brightestCellIndex;
			if (dot(deltaGrid, deltaGrid) < 10) { // If cell proximity is good enough 
				// Sample from texel
				result.flux += data.flux;
				result.position += data.position;
				result.normal += data.normal;
				numSamples+=1.0;
			}
		}
	}
	
	if (numSamples > 0)  {
		result.position /= numSamples;
		result.normal /= numSamples;
		float l2 = dot(result.normal.xyz, result.normal.xyz);
		if (l2 > 0.0) {
			result.normal /= sqrt(l2);
		} else {
			gl_ClipDistance[0] = -1;
			gl_Position = vec4(0..xxx, 1.0);
			sh = 0..xxxx;
			flux = 0..xxx;
			return;
		}
		result.flux /= numSamples;
	} else{
		gl_ClipDistance[0] = -1;
		gl_Position = vec4(0..xxx, 1.0);
		sh = 0..xxxx;
		flux = 0..xxx;
		return;
	}
	
	vec3 pxCoords = floor(worldToUV(result.position) * (LPV_RESOLUTION - 1));
	
	gl_Position = vec4(pxCoords.xyz / vec3(LPV_RESOLUTION - 1), 1.0);
	gl_Position.xy = gl_Position.xy * 2..xx - 1..xx;
	gl_Position.y = -gl_Position.y;
	layer = int(pxCoords.z);
	if (layer < 0 || layer >= LPV_RESOLUTION) {
		gl_ClipDistance[0] = -1;
		gl_Position = vec4(0..xxx, 1.0);
		sh = 0..xxxx;
		flux = 0..xxx;
		return;
	}
	gl_ClipDistance[0] = 1.0;
	
	vec4 coef = (dirToCosineLobe(result.normal.xyz) / PI) * SURFEL_WEIGHT;
	
	sh = coef;
	flux = result.flux.rgb;
}