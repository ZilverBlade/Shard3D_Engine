#version 450
layout (points) in;
layout (points, max_vertices = 1) out; 

layout (location = 0) in flat int layer[];
layout (location = 1) in flat vec4 sh[];
layout (location = 2) in flat vec3 flux[];
layout (location = 0) out flat vec4 out_sh;
layout (location = 1) out flat vec3 out_flux;

void main() {
	for (int i = 0; i < gl_in.length(); i++) {
        gl_Position = gl_in[i].gl_Position;
        gl_ClipDistance[0] = gl_in[i].gl_ClipDistance[0];
        gl_Layer = layer[i];
        out_sh = sh[i];
        out_flux = flux[i];
        EmitVertex();
    }

    EndPrimitive();
}