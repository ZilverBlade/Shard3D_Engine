#version 450

#extension GL_KHR_vulkan_glsl : enable

layout (location = 0) in vec2 fragUV;
layout (location = 0) out vec4 outColor; // (Depth, DepthSQ, InvDensity, Occlusion)

layout (set = 0, binding = 0) uniform sampler2D depthImage;
layout (set = 0, binding = 1) uniform sampler2D densityImage;

layout (push_constant) uniform myParams {
    vec2 screenSize;
} push;

float fetchDepthTexel(vec2 offset) {
    return texture(depthImage, fragUV + offset).r;
}
vec2 fetchVTexel(vec2 offset) {
    float d = fetchDepthTexel(offset);
    return vec2(d, d*d);
}
float fetchDTexel(vec2 offset) {
    return texture(densityImage,  fragUV + offset).r;
}

void main(){
    vec2 texelSize = vec2(1.0) / push.screenSize;
    vec2 vd = fetchVTexel(vec2(0.0));
	vec2 vFilter = vd;
    vFilter += fetchVTexel(vec2(texelSize.x, texelSize.y));
    vFilter += fetchVTexel(vec2(-texelSize.x, -texelSize.y));
    vFilter += fetchVTexel(vec2(texelSize.x, -texelSize.y));
    vFilter += fetchVTexel(vec2(-texelSize.x, texelSize.y));
    vFilter /= 5.0;

    float radius = 15.0; // make push
    float occlusion = 0.0;
    
    float sd0 =     fetchDepthTexel(radius * texelSize * vec2(1.0, 0.0));
    float sd30 =    fetchDepthTexel(radius * texelSize * vec2(sqrt(3.0) / 2.0, 0.5));
    float sd60 =    fetchDepthTexel(radius * texelSize * vec2(0.5, sqrt(3.0) / 2.0));
    float sd90 =    fetchDepthTexel(radius * texelSize * vec2(0.0, 1.0));
    float sd120 =   fetchDepthTexel(radius * texelSize * vec2(-0.5, sqrt(3.0) / 2.0));
    float sd150 =   fetchDepthTexel(radius * texelSize * vec2(-sqrt(3.0) / 2.0, 0.5));
    float sd180 =   fetchDepthTexel(radius * texelSize * vec2(-1.0, 0.0));
    float sd210 =   fetchDepthTexel(radius * texelSize * vec2(-sqrt(3.0) / 2.0, -0.5));
    float sd240 =   fetchDepthTexel(radius * texelSize * vec2(-0.5, -sqrt(3.0) / 2.0));
    float sd270 =   fetchDepthTexel(radius * texelSize * vec2(0.0, -1.0));
    float sd300 =   fetchDepthTexel(radius * texelSize * vec2(0.5, -sqrt(3.0) / 2.0));
    float sd330 =   fetchDepthTexel(radius * texelSize * vec2(sqrt(3.0) / 2.0, -0.5));

    float rangeCheckSD0 =   smoothstep(0.0, 1.0, 0.6 / abs(vd.x - sd0));
    float rangeCheckSD30 =  smoothstep(0.0, 1.0, 0.6 / abs(vd.x - sd30));
    float rangeCheckSD60 =  smoothstep(0.0, 1.0, 0.6 / abs(vd.x - sd60));
    float rangeCheckSD90 =  smoothstep(0.0, 1.0, 0.6 / abs(vd.x - sd90));
    float rangeCheckSD120 = smoothstep(0.0, 1.0, 0.6 / abs(vd.x - sd120));
    float rangeCheckSD150 = smoothstep(0.0, 1.0, 0.6 / abs(vd.x - sd150));
    float rangeCheckSD180 = smoothstep(0.0, 1.0, 0.6 / abs(vd.x - sd180));
    float rangeCheckSD210 = smoothstep(0.0, 1.0, 0.6 / abs(vd.x - sd210));
    float rangeCheckSD240 = smoothstep(0.0, 1.0, 0.6 / abs(vd.x - sd240));
    float rangeCheckSD270 = smoothstep(0.0, 1.0, 0.6 / abs(vd.x - sd270));
    float rangeCheckSD300 = smoothstep(0.0, 1.0, 0.6 / abs(vd.x - sd300));
    float rangeCheckSD330 = smoothstep(0.0, 1.0, 0.6 / abs(vd.x - sd330));

	occlusion += float(int(bool(sd0 <= vd.x))) * rangeCheckSD0;
	occlusion += float(int(bool(sd30 <= vd.x))) * rangeCheckSD30;
	occlusion += float(int(bool(sd60 <= vd.x))) * rangeCheckSD60;
	occlusion += float(int(bool(sd90 <= vd.x))) * rangeCheckSD90;
	occlusion += float(int(bool(sd120 <= vd.x))) * rangeCheckSD120;
	occlusion += float(int(bool(sd150 <= vd.x))) * rangeCheckSD150;
	occlusion += float(int(bool(sd180 <= vd.x))) * rangeCheckSD180;
	occlusion += float(int(bool(sd210 <= vd.x))) * rangeCheckSD210;
	occlusion += float(int(bool(sd240 <= vd.x))) * rangeCheckSD240;
	occlusion += float(int(bool(sd270 <= vd.x))) * rangeCheckSD270;
	occlusion += float(int(bool(sd300 <= vd.x))) * rangeCheckSD300;
	occlusion += float(int(bool(sd330 <= vd.x))) * rangeCheckSD330;

    occlusion /= 12.0;
    
    float invDensity = 1.0 / (1.0 + fetchDTexel(vec2(0.0))); // compression

    outColor = vec4(vFilter.x, vFilter.y, invDensity, occlusion);
	
	// variance check with vFilter.xy, compare depth with density, and add the occlusion
}