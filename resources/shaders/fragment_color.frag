#version 450

#include "weighted_blending.glsl"
layout (location = 0) in flat vec4 color;
layout (location = 1) in flat float weightBias;
layout (location = 0) out vec4 outAccum;
layout (location = 1) out float outReveal;
void main(){
	outAccum = getAlphaAccum(color.xyz, color.w) * weightBias;
	outReveal = color.w;
}