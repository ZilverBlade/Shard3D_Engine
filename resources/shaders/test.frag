#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_KHR_vulkan_glsl : enable

layout (location = 0) out vec3 GBuffer0; // diffuse + chrominess R8G8B8A8 unorm
layout (set = 0, binding = 0) uniform sampler2D diffuseChrom;
layout (location = 0) in vec2 FragUV;
void main() {
	vec3 tint = vec3(1.0, 0.0, 1.0);
	float blend = 0.5;
	
	vec3 color = textureLod(diffuseChrom, FragUV, 0.0).rgb;
	
	GBuffer0 =  mix(color, tint, blend);
}