#version 450
#extension GL_GOOGLE_include_directive : enable

layout (constant_id = 0) const uint Diffuse = 0x000000FF;
layout (constant_id = 1) const float specular = 0.5;
layout (constant_id = 2) const float shininess = 128.0;
layout (constant_id = 3) const uint Emission = 0xFFFFFF00;

#include "compression.glsl"

layout(location = 0) in vec3 fragPosWorld;
layout(location = 1) in vec2 fragUV;
layout(location = 2) in vec3 fragNormalWorld;
layout(location = 3) in flat ivec2 itile;
layout (location = 0) out vec4 GBuffer0; // diffuse + chrominess R8G8B8A8 unorm
layout (location = 1) out vec2 GBuffer1; // material R8G8 unorm
layout (location = 2) out vec3 GBuffer2; // emission B10G11R11 ufloat
layout (location = 3) out uint GBuffer3; // normal pack uint32
layout (location = 4) out uint GBuffer4; // shading model uint8
layout(set = 2, binding = 0) uniform sampler2DArray heightMap; // R16_SFLOAT height map, divided by tiles


float random1(vec2 coords) {
   return fract(sin(dot(coords.xy, vec2(49.9898,148.233))) * 43758.5453) * 2.0 - 1.0;
}
float random2(vec2 coords) {
   return fract(sin(dot(coords.xy, vec2(-2121.2132,22.56167))) * 122.3623) * 2.0 - 1.0;
}

vec2 getRandomOrientation(vec2 uv, vec2 tile){
	float x = sign(random1(tile));
	float y = sign(random2(tile));
	x = x == 0.0 ? 1.0 : x;
	y = y == 0.0 ? 1.0 : y;
	return vec2(x, y) ;
}

vec3 previewTileColor(vec2 tile) {
	return abs(vec3(random1(tile), random2(tile), random2(tile + 8591.23)));
}
void main() {

	vec3 diffuse = vec3(
		(Diffuse & 0xFF000000 >> 24) / 255.0,
		(Diffuse & 0x00FF0000 >> 16) / 255.0,
		(Diffuse & 0x0000FF00 >> 8) / 255.0
	);
	diffuse = previewTileColor(vec2(itile));

	vec4 emissive = vec4(0.0);
	GBuffer0 = vec4(diffuse.r, diffuse.g, diffuse.b, 1.0);
	GBuffer1 = vec2(specular, (shininess - 1.0) / 255.0);
	GBuffer2 = emissive.rgb * emissive.a;
	GBuffer3 = cram_32bnormal(normalize(fragNormalWorld));
	GBuffer4 = 2;
	
}