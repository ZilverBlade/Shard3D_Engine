#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_control_flow_attributes : enable

#pragma optionNV(fastmath on)
#pragma optionNV(fastprecision on)

layout (constant_id = 0) const uint LIGHT_TYPE = 0; // 0 == dir, 1 == spot, 2 == point
//layout (constant_id = 1) const bool CAST_SHADOWS = false;
//layout (constant_id = 2) const uint SHADOW_FLAGS = 0x00; // 0x01 == PCF, 0x02 == VARIANCE, 0x10 == OPAQUE, 0x20 == TRANSLUCENT  

#define DISABLE_LIGHT_CULLING

#include "global_ubo.glsl"
#include "scene_ssbo.glsl"
#include "surface_lighting.glsl"
#include "depth_to_worldpos.glsl"
#include "shadow_math.glsl"

#include "descriptor_indexing.glsl"
#include "compression.glsl"

layout (set = 3, binding = 0) uniform sampler2D inputDepth;
layout (set = 3, binding = 2) uniform sampler2D inputGBuffer1; // specular + shininess buffer
layout (set = 3, binding = 4) uniform usampler2D inputGBuffer3; // normal buffer
layout (set = 3, binding = 5) uniform usampler2D inputGBuffer4; // shading model
#ifndef NO_DECAL_NORMAL
layout (set = 3, binding = 7) uniform sampler2D inputDBuffer2;
#endif
#ifdef MULTI_SAMPLED
#error unsupported
#endif

layout (push_constant) uniform Push {
	uint lightIndex;
} push;

layout (location = 0) in vec4 clipSpace;
layout (location = 0) out vec3 outDiffuse;
layout (location = 1) out vec3 outSpecular;

void main(){
	vec2 fragUV = (clipSpace.xy / clipSpace.w) * 0.5.xx + 0.5.xx;
	const uint gbuffer4 = textureLod(inputGBuffer4, fragUV, 0.0).x & 0x0F;
// depth test will take care of undrawn area
	if (gbuffer4 == SHADING_MODEL_UNSHADED) discard; 
	const float depth = textureLod(inputDepth, fragUV, 0.0).x;
	
	const vec2 gbuffer1 = textureLod(inputGBuffer1, fragUV, 0.0).xy;
	const uint gbuffer3 = textureLod(inputGBuffer3, fragUV, 0.0).x;
	
	ShadedPixelInfo pixel;
	pixel.positionWorld = WorldPosFromDepth(fragUV, depth, ubo.invProjection, ubo.invView);
	vec4 norm = unpack_normal(gbuffer3).xyzw; // W component stores front/backfacing, flip backfacing normals to get correct shading
	pixel.normal = norm.xyz * norm.w;
#ifndef NO_DECAL_NORMAL
	vec3 Q2 = dFdy(pixel.positionWorld);
	float len2Q2 = dot(Q2, Q2);
	if (len2Q2 > 0.000000001) {
		vec4 dbufferNormalMap = textureLod(inputDBuffer2, fragUV, 0.0).xyzw;
		if (dbufferNormalMap.a != 0.0) {
			dbufferNormalMap.xyz = dbufferNormalMap.xyz * 2..xxx - 1..xxx;
			vec3 T = Q2 / sqrt(len2Q2).xxx;
			mat3 TBN = mat3(T, cross(pixel.normal, T), pixel.normal);
			pixel.normal = normalize(mix(pixel.normal, TBN * dbufferNormalMap.xyz, dbufferNormalMap.a));
		}
	}
#endif
	pixel.specular = gbuffer1.x;
	pixel.shininess = gbuffer1.y * 255.0 + 1.0;
	pixel.shadingModel = gbuffer4;
	
	GlobalFragmentInfo gFragInfo = getGlobalFragmentInfo(pixel.positionWorld, pixel.normal);
	
	LightingInfo info;
	vec3 specularBoost = vec3(1.0);
	const vec3 SUN_TINT = vec3(1.0, 0.9834, 0.8843) ; // made to look a bit more like the sun
	[[flatten]]
	switch (LIGHT_TYPE) {
	case(2): 
		info = getPointLightingInfo(gFragInfo, pixel, sceneOmniLights.pointlights[push.lightIndex]);		
		break;
	case(1): 
		info = getSpotLightingInfo(gFragInfo, pixel, sceneOmniLights.spotlights[push.lightIndex]);
		break;
	case(0): 
		info = getDirectionalLightingInfo(gFragInfo, pixel, sceneEnvironment.directionalLights[push.lightIndex]);
		if (info.skip) { 
			discard; // sun is only case of culling allowed
		}
		specularBoost = SUN_TINT * 8.0 ; // specular boost for highlights
		break;
	}
	
	LightResult result = calculateLightFromSource(gFragInfo, info, pixel);
	outDiffuse = result.diffuse;
	outSpecular = result.specular;
}