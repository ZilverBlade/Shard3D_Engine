#version 450
// screen view aligned

const vec2 OFFSETS[6] = vec2[](
  vec2(1.0, -1.0),
  vec2(1.0, 1.0),
  vec2(-1.0, -1.0),
  vec2(-1.0, -1.0),
  vec2(1.0, 1.0),
  vec2(-1.0, 1.0)
);
const vec2 UV[6] = vec2[](
  vec2(1.0, 1.0),
  vec2(1.0, 0.0),
  vec2(0.0, 1.0),
  vec2(0.0, 1.0),
  vec2(1.0, 0.0),
  vec2(0.0, 0.0)
);

// hope this shit works
#define BILLBOARD_NORMAL_VECTOR vec3(0.0, 0.0, -1.0) 
#define BILLBOARD_TANGENT_VECTOR vec3(0.0, 1.0, 0.0)

layout (location = 0) out vec3 fragPosWorld;
layout (location = 1) out vec2 fragUV;
layout (location = 2) out vec3 fragNormalWorld;
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableVertexTangents
layout(location = 3) out vec3 fragTangentWorld;
#endif

layout(set = 0, binding = 0) uniform GlobalUbo{
	mat4 projection;
	mat4 invProjection;
	mat4 view;
} ubo;

layout(push_constant) uniform Push {
	mat4 transform;
	mat4 normal;
} push;

void main(){
	vec2 fragOffset = OFFSETS[gl_VertexIndex];
	fragUV = UV[gl_VertexIndex];

	vec4 texInCameraSpace = ubo.view * push.transform[3];
	vec3 positionInCameraSpace = texInCameraSpace.xyz + mat3(push.transform) * vec3(fragOffset, 0.0);
	fragPosWorld = positionInCameraSpace.xyz;
	
	fragNormalWorld = push.normal * BILLBOARD_NORMAL_VECTOR;
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableVertexTangents
	fragTangentWorld = push.normal * BILLBOARD_NORMAL_VECTOR;
#endif
	gl_Position = ubo.projection * vec4(positionInCameraSpace, 1.0);
}