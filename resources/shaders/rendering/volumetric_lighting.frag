#version 450
#extension GL_EXT_control_flow_attributes : enable

#pragma optionNV(fastmath on)
#pragma optionNV(fastprecision on)
// directional: low: 4, medium 8, high 12
// spot: low: 2, medium 6, high 10
// point: low: 2, medium 6, high 10
layout (constant_id = 0) const uint LIGHT_TYPE = 0; // 0 = dir, 1 = spot, 2 = point 	
layout (constant_id = 1) const uint MAX_STEPS = 6; 			

#define DESCRIPTOR_BINDLESS_SET 2

#include "global_ubo.glsl"
#include "scene_buffers.glsl"
#include "shadow_math.glsl"
#include "depth.glsl"
#include "depth_to_worldpos.glsl"

layout (location = 0) in vec4 clipVert;
layout (location = 1) in vec3 samplingBack;
layout (location = 0) out vec3 outColor;

layout (set = 3, binding = 0) uniform sampler2D inputDepth;
layout (push_constant) uniform Push {
	uint lightIndex;
	float gScattering;
	float cScattering; // constant scattering
	float padd;
	vec3 tint;
} push;

SAMPLER2D_TEXTURE(tex2DID);
const float PI = 3.14159265359;
const float FOG_BOOST = 1.0;
const float MAX_OMNI_RAY_DIST = 128.0;
const float ATTENUATION_EPSILON = 0.01;
const float ATTENUATION_EPSILON_2 = 0.2;
const float ditherPattern [4][4] = {{ 0.0f, 0.5f, 0.125f, 0.625f},
{ 0.75f, 0.22f, 0.875f, 0.375f},
{ 0.1875f, 0.6875f, 0.0625f, 0.5625},
{ 0.9375f, 0.4375f, 0.8125f, 0.3125}};


float ComputeScattering(float lightDotView) {
	float result = 1.0f - push.gScattering * push.gScattering;
	result /= (2.0f * PI * pow(1.0f + push.gScattering * push.gScattering - (2.0f * push.gScattering) * lightDotView, 1.5f));
	return push.cScattering + result;
}

vec3 applyLightCookie(sampler2D cookie, vec4 clip) {
	clip.xy = clip.xy * 0.5 + 0.5;
	return texture(cookie, clip.xy).rgb;
}

vec3 calculateVolumetricLightDirectional(vec3 fragPosWorld, DirectionalLight light) {
	vec3 incident = fragPosWorld - ubo.invView[3].xyz;
	vec3 I = normalize(incident);
	vec4 clipPosition = sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex].lightSpaceMatrix * vec4(fragPosWorld, 1.0);
	vec3 L = -light.direction.xyz;
	vec4 cameraInShadowSpace = sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex].lightSpaceMatrix * vec4(ubo.invView[3].xyz, 1.0);
	
	vec3 rayVector = clipPosition.xyz - cameraInShadowSpace.xyz;
	
	float rayLength = length(rayVector);
	vec3 rayDirection = rayVector / rayLength;
	
	float stepLength = rayLength / float(MAX_STEPS);
	
	vec3 rstep = rayDirection * stepLength;
	
	vec3 accumFog = vec3(0.0);
	
	[[unroll]]
	for (int i = 0; i < MAX_STEPS; i++) {
		float ditherValue = ditherPattern[int(gl_FragCoord.x) % 4][int(gl_FragCoord.y) % 4];
		vec3 clipSpaceStep = cameraInShadowSpace.xyz + rstep * float(i) + ditherValue * rstep;
		clipSpaceStep.xy = clipSpaceStep.xy * 0.5 + 0.5;
		
		float shadowMapValue = textureLod(pcfShadowMaps[sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex].shadowMap_ID], clipSpaceStep.xyz, 0.0).x;
		
		accumFog += shadowMapValue * vec3(1.0);
	}
	return FOG_BOOST * light.color.rgb * (accumFog / float(MAX_STEPS)) * ComputeScattering(max(dot(I, L), 0.0));
}


vec3 calculateVolumetricLightSpot(vec3 fragPosWorld, SpotLight light) {
	vec3 incident = fragPosWorld - ubo.invView[3].xyz;
	vec3 I = normalize(incident);
	float outerCosAngle = light.angle.x;
	
	vec3 target = ubo.invView[3].xyz + I * min(length(incident), MAX_OMNI_RAY_DIST);
	vec3 startPosition = ubo.invView[3].xyz ;
	vec3 endPosition = target;

	vec3 rayVector = endPosition - startPosition;
	vec4 clip = sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex].lightSpaceMatrix * vec4(endPosition, 1.0);	
	vec4 startInShadowSpace = sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex].lightSpaceMatrix * vec4(startPosition, 1.0);
	
	vec4 rayVectorClip = clip - startInShadowSpace;
	
	float rayLengthClip = length(rayVectorClip);
	vec4 rayDirectionClip = rayVectorClip / rayLengthClip;
	
	float stepLengthClip = rayLengthClip / float(MAX_STEPS);
	
	vec4 rstepClip = rayDirectionClip * stepLengthClip;
	
	float rayLength = length(rayVector);
	vec3 rayDirection = rayVector / rayLength;
	
	float stepLength = rayLength / float(MAX_STEPS);
	
	vec3 rstep = rayDirection * stepLength;
	
	vec3 accumFog = vec3(0.0);
	
	vec3 currentPosition;	
	vec4 currentPositionClip;
	
	bool requiresClip = light.cookie_ID != 0 || light.shadowMapInfoIndex != -1;

	
	[[unroll]]
	for (int i = 0; i < MAX_STEPS; i++) {
		float ditherValue = ditherPattern[int(gl_FragCoord.x) % 4][int(gl_FragCoord.y) % 4];
		currentPosition = startPosition + rstep * i + ditherValue * rstep;
		
		vec3 fragToLight = light.position.xyz - currentPosition;
		vec3 L = normalize(fragToLight);

		float distSq = dot(fragToLight, fragToLight);
		float radiusSq = light.radius * light.radius;

		float dOverLr2 = distSq / radiusSq;
		float dOverLr4 = dOverLr2 * dOverLr2;
		float fallNum = clamp(1.0 - dOverLr4, 0.0, 1.0);
		float attenuation = fallNum * fallNum / (distSq + 1.0);

		float theta = dot(L, -light.direction.xyz);
		float insideCone = step(outerCosAngle - theta, 0.0);
		float epsilon = light.angle.y - outerCosAngle;
		float intensity = clamp((theta - outerCosAngle) / epsilon, 0.0, 1.0);
		
		vec3 cookie = vec3(1.0);
		//if (insideCone == 0.0) continue;
		if (requiresClip) {
			currentPositionClip = startInShadowSpace + rstepClip * i + ditherValue * rstepClip;
			currentPositionClip.xyz /= currentPositionClip.w;
			if (light.cookie_ID != 0) {
				cookie = applyLightCookie(tex2DID[light.cookie_ID], currentPositionClip);
			}
			if (light.shadowMapInfoIndex != -1) {
#ifdef S3DSDEF_SHADER_PERMUTATION_SpotShadowMapType_Variance
				if (sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex].shadowMapTranslucent_ID != 0) {
					cookie *= getTranslucentVarianceShadow(currentPositionClip, sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex]);
				} else {
					cookie *= getVarianceShadow(currentPositionClip, sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex]);
				}
#endif
#ifdef S3DSDEF_SHADER_PERMUTATION_SpotShadowMapType_PCF
				light.shadow.poissonSamples = 1;
				light.shadow.poissonRadius = 0.0;
				if (sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex].shadowMapTranslucent_ID != 0) {
					cookie *= getTranslucentPCFShadow(currentPositionClip, 0.0, sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex]);
				} else {
					cookie *= getPCFShadow(currentPositionClip, lightToFrag, L, 0.0, sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex]);
				}
#endif 
			}
		}
		
		accumFog += cookie * insideCone * intensity * attenuation * ComputeScattering(max(dot(I, L), 0.0));
	}
	
	return FOG_BOOST * light.color.rgb * accumFog / float(MAX_STEPS);
}

vec3 calculateVolumetricLightPoint(vec3 fragPosWorld, PointLight light) {
	vec3 startPosition = ubo.invView[3].xyz;
	
	vec3 incident = fragPosWorld - ubo.invView[3].xyz;
	vec3 I = normalize(incident);
	vec3 rayVector = I * min(length(incident), MAX_OMNI_RAY_DIST);
	
	float rayLength = length(rayVector);
	vec3 rayDirection = rayVector / rayLength;
	
	float stepLength = rayLength / float(MAX_STEPS);
	
	vec3 rstep = rayDirection * stepLength;
	
	vec3 accumFog = vec3(0.0);
	
	vec3 currentPosition;	
	
	[[unroll]]
	for (int i = 0; i < MAX_STEPS; i++) {
		float ditherValue = ditherPattern[int(gl_FragCoord.x) % 4][int(gl_FragCoord.y) % 4];
		vec3 currentPosition = startPosition + rstep * i + ditherValue * rstep;
		
		vec3 lightToFrag = light.position.xyz - currentPosition;
		vec3 L = normalize(lightToFrag);
		float atten = 1.0 / max(dot(lightToFrag, lightToFrag), 0.01);
		
		float currentDepth = length(lightToFrag) / 256.0; 
		float shadowMapValue = texture(pcfShadowCubes[sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex].shadowMap_ID], vec4(L, currentDepth)).x;
		
		accumFog += atten * shadowMapValue * ComputeScattering(dot(rayDirection, -L));
	}
	return light.color.rgb * accumFog / float(MAX_STEPS);
}


void main() {
	vec2 fragUV = (clipVert.xy/clipVert.w) * 0.5.xx + 0.5.xx;
	float depth = textureLod(inputDepth, fragUV, 0.0).x;
	vec3 world = WorldPosFromDepth(fragUV, depth, ubo.invProjection, ubo.invView);
	world = depth == CLEAR_DEPTH ? samplingBack :
	 	length(samplingBack - ubo.invView[3].xyz) < length(world - ubo.invView[3].xyz) ? samplingBack : world;
	vec3 mist;
	[[flatten]]
	if (LIGHT_TYPE == 0) {
		mist = calculateVolumetricLightDirectional(world, sceneEnvironment.directionalLights[push.lightIndex]);
	} else if (LIGHT_TYPE == 1) {
		//vec3 inc = world - ubo.invView[3].xyz;
		//Spotlight light = sceneOmniLights.spotlights[push.lightIndex];
		//vec3 ltf = light.position.xyz - world;
		//float attenuation = 1.0 / dot(ltf, ltf);
		//if (attenuation * light.color.w < ATTENUATION_EPSILON) {
		//	vec3 I = normalize(inc);
		//	float stheta = sqrt(1.0 - light.angle.x * light.angle.x); // always between 0 and 1.5 radians;
		//	world -= I * stheta * (length(ltf) - sqrt(light.color.w / ATTENUATION_EPSILON_2));
		//}
		mist = calculateVolumetricLightSpot(world, sceneOmniLights.spotLights[push.lightIndex]);
	} else if (LIGHT_TYPE == 2) {
		mist = calculateVolumetricLightPoint(world, sceneOmniLights.pointLights[push.lightIndex]);
	}
	
	outColor = mist * push.tint;
	//outColor = 0..xxx;
}