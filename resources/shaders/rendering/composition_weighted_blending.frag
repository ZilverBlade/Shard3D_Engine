#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_KHR_vulkan_glsl : enable

layout (set = 0, binding = 0) uniform sampler2D inputAlphaColorAccum;
layout (set = 0, binding = 1) uniform sampler2D inputAlphaColorReveal;

layout( location = 0) in vec2 fragUV;
layout (location = 0) out vec4 outColor;

// epsilon number
const float EPSILON = 0.00001;

// calculate floating point numbers equality accurately
bool isApproximatelyEqual(float a, float b) {
    return abs(a - b) <= (abs(a) < abs(b) ? abs(b) : abs(a)) * EPSILON;
}

// get the max value between three values
float max3(vec3 v) {
    return max(max(v.x, v.y), v.z);
}


// shader code
void main(){
    // fragment revealage
    float revealage = texture(inputAlphaColorReveal, fragUV).r;

    // save the blending and color texture fetch cost if there is not a transparent fragment
    if (isApproximatelyEqual(revealage, 1.0)) discard;
	// fragment color
	vec4 accumulation = texture(inputAlphaColorAccum, fragUV);
	
	// suppress overflow
	if (isinf(max3(abs(accumulation.rgb))))
		accumulation.rgb = vec3(accumulation.a);
	
	// prevent floating point precision bug
	vec3 average_color = accumulation.rgb / max(accumulation.a, EPSILON);
	
	// blend pixels
	outColor = vec4(average_color, 1.0 - revealage);
}