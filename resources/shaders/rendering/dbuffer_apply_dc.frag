#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_KHR_vulkan_glsl : enable


layout (set = 0, binding = 0) uniform sampler2D inputDBuffer0; 
layout (set = 0, binding = 1) uniform sampler2D inputDBuffer1; 

layout (location = 0) in vec2 fragUV;
layout (location = 0, index = 0) out vec4 GBuffer0;
layout (location = 0, index = 1) out vec4 GBuffer0_Alpha;

// shader code
void main() {
	vec4 diffuse = textureLod(inputDBuffer0, fragUV, 0.0).rgba;
	vec2 chro = textureLod(inputDBuffer1, fragUV, 0.0).ba;
	
	if (diffuse.a == 0.0 && chro.g == 0.0) discard;
	
	GBuffer0 = vec4(diffuse.rgb, chro.r);
	GBuffer0_Alpha = vec4(diffuse.aaa, chro.g);
}
