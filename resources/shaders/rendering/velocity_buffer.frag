#version 450

#include "global_ubo.glsl"
#include "depth_to_worldpos.glsl"
#include "depth.glsl"

layout (set = 1, binding = 0) uniform sampler2D oldDepth;
layout (set = 1, binding = 1) uniform usampler2D objectMask;

layout (location = 0) in vec2 fragUV;
layout (location = 0) out vec2 velocity;

layout (push_constant) uniform OldCameraUnproject {
	mat4 invView;
	mat4 invProjection;
} oldCamera;

void main() {
	if (oldCamera.invView[3].w == 0.0 || (textureLod(objectMask, fragUV, 0.0).x & 0x10) != 0) {
		discard; // old depth buffer not ready yet or pixel already has velocity data
	}
	const vec2 currentClipSpacePosition = fragUV * 2.0 - 1.0;
	float previousDepth = textureLod(oldDepth, fragUV, 0.0).x;
	
#ifdef S3DSDEF_SHADER_PERMUTATION_INFINITE_FAR_Z
	// treat differently as reverse depth might have infinite far plane, 
	// we must add a slight offset to avoid reaching division by 0.0
	if (previousDepth == 0.0) { 
		previousDepth += 1e-9; // small epsilon value 
		
		const vec4 previousPositionWorld = vec4(WorldPosFromDepth(fragUV, previousDepth, oldCamera.invProjection, oldCamera.invView), 1.0);
		
		const vec4 reprojectedLastClipSpacePosition = ubo.projectionView * previousPositionWorld;
		const vec2 deltaMotionVector = currentClipSpacePosition - (reprojectedLastClipSpacePosition.xy / reprojectedLastClipSpacePosition.w);
		
		const vec2 v = -0.5 * deltaMotionVector;// negate to get same vector orientation as usual
		
		velocity = v * vec2(ubo.velocityBufferTileExtent);
	} else
#endif
	{
		const vec4 previousPositionWorld = vec4(WorldPosFromDepth(fragUV, previousDepth, oldCamera.invProjection, oldCamera.invView), 1.0);
		
		const vec4 reprojectedLastClipSpacePosition = ubo.projectionView * previousPositionWorld;
		const vec2 deltaMotionVector = currentClipSpacePosition - (reprojectedLastClipSpacePosition.xy / reprojectedLastClipSpacePosition.w);
		
		const vec2 v = -0.5 * deltaMotionVector;// negate to get same vector orientation as usual
		
		velocity = v * vec2(ubo.velocityBufferTileExtent);
	}
}  