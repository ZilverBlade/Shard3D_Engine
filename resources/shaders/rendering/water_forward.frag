#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_KHR_vulkan_glsl : enable

#include "global_ubo.glsl"
#include "surface_lighting.glsl"

layout(location = 0) in vec3 fragPosWorld;
layout(location = 1) in vec2 fragUV;

// the water shader is opaque, however we refract the deferred pass to convey the appearance of transparency (no transparent objects will be visible tho >:D, too bad!) 

layout(set = 1, binding = 1) uniform sampler2D DuDvMap; // only use if sampling planar reflections
layout(set = 1, binding = 2) uniform sampler2D NormalMap;

layout (push_constant) uniform Push {
	vec4 translation;
	vec2 offset;
	uint useWaveMap;
	float heightMul;
} push;

void main() {
	bool sampleReflection = push.useWaveMap == 1; // if using wave map, we are likely to have something like the sea, dense, poor reflection except specular
	
	
	
	fragPosWorld = positionWorld.xyz;

#ifdef ENABLE_CLIP_PLANE
	gl_ClipDistance[0] = dot(positionWorld, ubo.clipPlane);
#endif
}