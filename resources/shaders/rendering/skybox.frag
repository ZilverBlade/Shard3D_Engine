#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_KHR_vulkan_glsl : enable
#extension GL_EXT_nonuniform_qualifier : enable

#include "global_ubo.glsl"
#include "scene_buffers.glsl"
#include "depth.glsl"

layout (location = 0) out vec4 outColor;
layout (location = 0) in vec3 texCoords;


layout(set = 2, binding = 1) uniform samplerCube texCubeID[];


void main() {    
    outColor = vec4(texture(texCubeID[sceneEnvironment.activeSkybox.skybox_id], texCoords).xyz * sceneEnvironment.activeSkybox.tint.xyz * sceneEnvironment.activeSkybox.tint.w, 1.0);
}