#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_KHR_vulkan_glsl : enable


layout (set = 0, binding = 1) uniform sampler2D inputDBuffer1; 

layout (location = 0) in vec2 fragUV;
layout (location = 0, index = 0) out vec2 GBuffer1;
layout (location = 0, index = 1) out vec2 GBuffer1_Alpha;

// shader code
void main() {
	vec3 specshin = textureLod(inputDBuffer1, fragUV, 0.0).rga;
	
	if (specshin.b == 0.0) discard;
	
	GBuffer1 = specshin.rg;
	GBuffer1_Alpha = specshin.bb;
}
