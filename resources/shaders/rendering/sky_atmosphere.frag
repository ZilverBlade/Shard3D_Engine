#version 450

layout (location = 0) out vec4 outColor;
layout (location = 0) in vec3 rayleighColor;
layout (location = 1) in vec3 mieColor;
layout (location = 2) in vec3 uv;

layout (constant_id = 0) const bool ENABLE_SUN = false;
layout (push_constant) uniform Push {
	mat4 projectionViewNoTranslate;
	vec4 direction;
	float rayleighTerm;
	float mieTerm;
	float height;
	float sunBoost;
} push;

float getMiePhase(float theta, float g) {
	if (ENABLE_SUN) {
		return ( ( 3.0 * (1.0 - g * g) ) / ( 2.0 * (2.0 + g * g )) ) * 
		( (1.0 + theta * theta) / (pow(1.0 + g * g - 2.0 * g * theta, 3.0 / 2.0)));
	} else {
		return 1.0;
	}
}
float getRayleighPhase(float theta) { 
	//return 0.75 + 0.75*theta*theta;
	return 0.75* (1.0 + theta*theta);
}
float g = -0.990;

void main() {    
	float fCos = dot(push.direction.xyz, normalize(uv));  
	vec3 color = getRayleighPhase(fCos) * rayleighColor + getMiePhase(fCos, g) * mieColor;
	
	//float amt = exp(-uv.y*0.5);
	//color = mix(color, vec3( 0.7594, 0.8235, 1.0000 ), clamp(amt, 0.0, 1.0));
	
    outColor = vec4(color, 1.0);
}