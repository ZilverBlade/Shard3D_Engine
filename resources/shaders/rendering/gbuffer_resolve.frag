#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_KHR_vulkan_glsl : enable
#extension GL_EXT_control_flow_attributes : enable

layout (constant_id = 0) const uint MSAA_SAMPLE_COUNT = 2;

#define MSAA_MAX_SAMPLES 8

#include "global_ubo.glsl"
#include "compression.glsl"
#include "depth.glsl"

layout (location = 0) in vec2 fragUV;

layout (set = 1, binding = 0) uniform sampler2DMS inputDepth;
layout (set = 1, binding = 1) uniform sampler2DMS inputGBuffer0; // diffuse RGB + chrominess buffer
layout (set = 1, binding = 2) uniform sampler2DMS inputGBuffer1; // specular + shininess buffer
layout (set = 1, binding = 3) uniform sampler2DMS inputGBuffer2; // emission buffer
layout (set = 1, binding = 4) uniform usampler2DMS inputGBuffer3; // normal buffer
layout (set = 1, binding = 5) uniform usampler2DMS inputGBuffer4; // shading model, obj data
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableMotionBlur
layout (set = 1, binding = 6) uniform sampler2DMS inputVelocity; // velocity buffer
#endif

layout (location = 0) out vec4 GBuffer0; // diffuse + chrominess R8G8B8A8 unorm
layout (location = 1) out vec2 GBuffer1; // material R8G8 unorm
layout (location = 2) out vec3 GBuffer2; // emission B10G11R11 ufloat
layout (location = 3) out uint GBuffer3; // normal pack uint32 
layout (location = 4) out uint GBuffer4; // shading model, obj data uint44
layout (location = 5) out uint MSAASampleWeights;
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableMotionBlur
layout (location = 6) out vec2 VelocityBuffer;
#endif

layout (push_constant) uniform Push {
	float depthDiffFactor;
} push;

// shader code
void main(){
	vec2 size = vec2(textureSize(inputDepth).xy - 1);
	float invSamples = 1.0 / float(MSAA_SAMPLE_COUNT);
	
	vec4 accumGBuffer0 = vec4(0.0);
	vec2 accumGBuffer1 = vec2(0.0);
	vec3 accumGBuffer2 = vec3(0.0);
	
	int shadeSamples = 1;
	int samples[MSAA_MAX_SAMPLES] = { 0, 0, 0, 0, 0, 0, 0, 0 };
	float depths[MSAA_MAX_SAMPLES] = { CLEAR_DEPTH,CLEAR_DEPTH, CLEAR_DEPTH, CLEAR_DEPTH, CLEAR_DEPTH, CLEAR_DEPTH, CLEAR_DEPTH, CLEAR_DEPTH };
	bool nonShadableSamples[MSAA_MAX_SAMPLES] = { false, false, false, false, false, false, false, false};
	float averageDepth = 0.0;
	float closestDepth = CLEAR_DEPTH;
	uint activeShadingModel = 0x0F;
	uint priorityObjectData = 0x00;
	uint normalData = 0;
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableMotionBlur
	vec2 velocityAccum = vec2(0.0);
#endif
	
// todo: use gl_SampleMaskIn? and write to R8 (2x) / R16 (4x) / R32 (8x) UINT buffers to store sample weights
	
	ivec2 coords = ivec2(fragUV * size);
	
	uint nonComplexSampleMask;
	
	[[flatten]]
	switch (MSAA_SAMPLE_COUNT) {
		case(2):
		nonComplexSampleMask = 3;
		
		case(4):
		nonComplexSampleMask = 15;
		case(8):
		nonComplexSampleMask = 63;
	}
	
	
	[[unroll]]
	for (int s = 0; s < MSAA_SAMPLE_COUNT; s++) {
		float rDepth = texelFetch(inputDepth, coords, s).x;
		nonShadableSamples[s] = rDepth == CLEAR_DEPTH;
		float depth = linearize_depth(rDepth, ubo.nearPlane, ubo.farPlane);
		averageDepth += depth;
		depths[s] = depth;
		bool passedDepthTest = rDepth DEPTH_COMPARE_SIGN closestDepth;
		closestDepth = passedDepthTest ? rDepth : closestDepth;
		
		if (passedDepthTest) {
			uint shadeObjectData = texelFetch(inputGBuffer4, coords, s).x ;
			activeShadingModel = shadeObjectData & 0x0F;
			// every single flag takes priority to reduce artifacts
			priorityObjectData |= (shadeObjectData & 0xF0) * int(activeShadingModel != 0x0F); 
			if (activeShadingModel != 0) {
				normalData = texelFetch(inputGBuffer3, coords, s).x;
			}
		}
	}
	
	if (activeShadingModel == 0x0F) discard; // nothing else to be resolved
	
	averageDepth *= invSamples;
	
	[[unroll]]
	for (int s = 0; s < MSAA_SAMPLE_COUNT; s++) {
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableMotionBlur
		velocityAccum += texelFetch(inputVelocity, coords, s).xy;
#endif
		if (activeShadingModel != 0) { // only shade if shading model requires shading
			accumGBuffer0 += texelFetch(inputGBuffer0, coords, s).xyzw;
			accumGBuffer1 += texelFetch(inputGBuffer1, coords, s).xy;
		}
		if (activeShadingModel == 2 || activeShadingModel == 0) {
			accumGBuffer2 += texelFetch(inputGBuffer2, coords, s).xyz;
		}
	}
	
	// sample weights
	//float weights[MSAA_MAX_SAMPLES];
	//int pixelIsNonComplex = 1;
	//for (int s = 0; s < MSAA_SAMPLE_COUNT; s++) {
	//	uint subpixelMask = texelFetch(inputMSAASampleMask, coords, s).x;
	//	pixelIsNonComplex &= int(subpixelMask == nonComplexSampleMask);
	//	
	//	uint seek = MSAA_SAMPLE_COUNT;
	//	while (seek != 0) {
	//		uint subpixelWeight = (subpixelMask >> seek) & 0x01;
	//		weights[s] += float(subpixelWeight);
	//		seek--;
	//	}
	//}
	
	float outsideRangeDepths[MSAA_MAX_SAMPLES] = { depths[0], 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
	float weights[MSAA_MAX_SAMPLES] = { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
	
	[[unroll]]
	for (int s = 1; s < MSAA_SAMPLE_COUNT; s++) { // find unique sample weights
		if (nonShadableSamples[s]) continue;
		float depth = depths[s];
		bool outsideRange = abs(averageDepth - depth) > push.depthDiffFactor;
		if (outsideRange) { // first check if we already have a similar depth to this to avoid unecessary shading
			bool similarity = false;
			int similarSample = 0;
			for (int i = 0; i < shadeSamples; i++) {
				similarity = abs(outsideRangeDepths[i] - depth) < push.depthDiffFactor;
				if (similarity) {
					similarSample = i;
					break;
				}
			}
			if (similarity) {
				weights[similarSample] += 1.0;
			} else {
				weights[shadeSamples] += 1.0;
				outsideRangeDepths[s] = depth;
				shadeSamples++;
			}
		} else { // TODO: wtf is this
			weights[0] += 1.0;
		}
	}
	
	if (shadeSamples == 1) {
		MSAASampleWeights = 0;
	} else {
		uint pWeights = 0;
		for (int s = 0; s < MSAA_SAMPLE_COUNT; s++) {
			uint weight = uint(weights[s]);
			pWeights |= weight << (4 * s);
		}
		MSAASampleWeights = pWeights;
	}
	
	gl_FragDepth = closestDepth;
	GBuffer0 = invSamples * accumGBuffer0;
	GBuffer1 = invSamples * accumGBuffer1;
	GBuffer2 = invSamples * accumGBuffer2;
	GBuffer3 = normalData;
	GBuffer4 = activeShadingModel | priorityObjectData;
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableMotionBlur
	VelocityBuffer = invSamples * velocityAccum;
#endif
}