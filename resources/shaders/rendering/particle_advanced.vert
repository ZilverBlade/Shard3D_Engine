#version 450
#extension GL_GOOGLE_include_directive : enable

#include "global_ubo.glsl"

// 43 bytes
layout(location = 4) in vec3 position; // R32G32B32_SFLOAT 12 byte
layout(location = 5) in vec3 velocity; // R16G16B16_SFLOAT 6 byte
layout(location = 6) in vec4 quat; // R16G16B16A16_SFLOAT 8 byte
layout(location = 7) in vec2 scale; // R16G16_SFLOAT 4 byte
layout(location = 8) in vec3 color; // B10G11R11_UFLOAT 4 byte
layout(location = 9) in float specular; // R8_UNORM 1 byte
layout(location = 10) in float shininess; // R8_UNORM 1 byte
layout(location = 11) in float chrominess; // R8_UNORM 1 byte
layout(location = 12) in float opacity; // R8_UNORM 1 byte
layout(location = 13) in uint coltex0; // R16_UINT 2 byte
layout(location = 14) in uint coltex1; // R16_UINT 2 byte
layout(location = 15) in float texLerp; // R8_UNORM 1 byte

layout(location = 0) out vec2 fragUV;
layout(location = 1) out vec3 fragPosWorld;
layout(location = 2) out vec4 projPrevFrame;
layout(location = 3) out flat vec3 color;
layout(location = 4) out flat float specular;
layout(location = 5) out flat float shininess;
layout(location = 6) out flat float chrominess;
layout(location = 7) out flat float opacity;
layout(location = 8) out flat uint coltex0;
layout(location = 9) out flat uint coltex1;
layout(location = 10) out flat float texLerp;

mat3 quatRot() {
	mat3 result;
	float qxx = quat.x * quat.x;
	float qyy = quat.y * quat.y;
	float qzz = quat.z * quat.z;
	float qxz = quat.x * quat.z;
	float qxy = quat.x * quat.y;
	float qyz = quat.y * quat.z;
	float qwx = quat.w * quat.x;
	float qwy = quat.w * quat.y;
	float qwz = quat.w * quat.z;

	result[0][0] = 1.0 - 2.0 * (qyy + qzz);
	result[0][1] = 2.0 * (qxy + qwz);
	result[0][2] = 2.0 * (qxz - qwy);

	result[1][0] = 2.0 * (qxy - qwz);
	result[1][1] = 1.0 - 2.0 * (qxx + qzz);
	result[1][2] = 2.0 * (qyz + qwx);

	result[2][0] = 2.0 * (qxz + qwy);
	result[2][1] = 2.0 * (qyz - qwx);
	result[2][2] = 1.0 - 2.0 * (qxx + qyy);
}

void main() {
	
	gl_Position = ubo.projectionView * vec4(position, 1.0);
}