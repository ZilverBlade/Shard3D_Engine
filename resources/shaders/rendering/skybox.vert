#version 450

#include "global_ubo.glsl"
#include "depth.glsl"

layout (location = 0) in vec3 vertexPos;
layout (location = 0) out vec3 texCoords;

void main() {
    texCoords = vertexPos;
    vec4 position = ubo.projection * mat4(mat3(ubo.view)) * vec4(vertexPos, 1.0);
	
#ifdef REVERSE_DEPTH_BUFFER
	position.z = 0.0;
	gl_Position = position.xyzw;
#else
	gl_Position = position.xyww;
#endif
}  