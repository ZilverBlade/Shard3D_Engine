#version 450
#extension GL_GOOGLE_include_directive : enable

#define DESCRIPTOR_BINDLESS_SET 1
#include "global_ubo.glsl"
#include "descriptor_indexing.glsl"

layout(location = 0) in vec3 position;
layout(location = 0) out flat uint instance;
layout(location = 1) out flat vec3 extent;

struct DecalInstanceData {
	mat4 transform;
	mat4 invTransform;
};
layout(std430, set = 3, binding = 0) readonly buffer DecalData {
	DecalInstanceData instances[8];
} decalData;

layout (push_constant) uniform Push {
	uint material;
} push;

void main() {
	mat4 transform = decalData.instances[gl_InstanceIndex].transform;
	instance = gl_InstanceIndex;
	
	extent = vec3(
		length(transform[0].xyz),
		length(transform[1].xyz),
		length(transform[2].xyz)
	);
	
	gl_Position = ubo.projectionView * transform * vec4(position, 1.0);
}