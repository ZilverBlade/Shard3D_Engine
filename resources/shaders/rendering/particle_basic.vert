#version 450
#extension GL_GOOGLE_include_directive : enable

#include "global_ubo.glsl"

const vec2 OFFSETS[4] = vec2[](
  vec2(1.0, -1.0),
  vec2(1.0, 1.0),
  vec2(-1.0, -1.0),
  vec2(-1.0, 1.0)
);
const vec2 UV[4] = vec2[](
  vec2(1.0, 1.0),
  vec2(1.0, 0.0),
  vec2(0.0, 1.0),
  vec2(0.0, 0.0)
);

// 31 bytes
layout(location = 4) in vec3 position; // R32G32B32_SFLOAT 12 byte
layout(location = 5) in vec3 worldVelocity; // R16G16B16_SFLOAT 6 byte
layout(location = 6) in float rotation; // R16_SFLOAT 2 byte
layout(location = 7) in vec2 scale; // R16G16_SFLOAT 4 byte
layout(location = 8) in vec3 color; // B10G11R11_UFLOAT 4 byte
layout(location = 9) in float opacity; // R8_UNORM 1 byte
layout(location = 10) in uint coltex0; // R16_UINT 2 byte

layout(location = 0) out vec2 fragUV;
layout(location = 1) out vec3 fragPosWorld;
layout(location = 2) out vec4 projPrevFrame;
layout(location = 3) out flat vec3 color;
layout(location = 7) out flat float opacity;
layout(location = 8) out flat uint coltex0;

vec2 rotate2D(vec2 vert) {
	float s = sin(rotation);
	float c = cos(rotation);
	return mat2(
	 vec2(c, s),
	 vec2(-s, c)
	) * vert;
};
vec4 billboardTransform(mat4 view, mat4 proj, vec3 pos) {
	vec4 particleInCameraSpace = view * vec4(pos, 1.0);
	return proj * vec4(texInCameraSpace.xyz + vec3(rotate2D(scale * OFFSETS[gl_VertexIndex]), 0.0), 1.0);
}
void main() {
	
	gl_Position = billboardTransform(ubo.view, ubo.projection , position);
	projPrevFrame = billboardTransform(ubo.viewPrevFrame, ubo.projectionPrevFrame, position - worldVelocity);
	fragUV = UV[gl_VertexIndex];
}