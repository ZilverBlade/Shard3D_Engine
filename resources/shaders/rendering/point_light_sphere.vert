#version 450
#extension GL_EXT_control_flow_attributes : enable

#include "global_ubo.glsl"
#include "scene_buffers.glsl"

layout (location = 0) in vec3 icoSphereVertex;
layout (location = 0) out vec4 clip;
#ifdef VOLUMETRICS_SAMPLING_POSITION
layout (location = 1) out vec3 samplingBack;
#endif

layout (push_constant) uniform Push {
	uint lightIndex;
	float attenuationEpsilon;
} push;

vec3 transformSpherePoint(PointLight light, vec3 point, float r) {
	return point * r.xxx + point * vec3(0.02, 0.02, 0.02) + light.position.xyz; // compensate for poly count
}
void main() {
	PointLight light = sceneOmniLights.pointLights[push.lightIndex];
	
	float r = light.radius;

#ifndef VOLUMETRICS_SAMPLING_POSITION
	vec3
#endif
		samplingBack = transformSpherePoint(light, icoSphereVertex, r);
	gl_Position = ubo.projectionView * vec4(samplingBack, 1.0);
	clip = gl_Position;
}