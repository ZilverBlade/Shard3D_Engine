#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_control_flow_attributes : enable

#define DESCRIPTOR_BINDLESS_SET 1

layout (constant_id = 1) const uint maxMaterialCount = 4; 
layout (constant_id = 2) const bool enableParallaxMapping = false; 

#include "descriptor_indexing.glsl"
#include "compression.glsl"
#include "terrain_material.glsl"
#include "terrain.glsl"

layout(location = 0) in vec3 fragPosWorld;
layout(location = 1) in vec2 fragUV;
layout(location = 2) in flat int index;
layout(location = 3) in flat ivec2 tile;

layout (location = 0) out vec4 GBuffer0; // diffuse + chrominess R8G8B8A8 unorm
layout (location = 1) out vec2 GBuffer1; // material R8G8 unorm
// writing to emission buffer is turned off
layout (location = 3) out uint GBuffer3; // normal pack uint32 
layout (location = 4) out uint GBuffer4; // shading model uint8 

layout(set = 2, binding = 0) buffer TileVisibility {
	TerrainTileData tiles[1];
} tileData;
layout(set = 2, binding = 1) uniform TerrainMaterialBuffer_ {
	TerrainMaterialBuffer buff;
} materialBuffer;
layout(set = 2, binding = 2) uniform sampler2D heightMap;
layout(set = 2, binding = 3) uniform sampler2D materialMap0;
//layout(set = 2, binding = 3) uniform sampler2DArray materialMap1;


layout (push_constant) uniform Push {
	vec4 translation;
	vec2 tiles;
	float tileExtent;
	float heightMul;
	uint LOD;
} push;


void main() {
	vec4 materialWeights0 = max(texture(materialMap0, fragUV) - 5e-2, 0.0);
	materialWeights0 /= dot(materialWeights0, 1..xxxx);
	
	float materialWeightArray[maxMaterialCount];
	[[unroll]]
	for (int i = 0; i < min(maxMaterialCount, 4); i++) {
		materialWeightArray[i] = materialWeights0[i];
	}
	//vec4 materialWeights1 = 0..xxxx;
	//materialWeights1 /= dot(materialWeights1, 1..xxxx);
	//[[unroll]]
	//for (int i = 4; i < min(maxMaterialCount, 8); i++) {
	//	materialWeightArray[i] = materialWeights1[i];
	//}
	
	
	TerrainTileData tileData = tileData.tiles[index];
	
	TerrainData data;
	
	data.translation	= push.translation;
	data.tiles			= push.tiles; 
	data.tileExtent	= push.tileExtent;
	data.heightMul		= push.heightMul;
	data.LOD			= push.LOD;
	
	vec3 normal = sampleTerrainNormal(data, heightMap, fragPosWorld, fragUV, tileData);
	
	TerrainMaterialResult result;
	result	= getTerrainMaterialData(fragPosWorld, normal, materialBuffer.buff.materials, materialWeightArray);
	
	GBuffer0 = vec4(result.diffuse, 1.0 - result.reflectivity);
	GBuffer1 = vec2(result.specular, result.glossiness);
	GBuffer3 = cram_32bnormal(result.normalVec);
	GBuffer4 = 1; // standard shaded shading model
}