#version 450

#include "depth.glsl"
#include "global_ubo.glsl"

layout (location = 0) in vec3 position;
layout (location = 0) out vec3 rayleighColor;
layout (location = 1) out vec3 mieColor;
layout (location = 2) out vec3 uv;

layout (push_constant) uniform Push {
	mat4 projectionViewNoTranslate;
	vec4 direction;
	float rayleighTerm;
	float mieTerm;
	float height;
	float sunBoost;
//	float atmosphereHeightKM;
} push;

float fInnerRadius = 6371.0; // The inner (planetary) radius (kilometers)
float fInnerRadius2 = fInnerRadius * fInnerRadius;
float fOuterRadius = 6530.0; // atmosphere radius (kilometers)
float fOuterRadius2 = fOuterRadius * fOuterRadius;

// 650 nm for red
// 570 nm for green
// 475 nm for blue
const vec3 v3InvWavelength = 1.0 / pow(vec3(0.650, 0.570, 0.475), vec3(4.0)); // 1 / pow(wavelength, 4) for the red, green, and blue channels

float fScale = 1.0 / (fOuterRadius -fInnerRadius );
			// 1 / (fOuterRadius - fInnerRadius)
float fScaleDepth = 0.25;		// The scale depth (i.e. the altitude at which the atmosphere's average density is found)
float fInvScaleDepth = 1.0 / fScaleDepth;
float fScaleOverScaleDepth = fScale /fInvScaleDepth ;	// fScale / fScaleDepth

//float Kr = 0.0025;// Rayleigh scattering constant
//float Km =  0.0010;// Mie scattering constant
//float ESun = 15.0; // sun brightness


float PI4 = 4.0 * 3.1415926;

float scale(float fCos)
{
	float x = 1.0 - fCos;
	return fScaleDepth * exp(-0.00287 + x*(0.459 + x*(3.83 + x*(-6.80 + x*5.25))));
}

// Returns the near intersection point of a line and a sphere
float getNearIntersection(vec3 v3Pos, vec3 v3Ray, float fDistance2, float fRadius2)
{
	float B = 2.0 * dot(v3Pos, v3Ray);
	float C = fDistance2 - fRadius2;
	float fDet = max(0.0, B*B - 4.0 * C);
	return 0.5 * (-B - sqrt(fDet));
}

// Returns the far intersection point of a line and a sphere
float getFarIntersection(vec3 v3Pos, vec3 v3Ray, float fDistance2, float fRadius2)
{
	float B = 2.0 * dot(v3Pos, v3Ray);
	float C = fDistance2 - fRadius2;
	float fDet = max(0.0, B*B - 4.0 * C);
	return 0.5 * (-B + sqrt(fDet));
}
uint samples = 3;

void main() {
	vec3 planetCamera = vec3(0.0, push.height, 0.0);
	
	float onePlusCosAngle = 1.0 + push.direction.y;
float Kr = push.rayleighTerm * (1.0 + 5.0 - 5.0 * (1.0 - onePlusCosAngle * onePlusCosAngle * onePlusCosAngle)); // Rayleigh scattering constant, scale with sun angle
float Km = push.mieTerm;// Mie scattering constant
float ESun = push.sunBoost; // sun brightness

	
	
	vec3 ray = normalize(position); 
	ray.y = max(ray.y, 0.0); // limit ray Y to the horizon to make reflections not look crappy
	float far = length(ray);    
	ray /= far;		// Calculate the closest intersection of the ray with    // the outer atmosphere (point A in Figure 16-3)     
	float fNear = getNearIntersection(vec3(0.0), ray, push.height, fOuterRadius2);      // Calculate the ray's start and end positions in the atmosphere,   
	// then calculate its scattering offset
	vec3 start = planetCamera.xyz;
	far -= fNear;
	float startAngle = dot(ray, start) / fOuterRadius;
	float startDepth = exp(-fScaleDepth);
	float startOffset = startDepth * scale(startAngle);	// Initialize the scattering loop variables     
	float sampleLength = far / float(samples);
	float scaledLength = sampleLength * fScale;
	vec3 sampleRay = ray * sampleLength;
	vec3 samplePoint = start + sampleRay * 0.5;    
	// Now loop through the sample points
	vec3 frontColor = vec3(0.0, 0.0, 0.0);
	for(int i=0; i < samples; i++) {
		float height = length(samplePoint);
		float depth = exp(fScaleOverScaleDepth * (fInnerRadius - height)); 
		float lightAngle = dot(push.direction.xyz, samplePoint) / height;
		float cameraAngle = dot(ray, samplePoint) / height;
		float scatter = (startOffset + depth * (scale(lightAngle), scale(cameraAngle)));
		vec3 attenuate = exp(-scatter * (v3InvWavelength * Kr * PI4 + Km * PI4));
		frontColor += attenuate * (depth * scaledLength);
		samplePoint += sampleRay;
	}      // Finally, scale the Mie and Rayleigh colors
	vec4 positionOut = push.projectionViewNoTranslate * vec4(position, 1.0);
#ifdef REVERSE_DEPTH_BUFFER
	positionOut.z = 0.0;
	gl_Position = positionOut.xyzw;
#else
	gl_Position = positionOut.xyww;
#endif
	
	uv = position;
	
	rayleighColor = frontColor * (v3InvWavelength * (Kr) * ESun);    
	mieColor = frontColor * Km * ESun;    
}