#version 450
const vec2 OFFSETS[6] = vec2[](
	vec2(1.0, -1.0),
	vec2(1.0, 1.0),
	vec2(-1.0, -1.0),
	vec2(-1.0, -1.0),
	vec2(1.0, 1.0),
	vec2(-1.0, 1.0)
);

layout (location = 0) out vec4 clip;

void main() {
	vec2 fragOffset = OFFSETS[gl_VertexIndex];
	gl_Position = vec4(fragOffset, 0.0, 1.0);
	clip = gl_Position;
}