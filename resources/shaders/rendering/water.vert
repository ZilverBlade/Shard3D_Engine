#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_KHR_vulkan_glsl : enable

#include "global_ubo.glsl"

//layout (constant_id = 0) const uint name = 0;

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv;

layout(location = 0) out vec3 fragPosWorld;
layout(location = 1) out vec2 fragUV;

layout(set = 1, binding = 0) uniform sampler2D WaveMap; // R16_SFLOAT height map for waves

layout (push_constant) uniform Push {
	vec4 translation;
	vec2 offset;
	uint useWaveMap;
	float heightMul;
} push;

void main() {
	float heightValue = 0.0;
	if (useWaveMap == 1) {
		heightValue = textureLod(waveMap, uv + push.offset, 0.0).x;
	}
	
	vec3 positionWorld = push.translation.xyz +vec3(position.x, position.y * heightValue * push.heightMul, position.z);
	gl_Position = ubo.projectionView * vec4(positionWorld, 1.0;
	
	
	fragPosWorld = positionWorld.xyz;

#ifdef ENABLE_CLIP_PLANE
	gl_ClipDistance[0] = dot(positionWorld, ubo.clipPlane);
#endif
}