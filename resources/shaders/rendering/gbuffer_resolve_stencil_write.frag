#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_KHR_vulkan_glsl : enable

layout (location = 0) in vec2 fragUV;

layout (set = 0, binding = 11) uniform usampler2D inputSampleWeightMask; // (2x) R8, (x4) R16, (8x) R32

void main(){
	if (textureLod(inputSampleWeightMask, fragUV, 0.0).x == 0)
		discard;
}