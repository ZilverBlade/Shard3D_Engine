#version 450
#extension GL_GOOGLE_include_directive : enable
#pragma optionNV(fastmath on)
#pragma optionNV(fastprecision on)

#ifndef EarlyZ
#define TerrainNormals
#endif
#include "global_ubo.glsl"
#include "terrain.glsl"

layout (constant_id = 0) const bool ENABLE_CLIP_PLANE = false;

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv;

#ifndef EarlyZ
layout(location = 0) out vec3 fragPosWorld;
layout(location = 1) out vec2 fragUV;
layout(location = 2) out flat int index;
layout(location = 3) out flat ivec2 tile;
#endif

layout(set = 2, binding = 0) buffer TileVisibility {
	TerrainTileData tiles[1];
} tileData;

layout(set = 2, binding = 2) uniform sampler2D heightMap;

layout (push_constant) uniform Push {
	vec4 translation;
	vec2 tiles;
	float tileExtent;
	float heightMul;
	uint LOD;
} push;

void main() {
	int tileIndex = int(push.LOD) * int(push.tiles.x * push.tiles.y) + gl_InstanceIndex;
	TerrainTileData tileData = tileData.tiles[tileIndex];
	
	TerrainData data;
	
	data.translation	= push.translation;
	data.tiles			= push.tiles; 
	data.tileExtent	= push.tileExtent;
	data.heightMul		= push.heightMul;
	data.LOD			= push.LOD;
	
	vec3 vertex = getTransformTerrain(data, heightMap, position, uv, tileData);
	
	gl_Position = ubo.projectionView * vec4(vertex, 1.0);
	
#ifndef EarlyZ
	fragPosWorld = vertex;
	fragUV = (uv + tileData.coord) / push.tiles;
	index = tileIndex;
	tile = ivec2(tileData.coord);
#endif
	
	[[flatten]]
	if (ENABLE_CLIP_PLANE) {
		gl_ClipDistance[0] = dot(vec4(vertex, 1.0), ubo.clipPlane);
	}
}