#version 450
#extension GL_GOOGLE_include_directive : enable

#ifdef PARTICLE_DEFERRED
#define DESCRIPTOR_BINDLESS_SET 1
#else
#define DESCRIPTOR_BINDLESS_SET 2
#endif
#include "descriptor_indexing.glsl"

SAMPLER2D(tex2D);
SAMPLER3D(tex3D);


#ifdef PARTICLE_FORWARD
//#include "surface_lighting.glsl"
#ifdef PARTICLE_ADVANCED

//layout (set = 2, binding = 0) uniform ParticleVoxelData {
//	uint PSM_texture;
//	uint VLV_texture;
//};
#endif
#endif

layout(location = 0) in vec2 fragUV;
layout(location = 1) in vec3 fragPosWorld;
layout(location = 2) in vec4 projPrevFrame;
layout(location = 3) in flat vec3 color;
layout(location = 7) in flat float opacity;
layout(location = 8) in flat uint coltex0;

#ifdef PARTICLE_ADVANCED
layout(location = 3) in flat float specular;
layout(location = 4) in flat float shininess;
layout(location = 5) in flat float chrominess;
layout(location = 8) in flat uint coltex1;
layout(location = 9) in flat float texLerp;
#endif

#ifdef PARTICLE_DEFERRED
layout (location = 0) out vec4 GBuffer0; // diffuse + chrominess R8G8B8A8 unorm
#ifdef PARTICLE_ADVANCED
layout (location = 1) out vec2 GBuffer1; // material R8G8 unorm
#endif
layout (location = 2) out vec3 GBuffer2; // emission B10G11R11 ufloat
layout (location = 3) out uint GBuffer3; // normal pack uint32 
layout (location = 4) out uint GBuffer4; // shading model, obj data uint44
layout (location = 5) out vec2 VelocityBuffer;
#endif

const uint PARTICLE_MODEL_UNSHADED = 1;
const uint PARTICLE_MODEL_SHADED = 2;

layout (push_constant) uniform Push {
	uint particleModel;
} push;

void main() {
	vec4 col = texture(tex2D[coltex0], fragUV);
	
#ifdef PARTICLE_DEFERRED
	if (col.a < 0.5) discard;
	uint objectData = 0x10 | 0x20; // dont receive decals and velocity buffer write
	GBuffer4 = objectData | push.particleModel;
	
	const vec2 cc = (vec2(gl_FragCoord.xy) / (ubo.screenSize - vec2(1.0))) * 2.0 - 1.0;
	const vec2 oldCC = projPrevFrame.xy / projPrevFrame.w;
	const vec2 v = 0.5 * (cc - oldCC);
	VelocityBuffer = v * vec2(ubo.velocityBufferTileExtent);
#endif

}