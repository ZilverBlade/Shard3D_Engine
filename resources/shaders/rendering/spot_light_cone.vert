#version 450
#extension GL_EXT_control_flow_attributes : enable

#include "global_ubo.glsl"
#include "scene_buffers.glsl"

layout (location = 0) in vec3 coneVertex;
layout (location = 0) out vec4 clip;
#ifdef VOLUMETRICS_SAMPLING_POSITION
layout (location = 1) out vec3 samplingBack;
#endif

layout (push_constant) uniform Push {
	uint lightIndex;
	float attenuationEpsilon;
} push;

vec3 transformConePoint(SpotLight light, vec3 point, float r, float h, mat3 rotation) {
	vec3 conePosition = point * vec3(r, h, r) + point * vec3(0.015, 0.0, 0.015); // compensate for poly count
	return rotation * conePosition + light.position.xyz;
}
void main() {
	SpotLight light = sceneOmniLights.spotLights[push.lightIndex];
	
	float h = light.radius;
	float theta = acos(light.angle.x);
	float r = tan(theta) * h;
	
	vec3 up = -light.direction.xyz;
	vec3 right = vec3(1.0, 0.0, 0.0);
	//right = abs(dot(up, right)) < 0.01 ? vec3(0.0, 0.0, -1.0) : right; // avoid null result of cross product, altho doesnt seem to be needed?
    vec3 front = normalize(cross(right, up));
	right = normalize(cross(up, front));
	mat3 rotation = mat3(right, up, front);
#ifndef VOLUMETRICS_SAMPLING_POSITION
	vec3
#endif
		samplingBack = transformConePoint(light, coneVertex, r, h, rotation);
	gl_Position = ubo.projectionView * vec4(samplingBack, 1.0);
	clip = gl_Position;
}