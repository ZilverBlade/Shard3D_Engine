#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_control_flow_attributes : enable

#pragma optionNV(fastmath on)
#pragma optionNV(fastprecision on)

#include "global_ubo.glsl"
#include "scene_ssbo.glsl"
#include "surface_lighting.glsl"
#include "depth_to_worldpos.glsl"
#include "shadow_math.glsl"
#include "ssao.glsl"

#include "descriptor_indexing.glsl"
#include "compression.glsl"

#ifdef MULTI_SAMPLED
#error unsupported
#endif

layout (set = 3, binding = 0) uniform sampler2D inputDepth;
layout (set = 3, binding = 1) uniform sampler2D inputGBuffer0; // diffuse RGB + chrominess buffer
layout (set = 3, binding = 2) uniform sampler2D inputGBuffer1; // specular + shininess buffer
layout (set = 3, binding = 3) uniform sampler2D inputGBuffer2; // emission buffer
layout (set = 3, binding = 4) uniform usampler2D inputGBuffer3; // normal buffer
layout (set = 3, binding = 5) uniform usampler2D inputGBuffer4; // shading model
#ifndef NO_SSAO
layout (set = 3, binding = 6) uniform sampler2D inputSSAO;
#endif
#ifndef NO_DECAL_NORMAL
layout (set = 3, binding = 7) uniform sampler2D inputDBuffer2;
#endif

layout (set = 3, binding = 8) uniform sampler2D inputLBuffer0; // diffuse lighting
layout (set = 3, binding = 9) uniform sampler2D inputLBuffer1; // specular lighting

layout (location = 0) in vec2 fragUV;
layout (location = 0) out vec4 outColor;

// shader code
void main(){
	const uint gbuffer4 = textureLod(inputGBuffer4, fragUV, 0.0).x & 0x0F;
	if (gbuffer4 == 0x0F) discard; // clear color = nothing was written for shading
	vec3 opaqueResult = vec3(0.0);
	if (gbuffer4 != SHADING_MODEL_UNSHADED) { // only shade if shading model requires shading
		const float depth = textureLod(inputDepth, fragUV, 0.0).x;
		const uint gbuffer3 = textureLod(inputGBuffer3, fragUV, 0.0).x;
		
		const vec4 gbuffer0 = textureLod(inputGBuffer0, fragUV, 0.0).xyzw;
		const vec2 gbuffer1 = textureLod(inputGBuffer1, fragUV, 0.0).xy;
		
		ShadedPixelInfo pixel;
		pixel.positionWorld = WorldPosFromDepth(fragUV, depth, ubo.invProjection, ubo.invView);
		vec3 Q1 = dFdx(pixel.positionWorld);
		vec3 Q2 = dFdy(pixel.positionWorld);
		pixel.dnormal = normalize(cross(Q1, Q2));
		pixel.diffuse = gbuffer0.rgba;
		vec4 norm = unpack_normal(gbuffer3).xyzw; // W component stores front/backfacing, flip backfacing normals to get correct shading
		pixel.normal = norm.xyz * norm.w;
#ifndef NO_DECAL_NORMAL
		float len2Q2 = dot(Q2, Q2);
		if (len2Q2 > 0.000000001) {
			vec4 dbufferNormalMap = textureLod(inputDBuffer2, fragUV, 0.0).xyzw;
			if (dbufferNormalMap.a != 0) {
				dbufferNormalMap.xyz = dbufferNormalMap.xyz * 2..xxx - 1..xxx;
				vec3 T = Q2 / sqrt(len2Q2).xxx;
				mat3 TBN = mat3(T, cross(pixel.normal, T), pixel.normal);
				pixel.normal = normalize(mix(pixel.normal, TBN * dbufferNormalMap.xyz, dbufferNormalMap.a));
			}
		}
#endif
		pixel.specular = gbuffer1.x;
		pixel.glossiness = gbuffer1.y;
		
		pixel.shadingModel = gbuffer4;
		const float ambientOcclusion = 
#ifndef NO_SSAO
		textureLod(inputSSAO, fragUV, 0.0).r;
#else
		1.0;
#endif
		if (gbuffer4 == SHADING_MODEL_SHADED || gbuffer4 == SHADING_MODEL_EMISSIVE) {
			GlobalFragmentInfo gFragInfo = getGlobalFragmentInfo(pixel.positionWorld, pixel.normal);
			EnvironmentLightingInfo envInfo = getEnvironmentLightingData(gFragInfo, pixel, ambientOcclusion);
			float fresnelFc = basicFresnel(gFragInfo.NdVa); 
			float fresnelTerm = mix(1.0, fresnelFc * (fresnelFc - 0.24), pixel.diffuse.w);
			envInfo.reflection *= fresnelTerm * ambientOcclusion;
			
			vec3 diffuseOutput = textureLod(inputLBuffer0, fragUV, 0.0).xyz + envInfo.ambient * envInfo.ambientOcclusion;
			vec3 specularOutput = textureLod(inputLBuffer1, fragUV, 0.0).xyz;
			
			vec3 result = mix(envInfo.reflection,
		#ifdef S3DSDEF_SHADER_PERMUTATION_DiffuseLightingAffectsPartialReflections
			* mix(dot(diffuseOutput, luma), 1.0, pixel.diffuse.w * pixel.diffuse.w * pixel.diffuse.w)
		#endif
				diffuseOutput * pixel.diffuse.rgb, pixel.diffuse.w) + specularOutput;
				
			opaqueResult = applyExponentialFog(result, pixel.positionWorld, ubo.invView[3].xyz);
		}
	} else {
		opaqueResult = textureLod(inputGBuffer2, fragUV, 0.0).xyz;
	}
	if (gbuffer4 == SHADING_MODEL_EMISSIVE) {
		opaqueResult += textureLod(inputGBuffer2, fragUV, 0.0).xyz;
	}
	
	outColor = vec4(opaqueResult, 1.0);
}