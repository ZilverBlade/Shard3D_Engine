#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_control_flow_attributes : enable

#ifndef S3DSDEF_SHADER_MATERIAL_SURFACE_EARLY_Z
#define ENABLE_VERTEX_NORMALS
#define DESCRIPTOR_BINDLESS_SET 2
#ifdef RIGGED_MESH
#ifdef S3DSDEF_SHADER_MATERIAL_SURFACE_FORWARD
#define RIGGDED_MESH_INSTANCE_DESCRIPTOR 4
#else
#define RIGGDED_MESH_INSTANCE_DESCRIPTOR 3
#endif
#endif
#else
#define DESCRIPTOR_BINDLESS_SET 1
#ifdef RIGGED_MESH
#define RIGGDED_MESH_INSTANCE_DESCRIPTOR 2
#endif
#endif

#define DISABLE_PARTIAL_DERIVATIVES
#include "material.glsl"
#include "global_ubo.glsl"
#include "depth.glsl"

layout (constant_id = 0) const bool ENABLE_CLIP_PLANE = false;


layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv;
#ifdef ENABLE_VERTEX_NORMALS
layout(location = 2) in uint CMPR_normal;
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableVertexTangents
layout(location = 3) in uint CMPR_tangent;
#endif
#endif
#ifdef S3DSDEF_SHADER_MATERIAL_SURFACE_EARLY_Z
layout(location = 0) out vec2 fragUV;
#else
layout(location = 0) out vec3 fragPosWorld;
#ifndef S3DSDEF_SHADER_MATERIAL_SURFACE_EARLY_Z
#ifndef ENABLE_ALPHA_TEST
layout(location = 1) out vec2 fragUV;
#endif
#endif
#ifdef ENABLE_ALPHA_TEST
layout(location = 1) out vec2 fragUV;
#endif

#endif
#ifdef ENABLE_VERTEX_NORMALS
layout(location = 2) out vec3 fragNormalWorld;
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableVertexTangents
layout(location = 3) out vec3 fragTangentWorld;
#endif
#endif
layout (location = 4) out flat uint materialIndex;
#ifdef S3DSDEF_SHADER_MATERIAL_SURFACE_DEFERRED
layout (location = 5) out flat uint receiveDecals;
#ifdef ACTOR_DYNAMIC
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableMotionBlur
layout(location = 6) out vec4 projPrevFrame;
#endif
#endif
#endif

#include "vertex_transform.glsl"

#ifdef ACTOR_DYNAMIC
READONLY_BUFFER DynamicActorData {
	mat4 modelMatrix;
	mat3 normalMatrix;
	mat4 modelMatrixPrevFrame;
} dynamicActorData[];
layout (push_constant) uniform Push {
	uint actorBuffer;
	uint materialID;
#ifdef S3DSDEF_SHADER_MATERIAL_SURFACE_DEFERRED
	uint receiveDecals;
#endif
} push;
#else
layout (push_constant) uniform Push {
	mat4 modelMatrix;
#ifdef ENABLE_VERTEX_NORMALS
	mat3 normalMatrix;
#endif
	uint materialID;
#ifdef S3DSDEF_SHADER_MATERIAL_SURFACE_DEFERRED
	uint receiveDecals;
#endif
} push;
#endif
void main() {
#ifdef ACTOR_DYNAMIC
	mat4 modelMatrix = dynamicActorData[push.actorBuffer].modelMatrix;
#ifdef ENABLE_VERTEX_NORMALS
	mat3 normalMatrix = dynamicActorData[push.actorBuffer].normalMatrix; 
#endif
#else
	mat4 modelMatrix = push.modelMatrix;
#ifdef ENABLE_VERTEX_NORMALS
	mat3 normalMatrix = push.normalMatrix; 
#endif
#endif
	materialIndex = push.materialID;
	
	VertexData vertexData = vertex_Transform();
	
#ifdef S3DSDEF_SHADER_MATERIAL_SURFACE_DEFERRED
	receiveDecals = push.receiveDecals;
#endif
	vec4 worldPos = modelMatrix * vec4(vertexData.position, 1.0);
	gl_Position = ubo.projectionView * vec4(worldPos.xyz, 1.0);
    MaterialData sfMaterial = getMaterialData(materialIndex);
#ifndef S3DSDEF_SHADER_MATERIAL_SURFACE_EARLY_Z
	fragPosWorld = worldPos.xyz;
#endif


#ifndef S3DSDEF_SHADER_MATERIAL_SURFACE_EARLY_Z
#ifndef ENABLE_ALPHA_TEST
	if (matIsTextured(sfMaterial)) {
		fragUV = uv;
	} else {
		fragUV = vec2(0.0);
	}
#endif
#endif
#ifdef ENABLE_ALPHA_TEST
	fragUV = uv;
#endif
	
#ifdef ENABLE_VERTEX_NORMALS
	fragNormalWorld = normalize(normalMatrix * vertexData.normal);
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableVertexTangents
	fragTangentWorld = normalize(normalMatrix * vertexData.tangent);
#endif
#endif
	
	[[flatten]]
	if (ENABLE_CLIP_PLANE) {
		gl_ClipDistance[0] = dot(vec4(worldPos.xyz, 1.0), ubo.clipPlane);
	}

#ifdef S3DSDEF_SHADER_MATERIAL_SURFACE_DEFERRED
#ifdef ACTOR_DYNAMIC
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableMotionBlur
// TODO: require bone transform from previous frame, need a second uniform buffer
	vec4 positionWorldPrevFrame = dynamicActorData[push.actorBuffer].modelMatrixPrevFrame * vec4(vertexData.position, 1.0);
	projPrevFrame = ubo.projectionViewPrevFrame * positionWorldPrevFrame;
#endif
#endif
#endif
}