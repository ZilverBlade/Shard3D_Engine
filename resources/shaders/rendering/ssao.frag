#version 450
#define GBUFFER_DONT_INCLUDE_SAMPLER_IDENTIFIERS
#include "gbuffer_ppfx.glsl"
#include "noise.glsl"
#include "global_ubo.glsl"

layout (set = 1, binding = 0) uniform sampler2D inputDepth;
layout (set = 1, binding = 1) uniform usampler2D inputGBuffer3;
layout (set = 1, binding = 2) uniform sampler2D inputBlueNoise;
layout (set = 1, binding = 3) uniform ssaoSampleBuffer{
	int count;
	vec4 samples[1];
} ssaoSamples;

layout (location = 0) in vec2 fragUV_in;
layout (location = 0) out float ao;

layout (push_constant) uniform SSAOInfo {
	vec2 screenSize;
	vec2 maxTiles;
	int tileIndex;
	int interleaveX;
	int interleaveY;
	float radius;
	float bias;
	float intensity;
	float power; // ssao power and intensity is done in the processing shader
	int kernelQuality;
} push;

const vec2 interleaveMatrix[2][2] = {
	{ { 0.0, 0.0 }, { 0.0, 1.0 } },
	{ { 1.0, 0.0 }, { 1.0, 1.0 } }
};

void main() {
	// discard fragments that arent participating in the interleaved pass
	vec2 interleaveOffset = interleaveMatrix[push.interleaveX.x][push.interleaveY];
	vec2 fragUV = fragUV_in + interleaveOffset / push.screenSize;
	
	uint normalComponentGBUFFER = textureLod(inputGBuffer3, fragUV, 0.0).x;
	if (normalComponentGBUFFER == 0x00000000) discard; // skip fragments with no normal
	
	float depth = textureLod(inputDepth, fragUV, 0.0).r;
    vec3 pixelPos = ViewPosFromDepth(fragUV, depth, ubo.invProjection);

	vec4 normalDat = unpack_normal(normalComponentGBUFFER);
	
	vec3 N = mat3(ubo.view) * vec3(normalDat.xyz * normalDat.w); // W stores sign, flip backface normals
	vec3 V = -normalize(pixelPos); // view space view vector (view * (invView - invView * viewSpacePos) = -viewSpacePos)
	
	vec3 normal = N;

	vec3 randomVec = normalize(vec3(
	    interleaveOffset * 2.0 - 1.0,
	    0.0
	)); 
	
    vec3 tangent   = normalize(randomVec - normal * dot(randomVec, normal));
    vec3 bitangent = cross(normal, tangent);
    mat3 TBN       = mat3(tangent, bitangent, normal); 

    float occlusion = 0.0;
    for (int i = 0; i < ssaoSamples.count; i++) {
        // get sample position
		vec3 ksample = ssaoSamples.samples[i].xyz;
		
        vec3 samplePos = TBN * ksample; // from tangent to view-space, normalization already done on the CPU
        samplePos = pixelPos.xyz + samplePos * push.radius; 
        
        vec4 pxoffset = ubo.projection * vec4(samplePos, 1.0); // from view to clip-space
        pxoffset.xy /= pxoffset.w;             // perspective divide (we also dont need the z component)
        pxoffset.xy = pxoffset.xy * 0.5 + 0.5;  // transform to range 0.0 - 1.0
        
        vec3 sampleDepthFullVector = ViewPosFromDepth(pxoffset.xy, textureLod(inputDepth, pxoffset.xy, 0.0).r, ubo.invProjection);
        float sampleDepth = sampleDepthFullVector.z;
	
        float rangeCheck = smoothstep(0.0, 1.0, push.radius / abs(pixelPos.z - sampleDepth));
		occlusion += float(sampleDepth <= (samplePos.z - push.bias)) * rangeCheck;
    }  
    ao = 1.0 - (occlusion / float(ssaoSamples.count));
}  