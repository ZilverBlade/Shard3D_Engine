#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_control_flow_attributes : enable

#pragma optionNV(fastmath on)
#pragma optionNV(fastprecision on)

#ifdef MULTI_SAMPLED
layout (constant_id = 0) const uint MSAA_SAMPLE_COUNT = 2;
#define MSAA_MAX_SAMPLES 8
#endif

#include "global_ubo.glsl"
#include "scene_ssbo.glsl"
#include "surface_lighting.glsl"
#include "depth_to_worldpos.glsl"
#include "shadow_math.glsl"
#include "ssao.glsl"

#include "descriptor_indexing.glsl"
#include "compression.glsl"

#ifdef MULTI_SAMPLED
#define unresolvableSampler2D sampler2DMS
#define unresolvableUSampler2D usampler2DMS

#define gbufferSampleUnresolvable(sampler, uv, sample_) texelFetch(sampler, ivec2(uv * vec2(textureSize(sampler) - 1)), sample_)
#else
#define unresolvableSampler2D sampler2D
#define unresolvableUSampler2D usampler2D

#define gbufferSampleUnresolvable(sampler, uv, sample_) textureLod(sampler, uv, 0.0)
#endif

layout (set = 3, binding = 0) uniform unresolvableSampler2D inputDepth;
layout (set = 3, binding = 1) uniform sampler2D inputGBuffer0; // diffuse RGB + chrominess buffer
layout (set = 3, binding = 2) uniform sampler2D inputGBuffer1; // specular + shininess buffer
layout (set = 3, binding = 3) uniform sampler2D inputGBuffer2; // emission buffer
layout (set = 3, binding = 4) uniform unresolvableUSampler2D inputGBuffer3; // normal buffer
layout (set = 3, binding = 5) uniform unresolvableUSampler2D inputGBuffer4; // shading model
#ifndef NO_SSAO
layout (set = 3, binding = 6) uniform sampler2D inputSSAO;
#endif
#ifndef NO_DECAL_NORMAL
layout (set = 3, binding = 7) uniform sampler2D inputDBuffer2;
#endif
#ifdef MULTI_SAMPLED
layout (set = 3, binding = 8) uniform unresolvableSampler2D inputGBuffer0MS;
layout (set = 3, binding = 9) uniform unresolvableSampler2D inputGBuffer1MS;
layout (set = 3, binding = 10) uniform unresolvableSampler2D inputGBuffer2MS;
layout (set = 3, binding = 11) uniform usampler2D inputSampleWeightMask;

bool allowUseResolvedGbuffer = false;
#endif

layout (location = 0) in vec2 fragUV;
layout (location = 0) out vec4 outColor;
//#ifdef MULTI_SAMPLED
//#ifndef MSAA_RESOLVED_SHADING
//layout (location = 0, index = 1) out vec4 outCoverageAlpha;
//#endif
//#endif


vec3 calculateSubpixelResult(uint gbuffer4, int subpixel) {
	vec3 lightOutput = vec3(0.0);
	if (gbuffer4 != SHADING_MODEL_UNSHADED) { // only shade if shading model requires shading
		const float depth = gbufferSampleUnresolvable(inputDepth, fragUV, subpixel).x;
		const uint gbuffer3 = gbufferSampleUnresolvable(inputGBuffer3, fragUV, subpixel).x;
		
#ifndef MULTI_SAMPLED
		const vec4 gbuffer0 = textureLod(inputGBuffer0, fragUV, 0.0).xyzw;
		const vec2 gbuffer1 = textureLod(inputGBuffer1, fragUV, 0.0).xy;
#else
		vec4 gbuffer0;
		vec2 gbuffer1;
		
		[[flatten]]
		if (allowUseResolvedGbuffer) {
			gbuffer0 = textureLod(inputGBuffer0, fragUV, 0.0).xyzw;
			gbuffer1 = textureLod(inputGBuffer1, fragUV, 0.0).xy;
		} else {
			gbuffer0 = gbufferSampleUnresolvable(inputGBuffer0MS, fragUV, subpixel).xyzw;
			gbuffer1 = gbufferSampleUnresolvable(inputGBuffer1MS, fragUV, subpixel).xy;
		}  
#endif
		
		ShadedPixelInfo pixel;
		pixel.positionWorld = WorldPosFromDepth(fragUV, depth, ubo.invProjection, ubo.invView);
		pixel.diffuse = gbuffer0.rgba;
		vec3 Q1 = dFdx(pixel.positionWorld);
		vec3 Q2 = dFdy(pixel.positionWorld);
		pixel.dnormal = normalize(cross(Q1, Q2));
		vec4 norm = unpack_normal(gbuffer3).xyzw; // W component stores front/backfacing, flip backfacing normals to get correct shading
		pixel.normal = norm.xyz * norm.w;
#ifndef NO_DECAL_NORMAL
		float len2Q2 = dot(Q2, Q2);
		if (len2Q2 > 0.000000001) {
			vec4 dbufferNormalMap = textureLod(inputDBuffer2, fragUV, 0.0).xyzw;
			if (dbufferNormalMap.a != 0) {
				dbufferNormalMap.xyz = dbufferNormalMap.xyz * 2..xxx - 1..xxx;
				vec3 T = Q2 / sqrt(len2Q2).xxx;
				mat3 TBN = mat3(T, cross(pixel.normal, T), pixel.normal);
				pixel.normal = normalize(mix(pixel.normal, TBN * dbufferNormalMap.xyz, dbufferNormalMap.a));
			}
		}
#endif
		pixel.specular = gbuffer1.x;
		pixel.glossiness = gbuffer1.y;
		
		pixel.shadingModel = gbuffer4;
		const float ambientOcclusion = 
#ifndef NO_SSAO
		textureLod(inputSSAO, fragUV, 0.0).r;
#else
		1.0;
#endif
		if (gbuffer4 == SHADING_MODEL_SHADED || gbuffer4 == SHADING_MODEL_EMISSIVE) {
			lightOutput = calculateLightShaded(pixel, ambientOcclusion);	
		}
	} else {
#ifndef MULTI_SAMPLED
		return textureLod(inputGBuffer2, fragUV, 0.0).xyz;
#else
		[[flatten]]
		if (allowUseResolvedGbuffer) {
			return textureLod(inputGBuffer2, fragUV, 0.0).xyz;
		} else {
			return gbufferSampleUnresolvable(inputGBuffer2MS, fragUV, subpixel).xyz;
		}  
#endif
	}
	vec3 opaqueResult = lightOutput;
	if (gbuffer4 == SHADING_MODEL_EMISSIVE) {
#ifndef MULTI_SAMPLED
		opaqueResult += textureLod(inputGBuffer2, fragUV, 0.0).xyz;
#else
		[[flatten]]
		if (allowUseResolvedGbuffer) {
			opaqueResult += textureLod(inputGBuffer2, fragUV, 0.0).xyz;
		} else {
			opaqueResult += gbufferSampleUnresolvable(inputGBuffer2MS, fragUV, subpixel).xyz;
		}  
#endif
	}
	return opaqueResult;
}

// shader code
void main(){
#ifndef MULTI_SAMPLED
	const uint gbuffer4 = textureLod(inputGBuffer4, fragUV, 0.0).x & 0x0F;
	if (gbuffer4 == 0x0F) discard; // clear color = nothing was written for shading
	vec3 opaqueResult = calculateSubpixelResult(gbuffer4, 0);
	outColor = vec4(opaqueResult, 1.0);
#else
	int shadeSamples = 1;
	float opacity = 0.0;
	float weights[MSAA_MAX_SAMPLES] = { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
	int samples[MSAA_MAX_SAMPLES] = { 0, 0, 0, 0, 0, 0, 0, 0 };
#ifdef MSAA_RESOLVED_SHADING
	allowUseResolvedGbuffer = true;
	opacity = 1.0;
	const uint gbuffer4 = gbufferSampleUnresolvable(inputGBuffer4, fragUV, 0).x & 0x0F;
	if (gbuffer4 == 0x0F) discard; // clear color = nothing was written for shading
	vec3 opaqueResult = calculateSubpixelResult(gbuffer4, 0);
	outColor = vec4(opaqueResult, 1.0);
#else
	const float invSamples = 1.0 / float(MSAA_SAMPLE_COUNT);
	uint pWeights = textureLod(inputSampleWeightMask, fragUV, 0.0).x;
	shadeSamples = 0;
	[[unroll]]
	for (int s = 0; s < MSAA_SAMPLE_COUNT; s++) {
		weights[s] = float((pWeights >> (4 * s)) & 15) * invSamples;
		bool shadeSample = weights[s] > 0.0;
		if (shadeSample) {
			samples[shadeSamples] = s;
			shadeSamples ++;
			opacity += weights[s];
		}
	}
	vec3 opaqueResult = vec3(0.0);
	for (int s = 0; s < shadeSamples; s++) {
		const uint gbuffer4 = gbufferSampleUnresolvable(inputGBuffer4, fragUV, s).x & 0x0F;
		opaqueResult += calculateSubpixelResult(gbuffer4, samples[s]) * weights[s];
	}
	
	outColor = vec4(opaqueResult, opacity);
	// debug edge sampling mask
	//outColor = vec4(float(shadeSamples - 1) / float(MSAA_SAMPLE_COUNT - 1), 0.0, 0.0, 1.0);
#endif
#endif
	
}