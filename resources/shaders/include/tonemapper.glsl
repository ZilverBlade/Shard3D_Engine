#ifndef TONEMAPPER_GLSL
#define TONEMAPPER_GLSL

//const vec3 luminance = vec3( 0.3086, 0.6094, 0.0820 );
const vec3 luminance = vec3( 0.2126, 0.7152, 0.0722 );

// Valid from 1000 to 40000K, (5200K == pure white)
vec3 colorTemperatureToRGB(float temperature){
  // Values from: http://blenderartists.org/forum/showthread.php?270332-OSL-Goodness&p=2268693&viewfull=1#post2268693   
  mat3 m = (temperature <= 6500.0) ? mat3(vec3(0.0, -2902.1955373783176, -8257.7997278925690),
	                                      vec3(0.0, 1669.5803561666639, 2575.2827530017594),
	                                      vec3(1.0, 1.3302673723350029, 1.8993753891711275)) : 
	 								 mat3(vec3(1745.0425298314172, 1216.6168361476490, -8257.7997278925690),
   	                                      vec3(-2666.3474220535695, -2173.1012343082230, 2575.2827530017594),
	                                      vec3(0.55995389139931482, 0.70381203140554553, 1.8993753891711275)); 
  return mix(clamp(vec3(m[0] / (vec3(clamp(temperature, 1000.0, 40000.0)) + m[1]) + m[2]), vec3(0.0), vec3(1.0)), vec3(1.0), smoothstep(1000.0, 0.0, temperature));
}

vec3 temperatureKelvin(vec3 color, float tempK) {
	return color * colorTemperatureToRGB(tempK);
}

vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

// hue range from 0-1 == 0-360
vec3 shiftHueSat(vec3 color, float hue, float saturation) {
	vec3 hsv = rgb2hsv(color);
	return hsv2rgb(vec3(mod(hsv.r + hue, 1.0), hsv.g * saturation, hsv.b));
}

vec3 brightenColor(vec3 color, float brightness) {
	return color + brightness;
}

float ACEScc_MIDGRAY = 0.4135884;

vec3 log10(vec3 x) {
	return log(x) / log(10.0);
}

const float LogC[] =
{
    5.555556, // a
    0.047996, // b
    0.244161, // c
    0.386036, // d
    5.301883, // e
    0.092819  // f
};
vec3 LinearToLogC(vec3 x) {
    return LogC[2] * log10(LogC[0] * x + LogC[1]) + LogC[3];
}

vec3 LogCToLinear(vec3 x) {
    return (pow(vec3(10.0), (x - LogC[3]) / LogC[2]) - LogC[1]) / LogC[0];
}


vec3 contrastColor(vec3 color, float contrast) {
	return LogCToLinear((LinearToLogC(color) - ACEScc_MIDGRAY) * contrast + ACEScc_MIDGRAY);
}

vec3 hable_tone_mapping_partial(vec3 x)
{
    float A = 0.15f;
    float B = 0.50f;
    float C = 0.10f;
    float D = 0.20f;
    float E = 0.02f;
    float F = 0.30f;
    return ((x*(A*x+C*B)+D*E)/(x*(A*x+B)+D*F))-E/F;
}
vec3 hable_tone_mapping(vec3 inColor) { // UC2 tonemapper
    vec3 curr = hable_tone_mapping_partial(inColor);

    vec3 W = vec3(11.2);
    vec3 white_scale = vec3(1.0) / hable_tone_mapping_partial(W);
    return curr * white_scale;
}
vec3 aces_approx(vec3 color) {
    const mat3 x = mat3(0.59719, 0.07600, 0.02840, 0.35458, 0.90834, 0.13383, 0.04823, 0.01566, 0.83777);
    const mat3 y = mat3(1.60475, -0.10208, -0.00327, -0.53108, 1.10813, -0.07276, -0.07367, -0.00605, 1.07602);
    vec3 v = x*color;    
    vec3 a = v*(v + 0.0245786) - 0.000090537;
    vec3 b = v*(0.983729*v + 0.4329510) + 0.238081;
    return clamp(y*(a/b), 0.0, 1.0);	
}

vec3 reinhard_extended(vec3 inColor, float max_white) {
    vec3 numerator = inColor * (1.0 + (inColor * vec3(max_white * max_white)));
    return numerator / (1.0 + inColor);
}
vec3 change_luminance(vec3 c_in, float l_out) {
    float l_in = dot(c_in, luminance);
    return c_in * (l_out / l_in);
}
vec3 reinhard_extended_luminance(vec3 inColor, float max_white) {
    float l_old = dot(inColor, luminance);
    float numerator = l_old * (1.0 + (l_old / (max_white * max_white)));
    float l_new = numerator / (1.0 + l_old);
    return change_luminance(inColor, l_new);
}
vec3 reinhard_exp_tone_mapping(vec3 v, float lim){
 return vec3(lim) - exp(-v);
}
vec3 reinhard_tone_mapping(vec3 v, float lim){
 return v / (v + lim);
}
vec3 reinhard_luma(vec3 v, float lim) {
    float l = dot(v, luminance);
    vec3 tv = reinhard_tone_mapping(v, lim);
	vec3 mv = reinhard_tone_mapping(v * l, lim);
    return mix(mv, tv, tv);
}

#endif