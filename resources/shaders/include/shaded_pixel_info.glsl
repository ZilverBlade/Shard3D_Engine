#ifndef SHADED_PIXEL_INFO_GLSL
#define SHADED_PIXEL_INFO_GLSL

const uint SHADING_MODEL_UNSHADED = 0;
const uint SHADING_MODEL_SHADED = 1;
const uint SHADING_MODEL_EMISSIVE = 2;
const uint SHADING_MODEL_DEFERRED_FOLIAGE = 3;
const uint SHADING_MODEL_DEFERRED_DLCC = 4;
const uint SHADING_MODEL_FORWARD_REFRACTIVE_GLASS = 5;



struct ShadedPixelInfo {
	vec4 diffuse;
	vec3 positionWorld;
	vec3 normal;
	vec3 dnormal;
	float specular;
	float glossiness;
	uint shadingModel;
};

// im probably yeeting this away at some point
struct DLCCPixelInfo {
	float metallicness;
	float glossiness;
	vec3 microFlakeNormal;
	float microflakePerturbationA;
	float normalPerturbation;
	vec3 paintLayer0;
	vec3 paintLayer1;
};

#endif