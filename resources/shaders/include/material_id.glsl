#ifndef MATERIAL_SSBO_GLSL
#define MATERIAL_SSBO_GLSL
#include "descriptor_indexing.glsl"
struct MaterialTextureData {
	uint emissiveTex_id;

	uint diffuseTex_id;
	
	uint specularTex_id;
	uint glossinessTex_id;
	uint reflectivityTex_id;
	
	uint normalTex_id;

	uint maskTex_id;
	uint opacityTex_id;
};

struct MaterialFactorData {
	vec2 texCoordOffset;
	vec2 texCoordMultiplier;
	vec4 emission;
	vec4 diffuse;
	float normalStrength;
	float specular;
	float glossiness;
	float reflectivity;
	// optional by material setting
	float opacity; 
	float forwardBlendingMulBias;
	float sfExt_ForwardRefractive_IOR;
	float sfExt_ForwardRefractive_Dispersion;
};

struct MaterialData {
	uint textureFlags;
	uint surfaceShadingModel;
	uint dbufferModel;
	uint align4;
	MaterialTextureData textureData;
	MaterialFactorData factorData;
};

READONLY_BUFFER _MaterialID {
	MaterialData data;
} matID[];

MaterialData getMaterialData(uint mid) {
	return matID[mid].data;
}
#endif