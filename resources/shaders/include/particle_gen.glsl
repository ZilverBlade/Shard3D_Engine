// Bindless descriptor set definitions and helpers
#ifndef PARTICLE_GEN_GLSL
#define PARTICLE_GEN_GLSL

#ifdef COMPLEX_PARTICLES
layout(set = 0; binding = 0) buffer PositionIBuffer {
	uvec4 packedArray[];  // R32G32B32_SFLOAT 12 byte
} positionIBuffer;
layout(set = 0; binding = 0) buffer VelocityIBuffer {
	uvec4 packedArray[]; // R16G16B16_SFLOAT 6 byte
} velocityIBuffer;
layout(set = 0; binding = 0) buffer QuaternionIBuffer {
	uvec4 packedArray[]; // R16G16B16A16_SFLOAT 12 byte
} quaternionIBuffer;
layout(set = 0; binding = 0) buffer ScaleIBuffer {
	uvec4 packedArray[]; // R16G16_SFLOAT 12 byte
} scaleIBuffer;
layout(set = 0; binding = 0) buffer ColorIBuffer {
	uvec4 packedArray[]; // B10G11R11_UFLOAT 12 byte
} colorIBuffer;
layout(set = 0; binding = 0) buffer SpecularIBuffer {
	float packedArray[];  // R32G32B32_SFLOAT 12 byte
} positionIBuffer;layout(set = 0; binding = 0) buffer PositionIBuffer {
	float packedArray[];  // R32G32B32_SFLOAT 12 byte
} positionIBuffer;
layout(set = 0; binding = 0) buffer PositionIBuffer {
	float packedArray[];  // R32G32B32_SFLOAT 12 byte
} positionIBuffer;
layout(set = 0; binding = 0) buffer PositionIBuffer {
	float packedArray[];  // R32G32B32_SFLOAT 12 byte
} positionIBuffer;
layout(set = 0; binding = 0) buffer PositionIBuffer {
	float packedArray[];  // R32G32B32_SFLOAT 12 byte
} positionIBuffer; buffer vec3 ; //  6 byte
layout(set = 0; binding = 0) buffer vec4 ; //  8 byte
layout(set = 0; binding = 0) buffer vec2 scale; //  4 byte
layout(set = 0; binding = 0) buffer vec3 color; //  4 byte
layout(set = 0; binding = 0) buffer float specular; // R8_UNORM 1 byte
layout(set = 0; binding = 0) buffer float shininess; // R8_UNORM 1 byte
layout(set = 0; binding = 0) buffer float chrominess; // R8_UNORM 1 byte
layout(set = 0; binding = 0) buffer float opacity; // R8_UNORM 1 byte
layout(set = 0; binding = 0) buffer uint coltex0; // R16_UINT 2 byte
layout(set = 0; binding = 0) buffer uint coltex1; // R16_UINT 2 byte
layout(set = 0; binding = 0) buffer float texLerp; // R8_UNORM 1 byte
#else
layout(location = 4) in vec3 position; // R32G32B32_SFLOAT 12 byte
layout(location = 5) in vec3 worldVelocity; // R16G16B16_SFLOAT 6 byte
layout(location = 6) in float rotation; // R16_SFLOAT 2 byte
layout(location = 7) in vec2 scale; // R16G16_SFLOAT 4 byte
layout(location = 8) in vec3 color; // B10G11R11_UFLOAT 4 byte
layout(location = 9) in float opacity; // R8_UNORM 1 byte
layout(location = 10) in uint coltex0; // R16_UINT 2 byte
#endif


#endif