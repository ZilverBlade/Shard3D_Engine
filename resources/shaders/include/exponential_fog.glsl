#ifndef EXPONENTIAL_FOG_GLSL
#define EXPONENTIAL_FOG_GLSL
#include "scene_buffers.glsl"

vec3 applyFog( ExponentialFog fog,
                vec3  rgb,      // original color of the pixel
               float dist, // camera to point distance
               vec3  rayOri,   // camera position
               vec3  rayDir   // camera to point vector
            )  
{
    vec3 fogColor = fog.color.xyz;
    for (int i = 0; i < sceneEnvironment.numDirectionalLights; i++){
        float sunAmount = max(dot(normalize(rayDir), normalize(sceneEnvironment.directionalLights[i].direction.xyz)), 0.0);
        
        fogColor = mix(fogColor, sceneEnvironment.directionalLights[i].color.xyz * vec3(1.0, 0.9834, 0.8843), // yellowish tint
                           clamp(pow(sunAmount, fog.sunScattPow) * sceneEnvironment.directionalLights[i].color.w, 0.0, 1.0));
    }
    
    float amt = exp(-rayOri.y*fog.density) * (1.0 - clamp(exp2(-fog.density * dist), 0.0, 1.0));
    return mix(rgb, fogColor, clamp(amt, 0.0, 1.0));
}

// shader code
vec3 applyExponentialFog(vec3 srcColor, vec3 worldPos, vec3 cameraPos){
	float dist = distance(cameraPos, worldPos);

    vec3 fogColor = vec3(0.0) + float(int(bool(sceneEnvironment.numExponentialFogs == 0))) * srcColor; // input color if no fog
    for (int i = 0; i < sceneEnvironment.numExponentialFogs; i++){
         vec3 v = (cameraPos - worldPos);
        // v.y = -abs(v.y) * float(int(bool(sceneEnvironment.exponentialFogs[i].updownbias == 2))) + -v.y * float(int(bool(sceneEnvironment.exponentialFogs[i].updownbias == 1))) + v.y * float(int(bool(sceneEnvironment.exponentialFogs[i].updownbias == 0))) ;
         fogColor+=applyFog(sceneEnvironment.exponentialFogs[i], srcColor, dist, cameraPos, v);
    }
	return fogColor;
}
#endif