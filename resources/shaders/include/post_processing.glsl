layout(local_size_x = 16, local_size_y = 16) in;

layout (set = 0, binding = 0, rgba16f) uniform image2D postProcessImage;

#define scene_load() imageLoad(postProcessImage, ivec2(gl_GlobalInvocationID.xy)).rgb; vec2 gbf_thispposize = imageSize(postProcessImage)

#define scene_store(value) imageStore(postProcessImage, ivec2(gl_GlobalInvocationID.xy), vec4(value, 1.0));
