#ifndef WEIGHTED_BLENDING_GLSL
#define WEIGHTED_BLENDING_GLSL

float getAlphaAccumDepthBlendWeight(vec3 color, float opacity) { // weight depending on depth
	float ft = min(1.0, opacity * 10.0) + 0.01;
#ifndef REVERSE_DEPTH_BUFFER
	float ftfc = 1.0 - sqrt(gl_FragCoord.z);
#else
	float ftfc = sqrt(gl_FragCoord.z) + 0.05;
#endif
    return clamp((ft * ft * ft) * 1e9 * (ftfc * ftfc * ftfc), 1e-2, 3e2);
}

float getAlphaAccumColorBlendWeight(vec3 color, float opacity) { // weight depending on color 
	return max(min(1.0, max(max(color.r, color.g), color.b) * opacity), opacity) * clamp(0.03 / (1e-5 + pow(gl_FragCoord.z / 200, 4.0)), 1e-2, 3e3);
}

vec4 getAlphaAccum(vec3 color, float opacity) {
	// blend func: VK_BLEND_FACTOR_ONE, VK_BLEND_FACTOR_ONE
	// switch to pre-multiplied alpha and weight
	vec4 accum = vec4(color.rgb * opacity, opacity) * getAlphaAccumDepthBlendWeight(color, opacity);
	return accum;
}

#endif