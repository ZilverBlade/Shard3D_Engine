#ifndef TERRAIN_GLSL
#define TERRAIN_GLSL

#extension GL_EXT_control_flow_attributes : enable

struct TerrainData {
	vec4 translation;
	vec2 tiles;
	float tileExtent;
	float heightMul;
	uint LOD;
};

struct TerrainTileData {
	vec2 coord;
};

vec3 sampleTerrainNormal(TerrainData data, sampler2D heightMapSampler, vec3 position, vec2 uv, TerrainTileData tile) {
	vec2 texelSize = 1.0 / textureSize(heightMapSampler, 0).xy;
	vec2 neighbors[4] = {
		{ 0.0, 1.0 }, // N
		{ 1.0, 0.0 }, // E
		{ 0.0, -1.0 }, // S
		{ -1.0, 0.0 } // W
	};
	float heights[4];
	[[unroll]]
	for (int i = 0; i < 4; i++) {
		heights[i] = texture(heightMapSampler, uv + neighbors[i] * texelSize).x;
	}
	
	vec2 dist = 2.0 * data.tileExtent * data.tiles * texelSize;
	
	vec3 posC = vec3(position.x, position.y, position.z);
	
	vec3 posN = vec3(position.x, 				heights[0], position.z + dist.y);
	vec3 posE = vec3(position.x + dist.x, 	heights[1], position.z);
	vec3 posS = vec3(position.x, 				heights[2], position.z - dist.y);
	vec3 posW = vec3(position.x - dist.x, 	heights[3], position.z);
	
	vec3 edge0 = posS - posC;
	vec3 edge1 = posN - posC;
	vec3 edge2 = posW - posC;
	vec3 edge3 = posE - posC;
	
	return normalize(cross(edge0, edge2) + cross(edge1, edge3));
}

float sampleTextureHeightAvg(TerrainData data, sampler2D heightMapSampler, vec3 position, vec2 uv, TerrainTileData tile) {
	float lod = any(equal(abs(position.xz), 1..xx)) ? 0.0 : float(data.LOD);
	vec2 texelSize = 1.0 / textureSize(heightMapSampler, 0).xy;
	
	vec2 currUV = (uv + tile.coord) / data.tiles;
	return textureLod(heightMapSampler, currUV, lod).x;
}

struct TerrainVertex {
	vec3 position;
	vec3 normal;
};

vec3 getTransformTerrain(TerrainData data, sampler2D heightMap, vec3 position, vec2 uv, TerrainTileData tile) {
	float height = sampleTextureHeightAvg(data, heightMap, position, uv, tile); 
	vec3 positionWorld = vec3(position.x, height, position.z);
	positionWorld.xz += tile.coord * 2..xx - (data.tiles - 1..xx);
	positionWorld.xz *= data.tileExtent;
	return positionWorld;
}

#endif