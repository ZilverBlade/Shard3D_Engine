#ifndef NORMAL_MAPPING_GLSL
#define NORMAL_MAPPING_GLSL

// Calculate new normals from vertex normal and tangent (more accurate, more VRAM usage, 24 bytes/vtx)
vec3 getNormalFromMap(vec3 tgNormal, vec3 N, vec3 T) {	
	mat3 TBN = mat3(T, cross(N, T), N);
	
	return normalize(TBN * tgNormal);
}

// Calculate new normals from vertex normal, uv, and position (less accurate, less VRAM usage, 20 bytes/vtx)
vec3 getNormalFromMapD(vec3 tgNormal, vec2 fragTexCoord, vec3 fragNormal, vec3 fragPos) {
    vec3 N = normalize(fragNormal);
#if 1
    vec3 Q1  = dFdx(fragPos);
    vec3 Q2  = dFdy(fragPos);
    vec2 st1 = dFdx(fragTexCoord);
    vec2 st2 = dFdy(fragTexCoord);
	
    vec3 T = normalize(Q1*st2.t - Q2*st1.t);
	mat3 TBN = mat3(T, cross(N, T), N);
#else
	vec3 dp1 = dFdx( fragPos ); 
	vec3 dp2 = dFdy( fragPos );
	vec2 duv1 = dFdx( fragTexCoord ); 
	vec2 duv2 = dFdy( fragTexCoord );   // solve the linear system 
	vec3 dp2perp = cross( dp2, N ); 
	vec3 dp1perp = cross( N, dp1 ); 
	vec3 T = dp2perp * duv1.x + dp1perp * duv2.x; 
	vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;   // construct a scale-invariant frame float 
	float invmax = inversesqrt( max( dot(T,T), dot(B,B) ) );
	mat3 TBN = mat3( T * invmax, B * invmax, N );
#endif
	return normalize(TBN * tgNormal);
}
#endif