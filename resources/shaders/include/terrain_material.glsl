#ifndef TERRAIN_MATERIAL_GLSL
#define TERRAIN_MATERIAL_GLSL

#include "global_ubo.glsl"
#include "descriptor_indexing.glsl"
#include "normal_mapping.glsl"

SAMPLER2D_TEXTURE(texID);

struct TerrainMaterial {
	uint diffuseTexture;
	uint specularTexture;
	uint glossinessTexture;
	uint reflectivityTexture;
	uint heightTexture;
	float bumpStrength;
	float parallaxDepth;
	float specular;
	vec2 uvScale;
	float glossiness;
	float reflectivity;
	vec3 diffuse;
};

struct TerrainMaterialBuffer {
	uint maxMaterials;
	TerrainMaterial materials[8];
};

struct TerrainMaterialResult {
	vec3 diffuse;
	float specular;
	float glossiness;
	float reflectivity;
	vec3 normalVec;
};

struct TerrainSamplingCoordinates {
	vec2 xPlane;
	vec2 yPlane;
	vec2 zPlane;
};

TerrainMaterialResult getDefaultTerrainMaterial() {
	TerrainMaterialResult data;
	data.diffuse = vec3(0.0, 0.0, 0.0);
	data.specular = 0.0;
	data.glossiness = 0.0;
	data.reflectivity = 0.0;
	data.normalVec = vec3(0.0, 0.0, 0.0);
	return data;
}

vec3 getTriplanarBlendVector(vec3 normal) {
    vec3 blending = abs(normal);
	
	blending = max(pow(blending, 4..xxx) - 0.05.xxx, 0..xxx);
	blending /= dot(blending, 1..xxx);
	
    return blending;
}

vec3 bumpToNormal(float hC, float hN, float hE, float multiplier) {
	float dx = (hE - hC) * multiplier;
	float dy = (hN - hC) * multiplier;
	float dz = sqrt(max(1.0 - dx*dx - dy*dy, 0.0));	
	return normalize(vec3(dx,dy,dz));
}

float textureTriplanarFloat(uint tex, TerrainSamplingCoordinates texCoords, vec3 blending) {
    float xaxis = float(0);
    float yaxis = float(0);
    float zaxis = float(0);
	if (blending.x > 0.0) {
		xaxis = texture(texID[tex], texCoords.xPlane).r;
	}
	if (blending.y > 0.0) {
		yaxis = texture(texID[tex], texCoords.yPlane).r;
	}
	if (blending.z > 0.0) {
		zaxis = texture(texID[tex], texCoords.zPlane).r;
	}
    return xaxis * blending.x + yaxis * blending.y + zaxis * blending.z;
}
vec3 textureTriplanarVec3(uint tex, TerrainSamplingCoordinates texCoords, vec3 blending) {
    vec3 xaxis = vec3(0);
    vec3 yaxis = vec3(0);
    vec3 zaxis = vec3(0);
	if (blending.x > 0.0) {
		xaxis = texture(texID[tex], texCoords.xPlane).rgb;
	}
	if (blending.y > 0.0) {
		yaxis = texture(texID[tex], texCoords.yPlane).rgb;
	}
	if (blending.z > 0.0) {
		zaxis = texture(texID[tex], texCoords.zPlane).rgb;
	}
    return xaxis * blending.x + yaxis * blending.y + zaxis * blending.z;
}

vec3 textureNormalTriplanar(vec3 blending, vec3 normal, vec3 bumpAxes, vec3 bumpAxesN, vec3 bumpAxesE, float strength) {
    vec3 xaxis = vec3(0);
    vec3 yaxis = vec3(0);
    vec3 zaxis = vec3(0);
	vec3 tangent = abs(normal.x) < 0.99 ? vec3(1.0, 0.0, 0.0) : vec3(0.0, 1.0, 0.0);	
	vec3 bitangent = cross(normal, tangent);
	tangent = cross(bitangent, normal);
	mat3 TBN = mat3(tangent, bitangent, normal);
	if (blending.x > 0.0) {
		xaxis = TBN *  bumpToNormal(bumpAxes.x, bumpAxesN.x, bumpAxesE.x, strength);
	}
	if (blending.y > 0.0) {
		yaxis = TBN * bumpToNormal(bumpAxes.y, bumpAxesN.y, bumpAxesE.y, strength);
	}
	if (blending.z > 0.0) {
		zaxis = TBN * bumpToNormal(bumpAxes.z, bumpAxesN.z, bumpAxesE.z, strength);
	}
	
    return normalize(
		xaxis * blending.x +
		yaxis * blending.y +
		zaxis * blending.z 
    );
}

vec2 parallaxMap(uint heightTexture, vec2 inCoords, vec3 viewDir, float strength) {
	const float minLayers = 4.0;
	const float maxLayers = 16.0;
	
	float layers = mix(minLayers, maxLayers, min(abs(dot(vec3(0.0, 0.0, 1.0), viewDir)), 1.0)); //mix(minLayers, maxLayers, strength * 8.0);
    float layerDepth = 1.0 / layers;
    float currentLayerDepth = 1.0;
	vec2 dUV = (viewDir.xy * strength) / (layers);
	vec2 cUV = inCoords;
	float depth = texture(texID[heightTexture], cUV).r;
	float previousDepth = depth;
	while(currentLayerDepth > depth) {
	//for (int i = 0; i < layers; i++) {
		currentLayerDepth -= layerDepth;
		cUV -= dUV;
		previousDepth = depth;
		depth = texture(texID[heightTexture], cUV).r;
		//if (depth < currentLayerDepth) {
        //    cUV += dUV;
        //    currentLayerDepth -= layerDepth;
        //    depth = 1.0 - texture(texID[heightTexture], cUV).r - currentLayerDepth + layerDepth;
		//	break;
		//}
	}

	vec2 pUV = cUV + dUV;
	
	float depthDiff = depth - currentLayerDepth;
	float lastDepthDiff = previousDepth - currentLayerDepth - layerDepth;
	float gradient	= depthDiff / (depthDiff - lastDepthDiff);

	return mix(cUV, pUV, gradient);
    //for (int i = 0; i < layers * 0.5; i++) {
    //    dUV *= 0.5;
    //    layerDepth *= 0.5;
	//	depth = 1.0 - texture(texID[heightTexture], cUV).r;
	//
	//	if (depth > currentLayerDepth) {
	//		currentLayerDepth += layerDepth;
	//	    cUV -= dUV;
	//	} else {
    //        currentLayerDepth -= layerDepth;
	//	    cUV += dUV;
    //    }
	//}
	//
	// return cUV;
}

vec2 parallaxMapSimple(uint heightTexture, vec2 inCoords, vec3 viewDir, float strength) {
	float depth = (1.0 - texture(texID[heightTexture], inCoords).r) * strength;
	vec2 p = viewDir.xy / viewDir.z * depth;
	
	return inCoords - p;
}

TerrainSamplingCoordinates textureParallaxMap(uint heightTexture, vec3 position, vec3 blending, vec3 normal, vec2 texCoordScale, float weight, float strength) {
	TerrainSamplingCoordinates coords;
    coords.xPlane = position.yz * texCoordScale;
    coords.yPlane = position.xz * texCoordScale;
    coords.zPlane = position.xy * texCoordScale;
	if (strength == 0.0 || heightTexture == 0) {	
		return coords;
	}
	// todo fix: this branch still causes slow downs for some reason, even if not used
	
	[[flatten]]
	if (enableParallaxMapping) {
		vec3 tangent = normal.x < 0.99 ? vec3(1.0, 0.0, 0.0) : vec3(0.0, 1.0, 0.0);	
		vec3 bitangent = cross(normal, tangent);
		tangent = cross(bitangent, normal);
		mat3 TBN = mat3(tangent, bitangent, normal);
		
		vec3 tanEye = TBN * ubo.invView[3].xyz;
		vec3 tanWorld = TBN * position;
		vec3 vdir = normalize(tanEye - tanWorld);
		if (blending.x > 0.0) {
			coords.xPlane = parallaxMap(heightTexture, coords.xPlane, vdir, weight * strength);
		}
		if (blending.y > 0.0) {
			coords.yPlane = parallaxMap(heightTexture, coords.yPlane, vdir, weight * strength);
		}
		if (blending.z > 0.0) {                   
			coords.zPlane = parallaxMap(heightTexture, coords.zPlane, vdir, weight * strength);
		}
	}
    return coords;
}

TerrainMaterialResult getTerrainMaterialData(vec3 position, vec3 normal, TerrainMaterial terrainMaterialArray[8], float weightArray[maxMaterialCount]) {
	const float HEIGHT_DEPTH_BLEND = 0.2;
	TerrainMaterialResult returnValue = getDefaultTerrainMaterial();
	vec3 blend = getTriplanarBlendVector(normal);
	TerrainSamplingCoordinates sampleCoordsArray[maxMaterialCount];
	
#ifndef TERRAIN_MATERIAL_ONLY_DIFFUSE
	float totalWSum = 0.0;
	float maxHeight = 0.0;
	vec3 heightArray[maxMaterialCount];
	vec3 heightNArray[maxMaterialCount];
	vec3 heightEArray[maxMaterialCount];	
	
	vec2 neighbors[2] = {
		{ 0.0, 1.0 },
		{ 1.0, 0.0 }
	};
#endif
	[[unroll]]
	for (int i = 0; i < maxMaterialCount; i++) {
		float w = weightArray[i];
		if (w > 0.0) {
			TerrainSamplingCoordinates sampleCoords = textureParallaxMap(terrainMaterialArray[i].heightTexture, position, blend, normal, terrainMaterialArray[i].uvScale, w, terrainMaterialArray[i].parallaxDepth);
			sampleCoordsArray[i] = sampleCoords;
#ifndef TERRAIN_MATERIAL_ONLY_DIFFUSE
			heightArray[i] = 0..xxx;
			if (terrainMaterialArray[i].heightTexture != 0) {
				vec2 invTs = 1.0 / textureSize(texID[terrainMaterialArray[i].heightTexture], 0).xy;
				if (blend.x > 0.0) {
					heightArray[i].x = texture(texID[terrainMaterialArray[i].heightTexture], sampleCoords.xPlane).r;
					if (terrainMaterialArray[i].bumpStrength > 0.0) {
						heightNArray[i].x = texture(texID[terrainMaterialArray[i].heightTexture], sampleCoords.xPlane + neighbors[0] * invTs.x).r;
						heightEArray[i].x = texture(texID[terrainMaterialArray[i].heightTexture], sampleCoords.xPlane + neighbors[1] * invTs.y).r;
					}
				}
				if (blend.y > 0.0) {
					heightArray[i].y = texture(texID[terrainMaterialArray[i].heightTexture], sampleCoords.yPlane).r;
					if (terrainMaterialArray[i].bumpStrength > 0.0) {
						heightNArray[i].y = texture(texID[terrainMaterialArray[i].heightTexture], sampleCoords.yPlane + neighbors[0] * invTs.x).r;
						heightEArray[i].y = texture(texID[terrainMaterialArray[i].heightTexture], sampleCoords.yPlane + neighbors[1] * invTs.y).r;
					}
				}
				if (blend.z > 0.0) {
					heightArray[i].z = texture(texID[terrainMaterialArray[i].heightTexture], sampleCoords.zPlane).r;
					if (terrainMaterialArray[i].bumpStrength > 0.0) {
						heightNArray[i].z = texture(texID[terrainMaterialArray[i].heightTexture], sampleCoords.zPlane + neighbors[0] * invTs.x).r;
						heightEArray[i].z = texture(texID[terrainMaterialArray[i].heightTexture], sampleCoords.zPlane + neighbors[1] * invTs.y).r;
					}
				}
			}
			float axisBlend = dot(heightArray[i], blend);
			weightArray[i] = axisBlend + w;
			maxHeight = max(maxHeight, weightArray[i]);
#endif
		}
	}
#ifndef TERRAIN_MATERIAL_ONLY_DIFFUSE
	maxHeight-=HEIGHT_DEPTH_BLEND;
	[[unroll]]
	for (int i = 0; i < 4; i++) {
		weightArray[i] = max(weightArray[i] - maxHeight, 0.0);
		totalWSum+=weightArray[i];
	}
	[[unroll]]
	for (int i = 0; i < 4; i++) {
		weightArray[i] /= totalWSum;
	}
#endif
	[[unroll]]
	for (int i = 0; i < 4; i++) {
		//int i = indices[m];
		float w = weightArray[i];
		if (w > 0.0) {
			TerrainSamplingCoordinates sampleCoords = sampleCoordsArray[i];
			vec3 diffuse = terrainMaterialArray[i].diffuse;
#ifndef TERRAIN_MATERIAL_ONLY_DIFFUSE
			float specular = terrainMaterialArray[i].specular;
			float glossiness = terrainMaterialArray[i].glossiness;
			float reflectivity = terrainMaterialArray[i].reflectivity;
			vec3 normalVec = 0..xxx;
#endif
			if (terrainMaterialArray[i].diffuseTexture != 0) {
				diffuse *= textureTriplanarVec3(terrainMaterialArray[i].diffuseTexture, sampleCoords, blend);
			}
#ifndef TERRAIN_MATERIAL_ONLY_DIFFUSE
			if (terrainMaterialArray[i].specularTexture != 0) {
				specular *= textureTriplanarFloat(terrainMaterialArray[i].specularTexture, sampleCoords, blend);
			}
			if (terrainMaterialArray[i].glossinessTexture != 0) {
				glossiness *= textureTriplanarFloat(terrainMaterialArray[i].glossinessTexture, sampleCoords, blend);
			}
			if (terrainMaterialArray[i].reflectivityTexture != 0) {
				reflectivity *= textureTriplanarFloat(terrainMaterialArray[i].reflectivityTexture, sampleCoords, blend);
			}
			if (terrainMaterialArray[i].bumpStrength > 0.0 && terrainMaterialArray[i].heightTexture != 0) {
				normalVec = textureNormalTriplanar(blend, normal, heightArray[i], heightNArray[i], heightEArray[i], terrainMaterialArray[i].bumpStrength);
			} else {
				normalVec = normal;
			}
#endif
			returnValue.diffuse += diffuse * w;
#ifndef TERRAIN_MATERIAL_ONLY_DIFFUSE
			returnValue.specular += specular * w;
			returnValue.glossiness += glossiness * w;
			returnValue.reflectivity += reflectivity * w;
			returnValue.normalVec += normalVec * w;
#endif	
		}
	}
	returnValue.normalVec = normalize(returnValue.normalVec);
	return returnValue;
}
#endif