#ifndef SHADOW_MATH_GLSL
#define SHADOW_MATH_GLSL
#include "scene_ssbo.glsl"
#include "descriptor_indexing.glsl"
#include "noise.glsl"
#include "paraboloid.glsl"
#include "math.glsl"
#include "depth_to_worldpos.glsl"

layout(set = DESCRIPTOR_BINDLESS_SET, binding = S3DSDEF_COMBINED_SAMPLER_BINDING) uniform sampler2DShadow pcfShadowMaps[]; //hw accelerated 2x2 pcf
layout(set = DESCRIPTOR_BINDLESS_SET, binding = S3DSDEF_COMBINED_SAMPLER_BINDING) uniform sampler2D colorfulShadowMaps[]; // variance shadows, translucent col, translucent reveal
layout(set = DESCRIPTOR_BINDLESS_SET, binding = S3DSDEF_COMBINED_SAMPLER_BINDING) uniform samplerCubeShadow pcfShadowCubes[];
layout(set = DESCRIPTOR_BINDLESS_SET, binding = S3DSDEF_COMBINED_SAMPLER_BINDING) uniform samplerCube colorfulShadowCubes[];
layout(set = DESCRIPTOR_BINDLESS_SET, binding = S3DSDEF_COMBINED_SAMPLER_BINDING) uniform sampler2DArrayShadow pcfShadowArrays[];
layout(set = DESCRIPTOR_BINDLESS_SET, binding = S3DSDEF_COMBINED_SAMPLER_BINDING) uniform sampler2DArray colorfulShadowArrays[];

// helpers
struct Variance {
	float p;
	float pMax;
};
const vec2 poissonDisk[16] = vec2[]( 
   vec2( 1.0, 0.0 ), 
   vec2( -0.8, -0.2 ), 
   vec2( 0.0, 0.8 ), 
   vec2( 0.0, -1.0 ), 
   vec2( -0.91588581, 0.45771432 ), 
   vec2( -0.81544232, -0.87912464 ), 
   vec2( -0.38277543, 0.27676845 ), 
   vec2( -0.94201624, -0.39906216 ), 
   vec2( 0.97484398, 0.75648379 ), 
   vec2( 0.44323325, -0.97511554 ), 
   vec2( 0.53742981, -0.47373420 ), 
   vec2( -0.26496911, -0.41893023 ), 
   vec2( -0.24188840, 0.99706507 ), 
   vec2( -0.81409955, 0.91437590 ), 
   vec2( 0.19984126, 0.78641367 ), 
   vec2( 0.14383161, -0.14100790 ) 
);
const vec3 poissonOmniDisk[16] = vec3[]( 
   vec3( -0.94201624, -0.39906216, 0.44323325 ), 
   vec3( 0.94558609, -0.76890725, -0.87373420 ), 
   vec3( -0.094184101, 0.92938870, 0.53742981 ), 
   vec3( 0.34495938, 0.29387760, -0.26496911 ), 
   vec3( -0.91588581, 0.45771432, 0.99706507 ), 
   vec3( -0.81544232, -0.87912464, 0.78641367 ), 
   vec3( -0.38277543, 0.27676845,  0.14383161), 
   vec3( 0.97484398, 0.75648379, -0.81409955 ),
   vec3( 0.97484398, -0.81409955, 0.75648379 ),
   vec3( 0.53742981, -0.47373420,  0.97484398 ), 
   vec3( -0.26496911, -0.41893023 , -0.15623645) , 
   vec3( 0.79197514, 0.19090188, 0.62197514 ), 
   vec3( -0.24188840, -0.99706507, 0.592173489 ),
   vec3( -0.6197514, 0.49090188, 0.32197514 ),
   vec3( 0.89984126, 0.78641367, 0.19984126 ), 
   vec3( 0.14383161, -0.14100790, 0.34495938 ) 
);

const float shadowMapCheckThresholdConstant = 1.0 - 2.0 / 32.0;

// shadow code

//vec2 getEVSMExponents(float positiveExponent, float negativeExponent) {
//    const float maxExponent = 5.54;
//
//    vec2 lightSpaceExponents = vec2(positiveExponent, negativeExponent);
//
//    // Clamp to maximum range of  prevent overflow/underflow
//    return min(lightSpaceExponents, vec2(maxExponent));
//}

//float getExponentialVarianceShadow(vec3 fragPosWorld, ShadowMapInfo shadowInfo) {
//	vec4 fragPosLightSpace = shadowInfo.lightSpaceMatrix * vec4(fragPosWorld, 1.0);
//    // perform perspective divide
//    vec3 clip = fragPosLightSpace.xyz / fragPosLightSpace.w;
//	
//	// transform to [0,1] range
//    clip.xy = clip.xy * 0.5 + 0.5;
//	bvec2 outOfBoundsA = greaterThan(clip.xy, vec2(1.0, 1.0));
//	bvec2 outOfBoundsB = lessThan(clip.xy, vec2(0.0, 0.0));
//	if (outOfBoundsA.x || outOfBoundsB.y || outOfBoundsB.x || outOfBoundsB.y) return 1.0;
//	
//	
//    float wdepth = 2.0f * clip.z - 1.0f;
//    float pos =  exp( 4.0 * wdepth);
//    float neg = -exp(-4.0 * wdepth);
//    vec2 warpedDepth = vec2(pos, neg);
//	
//    vec2 moments = textureLod(colorfulShadowMaps[shadowInfo.shadowMap_ID], clip.xy, 0.0).xy;
//
//    vec2 depthScale = 0.5 * 0.01 * vec2(4.0, 4.0) * warpedDepth;
//    vec2 minVariance = depthScale * depthScale;
//
//    // Compute variance
//    float variance = moments.y - (moments.x * moments.x);
//	variance = max(variance, minVariance.x);
//	
//    // Compute probabilistic upper bound
//    float d = warpedDepth.x - moments.x;
//    float pMax = variance / (variance + (d * d));
//
//	float ls = linstep(shadowInfo.bias, 1.0, pMax);
//
//	float shadow = min(ls, 1.0);
//	return 1.0 - clipstep(shadowInfo.sharpness, 1.0, 1.0 - shadow);
//}

float calculateVariance(vec2 moments, float currentDepth, float limit) {
	float p = step(currentDepth, moments.x);
	float variance = max(moments.y - moments.x * moments.x, 0.00002);
	float d = currentDepth - moments.x;
	float pMax = variance / (variance + d*d);
	
	float ls = linstep(limit, 1.0, pMax);
	return min(max(p, ls), 1.0);
}

float getVarianceShadow(vec4 clip, ShadowMapInfo shadowInfo) {
	if (any(greaterThan(abs(clip.xy), vec2(1.0, 1.0)))) return 1.0;
	clip.xy = clip.xy * 0.5 + 0.5;
	
	vec2 moments = textureLod(colorfulShadowMaps[shadowInfo.shadowMap_ID], clip.xy, 0.0).xy; 
	float shadow = calculateVariance(moments, clip.z, shadowInfo.bias);

	return 1.0 - clipstep(shadowInfo.sharpness, 1.0, 1.0 - shadow);
}
vec3 getTranslucentVarianceShadow(vec4 clip, ShadowMapInfo shadowInfo) {
	if (any(greaterThan(abs(clip.xy), vec2(1.0, 1.0)))) return vec3(1.0);
	clip.xy = clip.xy * 0.5 + 0.5;
	
	vec2 opaqueMoments = textureLod(colorfulShadowMaps[shadowInfo.shadowMap_ID], clip.xy, 0.0).xy; 
	float shadowOpaque = calculateVariance(opaqueMoments, clip.z, shadowInfo.bias);
	
	float translucentZ = clip.z - 0.001; // set a slight bias to avoid acne
	vec2 translucentMoments = textureLod(colorfulShadowMaps[shadowInfo.shadowMapTranslucent_ID], clip.xy, 0.0).xy; 	
	float shadowTranslucent = calculateVariance(translucentMoments, translucentZ, shadowInfo.bias);
	
	// find a fix for transparent shadows not having soft edges due to comparison check
	float casterMix = max(shadowOpaque - shadowTranslucent, 0.0);
	//if (shadowTranslucent < shadowOpaque) {
	//	vec3 translucentTransmissionColor = textureLod(colorfulShadowMaps[shadowInfo.shadowMapTranslucentColor_ID], clip.xy, 0.0).rgb;
	//	float translucentTransmissionReveal = textureLod(colorfulShadowMaps[shadowInfo.shadowMapTranslucentReveal_ID], clip.xy, 0.0).r;
	//	return mix(translucentTransmissionColor, vec3(1.0), translucentTransmissionReveal);
	//} else {
	//	return vec3(1.0 - clipstep(shadowInfo.sharpness, 1.0, 1.0 - shadowOpaque));
	//}	
	vec3 shadow = vec3(0.0);
	vec3 translucentTransmissionColor = textureLod(colorfulShadowMaps[shadowInfo.shadowMapTranslucentColor_ID], clip.xy, 0.0).rgb;
	float translucentTransmissionReveal = textureLod(colorfulShadowMaps[shadowInfo.shadowMapTranslucentReveal_ID], clip.xy, 0.0).r;
	shadow = mix(translucentTransmissionColor, vec3(1.0), translucentTransmissionReveal) * casterMix + shadowOpaque * (1.0 - casterMix);
	return shadow;
}

int getNecessaryPoissonSamples(int targetCount) {
  float dx = dFdx( gl_FragCoord.z );
  float dy = dFdy( gl_FragCoord.z );
  float d = max( dx, dy ) * 9000.0;
  return int(max((1.0 - clamp(d, 0.0, 1.0)) * float(targetCount), 1.0));
}
float getShadowBias(float NdD) {
	float invCosSqNdD = 1 / (NdD * NdD);
	float tanNdD = sqrt(invCosSqNdD) - 1.0;
	return max(tanNdD, NdD);
}
float getShadowBiasCoeficient(float activeDepth, float compareDepth, float fine) {
	float diff = abs(activeDepth - compareDepth);
	float p = 1.0 / max(6.0 * exp(fine * diff), 0.0001);
	return min(p / fine + 0.05, 1.0);
}

mat2 getGradientSampleMatrix() {		
	const vec3 magic = vec3( 0.06711056, 0.00583715, 52.9829189 );
	float noise0 = fract( magic.z * fract(dot( gl_FragCoord.xy, magic.xy ) ) );
	float rotX = cos( 2.0 * 3.1415926 * noise0);
	float rotY = sin( 2.0 * 3.1415926 * noise0);
	return mat2(vec2(rotX, rotY), vec2(-rotY, rotX));
}
mat3 getGradientSampleMatrix3D() {		
	const vec3 magic = vec3( 0.06711056, 0.00583715, 52.9829189 );
	const vec3 magic1 = vec3( 0.4912851295, 0.09521895408721, 63.52153 );
	float noise0 = fract( magic.z * fract(dot( gl_FragCoord.xy, magic.xy ) ) );
	float noise1 = fract( magic1.z * fract(dot( gl_FragCoord.xy, magic1.xy ) ) );
	float c2 = cos( 2.0 * 3.1415926 * noise0);
	float c1 = cos( 2.0 * 3.1415926 * noise0);
	float c3 = cos( 2.0 * 3.1415926 * noise1);
	float s2 = sin( 2.0 * 3.1415926 * noise0);
	float s1 = sin( 2.0 * 3.1415926 * noise0);
	float s3 = sin( 2.0 * 3.1415926 * noise1);
	
	
	return mat3(
		vec3(
			(c1 * c3 + s1 * s2 * s3),
			(c2 * s3),
			(c1 * s2 * s3 - c3 * s1)
		),		
		vec3(
			(c3 * s1 * s2 - c1 * s3),
			(c2 * c3),
			(c1 * c3 * s2 + s1 * s3)
		),	
		vec3(
			(c2 * s1),
			(-s2),
			(c1 * c2)
		)
	);
}

float getPCFShadow(vec4 clip, vec3 lightToFrag, vec3 lightToFragNormalized, float bias, ShadowMapInfo shadowInfo, float omniFarPlaneMax) {
	#ifdef S3DSDEF_SHADER_MATERIAL_SURFACE_FORWARD
		shadowInfo.poissonSamples = 1;
	#endif

	// much improved sampling, better performance and visuals, 4 samples is sweet spot, compared to poisson requiring 16 for mediocre results
	float shadow = 0.0;
	switch(shadowInfo.shadowMapConvention) {
		case(SHADOW_MAP_CONVENTION_CONVENTIONAL): {
			if (any(greaterThan(abs(clip.xy), vec2(1.0, 1.0)))) return 1.0;
			clip.xy = clip.xy * 0.5 + 0.5;
			float texelSize = 1.0 / textureSize(pcfShadowMaps[shadowInfo.shadowMap_ID], 0).x; // width&height should be equal
	
			mat2 gradSample = getGradientSampleMatrix();
	
			for(int i = 0; i < shadowInfo.poissonSamples; i++) {
				vec2 sampleOffset = gradSample * poissonDisk[i] * texelSize * shadowInfo.poissonRadius;
				shadow += textureLod(pcfShadowMaps[shadowInfo.shadowMap_ID], clip.xyz + vec3(sampleOffset, -bias), 0.0).x; 
			}
		} break;
		case(SHADOW_MAP_CONVENTION_PARALLEL_SPLIT4): {
			int cascade = -1;
			float cascadeMul = 1.0;
			for (int c = 0; c < 4; c++) {
				cascadeMul = float(1 << (2 * c));
				if (any(greaterThan(abs(clip.xy / cascadeMul), vec2(1.0, 1.0)))) {
					continue;
				}
				cascade = c;
				break;
			}
			if (cascade == -1) {
				return 1.0;
			}
			clip.xy = (clip.xy / cascadeMul) * 0.5 + 0.5;
			float texelSize = 1.0 / textureSize(pcfShadowArrays[shadowInfo.shadowMap_ID], 0).x; // width&height should be equal
			
			mat2 gradSample = getGradientSampleMatrix();
			for(int i = 0; i < shadowInfo.poissonSamples; i++) {
				vec2 sampleOffset = gradSample * poissonDisk[i] * texelSize * shadowInfo.poissonRadius;
				shadow += texture(pcfShadowArrays[shadowInfo.shadowMap_ID], vec4(clip.xy, 0.0, clip.z) + vec4(sampleOffset, float(cascade), -bias)).x; 
			}
		} break;
		case(SHADOW_MAP_CONVENTION_CUBE_MAP): {
			float texelSize = 1.0 / textureSize(pcfShadowCubes[shadowInfo.shadowMap_ID], 0).x; // width&height should be equal
			float invFarPlane = 1.0 / omniFarPlaneMax;
			
			mat3 gradSample = getGradientSampleMatrix3D();
			float currentDepth = (length(lightToFrag) - bias) * invFarPlane; 
			for(int i = 0; i < shadowInfo.poissonSamples; i++) {
				vec3 off = poissonOmniDisk[i].xyz * texelSize * shadowInfo.poissonRadius;
				vec3 sampleOffset = gradSample * off.xyz;
				shadow += texture(pcfShadowCubes[shadowInfo.shadowMap_ID], vec4(lightToFragNormalized + sampleOffset, currentDepth)).x;
			}
		} break;
		case(SHADOW_MAP_CONVENTION_PARABOLOID): {
			float texelSize = 1.0 / textureSize(pcfShadowArrays[shadowInfo.shadowMap_ID], 0).x; // width&height should be equal
			float invFarPlane = 1.0 / omniFarPlaneMax; 
			mat3 gradSample = getGradientSampleMatrix3D();
			vec3 L = mat3(shadowInfo.lightSpaceMatrix) * lightToFragNormalized; // fix orientation
			float currentDepth = (length(lightToFrag) - bias) * invFarPlane; 
			for(int i = 0; i < shadowInfo.poissonSamples; i++) {
				vec3 off = 2.0 * poissonOmniDisk[i].xyz * texelSize * shadowInfo.poissonRadius;
				vec3 sampleOffset = gradSample * off.xyz;
				shadow += textureLod(pcfShadowMaps[shadowInfo.shadowMap_ID], vec3(getFrontParaboloidUV(L + sampleOffset.xyz), currentDepth), 0.0).x; 
			}
		} break;
		case(SHADOW_MAP_CONVENTION_DUAL_PARABOLOID): {
			float texelSize = 1.0 / textureSize(pcfShadowArrays[shadowInfo.shadowMap_ID], 0).x; // width&height should be equal
			float invFarPlane = 1.0 / omniFarPlaneMax; 
			
			mat3 gradSample = getGradientSampleMatrix3D();
			bool sampleTopParaboloid = lightToFrag.y > 0.0;
			float currentDepth = (length(lightToFrag) - bias) * invFarPlane; 
			for(int i = 0; i < shadowInfo.poissonSamples; i++) {
				vec3 off = 2.0 * poissonOmniDisk[i].xyz * texelSize * shadowInfo.poissonRadius;
				vec3 sampleOffset = gradSample * off.xyz;
				shadow += texture(pcfShadowArrays[shadowInfo.shadowMap_ID], vec4(
					sampleTopParaboloid ? getTopParaboloidUV(lightToFragNormalized + sampleOffset.xyz) : getBottomParaboloidUV(lightToFragNormalized + sampleOffset.xyz), 
					sampleTopParaboloid ? 0.0 : 1.0,
					currentDepth)).x;
			}
		} break;
	}
	return shadow / float(shadowInfo.poissonSamples);
}

vec3 getTranslucentPCFShadow(vec4 clip, float bias, ShadowMapInfo shadowInfo) {
//#ifdef SURFACE_FORWARD
//	shadowInfo.poissonSamples = 1;
//#endif

	mat2 gradSample = getGradientSampleMatrix();
// CONVENTIONAL SHADOW MAP
	if (shadowInfo.shadowMapConvention == SHADOW_MAP_CONVENTION_CONVENTIONAL) {
		if (any(greaterThan(abs(clip.xy), vec2(1.0, 1.0)))) return vec3(1.0);
		clip.xy = clip.xy * 0.5 + 0.5;
		float texelSize = 1.0 / textureSize(pcfShadowMaps[shadowInfo.shadowMap_ID], 0).x; // width&height should be equal

		vec3 translucentTransmissionColor = vec3(0.0);
		float translucentTransmissionReveal = 0.0;
		float opaqueShadow = 0.0;
		int poissonSamples = getNecessaryPoissonSamples(int(shadowInfo.poissonSamples));

		float invPs = 1.0 / float(poissonSamples);
		
		float pTranslucent = 0.0; // probability receiver is below a translucent object
		for(int i = 0; i < shadowInfo.poissonSamples; i++) {
			vec2 scoord = gradSample * poissonDisk[i] * texelSize * shadowInfo.poissonRadius;
			vec3 sampleCoord = clip.xyz + vec3(scoord, -bias);
			float opaqueCast = textureLod(pcfShadowMaps[shadowInfo.shadowMap_ID], sampleCoord, 0.0).x;
			
			float translucentDepth = textureLod(colorfulShadowMaps[shadowInfo.shadowMapTranslucent_ID], sampleCoord.xy, 0.0).x;
			pTranslucent += float(translucentDepth <= sampleCoord.z);
			
			vec3 color = textureLod(colorfulShadowMaps[shadowInfo.shadowMapTranslucentColor_ID], sampleCoord.xy, 0.0).rgb;
			float reveal = textureLod(colorfulShadowMaps[shadowInfo.shadowMapTranslucentReveal_ID], sampleCoord.xy, 0.0).r;	 
			translucentTransmissionColor += color;
			translucentTransmissionReveal += reveal;
			opaqueShadow += opaqueCast ;
		}
		
		opaqueShadow *= invPs;
		
		vec3 translucentShadow = mix(translucentTransmissionColor * invPs, vec3(1.0), translucentTransmissionReveal * invPs);
		vec3 fullShadow = mix(vec3(opaqueShadow), translucentShadow*opaqueShadow , pTranslucent * invPs);
		
		return clamp(fullShadow, vec3(0.0),vec3(1.0));
	}

}

#endif