#ifndef GLOBAL_UBO_GLSL
#define GLOBAL_UBO_GLSL
layout(set = 0, binding = 0) uniform GlobalUbo{
	mat4 projection;
	mat4 invProjection;
	mat4 view;
	mat4 invView;
	mat4 projectionView;
	mat4 projectionPrevFrame;
	mat4 viewPrevFrame;
	mat4 projectionViewPrevFrame;
	vec4 frustumPlanes[6];
	vec4 clipPlane; // must be (0.0, 0.0, 0.0, 0.0) to disable
	vec2 screenSize;
	float nearPlane;
	float farPlane;
	int velocityBufferTileExtent;
} ubo;
#endif