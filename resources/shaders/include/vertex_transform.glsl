#ifndef VERTEX_TRANSFORM_GLSL
#define VERTEX_TRANSFORM_GLSL

#pragma optionNV(fastmath on)
#pragma optionNV(fastprecision on)

#include "compression.glsl"
#include "descriptor_indexing.glsl"

#ifdef RIGGED_MESH
#ifndef RIGGDED_MESH_INSTANCE_DESCRIPTOR
	#error No rigged mesh instance descriptor set defined!
#endif

READONLY_BUFFER MorphTargetData {
	uvec4 vectors[1][1]; // position, uv, normal, tangent
} morphTargets[];
READONLY_BUFFER SkeletonBoneIDData {
	uvec4 ids[1]; // 4x uint8 indices, packed 4x
} skeletonIDs[];
READONLY_BUFFER SkeletonBoneWeightData {
	uvec4 weights[1]; // 4x unorm8 weights, packed 4x
} skeletonWeights[];

struct SkeletonBoneMatrixData {
	mat4 transform;
	mat3 normal;
};
layout (set = RIGGDED_MESH_INSTANCE_DESCRIPTOR, binding = 0) uniform RiggedMeshData {
	uint indexOffset;
	bool hasSkeleton;
	bool hasMorphTarget;

	uint skeletonBoneIDBuffer;
	uint skeletonBoneWeightBuffer;
	uint morphTargetBuffer;

	uint morphTargetCount;
		
	float morphTargetWeights[16];
	SkeletonBoneMatrixData boneMatrixData[1];
} riggedMeshData;
#endif

struct VertexData {
	vec3 position;
#ifdef ENABLE_VERTEX_NORMALS
	vec3 normal;
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableVertexTangents
	vec3 tangent;
#endif
#endif
};

VertexData vertex_Transform() {
	VertexData outVertex;

	vec3 vertexPosition = position;
#ifdef ENABLE_VERTEX_NORMALS
	vec3 vertexNormal = LOWP_NVEC3_UNPACK(CMPR_normal);
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableVertexTangents
	vec3 vertexTangent = LOWP_NVEC3_UNPACK(CMPR_tangent);
#endif
#endif
	
#ifdef RIGGED_MESH
	uint vertexPullIndex = gl_VertexIndex - riggedMeshData.indexOffset;
	
	vec3 resultMorphPosition = 0..xxx;
	vec3 resultMorphNormal = 0..xxx;
	vec3 resultMorphTangent = 0..xxx;
	if (riggedMeshData.hasMorphTarget) {
		for (uint i = 0; i < riggedMeshData.morphTargetCount; i++) {
			float weight = riggedMeshData.morphTargetWeights[i];
			if (weight == 0.0) continue;
			
			vec3 positionVector = POLAR_VEC3_UNPACK(morphTargets[riggedMeshData.morphTargetBuffer].vectors[i][vertexPullIndex][0]);
			resultMorphPosition += (vertexPosition + positionVector) * weight; 
			
#ifdef ENABLE_VERTEX_NORMALS
			vec3 normalVector = POLAR_VEC3_UNPACK(morphTargets[riggedMeshData.morphTargetBuffer].vectors[i][vertexPullIndex][2]);
			resultMorphNormal += normalVector * weight; 
			vec3 tangentVector = POLAR_VEC3_UNPACK(morphTargets[riggedMeshData.morphTargetBuffer].vectors[i][vertexPullIndex][3]);
			resultMorphTangent += tangentVector * weight; 
#endif
		}
	} else {
		resultMorphPosition = vertexPosition;
#ifdef ENABLE_VERTEX_NORMALS
		resultMorphNormal = vertexNormal;
		resultMorphTangent = vertexTangent;
#endif
	}
	
	vec3 resultBonePosition = 0..xxx;
	vec3 resultBoneNormal = 0..xxx;
	vec3 resultBoneTangent = 0..xxx;
	if (riggedMeshData.hasSkeleton) {
		uint arrayBlock = uint(floor(float(vertexPullIndex) / 4.0));
		uint vecIndex = vertexPullIndex % 4;
		vec4 unormUnpack = unpackUnorm4x8(skeletonIDs[riggedMeshData.skeletonBoneIDBuffer].ids[arrayBlock][vecIndex]);
		for (int i = 0; i < 4; i++) {
			int boneID = int(255.0 * unormUnpack[i]);
			if (boneID == 0xFF) {
				if (i == 0) {
					resultBonePosition = resultMorphPosition;
#ifdef ENABLE_VERTEX_NORMALS
					resultBoneNormal = resultMorphNormal;
					resultBoneTangent = resultMorphTangent;
#endif
				}
				break;
			}
			float weight = unpackUnorm4x8(skeletonWeights[riggedMeshData.skeletonBoneWeightBuffer].weights[arrayBlock][vecIndex])[i];
			resultBonePosition += vec3(riggedMeshData.boneMatrixData[boneID].transform * vec4(resultMorphPosition, 1.0)) * weight;
#ifdef ENABLE_VERTEX_NORMALS
			resultBoneNormal += (riggedMeshData.boneMatrixData[boneID].normal * resultMorphNormal) * weight;
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableVertexTangents
			resultBoneTangent += (riggedMeshData.boneMatrixData[boneID].normal * resultMorphTangent) * weight;
#endif
#endif
		}
	} else {
		resultBonePosition = resultMorphPosition;
#ifdef ENABLE_VERTEX_NORMALS
		resultBoneNormal = resultMorphNormal;
		resultBoneTangent = resultMorphTangent;
#endif
	}
	vertexPosition = resultBonePosition;
#ifdef ENABLE_VERTEX_NORMALS
	vertexNormal = resultBoneNormal;
	vertexTangent = resultBoneTangent;
#endif
#endif

	outVertex.position = vertexPosition;

#ifdef ENABLE_VERTEX_NORMALS
	outVertex.normal = vertexNormal;
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableVertexTangents 
	outVertex.tangent = vertexTangent;
#endif
#endif
	
	return outVertex;
}

#endif