#ifndef MATERIAL_GLSL
#define MATERIAL_GLSL
#include "material_id.glsl"
#ifndef DISABLE_PARTIAL_DERIVATIVES
#include "normal_mapping.glsl"
#endif

const uint MAT_ALPHA_MASK_BIT = 0x4u;
const uint MAT_DIFFUSE_TEXTURE_BIT = 0x8u;
const uint MAT_SPECULAR_TEXTURE_BIT = 0x10u;
const uint MAT_GLOSSINESS_TEXTURE_BIT = 0x20u;
const uint MAT_REFLECTIVITY_TEXTURE_BIT = 0x40u;
const uint MAT_EMISSIVE_TEXTURE_BIT = 0x80u;
const uint MAT_NORMAL_MAP_BIT = 0x100u;
const uint MAT_OPACITY_MAP_BIT = 0x200u;

bool matHasAlphaMask(MaterialData material) {
 	return bool(material.textureFlags & MAT_ALPHA_MASK_BIT);
}
bool matHasDiffuseTexture(MaterialData material) {
	return bool(material.textureFlags & MAT_DIFFUSE_TEXTURE_BIT);
}
bool matHasSpecularTexture(MaterialData material) {
	return bool(material.textureFlags & MAT_SPECULAR_TEXTURE_BIT);
}
bool matHasGlossinessTexture(MaterialData material) {
	return bool(material.textureFlags & MAT_GLOSSINESS_TEXTURE_BIT);
}
bool matHasReflectivityTexture(MaterialData material) {
	return bool(material.textureFlags & MAT_REFLECTIVITY_TEXTURE_BIT);
}
bool matHasEmissiveTexture(MaterialData material) {
	return bool(material.textureFlags & MAT_EMISSIVE_TEXTURE_BIT);
}
bool matHasNormalMap(MaterialData material) {
	return bool(material.textureFlags & MAT_NORMAL_MAP_BIT);
}
bool matHasOpacityMap(MaterialData material) {
	return bool(material.textureFlags & MAT_OPACITY_MAP_BIT);
}

bool matIsTextured(MaterialData material) {
	return bool(material.textureFlags);
}

SAMPLER2D_TEXTURE(texID);

struct FragmentInput {
	vec3 position;
	vec2 uv;
	vec3 normal;
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableVertexTangents
	vec3 tangent;
#endif
};

vec2 getTexCoordValue(vec2 uv, vec2 co, vec2 cm) {
	return uv * cm + co;
}

bool getMaterialAlphaMaskCutout(MaterialData material, vec2 coord) {
	if (!matHasAlphaMask(material)) return false; // return false if no alpha mask
	
	//const vec2 ts = vec2(textureSize(texID[material.textureData.maskTex_id], 0).xy);
	//const vec2 InvSize = 1.0 / ts;
    //const float inv6 = 1./6.;
	//
    //vec2 Weight[2];
	//vec2 Sample[2];
    //
    //vec2 UV = coord * ts;
    //vec2 tc = floor(UV - 0.5) + 0.5;
	//vec2 f = UV - tc;
	//vec2 f2 = f * f; 
	//vec2 f3 = f2 * f;
    //
    //vec2 of = vec2(1.)-f;
    //vec2 of2 = of*of;
    //vec2 of3 = of2*of;
    //
    //vec2 w0 = inv6 * of3 ;
	//vec2 w1 = inv6 * (vec2(4.) + 3.*f3 - 6.*f2);
	//vec2 w2 = inv6 * (vec2(4.) + 3.*of3 - 6.*of2);
	//vec2 w3 = inv6 * f3;
    //
    //Weight[0] = w0 + w1;
	//Weight[1] = w2 + w3;
	//
	//Sample[0] = tc - (vec2(1.) - w1/Weight[0]);
	//Sample[1] = tc + vec2(1.) + w3/Weight[1];
	//
	//Sample[0] *= InvSize;
	//Sample[1] *= InvSize;
	//
    //float sampleWeight[4];
    //sampleWeight[0] = Weight[0].x * Weight[0].y;
    //sampleWeight[1] = Weight[1].x * Weight[0].y;
    //sampleWeight[2] = Weight[0].x * Weight[1].y;
    //sampleWeight[3] = Weight[1].x * Weight[1].y;
	//
    //float Ctl = texture(texID[material.textureData.maskTex_id], vec2(Sample[0].x, Sample[0].y)).x * sampleWeight[0];    
    //float Ctr = texture(texID[material.textureData.maskTex_id], vec2(Sample[1].x, Sample[0].y)).x * sampleWeight[1];    
    //float Cbl = texture(texID[material.textureData.maskTex_id], vec2(Sample[0].x, Sample[1].y)).x * sampleWeight[2];    
    //float Cbr = texture(texID[material.textureData.maskTex_id], vec2(Sample[1].x, Sample[1].y)).x * sampleWeight[3];
																					 
	//return bool((Ctl + Ctr + Cbl + Cbr) < 0.5);
	
	return bool((texture(texID[material.textureData.maskTex_id], coord).x)
	< 0.5);
}
vec3 getMaterialDiffuse(MaterialData material, bool useTexture, vec2 coord) {
	vec3 diffuse = material.factorData.diffuse.xyz;
	if (useTexture) {
		diffuse *= texture(texID[material.textureData.diffuseTex_id], coord).xyz;
	}
	return diffuse;
}
float getMaterialSpecular(MaterialData material, bool useTexture, vec2 coord) {
	float specular = material.factorData.specular;
	if (useTexture) {
		specular *= texture(texID[material.textureData.specularTex_id], coord).x;
	}
	return specular;
}
float getMaterialGlossiness(MaterialData material, bool useTexture, vec2 coord) {
	float glossiness = material.factorData.glossiness;
	if (useTexture) {
		glossiness *= texture(texID[material.textureData.glossinessTex_id], coord).x;
	}
	return glossiness;
}
float getMaterialReflectivity(MaterialData material, bool useTexture, vec2 coord) {
	float reflectivity = material.factorData.reflectivity;
	if (useTexture) {
		reflectivity *= texture(texID[material.textureData.reflectivityTex_id], coord).x;
	}
	return reflectivity;
}
vec4 getMaterialEmissive(MaterialData material, bool useTexture, vec2 coord) {
	vec3 emission = material.factorData.emission.xyz;
	if (useTexture) {
		emission *= texture(texID[material.textureData.emissiveTex_id], coord).xyz;
	}
	return vec4(emission, material.factorData.emission.w);
}

#ifndef DISABLE_PARTIAL_DERIVATIVES
vec3 getMaterialNormal(FragmentInput fragIn, MaterialData material, bool useTexture, vec2 coord) {
	if (!useTexture) return fragIn.normal;
	vec3 tgNormal = (texture(texID[material.textureData.normalTex_id], coord).xyz) * 2.0 - 1.0;
		
#ifdef S3DSDEF_SHADER_PERMUTATION_EnableVertexTangents
	return getNormalFromMap(tgNormal, fragIn.normal, fragIn.tangent);
#else
	return getNormalFromMapD(tgNormal, fragIn.uv, fragIn.normal, fragIn.position);
#endif
}
#endif

float getMaterialOpacity(MaterialData material, bool useTexture, vec2 coord) {
	float opacity = material.factorData.opacity;
	if (useTexture) {
		opacity *= texture(texID[material.textureData.opacityTex_id], coord).x;
	}
	return opacity;
}
#endif
