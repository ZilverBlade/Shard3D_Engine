#ifndef MATH_GLSL
#define MATH_GLSL
float random(vec3 seed, int i){
	vec4 seed4 = vec4(seed,i);
	float dot_product = dot(seed4, vec4(12.9898,78.233,45.164,94.673));
	return fract(sin(dot_product) * 43758.5453);
}
float linstep(float min, float max, float alpha) {
	return clamp((alpha - min) / (max - min), 0.0, 1.0);
}
float clipstep(float min, float max_, float alpha) {
	return max((alpha - min) * (max_ / (1.0 - min)), 0.0);
}
float saturate(float v) {
	return clamp(v, 0.0, 1.0);
}
#endif