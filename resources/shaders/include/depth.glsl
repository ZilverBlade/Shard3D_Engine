#ifndef DEPTH_GLSL
#define DEPTH_GLSL

#ifdef S3DSDEF_SHADER_PERMUTATION_DepthBufferFormat_L16
#define LOGARITHMIC_DEPTH_BUFFER
#endif
#ifdef S3DSDEF_SHADER_PERMUTATION_DepthBufferFormat_F32R
#define REVERSE_DEPTH_BUFFER
#endif
#ifdef REVERSE_DEPTH_BUFFER
const float CLEAR_DEPTH = 0.0;
#define DEPTH_COMPARE_SIGN >=
// reverse Z linearization function
#ifdef S3DSDEF_SHADER_PERMUTATION_INFINITE_FAR_Z
float linearize_depth(float d, float n, float f) {
    return n / d;
}
#else
float linearize_depth(float d, float n, float f) {
    return d / ((f * n - n) / (f - n));
}
#endif
#else
const float CLEAR_DEPTH = 1.0;
#define DEPTH_COMPARE_SIGN <=

#error depth linerarization isn't implemented!
// standard Z linearization function
float linearize_depth(float d, float n, float f) {
    
}
#endif
#endif
