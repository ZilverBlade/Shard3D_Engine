#ifndef PARABOLOID_GLSL
#define PARABOLOID_GLSL

// adapted from https://graphicsrunner.blogspot.com/2008/07/dual-paraboloid-reflections.html

vec4 getParaboloidPosition(vec3 position, float zNear, float zFar) {
	float L = length(position.xyz);
	vec4 returnPos;
	returnPos.xyz = position.xyz / L;
	returnPos.xy = returnPos.xy / (returnPos.z + 1.0);
	returnPos.z = (L - zNear) / (zFar - zNear);
	returnPos.w = 1.0;
	
	return returnPos;
}

vec2 getBottomParaboloidUV(vec3 R) {
	// calculate the back paraboloid map texture coordinates
	vec2 back;
	back.x = R.x / (1.0 - R.y);
	back.y = R.z / (1.0 - R.y);
	back.x = 0.5 * back.x + 0.5; //bias and scale to correctly sample a texture
	back.y = -0.5 * back.y + 0.5;
	return back;   // sample the back paraboloid map
}

vec2 getTopParaboloidUV(vec3 R) {
	
	// calculate the front paraboloid map texture coordinates
	vec2 front;
	front.x = R.x / (R.y + 1.0);
	front.y = R.z / (R.y + 1.0);
	front.x = 0.5 * front.x + 0.5; //bias and scale to correctly sample a texture
	front.y = 0.5 * front.y + 0.5;
	return front;  // sample the front paraboloid map
}
vec2 getFrontParaboloidUV(vec3 R) {
	// calculate the front paraboloid map texture coordinates
	vec2 front;
	front.x = R.x / (R.y + 1.0);
	front.y = R.z / (R.y + 1.0);
	front.x = -0.5 * front.x + 0.5; //bias and scale to correctly sample a texture
	front.y = -0.5 * front.y + 0.5;
	return front;  // sample the front paraboloid map
}
#endif