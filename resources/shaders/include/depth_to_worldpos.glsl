// this is supposed to get the world position from the depth buffer
#ifndef DEPTHWORLDPOS_GLSL
#define DEPTHWORLDPOS_GLSL
#include "depth.glsl"
vec3 WorldPosFromDepth(vec2 uvCoord, float depth, mat4 invProjectMatrix, mat4 invViewMatrix) {
    vec4 clipSpacePosition = vec4(uvCoord * 2.0 - 1.0, depth, 1.0);
    vec4 viewSpacePosition = invProjectMatrix * clipSpacePosition;

    viewSpacePosition /= viewSpacePosition.w;
    vec4 worldSpacePosition = invViewMatrix * viewSpacePosition;

    return worldSpacePosition.xyz;
}
vec3 ViewPosFromDepth(vec2 uvCoord, float depth, mat4 invProjectMatrix) {
    vec4 clipSpacePosition = vec4(uvCoord * 2.0 - 1.0, depth, 1.0);
    vec4 viewSpacePosition = invProjectMatrix * clipSpacePosition;

    viewSpacePosition /= viewSpacePosition.w;

    return viewSpacePosition.xyz;
}
vec3 ClipPosFromDepth(vec2 uvCoord, float depth) {
    vec4 clipSpacePosition = vec4(uvCoord * 2.0 - 1.0, depth, 1.0);
    return clipSpacePosition.xyz;
}
vec3 WorldPosFromDepth(vec2 fragCoord, vec2 screenArea, float depth, mat4 invProjectMatrix, mat4 invViewMatrix) {
    return WorldPosFromDepth(fragCoord / screenArea, depth, invProjectMatrix, invViewMatrix);
}
#endif