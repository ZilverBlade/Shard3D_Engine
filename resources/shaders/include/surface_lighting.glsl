#ifndef SURFACE_LIGHTING_GLSL
#define SURFACE_LIGHTING_GLSL
#extension GL_EXT_control_flow_attributes : enable

#define DESCRIPTOR_BINDLESS_SET 2

#include "global_ubo.glsl"
#include "scene_buffers.glsl"
#include "shadow_math.glsl"
#include "descriptor_indexing.glsl"
#include "exponential_fog.glsl"
#include "shaded_pixel_info.glsl"
#include "specular_model.glsl"

SAMPLERCUBE_TEXTURE(texCubeID);
SAMPLER2D_TEXTURE(tex2DID);
SAMPLER3D_TEXTURE(tex3DID);

const float PI = 3.14159265359;
const vec3 luma = vec3(0.299, 0.587, 0.114);
const float MAX_GLOSSINESS_MIP_LEVEL = 5.0;

struct LightResult {
	vec3 diffuse;
	vec3 specular;
};

float basicFresnel(float dotNV) {
     float fresnelFactor = 1.0 - dotNV;
	 float ff2 = fresnelFactor * fresnelFactor;
	 float ff5 = ff2 * ff2 * fresnelFactor;
     return fresnelFactor + ff5;
}

struct EnvironmentLightingInfo {
	vec3 ambientOcclusion;
	vec3 ambient;
	vec3 reflection;
};
struct LightingInfo{
	vec3 L;
	float halfWayProduct;
	float fragDist;
	vec3 radiance;
	float NdL;
	bool skip;
};

struct GlobalFragmentInfo{
	float NdVa;
	vec3 V;
	vec3 N;
	vec3 R;
};

bool isPointInCube(vec3 pt, vec3 cubeDimensions, mat4 invCubeTransform) {
	vec4 point = invCubeTransform * vec4(pt, 1.0);
	
	return all(lessThanEqual(-cubeDimensions, point.xyz)) && all(lessThanEqual(point.xyz, cubeDimensions));
}

float calcPointBetweenBoxLimits(vec3 pt, vec3 lowerLimit, vec3 upperLimit, mat4 invCubeTransform) {
	vec3 point = abs(invCubeTransform * vec4(pt, 1.0)).xyz;
	
	vec3 higherX = vec3(upperLimit.x, point.y, point.z);
	vec3 higherY = vec3(point.x, upperLimit.y, point.z);
	vec3 higherZ = vec3(point.x, point.y, upperLimit.z);
	
	float distX = distance(point, higherX);
	float distY = distance(point, higherY);
	float distZ = distance(point, higherZ);
	
	vec3 distLowToHigh = abs(lowerLimit - upperLimit);
	
	float diffX = clamp(distX / distLowToHigh.x, 0.0, 1.0);
	float diffY = clamp(distY / distLowToHigh.y, 0.0, 1.0);
	float diffZ = clamp(distZ / distLowToHigh.z, 0.0, 1.0);
	
	return diffX*diffY*diffZ;
}

GlobalFragmentInfo getGlobalFragmentInfo(vec3 P, vec3 N) {
	GlobalFragmentInfo globalInfo;
	globalInfo.V = normalize(ubo.invView[3].xyz - P);
	globalInfo.NdVa = abs(dot(N, globalInfo.V));
	globalInfo.N = N;
	globalInfo.R = reflect(-globalInfo.V, N);
	return globalInfo;
}

vec3 applyLightCookie(sampler2D cookie, vec4 clip) {
	clip.xy = clip.xy * 0.5 + 0.5;
	return texture(cookie, clip.xy).rgb;
}

LightResult calculateLightFromSource(GlobalFragmentInfo globalInfo, LightingInfo lightingInfo, ShadedPixelInfo pixel) {
	vec3 diffuse = lightingInfo.radiance * lightingInfo.NdL;
	vec3 specular = pixel.specular * lightingInfo.radiance * blinnPhongSpecular(lightingInfo.halfWayProduct, pixel.glossiness) * lightingInfo.NdL;

	return LightResult(diffuse, specular);
}

vec3 getCubemapReflection(BoxReflectionCube cubemap, vec3 R, float mip) {
	// unsigned floating point format with RGB 9 bit mantissa and 5 bit exponent
	return textureLod(texCubeID[cubemap.reflection_id], R, mip).rgb * cubemap.color.xyz * cubemap.color.w;
}
vec3 getCubemapIrradiance(BoxReflectionCube cubemap, vec3 R) {
	return textureLod(texCubeID[cubemap.irradiance_id], R, 0.0).rgb * cubemap.color.xyz * cubemap.color.w;
}
vec3 getSkyLightReflection(Skylight skylight, vec3 R, float mip) {
	// for some reason the reflection vector is inverted when sampling the sky atmosphere reflection, idk why
	return textureLod(texCubeID[skylight.reflection_id], skylight.dynamicSky == 1 ? -R : R, mip).rgb * skylight.reflectionTint.rgb * skylight.reflectionTint.w;
}
vec3 getSkyLightIrradiance(Skylight skylight, vec3 R) {
	return textureLod(texCubeID[skylight.irradiance_id], skylight.dynamicSky == 1 ? -R : R, 0.0).rgb * skylight.reflectionTint.rgb * skylight.reflectionTint.w;
}

vec3 getReflectionData(GlobalFragmentInfo globalInfo, ShadedPixelInfo pixel) {
	float reflectionMip = (1.0 - pixel.glossiness) * MAX_GLOSSINESS_MIP_LEVEL;
	vec3 refl = vec3(0.0);
	for (int i = 0; i < sceneEnvironment.numSkylights; i++) {
		refl += pixel.glossiness * getSkyLightReflection(sceneEnvironment.skylights[i], globalInfo.R, reflectionMip);
	}
	for (int i = 0; i < sceneSotAFX.numReflectionCubes; i++) {
		BoxReflectionCube cubemap = sceneSotAFX.reflectionCubes[i];
	
		vec3 worldSpaceCubeDim = cubemap.boundsScale + vec3(cubemap.intersectionDist);
		if (!isPointInCube(pixel.positionWorld, worldSpaceCubeDim.xyz, cubemap.invTransform)) continue;
		float transitionStrength = calcPointBetweenBoxLimits(pixel.positionWorld, cubemap.boundsScale, worldSpaceCubeDim, cubemap.invTransform);
		
		refl = mix(refl, getCubemapReflection(cubemap, globalInfo.R, reflectionMip), transitionStrength);
	}
	
	if (ubo.clipPlane == vec4(0, 0, 0, 0)) { // no clip plane so it's rendered from an actual camera
		for (int i = 0; i < sceneSotAFX.numReflectionPlanes; i++) {
			PlanarReflection reflection = sceneSotAFX.reflectionPlanes[i];
			if (reflection.reflection_id == 0) continue; // invalid reflection
			
			float maxDist = reflection.maxHeight + reflection.intersectionDist;
			float dist = dot(vec4(pixel.positionWorld, 1.0), reflection.cartesianPlane);
			if (dist > maxDist || dist < -0.001) continue;
			
			float transitionStrength = 1.0 - clamp((dist - reflection.maxHeight) / reflection.intersectionDist, 0.0, 1.0);

		// TODO: unreliable
			vec3 tangent = reflection.cartesianPlane.zxy;
			
			vec3 bitan = cross(reflection.cartesianPlane.xyz, tangent);
			
			tangent = cross(bitan,reflection.cartesianPlane.xyz);
			
			vec2 DuDv = vec2(dot(pixel.normal, tangent), dot(pixel.normal, bitan));
			DuDv = clamp(DuDv, -reflection.distortionDeltaLimit, reflection.distortionDeltaLimit) * reflection.distortDuDvFactor * 0.01;
			
			vec2 uv = vec2(gl_FragCoord.xy) / (ubo.screenSize - vec2(1));
			uv.y = 1.0 - uv.y;
			uv = clamp(uv + DuDv, vec2(0.0), vec2(1.0));
			vec3 reflRead = textureLod(tex2DID[reflection.reflection_id], uv, 0.0).rgb;
			refl = mix(refl, reflRead * reflection.color.rgb * reflection.color.w, transitionStrength);
		}
	}
	return refl;
}
float randomPos(vec3 seed, int i){
	vec4 seed4 = vec4(seed,i);
	float dot_product = dot(seed4, vec4(12.9898,78.233,45.164,94.673));
	return fract(sin(dot_product) * 43758.5453);
}

EnvironmentLightingInfo getEnvironmentLightingData(GlobalFragmentInfo globalInfo, ShadedPixelInfo pixel, float ssao) {
	float reflectionMip = (1.0 - pixel.glossiness) * MAX_GLOSSINESS_MIP_LEVEL;
	EnvironmentLightingInfo envInfo;	
	envInfo.ambient = vec3(0.0);
	envInfo.ambientOcclusion = vec3(ssao);
	envInfo.reflection = vec3(0.0);
	bool getReflection = pixel.diffuse.w != 1.0;
	for (int i = 0; i < sceneEnvironment.numSkylights; i++) {
		envInfo.ambient += 2.0 * getSkyLightIrradiance(sceneEnvironment.skylights[i], globalInfo.R);
		if (getReflection) {
			envInfo.reflection += pixel.glossiness * getSkyLightReflection(sceneEnvironment.skylights[i], globalInfo.R, reflectionMip);
		}
	}
	for (int i = 0; i < sceneSotAFX.numAOVolumes; i++) {
		BoxAmbientOcclusionVolume volume = sceneSotAFX.aoVolumes[i];

		vec3 worldSpaceCubeDim = volume.boundsScale + vec3(volume.intersectionDist);
		if (!isPointInCube(pixel.positionWorld, worldSpaceCubeDim.xyz, volume.invTransform)) continue;
		float transitionStrength = calcPointBetweenBoxLimits(pixel.positionWorld, volume.boundsScale, worldSpaceCubeDim, volume.invTransform);
		vec3 occlusionTerm = volume.tint_p_mOccl.xyz * volume.tint_p_mOccl.w;
		envInfo.ambientOcclusion = mix(envInfo.ambientOcclusion, envInfo.ambientOcclusion * occlusionTerm, transitionStrength);
	}
	vec3 irradianceData = vec3(0.0);
	for (int i = 0; i < sceneSotAFX.numReflectionCubes; i++) {
		BoxReflectionCube cubemap = sceneSotAFX.reflectionCubes[i];
		vec3 worldSpaceCubeDim = cubemap.boundsScale + vec3(cubemap.intersectionDist);
		if (!isPointInCube(pixel.positionWorld, worldSpaceCubeDim.xyz, cubemap.invTransform)) continue;
		float transitionStrength = calcPointBetweenBoxLimits(pixel.positionWorld, cubemap.boundsScale, worldSpaceCubeDim, cubemap.invTransform);
		
		if (getReflection) {
			vec3 reflRead = getCubemapReflection(cubemap, globalInfo.R, reflectionMip);
			envInfo.reflection = mix(envInfo.reflection, reflRead, transitionStrength);
		}
		if (cubemap.irradiance_id == 0) continue;
		vec3 irrRead = 2.0 * getCubemapIrradiance(cubemap, globalInfo.R);
		envInfo.ambient = mix(envInfo.ambient, irrRead, transitionStrength);
	}
	if (ubo.clipPlane == vec4(0, 0, 0, 0)) { // no clip plane so it's rendered from an actual camera
		for (int i = 0; i < sceneSotAFX.numReflectionPlanes; i++) {
			PlanarReflection reflection = sceneSotAFX.reflectionPlanes[i];
			if (reflection.reflection_id == 0) continue; // invalid reflection
			
			float maxDist = reflection.maxHeight + reflection.intersectionDist;
			float dist = dot(vec4(pixel.positionWorld, 1.0), reflection.cartesianPlane);
			if (dist > maxDist || dist < -0.001) continue;
			
			float transitionStrength = 1.0 - clamp((dist - reflection.maxHeight) / reflection.intersectionDist, 0.0, 1.0);

		// TODO: unreliable
			vec3 tangent = reflection.cartesianPlane.zxy;
			
			vec3 bitan = cross(reflection.cartesianPlane.xyz, tangent);
			
			tangent = cross(bitan,reflection.cartesianPlane.xyz);
			
			vec2 DuDv = vec2(dot(pixel.normal, tangent), dot(pixel.normal, bitan));
			DuDv = clamp(DuDv, -reflection.distortionDeltaLimit, reflection.distortionDeltaLimit) * reflection.distortDuDvFactor * 0.01;
			
			vec2 uv = vec2(gl_FragCoord.xy) / (ubo.screenSize - vec2(1));
			uv.y = 1.0 - uv.y;
			uv = clamp(uv + DuDv, vec2(0.0), vec2(1.0));
			vec3 reflRead = textureLod(tex2DID[reflection.reflection_id], uv, 0.0).rgb;
			envInfo.reflection = mix(envInfo.reflection, reflRead * reflection.color.rgb * reflection.color.w, transitionStrength);
			if (reflection.irradiance_id == 0) continue;
			vec3 irrRead = textureLod(tex2DID[reflection.irradiance_id], uv, 0.0).rgb;
			envInfo.ambient = mix(envInfo.ambient, 2.0 * irrRead * reflection.color.rgb * reflection.color.w, transitionStrength);
		}
	}
	
	float ws = 0.0;
	for (int i = sceneSotAFX.numLightPropagationVolumeCascades; i != 0; i--) {
		LightPropagationVolume volume = sceneSotAFX.lightPropagationVolumeCascades[i - 1];
		
		vec3 worldSpaceCubeDim = volume.boundsScale;
		vec3 cc = (pixel.positionWorld - volume.center) / volume.boundsScale;
		vec3 acc = abs(cc);
		float weight;
		if (ws != 1.0 && ws != 0.0) {
			weight = 1.0 - ws;
			ws = 1.0;
		} else {
			weight = clamp((1.00-acc.x) * volume.fadeRatio, 0.0, 1.0) * clamp((1.00-acc.z) * volume.fadeRatio, 0.0, 1.0) * clamp((1.00-acc.z) * volume.fadeRatio, 0.0, 1.0);
			if (weight == 0.0) {
				continue;
			}
			ws += weight;
		}
		vec3 uv = cc * 0.5 + 0.5;
		
		vec4 SHintensity = vec4( 0.282094792, 0.488602512 * pixel.normal.y, -0.488602512 * pixel.normal.z, 0.488602512 * pixel.normal.x);
		vec3 lpvIntensity = vec3(0.0);
		
		[[unroll]]
		for (int i = 0; i < 1; i++) {
			//vec3 cd = floor(pixel.positionWorld.xyz*1000.0);
			//vec3 cd = gl_FragCoord.xyy;
			//int idx = int(16.0*randomPos(cd, i))%16;
			vec3 suv = uv; //+ 0.25.xxx *
			//poissonOmniDisk[idx] / 32..xxx;
			lpvIntensity += vec3(
				dot(SHintensity, textureLod(tex3DID[volume.redvoxel_id], suv, 0.0).rgba),
				dot(SHintensity, textureLod(tex3DID[volume.greenvoxel_id], suv, 0.0).rgba),
				dot(SHintensity, textureLod(tex3DID[volume.bluevoxel_id], suv, 0.0).rgba)
			) /1.0;
		}
		envInfo.ambient += volume.color.rgb * volume.color.w * (1.0 / PI) * max(vec3(0.0), lpvIntensity) * weight;
		
		// debug cascades
		//if (i == 1 ){
		//envInfo.ambient += vec3(weight, 0.0, 0.0);
		//}if (i == 2 ){
		//envInfo.ambient += vec3(0.0, weight, 0.0);
		//}if (i == 3 ){
		//envInfo.ambient += vec3(0.0, 0.0, weight);
		//}
		if (ws == 1.0)
			break;
	}
	return envInfo;
}

LightingInfo getDirectionalLightingInfo(GlobalFragmentInfo globalInfo, ShadedPixelInfo pixel, DirectionalLight light) {
	LightingInfo info;
	info.skip = false;
	info.skip = false;
	vec3 L = -light.direction.xyz;
	float NdL = max(dot(pixel.normal, L), 0.0);
	
	if (NdL == 0.0) {
		info.skip = true;
		return info;
	}
	
	vec3 shadow = vec3(1.0);
	info.L = L;
	info.halfWayProduct = max(0.0, dot(-globalInfo.V, normalize(reflect(L, pixel.normal))));
	
	info.NdL = NdL;
	if (sceneShadowMapInfos.shadowMaps[ light.shadowMapInfoIndex].shadowMap_ID != 0) {
		vec4 clip = sceneShadowMapInfos.shadowMaps[ light.shadowMapInfoIndex].lightSpaceMatrix * vec4(pixel.positionWorld, 1.0);	
#ifdef S3DSDEF_SHADER_PERMUTATION_DirectionalShadowMapType_Variance
		if (sceneShadowMapInfos.shadowMaps[ light.shadowMapInfoIndex].shadowMapTranslucent_ID != 0) {
			shadow = getTranslucentVarianceShadow(clip, sceneShadowMapInfos.shadowMaps[ light.shadowMapInfoIndex]);
			} else {
			shadow = vec3(getVarianceShadow(clip, sceneShadowMapInfos.shadowMaps[ light.shadowMapInfoIndex]));
		}
#endif
#ifdef S3DSDEF_SHADER_PERMUTATION_DirectionalShadowMapType_PCF
		float bias = sceneShadowMapInfos.shadowMaps[ light.shadowMapInfoIndex].bias * mix(0.1, 0.01, max(dot(pixel.dnormal, L), 0.0));  // (pow(10.0, 1.0 - NdL) - 0.99) * 0.1;
		if (sceneShadowMapInfos.shadowMaps[ light.shadowMapInfoIndex].shadowMapTranslucent_ID != 0) {
			shadow = getTranslucentPCFShadow(clip, bias, sceneShadowMapInfos.shadowMaps[ light.shadowMapInfoIndex]);
		} else {
			shadow = vec3(getPCFShadow(clip, L, L, bias, sceneShadowMapInfos.shadowMaps[ light.shadowMapInfoIndex], 0.0));
		}
#endif 
	}


	info.radiance = light.color.rgb * shadow;
	return info;
}

LightingInfo getSpotLightingComplexInfo(GlobalFragmentInfo globalInfo, ShadedPixelInfo pixel, SpotLightComplex light) {
	LightingInfo info;
	info.skip = false;
	vec3 fragToLight = light.position.xyz - pixel.positionWorld;
	vec3 centerToRay = dot(fragToLight, globalInfo.R) * globalInfo.R - fragToLight;
	fragToLight += centerToRay * clamp(light.sourceRadius / length(centerToRay), 0.0, 1.0);
	float distSq = dot(fragToLight, fragToLight);
	float radiusSq = light.radius * light.radius;
#ifndef DISABLE_LIGHT_CULLING
	if (distSq > radiusSq) {	// light culling
		info.skip = true;
		return info;
	}
#endif

	vec3 L = normalize(fragToLight);
	float outerCosAngle = light.angle.x;
	float theta = dot(L, normalize(-light.direction.xyz));
#ifndef DISABLE_LIGHT_CULLING
	if (!(theta > outerCosAngle)) {
		info.skip = true;
		return info;
	}
#endif
	float dOverLr2 = distSq / radiusSq;
	float dOverLr4 = dOverLr2 * dOverLr2;
	float fallNum = clamp(1.0 - dOverLr4, 0.0, 1.0);
	float attenuation = fallNum * fallNum / (distSq + 1.0);

	float epsilon = light.angle.y - outerCosAngle;
	float intensity = clamp((theta - outerCosAngle) / epsilon, 0.0, 1.0);
		
	info.L = L;
	info.halfWayProduct = max(dot(pixel.normal, normalize(globalInfo.V + L)), 0.0);
	info.NdL = max(dot(pixel.normal, L), 0.0);
	
	vec4 clip;
	if (light.shadowMapInfoIndex != -1 || light.cookie_ID != 0) {
		clip = sceneShadowMapInfos.shadowMaps[ light.shadowMapInfoIndex].lightSpaceMatrix * vec4(pixel.positionWorld, 1.0);	
		clip.xyz /= clip.w;
	}
	
	vec3 shadow = vec3(1.0);
	if (light.shadowMapInfoIndex != -1) {
#ifdef S3DSDEF_SHADER_PERMUTATION_SpotShadowMapType_Variance
		if (sceneShadowMapInfos.shadowMaps[ light.shadowMapInfoIndex].shadowMapTranslucent_ID != 0) {
			shadow = getTranslucentVarianceShadow(clip, sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex]);
		} else {
			shadow = vec3(getVarianceShadow(clip, sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex]));
		}
#endif 
#ifdef S3DSDEF_SHADER_PERMUTATION_SpotShadowMapType_PCF
		float bias = sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex].bias * mix(4.0, 0.1, max(dot(pixel.dnormal, L), 0.0)); 
		if (sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex].shadowMapTranslucent_ID != 0) {
			shadow = getTranslucentPCFShadow(clip, bias, sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex]);
		} else {
			shadow = vec3(getPCFShadow(clip, -fragToLight, L, bias, sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex], light.radius));
		}
#endif
	};
	vec3 cookie = vec3(1.0);
	if (light.cookie_ID != 0) {
		cookie = applyLightCookie(tex2DID[light.cookie_ID], clip);
	}
	info.radiance = intensity * attenuation * light.color.rgb * shadow * cookie;
	return info;
}
LightingInfo getSpotLightingInfo(GlobalFragmentInfo globalInfo, ShadedPixelInfo pixel, SpotLight light) {
	LightingInfo info;
	info.skip = false;
	vec3 fragToLight = light.position.xyz - pixel.positionWorld;
	float distSq = dot(fragToLight, fragToLight);
	float radiusSq = light.radius * light.radius;
#ifndef DISABLE_LIGHT_CULLING
	if (distSq > radiusSq) {	// light culling
		info.skip = true;
		return info;
	}
#endif
	vec3 L = normalize(fragToLight);
	float outerCosAngle = light.angle.x;
	float theta = dot(L, normalize(-light.direction.xyz));
#ifndef DISABLE_LIGHT_CULLING
	if (!(theta > outerCosAngle)) {
		info.skip = true;
		return info;
	}
#endif
	float dOverLr2 = distSq / radiusSq;
	float dOverLr4 = dOverLr2 * dOverLr2;
	float fallNum = clamp(1.0 - dOverLr4, 0.0, 1.0);
	float attenuation = fallNum * fallNum / (distSq + 1.0);

	float epsilon = light.angle.y - outerCosAngle;
	float intensity = clamp((theta - outerCosAngle) / epsilon, 0.0, 1.0);
		
	info.L = L;
	info.halfWayProduct = max(dot(pixel.normal, normalize(globalInfo.V + L)), 0.0);
	info.NdL = max(dot(pixel.normal, L), 0.0);
	
	vec4 clip;
	if (light.shadowMapInfoIndex != -1 || light.cookie_ID != 0) {
		clip = sceneShadowMapInfos.shadowMaps[ light.shadowMapInfoIndex].lightSpaceMatrix * vec4(pixel.positionWorld, 1.0);	
		clip.xyz /= clip.w;
	}
	
	vec3 shadow = vec3(1.0);
	if (light.shadowMapInfoIndex != -1) {
#ifdef S3DSDEF_SHADER_PERMUTATION_SpotShadowMapType_Variance
		if (sceneShadowMapInfos.shadowMaps[ light.shadowMapInfoIndex].shadowMapTranslucent_ID != 0) {
			shadow = getTranslucentVarianceShadow(clip, sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex]);
		} else {
			shadow = vec3(getVarianceShadow(clip, sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex]));
		}
#endif 
#ifdef S3DSDEF_SHADER_PERMUTATION_SpotShadowMapType_PCF
		float bias = sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex].bias * mix(4.0, 0.1, max(dot(pixel.dnormal, L), 0.0)); 
		if (sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex].shadowMapTranslucent_ID != 0) {
			shadow = getTranslucentPCFShadow(clip, bias, sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex]);
		} else {
			shadow = vec3(getPCFShadow(clip, -fragToLight, -L, bias, sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex], light.radius));
		}
#endif
	};
	vec3 cookie = vec3(1.0);
	if (light.cookie_ID != 0) {
		cookie = applyLightCookie(tex2DID[light.cookie_ID], clip);
	}
	info.radiance = intensity * attenuation * light.color.rgb * shadow * cookie;
	return info;
}
float fun(float L, float A) {
	float al = atan(L);
	return al - (2.*al)/pow(A,4.)+al/pow(A,4.)+pow(L,7.)/(7.*pow(A,8.))-pow(L,5.)/(5.*pow(A,8.))+pow(L,3.)/(3.*pow(A,8.))-2.*pow(L,3.)/(3.*pow(A,4))+2.*L/pow(A,4)-L/pow(A,8);
}

LightingInfo getPointLightingComplexInfo(GlobalFragmentInfo globalInfo, ShadedPixelInfo pixel, PointLightComplex light) {
	LightingInfo info;
	info.skip = false;
	vec3 fragToLight0 = light.positionPipeEnd0.xyz - pixel.positionWorld;
	vec3 closestPoint = fragToLight0;
	float attenuation = 1.0;
	
	float radiusSq = light.radius * light.radius;
	float distSq = 0.0;
	
	vec3 shadowL = 0..xxx;
	if (light.isPipe) {
		vec3 fragToLight1 = light.positionPipeEnd1.xyz - pixel.positionWorld;
		float magL0 = length(fragToLight0);
		float magL1 = length(fragToLight1);
		vec3 L0 = fragToLight0 / magL0;
		vec3 L1 = fragToLight1 / magL1;
		float NdL0 = max(dot(pixel.normal, fragToLight0) / (2.0 * magL0), 0.0);
		float NdL1 = max(dot(pixel.normal, fragToLight1) / (2.0 * magL1), 0.0);
		info.NdL = 2.0 * clamp(NdL0 + NdL1, 0.0, 1.0);
	
		distSq = magL0 * magL1 + dot(fragToLight0, fragToLight1);
	
		float dOverLr2 = distSq / radiusSq;
		float dOverLr4 = dOverLr2 * dOverLr2;
		float fallNum = clamp(1.0 - dOverLr4, 0.0, 1.0);
		attenuation = fallNum / (distSq + 1.0);
		vec3 Ld = fragToLight1 - fragToLight0;
		float L0dLd = dot(fragToLight0, Ld);
		float RdL0 = dot(globalInfo.R, fragToLight0);
		float RdLd = dot(globalInfo.R, Ld);
		float t = clamp((RdL0 * RdLd - L0dLd) / (dot(Ld, Ld) - RdLd * RdLd), 0.0, 1.0);
	
		closestPoint += Ld * t;
		shadowL = (fragToLight1 + fragToLight0) / 2.0;
	}
	vec3 fragToLightCent = closestPoint;
	vec3 centerToRay = dot(closestPoint, globalInfo.R) * globalInfo.R - closestPoint;
	vec3 fragToLight = closestPoint + centerToRay * clamp(light.sourceRadius / length(centerToRay), 0.0, 1.0);
	if (!light.isPipe) {
		info.NdL = max(dot(pixel.normal, fragToLight), 0.0);
		shadowL = fragToLight0;
		distSq = dot(fragToLight, fragToLight);
		float dOverLr2 = distSq / radiusSq;
		float dOverLr4 = dOverLr2 * dOverLr2;
		float fallNum = clamp(1.0 - dOverLr4, 0.0, 1.0);
		attenuation = fallNum * fallNum / (distSq + 1.0);
	}
	
#ifndef DISABLE_LIGHT_CULLING
	if (distSq > radiusSq) {	// light culling
		info.skip = true;
		return info;
	}
#endif
	
	vec3 L = normalize(fragToLight);
	info.L = L;
	info.halfWayProduct = max(dot(pixel.normal, normalize(globalInfo.V + L)), 0.0);
	//info.halfWayProduct = max(L0 + t * Ld, 0) + srcRadius/(PI * fragToLightDist)
	vec3 shadow = vec3(1.0);
	if (light.shadowMapInfoIndex != -1) {
		float bias = sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex].bias * mix(20.0, 0.1, max(dot(pixel.dnormal, L), 0.0)); 
		shadow = vec3(getPCFShadow(vec4(0.0), shadowL, normalize(shadowL), bias, sceneShadowMapInfos.shadowMaps[light.shadowMapInfoIndex], light.radius));
	}
	
	info.radiance = attenuation * light.color.rgb * shadow;	
	return info;
}

LightingInfo getPointLightingInfo(GlobalFragmentInfo globalInfo, ShadedPixelInfo pixel, PointLight light) {
	LightingInfo info;
	info.skip = false;
	vec3 fragToLight = light.position.xyz - pixel.positionWorld;
	float distSq = dot(fragToLight, fragToLight);
	float radiusSq = light.radius * light.radius;
#ifndef DISABLE_LIGHT_CULLING
	if (distSq > radiusSq) {	// light culling
		info.skip = true;
		return info;
	}
#endif
	float dOverLr2 = distSq / radiusSq;
	float dOverLr4 = dOverLr2 * dOverLr2;
	float fallNum = clamp(1.0 - dOverLr4, 0.0, 1.0);
	float attenuation = fallNum * fallNum / (distSq + 1.0);

	vec3 L = normalize(fragToLight);
	info.L = L;
	
	info.halfWayProduct = max(dot(pixel.normal, normalize(globalInfo.V + L)), 0.0);
	info.NdL = max(dot(pixel.normal, info.L), 0.0);
	vec3 shadow = vec3(1.0);
	if (light.shadowMapInfoIndex != -1) {
		float bias = sceneShadowMapInfos.shadowMaps[ light.shadowMapInfoIndex].bias* mix(20.0, 0.1, max(dot(pixel.dnormal, L), 0.0));
		shadow = vec3(getPCFShadow(vec4(0.0), fragToLight, info.L, bias, sceneShadowMapInfos.shadowMaps[ light.shadowMapInfoIndex], light.radius));
	}
	
	info.radiance = attenuation * light.color.rgb * shadow;		
	return info;
}


vec3 disperseSample(vec3 offsetFactor, sampler2D tex, vec2 dir) {
	vec2 sR = dir.xy * offsetFactor.r;
	vec2 sG = dir.xy * offsetFactor.g;
	vec2 sB = dir.xy * offsetFactor.b;
	
	return vec3(textureLod(tex, sR, 0.0).r, textureLod(tex, sG, 0.0).g, textureLod(tex, sB, 0.0).b);
}

vec3 calculateScreenSpaceReflection(ShadedPixelInfo pixel, float ior, float dispersionFactor, sampler2D deferredTexture) {
	GlobalFragmentInfo gFragInfo = getGlobalFragmentInfo(pixel.positionWorld, pixel.normal);
	
	vec3 Rf = reflect(-gFragInfo.V, pixel.normal);
	vec3 vRf = mat3(ubo.view) * Rf; 
	vec4 scRf = ubo.projection * vec4(vRf, 1.0);
	scRf.xyz /= scRf.w;
	scRf.z = scRf.z * 2.0 - 1.0;	
	float weight = (1.0 - smoothstep(0.90, 1.00, abs(scRf.x))) * (1.0 - smoothstep(0.90, 1.00, abs(scRf.y))) * (1.0 - smoothstep(0.90, 1.00, abs(scRf.z)));
	
	vec3 b = getReflectionData(gFragInfo, pixel);
	
	scRf.xy = scRf.xy * 0.5 + 0.5;
	
	//float fade = min(10.0 * (1.0 - abs(scRf.x)), 1.0) * min(10.0 * (1.0 - abs(scRf.y)), 1.0);
	//
	//vec3 aux = vec3(0.0);
	//if (fade < 1.0) {
	//	aux += textureLod(deferredTexture, gl_FragCoord.xy / ubo.screenSize, 0.0).rgb * (1.0 - fade); 
	//}

	return /*aux + fade * */ mix(b, textureLod(deferredTexture, scRf.xy, 0.0).rgb, weight);
}

vec3 calculatePureRefraction(ShadedPixelInfo pixel, float ior, float dispersionFactor, sampler2D deferredTexture) {
	GlobalFragmentInfo gFragInfo = getGlobalFragmentInfo(pixel.positionWorld, pixel.normal);
	float fresnelFc = basicFresnel(gFragInfo.NdVa); 
	float fresnelFcS = basicFresnel(max(dot(pixel.normal, -gFragInfo.V), 0.0)); 
	float fresnelTerm = fresnelFc * (fresnelFc - 0.24);
	
	// 1.0 - fresnel is correct, however mixing between fresnels gives cool effects
	float factor = 1.0 - fresnelFcS;
	//float factor = 1.0 - mix(fresnelFc, fresnelFcS, fresnelTerm); 
 
	//vec3 Rf = refract(-gFragInfo.V, pixel.normal, 1.0 + factor * (ior - 1.0));
	vec3 Rf = refract(-gFragInfo.V, pixel.normal, 2.0 - ior);
	vec3 vRf = mat3(ubo.view) * Rf; 
	vec4 scRf = ubo.projection * vec4(vRf, 1.0);
	scRf.xy /= scRf.w;
	
	const float MIX_BOUNDS = 0.85;
	
	float weight = (1.0 - linstep(MIX_BOUNDS, 1.00, abs(scRf.x))) * (1.0 - linstep(MIX_BOUNDS, 1.00, abs(scRf.y))) ;
	scRf.xy = scRf.xy * 0.5 + 0.5;
	//float fade = min(10.0 * (1.0 - abs(scRf.x)), 1.0) * min(10.0 * (1.0 - abs(scRf.y)), 1.0);
	//
	//vec3 aux = vec3(0.0);
	//if (fade < 1.0) {
	//	aux += textureLod(deferredTexture, gl_FragCoord.xy / ubo.screenSize, 0.0).rgb * (1.0 - fade); 
	//}

	if (dispersionFactor != 0.0) { 
		return /*aux + fade * */ disperseSample(mix(vec3(1.0), vec3(0.85, 0.93, 0.98), fresnelTerm * dispersionFactor), deferredTexture, scRf.xy).rgb;
	} else {
		return /*aux + fade * */ mix(textureLod(deferredTexture, vec2(gl_FragCoord.xy)/vec2(ubo.screenSize - 1), 0.0).rgb, textureLod(deferredTexture, scRf.xy, 0.0).rgb, weight);
	}
}


vec3 calculatePureReflectionRefraction(ShadedPixelInfo pixel, float ior, float opacity, float dispersionFactor, sampler2D deferredTexture) {
	GlobalFragmentInfo gFragInfo = getGlobalFragmentInfo(pixel.positionWorld, pixel.normal);
	vec3 reflection = getReflectionData(gFragInfo, pixel);
	vec3 refraction = calculatePureRefraction(pixel, ior, dispersionFactor, deferredTexture);
	float fresnelFc = basicFresnel(gFragInfo.NdVa); 
	float transmission = mix(opacity, 1.0, fresnelFc); 
	
	return mix(refraction, reflection, transmission);
}


vec3 calculateLightShaded(ShadedPixelInfo pixel, float ssao) {

	GlobalFragmentInfo gFragInfo = getGlobalFragmentInfo(pixel.positionWorld, pixel.normal);
	EnvironmentLightingInfo envInfo = getEnvironmentLightingData(gFragInfo, pixel, ssao);
	float fresnelFc = basicFresnel(gFragInfo.NdVa); 
	float fresnelTerm = mix(1.0, fresnelFc * (fresnelFc - 0.24), pixel.diffuse.w) * ssao;
	envInfo.reflection *= fresnelTerm;
	
	vec3 diffuseOutput = envInfo.ambient * envInfo.ambientOcclusion;
	vec3 specularOutput = vec3(0.0);
	
	for (int i = 0; i < sceneOmniLights.numPointLights; i++) {
		LightingInfo info = getPointLightingInfo(gFragInfo, pixel, sceneOmniLights.pointLights[i]);
		if (info.skip) continue;
		
		LightResult result = calculateLightFromSource(gFragInfo, info, pixel);
		diffuseOutput += result.diffuse;
		specularOutput += result.specular;
	}
	for (int i = 0; i < sceneOmniComplexLights.numPointLights; i++) {
		LightingInfo info = getPointLightingComplexInfo(gFragInfo, pixel, sceneOmniComplexLights.pointLights[i]);
		if (info.skip) continue;
		
		LightResult result = calculateLightFromSource(gFragInfo, info, pixel);
		diffuseOutput += result.diffuse;
		specularOutput += result.specular;
	}
	for (int i = 0; i < sceneOmniLights.numSpotLights; i++) {
		LightingInfo info = getSpotLightingInfo(gFragInfo, pixel, sceneOmniLights.spotLights[i]);
		if (info.skip) continue;
		
		LightResult result = calculateLightFromSource(gFragInfo, info, pixel);
		diffuseOutput += result.diffuse;
		specularOutput += result.specular;
	}
	for (int i = 0; i < sceneOmniComplexLights.numSpotLights; i++) {
		LightingInfo info = getSpotLightingComplexInfo(gFragInfo, pixel, sceneOmniComplexLights.spotLights[i]);
		if (info.skip) continue;
		
		LightResult result = calculateLightFromSource(gFragInfo, info, pixel);
		diffuseOutput += result.diffuse;
		specularOutput += result.specular;
	}

	for (int i = 0; i < sceneEnvironment.numDirectionalLights; i++) {
		const vec3 SUN_TINT = vec3(1.0, 0.9834, 0.8843) ; // made to look a bit more like the sun
		LightingInfo info = getDirectionalLightingInfo(gFragInfo, pixel, sceneEnvironment.directionalLights[i]);
		if (info.skip) { 
			continue;	
		}
		
		LightResult result = calculateLightFromSource(gFragInfo, info, pixel);
		diffuseOutput += result.diffuse;
		specularOutput += result.specular * SUN_TINT; // specular boost for highlights
	}
	
	vec3 result = mix(envInfo.reflection,
#ifdef S3DSDEF_SHADER_PERMUTATION_DiffuseLightingAffectsPartialReflections
	* mix(dot(diffuseOutput, luma), 1.0, pixel.diffuse.w * pixel.diffuse.w * pixel.diffuse.w)
#endif
		diffuseOutput * pixel.diffuse.rgb, pixel.diffuse.w) + specularOutput;
		
	return applyExponentialFog(result, pixel.positionWorld, ubo.invView[3].xyz);
}
#endif