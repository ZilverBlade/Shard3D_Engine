#ifndef SCENE_BUFFERS_GLSL
#define SCENE_BUFFERS_GLSL

struct Skylight {
	vec4 reflectionTint; // skybox may provide custom tint
	int dynamicSky;
	uint reflection_id;
	uint irradiance_id;
};

struct Skybox {
	vec4 tint;
	uint skybox_id;
};
const int SHADOW_MAP_CONVENTION_CONVENTIONAL = 1;
const int SHADOW_MAP_CONVENTION_PARABOLOID = 2;
const int SHADOW_MAP_CONVENTION_DUAL_PARABOLOID = 3;
const int SHADOW_MAP_CONVENTION_CUBE_MAP = 4;
const int SHADOW_MAP_CONVENTION_PARALLEL_SPLIT2 = 5;
const int SHADOW_MAP_CONVENTION_PARALLEL_SPLIT3 = 6;
const int SHADOW_MAP_CONVENTION_PARALLEL_SPLIT4 = 7;

struct ShadowMapInfo {
	mat4 lightSpaceMatrix;
	int shadowMapConvention;
	uint shadowMap_ID;
	uint shadowMapTranslucent_ID;
	uint shadowMapTranslucentColor_ID;
	uint shadowMapTranslucentReveal_ID;
	float bias; // either variance probability bias or PCF shadow bias multiplier
	float sharpness; // 0 - 0.99 value for shadow sharpness filter
	// PCF exclusive stuff
	float poissonRadius;
	uint poissonSamples;
};
struct PointLight {
	vec3 position;
	int shadowMapInfoIndex;
	vec3 color;
	float radius;
};
struct PointLightComplex {
	vec4 positionPipeEnd0;
	vec4 positionPipeEnd1;
	vec4 color;
	float radius;
	float sourceRadius;
	bool isPipe;
	int shadowMapInfoIndex;
};

struct SpotLight {
	vec4 position;
	vec4 color;
	vec3 direction;
	int shadowMapInfoIndex;
	vec2 angle; //outer, inner
	uint cookie_ID;
	float radius;
};
struct SpotLightComplex {
	vec4 position;
	vec4 color;
	vec3 direction;
	int shadowMapInfoIndex;
	vec2 angle; //outer, inner
	uint cookie_ID;
	float sourceRadius;
	float radius;
};

struct DirectionalLight {
	vec4 position;
	vec4 color;
	vec3 direction;
	int shadowMapInfoIndex;
};
struct BoxReflectionCube {
	mat4 invTransform;
	vec4 color; // w intensity
	vec3 boundsScale;
	float intersectionDist;
	uint reflection_id;
	uint irradiance_id;
};
struct PlanarReflection {
	vec4 color;
	vec4 cartesianPlane;
	float maxHeight;
	float intersectionDist;
	float distortDuDvFactor;
	float distortionDeltaLimit;
	uint reflection_id;
	uint irradiance_id;
};
struct BoxAmbientOcclusionVolume {
	mat4 invTransform;
	vec4 tint_p_mOccl;
	vec3 boundsScale;
	float intersectionDist;
};
struct LightPropagationVolume {
	vec4 color; // w intensity
	vec3 center;
	vec3 boundsScale;
	float fadeRatio;
	uint redvoxel_id;
	uint greenvoxel_id;
	uint bluevoxel_id;
};

struct ExponentialFog {
	float density;
	float falloff;
	float strength;
	float sunScattPow;
	vec4 color;
	int updownbias;
};

layout(set = 1, binding = 0) uniform OmniLightUBO {
	PointLight pointLights[384];
	int numPointLights;	
	SpotLight spotLights[384];
	int numSpotLights;
} sceneOmniLights;
layout(set = 1, binding = 1) uniform OmniLightComplexUBO {
	PointLightComplex pointLights[256];
	int numPointLights;	
	SpotLightComplex spotLights[256];
	int numSpotLights;
} sceneOmniComplexLights;
layout(set = 1, binding = 2) uniform ShadowMapInfoUBO {
	ShadowMapInfo shadowMaps[256];
} sceneShadowMapInfos;

layout(set = 1, binding = 3) uniform EnvironmentUBO {
	DirectionalLight directionalLights[16];
	int numDirectionalLights;

	Skylight skylights[16];
	int numSkylights;
	
	ExponentialFog exponentialFogs[16];	
	int numExponentialFogs;
	
	Skybox activeSkybox;
} sceneEnvironment;

layout(set = 1, binding = 4) uniform SotAFXUBO {
	BoxReflectionCube reflectionCubes[128];
	int numReflectionCubes;

	PlanarReflection reflectionPlanes[8];
	int numReflectionPlanes;
	
	BoxAmbientOcclusionVolume aoVolumes[64];
	int numAOVolumes;
	
	LightPropagationVolume lightPropagationVolumeCascades[3]; // max of 3 cascades
	int numLightPropagationVolumeCascades;
} sceneSotAFX;


#endif