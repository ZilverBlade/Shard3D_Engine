#ifndef MOMENT_SHADOW_GLSL
#define MOMENT_SHADOW_GLSL

const mat4 momentMatrix = mat4(
	vec4(1.5, 0.0, sqrt(3) / 2, 0.0),
	vec4(0.0, 4.0, 0.0, 0.5),
	vec4(-2.0, 0.0, -sqrt(12) / 9, 0.0),
	vec4(0.0, -4.0, 0.0, 0.5)
);
const mat4 invMomentMatrix = mat4(
	vec4(-1 / 3, 0.0, -0.75, 0.0),
	vec4(0.0, 0.125, 0.0, -0.125),
	vec4(sqrt(3), 0.0, 0.75 * sqrt(3), 0.0),
	vec4(0.0, 1.0, 0.0, 1.0)
);


#endif