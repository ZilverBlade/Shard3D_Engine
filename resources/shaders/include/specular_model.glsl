#ifndef SPECULAR_MODEL_GLSL
#define SPECULAR_MODEL_GLSL

float linearSpecularExponent(float glossiness) {
	return exp(6.283185307 * glossiness - 6.283185307) * glossiness;
}

float specularExponent(float halfWayProduct, float glossiness) {
#ifdef S3DSDEF_SHADER_PERMUTATION_SpecularModel_Gaussian
	float angleNormalHalf = max(acos(halfWayProduct), 0.0001);
	float exponent = angleNormalHalf * linearSpecularExponent(glossiness) * 64.0;
	return exp(-(exponent * exponent));
#endif
#ifdef S3DSDEF_SHADER_PERMUTATION_SpecularModel_BlinnPhong
	return pow(max(clamp(halfWayProduct, 0.0, 1.0), 0), linearSpecularExponent(glossiness) * 695.87968);
#endif
}

float blinnPhongSpecular(float NdH, float glossiness) {
	float g2 = glossiness * glossiness;
	float specularBrightness = 3.14159265359 * g2*g2 + 3.14159265359 / 64.0; // normalize exponent
	return specularBrightness * specularExponent(NdH, glossiness);
}
#endif