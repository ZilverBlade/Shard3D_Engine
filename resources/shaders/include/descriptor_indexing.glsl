// Bindless descriptor set definitions and helpers
#ifndef DESCRIPTOR_INDEXING_GLSL
#define DESCRIPTOR_INDEXING_GLSL

#ifndef DESCRIPTOR_BINDLESS_SET
//#define DESCRIPTOR_BINDLESS_SET 2
#error You must define the bindless descriptor set with DESCRIPTOR_BINDLESS_SET <int>
#endif

#extension GL_EXT_nonuniform_qualifier : enable
#define READONLY_BUFFER layout(set = DESCRIPTOR_BINDLESS_SET, binding = S3DSDEF_STORAGE_BUFFER_BINDING) buffer
#define SAMPLER2D_TEXTURE(myVar) layout(set = DESCRIPTOR_BINDLESS_SET, binding = S3DSDEF_COMBINED_SAMPLER_BINDING) uniform sampler2D myVar[]
#define SAMPLER3D_TEXTURE(myVar) layout(set = DESCRIPTOR_BINDLESS_SET, binding = S3DSDEF_COMBINED_SAMPLER_BINDING) uniform sampler3D myVar[]
#define SAMPLERCUBE_TEXTURE(myVar) layout(set = DESCRIPTOR_BINDLESS_SET, binding = S3DSDEF_COMBINED_SAMPLER_BINDING) uniform samplerCube myVar[]
#endif