// Made by ZilverBlade 2022-10-11
#ifndef COMPRESSION_GLSL
#define COMPRESSION_GLSL

#ifdef __cplusplus
typedef unsigned int COMPRESSIONGLSL_uint;
typedef glm::vec2 COMPRESSIONGLSL_vec2;
typedef glm::vec3 COMPRESSIONGLSL_vec3;
typedef glm::vec4 COMPRESSIONGLSL_vec4;
typedef glm::uvec3 COMPRESSIONGLSL_uvec3;
typedef glm::uvec2 COMPRESSIONGLSL_uvec2;
#define GLSLFUNCDECL static
#else
#define GLSLFUNCDECL
#define COMPRESSIONGLSL_uint uint
#define COMPRESSIONGLSL_vec2 vec2
#define COMPRESSIONGLSL_vec3 vec3
#define COMPRESSIONGLSL_vec4 vec4
#define COMPRESSIONGLSL_uvec3 uvec3
#define COMPRESSIONGLSL_uvec2 uvec2
#endif

// pack float from range 0.0 to 1.0, with 8 bits of precision
GLSLFUNCDECL COMPRESSIONGLSL_uint pack_float8b_unsigned(float x) {
	return COMPRESSIONGLSL_uint(x * 255.0);
}

// pack float from range -1.0 to 1.0, with 7 bits of precision
GLSLFUNCDECL COMPRESSIONGLSL_uint pack_float8b_signed(float x) {
	return COMPRESSIONGLSL_uint((x + 1.0) * 127.0);
}

// unpack float from range 0.0 to 1.0, with 8 bits of precision
GLSLFUNCDECL float unpack_float8b_unsigned(COMPRESSIONGLSL_uint x, COMPRESSIONGLSL_uint pos, COMPRESSIONGLSL_uint offset) {
	return float((x & pos) >> offset) / 255.0;
}

// unpack float from range -1.0 to 1.0, with 7 bits of precision
GLSLFUNCDECL float unpack_float8b_signed(COMPRESSIONGLSL_uint x, COMPRESSIONGLSL_uint pos, COMPRESSIONGLSL_uint offset) {
	return float(((x & pos) >> offset)) / 127.0 - 1.0;
}

// pack float from range 0.0 to 1.0, with 10 bits of precision
GLSLFUNCDECL COMPRESSIONGLSL_uint pack_float10b_unsigned(float x) {
	return COMPRESSIONGLSL_uint(x * 1023.0);
}

// pack float from range -1.0 to 1.0, with 9 bits of precision
GLSLFUNCDECL COMPRESSIONGLSL_uint pack_float10b_signed(float x) {
	return COMPRESSIONGLSL_uint((x + 1.0) * 511.0);
}
// pack float from range -1.0 to 1.0, with 10 bits of precision
GLSLFUNCDECL COMPRESSIONGLSL_uint pack_float11b_signed(float x) {
	return COMPRESSIONGLSL_uint((x + 1.0) * 1023.0);
}

// unpack float from range 0.0 to 1.0, with 10 bits of precision
GLSLFUNCDECL float unpack_float10b_unsigned(COMPRESSIONGLSL_uint x, COMPRESSIONGLSL_uint pos, COMPRESSIONGLSL_uint offset) {
	return float((x & pos) >> offset) / 1023.0;
}

// unpack float from range -1.0 to 1.0, with 9 bits of precision
GLSLFUNCDECL float unpack_float10b_signed(COMPRESSIONGLSL_uint x, COMPRESSIONGLSL_uint pos, COMPRESSIONGLSL_uint offset) {
	return float(((x & pos) >> offset)) / 511.0 - 1.0;
}
// unpack float from range -1.0 to 1.0, with 10 bits of precision
GLSLFUNCDECL float unpack_float11b_signed(COMPRESSIONGLSL_uint x, COMPRESSIONGLSL_uint pos, COMPRESSIONGLSL_uint offset) {
	return float((x & pos) >> offset) / 1023.0 - 1.0;
}

#define LOWP_NVEC3_UNPACK(COMPRESSIONGLSL_uintType) COMPRESSIONGLSL_vec3(unpack_float11b_signed(COMPRESSIONGLSL_uintType, 0xffe00000, 21), unpack_float11b_signed(COMPRESSIONGLSL_uintType, 0x001ffc00, 10), unpack_float10b_signed(COMPRESSIONGLSL_uintType, 0x000003ff, 0))

// pack float from range 0.0 to 1.0, with 16 bits of precision
GLSLFUNCDECL COMPRESSIONGLSL_uint pack_float16b_unsigned(float x) {
	return COMPRESSIONGLSL_uint(x * 65535.0);
}

// pack float from range -1.0 to 1.0, with 15 bits of precision
GLSLFUNCDECL COMPRESSIONGLSL_uint pack_float16b_signed(float x) {
	return COMPRESSIONGLSL_uint((x + 1.0) * 32767.0);
}

// unpack float from range 0.0 to 1.0, with 16 bits of precision
GLSLFUNCDECL float unpack_float16b_unsigned(COMPRESSIONGLSL_uint x, COMPRESSIONGLSL_uint pos, COMPRESSIONGLSL_uint offset) {
	return float((x & pos) >> offset) / 65535.0;
}

// unpack float from range -1.0 to 1.0, with 15 bits of precision
GLSLFUNCDECL float unpack_float16b_signed(COMPRESSIONGLSL_uint x, COMPRESSIONGLSL_uint pos, COMPRESSIONGLSL_uint offset) {
	return float(((x & pos) >> offset)) / 32766.0 - 1.0;
}


// unpack an unsigned 1 byte integer
GLSLFUNCDECL COMPRESSIONGLSL_uint unpack_integer_unsigned(COMPRESSIONGLSL_uint x, COMPRESSIONGLSL_uint pos, COMPRESSIONGLSL_uint offset) {
	return COMPRESSIONGLSL_uint(x & pos) >> offset;
}

// combine 2 individual 16 bit values into a 4 byte integer
GLSLFUNCDECL COMPRESSIONGLSL_uint cram_bytes(COMPRESSIONGLSL_uint x, COMPRESSIONGLSL_uint y) {
	return COMPRESSIONGLSL_uint(x << 16 | y);
}

// combine 3 individual 10 bit values into a 4 byte integer
GLSLFUNCDECL COMPRESSIONGLSL_uint cram_bytes_x10y10z10(COMPRESSIONGLSL_uint x, COMPRESSIONGLSL_uint y, COMPRESSIONGLSL_uint z) {
	return COMPRESSIONGLSL_uint(x << 20 | y << 10 | z);
}
// combine 3 individual 10 bit values into a 4 byte integer
GLSLFUNCDECL COMPRESSIONGLSL_uint cram_bytes_x11y11z10(COMPRESSIONGLSL_uint x, COMPRESSIONGLSL_uint y, COMPRESSIONGLSL_uint z) {
	return COMPRESSIONGLSL_uint(x << 21 | y << 10 | z);
}

// combine 4 individual 8 bit values into a 4 byte integer
GLSLFUNCDECL COMPRESSIONGLSL_uint cram_bytes(COMPRESSIONGLSL_uint x, COMPRESSIONGLSL_uint y, COMPRESSIONGLSL_uint z, COMPRESSIONGLSL_uint w) {
	return COMPRESSIONGLSL_uint(x << 24 | y << 16 | z << 8 | w);
}

// combine 2 individual 8 bit values + 1 16 bit value into a 4 byte integer
GLSLFUNCDECL COMPRESSIONGLSL_uint cram_bytes_x8y8z16(COMPRESSIONGLSL_uint x, COMPRESSIONGLSL_uint y, COMPRESSIONGLSL_uint z) {
	return COMPRESSIONGLSL_uint(x << 24 | y << 16 | z);
}

GLSLFUNCDECL COMPRESSIONGLSL_vec3 cartesianToPolar(COMPRESSIONGLSL_vec3 c) {
#ifdef __cplusplus
	using namespace glm;
#endif
	float x2y2 = c.x * c.x + c.y * c.y;
	float r = sqrt(x2y2 + c.z * c.z);
	float theta = acos(x2y2 < 1.e-10f ? sign(c.z) : c.z / r);
	float phi = sign(c.y) * acos(x2y2 < 1.e-10f ? 1.f : c.x / sqrt(x2y2));
	return COMPRESSIONGLSL_vec3(theta, phi, r);
}
GLSLFUNCDECL COMPRESSIONGLSL_vec3 polarToCartesian(COMPRESSIONGLSL_vec3 p) {
	COMPRESSIONGLSL_vec3 c;
	float sintheta = p.z * sin(p.x);
	c.x = sintheta * cos(p.y);
	c.y = sintheta * sin(p.y);
	c.z = p.z * cos(p.x);
	return c;
}
#ifdef __cplusplus
#include <half.h>
#define COMPRESSIONGLSL_TOFLOAT16(rhs) FLOAT16::ToFloat16(rhs)
#define COMPRESSIONGLSL_FROMFLOAT16(rhs) FLOAT16::Tofloat(rhs)
#endif
#ifndef __cplusplus

// combine 3 component normals into 32 bits of data
//GLSLFUNCDECL COMPRESSIONGLSL_uint cram_32bnormal(COMPRESSIONGLSL_vec3 nor) {
//	vec2 xy = nor.xy;
//	COMPRESSIONGLSL_uint zSign = COMPRESSIONGLSL_uint(nor.z < 0.0);
//	COMPRESSIONGLSL_uint facing = 
//#ifdef NORMAL_COMPRESSION_INCLUDE_FRONT_FACE_DATA
//		COMPRESSIONGLSL_uint(gl_FrontFacing == false);
//#else
//		0;
//#endif
//	float xS = round(nor.x * 16383.0) + 16383.0;
//	float yS = round(nor.y * 16383.0) + 16383.0;
//	return ((COMPRESSIONGLSL_uint(xS) & 0x7FFF) << 17u) | (facing << 16u) | ((COMPRESSIONGLSL_uint(yS) & 0x7FFF) << 1u) | zSign ;
//}
//
//// extract 3 component normals from 32 bits of data, 4th component is if it's a front facing or back facing normal
//GLSLFUNCDECL vec4 unpack_normal(COMPRESSIONGLSL_uint data) {
//	COMPRESSIONGLSL_uint zSign = data & 0x01;
//	COMPRESSIONGLSL_uint facing = data & 0x00010000;
//	COMPRESSIONGLSL_uint xS = (data & 0xFFFE0000) >> 17;
//	COMPRESSIONGLSL_uint yS = (data & 0x0000FFFE) >> 1;
//	float x = (float(xS) - 16383.0) / 16383.0;
//	float y = (float(yS) - 16383.0) / 16383.0;
//	float z = (zSign == 1 ? -1.0 : 1.0) * sqrt(max(1.0 - x*x - y*y, 0.0));
//	float f = (facing == 1 ? -1.0 : 1.0);
//	
//	return vec4(normalize(COMPRESSIONGLSL_vec3(x,y,z)),f);
//}

GLSLFUNCDECL COMPRESSIONGLSL_uint cram_32bnormal(COMPRESSIONGLSL_vec3 nor) {
	COMPRESSIONGLSL_vec3 mor = nor.xyz;
	COMPRESSIONGLSL_uint id = 0;

#ifdef BRANCHLESS_NORMAL_COMPRESSION
	bool a = bool(abs(nor.y) > abs(mor.x));
	mor = mor * uint(!a) + nor.yzx * COMPRESSIONGLSL_uint(a);
	id = 1 * COMPRESSIONGLSL_uint(a);

	bool b = bool(abs(nor.z) > abs(mor.x));
	mor = mor * COMPRESSIONGLSL_uint(!b) + nor.zxy * COMPRESSIONGLSL_uint(b);
	id = 2 * COMPRESSIONGLSL_uint(b);
#else
	if (abs(nor.y) > abs(mor.x)) { mor = nor.yzx; id = 1; }
	if (abs(nor.z) > abs(mor.x)) { mor = nor.zxy; id = 2; }
#endif

	COMPRESSIONGLSL_uint fa = 
#ifdef NORMAL_COMPRESSION_INCLUDE_FRONT_FACE_DATA
		COMPRESSIONGLSL_uint(gl_FrontFacing == false);
#else
		0;
#endif
	COMPRESSIONGLSL_uint is = COMPRESSIONGLSL_uint(mor.x < 0.0);

	COMPRESSIONGLSL_vec2 uv = 0.5 + 0.5*mor.yz/abs(mor.x);
	COMPRESSIONGLSL_uvec2 iuv = COMPRESSIONGLSL_uvec2(round(uv*COMPRESSIONGLSL_vec2(16383.0,16383.0)));

	return COMPRESSIONGLSL_uint(iuv.x | (iuv.y<<14u) | (id<<28u) | (is<<30u) | (fa<<31u));
}

// extract 3 normals from 32 bits of data
COMPRESSIONGLSL_vec4 unpack_normal(COMPRESSIONGLSL_uint data) {
	COMPRESSIONGLSL_uvec2 iuv = COMPRESSIONGLSL_uvec2( data, data>>14u ) & uvec2(16383u,16383u);
	COMPRESSIONGLSL_vec2 uv = COMPRESSIONGLSL_vec2(iuv)*2.0/COMPRESSIONGLSL_vec2(16383.0,16383.0) - 1.0;
	
	COMPRESSIONGLSL_uint is = (data>>30u)&1u;
	COMPRESSIONGLSL_uint fa = (data>>31u)&1u;

	COMPRESSIONGLSL_vec3 nor = vec3(uintBitsToFloat(floatBitsToUint(1.0) | COMPRESSIONGLSL_uint(is!=0u) << 31u), uv.xy);

	COMPRESSIONGLSL_uint id = (data>>28u)&3u;
	nor = nor.xyz * COMPRESSIONGLSL_uint(id == 0) + nor.zxy * COMPRESSIONGLSL_uint(id == 1) + nor.yzx * COMPRESSIONGLSL_uint(id == 2);

	return vec4(normalize(nor), fa == 1 ? -1.0 : 1.0);
}




// maybe?
GLSLFUNCDECL COMPRESSIONGLSL_uint pack_e4b4g4r4_ufloat(COMPRESSIONGLSL_vec3 v){
return 0;

}

// adapted from <glm/gtc/packing.hpp>
GLSLFUNCDECL COMPRESSIONGLSL_uint pack_e5b9g9r9_ufloat(COMPRESSIONGLSL_vec3 v) {
	const float SharedExpMax = (pow(2.0f, 9.0f - 1.0f) / pow(2.0f, 9.0f)) * pow(2.0f, 31.f - 15.f);
	const COMPRESSIONGLSL_vec3 Color = clamp(v, 0.0f, SharedExpMax);
	const float MaxColor = max(Color.x, max(Color.y, Color.z));

	const float ExpSharedP = max(-15.f - 1.f, floor(log2(MaxColor))) + 1.0f + 15.f;
	const float MaxShared = floor(MaxColor / pow(2.0f, (ExpSharedP - 15.f - 9.f)) + 0.5f);
	float ExpShared = abs(MaxShared - pow(2.0f, 9.0f)) <= 0.00001 ? ExpSharedP + 1.0f : ExpSharedP;
		    
	COMPRESSIONGLSL_uvec3 ColorComp = COMPRESSIONGLSL_uvec3(floor(Color / pow(2.f, (ExpShared - 15.f - 9.f)) + 0.5f));

	return COMPRESSIONGLSL_uint(
		((COMPRESSIONGLSL_uint(ExpShared) << 27) 	& 0xF8000000) | 
		((ColorComp.b << 18) 		& 0x07FC0000) | 
		((ColorComp.g << 9) 		& 0x0003FE00) |
		(ColorComp.r 				& 0x000001FF)
	);
}


// adapted from half.h (see license)

//
// FLOAT16 Helpers
//

#define HALF_SIGN_SHIFT                 (15)
#define HALF_EXP_SHIFT                  (10)
#define HALF_MANT_SHIFT                 (0)

#define HALF_SIGN_MASK                  (0x8000)
#define HALF_EXP_MASK                   (0x7C00)
#define HALF_MANT_MASK                  (0x03FF)

#define HALF_POS_INFINITY               (0x7C00)
#define HALF_NEG_INFINITY               (0xFC00)

#define GET_HALF_SIGN_BIT(x)            ((x) >> HALF_SIGN_SHIFT)
#define GET_HALF_EXP_BITS(x)            (((x) >> HALF_EXP_SHIFT) & 0x1F)
#define GET_HALF_MANT_BITS(x)           ((x) & HALF_MANT_MASK)

#define SET_HALF_SIGN_BIT(x,dest)       ((dest) = ((((x) << HALF_SIGN_SHIFT) & HALF_SIGN_MASK) | ( (dest) & ( HALF_EXP_MASK  | HALF_MANT_MASK ))))
#define SET_HALF_EXP_BITS(x,dest)       ((dest) = ((((x) << HALF_EXP_SHIFT)  & HALF_EXP_MASK)  | ( (dest) & ( HALF_SIGN_MASK | HALF_MANT_MASK ))))
#define SET_HALF_MANT_BITS(x,dest)      ((dest) = ((((x) << HALF_MANT_SHIFT) & HALF_MANT_MASK) | ( (dest) & ( HALF_SIGN_MASK | HALF_EXP_MASK ))))


//
// float Helpers
//

#define SINGLE_SIGN_SHIFT               (31)
#define SINGLE_EXP_SHIFT                (23)
#define SINGLE_MANT_SHIFT               (0)

#define SINGLE_SIGN_MASK                (0x80000000)
#define SINGLE_EXP_MASK                 (0x7F800000)
#define SINGLE_MANT_MASK                (0x007FFFFF)

#define SINGLE_POS_INFINITY             (0x7F800000)
#define SINGLE_NEG_INFINITY             (0xFF800000)

#define GET_SINGLE_SIGN_BIT(x)          ((x) >> SINGLE_SIGN_SHIFT)
#define GET_SINGLE_EXP_BITS(x)          (((x) >> SINGLE_EXP_SHIFT) & 0xFF)
#define GET_SINGLE_MANT_BITS(x)         ((x) & SINGLE_MANT_MASK)

#define SET_SINGLE_SIGN_BIT(x,dest)     ((dest) = ((((x) << SINGLE_SIGN_SHIFT) & SINGLE_SIGN_MASK) | ( (dest) & ( SINGLE_EXP_MASK  | SINGLE_MANT_MASK ))))
#define SET_SINGLE_EXP_BITS(x,dest)     ((dest) = ((((x) << SINGLE_EXP_SHIFT)  & SINGLE_EXP_MASK)  | ( (dest) & ( SINGLE_SIGN_MASK | SINGLE_MANT_MASK ))))
#define SET_SINGLE_MANT_BITS(x,dest)    ((dest) = ((((x) << SINGLE_MANT_SHIFT) & SINGLE_MANT_MASK) | ( (dest) & ( SINGLE_SIGN_MASK | SINGLE_EXP_MASK ))))

// converts a 32 bit float to 16 bit float
GLSLFUNCDECL COMPRESSIONGLSL_uint float32bto16b(float rhs) {
	//
	   // (!) Truncation will occur for values outside the representable range for float16.
	   //   

	COMPRESSIONGLSL_uint fOutput;
	COMPRESSIONGLSL_uint uiInput = floatBitsToUint(rhs);

	//
	// Normalized values
	//

	SET_HALF_SIGN_BIT(GET_SINGLE_SIGN_BIT(uiInput), fOutput);
	SET_HALF_EXP_BITS(clamp(GET_SINGLE_EXP_BITS(uiInput) - 127 + 15, 0, 31), fOutput);
	SET_HALF_MANT_BITS(GET_SINGLE_MANT_BITS(uiInput) >> 13, fOutput);

	//
	// ATP: uiOutput equals the bit pattern of our floating point result.
	//

	bool isZero = (rhs == 0.0);
	bool isNegZero = (rhs == -0.0);

	return fOutput * COMPRESSIONGLSL_uint(!isZero) * COMPRESSIONGLSL_uint(!isNegZero) + COMPRESSIONGLSL_uint(isNegZero) * 0x8000; // value will output 0x00000000 if zero anyway
}

// converts a 16 bit float to 32 bit float
GLSLFUNCDECL float float16bto32b(COMPRESSIONGLSL_uint rhs) {
	COMPRESSIONGLSL_uint uiOutput = 0;         // bit manipulated output

	//
	// Normalized values
	//

	SET_SINGLE_SIGN_BIT(GET_HALF_SIGN_BIT(rhs), (uiOutput));
	SET_SINGLE_EXP_BITS((GET_HALF_EXP_BITS(rhs) - 15) + 127, (uiOutput));
	SET_SINGLE_MANT_BITS(GET_HALF_MANT_BITS(rhs) << 13, (uiOutput));

	//
	// ATP: uiOutput equals the bit pattern of our floating point result.
	//

	bool isZero = (rhs == 0);
	bool isNegZero = (rhs == 0x8000);

	COMPRESSIONGLSL_uint out_ = uiOutput * COMPRESSIONGLSL_uint(!isZero) * COMPRESSIONGLSL_uint(!isNegZero) + COMPRESSIONGLSL_uint(isNegZero) * 0x80000000; // value will output 0x00000000 if zero anyway

	return uintBitsToFloat(out_);
}
#define COMPRESSIONGLSL_TOFLOAT16(rhs) float32bto16b(rhs)
#define COMPRESSIONGLSL_FROMFLOAT16(rhs) float16bto32b(rhs)
#endif

GLSLFUNCDECL COMPRESSIONGLSL_uint POLAR_VEC3_PACK(COMPRESSIONGLSL_vec3 vector) {
	COMPRESSIONGLSL_vec3 polar = cartesianToPolar(vector);
	float scaledTheta = polar.x / 3.14159265359;
	float scaledPhi = abs(polar.y) / 3.14159265359;
	COMPRESSIONGLSL_uint ptheta = COMPRESSIONGLSL_uint(scaledTheta * 511.0);
	COMPRESSIONGLSL_uint pphi = COMPRESSIONGLSL_uint(scaledPhi * 511.0);
	COMPRESSIONGLSL_uint scale16 = COMPRESSIONGLSL_TOFLOAT16(polar.z);
	COMPRESSIONGLSL_uint signPhi = polar.y < 0.0 ? 1 : 0;
	scale16 &= 0x00007FFC;
	scale16 >>= 2;

	COMPRESSIONGLSL_uint pack = ptheta << 23 | pphi << 14 | signPhi << 13 | scale16;
	return pack;
}
GLSLFUNCDECL COMPRESSIONGLSL_vec3 POLAR_VEC3_UNPACK(COMPRESSIONGLSL_uint pack) {
	float theta = (float((pack & 0xFF800000) >> 23) / 511.0) * 3.14159265359;
	float phi = (float((pack & 0x007F0000) >> 14) / 511.0) * 3.14159265359;
	COMPRESSIONGLSL_uint scalebits = (pack & 0x00001FFF) << 2;
	COMPRESSIONGLSL_uint signPhi = (pack & 0x00002000) >> 13;
	float scale = COMPRESSIONGLSL_FROMFLOAT16(scalebits);

	return polarToCartesian(COMPRESSIONGLSL_vec3(theta, (signPhi == 1 ? -1.0 : 1.0) * phi, scale));
}
#endif
