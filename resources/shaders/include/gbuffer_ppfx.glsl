#ifndef GBUFFER_PPFX_GLSL
#define GBUFFER_PPFX_GLSL
#include "compression.glsl"
#include "depth_to_worldpos.glsl"

#ifndef GBUFFER_DONT_INCLUDE_SAMPLER_IDENTIFIERS
layout (set = 0, binding = 1) uniform sampler2D inputDepthImage;
layout (set = 0, binding = 2) uniform sampler2D inputGBuffer0; // diffuse
layout (set = 0, binding = 3) uniform sampler2D inputGBuffer1; // material
layout (set = 0, binding = 4) uniform sampler2D inputGBuffer2; // emission
layout (set = 0, binding = 5) uniform usampler2D inputGBuffer3; // uint32 packed normal
#endif
struct GBuffer {
	vec3 normal;

	vec3 materialDiffuse;
	vec3 materialEmission;
	float materialSpecular;
	float materialShininess;
	float materialChrominess;
};

#ifndef GBUFFER_DONT_INCLUDE_SAMPLER_IDENTIFIERS
GBuffer gbufferLoad(vec2 coord) {
	GBuffer scene;
	const vec4 gbf_gbuffer0 = textureLod(inputGBuffer0, coord, 0.0);
	scene.materialDiffuse = gbf_gbuffer0.rgb;

	const vec4 gbf_gbuffer1 = textureLod(inputGBuffer1, coord, 0.0);
	scene.materialSpecular = gbf_gbuffer1.r;
	scene.materialShininess = gbf_gbuffer1.g * 255.0 + 1.0;
	scene.materialChrominess = gbf_gbuffer1.b;
	
	const vec4 gbf_gbuffer2 = textureLod(inputGBuffer2, coord, 0.0);
	scene.materialEmission = gbf_gbuffer2.rgb / gbf_gbuffer2.a;
	const uint gbf_gbuffer3 = textureLod(inputGBuffer3, coord, 0.0).x;
	scene.normal = unpack_normal(gbf_gbuffer3);
	return scene;
}
#endif

#ifndef GBUFFER_DONT_INCLUDE_SAMPLER_IDENTIFIERS
float gbufferDepth(vec2 coord) {
	return texture(inputDepthImage, coord).r;
}
#endif

#ifndef GBUFFER_DONT_INCLUDE_SAMPLER_IDENTIFIERS
vec3 gbufferPosition(vec2 coord, mat4 inverseProjection, mat4 inverseView) {
	return WorldPosFromDepth(coord, gbufferDepth(coord), inverseProjection, inverseView);
}
#endif

#endif