#version 450
#extension GL_GOOGLE_include_directive : enable

const float PI = 3.1415926;
#include "depth.glsl"
layout (constant_id = 0) const float sampleDelta = 0.20;
layout (set = 0, binding = 0) uniform sampler2D planarReflection;
layout (set = 0, binding = 1) uniform sampler2D planarDepthData;

layout (location = 0) in vec2 fragUV;
layout (location = 0) out vec3 outDiffuse;

layout (push_constant) uniform Push {
	mat4 projection;
	mat3 orientation;
	float sampleRadius;
} push;

float magic(vec2 coords){
	return fract(sin(dot(coords, vec2(1242.24591249, -23.15492591))) * 43758.5453);
}

bool outOfBounds(vec2 uv) {
	if (any(greaterThan(abs(uv * 2..xx - 1..xx), 1.01.xx))) {
		return true;
	}
	return false;
}

void main() {
	vec3 irradiance = vec3(0.0); 
	
	float nrSamples = 0.0; 
	float randomOffset = magic(gl_FragCoord.xy);
	for(float phi = 0.0; phi < 2.0 * PI; phi += sampleDelta) {
		for(float theta = 0.0; theta < 0.5 * PI; theta += sampleDelta) {
			// spherical to cartesian (in tangent space)
			
			vec3 tangentSample = vec3(sin(theta) * cos(phi + randomOffset), sin(theta) * sin(phi + randomOffset), cos(phi + randomOffset));
			// tangent space to world
			vec3 sampleVec = /*push.orientation **/ tangentSample;

			vec4 sampleVecClip = push.projection * vec4(sampleVec, 1.0);
			sampleVecClip.xy *= push.sampleRadius.xx ;
			//sampleVecClip.xy /= sampleVecClip.w;
			vec2 sampleUV = sampleVecClip.xy + fragUV;
			if (outOfBounds(sampleUV)) {
				continue;
			}
			irradiance += textureLod(planarReflection, sampleUV, 0.0).rgb * cos(theta) * sin(theta);
			nrSamples++;
		}
	}
	outDiffuse = nrSamples > 0.0 ? irradiance / float(nrSamples) : 0..xxx;
}