#version 450
#extension GL_GOOGLE_include_directive : enable

#include "compression.glsl"
const float PI = 3.1415926;

	
layout (constant_id = 0) const float sampleDelta = 0.025;
layout (set = 0, binding = 0) uniform samplerCube environmentMap;

layout (location = 0) in vec3 fragUV;
layout (location = 0) out uint outDiffuse;

layout (push_constant) uniform Push {
	mat4 mvp;
} push;

// use this for static cubemap generation for correct results

void main() {
	const vec3 N = normalize(vec3(fragUV.x, -fragUV.y, fragUV.z));
	
	vec3 up = vec3(0.0, 1.0, 0.0);
	const vec3 right = normalize(cross(up, N));
    up = cross(N, right);
	
	vec3 irradiance = vec3(0.0); 
	
	float nrSamples = 0.0; 
	for(float phi = 0.0; phi < 2.0 * PI; phi += sampleDelta) {
		for(float theta = 0.0; theta < 0.5 * PI; theta += sampleDelta) {
			// spherical to cartesian (in tangent space)
			vec3 tangentSample = vec3(sin(theta) * cos(phi),  sin(theta) * sin(phi), cos(theta));
			// tangent space to world
			vec3 sampleVec = tangentSample.x * right + tangentSample.y * up + tangentSample.z * N; 
	
			irradiance += textureLod(environmentMap, sampleVec, 0.0).rgb * cos(theta) * sin(theta);
			nrSamples++;
		}
	}
	outDiffuse = pack_e5b9g9r9_ufloat(irradiance / float(nrSamples));
}