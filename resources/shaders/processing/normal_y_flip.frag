#version 450

layout (location = 0) in vec2 fragUV;
layout (location = 0) out vec4 outColor;

layout (set = 0, binding = 0) uniform sampler2D toProcess;

void main() {
	vec3 outNormal = textureLod(toProcess, vec2(fragUV.x, 1.0 - fragUV.y), 0.0).xyz;
	outNormal.y = -(outNormal.y * 2.0 - 1.0) * 0.5 + 0.5;
	outColor = vec4(outNormal.xyz, 1.0);
}