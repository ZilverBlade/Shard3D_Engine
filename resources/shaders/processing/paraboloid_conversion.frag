#version 450

#include "paraboloid.glsl"

layout (location = 0) in vec2 fragUV;
layout (location = 0) out vec4 outColor;

layout (set = 0, binding = 0) uniform samplerCube toProcess;

layout (push_constant) uniform Push {
	int topFace;
} push;

void main() {
	vec2 pos = fragUV * 2.0 - 1.0;
	float L = length(pos);
	
	if (L > 1.0) { // check if sample is outside of the paraboloid
		discard;
	}
	
	float vertical;
	if (push.topFace) { 
		vertical = 0.5 - 0.5 * length(pos);
	} else {
		vertical = -0.5 + 0.5 * length(pos);
	}
	vec3 samplePos = normalize(vec3(pos.x, vertical, pos.y));
	outColor = textureLod(toProcess, samplePos, 0.0);
}