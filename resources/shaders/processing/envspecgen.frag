#version 450
#extension GL_GOOGLE_include_directive : enable

const float PI = 3.1415926;

	
layout (constant_id = 0) const int sampleCount = 1024;
layout (set = 0, binding = 0) uniform samplerCube environmentMap;

layout (location = 0) in vec3 fragUV;
layout (location = 0) out vec4 outSpecular;

layout (push_constant) uniform Push {
	mat4 mvp;
	float glossiness;
} push;

// use this for static cubemap generation for correct results

float radicalInverseVdC(uint bits)  {
    bits = (bits << 16u) | (bits >> 16u);
    bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
    bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
    bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
    bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
    return float(bits) * 2.3283064365386963e-10; // / 0x100000000
}


void main() {
	const vec3 V = normalize(vec3(fragUV.x, -fragUV.y, fragUV.z));
	
	const vec3 up = abs(V.z) < 0.999 ? vec3(0.0, 0.0, 1.0) : vec3(1.0, 0.0, 0.0);
	const vec3 tangent = normalize(cross(up, V));
	const vec3 bitangent = cross(V, tangent);
	
	vec3 prefilteredColor = vec3(0.0); 
	float totalWeight = 0.0; 
	float r = (1.0 - push.glossiness);
	float r4 = r * r * r * r;
	
	for(int i = 0; i < sampleCount; i++) {
		float phi = 2.0 * PI * float(i) / float(sampleCount);
        float angle = radicalInverseVdC(i);
		 
		const float cosTheta = sqrt((1.0 - angle) / (1.0 + (r4 - 1.0) * angle));
		const float sinTheta = sqrt(1.0 - cosTheta*cosTheta);
		vec3 H = vec3(sinTheta * cos(phi), sinTheta * sin(phi), cosTheta);
		
		const vec3 sampleVec = tangent * H.x + bitangent * H.y + V * H.z;
		H = normalize(sampleVec);

		vec3 L = normalize(2.0 * dot(V, H) * H - V);
	
		float NdotL = dot(V, L);
		if(NdotL > 0.0)
		{
			prefilteredColor += textureLod(environmentMap, L, 0.0).rgb * NdotL;
			totalWeight      += NdotL;
		}
	}
	outSpecular = vec4(prefilteredColor / totalWeight, 1.0);
}