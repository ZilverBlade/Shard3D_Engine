#version 450

#define DESCRIPTOR_BINDLESS_SET 1
#include "descriptor_indexing.glsl"
#include "material.glsl"
layout (location = 0) in vec2 fragUV;
layout (location = 1) in flat uint materialIndex;
layout (location = 2) in flat uint id;
layout (location = 0) out uint outID;


void main()	{
    MaterialData sfMaterial = getMaterialData(materialIndex);
	
	if (matHasAlphaMask(sfMaterial)) {
		vec2 txcOff = sfMaterial.factorData.texCoordOffset;
		vec2 txcMul = sfMaterial.factorData.texCoordMultiplier;
		if (texture(texID[sfMaterial.textureData.maskTex_id], fragUV * txcMul + txcOff).x < 0.5)
			discard;
	}
	outID = id;
}