#version 450
// editor only

layout (location = 0) out flat vec4 color;
layout (location = 1) out flat float weightBias;
layout(set = 0, binding = 0) uniform GlobalUbo{
	mat4 projection;
	mat4 invProjection;
	mat4 view;
	mat4 invView;
} ubo;

layout(push_constant) uniform Push {
	vec4 position;
	vec4 color;
	float size;
	float weightBias;
} push;

void main() {
	color = push.color;
	weightBias = push.weightBias;
	gl_PointSize = push.size;
	gl_Position = ubo.projection * ubo.view * push.position;
}
