#version 450
layout(location = 0) in vec2 fragUV;
layout(location = 0) out vec4 materialQuarterResR8G8B8A8_UNORM;
layout(set = 0, binding = 0) uniform sampler2D materialMap;

void main() {
	materialQuarterResR8G8B8A8_UNORM = textureLod(materialMap, fragUV, 2.0).rgba;
}