#version 450
layout(location = 0) in vec2 fragUV;
layout(location = 0) out float heightR16f;
layout(set = 0, binding = 0) uniform sampler2D heightMap;

layout(push_constant) uniform Push {
	float LOD;
} push;

void main() {
	heightR16f = textureLod(heightMap, fragUV, push.LOD).r;
}