#version 450
#define DESCRIPTOR_BINDLESS_SET 1
#include "descriptor_indexing.glsl"
SAMPLER2D_TEXTURE(billboardTex);
#include "weighted_blending.glsl"
layout (location = 0) in vec2 fragUV;

layout (location = 0) out vec4 outAccum;
layout (location = 1) out float outReveal;

layout(push_constant) uniform Push {
	vec4 pos;
	uint textureID;
} push;

void main()	{
// clamp the "mask" value to be either 1 (visible) or 0 (discarded)
	if (texture(billboardTex[push.textureID], fragUV).w < 0.79) {
		discard;
	}
	
    outAccum = getAlphaAccum(vec3(1.0, 0.0, 1.0), 0.25);
	outReveal = 0.25;
}