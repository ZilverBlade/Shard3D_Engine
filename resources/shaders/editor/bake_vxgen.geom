#version 450
layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

layout (set = 0, binding = 0) uniform Buffer {
	mat4 viewProjX;
	mat4 viewProjY;
	mat4 viewProjZ;
} projData;

layout(location = 0) in vec2 in_fragUV[];
layout(location = 1) in vec3 in_fragNormalWorld[];

layout(location = 0) out vec2 fragUV;
layout(location = 1) out vec3 fragNormalWorld;
layout(location = 2) out flat int axis;

void main()
{
	vec3 e1 = gl_in[0].gl_Position.xyz - gl_in[1].gl_Position.xyz;
	vec3 e2 = gl_in[2].gl_Position.xyz - gl_in[0].gl_Position.xyz;

	vec3 n = normalize(cross(e1, e2));

	n = vec3(abs(n.x), abs(n.y), abs(n.z));

	float nx = n.x, ny = n.y, nz = n.z;

	if(nx >= ny && nx >= nz)
		axis = 1;
	else if(ny >= nx && ny >= nz)
		axis = 2;
	else
		axis = 3;

	mat4 pMat = axis == 1 ? projData.viewProjX : axis == 2 ?  projData.viewProjY : projData.viewProjZ;

	for(int i = 0; i < gl_in.length(); i++)
	{
		fragUV = in_fragUV[i];
		fragNormalWorld = in_fragNormalWorld[i];
		gl_Position = pMat * gl_in[i].gl_Position;
		EmitVertex();
	}
	EndPrimitive();
}