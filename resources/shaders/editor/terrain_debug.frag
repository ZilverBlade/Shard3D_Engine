#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_control_flow_attributes : enable

#include "global_ubo.glsl"
#include "compression.glsl"
#include "terrain.glsl"

layout(location = 0) in vec3 fragPosWorld;
layout(location = 1) in vec2 fragUV;
layout(location = 2) in flat int index;
layout(location = 3) in flat ivec2 tile;

layout (location = 0) out vec4 GBuffer0; // diffuse + chrominess R8G8B8A8 unorm
layout (location = 1) out vec2 GBuffer1; // material R8G8 unorm
layout (location = 2) out vec3 GBuffer2; // emission B10G11R11 ufloat
layout (location = 3) out uint GBuffer3; // normal pack uint32 
layout (location = 4) out uint GBuffer4; // shading model uint8 

layout(set = 2, binding = 2) uniform sampler2D heightMap;
layout(set = 2, binding = 3) uniform sampler2D materialMap0;
//layout(set = 2, binding = 2) uniform sampler2DArray materialMap1;

const uint FalloffLinear = 0;
const uint FalloffQuadratic = 1;
const uint FalloffCubic = 2;
const uint FalloffExponential2 = 3;

const uint ViewDefault = 0;
const uint ViewTiles = 1;
const uint ViewHeight = 2;
const uint ViewNormals = 3;
const uint ViewPhysMat = 4;
const uint ViewLOD = 5;

layout(set = 2, binding = 0) buffer TileVisibility {
	TerrainTileData tiles[1];
} tileData;
struct PaintConfig {
	float radius;
	float weight;
	uint falloff;
	float perlinIntensity;
	float perlinScale;
};
layout (push_constant) uniform Push {
	vec4 translation;
	vec2 tiles;
	float tileExtent;
	float heightMul;
	uint LOD;
	PaintConfig cfg;
	bool displayHover;
	uint view;
	vec3 mouseWorld;
} push;

float random1(vec2 coords) {
   return fract(sin(dot(coords.xy, vec2(49.9898,148.233))) * 43758.5453);
}
float random2(vec2 coords) {
   return fract(sin(dot(coords.xy, vec2(-2121.2132,22.56167))) * 122.3623);
}

vec3 previewTileColor(vec2 tile) {
	return vec3(random1(tile), random2(tile), random2(tile + vec2(8591.23, -42.352)));
}

void main() {
	TerrainTileData tileData = tileData.tiles[index];
	
	TerrainData data;
	
	data.translation	= push.translation;
	data.tiles			= push.tiles; 
	data.tileExtent	= push.tileExtent;
	data.heightMul		= push.heightMul;
	data.LOD			= push.LOD;
	
	vec3 normal = sampleTerrainNormal(data, heightMap, fragPosWorld, fragUV, tileData);
	int maxLOD = 8;//textureQueryLevels(heightMap) - 1;
	float lodFactor = maxLOD == 0 ? 0.0 : push.LOD / float(maxLOD);
	switch (push.view) {
		case (ViewTiles):
		GBuffer2 = previewTileColor(vec2(tile)); 
		break;
		case (ViewHeight):
		GBuffer2 = ((fragPosWorld.y - push.translation.y + 16.0) / 64.0).xxx; 
		break;
		
		case (ViewNormals):
		GBuffer2 = normal * 0.5 + 0.5.xxx; 
		break;
		
		
		case (ViewDefault):
		GBuffer2 = dot(normal, normalize(ubo.invView[3].xyz - fragPosWorld)).xxx;
		break;
		
		case (ViewLOD):
		GBuffer2 = mix(vec3(1.0, 0.0, 0.0), mix(vec3(0.0, 1.0, 0.0), vec3(0.0, 0.0, 1.0), clamp(lodFactor * 2.0 - 1.0, 0.0, 1.0)), clamp(lodFactor * 2.0, 0.0, 1.0));
		
		break;
	}
	
	if (push.displayHover) {
		vec2 terrainSpace = (push.mouseWorld.xyz - push.translation.xyz).xz;
		
		float weightStrength = (max(1.0 - distance(terrainSpace, fragPosWorld.xz) / (push.cfg.radius * 2.0), 0.0));
		switch (push.cfg.falloff) {
			case(FalloffLinear):
				weightStrength = weightStrength;
			break;
			case(FalloffQuadratic):
				weightStrength = 1.0 - pow(1.0 - weightStrength, 2.0);
			break;
			case(FalloffCubic):
				weightStrength = 1.0 - pow(1.0 - weightStrength, 3.0);
			case(FalloffExponential2):
				weightStrength = exp(-pow((weightStrength-1.0)*2.7182818,2.0));
			break;
		}
		GBuffer2 = mix(GBuffer2, vec3(1.0, 0.0, 1.0), weightStrength);
	}
	
	GBuffer0 = vec4(0.0);
	GBuffer1 = vec2(0.0);
	GBuffer3 = 0;
	GBuffer4 = 0; // unshaded model
}