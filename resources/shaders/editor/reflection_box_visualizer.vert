#version 450
// editor only
#include "line_cube.glsl"

layout (location = 0) out flat vec4 color;
layout (location = 1) out flat float weightBias;
layout(set = 0, binding = 0) uniform GlobalUbo{
	mat4 projection;
	mat4 invProjection;
	mat4 view;
	mat4 invView;
} ubo;

layout(push_constant) uniform Push {
	mat4 transformMatrix;
	vec4 color;
	float transition_bounds;
} push;
void main() {
	gl_Position = ubo.projection * ubo.view * push.transformMatrix * vec4(LineCube[gl_VertexIndex], 1.0);
	color = push.color;
	weightBias = 1.0;
}
