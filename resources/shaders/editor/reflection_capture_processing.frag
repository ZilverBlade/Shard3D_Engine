#version 450

layout (location = 0) in vec2 fragUV;

layout (location = 0) out uint outColor;

layout (set = 0, binding = 0) uniform sampler2D capturedBuffer;

#include "compression.glsl"

//const float acceptablePercentageError = (4.0 / 255.0) * 100.0; // about 1.6%
//
//float getSlackErrorOffset(float brightness) {
//	float o = 0.6;
//	float x = o * brightness - o;
//	return acceptablePercentageError * pow(x, 1.6);
//}
//float getTrueValue(float value) {
//	float inverseValue = 1.0 / value;
//	int truncated = int(inverseValue * 255.0);
//	float reprojectedValue = 1.0 / (float(truncated) / 255.0);
//	return reprojectedValue;
//}
//float getPrecisionError(float value) {
//	return abs(getTrueValue(value) - value);
//}
//float compareErrorPercentage(float experimental, float theoretical){
//	return 100.0 * abs(theoretical - experimental) / theoretical;
//}
//
//bool isAcceptablePrecision(vec3 nor, float len) {
//	float trueLen = getTrueValue(len);
//	float trueNorX = getTrueValue(nor.x);
//	float trueNorY = getTrueValue(nor.y);
//	float trueNorZ = getTrueValue(nor.z);
//	
//	// higher brightnesses are allowed to get away with worse precision as both it's harder to notice  
//	// and it will eventually start to get impossible to keep the 1.6% precision
//	float slack = getSlackErrorOffset(len); 
//	
//	float percentErrorX = compareErrorPercentage(trueNorX * trueLen, nor.x * len) * slack;
//	float percentErrorY = compareErrorPercentage(trueNorY * trueLen, nor.y * len) * slack;
//	float percentErrorZ = compareErrorPercentage(trueNorZ * trueLen, nor.z * len) * slack;
//	return 
//		percentErrorX < acceptablePercentageError || 
//		percentErrorY < acceptablePercentageError ||
//		percentErrorZ < acceptablePercentageError;
//}
//
//vec3 getBetterPrecisionPerformer(vec3 a, vec3 b) {
//	float aEX = compareErrorPercentage(getTrueValue(a.x), a.x);
//	float aEY = compareErrorPercentage(getTrueValue(a.y), a.y);
//	float aEZ = compareErrorPercentage(getTrueValue(a.z), a.z);
//	float bEX = compareErrorPercentage(getTrueValue(b.x), b.x);
//	float bEY = compareErrorPercentage(getTrueValue(b.y), b.y);
//	float bEZ = compareErrorPercentage(getTrueValue(b.z), b.z);
//	
//	bvec3 aBetterThanB = greaterThan(vec3(aEX, aEY, aEZ), vec3(bEX, bEY, bEZ));
//	bool aIsBetter = (int(aBetterThanB.x) + int(aBetterThanB.y) + int(aBetterThanB.z)) >= 2;
//	return aIsBetter ? a : b;
//}

void main()	{
	vec3 accumColor = vec3(0.0);
	vec2 texelSize = 1.0 / textureSize(capturedBuffer, 0);

// average out the multiple samples (super sampling 2x2)
	accumColor += texture(capturedBuffer, fragUV + vec2(-1, -1) * texelSize).rgb;
	accumColor += texture(capturedBuffer, fragUV + vec2(1, -1) * texelSize).rgb;
	accumColor += texture(capturedBuffer, fragUV + vec2(1, 1) * texelSize).rgb;
	accumColor += texture(capturedBuffer, fragUV + vec2(-1, 1) * texelSize).rgb;
	accumColor /= 4.0;
	
	outColor = pack_e5b9g9r9_ufloat(accumColor);
	
	return;
	
	
	
//	if (accumColor.r <= 1.0 && accumColor.g <= 1.0 && accumColor.b <= 1.0) {
//		outColor = vec4(accumColor, 1.0); // straight up store the raw values if brightness doesnt exceed 1.0
//		return;
//	} else {
// compress the brightness data 
		//float colorIntensity = length(accumColor);
		//vec3 normalizedColor = accumColor / colorIntensity;
		//
		//outColor = vec4(normalizedColor, max(1.0 / colorIntensity, 1.0 / 255)); 
		//if (isAcceptablePrecision(normalizedColor, colorIntensity)) {
		//	outColor = vec4(normalizedColor, max(1.0 / colorIntensity, 1.0 / 255)); 
		//	return;
		//} else { // if precision sucks then try to find a better normalized value and multiplier
		//	vec3 acceptableNormalizedColor = vec3(0.0);
		//	
		//	for (float i = 0.0; i <= 8.0 / 255.0; i += 1.0 / 255.0){
		//		acceptableNormalizedColor = getBetterPrecisionPerformer(accumColor - i, accumColor + i);
		//		acceptableNormalizedColor = getBetterPrecisionPerformer(acceptableNormalizedColor * colorIntensity, normalizedColor * colorIntensity);
		//		if (isAcceptablePrecision(acceptableNormalizedColor, colorIntensity)) {
		//			break; // if it's good enough then stop the loop
		//		}
		//	}
		//	outColor = vec4(acceptableNormalizedColor, max(1.0 / colorIntensity, 1.0 / 255)); 
		//	return;
		//}
//	}
	
}