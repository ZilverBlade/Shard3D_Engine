#version 450
// editor only
#extension GL_GOOGLE_include_directive : enable
#extension GL_KHR_vulkan_glsl : enable

#include "compression.glsl"

layout (location = 0) in vec3 position;
layout (location = 2) in uint CMPR_normal;
layout (location = 0) out vec3 fragNormalWorld;
layout(set = 0, binding = 0) uniform GlobalUbo{
	mat4 projection;
	mat4 invProjection;
	mat4 view;
	mat4 invView;
	mat4 projectionView;
} ubo;

layout(push_constant) uniform Push {
	mat4 transformMatrix;
} push;

void main() {
	gl_Position = ubo.projectionView * push.transformMatrix * vec4(vertexPos, 1.0);
	fragNormalWorld = vec3(
		unpack_float11b_signed(CMPR_normal, 0xffe00000, 21),
		unpack_float11b_signed(CMPR_normal, 0x001ffc00, 10),
		unpack_float10b_signed(CMPR_normal, 0x000003ff, 0)
	);
}
