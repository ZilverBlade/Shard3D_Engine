#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_KHR_vulkan_glsl : enable

layout(location = 0) in vec3 position;
layout(location = 0) out flat vec4 color;
layout(location = 1) out flat float weight;

layout(set = 0, binding = 0) uniform GlobalUbo{
	mat4 projection;
	mat4 invProjection;
	mat4 view;
	mat4 invView;
} ubo;

layout(push_constant) uniform Push {
	mat4 modelMatrix;
	vec4 color;
	float weight;
} push;

void main() {
	vec4 positionWorld = push.modelMatrix * vec4(position, 1.0);
	
	gl_Position = ubo.projection * ubo.view * positionWorld;

	color = push.color;
	weight = push.weight;
}