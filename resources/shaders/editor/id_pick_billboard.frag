#version 450
#define DESCRIPTOR_BINDLESS_SET 1
#include "descriptor_indexing.glsl"
SAMPLER2D_TEXTURE(billboardTex);

layout (location = 0) in vec2 fragUV;
layout (location = 0) out uint outID;

layout(push_constant) uniform Push {
	vec4 pos;
	uint textureID;
	uint id;
} push;

void main()	{
// clamp the "mask" value to be either 1 (visible) or 0 (discarded)
	if (texture(billboardTex[push.textureID], fragUV).w < 0.79) {
		discard;
	}
	outID = push.id;
}