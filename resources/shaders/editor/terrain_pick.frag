#version 450

layout (push_constant) uniform Push {
		vec4 translation;
		vec2 tiles;
		float tileExtent;
		float heightMul;
		uint lod;
		uint objectID;
} push;
layout (location = 0) out uint outID;

void main()	{
	outID = push.objectID;
}