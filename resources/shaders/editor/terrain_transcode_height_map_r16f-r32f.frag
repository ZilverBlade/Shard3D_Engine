#version 450
layout(location = 0) in vec2 fragUV;
layout(location = 0) out float heightR32f;
layout(set = 0, binding = 0) uniform sampler2D heightMap;

void main() {
	heightR32f = textureLod(heightMap, fragUV, 0.0).r;
}