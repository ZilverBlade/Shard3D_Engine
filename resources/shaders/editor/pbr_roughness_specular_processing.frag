#version 450

layout (location = 0) in vec2 fragUV;

layout (location = 0) out vec4 outSpecular;

layout (set = 0, binding = 0) uniform sampler2D inRoughness;
//layout (set = 0, binding = 1) uniform ProcessingInformation {
//	float gain;
//	float contrast;
//} pInfo;

//struct ProcessingInformation {
//	float gain = 0.0;
//	float contrast = 1.25;
//} pInfo;

mat4 contrastMatrix_( float contrast )
{
	float t = ( 1.0 - contrast ) / 2.0;
    
    return mat4( contrast, 0, 0, 0,
                 0, contrast, 0, 0,
                 0, 0, contrast, 0,
                 t, t, t, 1 );

}


void main()	{
	float roughness = texture(inRoughness, fragUV).x;
	float rNew = vec4(contrastMatrix_(1.25) * vec4(vec3(roughness), 1.0)).r;
	float specular = clamp(1.0 - (rNew + 0.0), 0.0, 1.0);
	outSpecular = vec4(vec3(specular), 1.0);
}