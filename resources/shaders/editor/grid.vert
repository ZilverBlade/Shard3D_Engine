#version 450

#include "depth.glsl"
#include "global_ubo.glsl"

layout(location = 0) out flat float near; //0.01
layout(location = 1) out flat float far; //100
layout(location = 2) out vec3 nearPoint;
layout(location = 3) out vec3 farPoint;

layout(location = 4) out flat mat4 fragView;
layout(location = 8) out flat mat4 fragProj;

// Grid position are in xy clipped space
vec3 gridPlane[6] = vec3[](
	vec3(1, 1, 0),
	vec3(-1, -1, 0), 
	vec3(-1, 1, 0),
	vec3(-1, -1, 0), 
	vec3(1, 1, 0), 
	vec3(1, -1, 0)
);

vec3 UnprojectPoint(float x, float y, float z) {
    vec4 unprojectedPoint =  ubo.invView * ubo.invProjection * vec4(x, y, z, 1.0);
    return unprojectedPoint.xyz / unprojectedPoint.w;
}

void main() {
	vec3 p = gridPlane[gl_VertexIndex].xyz;
    nearPoint = UnprojectPoint(p.x, p.y, 1.0 - CLEAR_DEPTH).xyz; // unprojecting on the near plane
    farPoint = UnprojectPoint(p.x, p.y, 0.5).xyz; // unprojecting on the far plane
		
	fragView = ubo.view;
	fragProj = ubo.projection;

	near = ubo.nearPlane;
	far = ubo.farPlane;

    gl_Position = vec4(p.xy, 0.0, 1.0); // using directly the clipped coordinates
}
