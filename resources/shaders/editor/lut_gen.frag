#version 450

layout(location = 0) in vec2 fragUV;
layout(location = 0) out vec4 outColor;

void main(){
    float tile = 16.;
    float tile2 = pow(tile,2.);
    float b = (floor(fragUV.x*tile)+floor(fragUV.y*tile)*tile)/tile2;
    vec2 rg = fract(fragUV*tile);
    outColor = vec4(rg,b, 1.0);
}