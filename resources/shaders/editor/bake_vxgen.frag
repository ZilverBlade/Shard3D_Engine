#version 450

#define DESCRIPTOR_BINDLESS_SET 2
#define SURFACE_DEFERRED
#define SURFACE_UBER_NO_FRAGMENT_INPUTS


layout(location = 0) in vec2 fragUV;
layout(location = 1) in vec3 fragNormalWorld;
layout(location = 2) in flat int axis;

#include "compression.glsl"
#include "surface_uber.glsl"

layout (set = 1, binding = 0, rgba16_snorm) uniform  image3D outputNormal;
layout (set = 1, binding = 1, rgba8)  uniform   image3D outputDiffuse;

void main() {
    MaterialID id = getMaterialInfos(getMaterialID());
	
	vec2 txcOff = factor[id.buffer_id].texCoordOffset;
	vec2 txcMul = factor[id.buffer_id].texCoordMultiplier;
	vec2 coord = getTexCoordValue(txcOff, txcMul);
	
	vec3 diffuse = vec3(0.0); 

	bool hasDiffuseTex = false;
	
#ifndef S3DSDEF_DISABLE_MATERIAL_TEXTURES
	hasDiffuseTex = sfHasDiffuseTexture(id);
#endif

	diffuse = getSurfaceMaterialDiffuse(id, hasDiffuseTex, coord);
	
	vec3 cd = gl_FragCoord.xyz;
	cd.z *= (imageSize(outputNormal).z - 1.0);
	cd.x = imageSize(outputNormal).x - cd.x - 1;
	vec3 UV = cd;
	switch (axis) {
		case (1): // X
		UV = UV.zyx;
     UV.z = imageSize(outputNormal).z - UV.z - 1;
		break;
		case (2): // Y
		UV = UV.xzy;
     UV.z = imageSize(outputNormal).z - UV.z - 1;
		break;
		case (3): // Z
		UV = UV.xyz;
		break;
	}
	
	imageStore(outputNormal, ivec3(UV), vec4(normalize(fragNormalWorld), 1.0));
	imageStore(outputDiffuse, ivec3(UV), vec4(diffuse, 1.0));
}
