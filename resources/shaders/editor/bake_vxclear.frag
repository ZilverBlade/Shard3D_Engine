#version 450

layout (set = 0, binding = 0, rgba16_snorm) uniform  image3D outputNormal;
layout (set = 0, binding = 1, rgba8) uniform image3D outputDiffuse;

void main() {
	vec3 UV = gl_FragCoord.xyz;
	
	for (int i = 0; i < imageSize(outputNormal).z; i++) {
		UV.z = i;
		
		imageStore(outputNormal, ivec3(UV), vec4(0.0, 0.0, 0.0, 0.0));
		imageStore(outputDiffuse, ivec3(UV), vec4(0.0, 0.0, 0.0, 0.0));
	}
}
