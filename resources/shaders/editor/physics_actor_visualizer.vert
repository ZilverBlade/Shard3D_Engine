#version 450
// editor only

layout (location = 0) in vec3 position;
layout (location = 0) out flat vec4 color;
layout (location = 1) out flat float weightBias;
layout(set = 0, binding = 0) uniform GlobalUbo{
	mat4 projection;
	mat4 invProjection;
	mat4 view;
	mat4 invView;
} ubo;

layout(set = 1, binding = 0) uniform ConvexHullVertices{
	vec4 positions[1];
} buff;

const int Sphere = 0;
const int Box = 1;
const int Plane = 2;
const int Capsule = 3;
const int Cylinder = 4;
const int ConvexHullAsset = 5;

layout(push_constant) uniform Push {
	mat4 transformMatrix;
	vec4 color;
	int type;
} push;

void main() {
	color = push.color;
	weightBias = 1.0;
	vec4 vertex;
	if (push.type == ConvexHullAsset){ 
		vertex = buff.positions[gl_VertexIndex];
	} else {
		vertex = vec4(position, 1.0);
	}
	gl_Position = ubo.projection * ubo.view * push.transformMatrix * vertex;
	gl_PointSize = 8.0;
}
