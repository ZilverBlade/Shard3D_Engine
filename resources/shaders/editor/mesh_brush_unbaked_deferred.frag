#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_KHR_vulkan_glsl : enable

#define SURFACE_DEFERRED

layout(location = 0) in vec3 fragNormalWorld;

#include "compression.glsl"

layout (location = 0) out vec4 GBuffer0; // diffuse + chrominess R8G8B8A8 unorm
layout (location = 1) out vec2 GBuffer1; // material R8G8 unorm
layout (location = 2) out vec3 GBuffer2; // emission B10G11R11 ufloat
layout (location = 3) out uint GBuffer3; // normal pack uint32 
layout (location = 4) out uint GBuffer4; // shading model uint8 


void main() {
	GBuffer0 = vec4(0.5, 0.5, 0.5, 1.0);
	GBuffer1 = vec2(0.1, (8.0 - 1.0) / 255.0);
	GBuffer2 = vec3(0.0);
	GBuffer3 = cram_32bnormal(normalize(fragNormalWorld));
	GBuffer4 = 1; // shaded
	
}