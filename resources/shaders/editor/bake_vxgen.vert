#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_KHR_vulkan_glsl : enable

#define DESCRIPTOR_BINDLESS_SET 2
#define SURFACE_UBER_NO_FRAGMENT_INPUTS
#define SURFACE_UBER_NO_TEXTURE_FUNCTIONS
#define MATERIAL_ID_NO_PUSH

#include "compression.glsl"
#include "depth.glsl"
#include "surface_uber.glsl"
#include "descriptor_indexing"

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv;
layout(location = 2) in uint CMPR_normal;
layout(location = 0) out vec2 fragUV;
layout(location = 1) out vec3 fragNormalWorld;


layout (push_constant) uniform Push {
	mat4 modelMatrix;
	mat4 normalMatrix;
} push;

void main() {
	mat4 modelMatrix = push.modelMatrix;
	mat3 normalMatrix = mat3(push.normalMatrix); 
	uint materialIndex = floatBitsToUint(push.normalMatrix[3].x);
	
	gl_Position = modelMatrix * vec4(position, 1.0);
	
    MaterialID id = getMaterialInfos(materialIndex);

	const vec3 normal = vec3(
		unpack_float11b_signed(CMPR_normal, 0xffe00000, 21),
		unpack_float11b_signed(CMPR_normal, 0x001ffc00, 10),
		unpack_float10b_signed(CMPR_normal, 0x000003ff, 0)
	);
	fragNormalWorld = normalize(normalMatrix * normal);
	
	if (sfIsTextured(id)) {
		fragUV = uv;
	} else {
		fragUV = vec2(0.0);
	}
}