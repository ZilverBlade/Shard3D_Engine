#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_KHR_vulkan_glsl : enable

#ifdef RIGGED_MESH
#define RIGGDED_MESH_INSTANCE_DESCRIPTOR 2
#endif
#define DESCRIPTOR_BINDLESS_SET 0
#include "global_ubo.glsl"

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv;
layout(location = 0) out vec2 fragUV;
layout(location = 1) out flat uint materialIndex;
layout(location = 2) out flat uint id;

#include "vertex_transform.glsl"

#ifdef ACTOR_DYNAMIC
READONLY_BUFFER DynamicActorData {
	mat4 modelMatrix;
	mat3 normalMatrix;
	mat4 modelMatrixPrevFrame;
} dynamicActorData[];
layout (push_constant) uniform Push {
	uint actorBuffer;
	uint materialID;
	uint id;
} push;
#else
layout (push_constant) uniform Push {
	mat4 modelMatrix;
	uint materialID;
	uint id;
} push;
#endif

void main() {
	
#ifdef ACTOR_DYNAMIC
	mat4 modelMatrix = dynamicActorData[push.actorBuffer].modelMatrix;
#else
	mat4 modelMatrix = push.modelMatrix;
#endif
	materialIndex = push.materialID;
	id = push.id;
	VertexData vertexData = vertex_Transform();

	gl_Position = ubo.projectionView * modelMatrix * vec4(vertexData.position, 1.0);

	fragUV = uv;
}