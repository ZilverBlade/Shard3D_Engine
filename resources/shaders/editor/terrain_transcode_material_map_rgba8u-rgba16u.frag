#version 450
layout(location = 0) in vec2 fragUV;
layout(location = 0) out vec4 materialFullResR16G16B16A16_UNORM;
layout(set = 0, binding = 0) uniform sampler2D materialMap;

void main() {
	materialFullResR16G16B16A16_UNORM = textureLod(materialMap, fragUV, 0.0).rgba;
}