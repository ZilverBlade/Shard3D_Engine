#version 450
// editor only

layout (location = 0) in vec3 vertexPos;
layout (location = 0) out flat vec4 color;
layout (location = 1) out flat float weightBias;
layout(set = 0, binding = 0) uniform GlobalUbo{
	mat4 projection;
	mat4 invProjection;
	mat4 view;
	mat4 invView;
} ubo;

layout(push_constant) uniform Push {
	mat4 transformMatrix;
	vec4 color;
	vec3 maxXYZ;
	vec3 minXYZ;
} push;

float getLimitCompare(float pointAxis, float minimum, float maximum){
	return float(int(bool(pointAxis > 0))) * maximum + float(int(bool(pointAxis < 0))) * minimum;
}

void main() {
	vec3 adjustedVertexPos = vec3(0.0);
	adjustedVertexPos.x = getLimitCompare(vertexPos.x, push.minXYZ.x, push.maxXYZ.x);
	adjustedVertexPos.y = getLimitCompare(vertexPos.y, push.minXYZ.y, push.maxXYZ.y);
	adjustedVertexPos.z = getLimitCompare(vertexPos.z, push.minXYZ.z, push.maxXYZ.z);

	gl_Position = ubo.projection * ubo.view * push.transformMatrix * vec4(adjustedVertexPos, 1.0);
	color = push.color;
	weightBias = 1.0;
}
