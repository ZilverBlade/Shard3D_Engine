#version 450

#define DESCRIPTOR_BINDLESS_SET 1
#include "descriptor_indexing.glsl"
#include "material.glsl"
#include "weighted_blending.glsl"
SAMPLER2D_TEXTURE(masktex);
layout (location = 0) in vec2 fragUV;
layout (location = 1) in flat uint materialIndex;
layout (location = 0) out vec4 outAccum;
layout (location = 1) out float outReveal;

void main()	{
    MaterialData sfMaterial = getMaterialData(materialIndex);
	
	if (matHasAlphaMask(sfMaterial)) {
		vec2 txcOff = sfMaterial.factorData.texCoordOffset;
		vec2 txcMul = sfMaterial.factorData.texCoordMultiplier;
		if (texture(texID[sfMaterial.textureData.maskTex_id], fragUV * txcMul + txcOff).x < 0.5)
			discard;
	}
	
    outAccum = getAlphaAccum(vec3(1.0, 0.0, 1.0), 0.25);
	outReveal = 0.25;
}