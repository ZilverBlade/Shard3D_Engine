#version 450

layout (location = 0) in vec2 fragUV;

#define DESCRIPTOR_BINDLESS_SET 1
#include "descriptor_indexing.glsl"
SAMPLER2D_TEXTURE(billboardTex);

layout (location = 0) out vec4 GBuffer0;
layout (location = 1) out vec2 GBuffer1;
layout (location = 2) out vec3 GBuffer2; // only care about the emission buffer
layout (location = 3) out uint GBuffer3;
layout (location = 4) out uint GBuffer4;

layout(push_constant) uniform Push {
	vec4 translation;
	uint textureID;
} push;

void main()	{
// clamp the "mask" value to be either 1 (visible) or 0 (discarded)
	if (texture(billboardTex[push.textureID], fragUV).w < 0.79) {
		discard;
	}
	GBuffer2 = texture(billboardTex[push.textureID], fragUV).xyz;
	
	GBuffer0 = vec4(0.0);
	GBuffer1 = vec2(0.0);
	GBuffer3 = 0x00000000;
	GBuffer4 = 0x00;
}