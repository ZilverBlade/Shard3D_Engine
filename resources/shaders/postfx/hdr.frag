#version 450
#extension GL_KHR_vulkan_glsl : enable
#include "tonemapper.glsl"
#include "globalubo.glsl"

layout (location = 0) in vec2 fragUV;
layout (location = 0) out vec4 outColor;

layout (set = 0, binding = 0) uniform sampler2D sceneInputImage;
//layout (set = 0, binding = 1) uniform sampler2D colorGradingLUT;

layout (push_constant) uniform myParams {
	int tone_mapper_method;
	float lim;
    float gain;
    float contrast;
    float saturation;
    float temperature;
    float hueShift;
	float n;
	vec3 shadows;
	vec3 midtones;
	vec3 highlights;
} hdrData;

const int REINHARD_TONE_MAPPING = 1;
const int REINHARD_EXTENDED_TONE_MAPPING = 2;
const int EXPONENTIAL_TONE_MAPPING = 3;
const int REINHARD_LUMA_TONE_MAPPING = 4;
const int HABLE_TONE_MAPPING = 5;
const int ACES_TONE_MAPPING = 6;

vec3 invLerp(vec3 A, vec3 B, vec3 t)
{
    return (t - A) / (B - A);
}

vec3 colorGrade( vec3 color )
{
    // Calculate the three offseted colors up-front
    vec3 offShadows  = LinearToLogC(color) + 2..xxx * hdrData.shadows - 1..xxx;
    vec3 offMidtones = LinearToLogC(color) + 2..xxx * hdrData.midtones - 1..xxx;
    vec3 offHilights = LinearToLogC(color) + 2..xxx * hdrData.highlights - 1..xxx;
    
    // Linearly interpolate between the 3 new colors, piece-wise
    return LogCToLinear(mix(
        // We pick which of the two control points to interpolate from based on which side of
        // 0.5 the input color channel lands on
        mix(offShadows,  offMidtones, invLerp(LinearToLogC(vec3(0.0)), LinearToLogC(vec3(0.5)), LinearToLogC(color))), // <  0.5
        mix(offMidtones, offHilights, invLerp(LinearToLogC(vec3(0.5)), LinearToLogC(vec3(1.0)), LinearToLogC(color))), // >= 0.5
        greaterThanEqual(LinearToLogC(color), LinearToLogC(vec3(0.5)))
    ));
}

void main()	{
    vec3 hdrColor = texture(sceneInputImage, fragUV).rgb; // exposure is already applied in bloom pass
  
    // exposure tone mapping
	vec3 mapped = clamp(hdrColor, 0.0, 1.0); // default
	
	if (hdrData.tone_mapper_method == REINHARD_TONE_MAPPING) {
		mapped = reinhard_tone_mapping(hdrColor, 1.0);
	} else if (hdrData.tone_mapper_method == REINHARD_EXTENDED_TONE_MAPPING) {
		mapped = reinhard_extended(hdrColor, hdrData.lim);
	} else if (hdrData.tone_mapper_method == EXPONENTIAL_TONE_MAPPING) {
		mapped = reinhard_exp_tone_mapping(hdrColor, 1.0);
	} else if (hdrData.tone_mapper_method == REINHARD_LUMA_TONE_MAPPING) {
		mapped = reinhard_luma(hdrColor, 1.0);
	} else if (hdrData.tone_mapper_method == HABLE_TONE_MAPPING) {
		mapped = hable_tone_mapping(hdrColor);
	} else if (hdrData.tone_mapper_method == ACES_TONE_MAPPING) {
		mapped = aces_approx(hdrColor);
	}
    

    vec3 adjusted = colorGrade(
		temperatureKelvin(
			brightenColor(
				contrastColor(
					shiftHueSat(mapped, hdrData.hueShift, hdrData.saturation),
						hdrData.contrast), 
							hdrData.gain), 
								hdrData.temperature)
									);
                
    outColor = vec4(adjusted.xyz, 1.0); // RGBA16F
}