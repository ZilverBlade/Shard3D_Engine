#version 450
layout (location = 0) in vec2 fragUV;

layout (location = 0) out vec4 outColor;
layout (set = 0, binding = 0) uniform sampler2D postProcessImage;

const highp float NOISE_GRANULARITY = 0.44/255.0;

//highp float random(vec2 coords) {
//	const highp vec3 magic = vec3( 0.06711056, 0.00583715, 52.9829189 );
//	highp float noise0 = fract( magic.z * fract(dot( gl_FragCoord.xy, magic.xy ) ) );
//	return cos(2.0 * 3.1415926 * noise0) * 0.5 + 0.5;
//}
highp float random(vec2 coords) {
   return clamp(fract(sin(dot(coords.xy, vec2(12.9898,148.233))) * 43758.5453), 0.0, 1.0);
}

layout (push_constant) uniform myParams {
    vec2 screenSize;
	float invGamma;
} push;


void main()	{
    vec2 fragCoords = gl_FragCoord.xy;
    vec2 texSize = push.screenSize;
	vec2 coord = vec2(gl_FragCoord.x, texSize.y - gl_FragCoord.y) / texSize;
	
    highp vec3 pixelColor = pow(texture(postProcessImage, coord).rgb, vec3(push.invGamma));
    
    highp float fragmentColor_r = pixelColor.x;
    highp float fragmentColor_g = pixelColor.y;
    highp float fragmentColor_b = pixelColor.z;
    fragmentColor_r += mix(-NOISE_GRANULARITY, NOISE_GRANULARITY, random(fragCoords));
	fragmentColor_g += mix(-NOISE_GRANULARITY, NOISE_GRANULARITY, random(fragCoords));
	fragmentColor_b += mix(-NOISE_GRANULARITY, NOISE_GRANULARITY, random(fragCoords));

    outColor = vec4(fragmentColor_r, fragmentColor_g, fragmentColor_b, 1.0);
}