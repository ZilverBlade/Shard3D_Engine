#version 450
#extension GL_KHR_vulkan_glsl : enable

layout (location = 0) in vec2 fragUV;
layout (location = 0) out vec4 outColor;

layout (set = 0, binding = 0) uniform sampler2D sceneInputImage;
layout (set = 0, binding = 1) uniform sampler2D blurInputImage;
#ifdef MultiBloomKnl
layout (set = 0, binding = 2) uniform sampler2D blurLargeInputImage;
#endif
//layout (set = 0, binding = 3) uniform sampler2D lensFlareTexture;

layout (push_constant) uniform myParams {
	float bloomSizeCoeficient;
	int bloomQuality;
	float exposure;
    vec2 screenSize;
} push;

void main()	{  
#ifdef MultiBloomKnl
	vec2 texelSize = 1.0 / push.screenSize;
	vec3 s_bc = vec3(0.0);
	vec3 l_bc = vec3(0.0);
	if (push.bloomQuality == 0) {
		l_bc = texture(blurLargeInputImage, fragUV).rgb;
		s_bc = texture(blurInputImage, fragUV).rgb;
	} else if (push.bloomQuality == 1) {	
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(14.0, 14.0)).rgb * 0.25;
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(-14.0, -14.0)).rgb * 0.25;
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(-14.0, 14.0)).rgb * 0.25;
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(14.0, -14.0)).rgb * 0.25; 
	
		s_bc += texture(blurInputImage, fragUV + texelSize * vec2(8.0, 8.0)).rgb * 0.50;
		s_bc += texture(blurInputImage, fragUV + texelSize * vec2(-8.0, -8.0)).rgb * 0.50;
	} else if (push.bloomQuality == 2) {	
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(8.0, 8.0)).rgb * 0.17;
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(-8.0, 8.0)).rgb * 0.17;
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(8.0, -8.0)).rgb * 0.17;
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(-8.0, -8.0)).rgb * 0.17; 
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(16.0, 16.0)).rgb * 0.08;
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(-16.0, 16.0)).rgb * 0.08;
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(16.0, -16.0)).rgb * 0.08;
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(-16.0, -16.0)).rgb * 0.08; 
	
		
		s_bc += texture(blurInputImage, fragUV + texelSize * vec2(8.0, 8.0)).rgb * 0.25;
		s_bc += texture(blurInputImage, fragUV + texelSize * vec2(-8.0, 8.0)).rgb * 0.25;
		s_bc += texture(blurInputImage, fragUV + texelSize * vec2(8.0, -8.0)).rgb * 0.25;
		s_bc += texture(blurInputImage, fragUV + texelSize * vec2(-8.0, -8.0)).rgb * 0.25;
	} else if (push.bloomQuality >= 3) {	
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(8.0, 8.0)).rgb * 0.0625;
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(-8.0, 8.0)).rgb * 0.0625;
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(8.0, -8.0)).rgb * 0.0625;
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(-8.0, -8.0)).rgb * 0.0625; 
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(12.0, 12.0)).rgb * 0.0625;
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(-12.0, 12.0)).rgb * 0.0625;
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(12.0, -12.0)).rgb * 0.0625;
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(-12.0, -12.0)).rgb * 0.0625; 
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(16.0, 16.0)).rgb * 0.0625;
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(-16.0, 16.0)).rgb * 0.0625;
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(16.0, -16.0)).rgb * 0.0625;
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(-16.0, -16.0)).rgb * 0.0625; 
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(20.0, 20.0)).rgb * 0.0625;
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(-20.0, 20.0)).rgb * 0.0625;
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(20.0, -20.0)).rgb * 0.0625;
		l_bc += texture(blurLargeInputImage, fragUV + texelSize * vec2(-20.0, -20.0)).rgb * 0.0625; 
		
		s_bc += texture(blurInputImage, fragUV + texelSize * vec2(8.0, 8.0)).rgb * 0.17;
		s_bc += texture(blurInputImage, fragUV + texelSize * vec2(-8.0, 8.0)).rgb * 0.17;
		s_bc += texture(blurInputImage, fragUV + texelSize * vec2(8.0, -8.0)).rgb * 0.17;
		s_bc += texture(blurInputImage, fragUV + texelSize * vec2(-8.0, -8.0)).rgb * 0.17; 
		s_bc += texture(blurInputImage, fragUV + texelSize * vec2(16.0, 16.0)).rgb * 0.08;
		s_bc += texture(blurInputImage, fragUV + texelSize * vec2(-16.0, 16.0)).rgb * 0.08;
		s_bc += texture(blurInputImage, fragUV + texelSize * vec2(16.0, -16.0)).rgb * 0.08;
		s_bc += texture(blurInputImage, fragUV + texelSize * vec2(-16.0, -16.0)).rgb * 0.08; 
	}

	float coefB = 1.0 - push.bloomSizeCoeficient;
	vec3 blurColor = s_bc * coefB + l_bc * push.bloomSizeCoeficient;
#else
    vec3 blurColor = texture(blurInputImage, fragUV).rgb;
#endif

    outColor = vec4(
		texture(sceneInputImage, fragUV).rgb * push.exposure +
		blurColor 
		//+texture(lensFlareTexture, fragUV).rgb
		, 1.0);
}