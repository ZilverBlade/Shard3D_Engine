#version 450
#extension GL_KHR_vulkan_glsl : enable

#include "tonemapper.glsl"
#include "gaussian_weights.glsl"

layout (location = 0) in vec2 fragUV;
layout (location = 0) out vec4 outColor;

layout (set = 0, binding = 0) uniform sampler2D sceneInputImage;


layout (push_constant) uniform myParams {
	float threshold;
	float strength;
	float exposure;
    vec2 screenSize;
} push;

vec3 fetchTexel(vec2 coord){
    return max(texture(sceneInputImage, coord).rgb * push.exposure - vec3(push.threshold), vec3(0.0));
}

void main() {
    vec3 crossBlurColor = vec3(0.0);

    vec2 texelSize = 1.0 / push.screenSize;

    for (int i = 0; i < 11; i++){
        int kernelOffset = i - 5;
        crossBlurColor += fetchTexel(vec2(fragUV.x + texelSize.x * kernelOffset, fragUV.y)) * gaussianWeight11[i];
    }
	for (int i = 0; i < 11; i++){
        int kernelOffset = i - 5;
        crossBlurColor += fetchTexel(vec2(fragUV.x, fragUV.y + texelSize.y * kernelOffset)) * gaussianWeight11[i];
    }

    outColor = vec4(crossBlurColor* push.strength, 1.0);
}