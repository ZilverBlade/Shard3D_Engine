#version 450

#include "noise.glsl"

layout (location = 0) in vec2 fragUV;
layout (location = 0) out vec4 outColor;

layout (set = 0, binding = 0) uniform sampler2D inputScene;
layout (set = 0, binding = 1) uniform sampler2D depthBuffer;
layout (set = 0, binding = 2) uniform sampler2D velocityBuffer;
layout (set = 0, binding = 3) uniform sampler2D velocityMaxBuffer;
layout (push_constant) uniform MotionBlurInfo {
	float targetFrameRate;
	float currentFrameRate;
	int samples;
	float cameraNearZ;
	float cameraFarZ;
	float k;
	float kEx;
} push;

void main() {
	vec2 screenSize = textureSize(inputScene, 0);
	vec2 invScreenSize = 1.0 / screenSize;
	float vScale = 0.5 * push.currentFrameRate / push.targetFrameRate;

	vec2 velocity = textureLod(velocityBuffer, fragUV, 0.0).rg * vScale / push.kEx;
	vec3 accumBlur = 0..xxx;
	float weight = 1.0;
	
	float speed = length(velocity * screenSize);
	if (speed <= 1.0) {
		outColor = vec4(textureLod(inputScene, fragUV, 0.0).rgb, 1.0); 
		return;	
	}
	int nSamples = clamp(int(speed), 1, push.samples);
	
	vec2 currentPixel = gl_FragCoord.xy;
    float j = -0.5 + 1.0 * rand(currentPixel);
		
	for (int i = 0; i < nSamples; i++) {
		float t = mix(-1.0, 1.0, (float(i) + j + 1.0) / (float(nSamples) + 1.0));
        vec2 off = velocity * t + invScreenSize * 0.5;

		accumBlur += textureLod(inputScene, fragUV + off, 0.0).rgb;
	}
	outColor = vec4(accumBlur / float(nSamples), 1.0);
}