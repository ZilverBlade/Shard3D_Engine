#version 450

layout (location = 0) in vec2 fragUV;
layout (location = 0) out vec3 outColor;

layout (set = 0, binding = 0) uniform sampler2D inputRenderedScene;

layout (push_constant) uniform Push {
	float ghostDistribution;
	int ghostSamples;
	uint lensColorTexture;
	uint lensFlareTexture;
	vec3 channelDistort;
} push;

vec3 sampleDistorted(vec2 uv, vec2 dir, vec3 distort) {
	return max(vec3(0.0), vec3(
		texture(inputRenderedScene, uv + dir * distort.r).r,
		texture(inputRenderedScene, uv + dir * distort.g).g,
		texture(inputRenderedScene, uv + dir * distort.b).b
	));
}

void main() {
	vec2 texcoord = -fragUV + vec2(1.0);
	vec2 texelSize = 1.0 / vec2(textureSize(inputRenderedScene, 0));
	  
	vec2 ghostVec = (vec2(0.5) - texcoord) * push.ghostDistribution;
	
	vec2 distortDir = normalize(ghostVec);
	
	vec3 result = vec3(0.0);
	for (int i = 0; i < push.ghostSamples; ++i) {
		vec2 haloVec = distortDir * 0.04;
		vec2 offset = fract(texcoord + haloVec);
		float weight = length(vec2(0.5) - offset) / length(vec2(0.5));
		weight = pow(1.0 - weight, 5.0);
		result += sampleDistorted(texcoord + haloVec, distortDir, push.channelDistort).rgb * weight;
	}
	//result *= texture(tex[push.lensColorTexture], length(vec2(0.5) - texcoord).rgb / length(vec2(0.5)));
	outColor = result;
}