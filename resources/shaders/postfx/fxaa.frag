#version 450
#extension GL_KHR_vulkan_glsl : enable
#extension GL_GOOGLE_include_directive : enable
#include "math.glsl"

layout (location = 0) in vec2 fragUV;
layout (location = 0) out vec4 outColor;

layout (set = 0, binding = 0) uniform sampler2D sceneInputImage;
layout (push_constant) uniform myParams {
	vec2 screenSize;
	float invGamma;
	float gamma;
	float maxSpan;
} push;


vec3 sampleGammaTexture(vec2 uv) {
	return pow(texture(sceneInputImage, uv).rgb, vec3(push.invGamma));
}

void main() {

	const float FXAA_MIN_REDC = 1.0 / 128.0;
	const float FXAA_MAX_SPAN = push.maxSpan;
	const float FXAA_MLT_REDC = 1.0 / FXAA_MAX_SPAN;

    const vec2 texelSize = vec2(1.0) / push.screenSize.xy;
	//const vec3 luma = vec3( 0.2126, 0.7152, 0.0722 );
	
	//// [tb][lr]
	//mat2 lumaMatrix;
	//lumaMatrix[0][0] = dot(sampleGammaTexture(fragUV + (vec2(-1.0, -1.0) * texelSize)), luma);
	//lumaMatrix[0][1] = dot(sampleGammaTexture(fragUV + (vec2(1.0, -1.0) * texelSize)), luma);
	//lumaMatrix[1][0] = dot(sampleGammaTexture(fragUV + (vec2(-1.0, 1.0) * texelSize)), luma);
	//lumaMatrix[1][1] = dot(sampleGammaTexture(fragUV + (vec2(1.0, 1.0) 	* texelSize)), luma);
	//float lumaMid = dot(sampleGammaTexture(fragUV), luma);
	//
	//vec2 dir = vec2(
	//	-(lumaMatrix[0][0] + lumaMatrix[0][1]) - (lumaMatrix[1][0] + lumaMatrix[1][1]),
	//	 (lumaMatrix[0][0] + lumaMatrix[1][0]) - (lumaMatrix[0][1] + lumaMatrix[1][1])
	//);
	//const float dirRdc = max(
	//	(lumaMatrix[0][0] + lumaMatrix[0][1] + lumaMatrix[1][0] + lumaMatrix[1][1]) * FXAA_MLT_REDC * 0.25, FXAA_MIN_REDC
	//);
	//
	//float invDirAdj = 1.0 / (min(abs(dir.x), abs(dir.y)) + dirRdc);
	//dir = clamp(dir * invDirAdj, vec2(-FXAA_MAX_SPAN), vec2(FXAA_MAX_SPAN)) * texelSize;
	//
	//vec3 ra = (
	//	sampleGammaTexture(fragUV + (dir * vec2(1.0/3.0 - 0.5))).rgb +
	//	sampleGammaTexture(fragUV + (dir * vec2(2.0/3.0 - 0.5))).rgb
	//) * 0.5;
	//vec3 rb = (
	//	sampleGammaTexture(fragUV + (dir * vec2(-0.5))).rgb +
	//	sampleGammaTexture(fragUV + (dir * vec2(0.5))).rgb
	//) * 0.25 + ra * 0.5;
	//
	//float luma_min = min(lumaMid, 
	//min(min(lumaMatrix[0][0], lumaMatrix[0][1]), 
	//	min(lumaMatrix[1][0], lumaMatrix[1][1]))
	//);
	//float luma_max = max(lumaMid, 
	//max(max(lumaMatrix[0][0], lumaMatrix[0][1]), 
	//	max(lumaMatrix[1][0], lumaMatrix[1][1]))
	//);
	//
	//float luma_rb = dot(luma, rb);
	//
	//bool isOutsideLuminocity = (luma_rb < luma_min || luma_rb > luma_max);
	//
	//vec3 result = ra * float(int(isOutsideLuminocity)) + rb * float(int(!isOutsideLuminocity));
	//outColor = vec4(pow(result, vec3(push.gamma)), 1.0);
	
	 vec3 rgbM = textureLod(sceneInputImage, fragUV, 0.0).rgb;

	// Sampling neighbour texels. Offsets are adapted to OpenGL texture coordinates. 
	
	// see http://en.wikipedia.org/wiki/Grayscale
	const vec3 luma = vec3(0.299, 0.587, 0.114);
	
	// Convert from RGB to luma.
	float lumaNW = dot(sampleGammaTexture(fragUV + (vec2(-1.0, 1.0) * texelSize)), luma);
	float lumaNE = dot(sampleGammaTexture(fragUV + (vec2(1.0, 1.0) 	* texelSize)), luma);
	float lumaSW = dot(sampleGammaTexture(fragUV + (vec2(-1.0, -1.0) * texelSize)), luma);
	float lumaSE = dot(sampleGammaTexture(fragUV + (vec2(1.0, -1.0) * texelSize)), luma);
	float lumaM = dot(sampleGammaTexture(fragUV), luma);

	float u_lumaThreshold = 0.5;

	// Gather minimum and maximum luma.
	float lumaMin = min(lumaM, min(min(lumaNW, lumaNE), min(lumaSW, lumaSE)));
	float lumaMax = max(lumaM, max(max(lumaNW, lumaNE), max(lumaSW, lumaSE)));
	
	// If contrast is lower than a maximum threshold ...
	if (lumaMax - lumaMin <= lumaMax * u_lumaThreshold)
	{
		// ... do no AA and return.
		outColor = vec4(rgbM, 1.0);
		
		return;
	}  
	
	// Sampling is done along the gradient.
	vec2 samplingDirection;	
	samplingDirection.x = -((lumaNW + lumaNE) - (lumaSW + lumaSE));
    samplingDirection.y =  ((lumaNW + lumaSW) - (lumaNE + lumaSE));
    
    // Sampling step distance depends on the luma: The brighter the sampled texels, the smaller the final sampling step direction.
    // This results, that brighter areas are less blurred/more sharper than dark areas.  
    float samplingDirectionReduce = max((lumaNW + lumaNE + lumaSW + lumaSE) * 0.25 * FXAA_MLT_REDC, FXAA_MIN_REDC);

	// Factor for norming the sampling direction plus adding the brightness influence. 
	float minSamplingDirectionFactor = 1.0 / (min(abs(samplingDirection.x), abs(samplingDirection.y)) + samplingDirectionReduce);
    
    // Calculate final sampling direction vector by reducing, clamping to a range and finally adapting to the texture size. 
    samplingDirection = clamp(samplingDirection * minSamplingDirectionFactor, vec2(-FXAA_MAX_SPAN), vec2(FXAA_MAX_SPAN)) * texelSize;
	
	// Inner samples on the tab.
	vec3 rgbSampleNeg = textureLod(sceneInputImage, fragUV + samplingDirection * (1.0/3.0 - 0.5), 0.0).rgb;
	vec3 rgbSamplePos = textureLod(sceneInputImage, fragUV + samplingDirection * (2.0/3.0 - 0.5), 0.0).rgb;

	vec3 rgbTwoTab = (rgbSamplePos + rgbSampleNeg) * 0.5;  

	// Outer samples on the tab.
	vec3 rgbSampleNegOuter = textureLod(sceneInputImage, fragUV + samplingDirection * (0.0/3.0 - 0.5), 0.0).rgb;
	vec3 rgbSamplePosOuter = textureLod(sceneInputImage, fragUV + samplingDirection * (3.0/3.0 - 0.5), 0.0).rgb;
	
	vec3 rgbFourTab = (rgbSamplePosOuter + rgbSampleNegOuter) * 0.25 + rgbTwoTab * 0.5;   
	
	// Calculate luma for checking against the minimum and maximum value.
	float lumaFourTab = dot(rgbFourTab, luma);
	
	// Are outer samples of the tab beyond the edge ... 
	if (lumaFourTab < lumaMin || lumaFourTab > lumaMax)
	{
		// ... yes, so use only two samples.
		outColor = vec4(rgbTwoTab, 1.0); 
	}
	else
	{
		// ... no, so use four samples. 
		outColor = vec4(rgbFourTab, 1.0);
	}
}
