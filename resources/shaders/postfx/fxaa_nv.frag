#version 450
#extension GL_KHR_vulkan_glsl : enable
#extension GL_GOOGLE_include_directive : enable


#define FXAA_PC 1
#define FXAA_GLSL_130 1 

//#define FXAA_QUALITY__PRESET 12 // low
//#define FXAA_QUALITY__PRESET 25	// medium
#define FXAA_QUALITY__PRESET 39	// high

#include "fxaa3_11.h"


layout (location = 0) in vec2 fragUV;
layout (location = 0) out vec4 outColor;

layout (set = 0, binding = 0) uniform sampler2D sceneInputImage;
layout (push_constant) uniform myParams {
	vec2 screenSize;
} push;

void main()	{
    outColor = FxaaPixelShader(
		fragUV, 
		vec4(0.0),
		sceneInputImage,
		sceneInputImage,
		sceneInputImage,
		1.0 / push.screenSize,
		vec4(0.0),
		vec4(0.0),
		vec4(0.0),
		0.75,
		0.125,
		0.0625,
		0.0,
		0.0,
		0.0,
		vec4(0.0)
	);
	
    //vec4(fxaaColor.xyz, 1.0); // RGBA16F
}