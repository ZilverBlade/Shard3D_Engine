#version 450

layout (set = 0, binding = 0) uniform sampler2D velocityTileMax;

layout (location = 0) in vec2 fragUV;
layout (location = 0) out vec2 velocityNeighborhood;

void main() {	
	vec2 resolution = textureSize(velocityTileMax, 0);
	vec2 invRes = 1.0 / (resolution);
	
	vec2 highestVelocity = vec2(0.0);
	float length2HighestVelocity = 0.0;
	
	for (int x = -1; x <= 1; x++) {
		for (int y = -1; y <= 1; y++) {
			vec2 sampled = textureLod(velocityTileMax, fragUV + vec2(x, y) * invRes, 0.0).xy;
			float length2Sampled = dot(sampled, sampled);
			bool greater = length2Sampled > length2HighestVelocity;
			length2HighestVelocity = max(length2Sampled, length2HighestVelocity);
			highestVelocity = sampled * float(greater) + highestVelocity * float(!greater);		
		}
	}
	
	velocityNeighborhood = highestVelocity;
} 
