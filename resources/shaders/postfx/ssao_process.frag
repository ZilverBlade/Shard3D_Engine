#version 450
#extension GL_EXT_control_flow_attributes : enable

layout (location = 0) in vec2 fragUV;
layout (location = 0) out float outColor;

#include "depth.glsl"

layout (set = 0, binding = 0) uniform sampler2D ssaoBlurImage;
layout (set = 0, binding = 1) uniform sampler2D depthImage;
layout (push_constant) uniform SSAOInfo{
	vec2 screenSize;
	vec2 maxTiles;
	int tileIndex;
	int interleaveX;
	int interleaveY;
	float radius;
	float bias;
	float intensity;
	float power;
	int kernelQuality;
	float near;
	float far;
} push;

const vec2 offsets[4] = {
	{0.0, 1.0},
	{1.0, 0.0},
	{0.0, -1.0},
	{-1.0, 0.0}
};

void main() {
	float linearDepths[4];
	float depthPivot = textureLod(depthImage, fragUV, 0.0).x;
	float linearDepthPivot = 
#ifdef S3DSDEF_SHADER_PERMUTATION_INFINITE_FAR_Z
			depthPivot == 0.0 ? 1e7 :
#endif
			linearize_depth(depthPivot, push.near, push.far);
	
	vec2 texelSize = 1.0 / textureSize(depthImage, 0).xy;
	[[unroll]]
	for (int i = 0; i < 4; i++) {
		float depth = textureLod(depthImage, fragUV + texelSize * offsets[i], 0.0).x;
		linearDepths[i] =
#ifdef S3DSDEF_SHADER_PERMUTATION_INFINITE_FAR_Z
			depth == 0.0 ? 1e7 : // scenes regardless should never exceed 1000km in size
#endif
			linearize_depth(depth, push.near, push.far);
	}
	float ssaoSum = 0.0;
	float weightSum = 0.0;
	if (linearDepths[0] == linearDepths[1] && linearDepths[2] == linearDepths[3] && linearDepths[1] == linearDepths[2]) {
		ssaoSum = textureLod(ssaoBlurImage, fragUV, 0.0).x;
		weightSum = 1.0;
	} else {
		[[unroll]]
		for (int i = 0; i < 4; i++) {
			float weight = 1.0 / (0.001 + abs(linearDepths[i] - linearDepthPivot) * 0.01);
			weightSum += weight;
			ssaoSum += weight * textureLod(ssaoBlurImage, fragUV + texelSize * offsets[i], 0.0).x;
		}
	}
    outColor = pow(1.0 - push.intensity * (1.0 - ssaoSum / weightSum), push.power); // R8
}