#version 450
#extension GL_KHR_vulkan_glsl : enable

layout (location = 0) in vec2 fragUV;
layout (location = 0) out float outColor;

layout (set = 0, binding = 0) uniform sampler2D sceneInputImage;
layout (push_constant) uniform myParams {
    vec2 screenSize;
} push;

float fetchTexel(vec2 coord){
    return texture(sceneInputImage, coord).r;
}
//#define SHADOW_BLUR_KERNEL_STRIKE // shittiest, dont use
#define SHADOW_BLUR_KERNEL_FAN // fastest 9x9 gaussian approximation, better for spot/point lights
//#define SHADOW_BLUR_KERNEL_TRIANGLE // softer, but slower (2 more texel fetches per pixel (40% more)), better for directional lights
//#define SHADOW_BLUR_KERNEL_CROSS // softer (and shittier), but slowest (4 more texel fetches per pixel (80% more))

#define NORTH 	vec2(0.000, 1.000)
#define SOUTH 	vec2(0.0, -1.000)

#ifdef SHADOW_BLUR_KERNEL_TRIANGLE
#define NWEST 	vec2(-0.866, 0.500)
#define NEAST 	vec2(0.866, 0.500)

#define SWEST 	vec2(-0.866, -0.500)
#define SEAST 	vec2(0.866, -0.500)
#else
#define NWEST 	vec2(-0.707, 0.707)
#define NEAST 	vec2(0.707, 0.707)

#define SWEST 	vec2(-0.707, -0.707)
#define SEAST 	vec2(0.707, -0.707)
#endif
#define SMALL  	vec2(1.4846153846)
#define LARGE  	vec2(3.2307692308)

#ifdef SHADOW_BLUR_KERNEL_TRIANGLE
#define WEAKG  	0.0377300000
#define HEAVY  	0.1630000000
#define CENTR   0.3978100000
#endif
#ifdef SHADOW_BLUR_KERNEL_FAN
#define WEAKG   0.0702702703
#define HEAVY  	0.3162162162
#define CENTR   0.2270270270
#endif
#ifdef SHADOW_BLUR_KERNEL_STRIKE
#define WEAKG   0.0702702703
#define HEAVY  	0.3162162162
#define CENTR   0.2270270270
#endif
#ifdef SHADOW_BLUR_KERNEL_CROSS
#define WEAKG   0.0351351351
#define HEAVY   0.1581081081
#define CENTR   0.2270270270
#endif
#ifndef CENTR
#define CENTR   1.0
#endif

void main()	{  
    float blurColor = float(0.0);
    float texelSizeX = 1.0 / push.screenSize.x;
	blurColor += fetchTexel(fragUV) * CENTR;

#ifdef SHADOW_BLUR_KERNEL_STRIKE // light (5 texel fetches)
	blurColor += fetchTexel(fragUV + (SMALL * NEAST * texelSizeX)) * HEAVY;
	blurColor += fetchTexel(fragUV + (SMALL * SWEST * texelSizeX)) * HEAVY;
	blurColor += fetchTexel(fragUV + (LARGE * NEAST * texelSizeX)) * WEAKG;
	blurColor += fetchTexel(fragUV + (LARGE * SWEST * texelSizeX)) * WEAKG;
#endif
#ifdef SHADOW_BLUR_KERNEL_FAN // light (5 texel fetches)
	blurColor += fetchTexel(fragUV + (SMALL * NEAST * texelSizeX)) * HEAVY;
	blurColor += fetchTexel(fragUV + (SMALL * SWEST * texelSizeX)) * HEAVY;
	blurColor += fetchTexel(fragUV + (LARGE * NORTH * texelSizeX)) * WEAKG;
	blurColor += fetchTexel(fragUV + (LARGE * SOUTH * texelSizeX)) * WEAKG;
#endif
#ifdef SHADOW_BLUR_KERNEL_CROSS // expensive (9 texel fetches)
	// (X, Y) >>> (-X, -Y)
	blurColor += fetchTexel(fragUV + (SMALL * NEAST * texelSizeX)) * HEAVY;
	blurColor += fetchTexel(fragUV + (SMALL * SWEST * texelSizeX)) * HEAVY;
	blurColor += fetchTexel(fragUV + (LARGE * NEAST * texelSizeX)) * WEAKG;
	blurColor += fetchTexel(fragUV + (LARGE * SWEST * texelSizeX)) * WEAKG;
	
	// (-X, Y) >>> (X, -Y)
	blurColor += fetchTexel(fragUV + (SMALL * NWEST * texelSizeX)) * HEAVY;
	blurColor += fetchTexel(fragUV + (SMALL * SEAST * texelSizeX)) * HEAVY;
	blurColor += fetchTexel(fragUV + (LARGE * NWEST * texelSizeX)) * WEAKG;
	blurColor += fetchTexel(fragUV + (LARGE * SEAST * texelSizeX)) * WEAKG;
#endif
#ifdef SHADOW_BLUR_KERNEL_TRIANGLE // costly (7 texel fetches)	
	// triange inner
	blurColor += fetchTexel(fragUV + (SMALL * NEAST * texelSizeX)) * HEAVY;
	blurColor += fetchTexel(fragUV + (SMALL * NWEST * texelSizeX)) * HEAVY;
	blurColor += fetchTexel(fragUV + (SMALL * SOUTH * texelSizeX)) * HEAVY;
	
	// triange outer
	blurColor += fetchTexel(fragUV + (LARGE * SEAST * texelSizeX)) * WEAKG;
	blurColor += fetchTexel(fragUV + (LARGE * SWEST * texelSizeX)) * WEAKG;
	blurColor += fetchTexel(fragUV + (LARGE * NORTH * texelSizeX)) * WEAKG;
#endif

    outColor = blurColor;
}