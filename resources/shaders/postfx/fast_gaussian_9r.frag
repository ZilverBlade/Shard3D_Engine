#version 450
#extension GL_KHR_vulkan_glsl : enable

layout (location = 0) in vec2 fragUV;
layout (location = 0) out float outColor;

layout (set = 0, binding = 0) uniform sampler2D sceneInputImage;
layout (push_constant) uniform myParams {
    vec2 screenSize;
	vec2 direction;
} push;

float fetchTexel(vec2 coord){
    return texture(sceneInputImage, coord).r;
}

void main()	{
    float blurColor = 0.0;
    float texelSizeX = 1.0 / push.screenSize.x;
	vec2 off1 = vec2(1.2846153846) * push.direction;
	vec2 off2 = vec2(2.5307692308) * push.direction;
	blurColor += fetchTexel(fragUV - off2 * texelSizeX) * 0.0702702703;
	blurColor += fetchTexel(fragUV - off1 * texelSizeX) * 0.3162162162;
	blurColor += fetchTexel(fragUV					 ) * 0.2270270270;
	blurColor += fetchTexel(fragUV + off1 * texelSizeX) * 0.3162162162;
	blurColor += fetchTexel(fragUV + off2 * texelSizeX) * 0.0702702703;
  
    outColor = blurColor;
}