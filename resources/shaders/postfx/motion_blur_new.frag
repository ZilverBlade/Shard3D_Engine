#version 450

#include "noise.glsl"
#include "depth.glsl"

layout (location = 0) in vec2 fragUV;
layout (location = 0) out vec4 outColor;

layout (set = 0, binding = 0) uniform sampler2D inputScene;
layout (set = 0, binding = 1) uniform sampler2D depthBuffer;
layout (set = 0, binding = 2) uniform sampler2D velocityBuffer;
layout (set = 0, binding = 3) uniform sampler2D velocityNeighborMax;

layout (push_constant) uniform MotionBlurInfo {
	float targetFrameRate;
	float currentFrameRate;
	int samples;
	float cameraNearZ;
	float cameraFarZ;
	float k;
	float kEx;
} push;

float cone(vec2 x, vec2 y, float v) {
	return clamp(1.0 - length(x - y) / v, 0.0, 1.0);
}
float cylinder(vec2 x, vec2 y, float v) {
	return 1.0 - smoothstep(0.95 * v, 1.05 * v, length(x - y));
}

// We use SOFT Z EXTENT = 1mm to 10cm for our results
const float SOFT_Z_EXTENT = 0.1; // 10 cm
float softDepthCompare(float z0, float z1) {
#ifdef REVERSE_DEPTH_BUFFER
	float diff = z0 - z1;
#else
	float diff = z1 - z0;
#endif
	return clamp(1.0 - diff / SOFT_Z_EXTENT, 0.0, 1.0);
}

void main() {
	vec2 screenSize = textureSize(inputScene, 0);
	vec2 invScreenSize = 1.0 / screenSize;
	
	float vScale = 0.5 * push.currentFrameRate / push.targetFrameRate;
	
	vec3 original = textureLod(inputScene, fragUV, 0.0).rgb;
	
	//vec2 tileSize = vec2(push.k) * 3.0;
	//vec2 currentTile = floor(vec2(gl_FragCoord.xy) / tileSize) * tileSize + vec2(push.k) * 1.5;
	vec2 maxVelocity = textureLod(velocityNeighborMax, fragUV, 0.0).rg * vScale / push.kEx;
	float maxSpeed = length(maxVelocity * screenSize);

	if (maxSpeed <= 0.5) {
		outColor = vec4(original, 1.0); 
		return;
	}
	
	vec2 currentVelocity = textureLod(velocityBuffer, fragUV, 0.0).rg * vScale / push.kEx;
	float currentSpeed = length(currentVelocity * screenSize);
	
		float currentDepthNonLinear = textureLod(depthBuffer, fragUV, 0.0).r;
		float currentDepth =
#ifdef S3DSDEF_SHADER_PERMUTATION_INFINITE_FAR_Z
			currentDepthNonLinear == 0.0 ? 1e7 : // scenes regardless should never exceed 1000km in size
#endif
			linearize_depth(currentDepthNonLinear, push.cameraNearZ, push.cameraFarZ); 

	float weight = currentSpeed == 0.0 ? 1.0 : 1.0 / currentSpeed;
	vec3 accumBlur = original * weight;
	
	int nSamples = clamp(int(maxSpeed), 1, push.samples);
	
	vec2 currentPixel = gl_FragCoord.xy;
    float j = -0.5 + 1.0 * rand(currentPixel);
		
	for (int i = 0; i < nSamples; i++) {
		float t = mix(-1.0, 1.0, (float(i) + j + 1.0) / (float(nSamples) + 1.0));
        vec2 off = maxVelocity * t + invScreenSize * 0.5;
		
		float depthNonLinear = textureLod(depthBuffer, fragUV + off, 0.0).r;
		float depth =
#ifdef S3DSDEF_SHADER_PERMUTATION_INFINITE_FAR_Z
			depthNonLinear == 0.0 ? 1e7 : // scenes regardless should never exceed 1000km in size
#endif
			linearize_depth(depthNonLinear, push.cameraNearZ, push.cameraFarZ); 
		vec3 color = textureLod(inputScene, fragUV + off, 0.0).rgb;			
		vec2 velocity = textureLod(velocityBuffer, fragUV + off, 0.0).rg * vScale / push.kEx;
		float speed = length(velocity * screenSize);
		
		float f = softDepthCompare(currentDepth, depth);
		float b = softDepthCompare(depth, currentDepth);
		
		vec2 pixel = (fragUV + off) * (screenSize + vec2(1.0));
		
		float fc = f * cone(currentPixel, pixel, currentSpeed);
		float bc = b * cone(currentPixel, pixel, speed);
		
		float cyl = cylinder(currentPixel, pixel, currentSpeed) * cylinder(currentPixel, pixel, speed) * 2.0;
		
		float w = fc + bc + cyl;
		
		accumBlur += color * w;
		weight += w;
	}
	outColor = vec4((accumBlur / weight) /** vec3((maxVelocity * 0.5 + 0.5) * 10.0, 1.0)*/, 1.0);
}