#version 450
#extension GL_KHR_vulkan_glsl : enable
#extension GL_EXT_control_flow_attributes : enable

#include "depth.glsl"
layout (location = 0) in vec2 fragUV;
layout (location = 0) out float outColor;

layout (set = 0, binding = 0) uniform sampler2D sceneInputImage;
layout (set = 0, binding = 1) uniform sampler2D depthInputImage;
layout (push_constant) uniform myParams {
    vec2 screenSize;
	vec2 direction;
	int bilateralBlurType; // 0 is color based, 1 is depth based
	float nearZ;
	float farZ;
} push;

#define SIGMA 10.0
#define BSIGMA 0.1
#define MSIZE 10
float normpdf(in float x, in float sigma) {
	return 0.39894*exp(-0.5*x*x/(sigma*sigma))/sigma;
}

float fetchTexel(vec2 coord){
    return texture(sceneInputImage, coord).r;
}

//void main()	{
//    float blurColor = 0.0;
//	const float base = fetchTexel(fragUV);
//	const vec2 texelSize = 1.0 / push.screenSize.xy;
//	
//	const vec2 dir = push.direction * texelSize;
//	
//
//	const vec2 off1 = vec2(1.2846153846) * dir;
//	const vec2 off2 = vec2(2.5307692308) * dir;
//	
//	
//	float Z = 0.2270270270;
//	const float bZ = 1.0/normpdf(0.0, BSIGMA);
//
//	
//	const float cc0 = fetchTexel(fragUV - off2);
//	const float factor0 = normpdf(cc0-base, BSIGMA)*bZ * 0.0702702703;
//	const float cc1 = fetchTexel(fragUV - off1);
//	const float factor1 = normpdf(cc1-base, BSIGMA)*bZ * 0.3162162162;
//	const float cc2 = fetchTexel(fragUV + off1);
//	const float factor2 = normpdf(cc2-base, BSIGMA)*bZ * 0.3162162162;
//	const float cc3 = fetchTexel(fragUV + off2);
//	const float factor3 = normpdf(cc3-base, BSIGMA)*bZ * 0.0702702703;
//	
//	Z += factor0 + factor1 + factor2 + factor3;
//	blurColor += factor0*cc0 + factor1*cc1 + factor2*cc2 + factor3*cc3;
//
//    outColor = blurColor / Z;
//}

void main() {
	float c = fetchTexel(fragUV);
	float d = 0.0;
	
	if (push.bilateralBlurType == 1) { // depth conv
		float depth = textureLod(depthInputImage, fragUV, 0.0).r;
		d =
#ifdef S3DSDEF_SHADER_PERMUTATION_INFINITE_FAR_Z
			depth == 0.0 ? 1e7 : // scenes regardless should never exceed 1000km in size
#endif
			linearize_depth(depth, push.nearZ, push.farZ);
	}
	//if (c > 0.99) { // threshold for blur, improve performance by skipping non ambient occluded fragments
	//	outColor = c;
	//	return;
	//}
	
	const int kSize = (MSIZE-1)/2;
	float kernel[MSIZE];
	float final_colour = 0.0;
	
	//create the 1-D kernel
	float Z = 0.0;
	[[unroll]]
	for (int j = 0; j <= kSize; ++j) {
		kernel[kSize+j] = kernel[kSize-j] = normpdf(float(j), SIGMA);
	}
	
	const vec2 texelSize = 1.0 / push.screenSize.xy;
	const vec2 dir = push.direction * texelSize;
	
	float cc;
	float dd;
	float factor;
	float bZ = 1.0/normpdf(0.0, BSIGMA);
	//read out the texels
	if (push.bilateralBlurType == 0) {
		[[unroll]]
		for (int i=-kSize; i <= kSize; ++i) {
			cc = fetchTexel(fragUV + float(i) * dir );
			factor = normpdf(cc-c, BSIGMA)*bZ*kernel[kSize+i];
			Z += factor;
			final_colour += factor*cc;
		}
	} else {
		[[unroll]]
		for (int i=-kSize; i <= kSize; ++i) {
			cc = fetchTexel(fragUV + float(i) * dir);
			float ddepth = texture(depthInputImage, fragUV + float(i) * dir).r;
			dd =
#ifdef S3DSDEF_SHADER_PERMUTATION_INFINITE_FAR_Z
				ddepth == 0.0 ? 1e7 : // scenes regardless should never exceed 1000km in size
#endif
				linearize_depth(ddepth, push.nearZ, push.farZ);
			float diff = abs(dd-d);
			factor = normpdf(pow(diff, 0.35), BSIGMA)*bZ*kernel[kSize+i];
			Z += factor;
			final_colour += factor*cc;
		}
	}
	
	outColor = final_colour / Z;
}