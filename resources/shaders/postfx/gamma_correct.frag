#version 450

layout (location = 0) in vec2 fragUV;
layout (location = 0) out vec4 outColor;

layout (set = 0, binding = 0) uniform sampler2D sceneInputImage;
layout (push_constant) uniform Push {
    float invGamma;
} push;

void main()	{
    vec3 color = texture(sceneInputImage, fragUV).rgb; 
    outColor = vec4(pow(color, vec3(push.invGamma)), 1.0); 
}