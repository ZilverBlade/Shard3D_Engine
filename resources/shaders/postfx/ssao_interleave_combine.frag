#version 450
#extension GL_KHR_vulkan_glsl : enable

layout (location = 0) in vec2 fragUV;
layout (location = 0) out float outColor;

layout (set = 0, binding = 0) uniform sampler2D ssaoInterleave00;
layout (set = 0, binding = 1) uniform sampler2D ssaoInterleave01;
layout (set = 0, binding = 2) uniform sampler2D ssaoInterleave10;
layout (set = 0, binding = 3) uniform sampler2D ssaoInterleave11;

void main() {
	
	ivec2 interleavedIndex = ivec2(int(gl_FragCoord.x) % 2, int(gl_FragCoord.y) % 2);
	int interleavedPass = interleavedIndex.x + interleavedIndex.y * 2;
	
	switch (interleavedPass) {
	case(0):
		outColor = textureLod(ssaoInterleave00, fragUV, 0.0).x;
		break;
	case(1):
		outColor = textureLod(ssaoInterleave01, fragUV, 0.0).x;
		break;
	case(2):
		outColor = textureLod(ssaoInterleave10, fragUV, 0.0).x;
		break;
	case(3):
		outColor = textureLod(ssaoInterleave11, fragUV, 0.0).x;
		break;
	}
}