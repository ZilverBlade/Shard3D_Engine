#version 450

layout (set = 0, binding = 0) uniform sampler2D velocity;

layout (location = 0) in vec2 fragUV;
layout (location = 0) out vec2 velocityMax;

layout (push_constant) uniform Push {
	int k;
} push;

void main() {
	int hk = push.k / 2;
	
	vec2 resolution = textureSize(velocity, 0);
	vec2 invRes = 1.0 / (resolution);
	
	vec2 highestVelocity = vec2(0.0);
	float length2HighestVelocity = 0.0;
	
	for (int x = -hk; x <= hk; x++) {
		for (int y = -hk; y <= hk; y++) {
			vec2 sampled = textureLod(velocity, fragUV + vec2(x, y) * invRes, 0.0).xy;
			float length2Sampled = dot(sampled, sampled);
			//if (lengthSampled > lengthHighestVelocity) {
			bool greater = length2Sampled > length2HighestVelocity;
			length2HighestVelocity = max(length2Sampled, length2HighestVelocity);
			highestVelocity = sampled * float(greater) + highestVelocity * float(!greater);
			//}
			
		}
	}
	
	velocityMax = highestVelocity;
}  