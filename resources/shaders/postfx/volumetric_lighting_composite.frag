#version 450

layout (location = 0) in vec2 fragUV;
layout (location = 0) out vec4 outColor;

layout (set = 0, binding = 0) uniform sampler2D volumetrics;





void main()	{

//float upSampledDepth = depth.Load(int3(screenCoordinates, 0)).x;
//
//float3 color = 0.0f.xxx;
//float totalWeight = 0.0f;
//
//// Select the closest downscaled pixels.
//
//int xOffset = screenCoordinates.x % 2 == 0 ? -1 : 1;
//int yOffset = screenCoordinates.y % 2 == 0 ? -1 : 1;
//
//int2 offsets[] = {int2(0, 0),
//int2(0, yOffset),
//int2(xOffset, 0),
//int2(xOffset, yOffset)};
//
//for (int i = 0; i < 4; i ++)
//{
//
//float3 downscaledColor = volumetricLightTexture.Load(int3(downscaledCoordinates + offsets[i], 0));
//
//float downscaledDepth = depth.Load(int3(downscaledCoordinates, + offsets[i] 1));
//
//float currentWeight = 1.0f;
//currentWeight *= max(0.0f, 1.0f - (0.05f) * abs(downscaledDepth - upSampledDepth));
//
//color += downscaledColor * currentWeight;
//totalWeight += currentWeight;
//
//}
//
//float3 volumetricLight;
//const float epsilon = 0.0001f;
//volumetricLight.xyz = color/(totalWeight + epsilon);
//
//return float4(volumetricLight.xyz, 1.0f);


	vec3 mist = textureLod(volumetrics, fragUV, 0.0).rgb;
	bvec3 inf = isinf(mist);
	vec3 scattering = vec3(
		inf.x ? 6.5e+4 : mist.x,
		inf.y ? 6.5e+4 : mist.y,
		inf.z ? 6.5e+4 : mist.z
	);
    outColor = vec4(scattering, 1.0); // RGBA16F
}