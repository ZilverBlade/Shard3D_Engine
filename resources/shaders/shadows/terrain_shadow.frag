#version 450
#extension GL_EXT_nonuniform_qualifier : enable
#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_control_flow_attributes : enable

#ifdef ReflectiveShadow
#define DESCRIPTOR_BINDLESS_SET 1


// 0 == Dir, 1 == Spot
layout (constant_id = 0) const uint LIGHT_TYPE = 0;
layout (constant_id = 1) const uint maxMaterialCount = 4; 
layout (constant_id = 2) const bool enableParallaxMapping = false; // no point in doing parallax mapping for the RSM

#define TERRAIN_MATERIAL_ONLY_DIFFUSE
#include "descriptor_indexing.glsl"
#include "terrain_material.glsl"

layout(set = 2, binding = 1) uniform TerrainMaterialBuffer_ {
	TerrainMaterialBuffer buff;
} materialBuffer;
layout(set = 2, binding = 2) uniform sampler2D heightMap;
layout(set = 2, binding = 3) uniform sampler2D materialMap0;
//layout(set = 2, binding = 3) uniform sampler2DArray materialMap1;

#endif

layout (set = 1, binding = 0) uniform ShadowMatrixData {
	mat4 viewProjection;
	mat4 invView;
	mat4 invProjection;
	// RSM LIGHT DATA
	
	vec3 RSMLIGHT_direction;
	vec3 RSMLIGHT_intensity;
	vec3 RSMLIGHT_position;
	vec2 RSMLIGHT_cosAngles;
} shadowBuffer;
#define LAST_IMG_LOCATION -1
#ifdef VarianceShadow
layout (location = 0) out vec2 variance;
#undef LAST_IMG_LOCATION
#define LAST_IMG_LOCATION 0
#endif
#ifdef ReflectiveShadow
layout (location = LAST_IMG_LOCATION + 1) out vec3 flux;
layout (location = LAST_IMG_LOCATION + 2) out vec4 normal;
#endif

layout(location = 0) in vec2 fragUV;
#ifdef ReflectiveShadow
layout(location = 1) in vec3 fragPosWorld;
#endif
layout(location = 3) in flat uint materialIndex;

void main(){
#ifdef VarianceShadow
	float d = gl_FragCoord.z;
	
	float dx = dFdx(d);
	float dy = dFdy(d);
	float moment_y = d * d + 0.25 * (dx * dx + dy * dy);

	variance = vec2(d, moment_y);
#endif

#ifdef ReflectiveShadow
	vec4 materialWeights0 = max(texture(materialMap0, fragUV) - 1e-2, 0.0);
	materialWeights0 /= dot(materialWeights0, 1..xxxx);
	
	float materialWeightArray[maxMaterialCount];
	[[unroll]]
	for (int i = 0; i < min(maxMaterialCount, 4); i++) {
		materialWeightArray[i] = materialWeights0[i];
	}
	//vec4 materialWeights1 = 0..xxxx;
	//materialWeights1 /= dot(materialWeights1, 1..xxxx);
	//[[unroll]]
	//for (int i = 4; i < min(maxMaterialCount, 8); i++) {
	//	materialWeightArray[i] = materialWeights1[i];
	//}
	
	TerrainMaterialResult result;
	result	= getTerrainMaterialData(fragPosWorld, fragNormalWorld, materialBuffer.buff.materials, materialWeightArray);
	
#endif
}