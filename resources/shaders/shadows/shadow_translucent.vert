#version 450
#define DISABLE_PARTIAL_DERIVATIVES

#ifdef RIGGED_MESH
#define RIGGDED_MESH_INSTANCE_DESCRIPTOR 2
#endif
#define DESCRIPTOR_BINDLESS_SET 0
#include "material.glsl"

layout (set = 1, binding = 0) uniform ShadowMatrixData {
	mat4 viewProjection[4];
	mat4 invView[4];
	mat4 invProjection[4];
} shadowBuffer;

#ifdef ACTOR_DYNAMIC
READONLY_BUFFER DynamicActorData {
	mat4 modelMatrix;
	mat3 normalMatrix;
	mat4 modelMatrixPrevFrame;
} dynamicActorData[];
layout (push_constant) uniform Push {
	uint actorBuffer;
	uint materialID;
	int layer;
} push;
#else
layout (push_constant) uniform Push {
	mat4 modelMatrix;
	uint materialID;
	int layer;
} push;
#endif
layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv;
layout(location = 0) out vec2 fragUV;
layout(location = 1) out vec3 fragPosWorld;
layout(location = 3) out flat uint materialIndex;

#include "vertex_transform.glsl"


void main() {
	
#ifdef ACTOR_DYNAMIC
	mat4 modelMatrix = dynamicActorData[push.actorBuffer].modelMatrix;
#else
	mat4 modelMatrix = push.modelMatrix;
#endif
	materialIndex = push.materialID;
	
	VertexData vertexData = vertex_Transform();
	vec4 worldPos = modelMatrix * vec4(vertexData.position, 1.0);
	gl_Position = shadowBuffer.viewProjection[push.layer]  * vec4(worldPos.xyz, 1.0);
	fragPosWorld = worldPos.xyz;
	
	MaterialData sfMaterial = getMaterialData(materialIndex);
	if (
#ifndef ENABLE_ALPHA_TEST
	matHasAlphaMask(sfMaterial) || 
#endif
	matHasDiffuseTexture(sfMaterial) || matHasOpacityMap(sfMaterial)) {
		fragUV = uv;
	} else {
		fragUV = vec2(0.0); // not using the UV if no alpha mask
	}
	
}
