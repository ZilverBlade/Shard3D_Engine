#version 450
#extension GL_EXT_control_flow_attributes : enable
#define MAX_NUM_CASCADES 4
layout (triangles) in;
layout (triangle_strip, max_vertices = 3 * MAX_NUM_CASCADES) out; 

#ifdef ENABLE_ALPHA_TEST
layout(location = 0) in vec2 fragUV[];
layout(location = 0) out vec2 fragUV_out;
#endif

const float size[4] = {
	1.0 / 1.0, 1.0 / 4.0, 1.0 / 16.0, 1.0 / 64.0
};

void main() {
	[[unroll]]
	for (int c = 0; c < 4; c++) {
		[[unroll]]
 		for (int i = 0; i < 3; i++) {
			gl_Position = gl_in[i].gl_Position;
			gl_Position.xy *= size[c];
			gl_Layer = c;
			EmitVertex();
		}
		EndPrimitive();
	}
}