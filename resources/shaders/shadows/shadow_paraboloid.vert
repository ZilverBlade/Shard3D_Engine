#version 450
#ifdef InstancedDrawing
#extension GL_ARB_shader_viewport_layer_array : enable
#endif

#define DISABLE_PARTIAL_DERIVATIVES
#ifdef RIGGED_MESH
#define RIGGDED_MESH_INSTANCE_DESCRIPTOR 2
#endif
#define DESCRIPTOR_BINDLESS_SET 0
#include "material.glsl"
#include "paraboloid.glsl"

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv;
layout(location = 0) out vec3 fragPosWorld;
#ifdef ENABLE_ALPHA_TEST
layout(location = 1) out vec2 fragUV;
#endif
layout(location = 2) out flat vec3 lightPosition;
layout(location = 3) out flat float farZ;
layout(location = 4) out flat uint materialIndex;
layout(location = 5) out vec2 outDepth;

#include "vertex_transform.glsl"

layout (set = 1, binding = 0) uniform OmniShadowMatrixData {
	mat4 view[6];
	mat4 invView[6];
	mat4 invProjection[6];
} shadowBuffer;

const float zNear = 0.01;
const float zFar = 256.0;

#ifdef ACTOR_DYNAMIC
READONLY_BUFFER DynamicActorData {
	mat4 modelMatrix;
	mat3 normalMatrix;
	mat4 modelMatrixPrevFrame;
} dynamicActorData[];
layout (push_constant) uniform Push {
	uint actorBuffer;
	uint materialID;
	int layer;
	float maxPlane;
} push;
#else
layout (push_constant) uniform Push {
	mat4 modelMatrix;
	uint materialID;
	int layer;
	float maxPlane;
} push;
#endif
void main() {
#ifdef ACTOR_DYNAMIC
	mat4 modelMatrix = dynamicActorData[push.actorBuffer].modelMatrix;
#else
	mat4 modelMatrix = push.modelMatrix;
#endif
	materialIndex = push.materialID;
	
	VertexData vertexData = vertex_Transform();
	vec4 worldPos = modelMatrix * vec4(vertexData.position, 1.0);
	fragPosWorld = worldPos.xyz;
	
	farZ = push.maxPlane;
	int layer = 
#ifdef InstancedDrawing
	 gl_InstanceIndex;
#else
	 push.layer;
#endif

	vec4 projPos =  getParaboloidPosition(vec3(shadowBuffer.view[layer] * vec4(worldPos.xyz, 1.0)), zNear, zFar);
	outDepth = projPos.zw;
	gl_Position = projPos;
	lightPosition =  shadowBuffer.invView[layer][3].xyz;
	
#ifdef InstancedDrawing
		gl_Layer = gl_InstanceIndex;
		gl_ViewportIndex = gl_InstanceIndex;
#endif

	
#ifdef ENABLE_ALPHA_TEST
	fragUV = uv;
#endif
	
}
