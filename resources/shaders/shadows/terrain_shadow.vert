#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_control_flow_attributes : enable

#include "terrain.glsl"

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv;

#ifdef ReflectiveShadow
layout(location = 1) out vec3 fragPosWorld;
#endif
layout (set = 1, binding = 0) uniform ShadowMatrixData {
	mat4 viewProjection[4];
	mat4 invView[4];
	mat4 invProjection[4];
} shadowBuffer;

layout(set = 2, binding = 0) buffer TileVisibility {
	TerrainTileData tiles[1];
} tileData;
layout(set = 2, binding = 2) uniform sampler2D heightMap;

layout (push_constant) uniform Push {
	vec4 translation;
	vec2 tiles;
	float tileExtent;
	float heightMul;
	uint LOD;
	int layer;
} push;

void main() {
	TerrainTileData tileData = tileData.tiles[push.LOD * int(push.tiles.x * push.tiles.y) + gl_InstanceIndex];
	
	TerrainData data;
	
	data.translation	= push.translation;
	data.tiles			= push.tiles; 
	data.tileExtent	= push.tileExtent;
	data.heightMul		= push.heightMul;
	data.LOD			= push.LOD;
	
	vec3 vertex = getTransformTerrain(data, heightMap, position, uv, tileData);

	gl_Position = shadowBuffer.viewProjection[push.layer] * vec4(vertex, 1.0);
	
#ifdef ReflectiveShadow
	fragPosWorld = vertex;
#endif
}