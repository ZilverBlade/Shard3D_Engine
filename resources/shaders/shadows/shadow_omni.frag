#version 450
#extension GL_EXT_nonuniform_qualifier : enable
#define DESCRIPTOR_BINDLESS_SET 0
#include "material.glsl"

#ifdef VarianceShadow
layout (location = 0) out vec2 variance;
#endif

layout(depth_less) out float gl_FragDepth;
layout(location = 0) in vec3 fragPosWorld;
#ifdef ENABLE_ALPHA_TEST
layout(location = 1) in vec2 fragUV;
#endif
layout(location = 2) in flat vec3 lightPosition;
layout(location = 3) in flat float farPlane;
layout(location = 4) in flat uint materialIndex;
layout(location = 5) in vec2 inDepth;

void main() {
#ifdef ENABLE_ALPHA_TEST
    MaterialData sfMaterial = getMaterialData(materialIndex);
	if (matHasAlphaMask(sfMaterial)) {
		vec2 txcOff = sfMaterial.factorData.texCoordOffset;
		vec2 txcMul = sfMaterial.factorData.texCoordMultiplier;
		if (texture(texID[sfMaterial.textureData.maskTex_id], fragUV * txcMul + txcOff).x < 0.5)
			discard;
	}
#endif
	float slopeFactor = gl_FragCoord.z - (inDepth.x / inDepth.y);
	float depth = length(lightPosition - fragPosWorld) / farPlane;
	gl_FragDepth = depth + slopeFactor * 2.0;
#ifdef VarianceShadowCode
	float d = depth;
	
	float dx = dFdx(d);
	float dy = dFdy(d);
	float moment_y = d * d + 0.25 * (dx * dx + dy * dy);

	variance = vec2(d, moment_y);
#endif
}