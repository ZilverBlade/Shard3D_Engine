#version 450
#define DISABLE_PARTIAL_DERIVATIVES

#ifdef RIGGED_MESH
#define RIGGDED_MESH_INSTANCE_DESCRIPTOR 2
#endif
#define DESCRIPTOR_BINDLESS_SET 0
#include "descriptor_indexing.glsl"
#ifndef ENABLE_ALPHA_TEST
#ifdef ReflectiveShadow
#include "material.glsl"
#endif
#endif

#ifdef ACTOR_DYNAMIC
READONLY_BUFFER DynamicActorData {
	mat4 modelMatrix;
	mat3 normalMatrix;
	mat4 modelMatrixPrevFrame;
	uint materialID;
} dynamicActorData[];
layout (push_constant) uniform Push {
	uint actorBuffer;
	uint materialID;
	int layer;
} push;
#else
layout (push_constant) uniform Push {
	mat4 modelMatrix;
	uint materialID;
	int layer;
} push;
#endif

layout (set = 1, binding = 0) uniform ShadowMatrixData {
	mat4 viewProjection[4];
	mat4 invView[4];
	mat4 invProjection[4];
} shadowBuffer;

layout(location = 0) in vec3 position;
#include "vertex_transform.glsl"
#if defined(ENABLE_ALPHA_TEST) || defined(ReflectiveShadow)
layout(location = 1) in vec2 uv;
layout(location = 0) out vec2 fragUV;
#endif
#ifdef ReflectiveShadow
layout(location = 1) out vec3 fragPosWorld;
#endif
layout(location = 3) out flat uint materialIndex;


void main() {
	
#ifdef ACTOR_DYNAMIC
	mat4 modelMatrix = dynamicActorData[push.actorBuffer].modelMatrix;
#else
	mat4 modelMatrix = push.modelMatrix;
#endif
	materialIndex = push.materialID;
	
	VertexData vertexData = vertex_Transform();

	vec4 worldPos = modelMatrix * vec4(vertexData.position, 1.0);
	gl_Position = shadowBuffer.viewProjection[push.layer] * vec4(worldPos.xyz, 1.0);
	
#ifdef ENABLE_ALPHA_TEST
	fragUV = uv;
#endif
#ifndef ENABLE_ALPHA_TEST
#ifdef ReflectiveShadow
	MaterialData sfMaterial = getMaterialData(materialIndex);
	if (matHasDiffuseTexture(sfMaterial)) {
		fragUV = uv;
	} else {
		fragUV = vec2(0.0); 
	}
#endif
#endif

#ifdef ReflectiveShadow
	fragPosWorld = worldPos.xyz;
#endif
}