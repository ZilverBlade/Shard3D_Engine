#version 450
#define DESCRIPTOR_BINDLESS_SET 0
#include "material.glsl"
layout (set = 1, binding = 0) uniform ShadowMatrixData {
	mat4 viewProjection;
	mat4 invView;
	mat4 invProjection;
} shadowBuffer;
layout (location = 0) out vec3 translucentColor; // R5G6B6
layout (location = 1) out float translucentFactor; // R8
layout (location = 0) in vec2 fragUV;
layout (location = 1) in vec3 fragPosWorld;
layout(location = 3) in flat uint materialIndex;


void main(){
	MaterialData sfMaterial = getMaterialData(materialIndex);
	vec2 txcOff = sfMaterial.factorData.texCoordOffset;
	vec2 txcMul = sfMaterial.factorData.texCoordMultiplier;
	vec2 coord = fragUV * txcMul + txcOff;
#ifdef ENABLE_ALPHA_TEST
	if (matHasAlphaMask(sfMaterial)) {
		if (texture(texID[sfMaterial.textureData.maskTex_id], fragUV * txcMul + txcOff).x < 0.5)
			discard;
	}
#endif
//	vec3 wpDx = dFdx(fragPosWorld);
//	vec3 wpDy = dFdy(fragPosWorld);
//	vec3 N = normalize(cross(wpDy, wpDx));
//	if (dot(N, N) < 0.000001) {
//		N = normalize(fwidth(N)); // get closest accurate normal
//	}
//	if (gl_FrontFacing == false) {
//		N = -N;
//	}
//	vec3 V = shadowBuffer.invView[3].xyz;
//	float fresnel = pow(dot(N, V), 5.0);
//	
//	const float FRESNEL_SCALE = 0.0;
	
	vec3 diffuse = sfMaterial.factorData.diffuse.xyz;
	if (matHasDiffuseTexture(sfMaterial)) {
		diffuse *= texture(texID[sfMaterial.textureData.diffuseTex_id], coord).xyz;
	}
	float opacity = sfMaterial.factorData.opacity;
	if (matHasOpacityMap(sfMaterial)) {
		opacity *= texture(texID[sfMaterial.textureData.opacityTex_id], coord).x;
	}
	float oneMinOpacity = (1.0 - opacity);
	translucentColor = diffuse * pow(oneMinOpacity, 1.0 / 3.0); // better curve for translucent shadows
	translucentFactor = opacity; //* mix(1.0, fresnel, FRESNEL_SCALE);
}
