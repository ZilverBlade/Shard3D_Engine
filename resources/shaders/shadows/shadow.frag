#version 450
#extension GL_EXT_nonuniform_qualifier : enable
#define DESCRIPTOR_BINDLESS_SET 0
#include "material.glsl"

// 0 == Dir, 1 == Spot
layout (constant_id = 0) const uint LIGHT_TYPE = 0;

layout (set = 1, binding = 0) uniform ShadowMatrixData {
	mat4 viewProjection[4];
	mat4 invView[4];
	mat4 invProjection[4];
	// RSM LIGHT DATA
	
	vec3 RSMLIGHT_direction;
	vec3 RSMLIGHT_intensity;
	vec3 RSMLIGHT_position;
	vec2 RSMLIGHT_cosAngles;
} shadowBuffer;

#define LAST_IMG_LOCATION -1
#ifdef VarianceShadow
layout (location = 0) out vec2 variance;
#undef LAST_IMG_LOCATION
#define LAST_IMG_LOCATION 0
#endif
#ifdef ReflectiveShadow
layout (location = LAST_IMG_LOCATION + 1) out vec3 flux;
layout (location = LAST_IMG_LOCATION + 2) out vec4 normal;
#endif

#if defined(ENABLE_ALPHA_TEST) || defined(ReflectiveShadow)
layout(location = 0) in vec2 fragUV;
#endif
#ifdef ReflectiveShadow
layout(location = 1) in vec3 fragPosWorld;
#endif
layout(location = 3) in flat uint materialIndex;

void main(){
#if defined(ENABLE_ALPHA_TEST) || defined(ReflectiveShadow)
	MaterialData sfMaterial = getMaterialData(materialIndex);
	vec2 txcOff = sfMaterial.factorData.texCoordOffset;
	vec2 txcMul = sfMaterial.factorData.texCoordMultiplier;
	vec2 coord = fragUV * txcMul + txcOff;
#endif
#ifdef ENABLE_ALPHA_TEST
	if (matHasAlphaMask(sfMaterial)) {
		if (texture(texID[sfMaterial.textureData.maskTex_id], coord).x < 0.5)
			discard;
	}
#endif

#ifdef VarianceShadow
	
	float d = gl_FragCoord.z;
	
	float dx = dFdx(d);
	float dy = dFdy(d);
	float moment_y = d * d + 0.25 * (dx * dx + dy * dy);

	variance = vec2(d, moment_y);
#endif

#ifdef ReflectiveShadow
	vec3 diffuse = sfMaterial.factorData.diffuse.rgb;
	if (matHasDiffuseTexture(sfMaterial)) {
		diffuse *= texture(texID[sfMaterial.textureData.diffuseTex_id], coord).rgb;
	}
	vec3 wpDx = dFdx(fragPosWorld);
	vec3 wpDy = dFdy(fragPosWorld);
	vec3 N = normalize(cross(wpDy, wpDx));
	if (gl_FrontFacing == false) {
		N = -N;
	}
	
	if (LIGHT_TYPE == 0) {
		flux = max(dot(N, -shadowBuffer.RSMLIGHT_direction.xyz), 0.0) * shadowBuffer.RSMLIGHT_intensity * diffuse;
	} else if (LIGHT_TYPE == 1) {
		vec3 fragToLight = shadowBuffer.RSMLIGHT_position - fragPosWorld;
		vec3 L = normalize(fragToLight);
		float theta = dot(L, -shadowBuffer.RSMLIGHT_direction.xyz);
	
		if (theta > shadowBuffer.RSMLIGHT_cosAngles.x) { 
			float epsilon = shadowBuffer.RSMLIGHT_cosAngles.y - shadowBuffer.RSMLIGHT_cosAngles.x;
			float intensity = clamp((theta - shadowBuffer.RSMLIGHT_cosAngles.x) / epsilon, 0.0, 1.0);
			float attenuation = 1.0 / dot(fragToLight, fragToLight);
			flux = attenuation * intensity * max(dot(N, L), 0.0) * shadowBuffer.RSMLIGHT_intensity * diffuse;
		} else {
			flux = vec3(0.0);
		}
	
	}
	normal = vec4(N * 0.5 + 0.5, 1.0);
#endif

}